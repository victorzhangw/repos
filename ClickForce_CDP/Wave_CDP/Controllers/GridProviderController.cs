﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using CACO_CDP.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Globalization;
using Dapper;
using Newtonsoft.Json;
using static CACO_CDP.Models.Grids;
using System.IO;
using System.IO.Compression;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CACO_CDP.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GridProviderController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<GridProviderController> _logger { get; set; }
        public GridProviderController(IConfiguration config, ILogger<GridProviderController> logger)
        {

            _configuration = config;
            _logger = logger;
        }
        [HttpPost, HttpGet]
        public object GetGridData([FromForm]Authorize.GridParameter paras)
        {
            _logger.LogInformation("GetGridData Start:");
            var result = string.Empty;
            string[] condArray;
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            List<string> conditions = new List<string>();

            switch (paras.Source)
            {

                case "memberlabel":
                    try {
                        using (var connection = new MySqlConnection(cnStr.Value))
                        {
                            var myCommand = connection.CreateCommand();
                            var parameters = new DynamicParameters();
                            if (!String.IsNullOrEmpty(paras.Clause))
                            {
                                condArray = paras.Clause.Split("|");
                                for (int i = 0; i < condArray.Length; i++)
                                {
                                    string _value = "%" + condArray[i] + "%";
                                    conditions.Add($"label like @label{i}");
                                    //myCommand.Parameters.Add($"@label{i}", MySqlDbType.String).Value = condArray[i];
                                    parameters.Add(name: $"@label{i}", value: _value, dbType: DbType.String);
                                    //parameters.Add($"@label{i}", "%"+condArray[i]+"%");
                                }
                            }


                            myCommand.CommandText = string.Format(CultureInfo.CurrentCulture, @"SELECT * FROM memberanalysisresult{0}{1}",
                           conditions.Count > 0 ? " WHERE " : "", string.Join(" OR ", conditions.ToArray()));
                            if (!(paras.Limit == "0"))
                            {
                                myCommand.CommandText += string.Format(CultureInfo.CurrentCulture, "{0}order by memberid limit {1} offset {2}", " ", paras.Limit, paras.Offset);
                            }


                            //_logger.LogInformation("GetGridData :SQL statement:{0}" + myCommand.CommandText);
                            var querylist = connection.Query<Grids.MemberLabel>(myCommand.CommandText, parameters).ToList();
                            ;
                            List<Grids.Result> resultList = new List<Grids.Result>();
                            if (conditions.Count > 0)
                            {
                                resultList.Add(new Grids.Result { count = querylist.Count, result = querylist });
                            }
                            else
                            {
                                string totalCountSql = "SELECT count(*) from memberanalysisresult ";
                                int totalCount = connection.Query<int>(totalCountSql).FirstOrDefault();
                                resultList.Add(new Grids.Result { count = totalCount, result = querylist });
                            }

                            result = JsonConvert.SerializeObject(resultList);

                        }
                    } 
                    catch(Exception ex)
                    {
                        _logger.LogError(ex.Message);
                    }
                   
                    
                    break;
                case "labelCategory":
                    try {

                        using (var connection = new MySqlConnection(cnStr.Value))
                        {
                            string parentsql = "select id,labelname from  label where label.parent ='' or  ISNULL(label.parent)";
                            string childsql = @"select id,labelname from  label where parent = @parent ORDER BY
                            CONVERT(SPLIT_STR(id, '-', 2), INTEGER) asc; ";
                            List<LabelCategoryResult> labelCategoryResults = new List<LabelCategoryResult>();
                            List<LabelCategory> parentResult = connection.Query<LabelCategory>(parentsql).ToList();

                            if (parentResult.Count > 0)
                            {
                                for(int i=0; i< parentResult.Count; i++)
                                {
                                    var childResult = connection.Query<LabelCategory>(childsql,new { parent= parentResult[i].id }).ToList();
                                    labelCategoryResults.Add(new LabelCategoryResult { 
                                        id = parentResult[i].id, labelname = parentResult[i].labelname, child = childResult });

                                }
                                result = JsonConvert.SerializeObject(labelCategoryResults);

                            }
                            else
                            {
                                _logger.LogError("No Parent label ！");
                            }
                            
                        }


                    }
                    catch(Exception ex)
                    {
                        _logger.LogError(ex.Message);
                    }
                    break;
                default:
                    break;
            }
            //byte[] UTF8bytes = Encoding.UTF8.GetBytes(result);
            //Response.Headers.Add("Content-Encoding", "gzip");
            //Response.Headers.Add("Content-Type", "application/json");
            return result;

        }

      
       
        /// <summary>
        /// GZip the byte.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private byte[] GZipByte(byte[] data)
        {
            using (var output = new MemoryStream())
            {
                using (var compressor = new GZipStream(output, CompressionMode.Compress))
                {
                    compressor.Write(data, 0, data.Length);
                    compressor.Close();
                }

                return output.ToArray();
            }
        }

        /// <summary>
        /// Deflate the byte.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private byte[] DeflateByte(byte[] data)
        {
            using (var output = new MemoryStream())
            {
                using (var compressor = new DeflateStream(output, CompressionMode.Compress))
                {
                    compressor.Write(data, 0, data.Length);
                    compressor.Close();
                }

                return output.ToArray();
            }
        }

    }
}