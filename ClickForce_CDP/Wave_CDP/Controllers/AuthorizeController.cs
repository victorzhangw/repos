﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using Microsoft.Extensions.Configuration;
using CACO_CDP.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Dapper;
using RestSharp;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Logging;

namespace CACO_CDP.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<AuthorizeController> _logger { get; set; }
        public AuthorizeController(IConfiguration config,ILogger<AuthorizeController> logger)
        {
            _configuration = config;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GoogleAuthorize(string gmailId)
        {
            var clientId = _configuration.GetSection("ClientId");
            var client_secret = _configuration.GetSection("client_secret");
            string url = "https://accounts.google.com/o/oauth2/auth?";
            string redirectUrl = "https://localhost:44307/api/Authorize/CallBack";
            string Scopes = @"https://www.googleapis.com/auth/analytics ";
            
            StringBuilder urlbuilder = new StringBuilder(url);
            urlbuilder.Append("client_id=" + clientId.Value);
            urlbuilder.Append("&redirect_uri=" + redirectUrl);
            urlbuilder.Append("&response_type=" + "code");
            urlbuilder.Append("&scope=" + Scopes);
            urlbuilder.Append("&access_type=" + "offline");
            urlbuilder.Append("&state=" + gmailId); //setting the user id in state  
            //HttpContext.Current.Response.Redirect(Url, false);
            string getCodeUrl = urlbuilder.ToString();
            HttpContext.Response.Redirect(getCodeUrl, false);
            return Ok();
        }
        [HttpGet]
        public IActionResult CallBack([FromQuery(Name = "code")] string code,[FromQuery(Name = "state")] string id)
        {
            string token = ExchangeAuthorizationCode(id,code);
            return Ok();
        }
        private string ExchangeAuthorizationCode(string userId, string code)
        {
            string accessToken = string.Empty;

            var ClientSecret = _configuration.GetSection("client_secret");
            var ClientId = _configuration.GetSection("ClientId");
           ;
            string RedirectUrl = "https://localhost:44307/api/Authorize/CallBack";
            var Content = @"code=" + code +
                "&client_id=" + ClientId.Value +
                "&client_secret=" + ClientSecret.Value +
                "&redirect_uri=" + RedirectUrl +
                "&grant_type=authorization_code";
            var client = new RestClient("https://accounts.google.com/o/oauth2/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Host", "accounts.google.com");
            request.AddParameter("undefined", Content, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var result = response.Content;
           
            

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var ReturnedToken = JsonConvert.DeserializeObject<Authorize.Token>(result);
                
                if (ReturnedToken.refresh_token != null)
                {
                    var rowCount =  SaveRefreshToken(userId, ReturnedToken.refresh_token);
                    accessToken = ReturnedToken.access_token;
                    return ReturnedToken.refresh_token;
                }
                else if (ReturnedToken.refresh_token == null && ReturnedToken.access_token != null)
                {
                    return "AccessTokenLive";
                }

                else
                {
                    return null;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        private async Task <int> SaveRefreshToken(string userId, string refreshToken)
        {
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string insertSql =@"INSERT INTO token(userid,refreshtoken, tokentype) VALUES (@userid,@refreshtoken,@tokentype)";
            string checkSql = "SELECT distinct id FROM token where userid=@userid";
            string updateSql = "UPDATE token SET refreshtoken=@refreshtoken where userid=@userid";
            try
            {
                using (var connection = new MySqlConnection(cnStr.Value))
                {
                    await connection.OpenAsync();
                    var token = new Authorize.Token()
                    {
                        refresh_token = refreshToken,
                        user_id = userId,
                        token_type="Google"
                    };
                    int usercount = connection.Query<Authorize.Token>(checkSql, new { userid= userId }).Count();
                    if (usercount == 0)
                    {
                        await connection.ExecuteAsync(insertSql, new {userid=token.user_id, refreshtoken= token.refresh_token, tokentype=token.token_type
                            });
                    }
                    else
                    {
                        await connection.ExecuteAsync(updateSql, new { refreshtoken=token.refresh_token,userid=token.user_id });
                    }
                   
                 
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.ToString());
                Console.WriteLine(ex);
            }
            return 0;
          
           
        }


    }
}