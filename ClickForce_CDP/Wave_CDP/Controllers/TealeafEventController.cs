﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CACO_CDP.Helpers;
using Microsoft.Extensions.Configuration;
using Dapper;
using CACO_CDP.Models;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Logging;
using System.Net.Mail;

namespace CACO_CDP.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TealeafEventController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly MailHelpers _mailhelper;
        private readonly CsvHelpers _csvhelper;
        private readonly TealeafEventHelpers _tealeafEventHelpers;

        
        private ILogger<TealeafEventController> _logger { get; set; }
        public TealeafEventController(IConfiguration config,TealeafEventHelpers tealeafEventHelpers ,MailHelpers mailHelpers, CsvHelpers csvHelpers, ILogger<TealeafEventController> logger)
        {
            _mailhelper = mailHelpers;
            _configuration = config;
            _csvhelper = csvHelpers;
            _tealeafEventHelpers = tealeafEventHelpers;
            _logger = logger;
            
        }
        [HttpGet]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        public IActionResult ImportEvents()
        {
            var isRetrieve= _mailhelper.RetrieveMail();
            bool imported=false;    
            string mailHeaderKeyWord = _configuration.GetSection("EmailConfiguration:mailHeaderKeyWord").Value;
            string returnString = $"mailHeaderKeyWord  + {DateTime.Now:yyyy/MM/dd}+ 資料未匯入";
            if (isRetrieve)
            {
                string recordCount = _csvhelper.ParseCsvFiles().ToString();
                imported = true;
                returnString = mailHeaderKeyWord + "成功:" + imported.ToString() + ",資料筆數:" + recordCount;
            }
            SendAutomatedEmail("victor.cheng@aurocore.com", $"{DateTime.Now:yyyy/MM/dd}-Tealeaf 資料匯入", returnString, "caco.tealeaf@gmail.com");
            return Ok(returnString);
        }
        [HttpPost]
        public object TealeafReport([FromForm]Authorize.TealeafReportParameter paras)
        {
            _logger.LogInformation("TealeafEventController {0}", "TealeafReport");
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string memberKey= paras.MemberKey;
            string loginID= paras.LoginId;
            DateTime startDate = Convert.ToDateTime(paras.StartDate);
            DateTime endDate = Convert.ToDateTime(paras.EndDate);
            //DateTime startDate= DateTime.ParseExact(paras.StartDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            //DateTime endDate= DateTime.ParseExact(paras.EndDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            
            string selectCommnad = @"SELECT eventstring,eventname,eventdate FROM tealeaf where eventdate
            between @startDate and @endDate";
            
            try
            {
                using (var connection = new MySqlConnection(cnStr.Value))
                {

                    var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad, new { startDate = paras.StartDate, endDate= paras.EndDate }).ToList();
                    //var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad1).ToList();
                    var tealeafdata = _tealeafEventHelpers.EventParser(tealeafEventsData);
                    //_logger.LogInformation("Bye {0}", "TealeafReport");
                    return tealeafdata;
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return Ok();
            }
            
        }

        public  void SendAutomatedEmail(string ReceiveMail,string Subject,string Body,string FromAddress)
        {
            var smtpServer = _configuration.GetSection("EmailConfiguration:SmtpServer").Value;
            var smtpPort = Int32.Parse(_configuration.GetSection("EmailConfiguration:SmtpPort").Value);
            var userName = _configuration.GetSection("EmailConfiguration:SmtpUsername").Value;
            var userPassword = _configuration.GetSection("EmailConfiguration:SmtpPassword").Value;
            
            MailMessage MyMail = new MailMessage();
            MyMail.From = new System.Net.Mail.MailAddress(FromAddress);
            MyMail.To.Add(ReceiveMail); //設定收件者Email
           // MyMail.Bcc.Add("密件副本的收件者Mail"); //加入密件副本的Mail          
            MyMail.Subject = Subject;
            MyMail.Body = Body; //設定信件內容
            MyMail.IsBodyHtml = true; //是否使用html格式
            SmtpClient MySMTP = new SmtpClient(smtpServer, smtpPort);
            MySMTP.EnableSsl = true;
            MySMTP.Credentials = new System.Net.NetworkCredential(userName,userPassword);
            try
            {
                MySMTP.Send(MyMail);
                MyMail.Dispose(); //釋放資源
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ex.ToString();
            }
        }


    }
}