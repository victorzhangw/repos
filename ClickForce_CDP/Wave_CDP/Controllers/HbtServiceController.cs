﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using CACO_CDP.Helpers;
using Ionic.Zip;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CACO_CDP.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HbtServiceController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly CsvHelpers _csvhelper;
        private FtpHelpers _ftpHelpers;
        private ILogger<HbtServiceController> _logger { get; set; }
        public HbtServiceController(IConfiguration config, ILogger<HbtServiceController> logger, FtpHelpers ftpHelpers, CsvHelpers csvHelpers)
        {
            _ftpHelpers = ftpHelpers;
            _configuration = config;
            _logger = logger;
            _csvhelper = csvHelpers;

        }

        [HttpGet]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        public bool DownloadData()
        {
            try {
                var isRetrieve = _ftpHelpers.HBFtpDownload();
                if (isRetrieve)
                {
                    var outDirPath = _configuration.GetSection("FtpConfiguration:ftp1:filepath").Value;
                    var zipDirPath = _configuration.GetSection("FtpConfiguration:ftp1:archivepath").Value;
                    outDirPath = outDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
                    zipDirPath = zipDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
                    using (ZipFile zip = new ZipFile(Encoding.Default))
                    {

                        //string filePath = Path.Combine(outDirPath);
                        string filePath = Path.GetFullPath(Path.Combine(outDirPath));

                        string zipFileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                        // string zipPath = Path.Combine(zipDirPath, zipFileName);
                        string zipPath = Path.GetFullPath(Path.Combine(zipDirPath, zipFileName));

                        zip.AddDirectory(filePath);
                        zip.Save(zipPath);

                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                return false;
            }
            
            return true;
        }
        [HttpGet]
        public IActionResult SaveData()
        {
            var isRetrieve = _csvhelper.ParseHBTFiles();

            return Ok();
        }



    }
}