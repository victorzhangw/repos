﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CACO_CDP.Models
{
    public static class Grids
    {
        public class MemberLabel
        {
            public string memberid { get; set; } //會員 ID
            public string membername { get; set; } //姓名
            public string shippingaddress { get; set; } // 遞送地址
            public string contactaddress { get; set; } // 聯絡地址
            public string membermail { get; set; } // 電子郵件
            public string label { get; set; } // 標籤
        }
        public class Result
        {
            public int count { get; set; } //會員 ID
            public List<MemberLabel> result { get; set; } //姓名
           
        }
        public class LabelCategory
        {
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("text")]
            public string labelname { get; set; } 
        }
        public class LabelCategoryResult
        {
            [JsonProperty("id")]
            public string id { get; set; } //會員 ID
            [JsonProperty("text")]
            public string labelname { get; set; } //會員 ID
            public List<LabelCategory> child { get; set; } //姓名

        }
    }
}
