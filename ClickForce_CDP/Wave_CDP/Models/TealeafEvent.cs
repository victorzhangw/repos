﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using System.Web;

namespace CACO_CDP.Models
{
    public class TealeafEvent
    {

        
        public class SessionCounts
        {
            public string Day { get; set; }
            public int Counts { get; set; }


        }
        public class PlatformCounts
        {
            public string Day { get; set; }
            public int Counts { get; set; }
        }
        public class TealeafEvents
        {
            public string eventstring { get; set; }
            public string eventname { get; set; }
            public DateTime eventdate { get; set; }
        }
        public class ReportDataset
        {
            public string Column1 { get; set; } = "0";
            public string Column2 { get; set; } = "0";
            public string Column3 { get; set; } = "0";
            public string Column4 { get; set; } = "0";
            public string Column5 { get; set; } = "0";
            public string Column6 { get; set; } = "0";
            public string Column7 { get; set; } = "0";
            public string Column8 { get; set; } = "0";
            public string Column9 { get; set; } = "0";
        }
        public class ReportCollection
        {
            public string Name { get; set; }
            public dynamic  Data { get; set; } 
        }
        public class ubx_ibmvideoplayed
        {
            private string identifier = string.Empty;
            public string version { get; set; }
            public string provider { get; set; }
            public string source { get; set; }
            public string channel { get; set; }
            public string x1id { get; set; }
            public string event_code { get; set; }
            public string event_timestamp { get; set; }
            public string event_namespace { get; set; }
            public string event_version{ get; set; }
            public string identifier_deviceid{
                get { return identifier; }
                set {
                    string[] identifierAry;
                    identifier = HttpUtility.UrlDecode(identifier);
                    identifierAry = identifier.Split("|");
                    for(int i = 0; i < identifierAry.Length; i++)
                    {
                        if (String.IsNullOrWhiteSpace(identifierAry[i]))
                        {
                            identifierAry[i] = "NONE";
                        }
                    }
                    identifier = String.Join("|", identifierAry);
                    
                }

            }
            public string identifier_googleanalyticscookie { get; set; }
            public string event_attribute_interactionid { get; set; }
            public string event_attribute_browsername { get; set; }
            public string event_attribute_browserversion { get; set; }
            public string event_attribute_vendor { get; set; }
            public string event_attribute_model { get; set; }
            public string event_attribute_manufacturer { get; set; }
            public string event_attribute_modelname { get; set; }
            public string event_attribute_modelyear { get; set; }
            public string event_attribute_ip { get; set; }
            public string event_attribute_locationcontinent { get; set; }
            public string event_attribute_locationcountry { get; set; }
            public string event_attribute_locationcity { get; set; }
            public string event_attribute_latitude { get; set; }
            public string event_attribute_longitude { get; set; }
            public string event_attribute_os { get; set; }
            public string event_attribute_versionos { get; set; }
            public string event_attribute_subchannel { get; set; }
            public string event_attribute_devicetype { get; set; }
            public string event_attribute_tldatacenter { get; set; }
            public string event_attribute_tlorgcode { get; set; }
            public string event_attribute_appkey { get; set; }
            public string event_attribute_tlprocid { get; set; }
            public string event_attribute_eventname { get; set; }
            public string event_attribute_elementid { get; set; }
            public string identifier_dacookieid { get; set; }

        }
        }
    }
