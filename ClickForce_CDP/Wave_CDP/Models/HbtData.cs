﻿using ChoETL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace CACO_CDP.Models
{
    public class HbtData
    {
        [Serializable]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1034:Nested types should not be visible", Justification = "<Pending>")]
        public class Members
        {
            [ChoCSVRecordField(2, FieldName = "memberid")]
            public string memberid { get; set; }
            [ChoCSVRecordField(3, FieldName = "membername")]
            public string membername { get; set; }
            public string shippingaddress { get; set; }
            public string contactaddress { get; set; }
            [ChoCSVRecordField(5, FieldName = "memberemail")]
            public string memberemail { get; set; }
            [ChoCSVRecordField(1)]
            public string channel { get; set; }
            public string gender { get; set; }
            
            [ChoCSVRecordField(4, FieldName = "registerdate")]
            public string registerdate { get; set; }
            public string create_date { get; set; }
            public string modify_date { get; set; }


        }
        public class Orders
        {
            public string channel { get; set; }
            public string orderno { get; set; }
            public string orderdate { get; set; }
            public string shop { get; set; }
            public string memberid { get; set; }
            public string membername { get; set; }
            public string productid { get; set; }
            public string productcategory { get; set; }
            public string unitprice { get; set; }
            public string quantity { get; set; }
            public string amount { get; set; }
            public string create_date { get; set; }
            public string modify_date { get; set; }
            public string ordertype { get; set; }
            public string shippingaddress { get; set; }
            public string ordercampaign { get; set; }
        }
        public class Products
        {
            public string product_id { get; set; }
            public string product_group_id { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string analyze_title { get; set; }
            public string analyze_description { get; set; }
            public string availability { get; set; }
            public string price { get; set; }
            public string currency { get; set; }
            public string brand { get; set; }
            public string product_type { get; set; }
            public string condition { get; set; }
            public string size { get; set; }
            public string size_system { get; set; }
            public string color { get; set; }
            public string pattern { get; set; }
            public string sale_price { get; set; }
            public string custom_label_0 { get; set; }//年度
            public string custom_label_1 { get; set; }//季別
            
            public string create_date { get; set; }
            public string modify_date { get; set; }
        }
        public class RevenuefromHBT{
            public List<SalesRevenue> salesRevenues { get; set; }        
        }
        public class SalesRevenue
        {
            public double _salesratio = 0;
            public string category { get; set; }
            public string id { get; set; }
            public string revenue { get; set; }
            public double salesratio
            {
                get { return _salesratio; }
                set { _salesratio = value * 100; }
            }
        }
    }
}
