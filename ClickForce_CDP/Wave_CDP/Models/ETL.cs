﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CACO_CDP.Models
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "<Pending>")]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1034:Nested types should not be visible", Justification = "<Pending>")]
	public class ETL
    {
		
		public class Product_dimensions
		{
			
			public string product_id { get; set; }
			public string dim_category { get; set; }
			public string dim_pricelevel { get; set; }
			public string dim_color { get; set; }
			public string dim_brand { get; set; }
			public string dim_type { get; set; }

			
		}
	}
}
