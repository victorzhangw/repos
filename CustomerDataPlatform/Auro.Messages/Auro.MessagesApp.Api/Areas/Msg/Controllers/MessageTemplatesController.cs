using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.Commons.Linq;
using Auro.Commons.Mapping;
using Auro.Commons.Models;
using Auro.Messages.Dtos;
using Auro.Messages.IServices;
using Auro.Messages.Models;

namespace Auro.MessagesApp.Api.Areas.Msg.Controllers
{
    /// <summary>
    /// 訊息模板
    /// </summary>
    [Route("api/Msg/[controller]")]
    public class MessageTemplatesController : AreaApiController<MessageTemplates, IMessageTemplatesService>
    {
        public MessageTemplatesController(IMessageTemplatesService _iService) : base(_iService)
        {
        }

        /// <summary>
        /// 獲取微信小程式訂閱訊息列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetListByUseInWxApplet")]
        public IActionResult GetListByUseInWxApplet()
        {
            CommonResult result = new CommonResult();
            result = CheckToken();
            if (result.ErrCode == ErrCode.successCode)
            {
                string sqlwhere = "UseInWxApplet=1";
                List<MessageTemplates> tempList = iService.GetListWhere(sqlwhere).AsToList();
                List<MessageTemplatesOuputDto> list = tempList.MapTo<MessageTemplatesOuputDto>();
                result.ResData = list;
                result.ErrCode = ErrCode.successCode;
            }
            return ToJsonContent(result);
        }
    }
}
