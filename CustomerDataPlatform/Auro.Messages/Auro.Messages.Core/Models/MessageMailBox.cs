using System;
using System.ComponentModel.DataAnnotations.Schema;
using Auro.Commons.Models;

namespace Auro.Messages.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("Sys_MessageMailBox")]
    [Serializable]
    public class MessageMailBox:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        //public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否簡訊提醒
        /// </summary>
        public bool? IsMsgRemind { get; set; }

        /// <summary>
        /// 設定或獲取是否發送
        /// </summary>
        public bool? IsSend { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime SendDate { get; set; }

        /// <summary>
        /// 設定或獲取是否是強制訊息
        /// </summary>
        public bool? IsCompel { get; set; }

        /// <summary>
        /// 設定或獲取是否發送
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeleteUserId { get; set; }


    }
}
