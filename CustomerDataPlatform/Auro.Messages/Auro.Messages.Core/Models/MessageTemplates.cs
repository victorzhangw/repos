using System;
using System.ComponentModel.DataAnnotations.Schema;
using Auro.Commons.Models;

namespace Auro.Messages.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("Sys_MessageTemplates")]
    [Serializable]
    public class MessageTemplates:BaseEntity<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MessageType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool SendEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool SendSMS { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool SendInnerMessage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool SendWeixin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string WeixinTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TagDescription { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EmailSubject { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EmailBody { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InnerMessageSubject { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InnerMessageBody { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SMSBody { get; set; }

        /// <summary>
        /// 設定或獲取微信模板編號，如果為空則表示不支援微信訊息提醒
        /// </summary>
        public string WeiXinTemplateNo { get; set; }

        /// <summary>
        /// 設定或獲取微信模板中對應的名稱
        /// </summary>
        public string WeiXinName { get; set; }

        /// <summary>
        /// 設定或獲取是否用於微信小程式
        /// </summary>
        public bool UseInWxApplet { get; set; }

        /// <summary>
        /// 設定或獲取小程式模板ID
        /// </summary>
        public string WxAppletTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取小程式模板編號
        /// </summary>
        public string AppletTemplateNo { get; set; }

        /// <summary>
        /// 設定或獲取小程式模板名稱
        /// </summary>
        public string AppletTemplateName { get; set; }

        /// <summary>
        /// 設定或獲取訂閱訊息模板ID
        /// </summary>
        public string WxAppletSubscribeTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取模板編號
        /// </summary>
        public string WxAppletSubscribeTemplateNo { get; set; }

        /// <summary>
        /// 設定或獲取標題
        /// </summary>
        public string WxAppletSubscribeTemplateName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SMSTemplateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SMSTemplateContent { get; set; }

        /// <summary>
        /// 設定或獲取O2O小程式模板ID 
        /// </summary>
        public string WxO2OAppletTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取是否在O2O小程式中使用
        /// </summary>
        public bool? UseInO2OApplet { get; set; }


    }
}
