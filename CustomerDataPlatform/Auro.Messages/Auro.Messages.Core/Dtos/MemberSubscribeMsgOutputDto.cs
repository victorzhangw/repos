using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Auro.Messages.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class MemberSubscribeMsgOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取訂閱使用者
        /// </summary>
        [MaxLength(50)]
        public string SubscribeUserId { get; set; }

        /// <summary>
        /// 設定或獲取訂閱型別：SMS簡訊，WxApplet 微信小程式，InnerMessage站內訊息 ，Email郵件通知
        /// </summary>
        [MaxLength(50)]
        public string SubscribeType { get; set; }

        /// <summary>
        /// 設定或獲取訊息模板Id主鍵
        /// </summary>
        [MaxLength(200)]
        public string MessageTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(200)]
        public string SubscribeTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取訂閱狀態
        /// </summary>
        [MaxLength(50)]
        public string SubscribeStatus { get; set; }


    }
}
