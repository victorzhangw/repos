using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Commons.Models;
using Auro.Messages.Models;

namespace Auro.Messages.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(MessageMailBox))]
    [Serializable]
    public class MessageMailBoxInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否簡訊提醒
        /// </summary>
        public bool? IsMsgRemind { get; set; }

        /// <summary>
        /// 設定或獲取是否發送
        /// </summary>
        public bool? IsSend { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime SendDate { get; set; }

        /// <summary>
        /// 設定或獲取是否是強制訊息
        /// </summary>
        public bool? IsCompel { get; set; }


    }
}
