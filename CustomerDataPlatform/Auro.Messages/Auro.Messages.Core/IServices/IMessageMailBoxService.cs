using System;
using Auro.Commons.IServices;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.IServices
{
    /// <summary>
    /// 定義服務介面
    /// </summary>
    public interface IMessageMailBoxService:IService<MessageMailBox,MessageMailBoxOutputDto, string>
    {
    }
}
