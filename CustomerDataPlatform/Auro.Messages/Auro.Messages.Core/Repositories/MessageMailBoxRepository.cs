using System;

using Auro.Commons.Repositories;
using Auro.Messages.IRepositories;
using Auro.Messages.Models;

namespace Auro.Messages.Repositories
{
    /// <summary>
    /// 倉儲介面的實現
    /// </summary>
    public class MessageMailBoxRepository : BaseRepository<MessageMailBox, string>, IMessageMailBoxRepository
    {
		public MessageMailBoxRepository()
        {
            this.tableName = "Sys_MessageMailBox";
            this.primaryKey = "Id";
        }
    }
}