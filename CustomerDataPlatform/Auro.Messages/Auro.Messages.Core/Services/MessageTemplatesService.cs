using System;
using Auro.Commons.Services;
using Auro.Security.IServices;
using Auro.Messages.IRepositories;
using Auro.Messages.IServices;
using Auro.Messages.Dtos;
using Auro.Messages.Models;
using System.Collections.Generic;

namespace Auro.Messages.Services
{
    /// <summary>
    /// 服務介面實現
    /// </summary>
    public class MessageTemplatesService: BaseService<MessageTemplates,MessageTemplatesOutputDto, string>, IMessageTemplatesService
    {
		private readonly IMessageTemplatesRepository _repository;
        private readonly ILogService _logService;
        public MessageTemplatesService(IMessageTemplatesRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }

        /// <summary>
        /// 根據訊息型別查詢訊息模板
        /// </summary>
        /// <param name="messageType">訊息型別</param>
        /// <returns></returns>
        public MessageTemplates GetByMessageType(string messageType)
        {
            return _repository.GetWhere("messageType='" + messageType + "'");
        }

        /// <summary>
        /// 根據使用者查詢微信小程式訂閱訊息模板列表，關聯使用者訂閱表
        /// </summary>
        /// <param name="userId">使用者編號</param>
        /// <returns></returns>
        public List<MemberMessageTemplatesOuputDto> ListByUseInWxApplet(string userId)
        {
            return _repository.ListByUseInWxApplet(userId);
        }
    }
}