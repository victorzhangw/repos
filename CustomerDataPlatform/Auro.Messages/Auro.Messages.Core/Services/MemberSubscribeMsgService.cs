using System;
using Auro.Commons.Services;
using Auro.Security.IServices;
using Auro.Messages.IRepositories;
using Auro.Messages.IServices;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.Services
{
    /// <summary>
    /// 服務介面實現
    /// </summary>
    public class MemberSubscribeMsgService: BaseService<MemberSubscribeMsg,MemberSubscribeMsgOutputDto, string>, IMemberSubscribeMsgService
    {
		private readonly IMemberSubscribeMsgRepository _repository;
        private readonly ILogService _logService;
        public MemberSubscribeMsgService(IMemberSubscribeMsgRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }


        /// <summary>
        /// 根據訊息型別查詢訊息模板
        /// </summary>
        /// <param name="messageType">訊息型別</param>
        /// <param name="userId">使用者Id</param>
        /// <returns></returns>
        public MemberMessageTemplatesOuputDto GetByMessageTypeWithUser(string messageType, string userId)
        {
            return _repository.GetByMessageTypeWithUser(messageType, userId);
        }


        /// <summary>
        /// 按使用者、訂閱型別和訊息模板主鍵查詢
        /// </summary>
        /// <param name="subscribeType">訊息型別</param>
        /// <param name="userId">使用者</param>
        /// <param name="messageTemplateId">模板Id主鍵</param>
        /// <returns></returns>
        public MemberMessageTemplatesOuputDto GetByWithUser(string subscribeType, string userId, string messageTemplateId)
        {

            return _repository.GetByWithUser(subscribeType, userId, messageTemplateId);
        }


        /// <summary>
        /// 根據訊息模板Id（主鍵）查詢使用者訂閱訊息
        /// </summary>
        /// <param name="messageTemplateId">訊息模板主鍵</param>
        /// <param name="userId">使用者</param>
        /// <param name="subscribeType">訊息型別</param>
        /// <returns></returns>
        public MemberSubscribeMsg GetByMessageTemplateIdAndUser(string messageTemplateId, string userId, string subscribeType)
        {
            string sqlWhere = "MessageTemplateId='" + messageTemplateId + "' and SubscribeUserId='" + userId + "' and SubscribeType='" + subscribeType + "'";
            return _repository.GetWhere(sqlWhere);
        }
        /// <summary>
        /// 更新使用者訂閱訊息
        /// </summary>
        /// <param name="messageTemplateId">訊息模板主鍵</param>
        /// <param name="userId">使用者</param>
        /// <param name="subscribeType">訊息型別</param>
        /// <param name="subscribeStatus">訂閱狀態</param>
        /// <returns></returns>
        public bool UpdateByMessageTemplateIdAndUser(string messageTemplateId, string userId, string subscribeType, string subscribeStatus)
        {
            string sqlWhere = "MessageTemplateId='" + messageTemplateId + "' and SubscribeUserId='" + userId + "' and SubscribeType='" + subscribeType + "'";
            return _repository.UpdateTableField("SubscribeStatus", subscribeStatus, sqlWhere);
        }

        public long Insert(MemberSubscribeMsg info)
        {
            return _repository.Insert(info);
        }
        /// <summary>
        /// 更新訂閱狀態
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool UpdateSubscribeStatus(MemberSubscribeMsg info)
        {
            string sqlWhere = "MessageTemplateId='" + info.MessageTemplateId + "' and SubscribeUserId='" + info.SubscribeUserId + "' and SubscribeType='" + info.SubscribeType + "'";
            return _repository.UpdateTableField("SubscribeStatus", info.SubscribeStatus, sqlWhere);
        }
    }
}