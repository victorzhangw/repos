using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.Commons.Helpers;
using Auro.Commons.Log;
using Auro.Commons.Mapping;
using Auro.Commons.Models;
using Auro.Commons.Pages;
using Auro.WebAnalytics.GoogleAnalytics.Dtos;
using Auro.WebAnalytics.GoogleAnalytics.Models;
using Auro.WebAnalytics.GoogleAnalytics.IServices;
using Auro.AspNetCore.Mvc.Filter;
using Microsoft.AspNetCore.Authorization;

namespace Auro.GoogleAnalyticsApi.Areas.GoogleAnalytics.Controllers
{
    /// <summary>
    /// Google Analytics接口
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class GoogleAnalyticsController : AreaApiController<GAModel, GoogleAnalyticsOutputDto,GoogleAnalyticsInputDto,IGoogleAnalyticsService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public GoogleAnalyticsController(IGoogleAnalyticsService _iService) : base(_iService)
        {
            iService = _iService;
           
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(GAModel info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(GAModel info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(GAModel info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        ///  讀取 日期區間 GA 報表
        /// </summary>
        /// <param name="ginfo"></param>
        /// <returns></returns>
        [HttpPost("GAReports")]
        //[AuroAuthorize("Add")]
        [NoPermissionRequired]
        [AllowAnonymous]
        public async Task<IActionResult> RetrieveGoogleAnalyticsResult(GoogleAnalyticsInputDto ginfo)
        {
            CommonResult result = new CommonResult();

            //ginfo.Id= GuidUtils.CreateNo();
            ginfo.CreatorTime = DateTime.Now;
            ginfo.CreatorUserId = "Admin";
            ginfo.DeleteMark = false;
            string OpenIdType = "Google";
            string OpenName = "Analytics";
            ResponseReport records = await iService.GetDateRangeGAData(OpenIdType, OpenName, ginfo);


            if (records != null)
            {
                result.Success = true;
            }

            if (result.Success)
            {
                result.ResData = records;
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 每日存儲 GA 報表
        /// </summary>
        /// <param name="ginfo"></param>
        /// <returns></returns>
        [HttpPost("AddDeaultDailyReports")]
        //[AuroAuthorize("Add")]
        [NoPermissionRequired]
        [AllowAnonymous]
        public  async Task<IActionResult> StoreGoogleAnalyticsResult(GoogleAnalyticsInputDto ginfo)
        {
            CommonResult result = new CommonResult();

            //ginfo.Id= GuidUtils.CreateNo();
            ginfo.CreatorTime = DateTime.Now;
            ginfo.CreatorUserId = "Admin";
            ginfo.DeleteMark = false;
            string OpenIdType = "Google";
            string OpenName = "Analytics";
            int records = await iService.StoreGoogleAnalyticsResult(OpenIdType,OpenName,ginfo);


            if (records > 0)
            {
                result.Success = true;
            }

            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        
    }
}