using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.Commons.Helpers;
using Auro.Commons.Log;
using Auro.Commons.Mapping;
using Auro.Commons.Models;
using Auro.Commons.Pages;
using Auro.WebAnalytics.Tealeaf.Dtos;
using Auro.WebAnalytics.Tealeaf.Models;
using Auro.WebAnalytics.Tealeaf.IServices;

namespace Auro.TealeafApi.Areas.Tealeaf.Controllers
{
    /// <summary>
    /// Tealeaf Events接口
    /// </summary>
    [ApiController]
    [Route("api/Tealeaf/[controller]")]
    public class TealeafController : AreaApiController<TealeafModel, TealeafOutputDto,TealeafInputDto,ITealeafService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public TealeafController(ITealeafService _iService) : base(_iService)
        {
            iService = _iService;
           
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(TealeafModel info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(TealeafModel info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(TealeafModel info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
    }
}