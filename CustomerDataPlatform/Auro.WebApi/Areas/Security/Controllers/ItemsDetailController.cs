using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.AspNetCore.Mvc;
using Auro.Commons.Helpers;
using Auro.Commons.Log;
using Auro.Commons.Models;
using Auro.Security.Dtos;
using Auro.Security.IServices;
using Auro.Security.Models;

namespace Auro.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 資料字典明細
    /// </summary>
    [Route("api/Security/[controller]")]
    [ApiController]
    public class ItemsDetailController : AreaApiController<ItemsDetail, ItemsDetailOutputDto, ItemsDetailInputDto, IItemsDetailService, string>
    {
        private readonly IItemsService itemsService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="itemsService"></param>
        public ItemsDetailController(IItemsDetailService _iService, IItemsService itemsService) : base(_iService)
        {
            iService = _iService;
            this.itemsService = itemsService;
        }

        /// <summary>
        /// 在新增資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(ItemsDetail info)
        {
            //留給子類對參數物件進行修改
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
            bool bltree = itemsService.Get(info.ItemId).IsTree;
            if (bltree)
            {
                if (string.IsNullOrEmpty(info.ParentId))
                {
                    info.Layers = 1;
                    info.ParentId = "";
                }
                else
                {
                    info.Layers = iService.Get(info.ParentId).Layers + 1;
                }
            }
            else
            {
                info.ParentId = "";
            }

        }

        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(ItemsDetail info)
        {
            //留給子類對參數物件進行修改 
            info.LastModifyTime = DateTime.Now;
            info.LastModifyUserId = CurrentUser.UserId;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
            bool bltree = itemsService.Get(info.ItemId).IsTree;
            if (bltree)
            {
                if (string.IsNullOrEmpty(info.ParentId))
                {
                    info.Layers = 1;
                    info.ParentId = "";
                }
                else
                {
                    info.Layers = iService.Get(info.ParentId).Layers + 1;
                }
            }
            else
            {
                info.ParentId = "";
            }
        }


        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AuroAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(ItemsDetailInputDto tinfo)
        {
            CommonResult result = new CommonResult();

            ItemsDetail info = iService.Get(tinfo.Id);
            info.ItemName = tinfo.ItemName;
            info.ItemCode = tinfo.ItemCode;
            info.ItemId = tinfo.ItemId;
            info.IsDefault = tinfo.IsDefault;
            info.ParentId = tinfo.ParentId;
            info.EnabledMark = tinfo.EnabledMark;
            info.SortCode = tinfo.SortCode;
            info.Description = tinfo.Description;


            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, tinfo.Id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 獲取某資料字典類別下的資料適用於Vue 樹形列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllItemsDetailTreeTable")]
        [AuroAuthorize("List")]
        public async Task<IActionResult> GetAllItemsDetailTreeTable(string itemId)
        {
            CommonResult result = new CommonResult();
            try
            {
                if (!string.IsNullOrEmpty(itemId))
                {
                    List<ItemsDetailOutputDto> list = await iService.GetAllItemsDetailTreeTable(itemId);
                    result.Success = true;
                    result.ErrCode = ErrCode.successCode;
                    result.ResData = list;
                }
                else
                {
                    result.ErrMsg = "資料字典類別ID不能為空";
                }
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取資料字典異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }
    }
}