using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.AspNetCore.Mvc;
using Auro.Commons.Helpers;
using Auro.Commons.Models;
using Auro.Security.Dtos;
using Auro.Security.IServices;
using Auro.Security.Models;

namespace Auro.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// IP地址禁用/可用介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class FilterIPController : AreaApiController<FilterIP, FilterIPOutputDto, FilterIPInputDto, IFilterIPService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public FilterIPController(IFilterIPService _iService) : base(_iService)
        {
            iService = _iService;
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(FilterIP info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(FilterIP info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(FilterIP info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }

        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AuroAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(FilterIPInputDto tinfo)
        {
            CommonResult result = new CommonResult();

            FilterIP info = iService.Get(tinfo.Id);
            info.FilterType = tinfo.FilterType;
            info.EndIP = tinfo.EndIP;
            info.StartIP = tinfo.StartIP;
            info.SortCode = tinfo.SortCode;
            info.EnabledMark = tinfo.EnabledMark;
            info.Description = tinfo.Description;

            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, tinfo.Id).ConfigureAwait(true);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }

    }
}