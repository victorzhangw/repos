using System;
using Auro.Commons.IRepositories;
using Auro.Quartz.Models;
using Auro.Security.Models;

namespace Auro.Quartz.IRepositories
{
    /// <summary>
    /// 定義定時任務執行日誌倉儲介面
    /// </summary>
    public interface ITaskJobsLogRepository:IRepository<TaskJobsLog, string>
    {
    }
}