using System;
using Auro.Commons.IRepositories;
using Auro.Quartz.Models;
using Auro.Security.Models;

namespace Auro.Quartz.IRepositories
{
    /// <summary>
    /// 定義定時任務倉儲介面
    /// </summary>
    public interface ITaskManagerRepository:IRepository<TaskManager, string>
    {
    }
}