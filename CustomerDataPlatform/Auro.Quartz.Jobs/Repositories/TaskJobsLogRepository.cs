using System;
using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Quartz.IRepositories;
using Auro.Quartz.Models;
using Auro.Security.IRepositories;
using Auro.Security.Models;

namespace Auro.Quartz.Repositories
{
    /// <summary>
    /// 定時任務執行日誌倉儲介面的實現
    /// </summary>
    public class TaskJobsLogRepository : BaseRepository<TaskJobsLog, string>, ITaskJobsLogRepository
    {
		public TaskJobsLogRepository()
        {
        }

        public TaskJobsLogRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}