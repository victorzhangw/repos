using System;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 定義序號編碼規則表服務介面
    /// </summary>
    public interface ISequenceRuleService:IService<SequenceRule,SequenceRuleOutputDto, string>
    {
    }
}
