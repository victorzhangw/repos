using System;
using System.Collections.Generic;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRoleService:IService<Role, RoleOutputDto, string>
    {
        /// <summary>
        /// 根據角色編碼獲取角色
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        Role GetRole(string enCode);


        /// <summary>
        /// 根據使用者角色ID獲取角色編碼
        /// </summary>
        /// <param name="ids">角色ID字串，用「,」分格</param>
        /// <returns></returns>
        string GetRoleEnCode(string ids);


        /// <summary>
        /// 根據使用者角色ID獲取角色編碼
        /// </summary>
        /// <param name="ids">角色ID字串，用「,」分格</param>
        /// <returns></returns>
       string GetRoleNameStr(string ids);
    }
}
