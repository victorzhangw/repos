using System;
using System.Threading.Tasks;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    public interface IUserLogOnService:IService<UserLogOn, UserLogOnOutputDto, string>
    {

        /// <summary>
        /// 根據會員ID獲取使用者登錄資訊實體
        /// </summary>
        /// <param name="userId">使用者Id</param>
        /// <returns></returns>
        UserLogOn GetByUserId(string userId);

        /// <summary>
        /// 根據會員ID獲取使用者登錄資訊實體
        /// </summary>
        /// <param name="info">主題配置資訊</param>
        /// <param name="userId">使用者Id</param>
        /// <returns></returns>
        Task<bool> SaveUserTheme(UserThemeInputDto info, string userId);
    }
}
