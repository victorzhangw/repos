using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    public interface IUploadFileService : IService<UploadFile, UploadFileOutputDto, string>
    {
        /// <summary>
        /// 根據應用Id和應用標識批量更新資料
        /// </summary>
        /// <param name="beLongAppId">應用Id</param>
        /// <param name="oldBeLongAppId">更新前舊的應用Id</param>
        /// <param name="belongApp">應用標識</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        bool UpdateByBeLongAppId(string beLongAppId,string oldBeLongAppId, string belongApp = null, IDbTransaction trans = null);
    }
}
