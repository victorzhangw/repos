using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Auro.Commons.Core.Dtos;
using Auro.Commons.IServices;
using Auro.Commons.Models;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMenuService:IService<Menu, MenuOutputDto, string>
    {

        /// <summary>
        /// 根據使用者獲取功能菜單
        /// </summary>
        /// <param name="userId">使用者ID</param>
        /// <returns></returns>
        List<Menu> GetMenuByUser(string userId);

        /// <summary>
        /// 獲取功能菜單適用於Vue 樹形列表
        /// </summary>
        /// <param name="systemTypeId">子系統Id</param>
        /// <returns></returns>
        Task<List<MenuTreeTableOutputDto>> GetAllMenuTreeTable(string systemTypeId);


        /// <summary>
        /// 根據角色ID字串（逗號分開)和系統型別ID，獲取對應的操作功能列表
        /// </summary>
        /// <param name="roleIds">角色ID</param>
        /// <param name="typeID">系統型別ID</param>
        /// <param name="isMenu">是否是菜單</param>
        /// <returns></returns>
        List<Menu> GetFunctions(string roleIds, string typeID,bool isMenu=false);

        /// <summary>
        /// 根據系統型別ID，獲取對應的操作功能列表
        /// </summary>
        /// <param name="typeID">系統型別ID</param>
        /// <returns></returns>
        List<Menu> GetFunctions(string typeID);

        /// <summary>
        /// 根據父級功能編碼查詢所有子集功能，主要用於頁面操作按鈕許可權
        /// </summary>
        /// <param name="enCode">菜單功能編碼</param>
        /// <returns></returns>
        Task<IEnumerable<MenuOutputDto>> GetListByParentEnCode(string enCode);

        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="ids">主鍵Id集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        CommonResult DeleteBatchWhere(DeletesInputDto ids, IDbTransaction trans = null);
        /// <summary>
        /// 非同步按條件批量刪除
        /// </summary>
        /// <param name="ids">主鍵Id集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<CommonResult> DeleteBatchWhereAsync(DeletesInputDto ids, IDbTransaction trans = null);
    }
}
