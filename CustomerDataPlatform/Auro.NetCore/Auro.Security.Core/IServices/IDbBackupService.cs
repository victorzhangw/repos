using System;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    public interface IDbBackupService:IService<DbBackup, DbBackupOutputDto, string>
    {
    }
}
