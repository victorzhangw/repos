using System;
using System.Threading.Tasks;
using Auro.Commons.IServices;
using Auro.Commons.Models;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 定義單據編碼服務介面
    /// </summary>
    public interface ISequenceService:IService<Sequence,SequenceOutputDto, string>
    {
        /// <summary>
        /// 獲取最新業務單據編碼
        /// </summary>
        /// <param name="sequenceName">業務單據編碼名稱</param>
        /// <returns></returns>
        Task<CommonResult> GetSequenceNextTask(string sequenceName);
        /// <summary>
        /// 獲取最新業務單據編碼
        /// </summary>
        /// <param name="sequenceName">業務單據編碼名稱</param>
        /// <returns></returns>
       CommonResult GetSequenceNext(string sequenceName);
    }
}
