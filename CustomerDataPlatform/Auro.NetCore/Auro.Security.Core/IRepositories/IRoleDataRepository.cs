using System;
using System.Collections.Generic;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface IRoleDataRepository:IRepository<RoleData, string>
    {
        /// <summary>
        /// 根據角色返回授權訪問部門資料
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
       List<string> GetListDeptByRole(string roleIds);
    }
}