using System;
using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Security.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.Repositories
{
    /// <summary>
    /// 序號編碼規則表倉儲介面的實現
    /// </summary>
    public class SequenceRuleRepository : BaseRepository<SequenceRule, string>, ISequenceRuleRepository
    {
		public SequenceRuleRepository()
        {
        }

        public SequenceRuleRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}