using Dapper;
using System;
using System.Data;
using Auro.Commons.IDbContext;
using Auro.Commons.Options;
using Auro.Commons.Repositories;
using Auro.Security.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.Repositories
{
    public class UserLogOnRepository : BaseRepository<UserLogOn, string>, IUserLogOnRepository
    {
        public UserLogOnRepository()
        {
        }

        public UserLogOnRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }



        /// <summary>
        /// 根據會員ID獲取使用者登錄資訊實體
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserLogOn GetByUserId(string userId)
        {
            string sql = @"SELECT * FROM Sys_UserLogOn t WHERE t.UserId = @UserId";
            return DapperConn.QueryFirst<UserLogOn>(sql, new { UserId = userId });
        }
    }
}