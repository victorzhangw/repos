using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Security.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.Repositories
{
    public class ItemsDetailRepository : BaseRepository<ItemsDetail, string>, IItemsDetailRepository
    {
        public ItemsDetailRepository()
        {
        }

        public ItemsDetailRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }

    }
}