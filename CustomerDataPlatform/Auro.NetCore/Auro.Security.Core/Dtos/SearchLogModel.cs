using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auro.Commons.Dtos;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 日誌搜索條件
    /// </summary>
    public class SearchLogModel : SearchInputDto<Log>
    {
        /// <summary>
        /// 新增開始時間 
        /// </summary>
        public string CreatorTime1
        {
            get; set;
        }
        /// <summary>
        /// 新增結束時間 
        /// </summary>
        public string CreatorTime2
        {
            get; set;
        }
    }
}
