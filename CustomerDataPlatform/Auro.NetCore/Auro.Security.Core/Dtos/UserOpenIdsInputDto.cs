using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Models;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(UserOpenIds))]
    [Serializable]
    public class UserOpenIdsInputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OpenIdType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OpenId { get; set; }


    }
}
