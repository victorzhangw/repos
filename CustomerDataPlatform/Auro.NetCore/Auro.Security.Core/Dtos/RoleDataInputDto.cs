using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Commons.Models;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(RoleData))]
    [Serializable]
    public class RoleDataInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 型別，company-公司，dept-部門，person-個人
        /// </summary>
        public virtual string DType { get; set; }

        /// <summary>
        /// 資料資料，部門ID或個人ID
        /// </summary>
        public virtual string AuthorizeData { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Note { get; set; }


    }
}
