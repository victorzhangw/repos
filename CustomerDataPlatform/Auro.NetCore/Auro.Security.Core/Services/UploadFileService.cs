using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Auro.Commons.Dtos;
using Auro.Commons.Mapping;
using Auro.Commons.Pages;
using Auro.Commons.Services;
using Auro.Security.Dtos;
using Auro.Security.IRepositories;
using Auro.Security.IServices;
using Auro.Security.Models;

namespace Auro.Security.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class UploadFileService : BaseService<UploadFile, UploadFileOutputDto, string>, IUploadFileService
    {
        private readonly IUploadFileRepository _uploadFileRepository;
        private readonly ILogService _logService;
        public UploadFileService(IUploadFileRepository repository, ILogService logService) : base(repository)
        {
            _uploadFileRepository = repository;
            _logService = logService;
        }

        /// <summary>
        /// 根據應用Id和應用標識批量更新資料
        /// </summary>
        /// <param name="beLongAppId">應用Id</param>
        /// <param name="oldBeLongAppId">更新前舊的應用Id</param>
        /// <param name="belongApp">應用標識</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public bool UpdateByBeLongAppId(string beLongAppId, string oldBeLongAppId,string belongApp = null, IDbTransaction trans = null)
        {
           return _uploadFileRepository.UpdateByBeLongAppId(beLongAppId, oldBeLongAppId, belongApp,trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定對象的集合</returns>
        public override async Task<PageResult<UploadFileOutputDto>> FindWithPagerAsync(SearchInputDto<UploadFile> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege(false);
            if (!string.IsNullOrEmpty(search.Keywords))
            {
                where += string.Format(" and  FileName like '%{0}%' ", search.Keywords);
            };
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<UploadFile> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
            PageResult<UploadFileOutputDto> pageResult = new PageResult<UploadFileOutputDto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<UploadFileOutputDto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }
    }
}
