using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auro.AspNetCore.Mvc
{
    /// <summary>
    /// 功能許可權屬性配置
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AuroAuthorizeAttribute: ActionFilterAttribute
    {
        /// <summary>
        /// 功能許可權
        /// </summary>
        public string Function { get; set; }

        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="function">功能程式碼</param>
        public AuroAuthorizeAttribute(string function)
        {
            Function = function;
        }
    }
}
