﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auro.AspNetCore.Common
{
    /// <summary>
    /// 租戶外部資料源設定
    /// </summary>
    public static class ExternalDataSource
    {
        /// <summary>
        /// Universal Google Analytics
        /// </summary>
        public static string UniversalGoogleAnalytics = "UniGA";
        /// <summary>
        ///  Google Analytics V4
        /// </summary>
        public static string GoogleAnalyticsV4 = "GAV4";
        /// <summary>
        ///   Facebook 洞察報告
        /// </summary>
        public static string FBInsight = "FBInsight";
        /// <summary>
        ///  Facebook 轉換 API
        /// </summary>
        public static string FBConversion = "FBConversion";
        /// <summary>
        ///  MultiForce
        /// </summary>
        public static string MultiForce = "MF";
        /// <summary>
        ///  Acoustic Tealeaf
        /// </summary>
        public static string Tealeaf = "Tealeaf";
        /// <summary>
        ///  Google 廣告管理員
        /// </summary>
        public static string GoogleMCCADS = "GoogleMccAdm";
    }
}
