using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Auro.Security.Models;

namespace Auro.WebApi.Common
{ 
    /// <summary>
    /// SessionObject是登錄之後，給客戶端傳回的對象
    /// </summary>
    public class SessionObject
    {
        /// <summary>
        /// SessionKey
        /// </summary>
        public string SessionKey { get; set; }
        /// <summary>
        /// 目前登錄的使用者的資訊
        /// </summary>
        public User LogonUser { get; set; }
    }
}
