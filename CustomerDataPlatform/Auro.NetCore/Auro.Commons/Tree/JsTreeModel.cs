using System;
using System.Collections.Generic;
using System.Text;

namespace Auro.Commons.Tree
{
    /// <summary>
    /// JsTree 資料模型實體
    /// {
    ///     id          : "string" // required
    ///     parent      : "string" // required
    ///     text        : "string" // node text
    ///     icon        : "string" // string for custom
    ///     state       : {
    ///         opened    : boolean  // is the node open
    ///         disabled  : boolean  // is the node disabled
    ///         selected  : boolean  // is the node selected
    ///     },
    ///     li_attr     : {}  // attributes for the generated LI node
    ///     a_attr      : {}  // attributes for the generated A node
    /// }
    /// </summary>
    public class JsTreeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string parent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string text { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public JsTreeStateModel state { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<JsTreeModel> children { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class JsTreeStateModel
    {
        /// <summary>
        /// 
        /// </summary>
        public bool opened { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool disabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool selected { get; set; }
    }
    }
