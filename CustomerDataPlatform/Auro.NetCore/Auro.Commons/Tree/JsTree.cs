using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Json;

namespace Auro.Commons.Tree
{
    /// <summary>
    /// 
    /// </summary>
    public static class JsTree
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<JsTreeModel> JsTreeJson(this List<JsTreeModel> data)
        {
            return JsTreeJson(data, "", "").ToList<JsTreeModel>();
        }
        private static string JsTreeJson(List<JsTreeModel> data, string parentId, string blank)
        {
            List<JsTreeModel> list = new List<JsTreeModel>();
            JsTreeModel jsTreeModel = new JsTreeModel();
            var ChildNodeList = data.FindAll(t => t.parent == parentId);
            var tabline = "";
            if (!string.IsNullOrEmpty(parentId))
            {
                tabline = "";
            }
            if (ChildNodeList.Count > 0)
            {
                tabline = tabline + blank;
            }
            foreach (JsTreeModel entity in ChildNodeList)
            {
                jsTreeModel = entity;
                jsTreeModel.children= JsTreeJson(data, entity.id, tabline).ToList<JsTreeModel>();
                list.Add(jsTreeModel);
                
            }
            return list.ToJson().ToString();
        }
    }
}
