/*******************************************************************************
 * Copyright © 2017-2020 Auro.Framework 版權所有
 * Author: Auro
 * Description: Auro快速開發平臺
 * Website：http://www.Auro.com
*********************************************************************************/
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Auro.Commons.Extensions
{
    /// <summary>
    /// Session 擴充套件函式,直接將實體類序列化成json儲存和讀取
    /// </summary>
    public static class SessionExtensions
    {
        /// <summary>
        /// 設定session值
        /// </summary>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Set(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        /// <summary>
        /// 獲取session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
