/*******************************************************************************
 * Copyright © 2017-2020 Auro.Framework 版權所有
 * Author: Auro
 * Description: Auro快速開發平臺
 * Website：http://www.Auro.com
*********************************************************************************/
using System;
using System.Security.Cryptography;
using System.Text;
namespace Auro.Commons.Encrypt
{
    /// <summary>
    /// Encrypt 的加密/解密。
    /// </summary>
    public class DEncrypt
    {
        /// <summary>
        /// 建構方法
        /// </summary>
        public DEncrypt()
        {
        }

        #region 使用 預設金鑰字串 加密/解密string

        /// <summary>
        /// 使用預設金鑰字串加密string
        /// </summary>
        /// <param name="original">明文</param>
        /// <returns>密文</returns>
        public static string Encrypt(string original)
        {
            return Encrypt(original, "AuroSOFT");
        }
        /// <summary>
        /// 使用預設金鑰字串解密string
        /// </summary>
        /// <param name="original">密文</param>
        /// <returns>明文</returns>
        public static string Decrypt(string original)
        {
            return Decrypt(original, "AuroSOFT", System.Text.Encoding.Default);
        }

        #endregion

        #region 使用 給定金鑰字串 加密/解密string
        /// <summary>
        /// 使用給定金鑰字串加密string
        /// </summary>
        /// <param name="original">原始文字</param>
        /// <param name="key">金鑰</param>
        /// <returns>密文</returns>
        public static string Encrypt(string original, string key)
        {
            byte[] buff = System.Text.Encoding.Default.GetBytes(original);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return Convert.ToBase64String(Encrypt(buff, kb));
        }
        /// <summary>
        /// 使用給定金鑰字串解密string
        /// </summary>
        /// <param name="original">密文</param>
        /// <param name="key">金鑰</param>
        /// <returns>明文</returns>
        public static string Decrypt(string original, string key)
        {
            return Decrypt(original, key, System.Text.Encoding.Default);
        }

        /// <summary>
        /// 使用給定金鑰字串解密string,返回指定編碼方式明文
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">金鑰</param>
        /// <param name="encoding">字元編碼方案</param>
        /// <returns>明文</returns>
        public static string Decrypt(string encrypted, string key, Encoding encoding)
        {
            byte[] buff = Convert.FromBase64String(encrypted);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return encoding.GetString(Decrypt(buff, kb));
        }
        #endregion

        #region 使用 預設金鑰字串 加密/解密/byte[]
        /// <summary>
        /// 使用預設金鑰字串解密byte[]
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">金鑰</param>
        /// <returns>明文</returns>
        public static byte[] Decrypt(byte[] encrypted)
        {
            byte[] key = System.Text.Encoding.Default.GetBytes("AuroSOFT");
            return Decrypt(encrypted, key);
        }
        /// <summary>
        /// 使用預設金鑰字串加密
        /// </summary>
        /// <param name="original">原始資料</param>
       
        /// <returns>密文</returns>
        public static byte[] Encrypt(byte[] original)
        {
            byte[] key = System.Text.Encoding.Default.GetBytes("AuroSOFT");
            return Encrypt(original, key);
        }
        #endregion

        #region  使用 給定金鑰 加密/解密/byte[]

        /// <summary>
        /// 產生MD5摘要
        /// </summary>
        /// <param name="original">資料源</param>
        /// <returns>摘要</returns>
        public static byte[] MakeMD5(byte[] original)
        {
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            byte[] keyhash = hashmd5.ComputeHash(original);
            hashmd5 = null;
            return keyhash;
        }


        /// <summary>
        /// 使用給定金鑰加密
        /// </summary>
        /// <param name="original">明文</param>
        /// <param name="key">金鑰</param>
        /// <returns>密文</returns>
        public static byte[] Encrypt(byte[] original, byte[] key)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = MakeMD5(key);
            des.Mode = CipherMode.ECB;

            return des.CreateEncryptor().TransformFinalBlock(original, 0, original.Length);
        }

        /// <summary>
        /// 使用給定金鑰解密資料
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">金鑰</param>
        /// <returns>明文</returns>
        public static byte[] Decrypt(byte[] encrypted, byte[] key)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = MakeMD5(key);
            des.Mode = CipherMode.ECB;

            return des.CreateDecryptor().TransformFinalBlock(encrypted, 0, encrypted.Length);
        }

        #endregion




    }
}
