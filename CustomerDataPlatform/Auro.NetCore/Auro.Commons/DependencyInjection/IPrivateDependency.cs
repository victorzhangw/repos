using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.Commons.DependencyInjection
{
    /// <summary>
    /// 依賴空介面（禁止外部繼承）
    /// </summary>
    public interface IPrivateDependency
    {
    }
}
