using System;
using System.Collections.Generic;
using System.Text;

namespace Auro.Commons.DependencyInjection
{

    /// <summary>
    /// 暫時服務註冊依賴
    /// </summary>
    public interface ITransientDependency:IPrivateDependency
    {
    }
}
