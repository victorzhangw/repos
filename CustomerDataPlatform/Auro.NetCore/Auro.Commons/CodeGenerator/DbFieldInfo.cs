using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Auro.Commons.CodeGenerator
{
    /// <summary>
    /// 表的欄位
    /// </summary>
    public class DbFieldInfo
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public DbFieldInfo()
        {
            FieldName = string.Empty;
            Description = string.Empty;
        }
        /// <summary>
        /// 欄位名稱
        /// </summary>
        public string FieldName { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 系統資料型別，如 int
        /// </summary>
        public string DataType
        {
            get;
            set;
        }

        /// <summary>
        /// 資料庫裡面存放的型別。
        /// </summary>
        public string FieldType { get; set; }

        /// <summary>
        /// 代表小數位精度。
        /// </summary>
        public long? FieldScale { get; set; }
        /// <summary>
        /// 資料精度，僅數字型別有效，總共多少位數字（10進位制）。
        /// 在MySql裡面代表了欄位長度
        /// </summary>
        public long? FieldPrecision { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? FieldMaxLength { get; set; }

        /// <summary>
        /// 可空
        /// </summary>
        public bool IsNullable { get; set; }
        /// <summary>
        /// 是否為主鍵欄位
        /// </summary>
        public bool IsIdentity { get; set; }
        /// <summary>
        /// 【未用上】該欄位是否自增
        /// </summary>
        public bool Increment { get; set; }


        /// <summary>
        /// 預設值
        /// </summary>
        public string FieldDefaultValue { get; set; }
       
    }
}
