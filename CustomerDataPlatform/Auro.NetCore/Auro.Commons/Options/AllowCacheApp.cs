using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.Commons.Options
{
    /// <summary>
    /// 快取中可用的應用
    /// </summary>
    [Serializable]
    public class AllowCacheApp
    {
        /// <summary>
        /// 設定或獲取 ID
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 應用Id
        /// </summary>
        [MaxLength(50)]
        public string AppId { get; set; }

        /// <summary>
        /// 設定或獲取 應用金鑰
        /// </summary>
        [MaxLength(50)]
        public string AppSecret { get; set; }

        /// <summary>
        /// 設定或獲取 訊息加密金鑰
        /// </summary>
        [MaxLength(256)]
        public string EncodingAESKey { get; set; }

        /// <summary>
        /// 設定或獲取 請求url
        /// </summary>
        [MaxLength(256)]
        public string RequestUrl { get; set; }

        /// <summary>
        /// 設定或獲取 token
        /// </summary>
        [MaxLength(256)]
        public string Token { get; set; }

        /// <summary>
        /// 設定或獲取  是否開啟訊息加解密
        /// </summary>
        public bool? IsOpenAEKey { get; set; }

    }
}
