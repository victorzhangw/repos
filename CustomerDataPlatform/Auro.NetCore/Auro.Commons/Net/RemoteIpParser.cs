using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace Auro.Commons.Net
{
    /// <summary>
    /// 
    /// </summary>
    public class RemoteIpParser : IIpAddressParser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRule"></param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        public bool ContainsIp(string ipRule, string clientIp)
        {
            return IpAddressUtil.ContainsIp(ipRule, clientIp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRules"></param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        public bool ContainsIp(List<string> ipRules, string clientIp)
        {
            return IpAddressUtil.ContainsIp(ipRules, clientIp);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRules"></param>
        /// <param name="clientIp"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        public bool ContainsIp(List<string> ipRules, string clientIp, out string rule)
        {
            return IpAddressUtil.ContainsIp(ipRules, clientIp, out rule);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public virtual IPAddress GetClientIp(HttpContext context)
        {
            return context.Connection.RemoteIpAddress;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public IPAddress ParseIp(string ipAddress)
        {
            return IpAddressUtil.ParseIp(ipAddress);
        }
    }
}
