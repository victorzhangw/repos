using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace Auro.Commons.Net
{
    /// <summary>
    /// 
    /// </summary>
    public class ReversProxyIpParser : RemoteIpParser
    {
        private readonly string _realIpHeader;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="realIpHeader"></param>
        public ReversProxyIpParser(string realIpHeader)
        {
            _realIpHeader = realIpHeader;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override IPAddress GetClientIp(HttpContext context)
        {
            if (context.Request.Headers.Keys.Contains(_realIpHeader, StringComparer.CurrentCultureIgnoreCase))
            {
                return ParseIp(context.Request.Headers[_realIpHeader].First());
            }

            return base.GetClientIp(context);
        }
    }
}
