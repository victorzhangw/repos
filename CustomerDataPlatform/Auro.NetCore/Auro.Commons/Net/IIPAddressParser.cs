using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace Auro.Commons.Net
{
    /// <summary>
    /// 
    /// </summary>
    public interface IIpAddressParser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRule"></param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        bool ContainsIp(string ipRule, string clientIp);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRules"></param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        bool ContainsIp(List<string> ipRules, string clientIp);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipRules"></param>
        /// <param name="clientIp"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        bool ContainsIp(List<string> ipRules, string clientIp, out string rule);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        
        IPAddress GetClientIp(HttpContext context);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        IPAddress ParseIp(string ipAddress);
    }
}
