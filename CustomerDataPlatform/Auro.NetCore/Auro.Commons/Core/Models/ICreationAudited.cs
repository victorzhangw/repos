using System;

namespace Auro.Commons.Models
{
    /// <summary>
    /// 定義建立審計資訊：給實體新增建立時的 建立人CreatorUserId，建立時間CreatorTime 的審計資訊，這些值將在資料層執行 建立Insert 時自動賦值。
    /// </summary>
    public interface ICreationAudited
    {

        /// <summary>
        /// 獲取或設定 建立日期
        /// </summary>
        DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 獲取或設定 建立使用者主鍵
        /// </summary>
        string CreatorUserId { get; set; }
    }
}