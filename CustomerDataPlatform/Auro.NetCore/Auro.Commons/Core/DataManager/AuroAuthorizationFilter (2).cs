using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auro.Commons.Enums;

namespace Auro.Commons.Core.DataManager
{
    public class AuroDbOptions
    {
        public AuroDbOptions()
        {
        }

        /// <summary>
        /// 預設資料庫型別
        /// </summary>
        public DatabaseType DefaultDatabaseType { get; set; } = DatabaseType.SqlServer;

        /// <summary>
        /// 資料庫連線配置
        /// </summary>
        public IDictionary<string, DbConnectionOptions> DbConnections { get; set; }
    }
}
