using System;
using System.Collections.Generic;
using System.Text;

namespace Auro.Commons.Core.DataManager
{
    /// <summary>
    /// 資料庫連線配置特性
    /// </summary>
    public class AppDBContextAttribute : Attribute
    {
        /// <summary>
        /// 資料庫配置名稱
        /// </summary>
        public string DbConfigName { get; set; }
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="dbConfigName"></param>
        public AppDBContextAttribute(string dbConfigName)
        {
            DbConfigName = dbConfigName;
        }
    }
}
