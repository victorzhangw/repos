using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auro.Commons.Extensions;
//通過HostingStartup指定要啟動的型別
[assembly: HostingStartup(typeof(Auro.Commons.Core.App.HostingStartup))]
namespace Auro.Commons.Core.App
{
    /// <summary>
    /// 配置程式啟動時自動注入
    /// </summary>
    public sealed class HostingStartup : IHostingStartup
    {
        /// <summary>
        /// 配置應用啟動
        /// </summary>
        /// <param name="builder"></param>
        public void Configure(IWebHostBuilder builder)
        {
            //可以新增配置
            builder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                // 自動裝載配置
                App.AddConfigureFiles(config, hostingContext.HostingEnvironment);
            });

            //可以新增ConfigureServices
            // 自動注入 AddApp() 服務
            builder.ConfigureServices(services =>
            {
                
            });
        }
    }
}
