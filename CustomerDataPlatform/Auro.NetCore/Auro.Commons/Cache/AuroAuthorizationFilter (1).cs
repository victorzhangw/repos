using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Core.App;
using Auro.Commons.IoC;
using Auro.Commons.Json;

namespace Auro.Commons.Cache
{
    /// <summary>
    /// 快取操作幫助類
    /// </summary>
    public class AuroCacheHelper
    {
        /// <summary>
        /// 快取提供模式
        /// </summary>
        private static CacheProvider cacheProvider;
        /// <summary>
        /// 快取介面
        /// </summary>
        private ICacheService cacheservice;
        /// <summary>
        /// 
        /// </summary>
        public AuroCacheHelper()
        {

            cacheProvider = App.GetService<CacheProvider>();
            if (cacheProvider == null)
            {
                throw new ArgumentNullException(nameof(cacheProvider));
            }
            else
            {
                cacheservice= App.GetService<ICacheService>();
            }
        }

        /// <summary>
        /// 使用MemoryCache快取操作
        /// </summary>
        /// <param name="isMemoryCache">是否使用MemoryCache</param>
        public AuroCacheHelper(bool isMemoryCache=false)
        {

            cacheProvider = App.GetService<CacheProvider>();
            if (cacheProvider == null)
            {
                throw new ArgumentNullException(nameof(cacheProvider));
            }
            else
            {
                if (isMemoryCache)
                {
                    cacheservice = App.GetService<MemoryCacheService>();
                }
                else
                {
                    cacheservice = App.GetService<ICacheService>();

                }
            }
        }
        #region 驗證快取項是否存在
        /// <summary>
        /// 驗證快取項是否存在,TryGetValue 來檢測 Key是否存在的
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            return cacheservice.Exists(key);
        }
        #endregion

        #region 新增快取

        /// <summary>
        /// 新增快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">快取Value</param>
        /// <returns></returns>
        public bool Add(string key, object value)
        {
            return cacheservice.Add(key, value);
        }
        /// <summary>
        /// 新增快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">快取Value</param>
        /// <param name="expiresSliding">滑動過期時長（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <param name="expiressAbsoulte">絕對過期時長</param>
        /// <returns></returns>
        public bool Add(string key, object value, TimeSpan expiresSliding, TimeSpan expiressAbsoulte)
        {
            return cacheservice.Add(key, value, expiresSliding, expiressAbsoulte);
        }
        /// <summary>
        /// 新增快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">快取Value</param>
        /// <param name="expiresIn">快取時長</param>
        /// <param name="isSliding">是否滑動過期（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <returns></returns>
        public bool Add(string key, object value, TimeSpan expiresIn, bool isSliding = false)
        {
            return cacheservice.Add(key,value,expiresIn,isSliding);
        }
        #endregion

        #region 刪除快取

        /// <summary>
        /// 刪除快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        public bool Remove(string key)
        {
            return cacheservice.Remove(key);
        }
        /// <summary>
        /// 批量刪除快取
        /// </summary>
        /// <param name="keys">快取Key集合</param>
        /// <returns></returns>
        public void RemoveAll(IEnumerable<string> keys)
        {
            cacheservice.RemoveAll(keys);
        }
        /// <summary>
        /// 刪除匹配到的快取
        /// </summary>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public void RemoveByPattern(string pattern)
        {
            cacheservice.RemoveByPattern(pattern);
        }
        /// <summary>
        /// 刪除所有快取
        /// </summary>
        public void RemoveCacheAll()
        {
            cacheservice.RemoveCacheAll();
        }
        #endregion

        #region 獲取快取
        /// <summary>
        /// 獲取快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        public T Get<T>(string key) where T : class
        {
            return cacheservice.Get<T>(key);
        }
        /// <summary>
        /// 獲取快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        public object Get(string key)
        {
            return cacheservice.Get(key);
        }
        /// <summary>
        /// 獲取快取集合
        /// </summary>
        /// <param name="keys">快取Key集合</param>
        /// <returns></returns>
        public IDictionary<string, object> GetAll(IEnumerable<string> keys)
        {
            return cacheservice.GetAll(keys);
        }

        #endregion

        #region 修改快取
        /// <summary>
        /// 修改快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">新的快取Value</param>
        /// <returns></returns>
        public bool Replace(string key, object value)
        {
            return cacheservice.Replace(key, value);

        }
        /// <summary>
        /// 修改快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">新的快取Value</param>
        /// <param name="expiresSliding">滑動過期時長（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <param name="expiressAbsoulte">絕對過期時長</param>
        /// <returns></returns>
        public bool Replace(string key, object value, TimeSpan expiresSliding, TimeSpan expiressAbsoulte)
        {
            return cacheservice.Replace(key, value, expiresSliding, expiressAbsoulte);
        }
        /// <summary>
        /// 修改快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">新的快取Value</param>
        /// <param name="expiresIn">快取時長</param>
        /// <param name="isSliding">是否滑動過期（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <returns></returns>
        public bool Replace(string key, object value, TimeSpan expiresIn, bool isSliding = false)
        {
            return cacheservice.Replace(key,value,expiresIn,isSliding);
        }
        #endregion

    }
}
