module.exports = {
  title: '奧洛後台管理系統',
  /**
   * 側邊欄主題 深色主題theme-dark，淺色主題theme-light
   */
  sideTheme: 'theme-dark',
  /**
   * 是否系統佈局配置
   */
  showSettings: false,

  /**
   * 是否顯示 tagsView
   */
  tagsView: true,
  /**
   *是否固定頭部
   */
  fixedHeader: false,
  /**
   * 是否顯示側邊Logo
   */
  sidebarLogo: true,

  /**
   * 應用Id
   */
  appId: 'system',
  /**
   * 應用金鑰
   */
  appSecret: '87135AB0160F706D8B47F06BDABA6FC6',
  /**
   * 子系統
   */
  subSystem: {},
  /**
   * 目前訪問系統程式碼
   */
  activeSystemCode: 'openauth',
  /**
   * 目前訪問系統名稱
   */
  activeSystemName: '',
  /**
   * 動態可訪問路由
   */
  addRouters: {},

  // apiHostUrl: 'http://netcoreapi.ts.Auro.com/api/', // 基礎介面
  // apiSecurityUrl: 'http://netcoreapi.ts.Auro.com/api/Security/', // 許可權管理系統介面
  // apiCMSUrl: 'http://netcoreapi.ts.Auro.com/api/CMS/', // 文章
  // fileUrl: 'http://netcoreapi.ts.Auro.com/', // 檔案訪問路徑
  // fileUploadUrl: 'http://netcoreapi.ts.Auro.com/api/Files/Upload'// 檔案上傳路徑

  // apiHostUrl: 'http://localhost:54678/api/', // 基礎介面
  // apiSecurityUrl: 'http://localhost:54678/api/Security/', // 許可權管理系統介面
  // apiCMSUrl: 'http://localhost:54678/api/CMS/', // 文章
  // fileUrl: 'http://localhost:54678/', // 檔案訪問路徑
  // fileUploadUrl: 'http://localhost:54678/api/Files/Upload'// 檔案上傳路徑

  apiHostUrl: 'https://localhost:44366/api/', // 基礎介面
  apiSecurityUrl: 'https://localhost:44366/api/Security/', // 許可權管理系統介面
  apiCMSUrl: 'https://localhost:44366/api/CMS/', // 文章
  fileUrl: 'https://localhost:44366/', // 檔案訪問路徑
  fileUploadUrl: 'https://localhost:44366/api/Files/Upload' // 檔案上傳路徑
  // apiHostUrl: 'http://193.168.25.137:8082/api/', // 基礎介面
  // apiSecurityUrl: 'http://193.168.25.137:8082/api/Security/', // 許可權管理系統介面
  // apiCMSUrl: 'http://193.168.25.137:8082/api/CMS/', // 文章
  // fileUrl: 'http://193.168.25.137:8082/', // 檔案訪問路徑
  // fileUploadUrl: 'http://193.168.25.137:8082/api/Files/Upload'// 檔案上傳路徑
}
