using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.Commons.Dtos;
using Auro.Commons.Mapping;
using Auro.Commons.Pages;
using Auro.Commons.Encrypt;
using Auro.Commons.Json;
using Auro.Commons.Services;
using Auro.WebAnalytics.GoogleAnalytics.IRepositories;
using Auro.WebAnalytics.GoogleAnalytics.IServices;
using Auro.WebAnalytics.GoogleAnalytics.Dtos;
using Auro.WebAnalytics.GoogleAnalytics.Models;
using Auro.WebAnalytics.Enums;
using System.Text.RegularExpressions;
using Auro.Commons.Helpers;

namespace Auro.GoogleAnalytics.Services
{
    /// <summary>
    /// Google Analytics服務
    /// </summary>
    public class GoogleAnalyticsService: BaseService<GAModel,GoogleAnalyticsOutputDto, string>, IGoogleAnalyticsService
    {
        private readonly IGoogleAnalyticsRepository _repository;
        public GoogleAnalyticsService(IGoogleAnalyticsRepository repository) : base(repository)
        {
            _repository=repository;
        }

       public  async  Task<ResponseReport> GetDateRangeGAData(string OpenIdType, string OpenName, GoogleAnalyticsInputDto inputDto)
        {

            ResponseReport responseReport= new();
            try
            {

                
                if (DateTime.TryParse(inputDto.EventStartDate, out DateTime sDatetime) && DateTime.TryParse(inputDto.EventEndDate, out DateTime eDatetime))
                {
                    var eventDateDiff =((eDatetime - sDatetime).Days);
                    string inParas = string.Empty;
                    string[] strAry =new string[inputDto.ParameterDefinition.Count];
                    for (int i = 0; i < inputDto.ParameterDefinition.Count; i++)
                    {
                        strAry[i] = DEncrypt.Encrypt(inputDto.EventStartDate + inputDto.EventEndDate + JsonHelper.ToJson(inputDto.ParameterDefinition[i]));
                        if (i!= inputDto.ParameterDefinition.Count - 1)
                        {
                            
                            inParas += "'" + strAry[i] + "',";
                        }
                        else
                        {
                            inParas += "'" + strAry[i] + "'";
                        }
                        
                    }
                    string where = string.Empty;
                    where = GetDataPrivilege(false);
                    where += " AND Parameters IN(" + inParas+")" ;
                    var records = await _repository.GetAllByIsEnabledMarkAsync(where);
                    
                    List<GAModel> _eventList = records.ToList();
                    List<OutputReport> outputReport = new();
                   
                    //List<GAModel> gaModelList = enumerable.ToList();
                    if (_eventList != null && _eventList.Count== inputDto.ParameterDefinition.Count)
                    {
                        Array.Clear(strAry, 0, strAry.Length);
                        List<DataResult> dataResults = new();
                        

                        for (int i=0;i< inputDto.ParameterDefinition.Count; i++)
                        {
                            DataResult dataResult = JsonHelper.ToObject<DataResult>(_eventList[i].Events) ;
                            //strAry[i] = _eventList[i].Events;

                            dataResults.Add(dataResult);
                           
                        }
                        responseReport = new ResponseReport { 
                            StartDate= inputDto.EventStartDate,
                            EndDate= inputDto.EventEndDate,
                            GaReport= dataResults

                        };


                    }
                    else
                    {

                        responseReport = await _repository.AnalyticsReport(inputDto.Description, OpenIdType, OpenName, inputDto.ParameterDefinition, inputDto.EventStartDate, inputDto.EventEndDate);
                        
                    }

                }
               
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
               
            }


            return responseReport;

        }

       public async Task<int> StoreGoogleAnalyticsResult( string OpenIdType, string OpenName, GoogleAnalyticsInputDto inputDto)
        {
            
            var responseReport = await _repository.AnalyticsReport(inputDto.Description,OpenIdType, OpenName, inputDto.ParameterDefinition, inputDto.EventStartDate, inputDto.EventEndDate);
            List<GAModel> gAModelList = new();
            if(responseReport!=null && responseReport.GaReport.Count > 0)
            {
                for(int i=0;i< responseReport.GaReport.Count; i++)
                {
                    GAModel aModel = new();
                    aModel.Events = Regex.Replace(responseReport.GaReport[i].ToJson(), @"\s+", "");
                    aModel.EventStartDate = inputDto.EventStartDate;
                    aModel.EventEndDate = inputDto.EventEndDate;
                    aModel.EventType = inputDto.EventType;
                    aModel.EventName = nameof(ReportName.GADailyReport);
                    aModel.Parameters = DEncrypt.Encrypt(inputDto.EventStartDate + inputDto.EventEndDate + JsonHelper.ToJson(inputDto.ParameterDefinition[i]));
                    aModel.Id = GuidUtils.CreateNo();
                    aModel.CreatorTime = inputDto.CreatorTime;
                    aModel.CreatorUserId = inputDto.CreatorUserId;
                    aModel.DeleteMark = inputDto.DeleteMark;
                    gAModelList.Add(aModel);
                }
                

               
                
            }
            
            return await _repository.AddRangeAsync(gAModelList);
        } 

       
    }
}