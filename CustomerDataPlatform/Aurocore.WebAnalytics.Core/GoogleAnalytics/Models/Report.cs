﻿using Google.Apis.AnalyticsReporting.v4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.WebAnalytics.GoogleAnalytics.Models
{
   
     
    public class DataRowDimension
    {
        public string[] Value { get; set; }// Dimsion 值
    }
    public class DataRowMetric
    {

        public string[] Value { get; set; } //量值
    }
    public class MetricEntry
    {

        public string name { get; set; }
        public string type { get; set; } 
    }
    public class MetricHeader
    {
        public MetricHeader()
        {

            metricEntries = new List<MetricEntry>();
           
        }
        public List<MetricEntry> metricEntries { get; set; }

    }
    /// <summary>
    /// 單一報表資料集合
    /// </summary>

    public class DataResult
    {
        public DataResult(){

            rowDimension = new List<DataRowDimension>();
            rowMetric = new List<DataRowMetric>();
        }
        public string rowCount { get; set; }
        public string[] dimensionHeader { get; set; }//Dimsion 欄位名
        public MetricHeader metricHeader { get; set; }// Metric 欄位名
        public List<DataRowDimension> rowDimension { get; set; }
        public List<DataRowMetric> rowMetric { get; set; }
        public IList<DateRangeValues> Total { get; set; }
    }
    public interface IResponseReport
    {
        public string StartDate { get; set; } //資料起始日
        public string EndDate { get; set; }// 結束日
    }

    public class ResponseReport: IResponseReport
    {
        public ResponseReport()
        {
            GaReport = new List<DataResult>();
        }
        public string StartDate { get; set; } //資料起始日
        public string EndDate { get; set; }// 結束日
        public List<DataResult> GaReport { get; set; }
    }
    public class OutputReport: IResponseReport
    {
        public string StartDate { get; set; } //資料起始日
        public string EndDate { get; set; }// 結束日
        public string[] GaReport { get; set; }
    }

}
