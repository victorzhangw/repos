using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Auro.Commons.Models;

namespace Auro.WebAnalytics.GoogleAnalytics.Models
{
    /// <summary>
    /// Google Analytics，資料實體物件
    /// </summary>
    [Table("CRM_GoogleAnalytics")]
    [Serializable]
    public class GAModel:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
       

        /// <summary>
        /// 事件形態
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// 事件
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 開始日期
        /// </summary>
        public string EventStartDate { get; set; }
        /// <summary>
        /// 結束日期
        /// </summary>
        public string EventEndDate { get; set; }

        /// <summary>
        /// 鑰 GA Dimension/Metric
        /// </summary>
        public string Parameters { get; set; }

        /// <summary>
        /// 事件資料
        /// </summary>
        public string Events { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DeleteUserId { get; set; }

    }
}
