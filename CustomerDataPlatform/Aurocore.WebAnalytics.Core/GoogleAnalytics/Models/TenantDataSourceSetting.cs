﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.WebAnalytics.GoogleAnalytics.Models
{
    /// <summary>
    /// 租戶資料來源設定
    /// </summary>
    public class TenantDataSourceSetting
    {
        /// <summary>
        /// 存取 Id
        /// </summary>
        public string AccessId { get; set; }
        /// <summary>
        /// 存取 Id
        /// </summary>
        public string SecretKey { get; set; }
        /// <summary>
        /// 存取 Token
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// 更新 Token
        /// </summary>
        public string RefreshToken { get; set; }
        
        /// <summary>
        /// 關鍵字
        /// </summary>
        public string IdentfyKeyword { get; set; }
       
    }
    /// <summary>
    /// Univer GA 資料來源設定
    /// </summary>
    public class UniversalGACfg:TenantDataSourceSetting
    {
        public string ViewId { get; set; } 
        
        

    }
    /// <summary>
    /// Tealeaf 資料來源設定
    /// </summary>
    public class TealeafCfg : TenantDataSourceSetting
    {
       public TealeafCfg()
        {
            TealeafReports = new();
        }
        /// <summary>
        /// 
        /// </summary>
       List<TealeafReport> TealeafReports { get; set; }
    }
    public class TealeafReport
    {
        /// <summary>
        /// 報表名
        /// </summary>
        public string ReportName { get; set; }
        /// <summary>
        /// 報表略過列數
        /// </summary>
        public int SkippedRow { get; set; }
        /// <summary>
        /// 執行報表形態
        /// </summary>
        public string ExcuteMethod { get; set; }
        /// <summary>
        /// 最大輸出筆數
        /// </summary>
        public int MaxOutput { get; set; }
    }
}
