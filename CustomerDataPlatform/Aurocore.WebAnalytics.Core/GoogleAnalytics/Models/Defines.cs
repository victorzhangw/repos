﻿using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.WebAnalytics.GoogleAnalytics.Models
{
    public class ParameterDefines
    {
        public string Dimension { get; set; }
        public string Metric { get; set; }
    }
    public  class OpenSettings
    {
        public string Id { get; set; }
        public string Token { get; set; }
        public string VIewId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
    public  class ConnectedSettings
    {
        public ConnectedSettings()
        {
            openSettings = new();
            analyticsReportingService = new();
            clientSecrets = new();
            tokenResponse = new();
        }
        public  OpenSettings openSettings { get; set; }
        public  AnalyticsReportingService analyticsReportingService { get; set; }
        public  ClientSecrets clientSecrets { get; set; }
        public  TokenResponse tokenResponse { get; set; }
    }

}
