using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.Commons.IServices;
using Auro.WebAnalytics.GoogleAnalytics.Dtos;
using Auro.WebAnalytics.GoogleAnalytics.Models;

namespace Auro.WebAnalytics.GoogleAnalytics.IServices
{
    /// <summary>
    /// 定義Google Analytics服務接口
    /// </summary>
    public interface IGoogleAnalyticsService:IService<GAModel,GoogleAnalyticsOutputDto, string>
    {
         Task<int> StoreGoogleAnalyticsResult(string OpenIdType, string OpenName, GoogleAnalyticsInputDto inputDto);
         Task <ResponseReport> GetDateRangeGAData(string OpenIdType, string OpenName, GoogleAnalyticsInputDto inputDto);

    }
}
