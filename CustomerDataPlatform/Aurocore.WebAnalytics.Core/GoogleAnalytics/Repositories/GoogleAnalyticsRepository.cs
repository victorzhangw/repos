using System;
using Dapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Commons.Json;
using Auro.WebAnalytics.GoogleAnalytics.IRepositories;
using Auro.WebAnalytics.GoogleAnalytics.Models;
using Auro.WebAnalytics.Helpers;
using Auro.Commons.Log;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Services;
using System.IO;

namespace Auro.WebAnalytics.GoogleAnalytics.Repositories
{
    /// <summary>
    /// Google Analytics倉儲接口的實現
    /// </summary>
    public class GoogleAnalyticsRepository : BaseRepository<GAModel, string>, IGoogleAnalyticsRepository
    {
        public GoogleAnalyticsRepository()
        {
           
        }

        public GoogleAnalyticsRepository(IDbContextCore context) : base(context)
        {
        }
        // 全域變數
        public GetReportsResponse storeGAReports = new()
        {
            Reports = new List<Report>()

        };
        public async Task<ResponseReport> AnalyticsReport(string desc,string OpenIdType, string OpenName, List<ParameterDefines> parameterDefine, string startDate, string endDate)
        {
            ResponseReport responseReports = new();
            try
            {

                ConnectedSettings connectedSetting = new();
                connectedSetting = await ConnectGoogleService(desc,OpenIdType, OpenName).ConfigureAwait(false);
                /*
                 var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                  new GoogleAuthorizationCodeFlow.Initializer
                  {
                      ClientSecrets = connectedSetting.clientSecrets,
                     //ClientSecrets = GetClientConfiguration().Secrets
                  }), connectedSetting.openSettings.Id, connectedSetting.tokenResponse);

                 using var svc = new AnalyticsReportingService(
                          new BaseClientService.Initializer
                          {
                              HttpClientInitializer = credentials,
                              ApplicationName = "My Project"
                          });*/

                var dateRange = new DateRange
                {
                    StartDate = startDate,
                    EndDate = endDate
                };
                List<ReportRequest> reportRequests = new List<ReportRequest>();
                List<DateRange> dateRanges = new List<DateRange> { dateRange };

                // GA SDK 批次取報表最多五筆

                foreach (var defineItem in parameterDefine)
                {

                    List<string> _dim = new();
                    List<string> _metric = new();

                    if (!String.IsNullOrWhiteSpace(defineItem.Dimension) && !String.IsNullOrWhiteSpace(defineItem.Metric))
                    {
                        _dim = defineItem.Dimension.Split(",").ToList();
                        _metric = defineItem.Metric.Split(",").ToList();

                        //var reportRequest = generateReportRequest(connectedSetting.openSettings.VIewId, dateRanges, _dim, _metric, dictDimensions, dictMetrics);
                        var reportRequest = generateReportRequest(connectedSetting.openSettings.VIewId, dateRanges, _dim, _metric);
                        reportRequests.Add(reportRequest);
                        // 建立 GetReportsRequest 實體


                    }
                   
                }
                GetReportsRequest getRequest = new GetReportsRequest() { ReportRequests = reportRequests };
                // responseReports = await GetGAReport(getRequest, connectedSetting.analyticsReportingService);
                responseReports = await GetGAReport(getRequest, connectedSetting.analyticsReportingService);
                responseReports.StartDate = startDate;
                responseReports.EndDate = endDate;

            }
            catch (Exception ex)
            {
                Log4NetHelper.Info(string.Format(ex.Message));
                throw new Exception(ex.Message);
            }
            // 重構 檢查輸入List[ParameterDefines]與 google 回傳 dimension 與 metric 相同
            return responseReports;
        }
        #region 連接 Google 服務
        private async Task<ConnectedSettings> ConnectGoogleService(string desc,string OpenIdType,string OpenName)
        {
            ConnectedSettings connectedSettings =new();
            
            string settingSql = $"SELECT Settings FROM Sys_OpenIdSettings WHERE OpenIdType=@OpenIdType AND Name=@OpenName AND Description=@desc";
            var settingStr = await DapperConn.ExecuteScalarAsync<string>(settingSql, new { @OpenIdType = OpenIdType, @OpenName = OpenName ,@desc =desc});
            OpenSettings openSettings = JsonHelper.ToObject<OpenSettings>(settingStr); 

            var refreshToken = new TokenResponse { RefreshToken = openSettings.Token };
            ClientSecrets clientSecrets = new ClientSecrets { ClientId = openSettings.ClientId, ClientSecret = openSettings.ClientSecret };
            
            var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                new GoogleAuthorizationCodeFlow.Initializer
                {
                     ClientSecrets = clientSecrets,
                  //  ClientSecrets = GetClientConfiguration().Secrets
                }), openSettings.Id, refreshToken);
            var svc = new AnalyticsReportingService(
                     new BaseClientService.Initializer
                     {
                         HttpClientInitializer = credentials,
                         ApplicationName = "My Project"
                     });
            connectedSettings.openSettings = openSettings;
            connectedSettings.tokenResponse = refreshToken;
            connectedSettings.clientSecrets = clientSecrets;
            connectedSettings.openSettings.ClientId = openSettings.ClientId;
            connectedSettings.openSettings.VIewId = openSettings.VIewId;
            connectedSettings.analyticsReportingService = svc;
            return connectedSettings;
        }
        #endregion
        #region 產生 Request 參數
        private ReportRequest generateReportRequest(string viewId, List<DateRange> dateRange, List<string> dimension, List<string> metric,string pageToken=null)
        {
            
            Dictionary<string, Dimension> dictDimensions = GaDefineHelper.SetDimensions();
            Dictionary<string, Metric> dictMetrics = GaDefineHelper.SetMetrics();
            
            List<Dimension> reqDimension = new();
            List<Metric> reqMetric = new();
            for (int i = 0; i < dimension.Count; i++)
            {
                reqDimension.Add(dictDimensions[dimension[i]]);
            }
            for (int j = 0; j < metric.Count; j++)
            {
                reqMetric.Add(dictMetrics[metric[j]]);
            }
            var reportRequest = new ReportRequest
            {
                DateRanges = dateRange,
                Dimensions = reqDimension,
                Metrics = reqMetric,
                ViewId = viewId,
                PageToken= pageToken
            };
            return reportRequest;
        }
        #endregion
        #region 取得 GA 報表
        private async Task<ResponseReport> GetGAReport(GetReportsRequest request, AnalyticsReportingService svc, bool askMore = false)
        {
           
            askMore = false;
            ResponseReport lstReports = new();
            
            try
            {
                GetReportsResponse _response = await svc.Reports.BatchGet(request).ExecuteAsync().ConfigureAwait(false);
              //  string[][] tmpary = new string { [0, 0],[0,0] };
                for(int i=0;i< _response.Reports.Count; i++)
                {
                    // 判斷是否還有未取回資料
                    if (!string.IsNullOrEmpty(_response.Reports[i].NextPageToken)&&(_response.Reports[i].NextPageToken!="0"))
                    {
                        request.ReportRequests[i].PageToken = _response.Reports[i].NextPageToken;
                        askMore = true;
                    }
                    else
                    {
                        request.ReportRequests[i].Metrics = new List<Metric>();
                        request.ReportRequests[i].Dimensions = new List<Dimension>();
                        request.ReportRequests[i].PageToken = null;
                        request.ReportRequests[i].PageSize = 0;
                        // request.ReportRequests[i].DateRanges = new List<DateRange>();
                    }
                    // 判斷報表是否已存在，若真則直接加入，否則加入 rows
                    if (storeGAReports.Reports.Count != request.ReportRequests.Count)
                    {
                        storeGAReports.Reports.Add(_response.Reports[i]);
                    }
                    else
                    {
                        if (_response.Reports[i].Data.Rows != null)
                        {
                            ((List<ReportRow>)storeGAReports.Reports[i].Data.Rows).AddRange(_response.Reports[i].Data.Rows);
                        }
                       
                    }
                    
                }

                if (askMore)
                {
                    await GetGAReport(request, svc, askMore).ConfigureAwait(false) ;
                }
                
                //printResults(response1.Reports as List<Report>);
                for (int i = 0; i < storeGAReports.Reports.Count; i++)
                {
                    Models.MetricHeader metricHeaders = new();
                    List<DataResult> lstresult = new();
                    int rowcount = storeGAReports.Reports[i].Data.RowCount.HasValue ? storeGAReports.Reports[i].Data.RowCount.Value : 0;
                    string[] dimensionHeader = storeGAReports.Reports[i].ColumnHeader.Dimensions.ToArray(); // dimension header
                    List<DataRowMetric> dataRowMetrics = new();
                    List<DataRowDimension> dataDims = new();
                   

                    for (int j = 0; j < storeGAReports.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries.Count; j++)
                    {

                        metricHeaders.metricEntries.Add(new MetricEntry { name = storeGAReports.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries[j].Name, type = storeGAReports.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries[j].Type });
                    }

                    for (int j = 0; j < rowcount; j++)
                    {



                        for (int k = 0; k < storeGAReports.Reports[i].Data.Rows[j].Metrics.Count; k++)
                        {
                            // Metric
                            DataRowMetric rowMetric = new ();
                            IList<string> _tmplist = storeGAReports.Reports[i].Data.Rows[j].Metrics[k].Values;
                            rowMetric.Value = new string[_tmplist.Count];
                            //Array.Clear(rowMetric.Value,0, rowMetric.Value.Length);
                            Array.ConstrainedCopy(_tmplist.ToArray(), 0, rowMetric.Value, 0, _tmplist.Count);
                            for (int index = 0; index < rowMetric.Value.Length; index++)
                            {
                                string _metric = string.Empty;
                                double num;
                                if (Double.TryParse(rowMetric.Value[index], out num))
                                {
                                    _metric = Math.Round(num, 2, MidpointRounding.AwayFromZero).ToString();
                                    rowMetric.Value[index] = _metric;
                                }
                            }
                            dataRowMetrics.Add(rowMetric);



                        }
                        int dimCount = 0;
                        try
                        {


                            if (storeGAReports.Reports[i].Data.Rows[j].Dimensions != null)
                            {
                                dimCount = storeGAReports.Reports[i].Data.Rows[j].Dimensions.Count;
                            }

                        }
                        catch (Exception ex)
                        {
                            Log4NetHelper.Error(ex.ToString());
                        }

                        // 加入 Dimension 
                        if (dimCount != 0)
                        {

                            DataRowDimension dataRowDimension = new();
                            dataRowDimension.Value = storeGAReports.Reports[i].Data.Rows[j].Dimensions.ToArray();
                            int dindex = Array.IndexOf(dimensionHeader, "ga:regionIsoCode");

                            if (dindex != -1)
                            {
                                var _val = TwISOCodeHelpers.IsoCodeConverter(dataRowDimension.Value[dindex]);
                                dataRowDimension.Value[dindex] = _val.ShortName;
                            }
                            dataDims.Add(dataRowDimension);

                        }

                    }

                    lstReports.GaReport.Add(new DataResult
                    {

                        rowCount = storeGAReports.Reports[i].Data.RowCount.ToString(),
                        metricHeader = metricHeaders,
                        dimensionHeader = dimensionHeader,
                        rowDimension = dataDims,
                        rowMetric = dataRowMetrics,
                        Total = storeGAReports.Reports[i].Data.Totals
                    });
                }


            }
            catch (Exception ex)
            {
                Log4NetHelper.Info(string.Format(ex.Message));
                throw new Exception(ex.Message);
                
                //Console.WriteLine(ex);
            }
            svc.Dispose();
            return lstReports;
        }
        #endregion
        #region 使用 client Json 檔案
        private static GoogleClientSecrets GetClientConfiguration()
        {
            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                return GoogleClientSecrets.Load(stream);
            }
        }
        #endregion

    }




}