using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.Commons.IRepositories;
using Auro.WebAnalytics.GoogleAnalytics.Models;


namespace Auro.WebAnalytics.GoogleAnalytics.IRepositories
{
    /// <summary>
    /// 定義Google Analytics倉儲接口
    /// </summary>
    public interface IGoogleAnalyticsRepository:IRepository<GAModel, string>
    {
         Task<ResponseReport> AnalyticsReport(string desc,string OpenIdType, string OpenName,  List<ParameterDefines> parameterDefine, string startDate, string endDate);
    }
}