using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Auro.WebAnalytics.GoogleAnalytics.Dtos
{
    /// <summary>
    /// Google Analytics輸出物件模型
    /// </summary>
    [Serializable]
    public class GoogleAnalyticsOutputDto
    {
        

        /// <summary>
        /// 鑰事件形態
        /// </summary>
        [MaxLength(100)]
        public string EventType { get; set; }

        /// <summary>
        /// 鑰事件
        /// </summary>
        [MaxLength(512)]
        public string EventName { get; set; }

        /// <summary>
        /// 鑰日期
        /// </summary>
        [MaxLength(3)]
        public string EventDate { get; set; }

        /// <summary>
        /// 鑰事件資料
        /// </summary>
        [MaxLength(-1)]
        public string Events { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

    }
}
