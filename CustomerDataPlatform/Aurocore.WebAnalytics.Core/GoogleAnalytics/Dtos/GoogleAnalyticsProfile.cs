using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.WebAnalytics.GoogleAnalytics.Models;

namespace Auro.WebAnalytics.GoogleAnalytics.Dtos
{
    public class GoogleAnalyticsProfile : Profile
    {
        public GoogleAnalyticsProfile()
        {
           CreateMap<GAModel, GoogleAnalyticsOutputDto>();
           CreateMap<GoogleAnalyticsInputDto, GAModel>();

        }
    }
}
