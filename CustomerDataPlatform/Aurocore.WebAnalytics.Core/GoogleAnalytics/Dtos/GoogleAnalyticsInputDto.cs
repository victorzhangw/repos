using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Models;
using Auro.Commons.Dtos;
using Auro.WebAnalytics.GoogleAnalytics.Models;

namespace Auro.WebAnalytics.GoogleAnalytics.Dtos
{
    /// <summary>
    /// Google Analytics輸入物件模型
    /// </summary>
    [AutoMap(typeof(GAModel))]
    [Serializable]
    public class GoogleAnalyticsInputDto: IInputDto<string>
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 自訂 Identifier 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 事件形態
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// 事件
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        public string EventStartDate { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string EventEndDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// GA Dimension/Metric
        /// </summary>
        public List<ParameterDefines> ParameterDefinition{ get; set; }


 
    }
}
