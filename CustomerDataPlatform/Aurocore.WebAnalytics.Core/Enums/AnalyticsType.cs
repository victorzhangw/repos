﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.WebAnalytics.Enums
{
    public enum AnalyticsType
    {
      Google_Analytics,
      Facebook_Pixel,
      Tealeaf
    }
   
}
