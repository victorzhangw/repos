﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auro.WebAnalytics.Enums
{
    public enum ReportName
    {
        GADailyReport,
        TealeafDailyReport
    }
}
