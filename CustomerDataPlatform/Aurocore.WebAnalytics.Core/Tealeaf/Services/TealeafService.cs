using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.Commons.Dtos;
using Auro.Commons.Mapping;
using Auro.Commons.Pages;
using Auro.Commons.Services;
using Auro.WebAnalytics.Tealeaf.IRepositories;
using Auro.WebAnalytics.Tealeaf.IServices;
using Auro.WebAnalytics.Tealeaf.Dtos;
using Auro.WebAnalytics.Tealeaf.Models;

namespace Auro.Tealeaf.Services
{
    /// <summary>
    /// Tealeaf Events服務接口實現
    /// </summary>
    public class TealeafService: BaseService<TealeafModel,TealeafOutputDto, string>, ITealeafService
    {
		private readonly ITealeafRepository _repository;
        public TealeafService(ITealeafRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}