using System;
using Auro.Commons.IRepositories;
using Auro.WebAnalytics.Tealeaf.Models;

namespace Auro.WebAnalytics.Tealeaf.IRepositories
{
    /// <summary>
    /// 定義Tealeaf Events倉儲接口
    /// </summary>
    public interface ITealeafRepository:IRepository<TealeafModel, string>
    {
    }
}