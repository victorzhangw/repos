using System;
using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.WebAnalytics.Tealeaf.IRepositories;
using Auro.WebAnalytics.Tealeaf.Models;

namespace Auro.Tealeaf.Repositories
{
	/// <summary>
	/// Tealeaf Events倉儲接口的實現
	/// </summary>
	public class TealeafRepository : BaseRepository<TealeafModel, string>, ITealeafRepository
	{
		public TealeafRepository()
		{
		}

		public TealeafRepository(IDbContextCore context) : base(context)
		{
		}
	}
}