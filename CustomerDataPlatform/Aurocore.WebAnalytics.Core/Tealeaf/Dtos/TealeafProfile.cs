using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.WebAnalytics.Tealeaf.Models;

namespace Auro.WebAnalytics.Tealeaf.Dtos
{
    public class TealeafProfile : Profile
    {
        public TealeafProfile()
        {
           CreateMap<TealeafModel, TealeafOutputDto>();
           CreateMap<TealeafInputDto, TealeafModel>();

        }
    }
}
