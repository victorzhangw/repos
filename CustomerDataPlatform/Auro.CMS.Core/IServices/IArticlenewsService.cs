using System;
using Auro.Commons.IServices;
using Auro.CMS.Dtos;
using Auro.CMS.Models;
using System.Threading.Tasks;
using Auro.Commons.Pages;
using Auro.Commons.Dtos;
using System.Collections.Generic;

namespace Auro.CMS.IServices
{
    /// <summary>
    /// 定義文章服務介面
    /// </summary>
    public interface IArticlenewsService:IService<Articlenews,ArticlenewsOutputDto, string>
    {
        /// <summary>
        /// 根據使用者角色獲取分類及該分類的文章
        /// </summary>
        /// <returns></returns>
        Task<List<CategoryArticleOutputDto>> GetCategoryArticleList();
    }
}
