using System;

using Auro.Commons.Repositories;
using Auro.CMS.IRepositories;
using Auro.CMS.Models;
using Auro.Commons.IDbContext;

namespace Auro.CMS.Repositories
{
    /// <summary>
    /// 文章倉儲介面的實現
    /// </summary>
    public class ArticlenewsRepository : BaseRepository<Articlenews, string>, IArticlenewsRepository
    {
		public ArticlenewsRepository()
        {
        }

        public ArticlenewsRepository(IDbContextCore context) : base(context)
        {
        }
    }
}