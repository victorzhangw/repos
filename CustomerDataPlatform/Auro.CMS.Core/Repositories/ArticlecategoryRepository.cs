using System;
using Auro.Commons.Repositories;
using Auro.CMS.IRepositories;
using Auro.CMS.Models;
using Auro.Commons.IDbContext;

namespace Auro.CMS.Repositories
{
    /// <summary>
    /// 文章分類倉儲介面的實現
    /// </summary>
    public class ArticlecategoryRepository : BaseRepository<Articlecategory, string>, IArticlecategoryRepository
    {
		public ArticlecategoryRepository()
        {
        }

        public ArticlecategoryRepository(IDbContextCore context) : base(context)
        {
        }
    }
}