using System;
using Auro.Commons.IRepositories;
using Auro.CMS.Models;

namespace Auro.CMS.IRepositories
{
    /// <summary>
    /// 定義文章分類倉儲介面
    /// </summary>
    public interface IArticlecategoryRepository:IRepository<Articlecategory, string>
    {
    }
}