using System;
using System.Threading.Tasks;
using Auro.Commons.IRepositories;
using Auro.Tenants.Dtos;
using Auro.Tenants.Models;

namespace Auro.Tenants.IRepositories
{
    /// <summary>
    /// 定義租戶倉儲介面
    /// </summary>
    public interface ITenantRepository:IRepository<Tenant, string>
    {
        /// <summary>
        /// 根據租戶帳號查詢租戶資訊
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<Tenant> GetByUserName(string userName);

        /// <summary>
        /// 註冊租戶戶
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="tenantLogOnEntity"></param>
        Task<bool> InsertAsync(Tenant entity, TenantLogon tenantLogOnEntity);

    }
}