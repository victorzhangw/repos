using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.Commons.Dtos;
using Auro.Commons.Mapping;
using Auro.Commons.Pages;
using Auro.Commons.Services;
using Auro.Tenants.IRepositories;
using Auro.Tenants.IServices;
using Auro.Tenants.Dtos;
using Auro.Tenants.Models;

namespace Auro.Tenants.Services
{
    /// <summary>
    /// 使用者登錄資訊服務介面實現
    /// </summary>
    public class TenantLogonService : BaseService<TenantLogon,TenantLogonOutputDto, string>, ITenantLogonService
    {
		private readonly ITenantLogonRepository _repository;
        public TenantLogonService(ITenantLogonRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}