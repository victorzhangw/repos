﻿using Elasticsearch.Net.Specification.IndicesApi;
using Google.Apis.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Auro_CDPV1.Models
{
    public static class Grids
    {
        public class MemberLabel
        {
            private string registedDate=string.Empty;
            private string purchaseDate=string.Empty;
            public string memberid { get; set; } //會員 ID
            public string membername { get; set; } //姓名
            public string shippingaddress { get; set; } // 遞送地址
            public string contactaddress { get; set; } // 聯絡地址
            public string memberemail { get; set; } // 電子郵件
            public string labels { get; set; } // 標籤
            public string lastpurchase 
            {
                get 
                {
                    DateTime _date;
                    string pattern = "yyyy/MM/dd";
                    
                    DateTime.TryParse(purchaseDate, out _date);
                    string newDate = _date.ToString("yyyy/MM/dd");
                    return newDate;
                }
                set
                {
                    purchaseDate = value;


                } 
            }// 最後購買

            public string contactphone { get; set; } // 聯絡電話
            
            public string channel { get; set; } //註冊通路
            public string gender { get; set; }//性別
            public string memberrank { get; set; }//會員等級
            public string lastvisit { get; set; }
            public string registerdate 
            {
                get
                {
                    DateTime _date;
           

                    DateTime.TryParse(registedDate, out _date);
                    string newDate = _date.ToString("yyyy/MM/dd");
                    return newDate;
                }
                set
                {
                    registedDate = value;


                }
            }
            public string memberfb { get; set; }
            public string mobile { get; set; } // 聯絡電話
        }
        public class Result
        {
            public int count { get; set; } //會員 ID
            public List<MemberLabel> result { get; set; } //姓名
           
        }
        public class LabelCategory
        {
            [JsonProperty("id")]
            public string id { get; set; }
            [JsonProperty("text")]
            public string labelname { get; set; }
            [JsonProperty("tooltipcontent")]
            public string tooltipcontent { get; set; }
        }
        public class LabelCategoryResult
        {
            [JsonProperty("id")]
            public string id { get; set; } //標籤 ID
            [JsonProperty("text")]
            public string labelname { get; set; } //標籤名稱
            public string tooltipcontent { get; set; }
            public List<LabelCategory> child { get; set; } //標籤項目

        }
    }
}
