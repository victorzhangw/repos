﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auro_CDPV1.Models
{
    public class GoogleAnalyricsData
    {
        public  class ResponseGAResult
        {
            public List<GA_Reports> GA_Reports { get; set; }
        }
        public  class Rtn_GAResult
        {
            public List<string> dimensions { get; set; }
            public List<string> values { get; set; }

        }
       
        public class Rtn_ratiowidget
        {
            public string SumuUers { get; set; }
            public string SumSessions { get; set; }
            public string SumPageviews { get; set; }
            public string AvgBounceRate { get; set; }
            public string LastPeriodUsers { get; set; }

        }
        public class Rtn_widget_Details
        {
            public string daytypes { get; set; }
            public List<Rtn_GAResult> Rtn_GAResult { get; set; }

        }
        public class Widgets
        {
            public string name { get; set; }
            public string value { get; set; }
        }
        
        public class GAMetric
        {
            public List<Metrics> metric { get; set; }
   
        }
        public class Metrics
        {
            
           
            public string value { get; set; } //  metric value
        }

        public class GA_Results
        {

            public List<string> dimension { get; set; } // dimension value
            public List<GAMetric> metrics { get; set; }
        }
        public class GA_Reports
        {

            public List<GA_Results> ListResult { get; set; } //GA_Results
            public List<string> MetricHead { get; set; }

        }
        public class DBProductCategory
        {
            public string id { get; set; }
            public string brand { get; set; }
           
            public string mainCates { get; set; }
            public string mediumCates { get; set; }
            public string minorCates { get; set; }
        }
        
    }
}
