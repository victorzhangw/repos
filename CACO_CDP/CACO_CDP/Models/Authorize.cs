﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;

namespace Auro_CDPV1.Models
{
    public class Authorize
    {
        public class Token
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string refresh_token { get; set; }
            public string user_id { get; set; }
        }
        public class GAReportParameter
        {
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string LoginId { get; set; }
            public string MemberKey { get; set; }

        }
        public class GAReportHeader
        {
            public string SaveDB { get; set; }

            

        }
        public class TealeafReportParameter
        {
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string LoginId { get; set; }
            public string MemberKey { get; set; }

        }
        public class MemberInfo
        {
            public string userid { get; set; }
            public string memberkey { get; set; }
            public string refreshtoken { get; set; }

        }
        public class CustomGAReportParameter
        {
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string LoginId { get; set; }
            public string MemberKey { get; set; }
            public string ReqDimension1 { get; set; }
            public string ReqMetric1 { get; set; }
            public string ReqDimension2 { get; set; }
            public string ReqMetric2 { get; set; }
            public string ReqDimension3 { get; set; }
            public string ReqMetric3 { get; set; }
            public string ReqDimension4 { get; set; }
            public string ReqMetric4 { get; set; }
            public string ReqDimension5 { get; set; }
            public string ReqMetric5 { get; set; }


        }
        public class GridParameter
        {
            public string Source { get; set; } //資料來源
            public string Clause { get; set; } //查詢字串
            public string Offset { get; set; } // 資料偏移量
            public string Limit { get; set; } // 單頁資料數
        }

    }
}
