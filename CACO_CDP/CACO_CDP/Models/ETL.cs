﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auro_CDPV1.Models
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "<Pending>")]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "<Pending>")]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1034:Nested types should not be visible", Justification = "<Pending>")]
	public static class ETL
    {
		
		public class Product_dimensions
		{
			
			public string product_id { get; set; }
			public string dim_category { get; set; }
			public string dim_pricelevel { get; set; }
			public string dim_color { get; set; }
			public string dim_brand { get; set; }
			public string dim_type { get; set; }

			
		}
		public class DataStock
        {
			public string eventvalue { get; set; }
			public string eventname { get; set; }
			public string source { get; set; }
			public string startdate { get; set; }
			public string enddate { get; set; }
			public string create_date { get; set; }
		}
	}
}
