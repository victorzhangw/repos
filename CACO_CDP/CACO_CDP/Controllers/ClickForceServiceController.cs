﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Dapper;

namespace Auro_CDPV1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ClickForceServiceController : ControllerBase
    {
        private readonly IConfiguration _configuration;


        private ILogger<ClickForceServiceController> _logger { get; set; }
        public ClickForceServiceController(IConfiguration config, ILogger<ClickForceServiceController> logger)
        {
           
            _configuration = config;
            _logger = logger;
       

        }

        
        [HttpGet]
        public IActionResult ReceiveId(string UID)
        {
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string insertCommand = "INSERT INTO `idsource` (`cfuid`) VALUES('" + UID + "')";
            try
            {
                using (var connection = new MySqlConnection(cnStr.Value))
                {


                    var insertResult = connection.Execute(insertCommand);
                    _logger.LogInformation($"<--- ClickForce ID 匯入資料庫 --->{0} " + "成功");

                }
                
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Content("Failed");
            }
            return Content("Data ReceiveId:" + UID);

        }
    }
}
