﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auro_CDPV1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommuncationController : ControllerBase
    {
        [HttpGet()]
        public IActionResult Get(string cid,string nid)
        {
            Console.WriteLine("Cid::", cid);
            Console.WriteLine("Nid::", nid);
            return Ok();
        }
    }
}
