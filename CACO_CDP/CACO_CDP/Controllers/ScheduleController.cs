﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Hangfire;


namespace Auro_CDPV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<ScheduleController> _logger { get; set; }
        public ScheduleController(IConfiguration config, ILogger<ScheduleController> logger)
        {
            _configuration = config;
            _logger = logger;
        }
        public IActionResult TealeafEvents()
        {
            try
            {

            }
            catch (Exception ex)
            {
                _logger.LogError($"<--- Scheduled Error:{0}--->"+ex.Message);
            }
            return Ok();

        }
    }
}