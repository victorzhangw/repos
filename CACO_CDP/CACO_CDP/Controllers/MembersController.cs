﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Auro_CDPV1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MembersController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<MembersController> _logger { get; set; }
        public MembersController(IConfiguration config, ILogger<MembersController> logger)
        {

            _configuration = config;
            _logger = logger;
        }
        [HttpGet,HttpPost]
        public IActionResult GetMembers()
        {
           
            return Ok();
        }
    }
}