﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit;
using MailKit.Search;
using MailKit.Net.Smtp;
using MailKit.Net.Imap;
using MailKit.Security;
using MimeKit;
using System.IO;
using Ionic.Zip;
using System.Text;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;

namespace Auro_CDPV1.Helpers
{
    public class MailHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<MailHelpers> _logger { get; set; }
        public MailHelpers(IConfiguration config,ILogger<MailHelpers> logger)
        {
            _logger = logger;
            _configuration = config;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        public bool RetrieveMail()
        {
            _logger.LogInformation("<----RetrieveMail--->");
            bool returnValue = false;
            var ImapServer = _configuration.GetSection("EmailConfiguration:ImapServer").Value;
            string mailHeaderKeyWord = _configuration.GetSection("EmailConfiguration:mailHeaderKeyWord").Value;
            int.TryParse(_configuration.GetSection("EmailConfiguration:ImapPort").Value,out int ImapPort);
            var ImapUsername = _configuration.GetSection("EmailConfiguration:ImapUsername").Value;
            var ImapPassword = _configuration.GetSection("EmailConfiguration:ImapPassword").Value;
            var outDirPath = _configuration.GetSection("EmailConfiguration:outDirPath").Value;
            var zipDirPath = _configuration.GetSection("EmailConfiguration:zipDirPath").Value;
            var addDays = Int32.Parse(_configuration.GetSection("EmailConfiguration:MailReceiveDay").Value);
            DateTime queryDate = DateTime.Now.Date.AddDays(addDays);
            outDirPath = outDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
            zipDirPath = zipDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
            using (var client = new ImapClient())
            {
     
               
                #region 連線到郵件伺服器
                try
                {
                    //一、建立獲取郵件客戶端並連線到郵件伺服器。
                    //帶埠號和協議的連線方式
                    client.Connect(ImapServer, ImapPort, SecureSocketOptions.SslOnConnect);
                    _logger.LogInformation("連線成功");
                }
                catch (ImapCommandException ex)
                {
                   _logger.LogInformation($"---------嘗試連線時出錯:{0}------------" + ex.Message);
                }
                catch (ImapProtocolException ex)
                {
                   _logger.LogInformation($"---------嘗試連線時的協議錯誤:{0}------------" + ex.Message);
                }
                catch (Exception ex)
                {
                  _logger.LogInformation($"---------伺服器連線錯誤:{0}------------" + ex.Message);
                }

                try
                {
                    // 二、驗證登入資訊，輸入賬號和密碼登入。
                    client.Authenticate(ImapUsername, ImapPassword);
                    _logger.LogInformation("驗證登入 EMAIL");
                }
                catch (AuthenticationException ex)
                {
                   _logger.LogInformation($"---------無效的使用者名稱或密碼:{0}------------" + ex.Message);
                }
                catch (ImapCommandException ex)
                {
                   _logger.LogInformation($"---------嘗試驗證錯誤:{0}------------" + ex.Message);
                }
                catch (ImapProtocolException ex)
                {
                  _logger.LogInformation($"---------嘗試驗證時的協議錯誤:{0}------------" + ex.Message);
                }
                catch (Exception ex)
                {
                   _logger.LogInformation($"---------賬戶認證錯誤:{0}------------" + ex.Message);
                }
                #endregion
                try
                {
                    var folder = client.GetFolder("INBOX");
                    folder.Open(MailKit.FolderAccess.ReadOnly);
                    var mailUid = folder.Search(SearchQuery.DeliveredOn(queryDate));
                    var MailHeaders = folder.Fetch(mailUid, MessageSummaryItems.UniqueId | MessageSummaryItems.Full);

                    var mailIds = from header in MailHeaders
                                  group header by header.NormalizedSubject into mGrp
                                  let maxTime = mGrp.Max(header => header.Date.DateTime)
                                  from row in mGrp
                                  where row.Date.DateTime == maxTime && row.NormalizedSubject.Contains(mailHeaderKeyWord)
                                  select new { row.UniqueId, row.NormalizedSubject };
                    if (mailIds != null)
                    {
                        foreach (var item in mailIds)
                        {

                            MimeMessage message = folder.GetMessage(item.UniqueId);
                            foreach (MimePart attachment in message.Attachments)
                            {
                                using (var cancel = new System.Threading.CancellationTokenSource())
                                {
                                    string fileName =  Regex.Replace(item.NormalizedSubject, @"[\W_]+", "") + "~" + attachment.FileName;
                                    string filePath = Path.GetFullPath(Path.Combine(outDirPath, fileName));
                                    _logger.LogInformation($"<---- Output File Path :{filePath}--->" + filePath);
                                    //string filePath = Path.Combine(outDirPath, fileName);
                                    using (var stream = File.Create(filePath))
                                    {

                                        attachment.Content.DecodeTo(stream, cancel.Token);
                                    }
                                }
                            }
                        }
                    }
                    using (ZipFile zip = new ZipFile(Encoding.Default))
                    {

                        //string filePath = Path.Combine(outDirPath);
                        string filePath = Path.GetFullPath(Path.Combine(outDirPath));
                        _logger.LogInformation($"<---Mail Output Path---->{0}" + filePath);
                        string zipFileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                       // string zipPath = Path.Combine(zipDirPath, zipFileName);
                        string zipPath = Path.GetFullPath(Path.Combine(zipDirPath, zipFileName));

                        zip.AddDirectory(filePath);
                        zip.Save(zipPath);

                    }
                    returnValue = true;
                }
                catch(Exception ex)
                {
                    _logger.LogError($"---------郵件操作錯誤:{0}------------" + ex.Message);
                }
                


            
                
               
               
            }
            return returnValue;
        }

       

    }
}
