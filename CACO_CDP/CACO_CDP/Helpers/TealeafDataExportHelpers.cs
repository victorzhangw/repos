﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace Auro_CDPV1.Helpers
{
    public class TealeafDataExportHelpers
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL= "https://tealeaf.ibmcloud.com/webapp/login";
        private bool acceptNextAlert = true;
        private readonly IConfiguration _configuration;
        public TealeafDataExportHelpers(IConfiguration config)
        {

            _configuration = config;
        }
        public void ExportChartData()
        {
             driver = new ChromeDriver();
            //開啟網頁
            driver.Navigate().GoToUrl(baseURL);
            //隱式等待 - 直到畫面跑出資料才往下執行
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

            driver.Navigate().GoToUrl("https://tealeaf.ibmcloud.com/webapp/login");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Or'])[1]/following::button[1]")).Click();
            driver.FindElement(By.Id("username")).Clear();
            driver.FindElement(By.Id("username")).SendKeys("victor.cheng@aurocore.com");
            driver.FindElement(By.Id("continue-button")).Click();
            driver.FindElement(By.Id("password")).Clear();
            driver.FindElement(By.Id("password")).SendKeys("@SOso3669");
            driver.FindElement(By.Id("signinbutton")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Active'])[3]/following::a[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Optimize'])[1]/following::span[4]")).Click();
            driver.FindElement(By.LinkText("Public Workspaces")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Create category'])[1]/following::span[1]")).Click();
            driver.FindElement(By.LinkText("Public")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Public'])[1]/following::div[5]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='汽車險_各項頁籤使用的比例'])[1]/following::span[1]")).Click();
            driver.FindElement(By.CssSelector("svg.x1-more-icon")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Save As'])[1]/following::span[3]")).Click();
            driver.FindElement(By.LinkText("CSV")).Click();
            // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_1 | ]]
            driver.Close();
            // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | win_ser_local | ]]
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }
        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
