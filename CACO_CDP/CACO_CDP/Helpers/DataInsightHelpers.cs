﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Auro_CDPV1.Models;
using Dapper;
using MySql.Data.MySqlClient;
using ChoETL;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;



namespace Auro_CDPV1.Helpers
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
    public class DataInsightHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<CsvHelpers> _logger { get; set; }
        private CommonHelpers _common;
        
        public DataInsightHelpers(IConfiguration config, ILogger<CsvHelpers> logger, CommonHelpers commonHelpers)
        {
            _common = commonHelpers;
            _configuration = config;
            _logger = logger;
            
        }


        //合併 GA與本地端資料 
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        public  List<GoogleAnalyricsData.GA_Reports> GAAnalysisRevenue(string StartDate, string EndDate, List<GoogleAnalyricsData.GA_Reports> gA_Reports,int report_Index,int primaryID)
        {
            List<GoogleAnalyricsData.GA_Reports> lstReports = new List<GoogleAnalyricsData.GA_Reports>();
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            var gA_Result = gA_Reports[report_Index].ListResult;
            List<string> lstID = new List<string>();
            foreach(var item in gA_Result)
            {
                lstID.Add(item.dimension[primaryID]);
                 
            }
            string selectCommand = @"select DISTINCT left(product_id,8) as id, brand,
                SPLIT_STR(product_type, '*',1) as mainCates , SPLIT_STR(product_type, '*',2) as mediumCates 
                from products where left(product_id,8) in @ID ";
            StartDate = DateTime.Parse(StartDate).ToString("yyyy/MM/dd");
            EndDate = DateTime.Parse(EndDate).ToString("yyyy/MM/dd");
            try
            {
                List<GoogleAnalyricsData.DBProductCategory> results = new List<GoogleAnalyricsData.DBProductCategory>();
                List<GoogleAnalyricsData.GA_Results> lstResult = new List<GoogleAnalyricsData.GA_Results>();
                using (var connection = new MySqlConnection(cnStr.Value))
                {

                     results = connection.Query<GoogleAnalyricsData.DBProductCategory>(selectCommand,new {ID = lstID }).ToList();
                    if (results.Count > 0)
                    {
                        var query = from g in gA_Result
                                           join r in results on  g.dimension[primaryID].Substring(0,8)
                       equals r.id.Substring(0, 8) into gj
                       from subset in gj.DefaultIfEmpty()
                                    select new 
                                           {
                                               
                                               metrics= g.metrics[0].metric,
                                               mainCates = subset?.mainCates ?? String.Empty,
                                               mediumCates = subset?.mediumCates ?? String.Empty,
                                             //  minorCates = subset?.minorCates ?? String.Empty,
                                               brand = subset?.brand ?? String.Empty

                                    };
                        var lstCombined = query.ToList();
                        
                        var querymainCates = from c in lstCombined
                                             group c by new { c.mainCates } into s
                                             select new 
                                             {
                                                 metric1 = s.Sum(c => decimal.Parse(c.metrics[0].value)),
                                                 metric2 = s.Sum(c => decimal.Parse(c.metrics[1].value)),
                                                 metric3 = s.Sum(c => decimal.Parse(c.metrics[2].value)),
                                                 metric4 = s.Sum(c => decimal.Parse(c.metrics[3].value)),
                                                 dim=s.Key.mainCates
                                             };
                        var querymediumCates = from c in lstCombined
                                             group c by new { c.mediumCates } into s
                                             select new
                                             {
                                                 metric1 = s.Sum(c => decimal.Parse(c.metrics[0].value)),
                                                 metric2 = s.Sum(c => decimal.Parse(c.metrics[1].value)),
                                                 metric3 = s.Sum(c => decimal.Parse(c.metrics[2].value)),
                                                 metric4 = s.Sum(c => decimal.Parse(c.metrics[3].value)),
                                                 dim = s.Key.mediumCates
                                             };
                        var querybrand = from c in lstCombined
                                               group c by new { c.brand } into s
                                               select new
                                               {
                                                   metric1 = s.Sum(c => decimal.Parse(c.metrics[0].value)),
                                                   metric2 = s.Sum(c => decimal.Parse(c.metrics[1].value)),
                                                   metric3 = s.Sum(c => decimal.Parse(c.metrics[2].value)),
                                                   metric4 = s.Sum(c => decimal.Parse(c.metrics[3].value)),
                                                   dim= s.Key.brand
                                               };
                        var lstmainCates = querymainCates.ToList();
                        var lstmediumCates = querymediumCates.ToList();
                        var lstbrand = querybrand.ToList();
                        List<GoogleAnalyricsData.GA_Results> lstresult1 = new List<GoogleAnalyricsData.GA_Results>();
                        List<GoogleAnalyricsData.GA_Results> lstresult2 = new List<GoogleAnalyricsData.GA_Results>();
                        List<GoogleAnalyricsData.GA_Results> lstresult3 = new List<GoogleAnalyricsData.GA_Results>();
                        decimal rst1_TotalRevenue = 0; 
                        decimal rst1_TotalClick = 0;
                        decimal rst1_TotalAddcart = 0;
                        decimal rst1_TotalCheckout = 0;
                        decimal rst2_TotalRevenue = 0;
                        decimal rst2_TotalClick = 0;
                        decimal rst2_TotalAddcart = 0;
                        decimal rst2_TotalCheckout = 0;
                        decimal rst3_TotalRevenue = 0;
                        decimal rst3_TotalClick = 0;
                        decimal rst3_TotalAddcart = 0;
                        decimal rst3_TotalCheckout = 0;

                        foreach (var item in lstmainCates)
                        {
                            List<GoogleAnalyricsData.Metrics> lstMetrics =new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();
                            List<string> lstDimension = new List<string>();
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric1.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric2.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric3.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric4.ToString() });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric() { metric = lstMetrics });
                            lstDimension.Add(item.dim);
                            lstresult1.Add(new GoogleAnalyricsData.GA_Results() { dimension = lstDimension, metrics = lstGAMetrics });
                            rst1_TotalRevenue = item.metric1 + rst1_TotalRevenue;
                            rst1_TotalClick = item.metric2 + rst1_TotalClick;
                            rst1_TotalAddcart = item.metric3 + rst1_TotalAddcart;
                            rst1_TotalCheckout = item.metric4 + rst1_TotalCheckout;
                        }
                        foreach (var item in lstmediumCates)
                        {
                            List<GoogleAnalyricsData.Metrics> lstMetrics = new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();
                            List<string> lstDimension = new List<string>();
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric1.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric2.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric3.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric4.ToString() });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric() { metric = lstMetrics });
                            lstDimension.Add(item.dim);
                            lstresult2.Add(new GoogleAnalyricsData.GA_Results() { dimension = lstDimension, metrics = lstGAMetrics });
                            rst2_TotalRevenue = item.metric1 + rst2_TotalRevenue;
                            rst2_TotalClick = item.metric2 + rst2_TotalClick;
                            rst2_TotalAddcart = item.metric3 + rst2_TotalAddcart;
                            rst2_TotalCheckout = item.metric4 + rst2_TotalCheckout;
                        }
                        foreach (var item in lstbrand)
                        {
                            List<GoogleAnalyricsData.Metrics> lstMetrics = new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();
                            List<string> lstDimension = new List<string>();
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric1.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric2.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric3.ToString() });
                            lstMetrics.Add(new GoogleAnalyricsData.Metrics() { value = item.metric4.ToString() });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric() { metric = lstMetrics });
                            lstDimension.Add(item.dim);
                            lstresult3.Add(new GoogleAnalyricsData.GA_Results() { dimension = lstDimension, metrics = lstGAMetrics });
                            rst3_TotalRevenue = item.metric1 + rst1_TotalRevenue;
                            rst3_TotalClick = item.metric2 + rst1_TotalClick;
                            rst3_TotalAddcart = item.metric3 + rst1_TotalAddcart;
                            rst3_TotalCheckout = item.metric4 + rst1_TotalCheckout;
                        }
                        List<string> MetricHeaderEntries1 = new List<string>();
                        List<string> MetricHeaderEntries2 = new List<string>();
                        List<string> MetricHeaderEntries3 = new List<string>();
                        MetricHeaderEntries1.Add(rst1_TotalRevenue.ToString());
                        MetricHeaderEntries1.Add(rst1_TotalClick.ToString());
                        MetricHeaderEntries1.Add(rst1_TotalAddcart.ToString());
                        MetricHeaderEntries1.Add(rst1_TotalCheckout.ToString());
                        MetricHeaderEntries2.Add(rst2_TotalRevenue.ToString());
                        MetricHeaderEntries2.Add(rst2_TotalClick.ToString());
                        MetricHeaderEntries2.Add(rst2_TotalAddcart.ToString());
                        MetricHeaderEntries2.Add(rst2_TotalCheckout.ToString());
                        MetricHeaderEntries3.Add(rst3_TotalRevenue.ToString());
                        MetricHeaderEntries3.Add(rst3_TotalClick.ToString());
                        MetricHeaderEntries3.Add(rst3_TotalAddcart.ToString());
                        MetricHeaderEntries3.Add(rst3_TotalCheckout.ToString());
                        // metrichead 代用放置加總值
                        lstReports.Add(new GoogleAnalyricsData.GA_Reports() { ListResult = lstresult1,MetricHead=MetricHeaderEntries1 });
                        lstReports.Add(new GoogleAnalyricsData.GA_Reports() { ListResult = lstresult2,MetricHead=MetricHeaderEntries2 });
                        lstReports.Add(new GoogleAnalyricsData.GA_Reports() { ListResult = lstresult3,MetricHead=MetricHeaderEntries3 });


                    }
                    

                }
            }
            
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return lstReports;
            }



            
            return lstReports;
        }
        public bool SaveEventtoDB(string eventValue,string eventName,string Source, string StartDate, string EndDate)
        {
            bool saveOK = false;
            DateTime dt = DateTime.Now;
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string insertCommand = "insert into datastock(eventvalue,eventname,source,create_date,startdate,enddate) values(@eventvalue,@eventname,@source,@create_date,@startdate,@enddate)";
            if (!string.IsNullOrEmpty(eventName)&&!string.IsNullOrEmpty(eventValue)&&!string.IsNullOrEmpty(Source))
            {
                try
                {
                    int recordCount = 0;
                    string selectCommand = "select  * from datastock where source=@source and startdate=@startdate and enddate=@enddate and eventname=@eventname ";
                    using (var connection = new MySqlConnection(cnStr.Value))
                    {

                        recordCount = connection.ExecuteScalar<int>(selectCommand, new { startdate = StartDate, enddate = EndDate, source = Source, eventname = eventName });
                    }
                    if (recordCount == 0)
                    {
                        using (var connection = new MySqlConnection(cnStr.Value))
                        {
                            List<ETL.DataStock> dataStocks = new List<ETL.DataStock>();
                            dataStocks.Add(new ETL.DataStock()
                            {
                                eventvalue = eventValue,
                                eventname = eventName,
                                source = Source,
                                startdate = StartDate,
                                enddate = EndDate,
                                create_date = dt.ToLocalTime().ToString("yyyyMMddHHmmss")
                            });
                            connection.Open();
                            using (var trans = connection.BeginTransaction())
                            {
                                var insertResult = connection.Execute(insertCommand, dataStocks, trans);
                                trans.Commit();
                            }

                        }
                    }
                        
                    saveOK = true;
                }
                catch(Exception ex)
                {
                    _logger.LogError(ex.ToString());
                }
                
            }
            return saveOK;
        }
        public string LoadEventfromDB(string StartDate, string EndDate, string eventName, string Source)
        {
            string  rtnString  = string.Empty;

            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            List<ETL.DataStock> dataStocks = new List<ETL.DataStock>();
            string selectCommand = "select  * from datastock where source=@source and startdate=@startdate and enddate=@enddate and eventname=@eventname ";
            if (!string.IsNullOrEmpty(eventName) && !string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(Source) && !string.IsNullOrEmpty(EndDate))
            {
                try
                {
                    using (var connection = new MySqlConnection(cnStr.Value))
                    {

                         dataStocks = connection.Query<ETL.DataStock>(selectCommand, new { startdate = StartDate,enddate=EndDate,source=Source, eventname=eventName }).ToList();
                                                
                    }
                    if (dataStocks.Count>0)
                    {
                        rtnString = dataStocks[0].eventvalue;
                    }

                    _logger.LogDebug("dataStocks: " + dataStocks.ToString());
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.ToString());
                }

            }
            return rtnString;
        }

    }
}
