﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Globalization;
using System.Text;
using Microsoft.Extensions.Logging;
using Auro_CDPV1.Models;
using System.Net;
using System.Text.RegularExpressions;

namespace Auro_CDPV1.Helpers
{
    public class FtpHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<FtpHelpers> _logger { get; set; }
        public FtpHelpers(IConfiguration config, ILogger<FtpHelpers> logger)
        {

            _configuration = config;
            _logger = logger;
        }
        public bool HBFtpDownload()
        {
            FtpWebRequest reqFTP;
            var ftpServerIP = _configuration.GetSection("FtpConfiguration:ftp1:ip").Value;
            var ftpPath = _configuration.GetSection("FtpConfiguration:ftp1:path").Value;
            var ftpUserID = _configuration.GetSection("FtpConfiguration:ftp1:account").Value;
            var ftpPassword = _configuration.GetSection("FtpConfiguration:ftp1:password").Value;
            var filepath = _configuration.GetSection("FtpConfiguration:ftp1:filepath").Value;
            filepath = filepath.Replace("-", Path.DirectorySeparatorChar.ToString());
            try
            {

                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServerIP + "/"+ ftpPath));
                reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                string pattern = @".*(?<month>(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\s*(?<day>[0-9]*)\s*(?<yearTime>([0-9]|:)*)\s*(?<fileName>.*)";
                Regex regex = new Regex(pattern,RegexOptions.Compiled | RegexOptions.IgnoreCase);
                IFormatProvider culture = CultureInfo.GetCultureInfo("en-us");
                // reqFTP.KeepAlive = false;
                WebResponse response = reqFTP.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                
                string line = reader.ReadLine();
               
                List<string> downloadList = new List<string>();
                List<string> storLocalList = new List<string>();
                while (line != null)
                {

                    
                    
                    Match match = regex.Match(line);
                    string month = match.Groups["month"].Value;
                    string day = match.Groups["day"].Value;
                    string yearTime = match.Groups["yearTime"].Value;
                    string fileName = match.Groups["fileName"].Value;
                    string filedate = DateTime.Now.Year.ToString() + " " + month + " " + day;
                    string downloadFile = DateTime.Parse(filedate, culture).ToString("yyyyMMdd")+"~"+ fileName;
                    downloadList.Add(fileName);
                    storLocalList.Add(downloadFile);
                    line = reader.ReadLine();
                }    
                reader.Close();
                using (WebClient ftpClient = new WebClient())
                {
                    ftpClient.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

                    for (int i = 0; i <= downloadList.Count - 1; i++)
                    {
                        if (downloadList[i].Contains("."))
                        {

                            string path = "ftp://" + ftpServerIP + "/" + ftpPath+ "/" + downloadList[i].ToString().Trim();
                            string trnsfrpth = Path.GetFullPath(Path.Combine(filepath, storLocalList[i].ToString().Trim()));
                            ftpClient.DownloadFile(path, trnsfrpth);
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return false;
                //Response.Write("Download Error：" + ex.Message);
            }
            return true;


        }
        

    }
}
