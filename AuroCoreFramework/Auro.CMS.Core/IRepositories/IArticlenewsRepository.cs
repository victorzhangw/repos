using System;
using Auro.Commons.IRepositories;
using Auro.CMS.Models;

namespace Auro.CMS.IRepositories
{
    /// <summary>
    /// 定義文章倉儲介面
    /// </summary>
    public interface IArticlenewsRepository:IRepository<Articlenews, string>
    {
    }
}