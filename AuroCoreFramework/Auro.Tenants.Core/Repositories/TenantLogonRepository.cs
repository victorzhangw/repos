using Dapper;
using System;
using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Tenants.IRepositories;
using Auro.Tenants.Models;

namespace Auro.Tenants.Repositories
{
    /// <summary>
    /// 使用者登錄資訊倉儲介面的實現
    /// </summary>
    public class TenantLogonRepository : BaseRepository<TenantLogon, string>, ITenantLogonRepository
    {
		public TenantLogonRepository()
        {
        }

        public TenantLogonRepository(IDbContextCore context) : base(context)
        {

        }

        #region Dapper 操作

        //DapperConn 用於讀寫操作
        //DapperConnRead 用於只讀操作

        /// <summary>
        /// 根據租戶ID獲取租戶登錄資訊實體
        /// </summary>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        public TenantLogon GetByTenantId(string tenantId)
        {
            string sql = $"SELECT * FROM {this.tableName} t WHERE t.TenantId = @TenantId";
            return DapperConn.QueryFirst<TenantLogon>(sql, new { @TenantId = tenantId });
        }
        #endregion


        #region EF 操作

        //DbContext 用於讀寫操作
        //DbContextRead 用於只讀操作

        #endregion
    }
}