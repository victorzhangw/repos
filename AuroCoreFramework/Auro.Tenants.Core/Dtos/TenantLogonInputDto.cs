using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Models;
using Auro.Commons.Dtos;
using Auro.Tenants.Models;

namespace Auro.Tenants.Dtos
{
    /// <summary>
    /// 使用者登錄資訊輸入物件模型
    /// </summary>
    [AutoMap(typeof(TenantLogon))]
    [Serializable]
    public class TenantLogonInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// 設定或獲取密碼
        /// </summary>
        public string TenantPassword { get; set; }

        /// <summary>
        /// 設定或獲取加密金鑰
        /// </summary>
        public string TenantSecretkey { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? AllowStartTime { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? AllowEndTime { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? LockStartDate { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? LockEndDate { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? FirstVisitTime { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? PreviousVisitTime { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? LastVisitTime { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public DateTime? ChangePasswordDate { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public bool? MultiUserLogin { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public int? LogOnCount { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public bool? TenantOnLine { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public string AnswerQuestion { get; set; }

        /// <summary>
        /// 設定或獲取
        /// </summary>
        public bool? CheckIPAddress { get; set; }

        /// <summary>
        /// 設定或獲取軟體語言
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// 設定或獲取軟體風格設定資訊
        /// </summary>
        public string Theme { get; set; }


    }
}
