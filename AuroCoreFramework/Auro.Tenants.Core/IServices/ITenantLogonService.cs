using System;
using Auro.Commons.IServices;
using Auro.Tenants.Dtos;
using Auro.Tenants.Models;

namespace Auro.Tenants.IServices
{
    /// <summary>
    /// 定義使用者登錄資訊服務介面
    /// </summary>
    public interface ITenantLogonService:IService<TenantLogon,TenantLogonOutputDto, string>
    {

    }
}
