using System;
using Auro.Commons.IRepositories;
using Auro.Tenants.Models;

namespace Auro.Tenants.IRepositories
{
    /// <summary>
    /// 定義使用者登錄資訊倉儲介面
    /// </summary>
    public interface ITenantLogonRepository:IRepository<TenantLogon, string>
    {
        /// <summary>
        /// 根據租戶ID獲取租戶登錄資訊實體
        /// </summary>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        TenantLogon GetByTenantId(string tenantId);
    }
}