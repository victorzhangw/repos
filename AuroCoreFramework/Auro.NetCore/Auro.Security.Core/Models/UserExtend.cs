
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Auro.Commons.Models;

namespace Auro.Security.Models
{
    /// <summary>
    /// 使用者表，資料實體物件
    /// </summary>
    [Table("Sys_UserExtend")]
    [Serializable]
    public class UserExtend : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    { 
        /// <summary>
        /// 預設建構函式（需要初始化屬性的在此處理）
        /// </summary>
	    public UserExtend()
        {
        }

        #region Property Members
        /// <summary>
        /// 帳戶
        /// </summary>
        public virtual string UserId { get; set; }

        /// <summary>
        /// 名片
        /// </summary>
        public virtual string CardContent { get; set; }

        /// <summary>
        /// 手機
        /// </summary>
        public virtual string Telphone { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        public virtual string Address { get; set; }

        /// <summary>
        /// 公司名稱
        /// </summary>
        public virtual string CompanyName { get; set; }

        /// <summary>
        /// 職位
        /// </summary>
        public virtual string PositionTitle { get; set; }

        

        /// <summary>
        /// 地區
        /// </summary>
        public virtual string IndustryId { get; set; }

        /// <summary>
        /// 公開情況
        /// </summary>
        public virtual int OpenType { get; set; }

        /// <summary>
        /// IsOtherShare
        /// </summary>
        [DataMember]
        public virtual bool IsOtherShare { get; set; }

        /// <summary>
        /// sharetitle
        /// </summary>
        [DataMember]
        public virtual string ShareTitle { get; set; }
        /// <summary>
        /// 主頁
        /// </summary>
        [DataMember]
        public virtual string WebUrl { get; set; }
        /// <summary>
        /// 公司簡介
        /// </summary>
        [DataMember]
        public virtual string CompanyDesc { get; set; }
        /// <summary>
        /// 主題
        /// </summary>
        public virtual string CardTheme { get; set; }

        /// <summary>
        /// VIP
        /// </summary>
        public virtual string VipGrade { get; set; }

        /// <summary>
        /// 是否認證
        /// </summary>
        public virtual bool IsAuthentication { get; set; }

        /// <summary>
        /// 認證型別
        /// </summary>
        public virtual int AuthenticationType { get; set; }        

        /// <summary>
        /// 刪除標誌
        /// </summary>
        public virtual bool? DeleteMark { get; set; }

        /// <summary>
        /// 有效標誌
        /// </summary>
        public virtual bool EnabledMark { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 建立使用者主鍵
        /// </summary>
        [MaxLength(50)]
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 公司id
        /// </summary>
        [MaxLength(50)]
        public virtual string CompanyId { get; set; }

        /// <summary>
        /// 部門id
        /// </summary>
        [MaxLength(50)]
        public virtual string DeptId { get; set; }

        /// <summary>
        /// 最後修改時間
        /// </summary>
        public virtual DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 最後修改使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string LastModifyUserId { get; set; }

        /// <summary>
        /// 刪除時間
        /// </summary>
        public virtual DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string DeleteUserId { get; set; }
        #endregion

    }
}