using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Auro.Commons.Options;

namespace Auro.Security.Models
{
    /// <summary>
    /// 系統配置
    /// </summary>
    [Serializable]
    public class SysSetting: AppSetting
    {
    }
}
