using System;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    /// <summary>
    /// 定義單據編碼倉儲介面
    /// </summary>
    public interface ISequenceRepository:IRepository<Sequence, string>
    {
    }
}