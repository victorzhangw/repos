using System;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    /// <summary>
    /// 定義序號編碼規則表倉儲介面
    /// </summary>
    public interface ISequenceRuleRepository:IRepository<SequenceRule, string>
    {
    }
}