using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface IRoleAuthorizeRepository:IRepository<RoleAuthorize,string>
    {
        /// <summary>
        /// 儲存角色授權
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <param name="roleAuthorizesList">角色功能模組</param>
        /// <param name="roleDataList">角色可訪問資料</param>
        /// <param name="trans"></param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        Task<bool> SaveRoleAuthorize(string roleId,List<RoleAuthorize> roleAuthorizesList, List<RoleData> roleDataList,
           IDbTransaction trans = null);
    }
}