using System;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface IDbBackupRepository:IRepository<DbBackup, string>
    {
    }
}