using System;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface IUserLogOnRepository:IRepository<UserLogOn, string>
    {
        /// <summary>
        /// 根據會員ID獲取使用者登錄資訊實體
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserLogOn GetByUserId(string userId);
    }
}