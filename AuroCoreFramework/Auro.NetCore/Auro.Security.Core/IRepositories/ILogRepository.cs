using System;
using System.Threading.Tasks;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface ILogRepository:IRepository<Log, string>
    {
        long InsertTset(int len);
    }
}