using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface ISystemTypeRepository : IRepository<SystemType, string>
    {
        /// <summary>
        /// 根據系統編碼查詢系統物件
        /// </summary>
        /// <param name="appkey">系統編碼</param>
        /// <returns></returns>
        SystemType GetByCode(string appkey);
    }
}
