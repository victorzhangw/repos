using System;
using System.Collections.Generic;
using Auro.Commons.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.IRepositories
{
    public interface IMenuRepository:IRepository<Menu, string>
    {

        /// <summary>
        /// 根據角色ID字串（逗號分開)和系統型別ID，獲取對應的操作功能列表
        /// </summary>
        /// <param name="roleIds">角色ID</param>
        /// <param name="typeID">系統型別ID</param>
        /// <param name="isMenu">是否是菜單</param>
        /// <returns></returns>
        IEnumerable<Menu> GetFunctions(string roleIds, string typeID, bool isMenu = false);

        /// <summary>
        /// 根據系統型別ID，獲取對應的操作功能列表
        /// </summary>
        /// <param name="typeID">系統型別ID</param>
        /// <returns></returns>
        IEnumerable<Menu> GetFunctions(string typeID);
    }
}