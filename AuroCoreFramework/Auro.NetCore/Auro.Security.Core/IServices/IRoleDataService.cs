using System;
using System.Collections.Generic;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    public interface IRoleDataService:IService<RoleData, RoleDataOutputDto, string>
    {

        /// <summary>
        /// 根據角色返回授權訪問部門資料
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        List<string> GetListDeptByRole(string roleIds);
    }
}
