using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 資料字典明細
    /// </summary>
    public interface IItemsDetailService:IService<ItemsDetail, ItemsDetailOutputDto, string>
    {
        /// <summary>
        /// 根據資料字典分類編碼獲取該分類可用內容
        /// </summary>
        /// <param name="itemCode">分類編碼</param>
        /// <returns></returns>
        Task<List<ItemsDetailOutputDto>> GetItemDetailsByItemCode(string itemCode);

        /// <summary>
        /// 獲取適用於Vue 樹形列表
        /// </summary>
        /// <param name="itemId">類別Id</param>
        /// <returns></returns>
       Task<List<ItemsDetailOutputDto>> GetAllItemsDetailTreeTable(string itemId);
    }
}
