using System;
using System.Collections.Generic;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAPPService:IService<APP, AppOutputDto, string>
    {
        /// <summary>
        /// 獲取app物件
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <param name="secret">應用金鑰AppSecret</param>
        /// <returns></returns>
        APP GetAPP(string appid, string secret);

        /// <summary>
        /// 獲取app物件
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <returns></returns>
        APP GetAPP(string appid);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppOutputDto> SelectApp();
        /// <summary>
        /// 更新可用的應用到快取
        /// </summary>
        void  UpdateCacheAllowApp();
    }
}
