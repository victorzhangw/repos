using System;
using System.Collections.Generic;
using Auro.Commons.IServices;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAreaService:IService<Area, AreaOutputDto, string>
    {

        #region 用於uniapp下拉選項
        /// <summary>
        /// 獲取所有可用的地區，用於uniapp下拉選項
        /// </summary>
        /// <returns></returns>
        List<AreaPickerOutputDto> GetAllByEnable();
        /// <summary>
        /// 獲取省、市、縣/區三級可用的地區，用於uniapp下拉選項
        /// </summary>
        /// <returns></returns>
        List<AreaPickerOutputDto> GetProvinceToAreaByEnable();
        #endregion
    }
}
