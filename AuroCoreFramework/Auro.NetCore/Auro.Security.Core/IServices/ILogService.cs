using System;
using System.Threading.Tasks;
using Auro.Commons.IServices;
using Auro.Commons.Pages;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Security.IServices
{
    /// <summary>
    /// 日誌記錄
    /// </summary>
    public interface ILogService:IService<Log, LogOutputDto, string>
    {
        /// <summary>
        /// 根據相關資訊，寫入使用者的操作日誌記錄
        /// 主要用於寫資料庫日誌
        /// </summary>
        /// <param name="tableName">操作表名稱</param>
        /// <param name="operationType">操作型別</param>
        /// <param name="note">操作詳細表述</param>
        /// <returns></returns>
         bool OnOperationLog(string tableName, string operationType, string note);

        /// <summary>
        /// 根據相關資訊，寫入使用者的操作日誌記錄
        /// 主要用於寫操作模組日誌
        /// </summary>
        /// <param name="module">操作模組名稱</param>
        /// <param name="operationType">操作型別</param>
        /// <param name="note">操作詳細表述</param>
        /// <param name="currentUser">操作使用者</param>
        /// <returns></returns>
        bool OnOperationLog(string module, string operationType,  string note, AuroCurrentUser currentUser);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定對象的集合</returns>
        Task<PageResult<LogOutputDto>> FindWithPagerSearchAsync(SearchLogModel search);
    }
}
