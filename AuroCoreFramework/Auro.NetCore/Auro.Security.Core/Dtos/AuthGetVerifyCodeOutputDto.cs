using System;
using System.Collections.Generic;
using System.Text;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 驗證碼輸出實體
    /// </summary>
    public class AuthGetVerifyCodeOutputDto
    {
        /// <summary>
        /// 快取鍵
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 圖片
        /// </summary>
        public string Img { get; set; }
    }
}
