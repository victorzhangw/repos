using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{

    /// <summary>
    /// 使用者搜索條件
    /// </summary>
    public class SearchUserModel : SearchInputDto<User>
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public string RoleId
        {
            get; set;
        }
        /// <summary>
        /// 註冊或新增時間 開始
        /// </summary>
        public string CreatorTime1
        {
            get; set;
        }
        /// <summary>
        /// 註冊或新增時間 結束
        /// </summary>
        public string CreatorTime2
        {
            get; set;
        }

    }
}
