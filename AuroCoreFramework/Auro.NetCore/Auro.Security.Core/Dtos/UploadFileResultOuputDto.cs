using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Commons.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 上傳結束輸出檔案物件
    /// </summary>
    [Serializable]
    public class UploadFileResultOuputDto : IOutputDto
    {
        /// <summary>
	    ///
	    /// </summary>
        public string Id { get; set; }
        /// <summary>
	    /// 檔名稱
	    /// </summary>
        public string FileName { get; set; }
        /// <summary>
	    /// 檔案路徑物理路徑
	    /// </summary>
        public string PhysicsFilePath { get; set; }
        /// <summary>
	    /// 縮圖
	    /// </summary>
        public string Thumbnail { get; set; }
        /// <summary>
	    /// 檔案路徑
	    /// </summary>
        public string FilePath { get; set; }
       
        /// <summary>
	    /// 檔案大小
	    /// </summary>
        public long FileSize { get; set; }
        /// <summary>
	    /// 檔案型別
	    /// </summary>
        public string FileType { get; set; }
        

    }
}
