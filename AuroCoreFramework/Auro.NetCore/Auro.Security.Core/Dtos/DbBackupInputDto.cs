using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Commons.Models;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(DbBackup))]
    [Serializable]
    public class DbBackupInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BackupType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FileSize { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? BackupTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }


    }
}
