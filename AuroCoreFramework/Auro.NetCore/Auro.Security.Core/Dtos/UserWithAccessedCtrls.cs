using System;
using System.Collections.Generic;
using System.Text;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 包括使用者及使用者可訪問的機構/資源/模組/角色
    /// </summary>
    [Serializable]
    public class UserWithAccessedCtrls
    {

        public UserLoginDto User { get; set; }
        /// <summary>
        /// 使用者可以訪問到的模組（包括所屬角色與自己的所有模組）
        /// </summary>
        public List<MenuOutputDto> Modules { get; set; }

        /// <summary>
        /// 使用者可以訪問的資源
        /// </summary>
        public List<RoleData> RoleDatas { get; set; }

        /// <summary>
        ///  使用者所屬機構
        /// </summary>
        public List<Organize> Organizes { get; set; }

        /// <summary>
        /// 使用者所屬角色
        /// </summary>
        public List<Role> Roles { get; set; }
    }
}
