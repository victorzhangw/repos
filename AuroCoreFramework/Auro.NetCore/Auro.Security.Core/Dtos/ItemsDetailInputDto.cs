using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Commons.Models;
using Auro.Security.Models;

namespace Auro.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(ItemsDetail))]
    [Serializable]
    public class ItemsDetailInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ItemId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ItemName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SimpleSpelling { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? IsDefault { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }


    }
}
