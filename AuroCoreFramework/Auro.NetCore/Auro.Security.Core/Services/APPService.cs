using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Auro.Commons.Cache;
using Auro.Commons.Dtos;
using Auro.Commons.Mapping;
using Auro.Commons.Pages;
using Auro.Commons.Services;
using Auro.Security.Dtos;
using Auro.Security.IRepositories;
using Auro.Security.IServices;
using Auro.Security.Models;

namespace Auro.Security.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class APPService: BaseService<APP,AppOutputDto,string>, IAPPService
    {
        private readonly IAPPRepository _appRepository;
        private readonly ILogService _logService;
        public APPService(IAPPRepository repository, ILogService logService) : base(repository)
        {
            _appRepository = repository;
            _logService = logService;
        }

        /// <summary>
        /// 同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public override long Insert(APP entity, IDbTransaction trans = null)
        {
            long result = repository.Insert(entity, trans); 
            this.UpdateCacheAllowApp();
            return result;
        }

        /// <summary>
        /// 非同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public override async Task<bool> UpdateAsync(APP entity, string id, IDbTransaction trans = null)
        {
            bool result=await repository.UpdateAsync(entity, id, trans);
            this.UpdateCacheAllowApp();
            return result;
        }
        /// <summary>
        /// 非同步步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public override async Task<long> InsertAsync(APP entity, IDbTransaction trans = null)
        {
            long result = await repository.InsertAsync(entity, trans);
            this.UpdateCacheAllowApp();
            return result;
        }
        /// <summary>
        /// 獲取app物件
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <param name="secret">應用金鑰AppSecret</param>
        /// <returns></returns>
        public APP GetAPP(string appid, string secret)
        {
            return _appRepository.GetAPP(appid, secret);
        }
        /// <summary>
        /// 獲取app物件
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <returns></returns>
        public APP GetAPP(string appid)
        {
            return _appRepository.GetAPP(appid);
        }
        public IList<AppOutputDto> SelectApp()
        {
            return _appRepository.SelectApp();
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定對象的集合</returns>
        public override async Task<PageResult<AppOutputDto>> FindWithPagerAsync(SearchInputDto<APP> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege(false);
            if (!string.IsNullOrEmpty(search.Keywords))
            {
                where += string.Format(" and (AppId like '%{0}%' or RequestUrl like '%{0}%')", search.Keywords);
            };
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<APP> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
            PageResult<AppOutputDto> pageResult = new PageResult<AppOutputDto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<AppOutputDto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }
        public void UpdateCacheAllowApp()
        {
            AuroCacheHelper AuroCacheHelper = new AuroCacheHelper();
            IEnumerable<APP> appList = repository.GetAllByIsNotDeleteAndEnabledMark();
            AuroCacheHelper.Add("AllowAppId", appList);
        }
    }
}