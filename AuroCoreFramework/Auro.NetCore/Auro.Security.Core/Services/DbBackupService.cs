using System;
using Auro.Commons.Services;
using Auro.Security.Dtos;
using Auro.Security.IRepositories;
using Auro.Security.IServices;
using Auro.Security.Models;

namespace Auro.Security.Services
{
    public class DbBackupService: BaseService<DbBackup, DbBackupOutputDto, string>, IDbBackupService
    {
        private readonly IDbBackupRepository _repository;
        private readonly ILogService _logService;
        public DbBackupService(IDbBackupRepository repository, ILogService logService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
        }
    }
}