using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Security.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.Repositories
{
    public class RoleRepository : BaseRepository<Role, string>, IRoleRepository
    {
        public RoleRepository()
        {
        }

        public RoleRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}