using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using Auro.Security.IRepositories;
using Auro.Security.Models;

namespace Auro.Security.Repositories
{
    public class AreaRepository : BaseRepository<Area, string>, IAreaRepository
    {
        public AreaRepository()
        {
        }

        public AreaRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}