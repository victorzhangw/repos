using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Models;
using Auro.Commons.Dtos;
using {ModelsNamespace};

namespace {DtosNamespace}
{
    /// <summary>
    /// {TableNameDesc}輸入物件模型
    /// </summary>
    [AutoMap(typeof({ModelTypeName}))]
    [Serializable]
    public class {ModelTypeName}InputDto: IInputDto<{KeyTypeName}>
    {
{ModelContent}
    }
}
