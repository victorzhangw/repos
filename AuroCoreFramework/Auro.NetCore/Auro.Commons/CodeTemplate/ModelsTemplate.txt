using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Auro.Commons.Models;

namespace {ModelsNamespace}
{
    /// <summary>
    /// {TableNameDesc}，資料實體物件
    /// </summary>
    [Table("{TableName}")]
    [Serializable]
    public class {ModelTypeName}:BaseEntity<{KeyTypeName}>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
{ModelContent}
    }
}
