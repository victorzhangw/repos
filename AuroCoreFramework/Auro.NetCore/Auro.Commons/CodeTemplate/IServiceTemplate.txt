using System;
using Auro.Commons.IServices;
using {DtosNamespace};
using {ModelsNamespace};

namespace {IServicsNamespace}
{
    /// <summary>
    /// 定義{TableNameDesc}服務介面
    /// </summary>
    public interface I{ModelTypeName}Service:IService<{ModelTypeName},{ModelTypeName}OutputDto, {KeyTypeName}>
    {
    }
}
