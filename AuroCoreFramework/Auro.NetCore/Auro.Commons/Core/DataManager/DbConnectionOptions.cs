using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auro.Commons.Enums;

namespace Auro.Commons.Core.DataManager
{
    /// <summary>
    /// 定義主資料和從資料庫配置選項
    /// </summary>
    public class DbConnections
    {
        /// <summary>
        /// 主資料庫
        /// </summary>
        public DbConnectionOptions MassterDB {get;set; }

        /// <summary>
        /// 從資料庫
        /// </summary>
        public List<DbConnectionOptions> ReadDB { get; set; }
    }


    /// <summary>
    /// 資料庫配置選項,定義資料庫連線字串、資料庫型別和訪問權重
    /// </summary>
    public class DbConnectionOptions
    {
        /// <summary>
        /// 資料庫連線字元
        /// </summary>
        public string ConnectionString { get;set; }

        /// <summary>
        /// 資料庫型別
        /// </summary>
        public DatabaseType DatabaseType { get; set; }

        /// <summary>
        /// 訪問權重，值越大權重越低
        /// </summary>
        public int DbLevel { get; set; }
    }
}
