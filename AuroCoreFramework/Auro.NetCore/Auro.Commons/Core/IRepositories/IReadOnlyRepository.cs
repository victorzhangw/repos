using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auro.Commons.Models;

namespace Auro.Commons.Core.IRepositories
{
    /// <summary>
    /// 只讀倉儲介面,提供查詢相關方法
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IReadOnlyRepository<T, TKey> : IDisposable where T : Entity
    {

    }
}
