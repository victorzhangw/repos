using System;

namespace Auro.Commons.Models
{
    /// <summary>
    ///  定義邏輯刪除功能審計資訊
    /// </summary>
    public interface IDeleteAudited 
    {
        /// <summary>
        /// 獲取或設定 邏輯刪除標記
        /// </summary>
        bool? DeleteMark { get; set; }

        /// <summary>
        /// 獲取或設定 刪除實體的使用者
        /// </summary>
        string DeleteUserId { get; set; }

        /// <summary>
        /// 獲取或設定 刪除實體時間
        /// </summary>
        DateTime? DeleteTime { get; set; } 
    }
}