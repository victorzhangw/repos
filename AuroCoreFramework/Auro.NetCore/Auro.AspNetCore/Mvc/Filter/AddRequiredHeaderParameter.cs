using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
namespace Auro.AspNetCore.Mvc.Filter
{
    /// <summary>
    /// 向swagger新增header參數
    /// </summary>
    public class AddRequiredHeaderParameter : IOperationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {

            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "sign",
                In = ParameterLocation.Header,
                Description = "是否啟用簽名",
                Required = true,
                AllowEmptyValue = false
            });
        }
    }
}
