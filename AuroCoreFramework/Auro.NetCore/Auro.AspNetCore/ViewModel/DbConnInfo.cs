using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Auro.AspNetCore.ViewModel
{
    /// <summary>
    /// 資料庫連線字串實體
    /// </summary>
    [Serializable]
    public class DbConnInfo
    {
        /// <summary>
        /// 訪問地址
        /// </summary>
        [DataMember]
        public string DbAddress { get; set; }
        /// <summary>
        /// 埠，預設SQLServer為1433；Mysql為3306
        /// </summary>
        [DataMember]
        public int DbPort { get; set; }
        /// <summary>
        /// 資料庫名稱
        /// </summary>
        [DataMember]
        public string DbName { get; set; }
        /// <summary>
        /// 使用者名稱
        /// </summary>
        [DataMember]
        public string DbUserName { get; set; }
        /// <summary>
        /// 訪問密碼
        /// </summary>
        [DataMember]
        public string DbPassword { get; set; }
        /// <summary>
        /// 資料庫型別
        /// </summary>
        [DataMember]
        public string DbType { get; set; }

    }
}
