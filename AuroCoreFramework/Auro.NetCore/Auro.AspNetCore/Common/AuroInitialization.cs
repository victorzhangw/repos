using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Auro.Commons.Cache;
using Auro.Commons.Helpers;
using Auro.Security.Models;

namespace Auro.AspNetCore.Common
{
    /// <summary>
    /// 系統初始化內容
    /// </summary>
    public abstract class AuroInitialization
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual  void Initial()
        {
            AuroCacheHelper AuroCacheHelper = new AuroCacheHelper();
            SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            if (sysSetting != null)
            {
                AuroCacheHelper.Add("SysSetting", sysSetting);
            }
       }
    }
}
