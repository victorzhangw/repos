using System;
using Auro.Commons.IRepositories;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.IRepositories
{
    /// <summary>
    /// 定義倉儲介面
    /// </summary>
    public interface IMemberSubscribeMsgRepository:IRepository<MemberSubscribeMsg, string>
    {

        /// <summary>
        /// 根據訊息型別查詢訊息模板
        /// </summary>
        /// <param name="messageType">訊息型別</param>
        /// <param name="userId">使用者Id</param>
        /// <returns></returns>
        MemberMessageTemplatesOuputDto GetByMessageTypeWithUser(string messageType, string userId);

        /// <summary>
        /// 按使用者、訂閱型別和訊息模板主鍵查詢
        /// </summary>
        /// <param name="subscribeType">訊息型別</param>
        /// <param name="userId">使用者</param>
        /// <param name="messageTemplateId">模板Id主鍵</param>
        /// <returns></returns>
        MemberMessageTemplatesOuputDto GetByWithUser(string subscribeType, string userId, string messageTemplateId);
    }
}