using System;
using Auro.Commons.IRepositories;
using Auro.Messages.Models;

namespace Auro.Messages.IRepositories
{
    /// <summary>
    /// 定義倉儲介面
    /// </summary>
    public interface IMessageMailBoxRepository:IRepository<MessageMailBox, string>
    {
    }
}