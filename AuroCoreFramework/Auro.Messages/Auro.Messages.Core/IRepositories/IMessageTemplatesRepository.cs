using System;
using System.Collections.Generic;
using Auro.Commons.IRepositories;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.IRepositories
{
    /// <summary>
    /// 定義倉儲介面
    /// </summary>
    public interface IMessageTemplatesRepository:IRepository<MessageTemplates, string>
    {

        /// <summary>
        /// 根據使用者查詢微信小程式訂閱訊息模板列表，關聯使用者訂閱表
        /// </summary>
        /// <param name="userId">使用者編號</param>
        /// <returns></returns>
        List<MemberMessageTemplatesOuputDto> ListByUseInWxApplet(string userId);
    }
}