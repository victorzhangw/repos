using System;
using Auro.Commons.Services;
using Auro.Security.IServices;
using Auro.Messages.IRepositories;
using Auro.Messages.IServices;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.Services
{
    /// <summary>
    /// 服務介面實現
    /// </summary>
    public class MemberMessageBoxService: BaseService<MemberMessageBox,MemberMessageBoxOutputDto, string>, IMemberMessageBoxService
    {
		private readonly IMemberMessageBoxRepository _repository;
        private readonly ILogService _logService;
        public MemberMessageBoxService(IMemberMessageBoxRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }

        public int GetTotalCounts(int isread, string userid)
        {
            return _repository.GetTotalCounts(isread,userid);
        }

        public bool UpdateIsReadStatus(string id, int isread, string userid)
        {
            return _repository.UpdateIsReadStatus(id, isread, userid);
        }
    }
}