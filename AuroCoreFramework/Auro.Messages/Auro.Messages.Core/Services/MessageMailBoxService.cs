using System;
using Auro.Commons.Services;
using Auro.Security.IServices;
using Auro.Messages.IRepositories;
using Auro.Messages.IServices;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.Services
{
    /// <summary>
    /// 服務介面實現
    /// </summary>
    public class MessageMailBoxService: BaseService<MessageMailBox,MessageMailBoxOutputDto, string>, IMessageMailBoxService
    {
		private readonly IMessageMailBoxRepository _repository;
        private readonly ILogService _logService;
        public MessageMailBoxService(IMessageMailBoxRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }
    }
}