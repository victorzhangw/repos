using System;
using Auro.Commons.IServices;
using Auro.Messages.Dtos;
using Auro.Messages.Models;

namespace Auro.Messages.IServices
{
    /// <summary>
    /// 定義服務介面
    /// </summary>
    public interface IMemberSubscribeMsgService:IService<MemberSubscribeMsg,MemberSubscribeMsgOutputDto, string>
    {

        /// <summary>
        /// 根據訊息型別查詢訊息模板
        /// </summary>
        /// <param name="messageType">訊息型別</param>
        /// <param name="userId">使用者Id</param>
        /// <returns></returns>
        MemberMessageTemplatesOuputDto GetByMessageTypeWithUser(string messageType, string userId);

        /// <summary>
        /// 按使用者、訂閱型別和訊息模板主鍵查詢
        /// </summary>
        /// <param name="subscribeType">訊息型別</param>
        /// <param name="userId">使用者</param>
        /// <param name="messageTemplateId">模板Id主鍵</param>
        /// <returns></returns>
        MemberMessageTemplatesOuputDto GetByWithUser(string subscribeType, string userId, string messageTemplateId);


        /// <summary>
        /// 根據訊息模板Id（主鍵）查詢使用者訂閱訊息
        /// </summary>
        /// <param name="messageTemplateId">訊息模板主鍵</param>
        /// <param name="userId">使用者</param>
        /// <param name="subscribeType">訊息型別</param>
        /// <returns></returns>
        MemberSubscribeMsg GetByMessageTemplateIdAndUser(string messageTemplateId, string userId, string subscribeType);
        /// <summary>
        /// 更新使用者訂閱訊息
        /// </summary>
        /// <param name="messageTemplateId">訊息模板主鍵</param>
        /// <param name="userId">使用者</param>
        /// <param name="subscribeType">訊息型別</param>
        /// <param name="subscribeStatus">訂閱狀態</param>
        /// <returns></returns>
        bool UpdateByMessageTemplateIdAndUser(string messageTemplateId, string userId, string subscribeType, string subscribeStatus);

        long Insert(MemberSubscribeMsg info);
        /// <summary>
        /// 更新訂閱狀態
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        bool UpdateSubscribeStatus(MemberSubscribeMsg info);
    }
}
