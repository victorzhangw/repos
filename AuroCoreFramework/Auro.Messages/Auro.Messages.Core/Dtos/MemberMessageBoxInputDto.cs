using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Auro.Commons.Dtos;
using Auro.Commons.Models;
using Auro.Messages.Models;

namespace Auro.Messages.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(MemberMessageBox))]
    [Serializable]
    public class MemberMessageBoxInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取訊息內容Id
        /// </summary>
        public long? ContentId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MsgContent { get; set; }

        /// <summary>
        /// 設定或獲取發送者
        /// </summary>
        public string Sernder { get; set; }

        /// <summary>
        /// 設定或獲取接受者
        /// </summary>
        public string Accepter { get; set; }

        /// <summary>
        /// 設定或獲取是否已讀
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? ReadDate { get; set; }


    }
}
