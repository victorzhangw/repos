using Dapper;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Auro.Commons.Linq;
using Auro.Commons.Repositories;
using Auro.Messages.Dtos;
using Auro.Messages.IRepositories;
using Auro.Messages.Models;

namespace Auro.Messages.Repositories
{
    /// <summary>
    /// 倉儲介面的實現
    /// </summary>
    public class MessageTemplatesRepository : BaseRepository<MessageTemplates, string>, IMessageTemplatesRepository
    {
		public MessageTemplatesRepository()
        {
            this.tableName = "Sys_MessageTemplates";
            this.primaryKey = "Id";
        }

        /// <summary>
        /// 根據使用者查詢微信小程式訂閱訊息模板列表，關聯使用者訂閱表
        /// </summary>
        /// <param name="userId">使用者編號</param>
        /// <returns></returns>
        public List<MemberMessageTemplatesOuputDto> ListByUseInWxApplet(string userId)
        {
            string sqlStr = @"select a.*,b.Id as MemberSubscribeMsgId,b.SubscribeStatus as SubscribeStatus  from Sys_MessageTemplates as a 
LEFT join Sys_MemberSubscribeMsg as b on a.Id = b.MessageTemplateId and a.UseInWxApplet =1 and b.SubscribeUserId='" + userId + "'  where  a.WxAppletSubscribeTemplateId is not null";

            return DapperConn.Query<MemberMessageTemplatesOuputDto>(sqlStr).AsToList();
        }
    }
}