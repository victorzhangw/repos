using Dapper;
using System;
using System.Data.Common;
using Auro.Commons.Repositories;
using Auro.Messages.Dtos;
using Auro.Messages.IRepositories;
using Auro.Messages.Models;

namespace Auro.Messages.Repositories
{
    /// <summary>
    /// 倉儲介面的實現
    /// </summary>
    public class MemberSubscribeMsgRepository : BaseRepository<MemberSubscribeMsg, string>, IMemberSubscribeMsgRepository
    {
		public MemberSubscribeMsgRepository()
        {
            this.tableName = "Sys_MemberSubscribeMsg";
            this.primaryKey = "Id";
        }


        /// <summary>
        /// 根據訊息型別查詢訊息模板
        /// </summary>
        /// <param name="messageType">訊息型別</param>
        /// <param name="userId">使用者Id</param>
        /// <returns></returns>
        public MemberMessageTemplatesOuputDto GetByMessageTypeWithUser(string messageType, string userId)
        {
            string sqlStr = @"select a.*,b.Id as MemberSubscribeMsgId,b.SubscribeStatus as SubscribeStatus,b.SubscribeType as SubscribeType  from Sys_MessageTemplates as a 
LEFT join Sys_MemberSubscribeMsg as b on a.Id = b.MessageTemplateId where a.UseInWxApplet =1 and a.WxAppletSubscribeTemplateId is not null and a.messageType = '" + messageType + "' and b.SubscribeUserId='" + userId + "'";
            return DapperConn.QueryFirstOrDefault<MemberMessageTemplatesOuputDto>(sqlStr);
        }
        /// <summary>
        /// 按使用者、訂閱型別和訊息模板主鍵查詢
        /// </summary>
        /// <param name="subscribeType">訊息型別</param>
        /// <param name="userId">使用者</param>
        /// <param name="messageTemplateId">模板Id主鍵</param>
        /// <returns></returns>
        public MemberMessageTemplatesOuputDto GetByWithUser(string subscribeType, string userId, string messageTemplateId)
        {
            string sqlStr = @"select * from [dbo].[Sys_MemberSubscribeMsg]   where SubscribeUserId = '" + userId + "' and SubscribeType='" + subscribeType + "' and MessageTemplateId='" + messageTemplateId + "'";
            return DapperConn.QueryFirstOrDefault<MemberMessageTemplatesOuputDto>(sqlStr);
        }
    }
}