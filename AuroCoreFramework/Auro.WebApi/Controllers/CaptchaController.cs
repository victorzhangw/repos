using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.AspNetCore.Mvc.Filter;
using Auro.Commons.Cache;
using Auro.Commons.Models;
using Auro.Commons.Net;
using Auro.Commons.VerificationCode;
using Auro.Security.Dtos;

namespace Auro.WebApi.Controllers
{
    /// <summary>
    /// 驗證碼介面
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class CaptchaController : ApiController
    {
        /// <summary>
        /// 獲取驗證碼
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [NoPermissionRequired]
        public async Task<CommonResult<AuthGetVerifyCodeOutputDto>> CaptchaAsync()
        {
            Captcha captcha = new Captcha();
            var code =await  captcha.GenerateRandomCaptchaAsync().ConfigureAwait(false);
            var result =await  captcha.GenerateCaptchaImageAsync(code);
            AuroCacheHelper AuroCacheHelper = new AuroCacheHelper();
            TimeSpan expiresSliding = DateTime.Now.AddMinutes(5) - DateTime.Now;
            
            AuroCacheHelper.Add("ValidateCode"+ result.Timestamp.ToString("yyyyMMddHHmmssffff"), code, expiresSliding,false);
            AuthGetVerifyCodeOutputDto authGetVerifyCodeOutputDto = new AuthGetVerifyCodeOutputDto();
            authGetVerifyCodeOutputDto.Img =Convert.ToBase64String(result.CaptchaMemoryStream.ToArray());
            authGetVerifyCodeOutputDto.Key = result.Timestamp.ToString("yyyyMMddHHmmssffff");
            CommonResult<AuthGetVerifyCodeOutputDto> commonResult = new CommonResult<AuthGetVerifyCodeOutputDto>();
            commonResult.ErrCode= ErrCode.successCode;
            commonResult.ResData = authGetVerifyCodeOutputDto;
            return commonResult;
        }
    }
}
