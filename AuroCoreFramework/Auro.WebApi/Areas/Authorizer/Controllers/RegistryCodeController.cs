using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.Commons.Helpers;
using Auro.Commons.Log;
using Auro.Commons.Mapping;
using Auro.Commons.Models;
using Auro.Commons.Pages;
using Auro.Authorizer.Dtos;
using Auro.Authorizer.Models;
using Auro.Authorizer.IServices;
using Auro.Commons.Encrypt;
using Auro.AspNetCore.Mvc;

namespace Auro.WebApi.Areas.Authorizer.Controllers
{
    /// <summary>
    /// 軟體授權註冊和驗證介面
    /// </summary>
    [ApiController]
    [Route("api/Authorizer/[controller]")]
    public class RegistryCodeController : AreaApiController<RegistryCode, RegistryCodeOutputDto, RegistryCodeInputDto, IRegistryCodeService,string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public RegistryCodeController(IRegistryCodeService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "RegistryCode/List";
            AuthorizeKey.InsertKey = "RegistryCode/Add";
            AuthorizeKey.UpdateKey = "RegistryCode/Edit";
            AuthorizeKey.UpdateEnableKey = "RegistryCode/Enable";
            AuthorizeKey.DeleteKey = "RegistryCode/Delete";
            AuthorizeKey.DeleteSoftKey = "RegistryCode/DeleteSoft";
            AuthorizeKey.ViewKey = "RegistryCode/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(RegistryCode info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(RegistryCode info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(RegistryCode info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 根據機器特徵碼獲取註冊序列號
        /// </summary>
        /// <param name="machineCode">機器特徵碼</param>
        [HttpPost("GetRegCode")]
        [AuroAuthorize("Edit")]
        public IActionResult GetRegCode(string machineCode)
        {
            CommonResult result = new CommonResult();
            try
            {
                //RSASecurityHelper.RSAEncrypSignature(machineCode,);
            }
            catch (Exception ex)
            {
                result.ErrMsg = ex.Message;
                Log4NetHelper.Error("獲取註冊序列號異常", ex);
            }
            return ToJsonContent(result);
        }
    }
}