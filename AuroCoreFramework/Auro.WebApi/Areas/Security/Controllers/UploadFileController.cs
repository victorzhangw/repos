using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Threading.Tasks;
using Auro.AspNetCore.Controllers;
using Auro.AspNetCore.Models;
using Auro.AspNetCore.Mvc;
using Auro.AspNetCore.ViewModel;
using Auro.Commons.Cache;
using Auro.Commons.Core.Dtos;
using Auro.Commons.Extensions;
using Auro.Commons.Json;
using Auro.Commons.Models;
using Auro.Security.Application;
using Auro.Security.Dtos;
using Auro.Security.IServices;
using Auro.Security.Models;

namespace Auro.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 檔案管理
    /// </summary>
    [Route("api/Security/[controller]")]
    [ApiController]
    public class UploadFileController : AreaApiController<UploadFile, UploadFileOutputDto, UploadFileInputDto, IUploadFileService, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_iService"></param>
        public UploadFileController(IUploadFileService _iService) : base(_iService)
        {
            iService = _iService;
        }


        /// <summary>
        /// 非同步批量物理刪除
        /// </summary>
        /// <param name="info">主鍵Id陣列</param>
        [HttpDelete("DeleteBatchAsync")]
        [AuroAuthorize("Delete")]
        public override async Task<IActionResult> DeleteBatchAsync(DeletesInputDto info)
        {
            CommonResult result = new CommonResult();
            string where = string.Empty;
            where = "id in ('" + info.Ids.Join(",").Trim(',').Replace(",", "','") + "')";

            if (!string.IsNullOrEmpty(where))
            {
                dynamic[] jobsId = info.Ids;
                foreach (var item in jobsId)
                {
                    if (string.IsNullOrEmpty(item.ToString())) continue;
                    UploadFile uploadFile = new UploadFileApp().Get(item.ToString());
                    AuroCacheHelper AuroCacheHelper = new AuroCacheHelper();
                    SysSetting sysSetting = AuroCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
                    if (uploadFile != null)
                    {
                        if (System.IO.File.Exists(sysSetting.LocalPath + "/" + uploadFile.FilePath))
                            System.IO.File.Delete(sysSetting.LocalPath + "/" + uploadFile.FilePath);
                        if (!string.IsNullOrEmpty(uploadFile.Thumbnail))
                        {
                            if (System.IO.File.Exists(sysSetting.LocalPath + "/" + uploadFile.Thumbnail))
                                System.IO.File.Delete(sysSetting.LocalPath + "/" + uploadFile.Thumbnail);
                        }
                    }
                }
                bool bl = await iService.DeleteBatchWhereAsync(where).ConfigureAwait(false);
                if (bl)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                }
                else
                {
                    result.ErrMsg = ErrCode.err43003;
                    result.ErrCode = "43003";
                }
            }
            return ToJsonContent(result);
        }

    }
}