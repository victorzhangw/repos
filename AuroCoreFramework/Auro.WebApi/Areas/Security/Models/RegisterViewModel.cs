using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auro.WebApi.Areas.Security.Models
{
    /// <summary>
    /// 註冊資訊
    /// </summary>
    [Serializable]
    public class RegisterViewModel
    {
        /// <summary>
        /// 設定帳號
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 設定電子郵件
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 設定密碼
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 設定驗證碼
        /// </summary>
        public string VerificationCode{ get; set; }
        /// <summary>
        /// 設定驗證碼Key
        /// </summary>
        public string VerifyCodeKey { get; set; }
    }
}
