using System;
using Auro.Commons.IServices;
using Auro.Quartz.Dtos;
using Auro.Quartz.Models;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Quartz.IServices
{
    /// <summary>
    /// 定義定時任務執行日誌服務介面
    /// </summary>
    public interface ITaskJobsLogService:IService<TaskJobsLog,TaskJobsLogOutputDto, string>
    {
    }
}
