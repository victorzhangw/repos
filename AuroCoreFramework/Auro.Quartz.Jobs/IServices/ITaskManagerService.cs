using Auro.Commons.Enums;
using Auro.Commons.IServices;
using Auro.Quartz.Dtos;
using Auro.Quartz.Models;
using Auro.Security.Dtos;
using Auro.Security.Models;

namespace Auro.Quartz.IServices
{
    /// <summary>
    /// 定義定時任務服務介面
    /// </summary>
    public interface ITaskManagerService:IService<TaskManager,TaskManagerOutputDto, string>
    {
        /// <summary>
        /// 記錄任務執行結果
        /// </summary>
        /// <param name="jobId">任務Id</param>
        /// <param name="jobAction">任務執行動作</param>
        /// <param name="blresultTag">任務執行結果表示，true成功，false失敗，初始執行為true</param>
        /// <param name="msg">任務記錄描述</param>
        void RecordRun(string jobId, JobAction jobAction, bool blresultTag = true, string msg = "");

    }
}
