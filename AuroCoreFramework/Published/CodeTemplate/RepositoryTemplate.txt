using System;
using Auro.Commons.IDbContext;
using Auro.Commons.Repositories;
using {IRepositoriesNamespace};
using {ModelsNamespace};

namespace {RepositoriesNamespace}
{
    /// <summary>
    /// {TableNameDesc}倉儲介面的實現
    /// </summary>
    public class {ModelTypeName}Repository : BaseRepository<{ModelTypeName}, {KeyTypeName}>, I{ModelTypeName}Repository
    {
		public {ModelTypeName}Repository()
        {
        }

        public {ModelTypeName}Repository(IDbContextCore context) : base(context)
        {

        }

        #region Dapper 操作

        //DapperConn 用於讀寫操作
        //DapperConnRead 用於只讀操作

        #endregion

        
        #region EF 操作

        //DbContext 用於讀寫操作
        //DbContextRead 用於只讀操作

        #endregion
    }
}