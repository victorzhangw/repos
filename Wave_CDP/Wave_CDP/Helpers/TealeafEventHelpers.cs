﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CACO_CDP.Models;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using System.Web;
using static CACO_CDP.Models.TealeafEvent;
using Newtonsoft.Json.Linq;

namespace CACO_CDP.Helpers
{
    public class TealeafEventHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<TealeafEventHelpers> _logger { get; set; }
        public TealeafEventHelpers(IConfiguration config, ILogger<TealeafEventHelpers> logger)
        {

            _configuration = config;
            _logger = logger;
        }
        public string  EventParser(List<TealeafEvent.TealeafEvents> tealeafEvents)
        {
            string returnString = string.Empty;
            var eventSections = _configuration.GetSection("Tealeaf:Events").Get<List<List<string>>>();
            var eventList = new List<dynamic>();
            try
            {
                List<dynamic> dynList = new List<dynamic>();
                List<ReportCollection> reportCollections = new List<ReportCollection>();
                
                foreach (List<string> eventSection in eventSections)
                {

                    var finds = from data in tealeafEvents
                                where data.eventname == eventSection[0]
                                select data;
                    //JObject jObject = new JObject();
                    
                    foreach (var find in finds)
                    {
                        //var tmpList = JsonConvert.DeserializeObject<dynamic>(find.eventstring);
                        
                        var tmpList = JsonConvert.DeserializeObject<List<TealeafEvent.ReportDataset>>(find.eventstring);
                        eventList.AddRange(tmpList);

                    }
                    
                    /*
                    var resultEvt1 = from evt in eventList
                                     group evt by (evt.Column1.ToString().Split('?')[0]) into grp
                                     select new { Col1 = grp.Key, Col2 = grp.Sum( evt =>(decimal) (NullableTryParseDecimal(evt.Column2))) };
                    */
                    var query1 = from evt in eventList
                                     group evt by StringDecode(ParseUrl(evt.Column1)) into grp
                                     orderby grp.Sum(evt => (decimal)(NullableTryParseDecimal(evt.Column2))) descending
                                     select new { Col1 = grp.Key, Col2 = grp.Sum(evt => (decimal)(NullableTryParseDecimal(evt.Column2))) };

                    var query2 = from evt in eventList 
                                 select new { Col1 = evt.Column2, Col2 = evt.Column3, Col3 = evt.Column4, Col4 = evt.Column5, Col5 = evt.Column6, Col6 = evt.Column7, Col7 = evt.Column8 };
                    var query3 = from evt in eventList
                                 select new { Col1 = evt.Column1, Col2 = evt.Column2, Col3 = evt.Column3, Col4 = evt.Column4, Col5 = evt.Column5, Col6 = evt.Column6, Col7 = evt.Column7 };



                    var resultEvt1 = query1.Take(Int32.Parse(eventSection[3])); //依據報表設定取資料筆數

                    var resultEvt2 = query2;
                    var resultEvt3 = query3;



                    switch (eventSection[2])
                    {
                        case "1":
                            var resultList1 = resultEvt1.ToList();
                           
                            //dynList.AddRange(resultList1);
                            reportCollections.Add(new ReportCollection { Name = eventSection[0],Data = resultList1 });
                            eventList.Clear();
                            //dynList.Clear();
                            break;
                        case "2":
                            var resultList2 = resultEvt2.ToList();
                            //dynList.AddRange(resultList2);
                            reportCollections.Add(new ReportCollection { Name = eventSection[0], Data = resultList2 });
                            
                            eventList.Clear();
                            //dynList.Clear();
                            break;
                        case "3":
                            var resultList3 = resultEvt3.ToList();
                            //dynList.AddRange(resultList2);
                            reportCollections.Add(new ReportCollection { Name = eventSection[0], Data = resultList3 });

                            eventList.Clear();
                            break;
                        case "4":
                            break;
                    }



                }
                try
                {
                    
                    returnString = JsonConvert.SerializeObject(reportCollections);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex.ToString());
                }
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.ToString());
            }
           
            return returnString;
        }
        public static decimal? NullableTryParseDecimal(string text)
        {
            decimal value;
            
            return decimal.TryParse(text, out value) ? (decimal?)value : 0;
        }
        public static string ParseUrl(string text)
        {
            string rtnUrl = string.Empty;
            var values = Regex.Split(text.Split('?')[0], @"(?<=[^/])/(?=[^/])", RegexOptions.None);
            try {
                // bool success = IsNullOrEmpty(values);
                bool success = true;
                if (success)
                {
                    if (values.Length > 2)
                    {

                        for (int i = 0; i < values.Length - 2; i++)
                            rtnUrl += values[i] + "/";
                    }
                    else
                    {
                        rtnUrl = values[0];
                    }
                    int indexVal = rtnUrl.LastIndexOf("/") + 1;
                    if (rtnUrl.Length == indexVal)
                    {
                        rtnUrl = rtnUrl.Substring(0, indexVal - 1);
                    }
                }
                
            }
            catch(Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }

            return rtnUrl;
        }
        public static string StringDecode(string text)
        {
            string _str;
            //return _str = Uri.EscapeDataString(text);
            return _str = HttpUtility.UrlDecode(text); 
        }
        public static bool IsNullOrEmpty<T>( T[] array)
        {
            return array == null || array.Length == 0;
        }

    }
}
