﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MySql.Data.MySqlClient;
using ChoETL;
using System.Globalization;
using System.Text;
using Microsoft.Extensions.Logging;
using System.Net.Mail;
using CACO_CDP.Models;
using Microsoft.AspNetCore.Razor.Language;

namespace CACO_CDP.Helpers
{
    public class CsvHelpers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        private readonly IConfiguration _configuration;
        private ILogger<CsvHelpers> _logger { get; set; }
        private CommonHelpers CommonHelpers { get; set; }
        public CsvHelpers(IConfiguration config ,ILogger<CsvHelpers> logger,CommonHelpers commonHelpers)
        {
            CommonHelpers = commonHelpers;
            _configuration = config;
            _logger = logger;
        }

        MailContextModel mailContextModel = new MailContextModel()
        {
            ToAddress = "victor.cheng@aurocore.com",
            FromAddress = "caco.tealeaf@gmail.com"
        };
        public int ParseCsvFiles()
        {
            
            int recordCount=0;
            var outDirPath = _configuration.GetSection("EmailConfiguration:outDirPath").Value;
            var addDays = Int32.Parse(_configuration.GetSection("EmailConfiguration:EventDay").Value);
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            int reportCount = Int32.Parse(_configuration.GetSection("Tealeaf:EventReportCount").Value);
            var eventSection = _configuration.GetSection("Tealeaf:Events").Get<List<List<string>>>();
            string insertCommand = "insert into tealeaf (eventstring,eventname,eventdate) values(@eventstring,@eventname,@eventdate)";
            string selectCommand = "select count(*) from  tealeaf where eventdate = @eventdate";
            DateTime eventDate = DateTime.Now.Date.AddDays(addDays);
            outDirPath = outDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
            try
            {
                for (int i = 0; i < eventSection.Count; i++)
                {
                    var fileName = Directory
                    .EnumerateFiles(outDirPath, "*", SearchOption.AllDirectories)
                    .Where(file => file.EndsWith(".csv") && file.Contains(eventSection[i][0]))
                    .Select(Path.GetFileName).FirstOrDefault(); // <-- note you can shorten the lambda
                    if (fileName != null)
                    {

                        string filePath =Path.GetFullPath(Path.Combine(outDirPath, fileName));
                        using var sr = new StreamReader(filePath);
                        string[] fileType = fileName.Split("~");
                        for (int j = 0; j < int.Parse(eventSection[i][1]); j++)
                        {
                            sr.ReadLine();
                        }
                        //  string remainingText = sr.ReadToEnd();
                        StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());
                        StringBuilder sb = new StringBuilder();
                        using (var csv = new ChoCSVReader(remainingText))
                        {
                            using (var parser = new ChoJSONWriter(sb))
                            {

                                parser.Write(csv);

                                var eventstring = sb.Append("]").ToString();
                                var tealeafEvent = new Models.TealeafEvent.TealeafEvents()
                                {
                                    eventdate = eventDate,
                                    eventstring = eventstring,
                                    eventname = fileType[0]
                                };
                                using (var connection = new MySqlConnection(cnStr.Value))
                                {
                                    recordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });

                                    if (recordCount <reportCount)
                                    {
                                        
                                        var insertResult = connection.Execute(insertCommand, tealeafEvent);
                                    }


                                }


                            }

                        }

                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"<--- Tealeaf Mail 匯入資料庫 --->{0} "+ ex.Message);
                mailContextModel.MailHead = "Tealeaf Mail 匯入資料庫失敗";
                mailContextModel.MailBody = ex.Message;
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            finally
            {
                _logger.LogInformation($"<--- Tealeaf Mail 匯入資料庫 --->{0} " + "成功");
            }

            // Get only the CSV files.
            try
            {
                string filePath = Path.GetFullPath(Path.Combine(outDirPath));
                foreach (string file in Directory.GetFiles(filePath))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                mailContextModel.MailHead = "Tealeaf Mail 匯入資料庫失敗";
                mailContextModel.MailBody = ex.Message;
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            finally
            {
                using (var connection = new MySqlConnection(cnStr.Value))
                {
                    recordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });

                   

                }
            }
            return (recordCount);
        }
        public int ParseTMEvents()
        {
            int recordCount = 0;
            string insertCommand = @"INSERT INTO tmevents.ubx_ibmvideoplayed
        (version, provider, source, channel, x1id, event_code, event_timestamp, 
        event_namespace, event_version, identifier_deviceid, identifier_googleanalyticscookie, 
        event_attribute_interactionid, event_attribute_browsername, 
        event_attribute_browserversion, event_attribute_vendor,
        event_attribute_model, event_attribute_manufacturer, 
        event_attribute_modelname, event_attribute_modelyear, 
        event_attribute_ip, event_attribute_locationcontinent, event_attribute_locationcountry,
        event_attribute_locationcity, event_attribute_latitude, event_attribute_longitude, 
        event_attribute_os, event_attribute_versionos, event_attribute_subchannel, 
        event_attribute_devicetype, event_attribute_tldatacenter, event_attribute_tlorgcode, 
        event_attribute_appkey, event_attribute_tlprocid, event_attribute_eventname, 
        event_attribute_elementid, identifier_dacookieid)
        VALUES(
        @version, @provider, @source, @channel, @x1id, @event_code, @event_timestamp, @
        event_namespace, @event_version, @identifier_deviceid, @identifier_googleanalyticscookie, @
        event_attribute_interactionid, @event_attribute_browsername, @
        event_attribute_browserversion, @event_attribute_vendor,
        event_attribute_model, @event_attribute_manufacturer, @
        event_attribute_modelname, @event_attribute_modelyear, @
        event_attribute_ip, @event_attribute_locationcontinent, @event_attribute_locationcountry,
        event_attribute_locationcity, @event_attribute_latitude, @event_attribute_longitude, @
        event_attribute_os, @event_attribute_versionos, @event_attribute_subchannel, @
        event_attribute_devicetype, @event_attribute_tldatacenter, @event_attribute_tlorgcode, @
        event_attribute_appkey, @event_attribute_tlprocid, @event_attribute_eventname, @
        event_attribute_elementid, @identifier_dacookieid);
            ";
            var ibmvideoplayed = new List<TealeafEvent.ubx_ibmvideoplayed>();

            return recordCount;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        public int ParseHBTFiles()
        {
            int recordCount = 0;
            var sendmailzone = _configuration.GetSection("Logging:host").Value;
            var outDirPath = _configuration.GetSection("FtpConfiguration:ftp1:filepath").Value;
            outDirPath = outDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            var eventSection = _configuration.GetSection("FtpConfiguration:ftp1:reports").Get<List<List<string>>>();
            DateTime dt = DateTime.Now;
            string create_date = dt.ToLocalTime().ToString("yyyyMMddHHmmss");
            string modify_date = dt.ToLocalTime().ToString("yyyyMMddHHmmss");
            var addDays = Int32.Parse(_configuration.GetSection("EmailConfiguration:EventDay").Value);
            var selectCommand = "select count(*) from orders where date_format(orderdate,'%Y%m%d') =date_format(@eventDate,'%Y%m%d')";
            DateTime eventDate = DateTime.Now.Date.AddDays(addDays);
            try
            {
                using (var connection = new MySqlConnection(cnStr.Value))
                {

                    recordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });
                }
                if (recordCount == 0)
                {
                    for (int i = 0; i < eventSection.Count; i++)
                    {

                        string filekeyWord = eventSection[i][3];
                        var fileName = Directory
                        .EnumerateFiles(outDirPath, "*", SearchOption.AllDirectories)
                        // .Where(file => file.EndsWith(".csv") && file.Contains(eventSection[i][0]))
                        .Where(file => file.Contains(eventSection[i][0]))
                        .Select(Path.GetFileName).FirstOrDefault(); // <-- note you can shorten the lambda
                        if (fileName != null)
                        {

                            bool isbig5=false;
                            string filePath = Path.GetFullPath(Path.Combine(outDirPath, fileName));
                            //using var sr = new StreamReader(filePath);
                            string[] fileType = fileName.Split("~");
                            switch (filekeyWord)
                            {
                                case "members":
                                    List<HbtData.Members> listMember = new List<HbtData.Members>();
                                    isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                    if (isbig5)
                                    {
                                        File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                    }
                                    using (var sr = new StreamReader(filePath, Encoding.GetEncoding(950)))
                                    {
                                        for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                        {
                                            sr.ReadLine();
                                        }
                                        StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                        StringBuilder sb = new StringBuilder();


                                        using (var csv = new ChoCSVReader(remainingText))
                                        {

                                            foreach (dynamic col in csv)
                                            {

                                                var member = new HbtData.Members()
                                                {
                                                    channel = col[0],
                                                    memberid = col[1].Replace("'", ""),
                                                    membername = col[2],
                                                    registerdate = col[3],
                                                    memberemail = col[4],
                                                    gender = col[5],
                                                    create_date = create_date,
                                                    modify_date = modify_date


                                                };
                                                listMember.Add(member);
                                            }
                                            //Console.WriteLine(listMember);
                                        }
                                    }

                                    using (var connection = new MySqlConnection(cnStr.Value))
                                    {
                                        connection.Open();
                                        using (var trans = connection.BeginTransaction())
                                        {
                                            var insertResult = connection.Execute(eventSection[i][1], listMember, trans);
                                            trans.Commit();
                                        }

                                    }

                                    break;
                                case "online_order_raw":
                                    List<HbtData.Orders> listOrder = new List<HbtData.Orders>();
                                    List<HbtData.Orders> detailListOrder = new List<HbtData.Orders>();
                                    isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                    if (isbig5)
                                    {
                                        File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                    }
                                    using (var sr = new StreamReader(filePath, Encoding.GetEncoding(950)))
                                    {
                                        for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                        {
                                            sr.ReadLine();
                                        }
                                        StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                        StringBuilder sb = new StringBuilder();


                                        using (var csv = new ChoCSVReader(remainingText))
                                        {

                                            foreach (dynamic col in csv)
                                            {

                                                var order = new HbtData.Orders()
                                                {
                                                    ordertype = col[0],
                                                    channel = col[1],
                                                    orderno = col[2],
                                                    orderdate = col[3],
                                                    memberid = col[4].Replace("'", ""),
                                                    membername = col[5],
                                                    productid = col[6],
                                                    productcategory = col[7],
                                                    unitprice = col[8],
                                                    quantity = col[9],
                                                    amount = col[10],
                                                    shippingaddress = col[11],
                                                    ordercampaign = col[12],
                                                    create_date = create_date,
                                                    modify_date = modify_date


                                                };
                                                if (col[0] == "d")
                                                {
                                                    var detailOrder = new HbtData.Orders()
                                                    {
                                                        ordertype = col[0],
                                                        channel = col[1],
                                                        orderno = col[2],
                                                        orderdate = col[3],
                                                        memberid = col[4].Replace("'", ""),
                                                        membername = col[5],
                                                        productid = col[6],
                                                        productcategory = col[7],
                                                        unitprice = col[8],
                                                        quantity = col[9],
                                                        amount = col[10],
                                                        shippingaddress = col[11],
                                                        ordercampaign = col[12],
                                                        create_date = create_date,
                                                        modify_date = modify_date


                                                    };
                                                    detailListOrder.Add(detailOrder);
                                                }

                                                listOrder.Add(order);

                                            }

                                        }
                                    }

                                    using (var connection = new MySqlConnection(cnStr.Value))
                                    {
                                        connection.Open();
                                        using (var trans = connection.BeginTransaction())
                                        {
                                            var insertResult1 = connection.Execute(eventSection[i][1], listOrder, trans);
                                            var insertResult2 = connection.Execute(eventSection[i][4], detailListOrder, trans);
                                            trans.Commit();
                                        }

                                    }

                                    break;
                                case "offline_order":
                                    List<HbtData.Orders> offlinelistOrder = new List<HbtData.Orders>();
                                    isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                    if (isbig5)
                                    {
                                        File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                    }
                                    using (var sr = new StreamReader(filePath, Encoding.GetEncoding(950)))
                                    {
                                        for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                        {
                                            sr.ReadLine();
                                        }
                                        StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                        StringBuilder sb = new StringBuilder();


                                        using (var csv = new ChoCSVReader(remainingText))
                                        {

                                            foreach (dynamic col in csv)
                                            {

                                                var order = new HbtData.Orders()
                                                {
                                                    channel = col[0],
                                                    orderno = col[1],
                                                    orderdate = col[2],
                                                    shop = col[3],
                                                    memberid = col[4].Replace("'", ""),
                                                    membername = col[5],
                                                    productid = col[6],
                                                    productcategory = col[7],
                                                    unitprice = col[8],
                                                    quantity = col[9],
                                                    amount = col[10],
                                                    create_date = create_date,
                                                    modify_date = modify_date


                                                };
                                                offlinelistOrder.Add(order);
                                            }

                                        }
                                    }

                                    using (var connection = new MySqlConnection(cnStr.Value))
                                    {
                                        connection.Open();
                                        using (var trans = connection.BeginTransaction())
                                        {
                                            var insertResult = connection.Execute(eventSection[i][1], offlinelistOrder, trans);
                                            trans.Commit();
                                        }

                                    }

                                    break;
                                case "products":

                                    List<HbtData.Products> listProduct = new List<HbtData.Products>();
                                    isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                    if (isbig5)
                                    {
                                        File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                    }

                                    using (var sr = new StreamReader(filePath))
                                    {
                                        for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                        {
                                            sr.ReadLine();
                                        }
                                        StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                        // StringBuilder sb = new StringBuilder();


                                        using (var csv = new ChoCSVReader(remainingText))
                                        {

                                            foreach (dynamic col in csv)
                                            {

                                                var product = new HbtData.Products()
                                                {
                                                    product_id = col[0],
                                                    product_group_id = col[1],
                                                    title = col[2],
                                                    description = col[3],
                                                    analyze_title = col[4],
                                                    analyze_description = col[5],
                                                    availability = col[8],
                                                    price = col[9],
                                                    currency = col[10],
                                                    brand = col[11],
                                                    product_type = col[12],
                                                    condition = col[14],
                                                    size = col[15],
                                                    size_system = col[16],
                                                    color = col[17],
                                                    pattern = col[18],
                                                    sale_price = col[21],
                                                    custom_label_0 = col[29],
                                                    custom_label_1 = col[30],
                                                    create_date = create_date,
                                                    modify_date = modify_date

                                                };
                                                listProduct.Add(product);
                                            }

                                        }
                                    }

                                    using (var connection = new MySqlConnection(cnStr.Value))
                                    {
                                        connection.Open();
                                        using (var trans = connection.BeginTransaction())
                                        {
                                            var insertResult = connection.Execute(eventSection[i][1], listProduct, trans);
                                            trans.Commit();

                                        }

                                    }
                                    break;

                            }


                        }
                    }
                } 
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"<--- 海博資料匯入{sendmailzone}資料庫  ");
                mailContextModel.MailHead = ($"海博資料匯入{sendmailzone}資料庫失敗" );
                mailContextModel.MailBody = ex.Message;
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            finally
            {
                _logger.LogInformation($"<--- Mail 匯入資料庫 --->{0} " + "成功");
            }

            // Get only the CSV files.
            try
            {
                string filePath = Path.GetFullPath(Path.Combine(outDirPath));
                foreach (string file in Directory.GetFiles(filePath))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                
            }
            finally
            {
                int resultrecordCount = 0;
                using (var connection = new MySqlConnection(cnStr.Value))
                {
                    
                    resultrecordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });
                }
                if (recordCount> 0)
                {
                    mailContextModel.MailHead = $"海博資料未匯入{sendmailzone}資料庫";
                    mailContextModel.MailBody = $"{DateTime.Now:yyyy/MM/dd}-{sendmailzone}資料庫已有訂單: {recordCount} 筆";
                    
                }
                else
                {
                    mailContextModel.MailHead = $"海博資料匯入{sendmailzone}資料庫成功";
                    mailContextModel.MailBody = $"{DateTime.Now:yyyy/MM/dd}-{sendmailzone}海博訂單資料匯入 {resultrecordCount} 筆";
                    recordCount = resultrecordCount;
                }
                
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            return recordCount;
        }
        
        private static string DateFormatter(string datecolumn)
        {
            CultureInfo MyCultureInfo = new CultureInfo("zh-TW");
            DateTime MyDateTime = DateTime.Parse(datecolumn, MyCultureInfo);
            var formatteredDate = MyDateTime.ToString("yyyy-MM-dd");
            return formatteredDate;
        }

       

    }
    }
