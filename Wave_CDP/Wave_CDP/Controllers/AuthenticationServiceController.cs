﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using CACO_CDP.Helpers;
using CACO_CDP.Models;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Dapper;
using Microsoft.Extensions.Logging;
namespace CACO_CDP.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class AuthenticationServiceController : ControllerBase
    {
        private readonly JwtHelpers jwt;
        private readonly IConfiguration _configuration;
        private ILogger<AuthenticationServiceController> _logger { get; set; }
        public AuthenticationServiceController(JwtHelpers jwt, IConfiguration config,ILogger<AuthenticationServiceController> logger)
        {
            this.jwt = jwt;
            _configuration = config;
            _logger = logger;
        }

        
        List<Authorize.MemberInfo> lstMemberInfo = new List<Authorize.MemberInfo>();
        [AllowAnonymous]
        [HttpPost]
        public ActionResult<string> SignIn([FromForm]LoginViewModel login)
        {
            _logger.LogInformation("AuthenticationService-SignIn");
            var validated = ValidateUser(login);
            if (validated.ValidateStatus)
            {
                return jwt.GenerateToken(login.Username);
            }
            else
            {
                return validated.Description;
            }
        }

        private ValidateResult ValidateUser(LoginViewModel login)
        {

            ValidateResult validateResult = new ValidateResult();
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string checkSql = @"select username,password from 
                users where username=@username and password=@password";
            try
            {
                using (var connection = new MySqlConnection(cnStr.Value))
                {
                     connection.Open();

                    var lstMemberInfo = connection.Query<LoginViewModel>(checkSql, new { username=login.Username, password = login.Password }).ToList();
                    bool returnBool = (lstMemberInfo.Count == 1) ? true : false;
                    validateResult.ValidateStatus = returnBool;
                    validateResult.Description = (validateResult.ValidateStatus) ? "OK" : "empty";
                    return validateResult;
                }

            }
            catch (Exception ex)
            {
                validateResult.ValidateStatus = false;
                validateResult.Description = ex.ToString();
                _logger.LogError(ex.ToString());
                //Console.WriteLine(e);
                
                return validateResult;
            }

           
        }

        [HttpGet]
        public IActionResult GetClaims()
        {
            return Ok(User.Claims.Select(p => new { p.Type, p.Value }));
        }

        [HttpGet]
        public IActionResult GetUserName()
        {
           
            return Ok(User.Identity.Name);
        }

        [HttpGet]
        public IActionResult GetUniqueId()
        {
            var jti = User.Claims.FirstOrDefault(p => p.Type == "jti");
            return Ok(jti.Value);
        }
    }
    /// <summary>
    /// Retrieve Membetr informations
    /// </summary>
    /// <param name="memberKey"></param>
    /// <param name="tokentype"></param>
    /// <returns></returns>
   
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class ValidateResult
    {
        public bool ValidateStatus { get; set; }
        public string Description { get; set; }
    }
}
