﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Aurobase.CDP.Core.Models
{
    [Serializable]
    public class CDPTenantConfig
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        /// <summary>
        /// Organization Id
        /// </summary>
        public string TenantId { get; set; }
        /// <summary>
        /// 來源
        /// </summary>
        public string SourceName { get; set; }
        /// <summary>
        /// 來源類別/服務
        /// </summary>
        public string SourceCategory { get; set; }
        public string ViewId { get; set; }
        public string AccessId { get; set; }
        public string SecretKey { get; set; }
        public object SuccessToken { get; set; }
        public object RefreshToken { get; set; }
        public object IdentfyKeyword { get; set; }
    }
}
