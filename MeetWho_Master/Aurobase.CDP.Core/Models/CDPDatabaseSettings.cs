﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.CDP.Core.Models
{
    public class CDPDatabaseSettings : ICDPDatabaseSettings
    {
        public string TenantCollectionName { get; set; }
        public string TealeafReportCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
    public interface ICDPDatabaseSettings
    {
        string TenantCollectionName { get; set; }
        string TealeafReportCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
