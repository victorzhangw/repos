﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Aurobase.CDP.Core.Models;
using MongoDB.Driver;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Log;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Pages;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Aurobase.CDP.Core.Services
{
    public class MeetwhoMongoService
    {
        private readonly IMongoCollection<CDPTenantConfig> _tentantConfig;

        public MeetwhoMongoService(ICDPDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _tentantConfig = database.GetCollection<CDPTenantConfig>(settings.TenantCollectionName);
        }

        public List<CDPTenantConfig> Get() =>
            _tentantConfig.Find(CDPTenantConfig => true).ToList();

        public CDPTenantConfig Get(string id, string sourcename, string sourcecategory) =>
            _tentantConfig.Find<CDPTenantConfig>(
                tentant => tentant.TenantId == id && tentant.SourceName == sourcename && tentant.SourceCategory == sourcecategory)
            .FirstOrDefault();
        public CDPTenantConfig Get(string id) =>
            _tentantConfig.Find<CDPTenantConfig>(
                tentant => tentant.TenantId == id)
            .FirstOrDefault();

        public CDPTenantConfig Create(CDPTenantConfig tentant)
        {
            _tentantConfig.InsertOne(tentant);
            return tentant;
        }

        public void Update(string id, CDPTenantConfig tentantIn) =>
            _tentantConfig.ReplaceOne(tentant => tentant.TenantId == id, tentantIn);


        public void Remove(string id) =>
            _tentantConfig.DeleteOne(tentant => tentant.TenantId == id);
    }
}