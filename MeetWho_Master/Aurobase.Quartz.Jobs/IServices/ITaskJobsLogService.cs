using System;
using Aurobase.Commons.IServices;
using Aurobase.Quartz.Dtos;
using Aurobase.Quartz.Models;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Quartz.IServices
{
    /// <summary>
    /// 定義定時任務執行日誌服務介面
    /// </summary>
    public interface ITaskJobsLogService : IService<TaskJobsLog, TaskJobsLogOutputDto, string>
    {
    }
}
