using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aurobase.WebApi.Areas.Security.Models
{
    public class LocalTaskModel
    {
        /// <summary>
        /// 類名
        /// </summary>
        public string FullName
        {
            get; set;
        }
    }
}
