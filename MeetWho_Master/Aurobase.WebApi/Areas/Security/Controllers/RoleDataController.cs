using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Log;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Commons.Pages;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;
using Aurobase.Security.IServices;
using Aurobase.AspNetCore.Mvc;

namespace Aurobase.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 角色數據許可權介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class RoleDataController : AreaApiController<RoleData, RoleDataOutputDto, RoleDataInputDto, IRoleDataService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public RoleDataController(IRoleDataService _iService) : base(_iService)
        {
            iService = _iService;
        }
        /// <summary>
        /// 新增前處理數據
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(RoleData info)
        {
            info.Id = GuidUtils.CreateNo();
        }

        /// <summary>
        /// 在更新數據前對數據的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(RoleData info)
        {
        }

        /// <summary>
        /// 在軟刪除數據前對數據的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(RoleData info)
        {
        }


        /// <summary>
        /// 角色可以訪問數據
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns></returns>
        [HttpGet("GetAllRoleDataByRoleId")]
        [AurobaseAuthorize("List")]
        public async Task<IActionResult> GetAllRoleDataByRoleId(string roleId)
        {
            CommonResult result = new CommonResult();
            string where = string.Format("RoleId='{0}'", roleId);
            List<string> resultlist = new List<string>();
            IEnumerable<RoleData> list = await iService.GetListWhereAsync(where);
            foreach (RoleData info in list)
            {
                resultlist.Add(info.AuthorizeData);
            }
            result.ResData = resultlist;
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }
    }
}