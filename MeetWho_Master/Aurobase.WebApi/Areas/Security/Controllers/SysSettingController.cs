using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.AspNetCore.Mvc;
using Aurobase.AspNetCore.Mvc.Filter;
using Aurobase.Commons;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Encrypt;
using Aurobase.Commons.Extend;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Json;
using Aurobase.Commons.Log;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Quartz.IServices;
using Aurobase.Security.Application;
using Aurobase.Security.Dtos;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;
using Aurobase.WebApi.Areas.Security.Models;

namespace Aurobase.WebApi.Areas.Security
{
    /// <summary>
    /// 系統基本資訊
    /// </summary>
    [Route("api/Security/[controller]")]
    [ApiController]
    public class SysSettingController : ApiController
    {

        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IUserService userService;
        private readonly IMenuService menuService;
        private readonly IRoleService roleService;
        private readonly ITaskManagerService taskManagerService;
        private readonly IAPPService aPPService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostingEnvironment"></param>
        /// <param name="_userService"></param>
        /// <param name="_menuService"></param>
        /// <param name="_roleService"></param>
        /// <param name="_taskManagerService"></param>
        /// <param name="_aPPService"></param>
        public SysSettingController(IWebHostEnvironment hostingEnvironment, IUserService _userService, IMenuService _menuService, IRoleService _roleService, ITaskManagerService _taskManagerService, IAPPService _aPPService)
        {
            _hostingEnvironment = hostingEnvironment;
            userService = _userService;
            menuService = _menuService;
            roleService = _roleService;
            taskManagerService = _taskManagerService;
            aPPService = _aPPService;
        }

        /// <summary>
        /// 獲取系統資訊
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetSysInfo")]
        [AurobaseAuthorize("GetSysInfo")]
        public async Task<IActionResult> GetSysInfo()
        {
            CommonResult result = new CommonResult();
            try
            {
                SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
                AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
                AurobaseCacheHelper.Add("SysSetting", sysSetting);
                DashboardOutModel dashboardOutModel = new DashboardOutModel();
                dashboardOutModel.CertificatedCompany = sysSetting.CompanyName;
                dashboardOutModel.WebUrl = sysSetting.WebUrl;
                dashboardOutModel.Title = sysSetting.SoftName;
                dashboardOutModel.MachineName = Environment.MachineName;
                dashboardOutModel.ProcessorCount = Environment.ProcessorCount;
                dashboardOutModel.SystemPageSize = Environment.SystemPageSize;
                dashboardOutModel.WorkingSet = Environment.WorkingSet;
                dashboardOutModel.TickCount = Environment.TickCount;
                dashboardOutModel.RunTimeLength = (Environment.TickCount / 1000).ToBrowseTime();
                dashboardOutModel.FrameworkDescription = RuntimeInformation.FrameworkDescription;
                dashboardOutModel.OSName = RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? "Linux" : RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "OSX" : "Windows";
                dashboardOutModel.OSDescription = RuntimeInformation.OSDescription + " " + RuntimeInformation.OSArchitecture;
                dashboardOutModel.OSArchitecture = RuntimeInformation.OSArchitecture.ToString();
                dashboardOutModel.ProcessArchitecture = RuntimeInformation.ProcessArchitecture.ToString();

                dashboardOutModel.Directory = AppContext.BaseDirectory;
                Version version = Environment.Version;
                dashboardOutModel.SystemVersion = version.Major + "." + version.Minor + "." + version.Build;
                dashboardOutModel.Version = AppVersionHelper.Version;
                dashboardOutModel.Manufacturer = AppVersionHelper.Manufacturer;
                dashboardOutModel.WebSite = AppVersionHelper.WebSite;
                dashboardOutModel.UpdateUrl = AppVersionHelper.UpdateUrl;
                dashboardOutModel.IPAdress = Request.HttpContext.Connection.LocalIpAddress.ToString();
                dashboardOutModel.Port = Request.HttpContext.Connection.LocalPort.ToString();
                dashboardOutModel.TotalUser = await userService.GetCountByWhereAsync("1=1");
                dashboardOutModel.TotalModule = await menuService.GetCountByWhereAsync("1=1");
                dashboardOutModel.TotalRole = await roleService.GetCountByWhereAsync("1=1");
                dashboardOutModel.TotalTask = await taskManagerService.GetCountByWhereAsync("1=1");
                result.ResData = dashboardOutModel;
                result.ErrCode = ErrCode.successCode;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取系統資訊異常", ex);
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 獲取系統基本資訊不完整資訊
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetInfo")]
        [NoPermissionRequired]
        [NoSignRequired]
        public IActionResult GetInfo()
        {
            CommonResult result = new CommonResult();
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            SysSetting sysSetting = AurobaseCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            SysSettingOutputDto sysSettingOutputDto = new SysSettingOutputDto();
            if (sysSetting == null)
            {
                sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            }
            sysSetting.Email = "";
            sysSetting.Emailsmtp = "";
            sysSetting.Emailpassword = "";
            sysSetting.Smspassword = "";
            sysSetting.SmsSignName = "";
            sysSetting.Smsusername = "";
            sysSettingOutputDto = sysSetting.MapTo<SysSettingOutputDto>();
            if (sysSettingOutputDto != null)
            {
                sysSettingOutputDto.CopyRight = UIConstants.CopyRight;
                result.ResData = sysSettingOutputDto;
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }

            IEnumerable<APP> appList = aPPService.GetAllByIsNotDeleteAndEnabledMark();
            AurobaseCacheHelper.Add("AllowAppId", appList);
            return ToJsonContent(result);
        }

        /// <summary>
        /// 獲取系統基本資訊
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllInfo")]
        [AurobaseAuthorize("GetSysInfo")]
        public IActionResult GetAllInfo()
        {
            CommonResult result = new CommonResult();
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            SysSetting sysSetting = AurobaseCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            SysSettingOutputDto sysSettingOutputDto = new SysSettingOutputDto();
            if (sysSetting == null)
            {
                sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            }

            //對關鍵資訊解密
            if (!string.IsNullOrEmpty(sysSetting.Email))
                sysSetting.Email = DEncrypt.Decrypt(sysSetting.Email);
            if (!string.IsNullOrEmpty(sysSetting.Emailsmtp))
                sysSetting.Emailsmtp = DEncrypt.Decrypt(sysSetting.Emailsmtp);
            if (!string.IsNullOrEmpty(sysSetting.Emailpassword))
                sysSetting.Emailpassword = DEncrypt.Decrypt(sysSetting.Emailpassword);
            if (!string.IsNullOrEmpty(sysSetting.Smspassword))
                sysSetting.Smspassword = DEncrypt.Decrypt(sysSetting.Smspassword);
            if (!string.IsNullOrEmpty(sysSetting.Smsusername))
                sysSetting.Smsusername = DEncrypt.Decrypt(sysSetting.Smsusername);
            sysSettingOutputDto = sysSetting.MapTo<SysSettingOutputDto>();
            if (sysSettingOutputDto != null)
            {
                sysSettingOutputDto.CopyRight = UIConstants.CopyRight;
                result.ResData = sysSettingOutputDto;
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 儲存系統設定資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost("Save")]
        [AurobaseAuthorize("Edit")]
        public IActionResult Save(SysSetting info)
        {
            CommonResult result = new CommonResult();
            info.LocalPath = _hostingEnvironment.WebRootPath;
            SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            sysSetting = info;
            //對關鍵資訊加密
            if (!string.IsNullOrEmpty(info.Email))
                sysSetting.Email = DEncrypt.Encrypt(info.Email);
            if (!string.IsNullOrEmpty(info.Emailsmtp))
                sysSetting.Emailsmtp = DEncrypt.Encrypt(info.Emailsmtp);
            if (!string.IsNullOrEmpty(info.Emailpassword))
                sysSetting.Emailpassword = DEncrypt.Encrypt(info.Emailpassword);
            if (!string.IsNullOrEmpty(info.Smspassword))
                sysSetting.Smspassword = DEncrypt.Encrypt(info.Smspassword);
            if (!string.IsNullOrEmpty(info.Smsusername))
                sysSetting.Smsusername = DEncrypt.Encrypt(info.Smsusername);
            string uploadPath = _hostingEnvironment.WebRootPath + "/" + sysSetting.Filepath;
            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
            }
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            if (AurobaseCacheHelper.Exists("SysSetting"))
            {
                AurobaseCacheHelper.Replace("SysSetting", sysSetting);
            }
            else
            {
                //寫入快取
                AurobaseCacheHelper.Add("SysSetting", sysSetting);
            }
            XmlConverter.Serialize<SysSetting>(sysSetting, "xmlconfig/sys.config");
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }
    }
}