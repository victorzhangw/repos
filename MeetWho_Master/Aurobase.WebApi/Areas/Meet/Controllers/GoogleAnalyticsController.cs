using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Log;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Commons.Pages;
using Aurobase.Google.Dtos;
using Aurobase.Google.Models;
using Aurobase.Google.IServices;

namespace Aurobase.Google.Areas.Google
{
    /// <summary>
    /// Google Analytics接口
    /// </summary>
    [ApiController]
    [Route("api/Google/[controller]")]
    public class GoogleAnalyticsController : AreaApiController<GoogleAnalytics, GoogleAnalyticsOutputDto, GoogleAnalyticsInputDto, IGoogleAnalyticsService, string>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="_iService"></param>
        public GoogleAnalyticsController(IGoogleAnalyticsService _iService) : base(_iService)
        {
            iService = _iService;
        }
        /// <summary>
        /// 新增前处理数据
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(GoogleAnalytics info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;

        }

        /// <summary>
        /// 在更新数据前对数据的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(GoogleAnalytics info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在软删除数据前对数据的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(GoogleAnalytics info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 取得 Google Analytics 報表
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> GAReport()
        {
            CommonResult result = new CommonResult();

            return ToJsonContent(result);
        }
    }
}