﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Aurobase.CDP.Core.Services;
using Aurobase.CDP.Core.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http.Headers;
using Aurobase.Commons.Models;
using Aurobase.AspNetCore.Models;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Mvc;
namespace Aurobase.WebApi.Areas.Meet.Controllers
{
    /// <summary>
    /// 用戶基礎設定
    /// </summary>
    [Route("api/Meet/[controller]")]
    [ApiController]

    public class CDPTenantController : ApiController
    {
        private readonly MeetwhoMongoService _meetService;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="meetwhoMongoService"></param>
        public CDPTenantController(MeetwhoMongoService meetwhoMongoService)
        {
            _meetService = meetwhoMongoService;
        }

        /// <summary>
        /// 取得全部資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<CDPTenantConfig>> Get()
        {

            return _meetService.Get();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sourceCategory"></param>
        /// <param name="sourceName"></param>
        /// <returns></returns>

        [HttpGet("GetTenant")]
        [AllowAnonymous]
        public IActionResult Get(string id, string sourceName, string sourceCategory)
        {
            CDPTenantConfig tenant = new CDPTenantConfig();
            CommonResult result = new CommonResult();
            Request.Headers.TryGetValue("TenantId", out var headerValue);
            if (headerValue.Count > 0)
            {
                tenant = _meetService.Get(headerValue, sourceName, sourceCategory);
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = tenant;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }


            return ToJsonContent(result);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tenant"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<CDPTenantConfig> Create(CDPTenantConfig tenant)
        {
            _meetService.Create(tenant);

            return CreatedAtRoute("GetBook", new { id = tenant.TenantId.ToString() }, tenant);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tenantCfg"></param>
        /// <param name="sourceCategory"></param>
        /// <param name="sourceName"></param>
        /// <returns></returns>
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, CDPTenantConfig tenantCfg, string sourceCategory, string sourceName)
        {
            var book = _meetService.Get(id, sourceName, sourceCategory);

            if (book == null)
            {
                return NotFound();
            }

            _meetService.Update(id, tenantCfg);

            return NoContent();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sourceCategory"></param>
        /// <param name="sourceName"></param>
        /// <returns></returns>
        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id, string sourceCategory, string sourceName)
        {
            var tenant = _meetService.Get(id, sourceCategory, sourceName);

            if (tenant == null)
            {
                return NotFound();
            }

            _meetService.Remove(tenant.TenantId);

            return NoContent();
        }
    }

}
