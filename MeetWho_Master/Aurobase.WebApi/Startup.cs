using AutoMapper;
using log4net;
using log4net.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.Impl;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using Aurobase.AspNetCore.Common;
using Aurobase.AspNetCore.Mvc;
using Aurobase.AspNetCore.Mvc.Filter;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Core.App;
using Aurobase.Commons.DbContextCore;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Linq;
using Aurobase.Commons.Log;
using Aurobase.Commons.Module;
using Aurobase.Commons.Options;
using Aurobase.Quartz.Jobs;

namespace Aurobase.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        public static ILoggerRepository LoggerRepository { get; set; }
        string targetPath = string.Empty;
        IMvcBuilder mvcBuilder;
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        private IApiVersionDescriptionProvider apiVersionProvider;//api介面版本控制
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            //初始化log4net
            LoggerRepository = LogManager.CreateRepository("NETCoreRepository");
            Log4NetHelper.SetConfig(LoggerRepository, "log4net.config");
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();
            //services.AddSingleton(Configuration);
            //如果部署在linux系統上，需要加上下面的配置：
            services.Configure<KestrelServerOptions>(options => options.AllowSynchronousIO = true);
            //如果部署在IIS上，需要加上下面的配置：
            services.AddOptions();
            services.Configure<IISServerOptions>(options => options.AllowSynchronousIO = true);

            #region Swagger Api文件

            // Api多版本版本配置
            services.AddApiVersioning(o =>
            {
                //是否在請求頭中返回受支援的版本資訊。
                o.ReportApiVersions = true;
                //請求中未指定版本時預設的版本號。
                o.DefaultApiVersion = new ApiVersion(1, 0);
                //版本號以什麼形式，什麼欄位傳遞？版本資訊放到header ,不寫在不配置路由的情況下，版本資訊放到response url 中
                o.ApiVersionReader = new HeaderApiVersionReader("api-version");
                //請求沒有指明版本的情況下是否使用預設的版本。
                o.AssumeDefaultVersionWhenUnspecified = true;
            }).AddVersionedApiExplorer(option =>
            {    // 版本名的格式：v+版本號
                option.GroupNameFormat = "'v'V";
                option.AssumeDefaultVersionWhenUnspecified = true;
            });

            //獲取webapi版本資訊，用於swagger多版本支援 
            services.AddOptions<SwaggerGenOptions>().Configure<IApiVersionDescriptionProvider>((options, service) =>
            {
                options.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                apiVersionProvider = service;
            });
            services.AddSwaggerGen(options =>
            {
                string contactName = Configuration.GetSection("SwaggerDoc:ContactName").Value;
                string contactNameEmail = Configuration.GetSection("SwaggerDoc:ContactEmail").Value;
                string contactUrl = Configuration.GetSection("SwaggerDoc:ContactUrl").Value;
                foreach (var description in apiVersionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName,
                        new OpenApiInfo()
                        {
                            Title = $"{Configuration.GetSection("SwaggerDoc:Title").Value}v{description.ApiVersion}",
                            Version = description.ApiVersion.ToString(),
                            Description = Configuration.GetSection("SwaggerDoc:Description").Value + (description.IsDeprecated ? " - 此版本已放棄相容" : ""),//描述
                            Contact = new OpenApiContact { Name = contactName, Email = contactNameEmail, Url = new Uri(contactUrl) },
                            License = new OpenApiLicense { Name = contactName, Url = new Uri(contactUrl) }
                        });
                }

                //載入XML註釋
                Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.xml").ToList().ForEach(file =>
                {
                    options.IncludeXmlComments(file, true);
                });
                options.DocumentFilter<HiddenApiFilter>(); // 在介面類、方法標記屬性 [HiddenApi]，可以阻止【Swagger文件】產生
                //給api新增token令牌證書
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT授權(數據將在請求頭中進行傳輸) 直接在下框中輸入Bearer {token}（注意兩者之間是一個空格）\"",
                    Name = "Authorization",//jwt預設的參數名稱
                    In = ParameterLocation.Header,//jwt預設存放Authorization資訊的位置(請求頭中)
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });
                //新增安全請求
                options.AddSecurityRequirement(
                    new OpenApiSecurityRequirement {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference=new OpenApiReference{
                                    Type=ReferenceType.SecurityScheme,
                                    Id= "Bearer"
                                }
                            }
                            ,new string[] { }
                        }
                    }
                 );
                options.OperationFilter<AddRequiredHeaderParameter>();
                //開啟加權鎖
                options.OperationFilter<AddResponseHeadersFilter>();
                options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                options.OperationFilter<SecurityRequirementsOperationFilter>();
            });


            #endregion

            #region 全域性設定跨域訪問
            //允許所有跨域訪問，測試用
            services.AddCors(options => options.AddPolicy("AurobaseCors",
                policy => policy.WithOrigins("*").AllowAnyHeader().AllowAnyMethod()));
            // 跨域設定 建議正式環境
            //services.AddCors(options => options.AddPolicy("AurobaseCors",
            //    policy => policy.WithOrigins(Configuration.GetSection("AppSetting:AllowOrigins").Value.Split(',', StringSplitOptions.RemoveEmptyEntries)).AllowAnyHeader().AllowAnyMethod()));
            #endregion

            #region MiniProfiler
            services.AddMiniProfiler(options =>
            {
                options.RouteBasePath = "/profiler";
            }).AddEntityFramework();
            #endregion

            #region 控制器
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.WriteIndented = true;
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                //設定時間格式
                options.JsonSerializerOptions.Converters.Add(new DateTimeJsonConverter());
                options.JsonSerializerOptions.Converters.Add(new DateTimeNullableConverter());
                //設定bool獲取格式
                options.JsonSerializerOptions.Converters.Add(new BooleanJsonConverter());
                //設定Decimal獲取格式
                options.JsonSerializerOptions.Converters.Add(new DecimalJsonConverter());
                //設定數字
                options.JsonSerializerOptions.Converters.Add(new IntJsonConverter());
                options.JsonSerializerOptions.PropertyNamingPolicy = new UpperFirstCaseNamingPolicy();
                options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
            });

            mvcBuilder = services.AddMvc(option =>
            {
                //option.Filters.Add<AurobaseAuthorizationFilter>();
                option.Filters.Add(new ExceptionHandlingAttribute());
                //option.Filters.Add<ActionFilter>();
            }).AddRazorRuntimeCompilation();

            services.AddMvcCore()
                .AddAuthorization().AddApiExplorer();
            #endregion
            services.AddSignalR();//使用 SignalR
            InitIoC(services);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="apiVersionProvider"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider apiVersionProvider)
        {
            if (app != null)
            {
                app.UseStaticHttpContextAccessor();
                IServiceProvider provider = app.ApplicationServices;
                AutoMapperService.UsePack(provider);
                //載入外掛應用
                LoadMoudleApps(env);

                app.UseMiniProfiler();
                if (env.IsDevelopment())
                {
                    //開發環境時才使用SwaggerUI，生產環境一般不開啟
                    app.UseDeveloperExceptionPage();
                }
                else
                {
                    app.UseExceptionHandler("/Home/Error");
                    app.UseHsts();
                }

                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                    foreach (var description in apiVersionProvider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", $"{Configuration.GetSection("SwaggerDoc:Title").Value + description.GroupName.ToUpperInvariant()}");
                        options.RoutePrefix = string.Empty;//這裡主要是不需要再輸入swagger這個預設字首
                    }
                });
                app.Use((context, next) =>
                {
                    context.Request.EnableBuffering();
                    return next();
                });
                app.UseStaticFiles();
                app.UseRouting();
                app.UseAuthentication();
                app.UseAuthorization();
                //跨域
                app.UseMiddleware<CorsMiddleware>();
                app.UseCors("AurobaseCors");
                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapControllerRoute("default", "api/{controller=Home}/{action=Index}/{id?}");
                });
                app.UseStatusCodePages();
            }
        }



        /// <summary>
        /// IoC初始化
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private void InitIoC(IServiceCollection services)
        {

            #region 快取
            CacheProvider cacheProvider = new CacheProvider
            {
                IsUseRedis = Configuration.GetSection("CacheProvider:UseRedis").Value.ToBool(false),
                ConnectionString = Configuration.GetSection("CacheProvider:Redis_ConnectionString").Value,
                InstanceName = Configuration.GetSection("CacheProvider:Redis_InstanceName").Value
            };

            var jsonOptions = new JsonSerializerOptions();
            jsonOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
            jsonOptions.WriteIndented = true;
            jsonOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            jsonOptions.AllowTrailingCommas = true;
            //設定時間格式
            jsonOptions.Converters.Add(new DateTimeJsonConverter());
            jsonOptions.Converters.Add(new DateTimeNullableConverter());
            //設定bool獲取格式
            jsonOptions.Converters.Add(new BooleanJsonConverter());
            //設定數字
            jsonOptions.Converters.Add(new IntJsonConverter());
            jsonOptions.PropertyNamingPolicy = new UpperFirstCaseNamingPolicy();
            jsonOptions.PropertyNameCaseInsensitive = true;                     //忽略大小寫
            //判斷是否使用Redis，如果不使用 Redis就預設使用 MemoryCache
            if (cacheProvider.IsUseRedis)
            {
                //Use Redis
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = cacheProvider.ConnectionString;
                    options.InstanceName = cacheProvider.InstanceName;
                });
                services.AddSingleton(typeof(ICacheService), new RedisCacheService(new RedisCacheOptions
                {
                    Configuration = cacheProvider.ConnectionString,
                    InstanceName = cacheProvider.InstanceName
                }, jsonOptions, 0));
                services.Configure<DistributedCacheEntryOptions>(option => option.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5));//設定Redis快取有效時間為5分鐘。
            }
            else
            {
                //Use MemoryCache
                services.AddSingleton<IMemoryCache>(factory =>
                {
                    var cache = new MemoryCache(new MemoryCacheOptions());
                    return cache;
                });
                services.AddSingleton<ICacheService, MemoryCacheService>();
                services.Configure<MemoryCacheEntryOptions>(
                    options => options.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)); //設定MemoryCache快取有效時間為5分鐘
            }
            services.AddSingleton(cacheProvider);//註冊快取配置
            services.AddTransient<MemoryCacheService>();
            services.AddMemoryCache();// 啟用MemoryCache

            #endregion

            #region 身份認證授權

            var jwtConfig = Configuration.GetSection("Jwt");
            var jwtOption = new JwtOption
            {
                Issuer = jwtConfig["Issuer"],
                Audience = jwtConfig["Audience"],
                Secret = jwtConfig["Secret"],
                Expiration = Convert.ToInt16(jwtConfig["Expiration"]),
                refreshJwtTime = Convert.ToInt16(jwtConfig["refreshJwtTime"])
            };
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme; ;

            });
            services.AddSingleton(jwtOption);//註冊配置
            #endregion

            services.AddAutoScanInjection();//自動化注入倉儲和服務
            services.AddTransient<IDbContextCore, MySqlDbContext>(); //注入EF上下文

            #region automapper
            List<Assembly> myAssembly = RuntimeHelper.GetAllAurobaseAssemblies().ToList();
            services.AddAutoMapper(myAssembly);
            services.AddTransient<IMapper, Mapper>();
            #endregion

            #region 定時任務
            services.AddTransient<HttpResultfulJob>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            //設定定時啟動的任務
            services.AddHostedService<QuartzService>();
            #endregion
            App.Services = services;
            new DefaultInitial().CacheAppList();
        }

        /// <summary>
        /// 載入模組應用
        /// </summary>
        /// <param name="env"></param>
        private void LoadMoudleApps(IWebHostEnvironment env)
        {
            // 定位到外掛應用目錄 Apps
            var apps = env.ContentRootFileProvider.GetFileInfo("Apps");
            if (!Directory.Exists(apps.PhysicalPath))
            {
                return;
            }

            // 把 Apps 下的動態庫拷貝一份來執行，
            // 使 Apps 中的動態庫不會在執行時被佔用（以便重新編譯）
            var shadows = targetPath = PrepareShadowCopies();
            // 從 Shadow Copy 目錄載入 Assembly 並註冊到 Mvc 中
            LoadFromShadowCopies(shadows);

            string PrepareShadowCopies()
            {
                // 準備 Shadow Copy 的目標目錄
                var target = Path.Combine(env.ContentRootPath, "app_data", "apps-cache");
                Directory.CreateDirectory(target);

                // 找到外掛目錄下 bin 目錄中的 .dll，拷貝
                Directory.EnumerateDirectories(apps.PhysicalPath)
                    .Select(path => Path.Combine(path, "bin"))
                    .Where(Directory.Exists)
                    .SelectMany(bin => Directory.EnumerateFiles(bin, "*.dll"))
                    .ForEach(dll => File.Copy(dll, Path.Combine(target, Path.GetFileName(dll)), true));

                return target;
            }

            void LoadFromShadowCopies(string targetPath)
            {
                foreach (string dll in Directory.GetFiles(targetPath, "*.dll"))
                {
                    try
                    {
                        //解決外掛還引用其他主程式沒有引用的第三方dll問題System.IO.FileNotFoundException
                        AssemblyLoadContext.Default.LoadFromAssemblyPath(dll);
                    }
                    catch (Exception ex)
                    {
                        //非.net程式集型別的dll關聯load時會報錯，這裡忽略就可以
                        Log4NetHelper.Error(ex.Message);
                    }
                }
                // 從 Shadow Copy 目錄載入 Assembly 並註冊到 Mvc 中
                var groups = Directory.EnumerateFiles(targetPath, "Aurobase.*App.Api.dll").Select(AssemblyLoadContext.Default.LoadFromAssemblyPath);

                // 直接載入到為 ApplicationPart
                groups.ForEach(mvcBuilder.AddApplicationPart);
            }
        }
    }
}