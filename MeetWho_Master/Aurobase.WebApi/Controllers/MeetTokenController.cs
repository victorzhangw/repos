﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Models;
using Aurobase.AspNetCore.Mvc;
using Aurobase.Commons.Json;
using Aurobase.Commons.Log;
using Aurobase.Commons.Models;
using Aurobase.Commons.Options;
using Aurobase.Security.Application;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;
using Aurobase.Tenants.IServices;

namespace Aurobase.WebApi.Controllers
{
    /// <summary>
    /// 取得 Token
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MeetTokenController : TokenController
    {
        private IAPPService _iAPPService;
        private readonly IUserService userService;
        private readonly JwtOption _jwtModel;
        private readonly ITenantService _tenantService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="iAPPService"></param>
        /// <param name="_userService"></param>
        /// <param name="jwtModel"></param>
        /// <param name="tenantService"></param>
        public MeetTokenController(IAPPService iAPPService, IUserService _userService, JwtOption jwtModel, ITenantService tenantService) : base(iAPPService, _userService, jwtModel)
        {
            if (iAPPService == null)
                throw new ArgumentNullException(nameof(iAPPService));
            _iAPPService = iAPPService;
            userService = _userService;
            _jwtModel = jwtModel;
            _tenantService = tenantService;

        }
        /// <summary>
        /// 根据应用信息获得token令牌
        /// </summary>
        /// <param name="grant_type">获取access_token填写client_credential</param>
        /// <param name="appid">应用唯一凭证，应用AppId</param>
        /// <param name="secret">应用密钥AppSecret</param>
        /// <returns></returns>
        [HttpGet("GetToken")]
        [AllowAnonymous]
        public new IActionResult Get(string grant_type, string appid, string secret)
        {
            CommonResult result = new CommonResult();
            Request.Headers.TryGetValue("TenantId", out var headerValue);

            if (!grant_type.Equals(GrantType.ClientCredentials))
            {
                result.ErrCode = "40003";
                result.ErrMsg = ErrCode.err40003;
                return ToJsonContent(result);
            }
            else if (string.IsNullOrEmpty(grant_type))
            {
                result.ErrCode = "40003";
                result.ErrMsg = ErrCode.err40003;
                return ToJsonContent(result);
            }
            if (headerValue.Count > 0)
            {
                var tenant = _tenantService.Get(headerValue);

            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
                return ToJsonContent(result);
            }
            string strHost = Request.Host.ToString();
            APP app = _iAPPService.GetAPP(appid, secret);
            if (app == null)
            {
                result.ErrCode = "40001";
                result.ErrMsg = ErrCode.err40001;
            }
            else
            {
                if (!app.RequestUrl.Contains(strHost))
                {
                    result.ErrCode = "40002";
                    result.ErrMsg = ErrCode.err40002 + "，你当前请求主机：" + strHost + ",请参考：http://docs.v.Aurobase.com/guide/faq.html#%E6%8F%90%E7%A4%BA%E9%9C%80%E8%A6%81%E6%8E%88%E6%9D%83%E6%80%8E%E4%B9%88%E5%8A%9E";
                }
                else
                {
                    TokenProvider tokenProvider = new TokenProvider(_jwtModel);
                    TokenResult tokenResult = tokenProvider.GenerateToken(grant_type, appid, secret);
                    result.ResData = tokenResult;
                    result.ErrCode = "0";
                    return ToJsonContent(result);
                }
            }
            return ToJsonContent(result);
        }

    }
}
