using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Common;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.AspNetCore.Mvc;
using Aurobase.AspNetCore.Mvc.Filter;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Core.App;
using Aurobase.Commons.IoC;
using Aurobase.Commons.Json;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Commons.Net;
using Aurobase.Commons.Options;
using Aurobase.Security.Application;
using Aurobase.Security.Dtos;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.WebApi.Controllers
{
    /// <summary>
    /// 使用者登錄介面控制器
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class LoginController : ApiController
    {
        private IUserService _userService;
        private IUserLogOnService _userLogOnService;
        private ISystemTypeService _systemTypeService;
        private IAPPService _appService;
        private IRoleService _roleService;
        private IRoleDataService _roleDataService;
        private ILogService _logService;
        private IFilterIPService _filterIPService;
        private IMenuService _menuService;

        /// <summary>
        /// 建構函式注入服務
        /// </summary>
        /// <param name="iService"></param>
        /// <param name="userLogOnService"></param>
        /// <param name="systemTypeService"></param>
        /// <param name="logService"></param>
        /// <param name="appService"></param>
        /// <param name="roleService"></param>
        /// <param name="filterIPService"></param>
        /// <param name="roleDataService"></param>
        /// <param name="menuService"></param>
        public LoginController(IUserService iService, IUserLogOnService userLogOnService, ISystemTypeService systemTypeService, ILogService logService, IAPPService appService, IRoleService roleService, IFilterIPService filterIPService, IRoleDataService roleDataService, IMenuService menuService)
        {
            _userService = iService;
            _userLogOnService = userLogOnService;
            _systemTypeService = systemTypeService;
            _logService = logService;
            _appService = appService;
            _roleService = roleService;
            _filterIPService = filterIPService;
            _roleDataService = roleDataService;
            _menuService = menuService;
        }
        /// <summary>
        /// 使用者登錄，必須要有驗證碼
        /// </summary>
        /// <param name="username">使用者名稱</param>
        /// <param name="password">密碼</param>
        /// <param name="vcode">驗證碼</param>
        /// <param name="vkey">驗證碼key</param>
        /// <param name="appId">AppId</param>
        /// <param name="systemCode">系統編碼</param>
        /// <returns>返回使用者User對像</returns>
        [HttpGet("GetCheckUser")]
        [NoPermissionRequired]
        public async Task<IActionResult> GetCheckUser(string username, string password, string vcode, string vkey, string appId, string systemCode)
        {

            CommonResult result = new CommonResult();
            RemoteIpParser remoteIpParser = new RemoteIpParser();
            string strIp = remoteIpParser.GetClientIp(HttpContext).MapToIPv4().ToString();
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            var vCode = AurobaseCacheHelper.Get("ValidateCode" + vkey);
            string code = vCode != null ? vCode.ToString() : "11";
            if (vcode.ToUpper() != code)
            {
                result.ErrMsg = "驗證碼錯誤";
                return ToJsonContent(result);
            }
            Log logEntity = new Log();
            bool blIp = _filterIPService.ValidateIP(strIp);
            if (blIp)
            {
                result.ErrMsg = strIp + "該IP已被管理員禁止登錄！";
            }
            else
            {

                if (string.IsNullOrEmpty(username))
                {
                    result.ErrMsg = "使用者名稱不能為空！";
                }
                else if (string.IsNullOrEmpty(password))
                {
                    result.ErrMsg = "密碼不能為空！";
                }
                if (string.IsNullOrEmpty(systemCode))
                {

                    result.ErrMsg = ErrCode.err40006;
                }
                else
                {
                    string strHost = Request.Host.ToString();
                    APP app = _appService.GetAPP(appId);
                    if (app == null)
                    {
                        result.ErrCode = "40001";
                        result.ErrMsg = ErrCode.err40001;
                    }
                    else
                    {
                        if (!app.RequestUrl.Contains(strHost, StringComparison.Ordinal) && !strHost.Contains("localhost", StringComparison.Ordinal))
                        {
                            result.ErrCode = "40002";
                            result.ErrMsg = ErrCode.err40002 + "，你目前請求主機：" + strHost;
                        }
                        else
                        {
                            SystemType systemType = _systemTypeService.GetByCode(systemCode);
                            if (systemType == null)
                            {
                                result.ErrMsg = ErrCode.err40006;
                            }
                            else
                            {
                                Tuple<User, string> userLogin = await this._userService.Validate(username, password);
                                if (userLogin != null)
                                {
                                    string ipAddressName = IpAddressUtil.GetCityByIp(strIp);
                                    if (userLogin.Item1 != null)
                                    {
                                        result.Success = true;
                                        User user = userLogin.Item1;
                                        JwtOption jwtModel = App.GetService<JwtOption>();
                                        TokenProvider tokenProvider = new TokenProvider(jwtModel);
                                        TokenResult tokenResult = tokenProvider.LoginToken(user, appId);
                                        AurobaseCurrentUser currentSession = new AurobaseCurrentUser
                                        {
                                            UserId = user.Id,
                                            Name = user.RealName,
                                            AccessToken = tokenResult.AccessToken,
                                            AppKey = appId,
                                            CreateTime = DateTime.Now,
                                            Role = _roleService.GetRoleEnCode(user.RoleId),
                                            ActiveSystemId = systemType.Id,
                                            CurrentLoginIP = strIp,
                                            IPAddressName = ipAddressName
                                        };
                                        TimeSpan expiresSliding = DateTime.Now.AddMinutes(120) - DateTime.Now;
                                        AurobaseCacheHelper.Add("login_user_" + user.Id, currentSession, expiresSliding, true);

                                        List<AllowCacheApp> list = AurobaseCacheHelper.Get("AllowAppId").ToJson().ToList<AllowCacheApp>();
                                        if (list.Count == 0)
                                        {
                                            IEnumerable<APP> appList = _appService.GetAllByIsNotDeleteAndEnabledMark();
                                            AurobaseCacheHelper.Add("AllowAppId", appList);
                                        }
                                        CurrentUser = currentSession;
                                        result.ResData = currentSession;
                                        result.ErrCode = ErrCode.successCode;
                                        result.Success = true;

                                        logEntity.Account = user.Account;
                                        logEntity.NickName = user.NickName;
                                        logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                                        logEntity.IPAddress = CurrentUser.CurrentLoginIP;
                                        logEntity.IPAddressName = CurrentUser.IPAddressName;
                                        logEntity.Result = true;
                                        logEntity.ModuleName = "登錄";
                                        logEntity.Description = "登錄成功";
                                        logEntity.Type = "Login";
                                        _logService.Insert(logEntity);
                                    }
                                    else
                                    {
                                        result.ErrCode = ErrCode.failCode;
                                        result.ErrMsg = userLogin.Item2;
                                        logEntity.Account = username;
                                        logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                                        logEntity.IPAddress = strIp;
                                        logEntity.IPAddressName = ipAddressName;
                                        logEntity.Result = false;
                                        logEntity.ModuleName = "登錄";
                                        logEntity.Type = "Login";
                                        logEntity.Description = "登錄失敗，" + userLogin.Item2;
                                        _logService.Insert(logEntity);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            AurobaseCacheHelper.Remove("LoginValidateCode");
            return ToJsonContent(result, true);
        }

        /// <summary>
        /// 獲取登錄使用者許可權資訊
        /// </summary>
        /// <returns>返回使用者User對像</returns>
        [HttpGet("GetUserInfo")]
        [AurobaseAuthorize("")]
        public IActionResult GetUserInfo()
        {
            CommonResult result = new CommonResult();
            User user = _userService.Get(CurrentUser.UserId);
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            SystemType systemType = _systemTypeService.Get(CurrentUser.ActiveSystemId);
            AurobaseCurrentUser currentSession = new AurobaseCurrentUser
            {
                UserId = user.Id,
                Account = user.Account,
                Name = user.RealName,
                NickName = user.NickName,
                AccessToken = CurrentUser.AccessToken,
                AppKey = CurrentUser.AppKey,
                CreateTime = DateTime.Now,
                HeadIcon = user.HeadIcon,
                Gender = user.Gender,
                ReferralUserId = user.ReferralUserId,
                MemberGradeId = user.MemberGradeId,
                Role = _roleService.GetRoleEnCode(user.RoleId),
                MobilePhone = user.MobilePhone,
                OrganizeId = user.OrganizeId,
                DeptId = user.DepartmentId,
                CurrentLoginIP = CurrentUser.CurrentLoginIP,
                IPAddressName = CurrentUser.IPAddressName,
                TenantId = ""
            };
            CurrentUser = currentSession;

            CurrentUser.ActiveSystemId = systemType.Id;
            CurrentUser.ActiveSystem = systemType.FullName;
            CurrentUser.ActiveSystemUrl = systemType.Url;

            List<MenuOutputDto> listFunction = new List<MenuOutputDto>();
            MenuApp menuApp = new MenuApp();
            if (Permission.IsAdmin(CurrentUser))
            {
                CurrentUser.SubSystemList = _systemTypeService.GetAllByIsNotDeleteAndEnabledMark().MapTo<SystemTypeOutputDto>();
                //取得使用者可使用的授權功能資訊，並存儲在快取中
                listFunction = menuApp.GetFunctionsBySystem(CurrentUser.ActiveSystemId);
                CurrentUser.MenusRouter = menuApp.GetVueRouter("", systemType.EnCode);
            }
            else
            {
                CurrentUser.SubSystemList = _systemTypeService.GetSubSystemList(user.RoleId);
                //取得使用者可使用的授權功能資訊，並存儲在快取中
                listFunction = menuApp.GetFunctionsByUser(user.Id, CurrentUser.ActiveSystemId);
                CurrentUser.MenusRouter = menuApp.GetVueRouter(user.RoleId, systemType.EnCode);
            }
            UserLogOn userLogOn = _userLogOnService.GetByUserId(CurrentUser.UserId);
            CurrentUser.UserTheme = userLogOn.Theme == null ? "default" : userLogOn.Theme;
            TimeSpan expiresSliding = DateTime.Now.AddMinutes(120) - DateTime.Now;
            AurobaseCacheHelper.Add("User_Function_" + user.Id, listFunction, expiresSliding, true);
            List<string> listModules = new List<string>();
            foreach (MenuOutputDto item in listFunction)
            {
                listModules.Add(item.EnCode);
            }
            CurrentUser.Modules = listModules;
            AurobaseCacheHelper.Add("login_user_" + user.Id, CurrentUser, expiresSliding, true);
            //該使用者的數據許可權
            List<String> roleDateList = _roleDataService.GetListDeptByRole(user.RoleId);
            AurobaseCacheHelper.Add("User_RoleData_" + user.Id, roleDateList, expiresSliding, true);
            result.ResData = CurrentUser;
            result.ErrCode = ErrCode.successCode;
            result.Success = true;
            return ToJsonContent(result, true);
        }

        /// <summary>
        /// 使用者登錄，無驗證碼，主要用於app登錄
        /// </summary>
        /// <param name="username">使用者名稱</param>
        /// <param name="password">密碼</param>
        /// <param name="appId">AppId</param>
        /// <param name="systemCode">系統編碼</param>
        /// <returns>返回使用者User對像</returns>
        [HttpGet("UserLogin")]
        [ApiVersion("2.0")]
        [NoPermissionRequired]
        public async Task<IActionResult> UserLogin(string username, string password, string appId, string systemCode)
        {
            CommonResult result = new CommonResult();
            RemoteIpParser remoteIpParser = new RemoteIpParser();
            string strIp = remoteIpParser.GetClientIp(HttpContext).MapToIPv4().ToString();
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            Log logEntity = new Log();
            bool blIp = _filterIPService.ValidateIP(strIp);
            if (blIp)
            {
                result.ErrMsg = strIp + "該IP已被管理員禁止登錄！";
            }
            else
            {
                if (string.IsNullOrEmpty(username))
                {
                    result.ErrMsg = "使用者名稱不能為空！";
                }
                else if (string.IsNullOrEmpty(password))
                {
                    result.ErrMsg = "密碼不能為空！";
                }
                if (string.IsNullOrEmpty(systemCode))
                {

                    result.ErrMsg = ErrCode.err40006;
                }
                else
                {
                    string strHost = Request.Host.ToString();
                    APP app = _appService.GetAPP(appId);
                    if (app == null)
                    {
                        result.ErrCode = "40001";
                        result.ErrMsg = ErrCode.err40001;
                    }
                    else
                    {
                        if (!app.RequestUrl.Contains(strHost, StringComparison.Ordinal) && !strHost.Contains("localhost", StringComparison.Ordinal))
                        {
                            result.ErrCode = "40002";
                            result.ErrMsg = ErrCode.err40002 + "，你目前請求主機：" + strHost;
                        }
                        else
                        {
                            SystemType systemType = _systemTypeService.GetByCode(systemCode);
                            if (systemType == null)
                            {
                                result.ErrMsg = ErrCode.err40006;
                            }
                            else
                            {
                                Tuple<User, string> userLogin = await this._userService.Validate(username, password);
                                if (userLogin != null)
                                {

                                    string ipAddressName = IpAddressUtil.GetCityByIp(strIp);
                                    if (userLogin.Item1 != null)
                                    {
                                        result.Success = true;

                                        User user = userLogin.Item1;

                                        JwtOption jwtModel = App.GetService<JwtOption>();
                                        TokenProvider tokenProvider = new TokenProvider(jwtModel);
                                        TokenResult tokenResult = tokenProvider.LoginToken(user, appId);
                                        AurobaseCurrentUser currentSession = new AurobaseCurrentUser
                                        {
                                            UserId = user.Id,
                                            Name = user.RealName,
                                            AccessToken = tokenResult.AccessToken,
                                            AppKey = appId,
                                            CreateTime = DateTime.Now,
                                            Role = _roleService.GetRoleEnCode(user.RoleId),
                                            ActiveSystemId = systemType.Id,
                                            CurrentLoginIP = strIp,
                                            IPAddressName = ipAddressName

                                        };
                                        TimeSpan expiresSliding = DateTime.Now.AddMinutes(120) - DateTime.Now;
                                        AurobaseCacheHelper.Add("login_user_" + user.Id, currentSession, expiresSliding, true);
                                        CurrentUser = currentSession;
                                        result.ResData = currentSession;
                                        result.ErrCode = ErrCode.successCode;
                                        result.Success = true;

                                        logEntity.Account = user.Account;
                                        logEntity.NickName = user.NickName;
                                        logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                                        logEntity.IPAddress = CurrentUser.CurrentLoginIP;
                                        logEntity.IPAddressName = CurrentUser.IPAddressName;
                                        logEntity.Result = true;
                                        logEntity.ModuleName = "登錄";
                                        logEntity.Description = "登錄成功";
                                        logEntity.Type = "Login";
                                        _logService.Insert(logEntity);
                                    }
                                    else
                                    {
                                        result.ErrCode = ErrCode.failCode;
                                        result.ErrMsg = userLogin.Item2;
                                        logEntity.Account = username;
                                        logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                                        logEntity.IPAddress = strIp;
                                        logEntity.IPAddressName = ipAddressName;
                                        logEntity.Result = false;
                                        logEntity.ModuleName = "登錄";
                                        logEntity.Type = "Login";
                                        logEntity.Description = "登錄失敗，" + userLogin.Item2;
                                        _logService.Insert(logEntity);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return ToJsonContent(result, true);
        }


        /// <summary>
        /// 退出登錄
        /// </summary>
        /// <returns></returns>
        [HttpGet("Logout")]
        [AurobaseAuthorize("")]
        public IActionResult Logout()
        {
            CommonResult result = new CommonResult();
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            AurobaseCacheHelper.Remove("login_user_" + CurrentUser.UserId);
            AurobaseCacheHelper.Remove("User_Function_" + CurrentUser.UserId);
            UserLogOn userLogOn = _userLogOnService.GetWhere("UserId='" + CurrentUser.UserId + "'");
            userLogOn.UserOnLine = false;
            _userLogOnService.Update(userLogOn, userLogOn.Id);
            CurrentUser = null;
            result.Success = true;
            result.ErrCode = ErrCode.successCode;
            result.ErrMsg = "成功退出";
            return ToJsonContent(result);
        }

        /// <summary>
        /// 子系統切換登錄
        /// </summary>
        /// <param name="openmf">憑據</param>
        /// <param name="appId">應用Id</param>
        /// <param name="systemCode">子系統編碼</param>
        /// <returns>返回使用者User對像</returns>
        [HttpGet("SysConnect")]
        [AllowAnonymous]
        [NoPermissionRequired]
        public IActionResult SysConnect(string openmf, string appId, string systemCode)
        {
            CommonResult result = new CommonResult();
            RemoteIpParser remoteIpParser = new RemoteIpParser();
            string strIp = remoteIpParser.GetClientIp(HttpContext).MapToIPv4().ToString();
            if (string.IsNullOrEmpty(openmf))
            {
                result.ErrMsg = "切換參數錯誤！";
            }

            bool blIp = _filterIPService.ValidateIP(strIp);
            if (blIp)
            {
                result.ErrMsg = strIp + "該IP已被管理員禁止登錄！";
            }
            else
            {
                string ipAddressName = IpAddressUtil.GetCityByIp(strIp);
                if (string.IsNullOrEmpty(systemCode))
                {
                    result.ErrMsg = ErrCode.err40006;
                }
                else
                {
                    string strHost = Request.Host.ToString();
                    APP app = _appService.GetAPP(appId);
                    if (app == null)
                    {
                        result.ErrCode = "40001";
                        result.ErrMsg = ErrCode.err40001;
                    }
                    else
                    {
                        if (!app.RequestUrl.Contains(strHost, StringComparison.Ordinal) && !strHost.Contains("localhost", StringComparison.Ordinal))
                        {
                            result.ErrCode = "40002";
                            result.ErrMsg = ErrCode.err40002 + "，你目前請求主機：" + strHost;
                        }
                        else
                        {
                            SystemType systemType = _systemTypeService.GetByCode(systemCode);
                            if (systemType == null)
                            {
                                result.ErrMsg = ErrCode.err40006;
                            }
                            else
                            {
                                AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
                                object cacheOpenmf = AurobaseCacheHelper.Get("openmf" + openmf);
                                AurobaseCacheHelper.Remove("openmf" + openmf);
                                if (cacheOpenmf == null)
                                {
                                    result.ErrCode = "40007";
                                    result.ErrMsg = ErrCode.err40007;
                                }
                                else
                                {
                                    User user = _userService.Get(cacheOpenmf.ToString());
                                    if (user != null)
                                    {
                                        result.Success = true;
                                        JwtOption jwtModel = App.GetService<JwtOption>();
                                        TokenProvider tokenProvider = new TokenProvider(jwtModel);
                                        TokenResult tokenResult = tokenProvider.LoginToken(user, appId);
                                        AurobaseCurrentUser currentSession = new AurobaseCurrentUser
                                        {
                                            UserId = user.Id,
                                            Name = user.RealName,
                                            AccessToken = tokenResult.AccessToken,
                                            AppKey = appId,
                                            CreateTime = DateTime.Now,
                                            Role = _roleService.GetRoleEnCode(user.RoleId),
                                            ActiveSystemId = systemType.Id,
                                            CurrentLoginIP = strIp,
                                            IPAddressName = ipAddressName,
                                            ActiveSystemUrl = systemType.Url

                                        };
                                        TimeSpan expiresSliding = DateTime.Now.AddMinutes(120) - DateTime.Now;
                                        AurobaseCacheHelper.Add("login_user_" + user.Id, currentSession, expiresSliding, true);
                                        CurrentUser = currentSession;
                                        result.ResData = currentSession;
                                        result.ErrCode = ErrCode.successCode;
                                        result.Success = true;
                                    }
                                    else
                                    {
                                        result.ErrCode = ErrCode.failCode;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 棄用介面演示
        /// </summary>
        /// <returns></returns>
        [HttpGet("TestLogin")]
        [Obsolete]
        [ApiVersion("2.0")]
        public IActionResult TestLogin()
        {
            CommonResult result = new CommonResult();
            result.Success = true;
            result.ErrCode = ErrCode.successCode;
            result.ResData = "棄用介面演示";
            result.ErrMsg = "成功退出";
            return ToJsonContent(result);
        }
    }
}