using System;
using Aurobase.Commons.IServices;
using Aurobase.CMS.Dtos;
using Aurobase.CMS.Models;
using System.Threading.Tasks;
using Aurobase.Commons.Pages;
using Aurobase.Commons.Dtos;
using System.Collections.Generic;

namespace Aurobase.CMS.IServices
{
    /// <summary>
    /// 定義文章服務介面
    /// </summary>
    public interface IArticlenewsService : IService<Articlenews, ArticlenewsOutputDto, string>
    {
        /// <summary>
        /// 根據使用者角色獲取分類及該分類的文章
        /// </summary>
        /// <returns></returns>
        Task<List<CategoryArticleOutputDto>> GetCategoryArticleList();
    }
}
