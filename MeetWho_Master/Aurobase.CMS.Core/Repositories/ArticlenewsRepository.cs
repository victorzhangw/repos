using System;

using Aurobase.Commons.Repositories;
using Aurobase.CMS.IRepositories;
using Aurobase.CMS.Models;
using Aurobase.Commons.IDbContext;

namespace Aurobase.CMS.Repositories
{
    /// <summary>
    /// 文章倉儲介面的實現
    /// </summary>
    public class ArticlenewsRepository : BaseRepository<Articlenews, string>, IArticlenewsRepository
    {
        public ArticlenewsRepository()
        {
        }

        public ArticlenewsRepository(IDbContextCore context) : base(context)
        {
        }
    }
}