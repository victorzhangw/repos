using System;
using Aurobase.Commons.Repositories;
using Aurobase.CMS.IRepositories;
using Aurobase.CMS.Models;
using Aurobase.Commons.IDbContext;

namespace Aurobase.CMS.Repositories
{
    /// <summary>
    /// 文章分類倉儲介面的實現
    /// </summary>
    public class ArticlecategoryRepository : BaseRepository<Articlecategory, string>, IArticlecategoryRepository
    {
        public ArticlecategoryRepository()
        {
        }

        public ArticlecategoryRepository(IDbContextCore context) : base(context)
        {
        }
    }
}