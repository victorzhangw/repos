using System;
using Aurobase.Commons.IRepositories;
using Aurobase.CMS.Models;

namespace Aurobase.CMS.IRepositories
{
    /// <summary>
    /// 定義文章分類倉儲介面
    /// </summary>
    public interface IArticlecategoryRepository : IRepository<Articlecategory, string>
    {
    }
}