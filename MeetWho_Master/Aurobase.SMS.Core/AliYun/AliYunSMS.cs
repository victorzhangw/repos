using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Http;
using Aliyun.Acs.Core.Profile;
using Newtonsoft.Json;
using System;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Encrypt;
using Aurobase.Commons.Json;
using Aurobase.Commons.Log;
using Aurobase.Commons.Options;

namespace Aurobase.SMS.AliYun
{
    /// <summary>
    /// 阿里云簡訊介面
    /// </summary>
    public class AliYunSMS
    {
        //產品名稱:云通訊簡訊API產品
        private const string product = "Dysmsapi";
        //簡訊API產品域名
        private const string domain = "dysmsapi.aliyuncs.com";

        public AliYunSMS()
        {
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            AppSetting sysSetting = AurobaseCacheHelper.Get<AppSetting>("SysSetting");
            if (sysSetting != null)
            {
                this.Appkey = DEncrypt.Decrypt(sysSetting.Smsusername);
                this.Appsecret = DEncrypt.Decrypt(sysSetting.Smspassword);
                this.SignName = sysSetting.SmsSignName;
            }
        }


        /// <summary>
        /// 群發
        /// </summary>
        /// <param name="phoneNumbers">必填:待發送手機號。支援JSON格式的批量呼叫，批量上限為100個手機號碼</param>
        /// <param name="TemplateCode">必填:簡訊模板</param>
        /// <param name="message">必填:模板中的變數替換JSON串,如模板內容為"親愛的${name},您的驗證碼為${code}"時,此處的值為 "{\"name\":\"Tom\"， \"code\":\"123\"}"</param>
        /// <param name="returnMsg"></param>
        /// <param name="OutId">可選:outId為提供給業務方擴充套件欄位,最終在簡訊回執訊息中將此值帶回給呼叫者</param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public bool Send(string[] phoneNumbers, string TemplateCode, string message, out string returnMsg, string OutId = "", string speed = "1")
        {
            if ((((phoneNumbers == null) || (phoneNumbers.Length == 0)) || string.IsNullOrEmpty(message)) || (message.Trim().Length == 0))
            {
                returnMsg = "手機號碼和訊息內容不能為空";
                return false;
            }
            string phoneNumbersTot = string.Join(",", phoneNumbers);

            return this.Send(phoneNumbersTot, TemplateCode, message, out returnMsg, OutId, "0");

        }



        /// <summary>
        /// 簡訊發送
        /// </summary>
        /// <param name="cellPhone">必填:待發送手機號。支援以逗號分隔的形式進行批量呼叫，批量上限為1000個手機號碼,批量呼叫相對於單條呼叫及時性稍有延遲,驗證碼型別的簡訊推薦使用單條呼叫的方式，發送國際/港澳臺訊息時，接收號碼格式為00+國際區號+號碼，如「0085200000000」</param>
        /// <param name="templateCode">模板code</param>
        /// <param name="OutId">可選:outId為提供給業務方擴充套件欄位,最終在簡訊回執訊息中將此值帶回給呼叫者</param>
        /// <param name="message">可選:模板中的變數替換JSON串,如模板內容為"親愛的${name},您的驗證碼為${code}"時,此處的值為 "{\"name\":\"Tom\"， \"code\":\"123\"}"</param>
        /// <param name="returnMsg"></param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public bool Send(string cellPhone, string templateCode, string message, out string returnMsg, string OutId = "", string speed = "0")
        {
            if ((string.IsNullOrEmpty(cellPhone) || (cellPhone.Trim().Length == 0)))
            {
                returnMsg = "手機號碼和訊息內容不能為空";
                return false;
            }
            if (string.IsNullOrEmpty(message))
            {
                message = "";
            }
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", Appkey, Appsecret);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            CommonRequest request = new CommonRequest();
            if (string.IsNullOrEmpty(templateCode) || templateCode.Length <= 0)
            {
                templateCode = "SMS_139390116";//阿里云自帶的
            }
            try
            {
                request.Domain = domain;
                request.Method = MethodType.POST;
                request.Action = "SendSms";
                request.Version = "2017-05-25";
                request.Product = "Dysmsapi";
                //必填:待發送手機號。支援以逗號分隔的形式進行批量呼叫，批量上限為20個手機號碼,批量呼叫相對於單條呼叫及時性稍有延遲,驗證碼型別的簡訊推薦使用單條呼叫的方式
                request.AddQueryParameters("PhoneNumbers", cellPhone);
                //必填:簡訊簽名-可在簡訊控制檯中找到
                request.AddQueryParameters("SignName", SignName);
                //必填:簡訊模板-可在簡訊控制檯中找到
                request.AddQueryParameters("TemplateCode", templateCode);
                //可選:模板中的變數替換JSON串,如模板內容為"親愛的${name},您的驗證碼為${code}"時,此處的值為 "{\"name\":\"Tom\"， \"code\":\"123\"}"

                request.AddQueryParameters("TemplateParam", message);

                //可選:outId為提供給業務方擴充套件欄位,最終在簡訊回執訊息中將此值帶回給呼叫者
                if (string.IsNullOrEmpty(OutId) || OutId.Length == 0)
                {
                    OutId = DateTime.Now.Ticks.ToString();
                }
                request.AddQueryParameters("OutId", OutId);
                //請求失敗這裡會拋ClientException異常
                CommonResponse commonResponse = new CommonResponse();
                commonResponse = acsClient.GetCommonResponse(request);
                SMSResult sMSResult = JsonConvert.DeserializeObject<SMSResult>(commonResponse.Data);
                returnMsg = sMSResult.Message;
                if (sMSResult.Code == "OK")
                {
                    return true;
                }
                else
                {
                    Log4NetHelper.Error("發送簡訊錯誤，" + commonResponse.ToJson().ToString());
                    return false;
                }


            }
            catch (ServerException e)
            {
                returnMsg = "未知錯誤(ServerException 1)" + e.ErrorMessage;
                return false;
            }
            catch (ClientException e)
            {
                returnMsg = "未知錯誤(ClientException 2)" + e.ErrorMessage;
                return false;
            }
        }
        /// <summary>
        /// Appkey 應用Id
        /// </summary>
        public string Appkey { get; set; }
        /// <summary>
        /// Appsecret應用金鑰
        /// </summary>
        public string Appsecret { get; set; }

        /// <summary>
        /// 簽名
        /// </summary>
        public string SignName { get; set; }

    }
}
