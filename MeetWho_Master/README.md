### 如果對您有幫助，您可以點右上角 "Star" 支援一下，這樣我們才有繼續免費下去的動力，謝謝！

### 概述
YuebonCore是基於.Net6.0開發的許可權管理及快速開發框架，整合應用最新技術包括Asp.NetCore MVC、Dapper、WebAPI、Swagger、EF、Vue等，核心模組包括：組織機構、角色使用者、許可權授權、多系統、多應用管理、定時任務、業務單據編碼規則、程式碼產生器等。它的架構易於擴充套件，規範了一套業務實現的程式碼結構與操作流程，使YuebonCore框架更易於應用到實際專案開發中。

YuebonCore FW其核心設計目標是開發迅速、程式碼量少、學習簡單、功能強大、輕量級、易擴充套件，讓Web開發更快速、簡單，解決70%重複工作。輕鬆開發，專注您的業務，從YuebonCore FW開始！

### 線上體驗

Vue版本體驗地址：[http://netvue.ts.yuebon.com/](http://netvue.ts.yuebon.com)（使用者名稱：test，密碼：test123）

WebApi介面地址：[http://netcoreapi.ts.yuebon.com](http://netcoreapi.ts.yuebon.com)

官方文件：[http://docs.v.yuebon.com/](http://docs.v.yuebon.com/)

視訊教程：[點選觀看](https://space.bilibili.com/1615836206)或QQ群下載觀看

交流QQ群: [90311523](https://jq.qq.com/?_wv=1027&k=p6IUTzDF)


### 更新日誌

更新日誌 [點選檢視](https://gitee.com/yuebon/YuebonNetCore/commits/master)

### 核心看點

使用 MIT 協議，完整開源。採用主流框架，容易上手，簡單易學，學習成本低。可完全實現二次開發、基本滿足80%專案需求。

程式碼產生器可以幫助解決.NET專案70%的重複工作，讓開發更多關注業務邏輯。既能快速提高開發效率，幫助公司節省人力成本，同時又不失靈活性。

操作許可權控制精密細緻，對所有管理鏈接都進行許可權驗證，可控制到導航菜單、功能按鈕。

數據許可權（精細化數據許可權控制，可以設定角色可以訪問的數據範圍，部門、工作組、公司數據）

常用類封裝，日誌、快取、驗證、字典、檔案、郵件、,Excel。等等，目前相容瀏覽器（IE11+、Chrome、Firefox、360瀏覽器等）

適用範圍：可以開發OA、ERP、BPM、CRM、WMS、TMS、MIS、BI、電商平臺後臺、物流管理系統、快遞管理系統、教務管理系統等各類管理軟體。


### 技術介紹

前端目前採用Vue家族前端技術。

####  前端技術 

Vue版前端技術棧 ：基於vue、vuex、vue-router 、vue-cli 、axios 和 element-ui，，前端採用vscode工具開發

#### 後端技術

核心框架：.Net6.0 + Web API + Dapper + EF + AutoMapper + swagger

定時計劃任務：Quartz.Net元件

安全支援：過濾器、Sql注入、請求偽造

服務端驗證：實體模型驗證、自己封裝Validator

快取框架：微軟自帶Cache、Redis

日誌管理：Log4net、登錄日誌、操作日誌

工具類：NPOI、驗證碼、豐富公共功能

效能分析：MiniProfiler元件

### 專案結構

Yuebon.NetCore解決方案包含：

Yuebon.Commons[基礎類庫]：包框架的核心元件，包含一系列快速開發中經常用到的Utility輔助工具功能，框架各個元件的核心介面定義，部分核心功能的實現；

Yuebon.Security.Core[許可權管理類庫]：以Security為基礎實現以角色-功能、使用者-功能的功能許可權實現，以角色-數據，使用者-數據的數據許可權的封裝

Yuebon.AspNetCore[AspNetCore類庫]，提供AspNetCore的服務端功能的封裝，支援webapi和webmvc模式，同時支援外掛式開發；

Yuebon.Cms.Core[CMS基礎類庫]，包含文章管理、廣告管理等內容，以此做案例給大家開發參考。

Yuebon.WebApi[webapi介面]：為Vue版或其他三方系統提供介面服務。

DataBase是最新數據庫備份檔案，目前支援MS SQL Server和MySql。


### 內建功能

1、系統設定：對系統動態配置常用參數。

2、使用者管理：使用者是系統操作者，該功能主要完成系統使用者配置。

3、組織機構：配置系統組織機構（公司、部門、小組），樹結構展現支援數據許可權。

4、角色管理：角色菜單許可權分配、設定角色按機構進行數據範圍許可權劃分。

5、字典管理：對系統中經常使用的一些較為固定的數據進行維護。

6、功能模組：配置系統菜單，操作許可權，按鈕許可權標識等。

7、定時任務：線上（新增、修改、刪除)任務排程包含執行結果日誌。

8、程式碼產生：前後端程式碼的產生（.cs、.vue、.js）程式碼。

9、日誌管理：系統正常操作日誌、登錄日誌記錄和查詢；系統異常資訊日誌記錄和查詢。

10、多應用管理：支援應用呼叫api授權控制。

11、多系統管理：實現各子系統的統一管理和授權。

13、業務單據編碼規則：可以按常量、日期、計數、時間等自定義業務單據編碼規則。

14、簡訊和郵件：整合騰訊云簡訊通知和EMail發送通知

15、支援租戶模式

16、支援使用者定義主題風格

17、支援一主多從數據庫讀寫分離



### 部分界面展示

1、登錄
![輸入圖片說明](https://images.gitee.com/uploads/images/2021/0124/092120_64eb54dc_1017224.png "1、登錄.jpg")

2、系統模組和功能管理
![輸入圖片說明](https://images.gitee.com/uploads/images/2021/0124/092135_56b7d10b_1017224.png "4.png")

3、使用者管理多角色
![輸入圖片說明](https://images.gitee.com/uploads/images/2020/0423/211818_f13ba83a_1017224.png "使用者管理多角色.png")

4、角色管理
![輸入圖片說明](https://images.gitee.com/uploads/images/2020/0423/211842_be7657c3_1017224.png "2.png")

5、應用管理
支援多個應用分別設定appId和金鑰，適用於多個應用訪問介面，每個應用採用jwt標準化token驗證訪問介面。
![輸入圖片說明](https://images.gitee.com/uploads/images/2020/0423/211927_40dbbbf1_1017224.png "應用管理.png")

6、數據字典
![輸入圖片說明](https://images.gitee.com/uploads/images/2020/0423/212216_0a8dc479_1017224.png "3.png")

7、多系統
![輸入圖片說明](https://images.gitee.com/uploads/images/2020/0423/212234_a23c3a34_1017224.png "多子系統管理.png")

8、日誌管理
![輸入圖片說明](https://images.gitee.com/uploads/images/2020/0423/212256_8a482274_1017224.png "日誌管理.png")

9、定時任務

![列表](https://images.gitee.com/uploads/images/2020/0930/145634_ed46d73b_1017224.png "螢幕截圖.png")

本地任務

![本地任務](https://images.gitee.com/uploads/images/2021/0124/092152_d941062d_1017224.png "螢幕截圖.png")

10、程式碼產生器
支援一鍵產生服務端程式碼和前端程式碼，複製貼上簡單快速高效實現功能
![輸入圖片說明](https://images.gitee.com/uploads/images/2021/0124/092205_a7c39eae_1017224.png "程式碼產生器.png")

11、WebApi 整合Swagger
![輸入圖片說明](https://images.gitee.com/uploads/images/2018/0719/120718_772240d6_1017224.png "9 webapi.png")
![輸入圖片說明](https://images.gitee.com/uploads/images/2018/0719/120732_0776845c_1017224.png "9-1 webapi.png")



### 部分應用案例

1、做個車吧

2、展途汽車

3、視奇光學倉庫發貨系統

4、金寶龍學校數據分析系統

5、匯聚自動化裝置

6、天逸電器訂單系統

### 開發者資訊

系統名稱：YuebonCore快速開發平臺

系統作者：YuebonCore團隊

版權所有：YuebonCore開發團隊出品

開源協議：Mit協議


### 各分支說明

|  分支      |     說明         
|-----------|---------------
| master    | 正式發佈的主分支，通常這個分支比較穩定，可以用於生產環境。
| dev | 1、開發分支，此分支通常為 Beta 版本，新版本都會先在此分支中進行開發，最後推送穩定版到 master 分支，如果想對新功能先睹為快，可以使用此分支。<br> 2、建議 Pull Request 的程式碼都到這個分支下，而不是 master
| 其他分支   | 其他分支請忽略。


### 友情鏈接

[IoTClient](https://gitee.com/zhaopeiym/IoTClient)

開源地址：[https://gitee.com/zhaopeiym/IoTClient](https://gitee.com/zhaopeiym/IoTClient)

這是一個物聯網裝置通訊協議實現客戶端，將包括主流PLC通訊讀取、ModBus協議、Bacnet協議等常用工業通訊協議

[aistudio.-wpf.-aclient](https://gitee.com/akwkevin/aistudio.-wpf.-aclient)

開源地址：[https://gitee.com/akwkevin/aistudio.-wpf.-aclient](https://gitee.com/akwkevin/aistudio.-wpf.-aclient)

使用Prism做MVVM實現Wpf客戶端應用程式。目使用的控制元件庫Util.Controls也是開源的， 完全相容MahApps.Metro ，可以與MahApps.Metro同時使用。

### 社區


歡迎你加入我們一起共商、共建、共享技術成果！開源讓我們進步，開源讓我們開闊視野！

有任何疑問加微信cqinwn或加入[QQ群 90311523](https://jq.qq.com/?_wv=1027&k=p6IUTzDF)，點選鏈接加入群聊[【YuebonCore交流群】](https://jq.qq.com/?_wv=1027&k=p6IUTzDF)

如果對您有幫助，您可以點 "Star" 支援一下，這樣我們才有繼續免費下去的動力，謝謝！