using System;
using Aurobase.Commons.Services;
using Aurobase.Security.Dtos;
using Aurobase.Security.IRepositories;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.Security.Services
{
    public class DbBackupService : BaseService<DbBackup, DbBackupOutputDto, string>, IDbBackupService
    {
        private readonly IDbBackupRepository _repository;
        private readonly ILogService _logService;
        public DbBackupService(IDbBackupRepository repository, ILogService logService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
        }
    }
}