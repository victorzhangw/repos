using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    /// <summary>
    /// 組織倉儲介面
    /// 這裡用到的Organize業務對象，是領域對像
    /// </summary>
    public interface IOrganizeRepository : IRepository<Organize, string>
    {
        /// <summary>
        /// 獲取根節點組織
        /// </summary>
        /// <param name="id">組織Id</param>
        /// <returns></returns>
        Organize GetRootOrganize(string id);
    }
}