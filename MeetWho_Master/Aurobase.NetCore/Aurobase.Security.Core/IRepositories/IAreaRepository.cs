using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface IAreaRepository : IRepository<Area, string>
    {
    }
}