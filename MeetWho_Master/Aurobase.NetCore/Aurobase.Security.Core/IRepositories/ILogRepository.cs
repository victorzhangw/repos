using System;
using System.Threading.Tasks;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface ILogRepository : IRepository<Log, string>
    {
        long InsertTset(int len);
    }
}