using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface ISystemTypeRepository : IRepository<SystemType, string>
    {
        /// <summary>
        /// 根據系統編碼查詢系統對像
        /// </summary>
        /// <param name="appkey">系統編碼</param>
        /// <returns></returns>
        SystemType GetByCode(string appkey);
    }
}
