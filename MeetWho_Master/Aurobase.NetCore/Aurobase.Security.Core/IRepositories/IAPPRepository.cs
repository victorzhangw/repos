using System;
using System.Collections.Generic;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAPPRepository : IRepository<APP, string>
    {
        /// <summary>
        /// 獲取app對像
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <param name="secret">應用金鑰AppSecret</param>
        /// <returns></returns>
        APP GetAPP(string appid, string secret);

        /// <summary>
        /// 獲取app對像
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <returns></returns>
        APP GetAPP(string appid);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppOutputDto> SelectApp();
    }
}