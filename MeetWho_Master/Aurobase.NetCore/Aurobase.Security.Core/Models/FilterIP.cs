using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Security.Models
{
    /// <summary>
    /// 過濾IP，數據實體對像
    /// </summary>

    [Table("Sys_FilterIP")]
    [Serializable]
    public class FilterIP : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 預設建構函式（需要初始化屬性的在此處理）
        /// </summary>

        public FilterIP()
        {

        }

        #region Property Members


        /// <summary>
        /// 型別
        /// </summary>
        public virtual bool? FilterType { get; set; }

        /// <summary>
        /// 開始IP
        /// </summary>
        public virtual string StartIP { get; set; }

        /// <summary>
        /// 結束IP
        /// </summary>
        public virtual string EndIP { get; set; }

        /// <summary>
        /// 排序碼
        /// </summary>
        public virtual int? SortCode { get; set; }


        /// <summary>
        /// 描述
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// 刪除標誌
        /// </summary>
        public virtual bool? DeleteMark { get; set; }

        /// <summary>
        /// 有效標誌
        /// </summary>
        public virtual bool EnabledMark { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 建立使用者主鍵
        /// </summary>
        [MaxLength(50)]
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 最後修改時間
        /// </summary>
        public virtual DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 最後修改使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string LastModifyUserId { get; set; }

        /// <summary>
        /// 刪除時間
        /// </summary>
        public virtual DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string DeleteUserId { get; set; }


        #endregion

    }
}