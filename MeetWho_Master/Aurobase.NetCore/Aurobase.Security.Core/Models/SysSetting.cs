using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Aurobase.Commons.Options;

namespace Aurobase.Security.Models
{
    /// <summary>
    /// 系統配置
    /// </summary>
    [Serializable]
    public class SysSetting : AppSetting
    {
    }
}
