
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Models;

namespace Aurobase.Security.Models
{
    /// <summary>
    /// 角色授權表，數據實體對像
    /// </summary>
    [Table("Sys_RoleAuthorize")]
    [Serializable]
    public class RoleAuthorize : BaseEntity<string>, ICreationAudited
    {
        /// <summary>
        /// 預設建構函式（需要初始化屬性的在此處理）
        /// </summary>
        public RoleAuthorize()
        {
            this.Id = GuidUtils.CreateNo();

        }

        #region Property Members


        /// <summary>
        /// 專案型別功能標識 0-子系統 1-標識菜單/模組，2標識按鈕功能
        /// </summary>
        public virtual int? ItemType { get; set; }

        /// <summary>
        /// 專案主鍵
        /// </summary>
        public virtual string ItemId { get; set; }

        /// <summary>
        /// 對像分類/型別1-角色，2-部門，3-使用者
        /// </summary>
        public virtual int? ObjectType { get; set; }

        /// <summary>
        /// 對像主鍵，對像分類/型別為角色時就是角色ID，部門就是部門ID，使用者就是使用者ID
        /// </summary>
        public virtual string ObjectId { get; set; }

        /// <summary>
        /// 排序碼
        /// </summary>
        public virtual int? SortCode { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 建立使用者主鍵
        /// </summary>
        [MaxLength(50)]
        public virtual string CreatorUserId { get; set; }

        #endregion

    }
}