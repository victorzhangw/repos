using System;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    /// <summary>
    /// 序號編碼規則表倉儲介面的實現
    /// </summary>
    public class SequenceRuleRepository : BaseRepository<SequenceRule, string>, ISequenceRuleRepository
    {
        public SequenceRuleRepository()
        {
        }

        public SequenceRuleRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}