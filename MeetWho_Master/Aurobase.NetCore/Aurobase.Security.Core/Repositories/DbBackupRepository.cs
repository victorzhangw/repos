using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    public class DbBackupRepository : BaseRepository<DbBackup, string>, IDbBackupRepository
    {
        public DbBackupRepository()
        {
        }

        public DbBackupRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}