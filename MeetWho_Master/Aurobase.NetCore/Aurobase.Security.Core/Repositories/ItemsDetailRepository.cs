using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    public class ItemsDetailRepository : BaseRepository<ItemsDetail, string>, IItemsDetailRepository
    {
        public ItemsDetailRepository()
        {
        }

        public ItemsDetailRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }

    }
}