using Dapper;
using System.Collections.Generic;
using System.Linq;
using Aurobase.Commons.DbContextCore;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.Dtos;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    /// <summary>
    /// 應用倉儲實現
    /// </summary>
    public class APPRepository : BaseRepository<APP, string>, IAPPRepository
    {
        public APPRepository()
        {
        }
        public APPRepository(IDbContextCore context) : base(context)
        {
        }
        /// <summary>
        /// 獲取app對像
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <param name="secret">應用金鑰AppSecret</param>
        /// <returns></returns>
        public APP GetAPP(string appid, string secret)
        {
            string sql = @"SELECT * FROM Sys_APP t WHERE t.AppId = @AppId and AppSecret=@AppSecret and EnabledMark=1";
            return DapperConnRead.QueryFirstOrDefault<APP>(sql, new { AppId = appid, AppSecret = secret });
        }

        /// <summary>
        /// 獲取app對像
        /// </summary>
        /// <param name="appid">應用ID</param>
        /// <returns></returns>
        public APP GetAPP(string appid)
        {
            string sql = @"SELECT * FROM Sys_APP t WHERE t.AppId = @AppId and EnabledMark=1";
            return DapperConnRead.QueryFirstOrDefault<APP>(sql, new { AppId = appid });

        }
        public IList<AppOutputDto> SelectApp()
        {
            const string query = @"select a.*,u.id as Id,u.NickName,u.Account,u.HeadIcon from Sys_APP a,Sys_User u where a.CreatorUserId=u.Id ";
            return DapperConnRead.Query<AppOutputDto, User, AppOutputDto>(query, (app, user) => { app.UserInfo = user; return app; }, null, splitOn: "Id").ToList<AppOutputDto>();
        }
    }
}