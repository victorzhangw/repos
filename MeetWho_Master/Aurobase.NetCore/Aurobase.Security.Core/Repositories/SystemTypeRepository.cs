using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    /// <summary>
    /// 系統型別，也是子系統
    /// </summary>
    public class SystemTypeRepository : BaseRepository<SystemType, string>, ISystemTypeRepository
    {
        public SystemTypeRepository()
        {
        }

        public SystemTypeRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// 根據系統編碼查詢系統對像
        /// </summary>
        /// <param name="appkey">系統編碼</param>
        /// <returns></returns>
        public SystemType GetByCode(string appkey)
        {
            string sql = @"SELECT * FROM " + this.tableName + " t WHERE t.EnCode = @EnCode";
            return DapperConn.QueryFirstOrDefault<SystemType>(sql, new { EnCode = appkey });
        }
    }
}
