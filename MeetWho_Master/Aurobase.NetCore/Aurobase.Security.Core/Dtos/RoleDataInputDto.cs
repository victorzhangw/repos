using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入對像模型
    /// </summary>
    [AutoMap(typeof(RoleData))]
    [Serializable]
    public class RoleDataInputDto : IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 型別，company-公司，dept-部門，person-個人
        /// </summary>
        public virtual string DType { get; set; }

        /// <summary>
        /// 數據數據，部門ID或個人ID
        /// </summary>
        public virtual string AuthorizeData { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Note { get; set; }


    }
}
