using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入對像模型
    /// </summary>
    [AutoMap(typeof(DbBackup))]
    [Serializable]
    public class DbBackupInputDto : IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BackupType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FileSize { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? BackupTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }


    }
}
