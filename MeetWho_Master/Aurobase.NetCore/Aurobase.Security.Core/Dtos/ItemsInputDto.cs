using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入對像模型
    /// </summary>
    [AutoMap(typeof(Items))]
    [Serializable]
    public class ItemsInputDto : IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool IsTree { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }


    }
}
