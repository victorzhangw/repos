using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入對像模型
    /// </summary>
    [AutoMap(typeof(RoleAuthorize))]
    [Serializable]
    public class RoleAuthorizeInputDto : IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? ItemType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ItemId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? ObjectType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public int? SortCode { get; set; }


    }
}
