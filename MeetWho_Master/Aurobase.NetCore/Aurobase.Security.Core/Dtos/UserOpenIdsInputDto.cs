using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入對像模型
    /// </summary>
    [AutoMap(typeof(UserOpenIds))]
    [Serializable]
    public class UserOpenIdsInputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OpenIdType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OpenId { get; set; }


    }
}
