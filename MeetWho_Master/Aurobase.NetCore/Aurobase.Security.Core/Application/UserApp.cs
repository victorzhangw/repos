using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Aurobase.Commons.Core.App;
using Aurobase.Commons.Encrypt;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.IoC;
using Aurobase.Commons.Mapping;
using Aurobase.Security.Dtos;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.Security.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class UserApp
    {
        IUserService service = App.GetService<IUserService>();
        IUserLogOnService userLogOnService = App.GetService<IUserLogOnService>();
        IRoleService roleService = App.GetService<IRoleService>();
        /// <summary>
        /// 獲取所有使用者資訊
        /// </summary>        
        /// <returns></returns>
        public IEnumerable<User> GetAll()
        {
            return service.GetAll();
        }

        /// <summary>
        /// 根據使用者賬號查詢使用者資訊
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public async Task<User> GetByUserName(string userName)
        {
            return await service.GetByUserName(userName);
        }

        /// <summary>
        /// 根據第三方OpenId查詢使用者資訊
        /// </summary>
        /// <param name="openIdType">第三方型別</param>
        /// <param name="openId">OpenId值</param>
        /// <returns></returns>
        public UserOutputDto GetUserOutDtoByOpenId(string openIdType, string openId)
        {
            return service.GetUserByOpenId(openIdType, openId).MapTo<UserOutputDto>();
        }
        /// <summary>
        /// 根據第三方OpenId查詢使用者資訊
        /// </summary>
        /// <param name="openIdType">第三方型別</param>
        /// <param name="openId">OpenId值</param>
        /// <returns></returns>
        public User GetUserByOpenId(string openIdType, string openId)
        {
            return service.GetUserByOpenId(openIdType, openId);
        }

        /// <summary>
        /// 更新使用者
        /// </summary>
        /// <param name="user">使用者資訊</param>
        /// <returns></returns>
        public bool UpdateUser(User user)
        {
            return service.Update(user, user.Id);
        }
        /// <summary>
        /// 根據使用者ID獲取頭像
        /// </summary>
        /// <param name="userid">使用者ID</param>
        /// <returns></returns>
        public string GetHeadIconById(string userid)
        {
            User user = service.Get(userid);

            if (user != null)
            {
                return user.HeadIcon;
            }
            else
            {
                return "";
            }
        }


        /// <summary>
        /// 查詢使用者資訊
        /// </summary>
        /// <param name="id">使用者Id</param>
        /// <returns></returns>
        public User GetUserById(string id)
        {
            return service.Get(id);
        }
        /// <summary>
        /// 根據使用者id和第三方型別查詢
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="openIdType"></param>
        /// <returns></returns>
        public UserOpenIds GetUserOpenIdById(string userId, string openIdType)
        {
            return service.GetUserOpenIdByuserId(openIdType, userId);
        }
        /// <summary>
        /// 根據微信統一ID（UnionID）查詢使用者
        /// </summary>
        /// <param name="unionId">UnionID</param>
        /// <returns></returns>
        public User GetUserByUnionId(string unionId)
        {
            return service.GetUserByUnionId(unionId);
        }

        /// <summary>
        /// 統計使用者數
        /// </summary>
        /// <returns></returns>
        public int GetCountTotal()
        {
            return service.GetCountByWhere("1=1");
        }
    }
}
