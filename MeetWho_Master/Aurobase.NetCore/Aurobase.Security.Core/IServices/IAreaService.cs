using System;
using System.Collections.Generic;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAreaService : IService<Area, AreaOutputDto, string>
    {

        #region 用於uniapp下拉選項
        /// <summary>
        /// 獲取所有可用的地區，用於uniapp下拉選項
        /// </summary>
        /// <returns></returns>
        List<AreaPickerOutputDto> GetAllByEnable();
        /// <summary>
        /// 獲取省、市、縣/區三級可用的地區，用於uniapp下拉選項
        /// </summary>
        /// <returns></returns>
        List<AreaPickerOutputDto> GetProvinceToAreaByEnable();
        #endregion
    }
}
