using System;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Commons.Models;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 定義單據編碼服務介面
    /// </summary>
    public interface ISequenceService : IService<Sequence, SequenceOutputDto, string>
    {
        /// <summary>
        /// 獲取最新業務單據編碼
        /// </summary>
        /// <param name="sequenceName">業務單據編碼名稱</param>
        /// <returns></returns>
        Task<CommonResult> GetSequenceNextTask(string sequenceName);
        /// <summary>
        /// 獲取最新業務單據編碼
        /// </summary>
        /// <param name="sequenceName">業務單據編碼名稱</param>
        /// <returns></returns>
        CommonResult GetSequenceNext(string sequenceName);
    }
}
