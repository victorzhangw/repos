using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Aurobase.Commons.Enums;
using Aurobase.Commons.IServices;
using Aurobase.Commons.Pages;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 使用者服務介面
    /// </summary>
    public interface IUserService : IService<User, UserOutputDto, string>
    {
        /// <summary>
        /// 使用者登陸驗證。
        /// </summary>
        /// <param name="userName">使用者名稱</param>
        /// <param name="password">密碼（第一次md5加密后）</param>
        /// <returns>驗證成功返回使用者實體，驗證失敗返回null|提示訊息</returns>
        Task<Tuple<User, string>> Validate(string userName, string password);

        /// <summary>
        /// 使用者登陸驗證。
        /// </summary>
        /// <param name="userName">使用者名稱</param>
        /// <param name="password">密碼（第一次md5加密后）</param>
        /// <param name="userType">使用者型別</param>
        /// <returns>驗證成功返回使用者實體，驗證失敗返回null|提示訊息</returns>
        Task<Tuple<User, string>> Validate(string userName, string password, UserType userType);

        /// <summary>
        /// 根據使用者賬號查詢使用者資訊
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<User> GetByUserName(string userName);

        /// <summary>
        /// 根據使用者手機號碼查詢使用者資訊
        /// </summary>
        /// <param name="mobilePhone">手機號碼</param>
        /// <returns></returns>
        Task<User> GetUserByMobilePhone(string mobilePhone);
        /// <summary>
        /// 根據Email、Account、手機號查詢使用者資訊
        /// </summary>
        /// <param name="account">登錄賬號</param>
        /// <returns></returns>
        Task<User> GetUserByLogin(string account);
        /// <summary>
        /// 註冊使用者
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="trans"></param>
        bool Insert(User entity, UserLogOn userLogOnEntity, IDbTransaction trans = null);
        /// <summary>
        /// 註冊使用者
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="trans"></param>
        Task<bool> InsertAsync(User entity, UserLogOn userLogOnEntity, IDbTransaction trans = null);
        /// <summary>
        /// 註冊使用者,第三方平臺
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="userOpenIds"></param>
        /// <param name="trans"></param>
        bool Insert(User entity, UserLogOn userLogOnEntity, UserOpenIds userOpenIds, IDbTransaction trans = null);
        /// <summary>
        /// 根據第三方OpenId查詢使用者資訊
        /// </summary>
        /// <param name="openIdType">第三方型別</param>
        /// <param name="openId">OpenId值</param>
        /// <returns></returns>
        User GetUserByOpenId(string openIdType, string openId);

        /// <summary>
        /// 根據微信UnionId查詢使用者資訊
        /// </summary>
        /// <param name="unionId">UnionId值</param>
        /// <returns></returns>
        User GetUserByUnionId(string unionId);
        /// <summary>
        /// 根據userId查詢使用者資訊
        /// </summary>
        /// <param name="openIdType">第三方型別</param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        UserOpenIds GetUserOpenIdByuserId(string openIdType, string userId);
        /// <summary>
        /// 更新使用者資訊,第三方平臺
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="trans"></param>
        bool UpdateUserByOpenId(User entity, UserLogOn userLogOnEntity, UserOpenIds userOpenIds, IDbTransaction trans = null);




        /// <summary>
        /// 微信註冊普通會員使用者
        /// </summary>
        /// <param name="userInPut">第三方型別</param>
        /// <returns></returns>
        bool CreateUserByWxOpenId(UserInputDto userInPut);
        /// <summary>
        /// 更新使用者
        /// </summary>
        /// <param name="userInPut"></param>
        /// <returns></returns>
        bool UpdateUserByOpenId(UserInputDto userInPut);

        /// <summary>
        /// 根據條件查詢數據庫,並返回對像集合(用於分頁數據顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定對象的集合</returns>
        Task<PageResult<UserOutputDto>> FindWithPagerSearchAsync(SearchUserModel search);
    }
}
