using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// �����ֵ���ϸ
    /// </summary>
    public interface IItemsDetailService : IService<ItemsDetail, ItemsDetailOutputDto, string>
    {
        /// <summary>
        /// ���������ֵ��������ȡ�÷����������
        /// </summary>
        /// <param name="itemCode">�������</param>
        /// <returns></returns>
        Task<List<ItemsDetailOutputDto>> GetItemDetailsByItemCode(string itemCode);

        /// <summary>
        /// ��ȡ������Vue �����б�
        /// </summary>
        /// <param name="itemId">���Id</param>
        /// <returns></returns>
        Task<List<ItemsDetailOutputDto>> GetAllItemsDetailTreeTable(string itemId);
    }
}
