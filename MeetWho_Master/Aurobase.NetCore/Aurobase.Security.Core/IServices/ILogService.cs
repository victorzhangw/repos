using System;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Commons.Pages;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// ��־��¼
    /// </summary>
    public interface ILogService : IService<Log, LogOutputDto, string>
    {
        /// <summary>
        /// ���������Ϣ��д���û��Ĳ�����־��¼
        /// ��Ҫ����д���ݿ���־
        /// </summary>
        /// <param name="tableName">����������</param>
        /// <param name="operationType">��������</param>
        /// <param name="note">������ϸ����</param>
        /// <returns></returns>
        bool OnOperationLog(string tableName, string operationType, string note);

        /// <summary>
        /// ���������Ϣ��д���û��Ĳ�����־��¼
        /// ��Ҫ����д����ģ����־
        /// </summary>
        /// <param name="module">����ģ������</param>
        /// <param name="operationType">��������</param>
        /// <param name="note">������ϸ����</param>
        /// <param name="currentUser">�����û�</param>
        /// <returns></returns>
        bool OnOperationLog(string module, string operationType, string note, AurobaseCurrentUser currentUser);
        /// <summary>
        /// ����������ѯ���ݿ�,�����ض��󼯺�(���ڷ�ҳ������ʾ)
        /// </summary>
        /// <param name="search">��ѯ������</param>
        /// <returns>ָ������ļ���</returns>
        Task<PageResult<LogOutputDto>> FindWithPagerSearchAsync(SearchLogModel search);
    }
}
