using System;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFilterIPService : IService<FilterIP, FilterIPOutputDto, string>
    {
        /// <summary>
        /// ��֤IP��ַ�Ƿ񱻾ܾ�
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        bool ValidateIP(string ip);
    }
}
