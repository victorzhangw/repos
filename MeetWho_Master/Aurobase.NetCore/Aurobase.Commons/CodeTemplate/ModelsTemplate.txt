using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace {ModelsNamespace}
{
    /// <summary>
    /// {TableNameDesc}，數據實體對像
    /// </summary>
    [Table("{TableName}")]
    [Serializable]
    public class {ModelTypeName}:BaseEntity<{KeyTypeName}>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
{ModelContent}
    }
}
