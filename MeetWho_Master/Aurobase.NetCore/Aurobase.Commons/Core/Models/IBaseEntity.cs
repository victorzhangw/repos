using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Models
{
    /// <summary>
    /// 數據模型介面
    /// </summary>
    /// <typeparam name="TKey">實體主鍵型別</typeparam>
    public interface IBaseEntity<out TKey> : IEntity
    {
        /// <summary>
        /// 獲取 實體唯一標識，主鍵
        /// </summary>
        TKey Id { get; }
    }
}
