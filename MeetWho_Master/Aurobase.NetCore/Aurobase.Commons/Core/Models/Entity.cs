using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Models
{
    /// <summary>
    /// 實體基類
    /// </summary>
    public abstract class Entity : IEntity
    {
        /// <summary>
        /// 判斷主鍵是否為空
        /// </summary>
        /// <returns></returns>
        public abstract bool KeyIsNull();

        /// <summary>
        /// 建立預設的主鍵值
        /// </summary>
        public abstract void GenerateDefaultKeyVal();

        /// <summary>
        /// 建構函式
        /// </summary>
        public Entity()
        {
            if (KeyIsNull())
            {
                GenerateDefaultKeyVal();
            }
        }
    }
}
