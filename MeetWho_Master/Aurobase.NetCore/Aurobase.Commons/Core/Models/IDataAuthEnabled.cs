using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Models
{
    /// <summary>
    /// 定義數據許可權的更新，刪除狀態
    /// </summary>
    public interface IDataAuthEnabled
    {
        /// <summary>
        /// 獲取或設定 是否可更新的數據許可權狀態
        /// </summary>
        bool Updatable { get; set; }

        /// <summary>
        /// 獲取或設定 是否可刪除的數據許可權狀態
        /// </summary>
        bool Deletable { get; set; }
    }
}
