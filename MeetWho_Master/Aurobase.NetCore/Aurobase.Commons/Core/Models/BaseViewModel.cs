using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;

namespace Aurobase.Commons.Models
{
    /// <summary>
    /// 所有數據庫檢視對應實體類必須繼承此類
    /// </summary>
    [Serializable]
    public abstract class BaseViewModel : IEntity
    {

    }
}
