using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Core.DataManager
{
    /// <summary>
    /// 數據庫連線配置特性
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct, AllowMultiple = true)]
    public class AppDBContextAttribute : Attribute
    {
        /// <summary>
        /// 數據庫配置名稱
        /// </summary>
        public string DbConfigName { get; set; }
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="dbConfigName"></param>
        public AppDBContextAttribute(string dbConfigName)
        {
            DbConfigName = dbConfigName;
        }
    }
}
