using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Aurobase.Commons.VerificationCode
{
    /// <summary>
    /// 驗證碼返回結果
    /// </summary>
    public class CaptchaResult
    {
        /// <summary>
        /// 驗證碼字串
        /// </summary>
        public string CaptchaCode { get; set; }

        /// <summary>
        /// 驗證碼記憶體流
        /// </summary>
        public MemoryStream CaptchaMemoryStream { get; set; }

        /// <summary>
        /// 驗證碼產生時間
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
