using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.Commons.Enums
{
    /// <summary>
    /// 使用者型別
    /// </summary>
    public enum UserType
    {
        /// <summary>
        /// 會員
        /// </summary>
        Member = 0,
        /// <summary>
        /// 租戶
        /// </summary>
        Tenant,
        /// <summary>
        /// 管理員
        /// </summary>
        Manager
    }
}
