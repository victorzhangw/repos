using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Enums
{
    /// <summary>
    /// 通知操作型別
    /// </summary>
    public enum MsgType
    {
        /// <summary>
        /// 不通知
        /// </summary>
        No = 0,
        /// <summary>
        /// 異常通知
        /// </summary>
        Error = 1,
        /// <summary>
        /// 通知所有
        /// </summary>
        All = 2
    }
}
