using Microsoft.Extensions.Configuration;
using Aurobase.Commons.Core.App;
using Aurobase.Commons.IoC;

namespace Aurobase.Commons
{
    /// <summary>
    /// 配置檔案讀取操作
    /// </summary>
    public class Configs
    {
        /// <summary>
        /// 
        /// </summary>
        public static IConfiguration configuration;
        static Configs()
        {
            configuration = App.GetService<IConfiguration>();

        }
        /// <summary>
        /// 根據Key獲取數配置內容
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IConfigurationSection GetSection(string key)
        {
            return configuration.GetSection(key);
        }
        /// <summary>
        /// 根據section和key獲取配置內容
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigurationValue(string section, string key)
        {
            return GetSection(section)?[key];
        }

        /// <summary>
        /// 根據Key獲取數據庫連線字串
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            return configuration.GetConnectionString(key);
        }
    }
}
