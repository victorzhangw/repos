using AutoMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.Properties;

namespace Aurobase.Commons.Mapping
{
    /// <summary>
    /// 對像對映擴充套件操作
    /// </summary>
    public static class MapperExtensions
    {
        private static IMapper _mapper;

        /// <summary>
        /// 設定對像對映執行者
        /// </summary>
        /// <param name="mapper">對映執行者</param>
        public static void SetMapper(IMapper mapper)
        {
            mapper.CheckNotNull("mapper");
            _mapper = mapper;
        }

        /// <summary>
        /// 將對像對映為指定型別
        /// </summary>
        /// <typeparam name="TTarget">要對映的目標型別</typeparam>
        /// <param name="source">源對像</param>
        /// <returns>目標型別的對象</returns>
        public static TTarget MapTo<TTarget>(this object source)
        {
            CheckMapper();
            return _mapper.Map<TTarget>(source);
        }

        /// <summary>
        /// 使用源型別的對象更新目標型別的對象
        /// </summary>
        /// <typeparam name="TSource">源型別</typeparam>
        /// <typeparam name="TTarget">目標型別</typeparam>
        /// <param name="source">源對像</param>
        /// <param name="target">待更新的目標對像</param>
        /// <returns>更新后的目標型別對像</returns>
        public static TTarget MapTo<TSource, TTarget>(this TSource source, TTarget target)
        {
            CheckMapper();
            return _mapper.Map(source, target);
        }

        /// <summary>
        /// 將數據源對映為指定<typeparamref name="TOutputDto"/>的集合
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TOutputDto"></typeparam>
        /// <param name="source"></param>
        /// <param name="membersToExpand"></param>
        /// <returns></returns>
        public static IQueryable<TOutputDto> ToOutput<TEntity, TOutputDto>(this IQueryable<TEntity> source,
            params Expression<Func<TOutputDto, object>>[] membersToExpand)
        {
            CheckMapper();
            return _mapper.ProjectTo<TOutputDto>(source, membersToExpand);
        }

        /// <summary>
        /// 集合到集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>

        public static List<T> MapTo<T>(this IEnumerable obj)
        {
            CheckMapper();
            return _mapper.Map<List<T>>(obj);
        }

        /// <summary>
        /// 驗證對映執行者是否為空
        /// </summary>
        private static void CheckMapper()
        {
            if (_mapper == null)
            {
                throw new NullReferenceException(Resources.Map_MapperIsNull);
            }
        }
    }
}
