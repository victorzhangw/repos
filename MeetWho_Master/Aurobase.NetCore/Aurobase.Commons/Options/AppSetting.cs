using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Aurobase.Commons.Options
{
    /// <summary>
    /// 應用設定實體類
    /// </summary>
    [Serializable]
    public class AppSetting
    {
        #region 系統基本資訊
        /// <summary>
        /// 系統名稱
        /// </summary>
        [XmlElement("SoftName")]
        public string SoftName { get; set; }
        /// <summary>
        /// 系統簡介
        /// </summary>
        [XmlElement("SoftSummary")]
        public string SoftSummary { get; set; }
        /// <summary>
        /// 訪問域名
        /// </summary>
        [XmlElement("WebUrl")]
        public string WebUrl { get; set; }
        /// <summary>
        /// Logo
        /// </summary>
        [XmlElement("SysLogo")]
        public string SysLogo { get; set; }
        /// <summary>
        /// 公司名稱
        /// </summary>
        [XmlElement("CompanyName")]
        public string CompanyName { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [XmlElement("Address")]
        public string Address { get; set; }
        /// <summary>
        /// 電話
        /// </summary>
        [XmlElement("Telphone")]
        public string Telphone { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        [XmlElement("Email")]
        public string Email { get; set; }
        /// <summary>
        /// ICP備案號
        /// </summary>
        [XmlElement("ICPCode")]
        public string ICPCode { get; set; }
        /// <summary>
        /// 公安備案號
        /// </summary>
        [XmlElement("PublicSecurityCode")]
        public string PublicSecurityCode { get; set; }
        /// <summary>
        /// 分享標題
        /// </summary>
        [XmlElement("ShareTitle")]
        public string ShareTitle { get; set; }
        /// <summary>
        /// 微信公眾號分享圖片
        /// </summary>
        [XmlElement("ShareWeChatImage")]
        public string ShareWeChatImage { get; set; }
        /// <summary>
        /// 微信小程式分享圖片
        /// </summary>
        [XmlElement("ShareWxAppletImage")]
        public string ShareWxAppletImage { get; set; }

        /// <summary>
        /// 微信推廣二維碼背景圖片
        /// </summary>
        [XmlElement("ShareBackgroundImage")]
        public string ShareBackgroundImage { get; set; }
        #endregion


        #region 功能許可權設定
        /// <summary>
        /// URL重寫開關
        /// </summary>
        [XmlElement("Staticstatus")]
        public string Staticstatus { get; set; }
        /// <summary>
        /// 靜態URL後綴
        /// </summary>
        [XmlElement("Staticextension")]
        public string Staticextension { get; set; }
        /// <summary>
        /// 開啟會員功能
        /// </summary>
        [XmlElement("Memberstatus")]
        public string Memberstatus { get; set; }
        /// <summary>
        /// 是否開啟網站
        /// </summary>
        [XmlElement("Webstatus")]
        public string Webstatus { get; set; }
        /// <summary>
        /// 網站關閉原因
        /// </summary>
        [XmlElement("Webclosereason")]
        public string Webclosereason { get; set; }
        /// <summary>
        /// 網站統計程式碼
        /// </summary>
        [XmlElement("Webcountcode")]
        public string Webcountcode { get; set; }
        #endregion


        #region 簡訊平臺設定
        /// <summary>
        /// 簡訊AP地址
        /// </summary>
        [XmlElement("Smsapiurl")]
        public string Smsapiurl { get; set; }
        /// <summary>
        /// 平臺登錄賬戶或Appkey
        /// </summary>
        [XmlElement("Smsusername")]
        public string Smsusername { get; set; }
        /// <summary>
        /// 平臺通訊金鑰或Appsecret
        /// </summary>
        [XmlElement("Smspassword")]
        public string Smspassword { get; set; }
        /// <summary>
        /// 簡訊簽名
        /// </summary>
        [XmlElement("SmsSignName")]
        public string SmsSignName { get; set; }
        #endregion


        #region 郵件發送設定
        /// <summary>
        /// SMTP伺服器
        /// </summary>
        [XmlElement("Emailsmtp")]
        public string Emailsmtp { get; set; }
        /// <summary>
        /// SSL加密連線
        /// </summary>
        [XmlElement("Emailssl")]
        public string Emailssl { get; set; }
        /// <summary>
        /// SMTP埠
        /// </summary>
        [XmlElement("Emailport")]
        public string Emailport { get; set; }
        /// <summary>
        /// 發件人地址
        /// </summary>
        [XmlElement("Emailfrom")]
        public string Emailfrom { get; set; }
        /// <summary>
        /// 郵箱賬號
        /// </summary>
        [XmlElement("Emailusername")]
        public string Emailusername { get; set; }
        /// <summary>
        /// 郵箱密碼
        /// </summary>
        [XmlElement("Emailpassword")]
        public string Emailpassword { get; set; }
        /// <summary>
        /// 發件人昵稱
        /// </summary>
        [XmlElement("Emailnickname")]
        public string Emailnickname { get; set; }
        #endregion


        #region 檔案伺服器
        /// <summary>
        /// 檔案伺服器
        /// </summary>
        [XmlElement("Fileserver")]
        public string Fileserver { get; set; }
        /// <summary>
        /// 本地檔案儲存物理物理路徑
        /// </summary>
        [XmlElement("LocalPath")]
        public string LocalPath { get; set; }
        /// <summary>
        /// 阿里云KeyId
        /// </summary>
        [XmlElement("Osssecretid")]
        public string Osssecretid { get; set; }
        /// <summary>
        /// 阿里云SecretKey
        /// </summary>
        [XmlElement("Osssecretkey")]
        public string Osssecretkey { get; set; }
        /// <summary>
        /// 阿里云Bucket
        /// </summary>
        [XmlElement("Ossbucket")]
        public string Ossbucket { get; set; }
        /// <summary>
        /// 阿里云EndPoint
        /// </summary>
        [XmlElement("Ossendpoint")]
        public string Ossendpoint { get; set; }
        /// <summary>
        /// 阿里云繫結域名
        /// </summary>
        [XmlElement("Ossdomain")]
        public string Ossdomain { get; set; }
        #endregion


        #region 檔案上傳設定
        /// <summary>
        /// 檔案上傳目錄
        /// </summary>
        [XmlElement("Filepath")]
        public string Filepath { get; set; }
        /// <summary>
        /// 檔案儲存方式
        /// </summary>
        [XmlElement("Filesave")]
        public string Filesave { get; set; }
        /// <summary>
        /// 編輯器圖片
        /// </summary>
        [XmlElement("Fileremote")]
        public string Fileremote { get; set; }
        /// <summary>
        /// 檔案上傳型別
        /// </summary>
        [XmlElement("Fileextension")]
        public string Fileextension { get; set; }
        /// <summary>
        /// 視訊上傳型別
        /// </summary>
        [XmlElement("Videoextension")]
        public string Videoextension { get; set; }
        /// <summary>
        /// 附件上傳大小
        /// </summary>
        [XmlElement("Attachsize")]
        public string Attachsize { get; set; }
        /// <summary>
        /// 視訊上傳大小
        /// </summary>
        [XmlElement("Videosize")]
        public string Videosize { get; set; }
        /// <summary>
        /// 圖片上傳大小
        /// </summary>
        [XmlElement("Imgsize")]
        public string Imgsize { get; set; }
        /// <summary>
        /// 圖片最大尺寸 高度
        /// </summary>
        [XmlElement("Imgmaxheight")]
        public string Imgmaxheight { get; set; }
        /// <summary>
        /// 圖片最大尺寸 寬度
        /// </summary>
        [XmlElement("Imgmaxwidth")]
        public string Imgmaxwidth { get; set; }
        /// <summary>
        /// 縮圖產生尺寸 高度
        /// </summary>
        [XmlElement("Thumbnailheight")]
        public string Thumbnailheight { get; set; }
        /// <summary>
        /// 縮圖產生尺寸 寬度
        /// </summary>
        [XmlElement("Thumbnailwidth")]
        public string Thumbnailwidth { get; set; }
        /// <summary>
        /// 縮圖產生方式
        /// </summary>
        [XmlElement("Thumbnailmode")]
        public string Thumbnailmode { get; set; }
        /// <summary>
        /// 圖片水印型別
        /// </summary>
        [XmlElement("Watermarktype")]
        public string Watermarktype { get; set; }
        /// <summary>
        /// 圖片水印位置
        /// </summary>
        [XmlElement("Watermarkposition")]
        public string Watermarkposition { get; set; }
        /// <summary>
        /// 圖片產生質量
        /// </summary>
        [XmlElement("Watermarkimgquality")]
        public string Watermarkimgquality { get; set; }
        /// <summary>
        /// 圖片水印檔案
        /// </summary>
        [XmlElement("Watermarkpic")]
        public string Watermarkpic { get; set; }
        /// <summary>
        /// 水印透明度
        /// </summary>
        [XmlElement("Watermarktransparency")]
        public string Watermarktransparency { get; set; }
        /// <summary>
        /// 水印文字
        /// </summary>
        [XmlElement("Watermarktext")]
        public string Watermarktext { get; set; }
        /// <summary>
        /// 文字字型格式
        /// </summary>
        [XmlElement("Watermarkfont")]
        public string Watermarkfont { get; set; }
        /// <summary>
        /// 文字字型大小
        /// </summary>
        [XmlElement("Watermarkfontsize")]
        public string Watermarkfontsize { get; set; }
        #endregion
    }
}
