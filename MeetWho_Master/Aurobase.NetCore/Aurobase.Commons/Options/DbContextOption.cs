using Aurobase.Commons.Enums;

namespace Aurobase.Commons.Options
{
    /// <summary>
    /// 數據庫上下文配置
    /// </summary>
    public class DbContextOption
    {
        /// <summary>
        /// 數據庫連線字串
        /// </summary>
        public string dbConfigName { get; set; }
        /// <summary>
        /// 實體程式集名稱
        /// </summary>
        public string ModelAssemblyName { get; set; }
        /// <summary>
        /// 數據庫型別
        /// </summary>
        public DatabaseType DbType { get; set; } = DatabaseType.SqlServer;
        /// <summary>
        /// 是否輸出Sql日誌
        /// </summary>
        public bool IsOutputSql;
    }

}
