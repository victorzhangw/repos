using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Aurobase.Commons.Extend
{
    /// <summary>
    /// 根據業務對象的型別進行反射操作輔助類
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Reflect<T> where T : class
    {
        private static Hashtable ObjCache = new Hashtable();
        private static object syncRoot = new Object();

        /// <summary>
        /// 根據參數建立對像實例
        /// </summary>
        /// <param name="sName">對像全域性名稱</param>
        /// <param name="sFilePath">檔案路徑</param>
        /// <returns></returns>
        public static T Create(string sName, string sFilePath)
        {
            return Create(sName, sFilePath, true);
        }

        /// <summary>
        /// 根據參數建立對像實例
        /// </summary>
        /// <param name="sName">對像全域性名稱</param>
        /// <param name="sFilePath">檔案路徑</param>
        /// <param name="bCache">快取集合</param>
        /// <returns></returns>
        public static T Create(string sName, string sFilePath, bool bCache)
        {
            string CacheKey = sName;
            T objType = null;
            if (bCache)
            {
                objType = (T)ObjCache[CacheKey];    //從快取讀取 
                if (!ObjCache.ContainsKey(CacheKey))
                {
                    lock (syncRoot)
                    {
                        objType = CreateInstance(CacheKey, sFilePath);
                        ObjCache.Add(CacheKey, objType);//快取數據訪問對像
                    }
                }
            }
            else
            {
                objType = CreateInstance(CacheKey, sFilePath);
            }

            return objType;
        }

        /// <summary>
        /// 根據全名和路徑構造對像
        /// </summary>
        /// <param name="sName">對像全名</param>
        /// <param name="sFilePath">程式集路徑</param>
        /// <returns></returns>
        private static T CreateInstance(string sName, string sFilePath)
        {
            Assembly assemblyObj = Assembly.Load(sFilePath);
            if (assemblyObj == null)
            {
                throw new ArgumentNullException("sFilePath", string.Format("無法載入sFilePath={0} 的程式集", sFilePath));
            }

            T obj = (T)assemblyObj.CreateInstance(sName); //反射建立 
            return obj;
        }
    }
}
