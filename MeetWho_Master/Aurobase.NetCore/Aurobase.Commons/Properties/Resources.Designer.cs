﻿//------------------------------------------------------------------------------
// <auto-generated>
//     這段程式碼是由工具產生的。
//     執行階段版本:4.0.30319.42000
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     變更將會遺失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aurobase.Commons.Properties {
    using System;
    
    
    /// <summary>
    ///   用於查詢當地語系化字串等的強類型資源類別。
    /// </summary>
    // 這個類別是自動產生的，是利用 StronglyTypedResourceBuilder
    // 類別透過 ResGen 或 Visual Studio 這類工具。
    // 若要加入或移除成員，請編輯您的 .ResX 檔，然後重新執行 ResGen
    // (利用 /str 選項)，或重建您的 VS 專案。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   傳回這個類別使用的快取的 ResourceManager 執行個體。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Aurobase.Commons.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   覆寫目前執行緒的 CurrentUICulture 屬性，對象是所有
        ///   使用這個強類型資源類別的資源查閱。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查詢類似 參數中的字元\&quot;{0}\&quot;不是 {1} 進位制數的有效字元。 的當地語系化字串。
        /// </summary>
        internal static string AnyRadixConvert_CharacterIsNotValid {
            get {
                return ResourceManager.GetString("AnyRadixConvert_CharacterIsNotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 0 的當地語系化字串。
        /// </summary>
        internal static string AnyRadixConvert_Overflow {
            get {
                return ResourceManager.GetString("AnyRadixConvert_Overflow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 快取功能尚未初始化，未找到可用的 ICacheProvider 實現。 的當地語系化字串。
        /// </summary>
        internal static string Caching_CacheNotInitialized {
            get {
                return ResourceManager.GetString("Caching_CacheNotInitialized", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 標識為「{0}」的項重複定義 的當地語系化字串。
        /// </summary>
        internal static string ConfigFile_ItemKeyDefineRepeated {
            get {
                return ResourceManager.GetString("ConfigFile_ItemKeyDefineRepeated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 名稱為「{0}」的型別不存在 的當地語系化字串。
        /// </summary>
        internal static string ConfigFile_NameToTypeIsNull {
            get {
                return ResourceManager.GetString("ConfigFile_NameToTypeIsNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 請先初始化依賴注入服務，再使用OSharpContext.IocRegisterservices屬性 的當地語系化字串。
        /// </summary>
        internal static string Context_BuildservicesFirst {
            get {
                return ResourceManager.GetString("Context_BuildservicesFirst", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 上下文初始化型別「{0}」不存在 的當地語系化字串。
        /// </summary>
        internal static string DbContextInitializerConfig_InitializerNotExists {
            get {
                return ResourceManager.GetString("DbContextInitializerConfig_InitializerNotExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 查詢條件組中的操作型別錯誤，只能為And或者Or。 的當地語系化字串。
        /// </summary>
        internal static string Filter_GroupOperateError {
            get {
                return ResourceManager.GetString("Filter_GroupOperateError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 指定的屬性「{0}」在型別「{1}」中不存在。 的當地語系化字串。
        /// </summary>
        internal static string Filter_RuleFieldInTypeNotFound {
            get {
                return ResourceManager.GetString("Filter_RuleFieldInTypeNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 無法解析型別「{0}」的建構函式中型別為「{1}」的參數 的當地語系化字串。
        /// </summary>
        internal static string Ioc_CannotResolveservice {
            get {
                return ResourceManager.GetString("Ioc_CannotResolveservice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 OSharp框架尚未初始化，請先初始化 的當地語系化字串。
        /// </summary>
        internal static string Ioc_FrameworkNotInitialized {
            get {
                return ResourceManager.GetString("Ioc_FrameworkNotInitialized", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型別「{0}」的實現型別無法找到 的當地語系化字串。
        /// </summary>
        internal static string Ioc_ImplementationTypeNotFound {
            get {
                return ResourceManager.GetString("Ioc_ImplementationTypeNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型別「{0}」中找不到合適參數的建構函式 的當地語系化字串。
        /// </summary>
        internal static string Ioc_NoConstructorMatch {
            get {
                return ResourceManager.GetString("Ioc_NoConstructorMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 實現型別不能為「{0}」，因為該型別與註冊為「{1}」的其他型別無法區分 的當地語系化字串。
        /// </summary>
        internal static string Ioc_TryAddIndistinguishableTypeToEnumerable {
            get {
                return ResourceManager.GetString("Ioc_TryAddIndistinguishableTypeToEnumerable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型別「{0}」不是倉儲介面「IRepository&lt;,&gt;」的派生類。 的當地語系化字串。
        /// </summary>
        internal static string IocInitializerBase_TypeNotIRepositoryType {
            get {
                return ResourceManager.GetString("IocInitializerBase_TypeNotIRepositoryType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型別「{0}」不是操作單元「IUnitOfWork」的派生類。 的當地語系化字串。
        /// </summary>
        internal static string IocInitializerBase_TypeNotIUnitOfWorkType {
            get {
                return ResourceManager.GetString("IocInitializerBase_TypeNotIUnitOfWorkType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 建立名稱為「{0}」的日誌實例時「{1}」返回空實例。 的當地語系化字串。
        /// </summary>
        internal static string Logging_CreateLogInstanceReturnNull {
            get {
                return ResourceManager.GetString("Logging_CreateLogInstanceReturnNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 MapperExtensions.Mapper不能為空，請先設定值 的當地語系化字串。
        /// </summary>
        internal static string Map_MapperIsNull {
            get {
                return ResourceManager.GetString("Map_MapperIsNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 目前Http上下文中不存在Request有效範圍的Mef部件容器。 的當地語系化字串。
        /// </summary>
        internal static string Mef_HttpContextItems_NotFoundRequestContainer {
            get {
                return ResourceManager.GetString("Mef_HttpContextItems_NotFoundRequestContainer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 指定對像中不存在名稱為「{0}」的屬性。 的當地語系化字串。
        /// </summary>
        internal static string ObjectExtensions_PropertyNameNotExistsInType {
            get {
                return ResourceManager.GetString("ObjectExtensions_PropertyNameNotExistsInType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 指定名稱「{0}」的屬性型別不是「{1}」。 的當地語系化字串。
        /// </summary>
        internal static string ObjectExtensions_PropertyNameNotFixedType {
            get {
                return ResourceManager.GetString("ObjectExtensions_PropertyNameNotFixedType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值必須在「{1}」與「{2}」之間。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_Between {
            get {
                return ResourceManager.GetString("ParameterCheck_Between", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值必須在「{1}」與「{2}」之間，且不能等於「{3}」。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_BetweenNotEqual {
            get {
                return ResourceManager.GetString("ParameterCheck_BetweenNotEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 指定的目錄路徑「{0}」不存在。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_DirectoryNotExists {
            get {
                return ResourceManager.GetString("ParameterCheck_DirectoryNotExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 指定的檔案路徑「{0}」不存在。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_FileNotExists {
            get {
                return ResourceManager.GetString("ParameterCheck_FileNotExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 集合「{0}」中不能包含null的項 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotContainsNull_Collection {
            get {
                return ResourceManager.GetString("ParameterCheck_NotContainsNull_Collection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值不能為Guid.Empty 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotEmpty_Guid {
            get {
                return ResourceManager.GetString("ParameterCheck_NotEmpty_Guid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值必須大於「{1}」。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotGreaterThan {
            get {
                return ResourceManager.GetString("ParameterCheck_NotGreaterThan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值必須大於或等於「{1}」。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotGreaterThanOrEqual {
            get {
                return ResourceManager.GetString("ParameterCheck_NotGreaterThanOrEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值必須小於「{1}」。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotLessThan {
            get {
                return ResourceManager.GetString("ParameterCheck_NotLessThan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」的值必須小於或等於「{1}」。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotLessThanOrEqual {
            get {
                return ResourceManager.GetString("ParameterCheck_NotLessThanOrEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」不能為空引用。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotNull {
            get {
                return ResourceManager.GetString("ParameterCheck_NotNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」不能為空引用或空集合。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotNullOrEmpty_Collection {
            get {
                return ResourceManager.GetString("ParameterCheck_NotNullOrEmpty_Collection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 參數「{0}」不能為空引用或空字串。 的當地語系化字串。
        /// </summary>
        internal static string ParameterCheck_NotNullOrEmpty_String {
            get {
                return ResourceManager.GetString("ParameterCheck_NotNullOrEmpty_String", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查詢類似 型別「{0}」不是實體型別 的當地語系化字串。
        /// </summary>
        internal static string QueryCacheExtensions_TypeNotEntityType {
            get {
                return ResourceManager.GetString("QueryCacheExtensions_TypeNotEntityType", resourceCulture);
            }
        }
    }
}
