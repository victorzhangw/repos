using System.Collections.Generic;
using System.Linq;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Core.App;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.AspNetCore.Common
{
    /// <summary>
    /// 預設初始化內容
    /// </summary>
    public class DefaultInitial : AurobaseInitialization
    {
        IAPPService _aPPService = App.GetService<IAPPService>();

        /// <summary>
        /// 記憶體中快取多應用
        /// </summary>
        public void CacheAppList()
        {
            _aPPService.UpdateCacheAllowApp();
        }
    }
}
