using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aurobase.AspNetCore.Models
{
    /// <summary>
    /// 錯誤程式碼描述
    /// </summary>
    public static class ErrCode
    {

        /// <summary>
        /// 請求成功
        /// </summary>
        public static string err0 = "請求成功";

        /// <summary>
        /// 請求成功程式碼0
        /// </summary>
        public static string successCode = "0";

        /// <summary>
        /// 請求失敗
        /// </summary>
        public static string err1 = "請求失敗";

        /// <summary>
        /// 請求失敗程式碼1
        /// </summary>
        public static string failCode = "1";

        /// <summary>
        /// 獲取access_token時AppID或AppSecret錯誤。請開發者認真比對appid和AppSecret的正確性，或檢視是否正在為恰當的應用呼叫介面
        /// </summary>
        public static string err40001 = "獲取access_token時AppID或AppSecret錯誤。請開發者認真比對appid和AppSecret的正確性，或檢視是否正在為恰當的應用呼叫介面";

        /// <summary>
        /// 呼叫介面的伺服器URL地址不正確，請聯繫供應商進行設定
        /// </summary>
        public static string err40002 = "呼叫介面的伺服器URL地址不正確，請聯繫供應商進行授權";

        /// <summary>
        /// 請確保grant_type欄位值為client_credential
        /// </summary>
        public static string err40003 = "請確保grant_type欄位值為client_credential";

        /// <summary>
        /// 不合法的憑證型別
        /// </summary>
        public static string err40004 = "不合法的憑證型別";

        /// <summary>
        /// 使用者令牌accesstoken超時失效
        /// </summary>
        public static string err40005 = "使用者令牌accesstoken超時失效";

        /// <summary>
        /// 您未被授權使用該功能，請重新登錄試試或聯繫管理員進行處理
        /// </summary>
        public static string err40006 = "您未被授權使用該功能，請重新登錄試試或聯繫系統管理員進行處理";

        /// <summary>
        /// 傳遞參數出現錯誤
        /// </summary>
        public static string err40007 = "傳遞參數出現錯誤";

        /// <summary>
        /// 使用者未登錄或超時
        /// </summary>
        public static string err40008 = "使用者未登錄或超時";
        /// <summary>
        /// 程式異常
        /// </summary>
        public static string err40110 = "程式異常";

        /// <summary>
        /// 更新數據失敗
        /// </summary>
        public static string err43001 = "新增數據失敗";

        /// <summary>
        /// 更新數據失敗
        /// </summary>
        public static string err43002 = "更新數據失敗";

        /// <summary>
        /// 物理刪除數據失敗
        /// </summary>
        public static string err43003 = "刪除數據失敗";

        /// <summary>
        /// 該使用者不存在
        /// </summary>
        public static string err50001 = "該使用者不存在";

        /// <summary>
        /// 該使用者已存在
        /// </summary>
        public static string err50002 = "使用者已存在，請登錄或重新註冊！";

        /// <summary>
        /// 會員註冊失敗
        /// </summary>
        public static string err50003 = "會員註冊失敗";

        /// <summary>
        /// 查詢數據不存在
        /// </summary>
        public static string err60001 = "查詢數據不存在";
    }
}
