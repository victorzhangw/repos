/**
 * 防止重複點選
 * @author yuebon.com chenqingwen
 */
export default {
  install(Vue) {
    // 防止重複點選
    Vue.directive('preventReClick', {
      inserted(el, binding) {
        el.addEventListener('click', () => {
          if (!el.disabled) {
            el.disabled = true
            setTimeout(() => {
              el.disabled = false
            }, binding.value || 1000)
          }
        })
      }
    })
  }
}
