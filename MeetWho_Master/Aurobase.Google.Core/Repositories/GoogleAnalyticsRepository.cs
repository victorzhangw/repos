using System;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Google.IRepositories;
using Aurobase.Google.Models;

namespace Aurobase.Google.Repositories
{
    /// <summary>
    /// Google Analytics仓储接口的实现
    /// </summary>
    public class GoogleAnalyticsRepository : BaseRepository<GoogleAnalytics, string>, IGoogleAnalyticsRepository
    {
        public GoogleAnalyticsRepository()
        {
        }

        public GoogleAnalyticsRepository(IDbContextCore context) : base(context)
        {

        }

        GoogleAnalytics IGoogleAnalyticsRepository.ConsumeGoogleAnalytics(string startdate, string enddate, string dimension, string metric)
        {
            throw new NotImplementedException();
        }

        GoogleAnalytics IGoogleAnalyticsRepository.GetAnalytics(string startdate, string enddate, string dimension, string metric)
        {
            throw new NotImplementedException();
        }

        #region Dapper 操作

        //DapperConn 用于读写操作
        //DapperConnRead 用于只读操作

        #endregion


        #region EF 操作

        //DbContext 用于读写操作
        //DbContextRead 用于只读操作

        #endregion
    }
}