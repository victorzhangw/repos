using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Google.Models;

namespace Aurobase.Google.Dtos
{
    public class GoogleProfile : Profile
    {
        public GoogleProfile()
        {
            CreateMap<GoogleAnalytics, GoogleAnalyticsOutputDto>();
            CreateMap<GoogleAnalyticsInputDto, GoogleAnalytics>();

        }
    }
}
