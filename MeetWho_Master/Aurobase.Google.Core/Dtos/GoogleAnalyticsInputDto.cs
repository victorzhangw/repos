using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Commons.Dtos;
using Aurobase.Google.Models;

namespace Aurobase.Google.Dtos
{
    /// <summary>
    /// Google Analytics输入对象模型
    /// </summary>
    [AutoMap(typeof(GoogleAnalytics))]
    [Serializable]
    public class GoogleAnalyticsInputDto : IInputDto<string>
    {
        /// <summary>
        /// 设置或获取Id

        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string SiteId { get; set; }
        /// <summary>
        /// 设置或获取事件形態(DImension)
        /// </summary>
        public string EventType { get; set; }
        /// <summary>
        /// 设置或获取事件（Metric Name）
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 设置或获取維度
        /// </summary>
        public string Dimension { get; set; }
        /// <summary>
        /// 设置或获取量值
        /// </summary>
        public string Metric { get; set; }
        /// <summary>
        /// 设置或获取日期
        /// </summary>
        public DateTime EventStartDate { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime EventEndDate { get; set; }
        /// <summary>
        /// 设置或获取資料
        /// </summary>
        public string Events { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string Description { get; set; }

    }
}
