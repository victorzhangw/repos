using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Google.Dtos
{
    /// <summary>
    /// Google Analytics输出对象模型
    /// </summary>
    [Serializable]
    public class GoogleAnalyticsOutputDto
    {
        /// <summary>
        /// 设置或获取Id

        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(40)]
        public string SiteId { get; set; }
        /// <summary>
        /// 设置或获取事件形態(DImension)
        /// </summary>
        [MaxLength(100)]
        public string EventType { get; set; }
        /// <summary>
        /// 设置或获取事件（Metric Name）
        /// </summary>
        [MaxLength(512)]
        public string EventName { get; set; }
        /// <summary>
        /// 设置或获取維度
        /// </summary>
        [MaxLength(100)]
        public string Dimension { get; set; }
        /// <summary>
        /// 设置或获取量值
        /// </summary>
        [MaxLength(100)]
        public string Metric { get; set; }
        /// <summary>
        /// 设置或获取日期
        /// </summary>
        public DateTime EventStartDate { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime EventEndDate { get; set; }
        /// <summary>
        /// 设置或获取資料
        /// </summary>
        [MaxLength(-1)]
        public string Events { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 设置或获取 
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

    }
}
