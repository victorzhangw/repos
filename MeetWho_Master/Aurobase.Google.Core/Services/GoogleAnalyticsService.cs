using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Pages;
using Aurobase.Commons.Services;
using Aurobase.Google.IRepositories;
using Aurobase.Google.IServices;
using Aurobase.Google.Dtos;
using Aurobase.Google.Models;

namespace Aurobase.Google.Services
{
    /// <summary>
    /// Google Analytics服务接口实现
    /// </summary>
    public class GoogleAnalyticsService : BaseService<GoogleAnalytics, GoogleAnalyticsOutputDto, string>, IGoogleAnalyticsService
    {
        private readonly IGoogleAnalyticsRepository _repository;
        public GoogleAnalyticsService(IGoogleAnalyticsRepository repository) : base(repository)
        {
            _repository = repository;
        }
    }
}