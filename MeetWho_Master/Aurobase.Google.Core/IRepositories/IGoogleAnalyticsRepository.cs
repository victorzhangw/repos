using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Google.Models;

namespace Aurobase.Google.IRepositories
{
    /// <summary>
    /// 定义Google Analytics仓储接口
    /// </summary>
    public interface IGoogleAnalyticsRepository : IRepository<GoogleAnalytics, string>
    {
        public GoogleAnalytics ConsumeGoogleAnalytics(string startdate, string enddate, string dimension, string metric);
        public GoogleAnalytics GetAnalytics(string startdate, string enddate, string dimension, string metric);

    }
}