using System;
using Aurobase.Commons.IServices;
using Aurobase.Google.Dtos;
using Aurobase.Google.Models;

namespace Aurobase.Google.IServices
{
    /// <summary>
    /// 定义Google Analytics服务接口
    /// </summary>
    public interface IGoogleAnalyticsService : IService<GoogleAnalytics, GoogleAnalyticsOutputDto, string>
    {
    }
}
