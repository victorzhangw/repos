﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.Google.Models
{
    /// <summary>
    /// 取得 GA 報表所需參數
    /// </summary>

    public class GAReportSettings
    {
        public string EventStartDate { get; set; }
        public string EventEndDate { get; set; }
        public string ReportType { get; set; }
        public string ReportId { get; set; }
    }
}
