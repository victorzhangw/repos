using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Google.Models
{
    /// <summary>
    /// Google Analytics，数据实体对象
    /// </summary>
    [Table("CRM_GoogleAnalytics")]
    [Serializable]
    public class GoogleAnalytics : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string SiteId { get; set; }

        /// <summary>
        /// 设置或获取事件形態(DImension)
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// 设置或获取事件（Metric Name）
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 设置或获取日期
        /// </summary>
        public DateTime EventStartDate { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime EventEndDate { get; set; }

        /// <summary>
        /// 设置或获取資料
        /// </summary>
        public string Events { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 设置或获取 
        /// </summary>
        public string DeleteUserId { get; set; }

    }
}
