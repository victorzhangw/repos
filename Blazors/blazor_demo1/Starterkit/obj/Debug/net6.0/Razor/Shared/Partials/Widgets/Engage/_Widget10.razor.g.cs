#pragma checksum "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Widgets\Engage\_Widget10.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fd8ddc934d1110de36d6e7733fa449f89dfe7b89"
// <auto-generated/>
#pragma warning disable 1591
namespace Starterkit.Shared.Partials.Widgets.Engage
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\_Imports.razor"
using Starterkit._keenthemes.libs;

#line default
#line hidden
#nullable disable
    public partial class _Widget10 : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<div class=""card card-flush h-md-100""><div class=""card-body d-flex flex-column justify-content-between mt-9 bgi-no-repeat bgi-size-cover bgi-position-x-center pb-0"" style=""background-position: 100% 50%; background-image:url('assets/media/stock/900x600/42.png')""><div class=""mb-10""><div class=""fs-2hx fw-bold text-gray-800 text-center mb-13""><span class=""me-2"">Try our all new Enviroment with 
			<br>
			<span class=""position-relative d-inline-block text-danger""><a href=""?page=pages/user-profile/overview"" class=""text-danger opacity-75-hover"">Pro Plan</a>
				
				<span class=""position-absolute opacity-15 bottom-0 start-0 border-4 border-danger border-bottom w-100""></span></span></span>for Free</div>
			
			
			<div class=""text-center""><a href='#' class=""btn btn-sm btn-dark fw-bold"" data-bs-toggle=""modal"" data-bs-target=""#kt_modal_upgrade_plan"">Upgrade Now</a></div></div>
		
		
		<img class=""mx-auto h-150px h-lg-200px theme-light-show"" src=""assets/media/illustrations/misc/upgrade.svg"" alt>
		<img class=""mx-auto h-150px h-lg-200px theme-dark-show"" src=""assets/media/illustrations/misc/upgrade-dark.svg"" alt></div></div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JS { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IKTTheme KTTheme { get; set; }
    }
}
#pragma warning restore 1591
