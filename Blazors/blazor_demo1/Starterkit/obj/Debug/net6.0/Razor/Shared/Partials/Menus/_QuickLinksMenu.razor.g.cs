#pragma checksum "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Menus\_QuickLinksMenu.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5347a0bac06006d1590d03bdd3ba83d8add2a308"
// <auto-generated/>
#pragma warning disable 1591
namespace Starterkit.Shared.Partials.Menus
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\_Imports.razor"
using Starterkit._keenthemes.libs;

#line default
#line hidden
#nullable disable
    public partial class _QuickLinksMenu : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "menu menu-sub menu-sub-dropdown menu-column w-250px w-lg-325px");
            __builder.AddAttribute(2, "data-kt-menu", "true");
            __builder.AddMarkupContent(3, @"<div class=""d-flex flex-column flex-center bgi-no-repeat rounded-top px-9 py-10"" style=""background-image:url('assets/media/misc/pattern-1.jpg')""><h3 class=""text-white fw-semibold mb-3"">Quick Links</h3>
		
		
		<span class=""badge bg-primary py-2 px-3"">25 pending tasks</span></div>
	
	
	");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "row g-0");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "col-6");
            __builder.OpenElement(8, "a");
            __builder.AddAttribute(9, "href", "?page=apps/projects/budget");
            __builder.AddAttribute(10, "class", "d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end border-bottom");
#nullable restore
#line (17,132)-(17,259) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Menus\_QuickLinksMenu.razor"
__builder.AddContent(11, (MarkupString)KTTheme.getSvgIcon("assets/media/icons/duotune/finance/fin009.svg", "svg-icon svg-icon-3x svg-icon-primary mb-2"));

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(12, " \r\n\t\t\t");
            __builder.AddMarkupContent(13, "<span class=\"fs-5 fw-semibold text-gray-800 mb-0\">Accounting</span>\r\n\t\t\t");
            __builder.AddMarkupContent(14, "<span class=\"fs-7 text-gray-400\">eCommerce</span>");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(15, "\r\n\t\t\r\n\t\t\r\n\t\t");
            __builder.OpenElement(16, "div");
            __builder.AddAttribute(17, "class", "col-6");
            __builder.OpenElement(18, "a");
            __builder.AddAttribute(19, "href", "?page=apps/projects/settings");
            __builder.AddAttribute(20, "class", "d-flex flex-column flex-center h-100 p-6 bg-hover-light border-bottom");
#nullable restore
#line (24,123)-(24,256) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Menus\_QuickLinksMenu.razor"
__builder.AddContent(21, (MarkupString)KTTheme.getSvgIcon("assets/media/icons/duotune/communication/com010.svg", "svg-icon svg-icon-3x svg-icon-primary mb-2"));

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(22, " \r\n\t\t\t");
            __builder.AddMarkupContent(23, "<span class=\"fs-5 fw-semibold text-gray-800 mb-0\">Administration</span>\r\n\t\t\t");
            __builder.AddMarkupContent(24, "<span class=\"fs-7 text-gray-400\">Console</span>");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(25, "\r\n\t\t\r\n\t\t\r\n\t\t");
            __builder.OpenElement(26, "div");
            __builder.AddAttribute(27, "class", "col-6");
            __builder.OpenElement(28, "a");
            __builder.AddAttribute(29, "href", "?page=apps/projects/list");
            __builder.AddAttribute(30, "class", "d-flex flex-column flex-center h-100 p-6 bg-hover-light border-end");
#nullable restore
#line (31,116)-(31,244) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Menus\_QuickLinksMenu.razor"
__builder.AddContent(31, (MarkupString)KTTheme.getSvgIcon("assets/media/icons/duotune/abstract/abs042.svg", "svg-icon svg-icon-3x svg-icon-primary mb-2"));

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(32, " \r\n\t\t\t");
            __builder.AddMarkupContent(33, "<span class=\"fs-5 fw-semibold text-gray-800 mb-0\">Projects</span>\r\n\t\t\t");
            __builder.AddMarkupContent(34, "<span class=\"fs-7 text-gray-400\">Pending Tasks</span>");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(35, "\r\n\t\t\r\n\t\t\r\n\t\t");
            __builder.OpenElement(36, "div");
            __builder.AddAttribute(37, "class", "col-6");
            __builder.OpenElement(38, "a");
            __builder.AddAttribute(39, "href", "?page=apps/projects/users");
            __builder.AddAttribute(40, "class", "d-flex flex-column flex-center h-100 p-6 bg-hover-light");
#nullable restore
#line (38,106)-(38,233) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Menus\_QuickLinksMenu.razor"
__builder.AddContent(41, (MarkupString)KTTheme.getSvgIcon("assets/media/icons/duotune/finance/fin006.svg", "svg-icon svg-icon-3x svg-icon-primary mb-2"));

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(42, " \r\n\t\t\t");
            __builder.AddMarkupContent(43, "<span class=\"fs-5 fw-semibold text-gray-800 mb-0\">Customers</span>\r\n\t\t\t");
            __builder.AddMarkupContent(44, "<span class=\"fs-7 text-gray-400\">Latest cases</span>");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(45, "\r\n\t\r\n\t\r\n\t");
            __builder.OpenElement(46, "div");
            __builder.AddAttribute(47, "class", "py-2 text-center border-top");
            __builder.OpenElement(48, "a");
            __builder.AddAttribute(49, "href", "?page=pages/user-profile/activity");
            __builder.AddAttribute(50, "class", "btn btn-color-gray-600 btn-active-color-primary");
            __builder.AddContent(51, "View All ");
#nullable restore
#line (47,114)-(47,217) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Partials\Menus\_QuickLinksMenu.razor"
__builder.AddContent(52, (MarkupString)KTTheme.getSvgIcon("assets/media/icons/duotune/arrows/arr064.svg", "svg-icon svg-icon-5"));

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IKTTheme KTTheme { get; set; }
    }
}
#pragma warning restore 1591
