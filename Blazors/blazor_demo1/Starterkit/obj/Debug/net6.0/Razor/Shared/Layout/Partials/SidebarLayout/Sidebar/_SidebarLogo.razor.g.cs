#pragma checksum "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "86e8daf954372107611d69620e794b4c46a13d47"
// <auto-generated/>
#pragma warning disable 1591
namespace Layout.Partials.SidebarLayout.Sidebar
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\_Imports.razor"
using Starterkit._keenthemes.libs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor"
using Layout._Helpers;

#line default
#line hidden
#nullable disable
    public partial class _SidebarLogo : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "app-sidebar-logo px-6");
            __builder.AddAttribute(2, "id", "kt_app_sidebar_logo");
            __builder.OpenElement(3, "a");
            __builder.AddAttribute(4, "href", "?page=index");
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor"
         if(sidebar=="dark-sidebar"){

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(5, "<img alt=\"Logo\" src=\"assets/media/logos/default-dark.svg\" class=\"h-25px app-sidebar-logo-default\">");
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor"
		} else {
			if(sidebar=="light-sidebar"){

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(6, "<img alt=\"Logo\" src=\"assets/media/logos/default.svg\" class=\"theme-light-show h-25px app-sidebar-logo-default\">\r\n\t\t\t\t<img alt=\"Logo\" src=\"assets/media/logos/default-dark.svg\" class=\"theme-dark-show h-25px app-sidebar-logo-default\">");
#nullable restore
#line 14 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor"
			}
		}

#line default
#line hidden
#nullable disable
            __builder.AddMarkupContent(7, "<img alt=\"Logo\" src=\"assets/media/logos/default-small.svg\" class=\"h-20px app-sidebar-logo-minimize\">");
            __builder.CloseElement();
            __builder.AddMarkupContent(8, "\r\n\t\r\n\r\n\t\r\n\t");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "id", "kt_app_sidebar_toggle");
            __builder.AddAttribute(11, "class", "app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-50 start-100 translate-middle rotate");
            __builder.AddAttribute(12, "data-kt-toggle", "true");
            __builder.AddAttribute(13, "data-kt-toggle-state", "active");
            __builder.AddAttribute(14, "data-kt-toggle-target", "body");
            __builder.AddAttribute(15, "data-kt-toggle-name", "app-sidebar-minimize");
#nullable restore
#line (22,5)-(22,119) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor"
__builder.AddContent(16, (MarkupString)KTTheme.getSvgIcon("assets/media/icons/duotune/arrows/arr079.svg", "svg-icon svg-icon-2 rotate-180"));

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 28 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Sidebar\_SidebarLogo.razor"
       
	private string sidebar;
	
	protected override async Task OnAfterRenderAsync(bool firstRender)
	{
		if(firstRender)
		{
			sidebar = await JS.InvokeAsync<string>("document.body.getAttribute", "data-kt-app-layout");
			StateHasChanged();
		}
	}

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JS { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IKTTheme KTTheme { get; set; }
    }
}
#pragma warning restore 1591
