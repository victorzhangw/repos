#pragma checksum "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\SystemLayout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7b654b6e4aaf8764f7d4b2a2bd0745c7abb622eb"
// <auto-generated/>
#pragma warning disable 1591
namespace Layout
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\_Imports.razor"
using Starterkit._keenthemes.libs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\SystemLayout.razor"
using Layout._Helpers;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MasterInit))]
    public partial class SystemLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "d-flex flex-column flex-root");
            __builder.AddAttribute(2, "id", "kt_app_root");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "d-flex flex-column flex-center flex-column-fluid");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "d-flex flex-column flex-center text-center p-10");
            __builder.OpenElement(7, "div");
            __builder.AddAttribute(8, "class", "card card-flush w-lg-650px py-5");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "card-body py-15 py-lg-20");
#nullable restore
#line (16,22)-(16,26) 25 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\SystemLayout.razor"
__builder.AddContent(11, Body);

#line default
#line hidden
#nullable disable
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 28 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\SystemLayout.razor"
       
    private IKTThemeHelpers _helper;
	
    protected override void OnAfterRender(bool firstRender){
        _helper = new KTThemeHelpers(JS);

        //Initialize classes and attributes for layout with dark header
        _helper.addBodyClass("app-black bgi-size-cover bgi-position-center bgi-no-repeat");
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await Task.Delay(300);
		await JS.InvokeVoidAsync("document.body.removeAttribute", "data-kt-app-reset-transition");
		await JS.InvokeVoidAsync("document.body.removeAttribute", "data-kt-app-page-loading");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JS { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IKTTheme KTTheme { get; set; }
    }
}
#pragma warning restore 1591
