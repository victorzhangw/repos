#pragma checksum "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\SidebarLayout\Header\_Menu\__PagesGeneral.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5608852d085f42a59037ebd22124e63cd9f68a23"
// <auto-generated/>
#pragma warning disable 1591
namespace Layout.Partials.SidebarLayout.Header._Menu
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\_Imports.razor"
using Starterkit._keenthemes.libs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
    public partial class __PagesGeneral : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<div class=\"row\"><div class=\"col-lg-8\"><div class=\"row\"><div class=\"col-lg-3 mb-6 mb-lg-0\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">User Profile</h4>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/user-profile/overview\" class=\"menu-link\"><span class=\"menu-title\">Overview</span></a></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/user-profile/projects\" class=\"menu-link\"><span class=\"menu-title\">Projects</span></a></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/user-profile/campaigns\" class=\"menu-link\"><span class=\"menu-title\">Campaigns</span></a></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/user-profile/documents\" class=\"menu-link\"><span class=\"menu-title\">Documents</span></a></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/user-profile/followers\" class=\"menu-link\"><span class=\"menu-title\">Followers</span></a></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/user-profile/activity\" class=\"menu-link\"><span class=\"menu-title\">Activity</span></a></div></div>\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t<div class=\"col-lg-3 mb-6 mb-lg-0\"><div class=\"mb-6\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">Corporate</h4>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/about\" class=\"menu-link\"><span class=\"menu-title\">About</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/team\" class=\"menu-link\"><span class=\"menu-title\">Our Team</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/contact\" class=\"menu-link\"><span class=\"menu-title\">Contact Us</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/licenses\" class=\"menu-link\"><span class=\"menu-title\">Licenses</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/sitemap\" class=\"menu-link\"><span class=\"menu-title\">Sitemap</span></a></div></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"mb-0\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">Careers</h4>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/careers/list\" class=\"menu-link\"><span class=\"menu-title\">Careers List</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/careers/apply\" class=\"menu-link\"><span class=\"menu-title\">Careers Apply</span></a></div></div></div>\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t<div class=\"col-lg-3 mb-6 mb-lg-0\"><div class=\"mb-6\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">FAQ</h4>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/faq/classic\" class=\"menu-link\"><span class=\"menu-title\">FAQ Classic</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/faq/extended\" class=\"menu-link\"><span class=\"menu-title\">FAQ Extended</span></a></div></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"mb-6\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">Blog</h4>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/blog/home\" class=\"menu-link\"><span class=\"menu-title\">Blog Home</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/blog/post\" class=\"menu-link\"><span class=\"menu-title\">Blog Post</span></a></div></div>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<div class=\"mb-0\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">Pricing</h4>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/pricing/column\" class=\"menu-link\"><span class=\"menu-title\">Column Pricing</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/pricing/table\" class=\"menu-link\"><span class=\"menu-title\">Table Pricing</span></a></div></div></div>\r\n\t\t\t\r\n\t\t\t\r\n\t\t\t<div class=\"col-lg-3 mb-6 mb-lg-0\"><div class=\"mb-0\"><h4 class=\"fs-6 fs-lg-4 fw-bold mb-3 ms-4\">Social</h4>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/social/feeds\" class=\"menu-link\"><span class=\"menu-title\">Feeds</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/social/activity\" class=\"menu-link\"><span class=\"menu-title\">Activty</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/social/followers\" class=\"menu-link\"><span class=\"menu-title\">Followers</span></a></div>\r\n\t\t\t\t\t\r\n\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu-item p-0 m-0\"><a href=\"?page=pages/social/settings\" class=\"menu-link\"><span class=\"menu-title\">Settings</span></a></div></div></div></div></div>\r\n\t\r\n\t\r\n\t<div class=\"col-lg-4\"><img src=\"assets/media/stock/600x600/img-82.jpg\" class=\"rounded mw-100\" alt></div></div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IKTTheme KTTheme { get; set; }
    }
}
#pragma warning restore 1591
