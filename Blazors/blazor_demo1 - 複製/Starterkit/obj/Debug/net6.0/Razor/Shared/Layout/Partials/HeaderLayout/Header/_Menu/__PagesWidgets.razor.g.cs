#pragma checksum "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\Partials\HeaderLayout\Header\_Menu\__PagesWidgets.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "02b003cb42366297af6d9bc6dc98282f79a42c7a"
// <auto-generated/>
#pragma warning disable 1591
namespace Layout.Partials.HeaderLayout.Header._Menu
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\_Imports.razor"
using Starterkit.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\_Imports.razor"
using Starterkit._keenthemes.libs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\auro-webpage\metronic_v8.1.1\blazor\blazor_demo1\Starterkit\Shared\Layout\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
    public partial class __PagesWidgets : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<div class=""row""><div class=""col-lg-4 mb-6 mb-lg-0""><div class=""menu-item p-0 m-0""><a href=""?page=widgets/lists"" class=""menu-link""><span class=""menu-title"">Lists</span></a></div>
		
		
		<div class=""menu-item p-0 m-0""><a href=""?page=widgets/statistics"" class=""menu-link""><span class=""menu-title"">Statistics</span></a></div>
		
		
		<div class=""menu-item p-0 m-0""><a href=""?page=widgets/charts"" class=""menu-link""><span class=""menu-title"">Charts</span></a></div>
		
		
		<div class=""menu-item p-0 m-0""><a href=""?page=widgets/mixed"" class=""menu-link""><span class=""menu-title"">Mixed</span></a></div>
		
		
		<div class=""menu-item p-0 m-0""><a href=""?page=widgets/tables"" class=""menu-link""><span class=""menu-title"">Tables</span></a></div>
		
		
		<div class=""menu-item p-0 m-0""><a href=""?page=widgets/feeds"" class=""menu-link""><span class=""menu-title"">Feeds</span></a></div></div>
	
	
	<div class=""col-lg-8""><img src=""assets/media/stock/900x600/44.jpg"" class=""rounded mw-100"" alt></div></div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IKTTheme KTTheme { get; set; }
    }
}
#pragma warning restore 1591
