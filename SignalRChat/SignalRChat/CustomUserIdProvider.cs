﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections.Features;
using Microsoft.AspNetCore.SignalR;

public class CustomUserIdProvider : IUserIdProvider
{
    public string GetUserId(HubConnectionContext connection )
    {
        string _rtnString = connection.Features.Get<IHttpContextFeature>().HttpContext.Request.Query["myClientId"].ToString();
        return _rtnString;
        //return connection.User?.FindFirst(ClaimTypes.SerialNumber)?.Value;
    }
    /*
    public virtual string GetUserId(HubConnectionContext connection)
    {
        return connection.User?.FindFirst(ClaimTypes.Email)?.Value;
    }*/
}
