﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;


namespace SignalRChat.Hubs
{
    public class ChatHub:Hub

    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
            
        }
        public async Task SendUserMessage(string user, string message)
        {
            await Clients.User(user).SendAsync("AlertMe", user,message);

        }
        public async Task SendImageList(string user)
        {
            var imageList = new List<string>();
            imageList.Add("http://dailysee.net/ImgLibrary/1.jpg");
            imageList.Add("http://dailysee.net/ImgLibrary/2.jpg");
            imageList.Add("http://dailysee.net/ImgLibrary/3.jpg");
            var imageJson = JsonConvert.SerializeObject(imageList);
            await Clients.User(user).SendAsync("ImageList", user, imageJson);

        }
    }
}
