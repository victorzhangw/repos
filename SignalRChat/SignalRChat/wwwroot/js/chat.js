﻿"use strict";
function getAccessToken() {
    let myClientId = "68604263506141506588601997951338";
    //setCookie("srconnectionid", myClientId, 100);
    return myClientId;
}
function getRandom(x) {
    return Math.floor(Math.random() * x);
};
var myClientId = getRandom(100000000000000);
console.log("myClientId:" + myClientId);
document.getElementById("myInput").value = myClientId; 
let connection = new signalR.HubConnectionBuilder()
    .withUrl("/chatHub?myClientId=" + myClientId, {
        accessTokenFactory: () => {
            // Get and return the access token.
            // This function can return a JavaScript Promise if asynchronous
            // logic is required to retrieve the access token.
            let myClientId = "68604263506141506588601997951338";
            //setCookie("srconnectionid", myClientId, 100);
            return myClientId;
        }
    })
    .build();


//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("ReceiveMessage", function (user, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = user + " says " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection.on("AlertMe", function (user, message) {
    let alertMessage = user+" 調用警告視窗:"+message ;
    alert(alertMessage);
});
connection.on("ImageList", function (user,data) {
    let rtnJson = JSON.parse(data);
    let imageHtml = "";

    console.log(rtnJson);
    
    for (let i = 0; i < rtnJson.length; i++) {
        let str = ' <img src="' + rtnJson[i]+'" class="rounded"  width="204" height="236"/>';
           
        
        imageHtml += str;
    }
    
    document.getElementById("imgList").innerHTML=imageHtml;
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
    console.log("HubID:");
    console.log(connection.connectionId);
    
}).catch(function (err) {
    return console.error(err.toString());
});
/*
connection.invoke("SendUserMessage", user, message).catch(function (err) {
    return console.error(err.toString());
});*/
document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});
document.getElementById("sendImageButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    
    connection.invoke("SendImageList", user).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

document.getElementById("sendUserButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendUserMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
