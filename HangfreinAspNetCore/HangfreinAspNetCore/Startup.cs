﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Hangfire;
using Hangfire.MySql.Core;
using Owin;
using Microsoft.Owin;
using RestSharp;
using MySql.Data.MySqlClient;
using System.Data;
using Dapper;
using System.Diagnostics;

[assembly: OwinStartup(typeof(HangfreinAspNetCore.Startup))]

namespace HangfreinAspNetCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            string connectionString = DMLString.AurocoreAPIConnectionstring;

            services.AddHangfire(config =>
            {
                GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(connectionString));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseHangfireDashboard();
            app.UseHangfireServer();
            // RecurringJob.AddOrUpdate<TriggerAdwordsOffline>("Account_Performance", x => x.Account_Performance(),Cron.Daily(1),


            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Account_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Campaign_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.AdGroup_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Ads_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Keywords_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Age_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Gender_Performance());
            //BackgroundJob.Enqueue<TriggerAdwordsOffline>(x => x.Geo_Performance());
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Account_Performance(), Cron.Daily(17, 10),TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Campaign_Performance(), Cron.Daily(17, 12), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.AdGroup_Performance(), Cron.Daily(17, 14), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Ads_Performance(), Cron.Daily(17, 16), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Keywords_Performance(), Cron.Daily(17, 18), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Age_Performance(), Cron.Daily(17, 20), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Gender_Performance(), Cron.Daily(17, 22), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate<TriggerAdwordsOffline>(x => x.Geo_Performance(), Cron.Daily(17, 24), TimeZoneInfo.Local);


            //TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time"));
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();

            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


        }
        
        

    public class TriggerAdwordsOffline
        {
            public void Account_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Accounts_Performance";
                var client = new RestClient($"{DMLString.baseurl}Accounts_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                
                for (int i=0; i<adwordsmcc.Count();i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }
                

               
               
               
            }
            public void Campaign_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Campaigns_Performance";
                var client = new RestClient($"{DMLString.baseurl}Campaigns_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
            public void AdGroup_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}AdGroup_Performance";
                var client = new RestClient($"{DMLString.baseurl}AdGroup_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
            public void Ads_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Ads_Performance";
                var client = new RestClient($"{DMLString.baseurl}Ads_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
            public void Keywords_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Keywords_Performance";
                var client = new RestClient($"{DMLString.baseurl}Keywords_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
            public void Age_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Age_Performance";
                var client = new RestClient($"{DMLString.baseurl}Age_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
            public void Gender_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Gender_Performance";
                var client = new RestClient($"{DMLString.baseurl}Gender_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
            public void Geo_Performance()
            {
                string StartDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                AdwordsMccAccouts adwordsaccounts = new AdwordsMccAccouts();
                List<AdwordsMCC> adwordsmcc = adwordsaccounts.RetrieveAccounts();
                var request = new RestRequest(Method.POST);
                string clinetstr = $"{DMLString.baseurl}Geo_Performance";
                var client = new RestClient($"{DMLString.baseurl}Geo_Performance");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");

                for (int i = 0; i < adwordsmcc.Count(); i++)
                {
                    var form = new AdwordsForm()
                    {
                        Startdate = StartDate,
                        Enddate = EndDate,
                        Userid = adwordsmcc[i].Gmailid
                    };
                    request.AddParameter("application/x-www-form-urlencoded", $"startdate={form.Startdate}&enddate={form.Enddate}&userid={form.Userid}", ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);

                }





            }
        }
        public class AdwordsMccAccouts
        {
            public List<AdwordsMCC>RetrieveAccounts(){
                IDbConnection conn1 = new MySqlConnection(DMLString.AurocoreAPIConnectionstring);
                if (conn1.State != ConnectionState.Open)
                {
                    conn1.Open();
                }

                using (var transaction1 = conn1.BeginTransaction())
                {
                    return conn1.Query<AdwordsMCC>
                    (DMLString.Dqlstring1, transaction1).ToList();
                }
            }
           
        }
        /// <summary>
        /// Pass adwords parallel report form data 
        /// </summary>
        public class AdwordsForm
        {
            public string Userid { get; set; }
            public string Startdate { get; set; }
            public string Enddate { get; set; }

        }
        public class AdwordsMCC
        {
            public string Gmailid { get; set; }
            public string GoogleRefreshToken { get; set; }
            public string clientCustomerID { get; set; }
            public string developerToken { get; set; }
        }
        /// <summary>
        /// Data Manipulation and Query Language 
        /// </summary>
        private static class DMLString
        {
            /// <summary>
            /// baseurl
            /// </summary>
            public static string baseurl = "http://192.168.101.59/AuroIntegratedAPI/adwords/AdwordsParallelReportsDownloader/";
            public static string Dmlstring1 = "";
            /// <summary>
            /// Connected to Mysql AurocoreAPI
            /// </summary>
            public static string AurocoreAPIConnectionstring = "server = 192.168.101.55; uid = root; pwd = auRo#passW0rd; database =AUROCOREAPI; Allow User Variables = True";
            /// <summary>
            /// Select Adwords MCC Accounts
            /// </summary>
            public static string Dqlstring1 = @"select GmailID,GoogleRefreshToken,clientCustomerID,developerToken from member where clientCustomerID<>'' and clientCustomerID is not null";
        }
    }
}
