﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using EnsembleCRM.Models;
using Microsoft.Extensions.Logging;
using System.Text.RegularExpressions;
using System.Web;
using static EnsembleCRM.Models.TealeafEvent;
using Newtonsoft.Json.Linq;

namespace EnsembleCRM.Helpers
{
    public class TealeafEventHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<TealeafEventHelpers> _logger { get; set; }
        public TealeafEventHelpers(IConfiguration config, ILogger<TealeafEventHelpers> logger)
        {

            _configuration = config;
            _logger = logger;
        }
        public string  EventParser(List<TealeafEvent.TealeafEvents> tealeafEvents,string reportType)
        {
            string returnString = string.Empty;
            string _sectionEvent = $"{reportType}:Events";
            var eventSections = _configuration.GetSection(_sectionEvent).Get<List<List<string>>>();
            var eventList = new List<dynamic>();
            try
            {
                List<dynamic> dynList = new List<dynamic>();
                List<ReportCollection> reportCollections = new List<ReportCollection>();
                
                foreach (List<string> eventSection in eventSections)
                {

                    var finds = from data in tealeafEvents
                                where data.eventname == eventSection[0]
                                select data;
                    //JObject jObject = new JObject();
                    if (finds.Count() > 0)
                    {
                        foreach (var find in finds)
                        {
                            //var tmpList = JsonConvert.DeserializeObject<dynamic>(find.eventstring);

                            var tmpList = JsonConvert.DeserializeObject<List<TealeafEvent.ReportDataset>>(find.eventstring);
                            eventList.AddRange(tmpList);

                        }

                        /*
                        var resultEvt1 = from evt in eventList
                                         group evt by (evt.Column1.ToString().Split('?')[0]) into grp
                                         select new { Col1 = grp.Key, Col2 = grp.Sum( evt =>(decimal) (NullableTryParseDecimal(evt.Column2))) };
                        */
                        var query1 = from evt in eventList
                                     group evt by StringDecode(ParseUrl(evt.Column1)) into grp
                                     orderby grp.Sum(evt => (decimal)(NullableTryParseDecimal(evt.Column2))) descending
                                     select new { Col1 = grp.Key, Col2 = grp.Sum(evt => (decimal)(NullableTryParseDecimal(evt.Column2))) };
                        /*
                        var query2 = from evt in eventList 
                                     select new { Col1 = (String.IsNullOrWhiteSpace(evt.Column2))?0: evt.Column2, Col2 = (String.IsNullOrWhiteSpace(evt.Column3)) ? 0 : evt.Column3, Col3 = (String.IsNullOrWhiteSpace(evt.Column4)) ? 0 : evt.Column4, Col4 = (String.IsNullOrWhiteSpace(evt.Column5)) ? 0 : evt.Column5, Col5 = (String.IsNullOrWhiteSpace(evt.Column6)) ? 0 : evt.Column6, Col6 = (String.IsNullOrWhiteSpace(evt.Column7)) ? 0 : evt.Column7, Col7 = (String.IsNullOrWhiteSpace(evt.Column8)) ? 0 : evt.Column8 };
                        */
                        //  without group by key field
                        // 漏斗圖常用
                        var query2 = from evt in eventList
                                     group evt by 1 into grp
                                     select new { Col1 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column2))), Col2 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column3))), Col3 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column4))), Col4 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column5))), Col5 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column6))), Col6 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column7))), Col7 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column8))), Col8 = grp.Sum(x => (decimal)(NullableTryParseDecimal(x.Column9))) };
                        var query3 = from evt in eventList
                                     group evt by new { col1 = StringDecode(ParseUrl(evt.Column1)), col2 = StringDecode(ParseUrl(evt.Column2)) } into grp
                                     orderby grp.Sum(evt => (decimal)(NullableTryParseDecimal(evt.Column3))) descending
                                     select new { Col1 = grp.Key.col1, Col2 = grp.Key.col2, Col3 = grp.Sum(evt => (decimal)(NullableTryParseDecimal(evt.Column3))) };

                        var query4 = from evt in eventList
                                     where evt.Column1 != "[Others]"
                                     select new { Cookies = evt.Column1 };

                        var resultEvt1 = query1.Take(Int32.Parse(eventSection[3])); //依據報表設定取資料筆數
                        var resultEvt2 = query2;
                        var resultEvt3 = query3;
                        var resultEvt4 = query4;



                        switch (eventSection[2])
                        {
                            case "1":
                                var resultList1 = resultEvt1.ToList();

                                //dynList.AddRange(resultList1);
                                reportCollections.Add(new ReportCollection { Name = eventSection[0], Data = resultList1 });
                                eventList.Clear();
                                //dynList.Clear();
                                break;
                            case "2":
                                var resultList2 = resultEvt2.ToList();
                                //dynList.AddRange(resultList2);
                                reportCollections.Add(new ReportCollection { Name = eventSection[0], Data = resultList2 });

                                eventList.Clear();
                                //dynList.Clear();
                                break;
                            case "3":
                                var resultList3 = resultEvt3.ToList();
                                //dynList.AddRange(resultList2);
                                reportCollections.Add(new ReportCollection { Name = eventSection[0], Data = resultList3 });

                                eventList.Clear();
                                break;

                            case "4"://分割 cookies 報表
                                var resultList4 = resultEvt4.ToList();
                                if (resultList4!=null && resultList4.Count > 0)
                                {
                                    List<CookiesReport> cookiesReports = new();
                                    List<string> exportCsv = new();
                                    foreach (var item in resultList4)
                                    {
                                        string csvStr = string.Empty;
                                        CookiesReport _cookiesReport = new();
                                        string[] cookieValue = item.ToString().Split(new char[] {'|','~' });
                                        _cookiesReport.cftuid = cookieValue[1];
                                        _cookiesReport.cftp = cookieValue[3];
                                        _cookiesReport.tltsid = cookieValue[5] ;
                                        _cookiesReport.cfuuidmain = cookieValue[7] ;
                                        _cookiesReport.cfpoint = cookieValue[9] ;
                                        cookiesReports.Add(_cookiesReport);
                                    }
                                    reportCollections.Add(new ReportCollection { Name = eventSection[0], Data = cookiesReports });
                                }
                                
                                eventList.Clear();
                                break;
                        }

                    }



                }
                try
                {
                    
                    returnString = JsonConvert.SerializeObject(reportCollections);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex.ToString());
                }
            }
            catch(Exception ex)
            {
                _logger.LogWarning(ex.ToString());
            }
           
            return returnString;
        }
        public List<CookiesReport> EventExport(List<TealeafEvent.TealeafEvents> tealeafEvents, string reportType)
        {
            string returnString = string.Empty;
            string _sectionEvent = $"{reportType}:Events";
            var eventSections = _configuration.GetSection(_sectionEvent).Get<List<List<string>>>();
            var eventList = new List<dynamic>();
            List<CookiesReport> cookiesReports = new();
            try
            {
                List<dynamic> dynList = new List<dynamic>();
                List<ReportCollection> reportCollections = new List<ReportCollection>();

                foreach (List<string> eventSection in eventSections)
                {

                    var finds = from data in tealeafEvents
                                where data.eventname == eventSection[0]
                                select data;
                    //JObject jObject = new JObject();
                    if (finds.Count() > 0)
                    {
                        foreach (var find in finds)
                        {
                            //var tmpList = JsonConvert.DeserializeObject<dynamic>(find.eventstring);

                            var tmpList = JsonConvert.DeserializeObject<List<TealeafEvent.ReportDataset>>(find.eventstring);
                            eventList.AddRange(tmpList);

                        }

                       
                        var query1 = from evt in eventList
                                     where evt.Column1 != "[Others]"
                                     select new { Cookies = evt.Column1 };

            
                        var resultEvt1 = query1;



                        switch (eventSection[2])
                        {
                           

                            case "4"://分割 cookies 報表
                                var resultList1 = resultEvt1.ToList();
                                if (resultList1 != null && resultList1.Count > 0)
                                {
                                    
                                   
                                    foreach (var item in resultList1)
                                    {
                                        string csvStr = string.Empty;
                                        CookiesReport _cookiesReport = new();
                                        string[] cookieValue = item.ToString().Split(new char[] { '|', '~' });
                                        _cookiesReport.cftuid = cookieValue[1];
                                        _cookiesReport.cftp = cookieValue[3];
                                        _cookiesReport.tltsid = cookieValue[5];
                                        _cookiesReport.cfuuidmain = cookieValue[7];
                                        _cookiesReport.cfpoint = cookieValue[9];
                                        cookiesReports.Add(_cookiesReport);



                                    }
                                    
                                }

                                eventList.Clear();
                                break;
                        }

                    }



                }
                
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex.ToString());
            }

            return cookiesReports;
        }
        public static decimal? NullableTryParseDecimal(string text)
        {
            decimal value;
            
            return decimal.TryParse(text, out value) ? (decimal?)value : 0;
        }
        public static string ParseUrl(string text)
        {
            string rtnUrl = string.Empty;
            var values = Regex.Split(text.Split('?')[0], @"(?<=[^/])/(?=[^/])", RegexOptions.None);
            try {
                // bool success = String.IsNullOrWhiteSpace(values);
                bool success = true;
                if (success)
                {
                    if (values.Length > 2)
                    {

                        for (int i = 0; i < values.Length - 2; i++)
                            rtnUrl += values[i] + "/";
                    }
                    else
                    {
                        rtnUrl = values[0];
                    }
                    int indexVal = rtnUrl.LastIndexOf("/") + 1;
                    if (rtnUrl.Length == indexVal)
                    {
                        rtnUrl = rtnUrl.Substring(0, indexVal - 1);
                    }
                }
                
            }
            catch(Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }

            return rtnUrl;
        }
        public static string StringDecode(string text)
        {
            string _str;
            //return _str = Uri.EscapeDataString(text);
            return _str = HttpUtility.UrlDecode(text); 
        }
        public static bool IsNullOrEmpty<T>( T[] array)
        {
            return array == null || array.Length == 0;
        }

    }
}
