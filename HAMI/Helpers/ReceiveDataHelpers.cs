﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using EnsembleCRM.Models;
using Dapper;
using System.Data.SqlClient;
using ChoETL;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using System.IO;
using Google.Apis.AnalyticsReporting.v4.Data;

namespace EnsembleCRM.Helpers
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2007:Consider calling ConfigureAwait on the awaited task", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
    public class ReceiveDataHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<CsvHelpers> _logger { get; set; }
        private CommonHelpers CommonHelpers { get; set; }
        private readonly TwISOCodeHelpers _twISO;
        public ReceiveDataHelpers(IConfiguration config, ILogger<CsvHelpers> logger, CommonHelpers commonHelpers, TwISOCodeHelpers twISO)
        {
            CommonHelpers = commonHelpers;
            _configuration = config;
            _logger = logger;
            _twISO = twISO;
        }
        
        MailContextModel mailContextModel = new MailContextModel()
        {
            ToAddress = "victor.cheng@aurocore.com",
            FromAddress = "caco.tealeaf@gmail.com"
        };
        List<Authorize.MemberInfo> lstMemberInfo = new List<Authorize.MemberInfo>();

        public async Task<int> ImportCustomGA(Authorize.CustomGAReportParameter paras)
        {
            int returnRecordCounts = 0;
            _logger.LogInformation("CustomGAReport");
            string viewID = string.Empty;
            if (string.IsNullOrEmpty(paras.ViewId))
            {
                viewID = "GoogleAnalytics_View:DefaultID";
            }
            else
            {
                viewID = "GoogleAnalytics_View:"+paras.ViewId;;
            }
            List<Authorize.MemberInfo> member = await GetMemberInfo(paras.MemberKey, "Google");
            try
            {
                var token = new TokenResponse { RefreshToken = member.FirstOrDefault().refreshtoken };
                var credentials = new Google.Apis.Auth.OAuth2.UserCredential(new GoogleAuthorizationCodeFlow(
                    new GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = GetClientConfiguration().Secrets,
                    }), member.FirstOrDefault().userid, token);

                using (var svc = new AnalyticsReportingService(
                    new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credentials,
                        ApplicationName = "My Project"
                    }))
                {
                    var dateRange = new DateRange
                    {
                        StartDate = paras.StartDate,
                        EndDate = paras.EndDate
                    };


                    
                    Dictionary<string, Dimension> dictDimensions = SettingGADimension();
                    Dictionary<string, Metric> dictMetrics = SettingGAMetric();
                    List<ReportRequest> requests1 = new List<ReportRequest>();
                    List<DateRange> lstDateRange = new List<DateRange> { dateRange };
                    for (int i = 1; i <= 5; i++) 
                    {
                        List<string> _dimension = new List<string>();
                        List<string> _metric = new List<string>();
                        switch (i)
                        {
                            case 1:

                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric1))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension1))
                                    {
                                        _dimension = paras.ReqDimension1.Split(",").ToList();
                                    }

                                    _metric = paras.ReqMetric1.Split(",").ToList();

                                    var reportRequest1 = generateReportRequest(viewID,lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest1);
                                }

                                break;
                            case 2:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric2))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension2))
                                    {
                                        _dimension = paras.ReqDimension2.Split(",").ToList();
                                    }

                                    _metric = paras.ReqMetric2.Split(",").ToList();

                                    var reportRequest2 = generateReportRequest(viewID,lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest2);
                                }
                                break;
                            case 3:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric3))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension3))
                                    {
                                        _dimension = paras.ReqDimension3.Split(",").ToList();
                                    }

                                    _metric = paras.ReqMetric3.Split(",").ToList();

                                    var reportRequest3 = generateReportRequest(viewID,lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest3);
                                }
                                break;
                            case 4:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric4))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension4))
                                    {
                                        _dimension = paras.ReqDimension4.Split(",").ToList();
                                    }
                                    _metric = paras.ReqMetric4.Split(",").ToList();

                                    var reportRequest4 = generateReportRequest(viewID,lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest4);
                                }
                                break;
                            case 5:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric5))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension5))
                                    {
                                        _dimension = paras.ReqDimension5.Split(",").ToList();
                                    }
                                    _metric = paras.ReqMetric5.Split(",").ToList();

                                    var reportRequest5 = generateReportRequest(viewID,lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest5);
                                }
                                break;
                        }


                    }







                    // Create the GetReportsRequest object.
                    GetReportsRequest getRequest1 = new GetReportsRequest() { ReportRequests = requests1 };


                    List<GoogleAnalyricsData.GA_Reports> dataRows1 = await GetGAReport(getRequest1, svc);

                    List<GoogleAnalyricsData.ResponseGAResult> gaResult = new List<GoogleAnalyricsData.ResponseGAResult>();
                    gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows1 });

                   

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                //Console.WriteLine(ex.ToString());
               
            }
            return returnRecordCounts;
        }

       
        private async Task<List<Authorize.MemberInfo>> GetMemberInfo(string memberKey, string tokentype)
        {
            _logger.LogInformation("GetMmberInfo");
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string checkSql = @"select userid,memberkey,refreshtoken from 
                token where tokentype=@tokenType and memberkey=@memberkey";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    await connection.OpenAsync();

                    lstMemberInfo = connection.Query<Authorize.MemberInfo>(checkSql, new { memberkey = memberKey, tokenType = tokentype }).ToList();
                    return lstMemberInfo;
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return lstMemberInfo;
            }


        }
        private ReportRequest generateReportRequest(string viewID, List<DateRange> dateRange, List<string> dimension, List<string> metric, Dictionary<string, Dimension> dictDimensions, Dictionary<string, Metric> dictMetric)
        {
            IList<Dimension> reqDimension = new List<Dimension>();
            IList<Metric> reqMetric = new List<Metric>();
            for (int j = 0; j < dimension.Count; j++)
            {
                reqDimension.Add(dictDimensions[dimension[j]]);
            }
            for (int j = 0; j < metric.Count; j++)
            {
                reqMetric.Add(dictMetric[metric[j]]);
            }
            var reportRequest = new ReportRequest
            {
                DateRanges = dateRange,
                Dimensions = reqDimension,
                Metrics = reqMetric,
                ViewId = _configuration.GetSection(viewID).Value
            };
            return reportRequest;
        }
        private async Task<List<GoogleAnalyricsData.GA_Reports>> GetGAReport(GetReportsRequest request, AnalyticsReportingService svc, bool limit = true)
        {
            _logger.LogInformation("GetGAReport");
            List<GoogleAnalyricsData.GA_Reports> lstReports = new List<GoogleAnalyricsData.GA_Reports>();

            try
            {
                GetReportsResponse response1 = await svc.Reports.BatchGet(request).ExecuteAsync();
                //printResults(response1.Reports as List<Report>);
                for (int i = 0; i < response1.Reports.Count; i++)
                {
                    List<string> MetricHeaderEntries = new List<string>();
                    List<GoogleAnalyricsData.GA_Results> lstresult = new List<GoogleAnalyricsData.GA_Results>();
                    int rowcount = response1.Reports[i].Data.RowCount.HasValue ? response1.Reports[i].Data.RowCount.Value : 0;
                    int reportDataRowcount = 0;
                    if (limit)
                    {
                        reportDataRowcount = rowcount >= 1000 ? 1000 : rowcount;
                    }
                    else
                    {
                        reportDataRowcount = rowcount;
                    }

                    for (int j = 0; j < response1.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries.Count; j++)
                    {
                        MetricHeaderEntries.Add(response1.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries[j].Name);
                    }
                    for (int j = 0; j < reportDataRowcount; j++)
                    {

                        List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();

                        for (int k = 0; k < response1.Reports[i].Data.Rows[j].Metrics.Count; k++)
                        {
                            List<string> lstMetricName = new List<string>();
                            List<GoogleAnalyricsData.Metrics> lstMetrics = new List<GoogleAnalyricsData.Metrics>();
                            for (int l = 0; l < response1.Reports[i].Data.Rows[j].Metrics[k].Values.Count; l++)
                            {

                                GoogleAnalyricsData.Metrics _metric = new GoogleAnalyricsData.Metrics();

                                _metric.value = response1.Reports[i].Data.Rows[j].Metrics[k].Values[l];
                                if (_metric.value.Contains("."))
                                {

                                    _metric.value = formatString(_metric.value);
                                }


                                lstMetrics.Add(_metric);
                                // lstMetricName.Add(response1.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries[l].Name);
                            }
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics });
                        }


                        int dimCount = 0;
                        try
                        {


                            if (response1.Reports[i].Data.Rows[j].Dimensions != null)
                            {
                                dimCount = response1.Reports[i].Data.Rows[j].Dimensions.Count;
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex.ToString());
                        }


                        List<string> lstDimension = new List<string>();
                        if (dimCount != 0)
                        {
                            for (int k = 0; k < response1.Reports[i].Data.Rows[j].Dimensions.Count; k++)
                            {
                                if (response1.Reports[i].ColumnHeader.Dimensions[0].ToString() == "ga:regionIsoCode")
                                {

                                    var _val = _twISO.IsoCodeConverter(response1.Reports[i].Data.Rows[j].Dimensions[0]);
                                    if (_val.Count == 0)
                                    {
                                        lstDimension.Add(response1.Reports[i].Data.Rows[j].Dimensions[k]);
                                    }
                                    else
                                    {
                                        lstDimension.Add(_val[0].ShortName);
                                    }

                                }
                                else
                                {
                                    lstDimension.Add(response1.Reports[i].Data.Rows[j].Dimensions[k]);

                                }


                            }

                        }
                        else
                        {
                            lstDimension.Add("Date");
                        }

                        lstresult.Add(new GoogleAnalyricsData.GA_Results { metrics = lstGAMetrics, dimension = lstDimension });


                    }

                    lstReports.Add(new GoogleAnalyricsData.GA_Reports { ListResult = lstresult, MetricHead = MetricHeaderEntries });
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                Console.WriteLine(ex);
            }
            return lstReports;
        }
        private string formatString(string Str)
        {
            string formatted = String.Empty;
            if (Str.Contains("."))
            {
                string[] tmpStr = Str.Split(".");
                string xvalue1 = tmpStr[0];
                string xvalue2 = tmpStr[1];


                if (xvalue2.Length > 2)
                {
                    formatted = xvalue1 + "." + xvalue2.Substring(0, 2);
                }
                else
                {
                    formatted = xvalue1 + "." + xvalue2;
                }

            }
            else
            {
                formatted = Str;
            }
            return formatted;
        }
        private static GoogleClientSecrets GetClientConfiguration()
        {
            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                return GoogleClientSecrets.Load(stream);
            }
        }
        private Dictionary<string, Dimension> SettingGADimension()
        {
            Dictionary<string, Dimension> dictDimension = new Dictionary<string, Dimension>();
            dictDimension.Add("date", new Dimension { Name = "ga:date" });
            dictDimension.Add("source", new Dimension { Name = "ga:source" });
            dictDimension.Add("country", new Dimension { Name = "ga:country" });
            dictDimension.Add("campaign", new Dimension { Name = "ga:campaign" });
            dictDimension.Add("adGroup", new Dimension { Name = "ga:adGroup" });
            dictDimension.Add("keyword", new Dimension { Name = "ga:keyword" });
            dictDimension.Add("exitPagePath", new Dimension { Name = "ga:exitPagePath" });
            dictDimension.Add("sourceMedium", new Dimension { Name = "ga:sourceMedium" });
            dictDimension.Add("medium", new Dimension { Name = "ga:medium" });
            dictDimension.Add("deviceCategory", new Dimension { Name = "ga:deviceCategory" });
            dictDimension.Add("fullReferrer", new Dimension { Name = "ga:fullReferrer" });
            dictDimension.Add("userGender", new Dimension { Name = "ga:userGender" });
            dictDimension.Add("userAgeBracket", new Dimension { Name = "ga:userAgeBracket" });
            dictDimension.Add("dayOfWeekName", new Dimension { Name = "ga:dayOfWeekName" });
            dictDimension.Add("pagePath", new Dimension { Name = "ga:pagePath" });
            dictDimension.Add("pagePath1", new Dimension { Name = "ga:pagePathLevel1" });
            dictDimension.Add("region", new Dimension { Name = "ga:region" });
            dictDimension.Add("regionId", new Dimension { Name = "ga:regionId" });
            dictDimension.Add("userType", new Dimension { Name = "ga:userType" });
            dictDimension.Add("productSku", new Dimension { Name = "ga:productSku" });
            

            return dictDimension;
        }
        /// <summary>
        /// Define Metrics 
        /// </summary>
        /// <returns> Metrics </returns>
        private Dictionary<string, Metric> SettingGAMetric()
        {
            Dictionary<string, Metric> dictMetric = new Dictionary<string, Metric>();
            dictMetric.Add("pageviews", new Metric { Expression = "ga:pageviews", Alias = "瀏覽量" });
            dictMetric.Add("uniquePageviews", new Metric { Expression = "ga:uniquePageviews", Alias = "不重複瀏覽量" });
            dictMetric.Add("users", new Metric { Expression = "ga:users", Alias = "訪客數" });
            dictMetric.Add("newUsers", new Metric { Expression = "ga:newUsers", Alias = "新訪客數" });
            dictMetric.Add("bounces", new Metric { Expression = "ga:bounces", Alias = "跳出數" });
            dictMetric.Add("bounceRate", new Metric { Expression = "ga:bounceRate", Alias = "跳出率" });
            dictMetric.Add("sessionDuration", new Metric { Expression = "ga:sessionDuration", Alias = "工作階段時間長度" });
            dictMetric.Add("avgSessionDuration", new Metric { Expression = "ga:avgSessionDuration", Alias = "平均工作階段時間長度" });
            dictMetric.Add("exitRate", new Metric { Expression = "ga:exitRate", Alias = "離開率" });
            dictMetric.Add("exits", new Metric { Expression = "ga:exits", Alias = "離開數" });
            dictMetric.Add("hits", new Metric { Expression = "ga:hits", Alias = "匹配" });
            dictMetric.Add("avgTimeOnPage", new Metric { Expression = "ga:avgTimeOnPage", Alias = "平均頁面瀏覽時長" });
            dictMetric.Add("sessions", new Metric { Expression = "ga:sessions", Alias = "工作階段" });
            dictMetric.Add("avgPageLoadTime", new Metric { Expression = "ga:avgPageLoadTime", Alias = "平均頁面載入時間" });
            dictMetric.Add("avgServerResponseTime", new Metric { Expression = "ga:avgServerResponseTime", Alias = "avgServerResponseTime" });
            dictMetric.Add("pageviewsPerSession", new Metric { Expression = "ga:pageviewsPerSession", Alias = "平均工作階段頁面瀏覽數" });
            dictMetric.Add("entranceRate", new Metric { Expression = "ga:entranceRate", Alias = "entranceRate" });
            dictMetric.Add("_1dayUsers", new Metric { Expression = "ga:1dayUsers", Alias = "1dayUsers" });
            dictMetric.Add("_7dayUsers", new Metric { Expression = "ga:7dayUsers", Alias = "7dayUsers" });
            dictMetric.Add("_14dayUsers", new Metric { Expression = "ga:14dayUsers", Alias = "14dayUsers" });
            dictMetric.Add("_28dayUsers", new Metric { Expression = "ga:28dayUsers", Alias = "28dayUsers" });
            dictMetric.Add("_30dayUsers", new Metric { Expression = "ga:30dayUsers", Alias = "30dayUsers" });
            dictMetric.Add("impressions", new Metric { Expression = "ga:impressions", Alias = "曝光" });
            dictMetric.Add("adClicks", new Metric { Expression = "ga:adClicks", Alias = "廣告點擊" });
            dictMetric.Add("adCost", new Metric { Expression = "ga:adCost", Alias = "廣告成本" });
            dictMetric.Add("CPM", new Metric { Expression = "ga:CPM", Alias = "CPM" });
            dictMetric.Add("CPC", new Metric { Expression = "ga:CPC", Alias = "CPC" });
            dictMetric.Add("CTR", new Metric { Expression = "ga:CTR", Alias = "CTR" });
            dictMetric.Add("costPerGoalConversion", new Metric { Expression = "ga:costPerGoalConversion", Alias = "costPerGoalConversion" });
            dictMetric.Add("costPerTransaction", new Metric { Expression = "ga:costPerTransaction", Alias = "costPerTransaction" });
            dictMetric.Add("RPC", new Metric { Expression = "ga:RPC", Alias = "RPC" });
            dictMetric.Add("ROAS", new Metric { Expression = "ga:ROAS", Alias = "ROAS" });
            dictMetric.Add("transactions", new Metric { Expression = "ga:transactions", Alias = "交易次數" });
            dictMetric.Add("totalValue", new Metric { Expression = "ga:totalValue", Alias = "總價值" });
            dictMetric.Add("transactionRevenue", new Metric { Expression = "ga:transactionRevenue", Alias = "收益" });
            dictMetric.Add("transactionsPerSession", new Metric { Expression = "ga:transactionsPerSession", Alias = "電商轉換率" });
            dictMetric.Add("revenuePerTransaction", new Metric { Expression = "ga:revenuePerTransaction", Alias = "平均訂單價值" });
            dictMetric.Add("goal1Completions", new Metric { Expression = "ga:goal1Completions", Alias = "加入會員" });
            dictMetric.Add("goal2Completions", new Metric { Expression = "ga:goal2Completions", Alias = "購物車檢視" });
            dictMetric.Add("goal3Completions", new Metric { Expression = "ga:goal3Completions", Alias = "交易完成" });
            dictMetric.Add("goal4Completions", new Metric { Expression = "ga:goal4Completions", Alias = "交易完成(海外)" });
            dictMetric.Add("itemQuantity", new Metric { Expression = "ga:itemQuantity", Alias = "購買數量" });
            dictMetric.Add("itemsPerPurchase", new Metric { Expression = "ga:itemsPerPurchase", Alias = "平均購買數量" });
            dictMetric.Add("avgEventValue", new Metric { Expression = "ga:avgEventValue", Alias = "平均訂單價值" });
            dictMetric.Add("revenuePerItem", new Metric { Expression = "ga:revenuePerItem", Alias = "平均結賬單價" });
            dictMetric.Add("itemRevenue", new Metric { Expression = "ga:itemRevenue", Alias = "產品收益" });
            dictMetric.Add("productListClicks", new Metric { Expression = "ga:productListClicks", Alias = "產品點擊次數" });
            dictMetric.Add("productCheckouts", new Metric { Expression = "ga:productCheckouts", Alias = "產品加入購物車次數" });


            return dictMetric;
        }
    }
}
