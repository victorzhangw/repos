﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnsembleCRM.Interfaces;
using EnsembleCRM.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;

namespace EnsembleCRM.Helpers
{
    public class InboundEvtHelpers
    {
        private static IConfiguration _configuration;
        private static ILogger<InboundEvtHelpers> _logger { get; set; }
        public InboundEvtHelpers(IConfiguration config, ILogger<InboundEvtHelpers> logger)
        {
            _configuration = config;
            _logger = logger;

        }
        public class MailEvent : JobInterface.InboundEvtInterface
        {
            public int Create(InboundEvent inboundEvent)
            {
                throw new NotImplementedException();
            }

            public int Delete(int id)
            {
                throw new NotImplementedException();
            }

            public InboundEvent Read(int id)
            {
                throw new NotImplementedException();
            }

            public List<InboundEvent> ReadAll()
            {
                throw new NotImplementedException();
            }
            public int ImportData()
            {
                
                throw new NotImplementedException();
            }

            public int Update(InboundEvent inbound)
            {
                throw new NotImplementedException();
            }
        }
        public class FaxEvent : JobInterface.InboundEvtInterface
        {
            public int Create(InboundEvent inboundEvent)
            {
                throw new NotImplementedException();
            }

            public int Delete(int id)
            {
                throw new NotImplementedException();
            }

            public int ImportData()
            {
                throw new NotImplementedException();
            }

            public InboundEvent Read(int id)
            {
                throw new NotImplementedException();
            }

            public List<InboundEvent> ReadAll()
            {
                throw new NotImplementedException();
            }

            public int Update(InboundEvent inbound)
            {
                throw new NotImplementedException();
            }
        }
    }
}
