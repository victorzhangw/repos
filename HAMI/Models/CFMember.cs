﻿namespace EnsembleCRM.Models
{
    public class CFMember
    {
        public string cft_uid { get; set; }
        public string cft_p { get; set; }
        public string grade { get; set; }
        public string last_value_time { get; set; }

        public string Id_Origin { get; set; }
        public string Createtime { get; set; }
        public string ModifiedTime { get; set; }
        public string Createuser { get; set; }
        public string Modifieduser { get; set; }
    }
    public class Cdp_Contact
    {
        public string ID { get; set; }

        public string GRADE { get; set; }
        public string LASTVALUETIME { get; set; }

        public string ID_ORIGIN { get; set; }
        public string CREATETIME { get; set; }
        public string MODIFIEDTIME { get; set; }
        public string CREATEUSER { get; set; }
        public string MODIFIEDUSER { get; set; }
    }
}
