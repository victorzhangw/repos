﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Models
{
    public class CTI
    {
        public class InfoACDCalls
        {
            public string CID { get; set; }
            public string C_From { get; set; }
            public DateTime Timestamp { get; set; }
        }
    }
}
