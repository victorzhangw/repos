﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Models
{
    public class MfParameters
    {
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }

    }
    public class Report
    {
       
        public string data_time { get; set; }
        public string advertiser { get; set; }
        public string campaign { get; set; }
        public string campaign_id { get; set; }
        public string strategy { get; set; }
        public string strategy_id { get; set; }
        public string creative { get; set; }
        public string creative_id { get; set; }
        public string budget { get; set; }
        public string impress { get; set; }
        public string click{ get; set; }
        public string clickrate { get; set; }
        public string ecpm { get; set; }
        public string ecpc { get; set; }
        

    }
    public class MFMonthly:MFDaily
    {

       


    }

    public class Location
    {
        public string country { get; set; }
        public string cadreon_entity_loc { get; set; }
        public string currency { get; set; }
        public string time_zone { get; set; }
    }
    public class MFDaily
    {
        public Location location { get; set; }
        public int status { get; set; }
        public string msg { get; set; }
        public List<Report> report { get; set; }
        public string market { get; set; }
    }
}
