﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hami.Models
{
    public class MultiForceAuth
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
