﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace EnsembleCRM.Models
{
     class BasicSetting
    {
        public class BasicOption
        {
            public List <CDPOption> CDPOptions { get; set; }
        }
        public class CDPOption
        {
            public string pagekey { get; set; }
            public string metakey { get; set; }
            public string metavalue { get; set; }

            
        }
        public enum PageKey
        {
            EcommercePOS
        }
        public enum MetaKey
        {
            chart
        }
        public enum Region
        {

        }
    }
}
