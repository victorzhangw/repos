﻿namespace EnsembleCRM.Models
{
    public class CFTags
    {
        public string Cft_p { get; set; }
        public string Cft_uid { get; set; }
        public string Label { get; set; }
        public string CreateDatetime { get; set; }
    }
}
