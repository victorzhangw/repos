﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Models
{
    public class CustomerTags
    {

        public string TltId { get; set; }
        public string Cftuid { get; set; }
        public string Cftp { get; set; }
        public string Grade { get; set; }
        public string Tags { get; set; }



    }
    public class CustomerRank
    {
        public List<CustomerGrade> customerGrades { get; set; }
        // [JsonProperty(PropertyName =)]
        public List<SegmentCustomerWeek> segmentCustomerWeeks { get; set; }
        public List<SegmentCustomerWeekGrade> segmentCustomerWeekGrades { get; set; }
        public List<CustomerWeekCount> customerWeekCounts { get; set; }
        public List<CustomerCate> GenderCates { get; set; }
        public List<CustomerCate> AgeCates { get; set; }
        public List<CustomerCate> LocationCates { get; set; }
        public List<CustomerCate> DayCates { get; set; }
        public List<CustomerCate> WeekCates { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class CustomerGrade
    {
        public int Grade { get; set; }
        public int RangeCount { get; set; }
        public string RangeCountDesc { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class SegmentCustomerWeek
    {

        public string Grade { get; set; }
        public int MemberCount1 { get; set; }
        public int MemberCount2 { get; set; }
        public int MemberCount3 { get; set; }
        public int MemberCount4 { get; set; }

    }
    /// <summary>
    /// View:VIEW_SEGMENTCUSTOMERRANK
    /// </summary>
    public class SegmentCustomerWeekGrade
    {

        public string RangeCountDesc { get; set; }

        public int RangeCount { get; set; }

    }
    /// <summary>
    /// View:VIEW_WEEKCUSTOMERRANK
    /// </summary>
    public class CustomerWeekCount
    {

        public string WeekRangeDesc { get; set; }

        public string WeekRange { get; set; }
        public int WeekRangeCount { get; set; }

    }
    public class CustomerCate
    {

        public string Category { get; set; }

        public string Name { get; set; }
        public string Value { get; set; }

    }

}
