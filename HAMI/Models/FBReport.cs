﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Models
{
    public class FbParameters
    {
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }

    }
    public class FBReport
    {
        public string Eventdate { get; set; }       
        public string CampaignName { get; set; }
        public string CampaignId { get; set; }
        public string AdId { get; set; }
        public string AdName { get; set; }
        public string AdsetId { get; set; }
        public string AdsetName { get; set; }
        public string Spends { get; set; }
        public string Impression { get; set; }
        public string Clicks { get; set; }
        public string REACH { get; set; }
        public string CPC { get; set; }
        public string CTR { get; set; }
        public string Inlinelinkclicks { get; set; }
        public string Inlinelinkclickctr { get; set; }


    }
}
