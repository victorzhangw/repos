﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Models
{
    public class InboundEvent
    {
		//public int INEVENTID { get; set; }
		public string SUBJECT { get; set; }
		public string CONTEXT { get; set; }
		public string ATTACHMENTS { get; set; }
		public string SOURCE { get; set; }
		public string HOMEACCOUNT { get; set; }
		public string STATUS { get; set; }
		public DateTime TIME_STAMP { get; set; }
	}
}
