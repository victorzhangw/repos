﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using EnsembleCRM.Helpers;
using RestSharp;
using Newtonsoft.Json;
using EnsembleCRM.Models;
using System.Data.SqlClient;
using Dapper;

namespace hami.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FBController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        private ILogger<FBController> _logger { get; set; }
        private CommonHelpers _common;
        public FBController(IConfiguration config, TwISOCodeHelpers twISO, ILogger<FBController> logger, CommonHelpers commonHelpers)
        {

            _configuration = config;
            _logger = logger;
            _common = commonHelpers;
        }
        [HttpPost]
        public async Task<List<FBReport>> FBDailyReport(FbParameters fbParameters)
        {
            List<FBReport> fBReports = new();

            if (string.IsNullOrEmpty(fbParameters.StartDateTime))
            {
                fbParameters.StartDateTime = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                fbParameters.EndDateTime = fbParameters.StartDateTime;
            }
            if (string.IsNullOrEmpty(fbParameters.EndDateTime))
            {
                
                fbParameters.EndDateTime = fbParameters.StartDateTime;
            }


            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string eventSql = @"SELECT * FROM [dbo].[FACEBOOK] WHERE EVENTDATE BETWEEN @startdate and @enddate";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    await connection.OpenAsync();

                    var fBs = await connection.QueryAsync<FBReport>(eventSql, new { startdate = fbParameters.StartDateTime ,enddate= fbParameters.EndDateTime});
                    fBReports = fBs.ToList();
                    
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return fBReports;
            }

            return fBReports;

        }
       
    }
}
