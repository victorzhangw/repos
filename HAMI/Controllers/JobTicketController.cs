﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobTicketController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<JobTicketController> _logger { get; set; }

        public JobTicketController(IConfiguration config, ILogger<JobTicketController> logger)
        {
            _logger = logger;
            _configuration = config;

        }
    }
}
