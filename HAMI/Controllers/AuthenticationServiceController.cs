﻿using Dapper;
using EnsembleCRM.Helpers;
using EnsembleCRM.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace hami.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AuthenticationServiceController : ControllerBase
    {
        private readonly JwtHelpers jwt;
        private readonly IConfiguration _configuration;
        private ILogger<AuthenticationServiceController> _logger { get; set; }
        public AuthenticationServiceController(JwtHelpers jwt, IConfiguration config, ILogger<AuthenticationServiceController> logger)
        {
            this.jwt = jwt;
            _configuration = config;
            _logger = logger;
        }


        List<Authorize.MemberInfo> lstMemberInfo = new List<Authorize.MemberInfo>();
        [AllowAnonymous]
        [HttpPost]
        public ActionResult<string> SignIn([FromForm] LoginViewModel login)
        {
            _logger.LogInformation("AuthenticationService-SignIn");
            var validated = ValidateUser(login);
            if (validated.ValidateStatus)
            {
                return jwt.GenerateToken(login.Username);
            }
            else
            {
                return validated.Description;
            }
        }

        private ValidateResult ValidateUser(LoginViewModel login)
        {

            ValidateResult validateResult = new ValidateResult();
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string checkSql = @"select USERACCOUNT,PASSWORD from 
                USERS where USERACCOUNT=@username and PASSWORD=@password";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    connection.Open();

                    var lstMemberInfo = connection.Query<LoginViewModel>(checkSql, new { username = login.Username, password = login.Password }).ToList();
                    bool returnBool = (lstMemberInfo.Count == 1) ? true : false;
                    validateResult.ValidateStatus = returnBool;
                    validateResult.Description = (validateResult.ValidateStatus) ? "OK" : "NO Member!";
                    return validateResult;
                }

            }
            catch (Exception ex)
            {
                validateResult.ValidateStatus = false;
                validateResult.Description = ex.ToString();
                _logger.LogError(ex.ToString());
                //Console.WriteLine(e);

                return validateResult;
            }


        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult<string> BasicSetting(string siteName)
        {
            string returnJson = string.Empty;
            ValidateResult validateResult = new ValidateResult();
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string optionSql = @"SELECT * FROM 
                cdp_options WHERE pagekey=@pagekey AND  metakey=@metakey AND application=@site";
            List<BasicSetting.BasicOption> lstOption = new List<BasicSetting.BasicOption>();


            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    foreach (var pk in Enum.GetValues(typeof(BasicSetting.PageKey)))
                    {
                        string _pk = pk.ToString();
                        List<BasicSetting.CDPOption> querylist = new List<BasicSetting.CDPOption>();
                        foreach (var mk in Enum.GetValues(typeof(BasicSetting.MetaKey)))
                        {
                            string _mk = mk.ToString();
                            querylist = connection.Query<BasicSetting.CDPOption>(optionSql, new { pagekey = _pk, metakey = _mk, site = siteName }).ToList();
                        }
                        lstOption.Add(new BasicSetting.BasicOption { CDPOptions = querylist });
                    }






                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }

            string json = JsonConvert.SerializeObject(lstOption);

            json = Regex.Replace(json, @"[\\\s]", "");
            json = json.Replace(":\"{", ":{");
            json = json.Replace("}\"}", "}}");
            returnJson = json;
            return returnJson;
        }

        [HttpGet]
        public IActionResult GetClaims()
        {
            return Ok(User.Claims.Select(p => new { p.Type, p.Value }));
        }

        [HttpGet]
        public IActionResult GetUserName()
        {

            return Ok(User.Identity.Name);
        }

        [HttpGet]
        public IActionResult GetUniqueId()
        {
            var jti = User.Claims.FirstOrDefault(p => p.Type == "jti");
            return Ok(jti.Value);
        }
    }
    /// <summary>
    /// Retrieve Membetr informations
    /// </summary>
    /// <param name="memberKey"></param>
    /// <param name="tokentype"></param>
    /// <returns></returns>

    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class ValidateResult
    {
        public bool ValidateStatus { get; set; }
        public string Description { get; set; }
    }

}
