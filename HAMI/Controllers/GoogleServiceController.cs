﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Services;
using EnsembleCRM.Models;
using Newtonsoft.Json;
using Dapper;
using System.Data.SqlClient;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Globalization;
using EnsembleCRM.Helpers;
using Microsoft.Extensions.Logging;
using System.Data;
using MimeKit;
using static EnsembleCRM.Models.Authorize;
using ChoETL;


namespace EnsembleCRM.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    
    public class GoogleServiceController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly TwISOCodeHelpers _twISO;
        private ILogger<GoogleServiceController> _logger { get; set; }
        private CommonHelpers _common;
        private DataInsightHelpers _dataInsight;
        public GoogleServiceController(IConfiguration config, TwISOCodeHelpers twISO, ILogger<GoogleServiceController> logger,CommonHelpers commonHelpers,DataInsightHelpers dataInsightHelpers)
        {
            _twISO = twISO;
            _configuration = config;
            _logger = logger;
            _common = commonHelpers;
            _dataInsight = dataInsightHelpers;

        }
        List<Authorize.MemberInfo> lstMemberInfo = new List<Authorize.MemberInfo>();
        /// <summary>
        /// Get GAReport
        /// </summary>
        /// <param name="paras">MemberKey</param>
        /// <param name="paras">StartDate</param>
        /// <param name="paras">EndDate</param>
        /// <param name="paras">LoginId</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> GAReport([FromForm] Authorize.GAReportParameter paras)

        {

            _logger.LogInformation("GA-Report");
            string returnjson = string.Empty;
            string viewID = string.Empty;
            if (string.IsNullOrEmpty(paras.ViewId))
            {
                viewID = "GoogleAnalytics_View:DefaultID";
            }
            else
            {
                viewID = "GoogleAnalytics_View:"+paras.ViewId;
            }
            int pageSize = 10000;
            if (!string.IsNullOrEmpty(paras.PageSize))
            {
                bool success = Int32.TryParse(paras.PageSize, out pageSize);
            }
           
            
            List<Authorize.MemberInfo> member = await GetMemberInfo(paras.MemberKey, "Google");
            string thisPeriod7DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd");
            string thisPeriod7DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-6)).ToString("yyyy-MM-dd");
            string lastPeriod7DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-7).ToString("yyyy-MM-dd");
            string lastPeriod7DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-13)).ToString("yyyy-MM-dd");
            string lastPeriod14DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-14).ToString("yyyy-MM-dd");
            string lastPeriod14DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-19)).ToString("yyyy-MM-dd");
            string lastPeriod21DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-15).ToString("yyyy-MM-dd");
            string lastPeriod21DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-21)).ToString("yyyy-MM-dd");
            
            try
            {
                var token = new TokenResponse { RefreshToken = member.FirstOrDefault().refreshtoken };
                var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                    new GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = GetClientConfiguration().Secrets,
                    }), member.FirstOrDefault().userid, token);

                using (var svc = new AnalyticsReportingService(
                    new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credentials,
                        ApplicationName = "My Project"
                    }))
                {
                    var dateRange = new DateRange
                    {
                        StartDate = paras.StartDate,
                        EndDate = paras.EndDate
                    };


                    var dateRange7 = new DateRange
                    {
                        StartDate = "7daysAgo",
                        EndDate = paras.EndDate
                    };

                    var dateRange14 = new DateRange
                    {
                        StartDate = "14daysAgo",
                        EndDate = paras.EndDate
                    };

                    var dateRange28 = new DateRange
                    {
                        StartDate = "28daysAgo",
                        EndDate = paras.EndDate
                    };
                    var thisPeriod7Days = new DateRange
                    {
                        StartDate = thisPeriod7DStartdate,
                        EndDate = thisPeriod7DEnddate
                    };
                    var lastPeriod7Days = new DateRange
                    {
                        StartDate = lastPeriod7DStartdate,
                        EndDate = lastPeriod7DEnddate
                    };
                    var lastPeriod14Days = new DateRange
                    {
                        StartDate = lastPeriod14DStartdate,
                        EndDate = lastPeriod14DEnddate
                    };
                    var lastPeriod21Days = new DateRange
                    {
                        StartDate = lastPeriod21DStartdate,
                        EndDate = lastPeriod21DEnddate
                    };
                    Dictionary<string, Dimension> dictDimensions = SettingGADimension();
                    Dictionary<string, Metric> dictMetrics = SettingGAMetric();
                    //Dimension :Date //
                    var reportRequest1 = new ReportRequest
                    {
                        DateRanges = new List<DateRange> { dateRange },
                       // Dimensions = new List<Dimension> { dictDimensions["date"],dictDimensions["sourceMedium"] },
                        Metrics = new List<Metric> {
                            dictMetrics["sessions"],
                            dictMetrics["newUsers"],
                            dictMetrics["users"],
                            dictMetrics["bounceRate"],
                            dictMetrics["uniquePageviews"],
                            dictMetrics["pageviewsPerSession"],
                            dictMetrics["avgSessionDuration"],
                            dictMetrics["avgTimeOnPage"],
                            dictMetrics["pageviews"],
                            dictMetrics["timeOnPage"],
                        },
                        PageSize= pageSize,
                        ViewId = _configuration.GetSection(viewID).Value
                    };
                    //Dimension :sourceMedium //
                    var reportRequest2 = new ReportRequest
                    {
                        DateRanges = new List<DateRange> { dateRange },
                        Dimensions = new List<Dimension> { dictDimensions["source"] },
                        Metrics = new List<Metric> {
                            dictMetrics["sessions"],
                            dictMetrics["newUsers"],
                            dictMetrics["users"],
                            dictMetrics["bounceRate"],
                            dictMetrics["uniquePageviews"],
                            dictMetrics["pageviewsPerSession"],
                            dictMetrics["avgSessionDuration"],
                            dictMetrics["avgTimeOnPage"],
                            dictMetrics["pageviews"],
                            dictMetrics["timeOnPage"],
                        },
                        PageSize = pageSize,
                        ViewId = _configuration.GetSection(viewID).Value
                    };
                    //Dimension :userGender //
                    var reportRequest3 = new ReportRequest
                    {
                        DateRanges = new List<DateRange> { dateRange },
                        Dimensions = new List<Dimension> { dictDimensions["userGender"], dictDimensions["userAgeBracket"] },
                        Metrics = new List<Metric> {
                            dictMetrics["sessions"],
                            dictMetrics["pageviewsPerSession"],
                            dictMetrics["newUsers"],
                            dictMetrics["users"],
                            dictMetrics["avgSessionDuration"],
                            dictMetrics["avgTimeOnPage"],
                            dictMetrics["bounceRate"],
                            dictMetrics["uniquePageviews"]
                        },
                        ViewId = _configuration.GetSection(viewID).Value
                    };
                    //Dimension :deviceCategory //
                    var reportRequest4 = new ReportRequest
                    {
                        DateRanges = new List<DateRange> { dateRange },
                        Dimensions = new List<Dimension> { dictDimensions["deviceCategory"] },
                        Metrics = new List<Metric> {
                            dictMetrics["sessions"],
                            dictMetrics["pageviewsPerSession"],
                            dictMetrics["newUsers"],
                            dictMetrics["users"],
                            dictMetrics["avgSessionDuration"],
                            dictMetrics["avgTimeOnPage"],
                            dictMetrics["bounceRate"],
                            dictMetrics["totalValue"]
                        },
                        ViewId = _configuration.GetSection(viewID).Value
                    };
                    // Dimension :Page //
                    var reportRequest5 = new ReportRequest
                    {
                        DateRanges = new List<DateRange> { dateRange },
                        Dimensions = new List<Dimension> { dictDimensions["pageTitle"], dictDimensions["userType"] },
                        Metrics = new List<Metric> {
                            dictMetrics["sessions"],
                            dictMetrics["pageviewsPerSession"],
                            dictMetrics["newUsers"],
                            dictMetrics["users"],
                            dictMetrics["avgSessionDuration"],
                            dictMetrics["avgTimeOnPage"],
                            dictMetrics["bounces"],

                        },
                        PageSize = pageSize,
                        FiltersExpression = "ga:userType==New Visitor",
                        ViewId = _configuration.GetSection(viewID).Value
                    };
                    // Dimension :7days //
                    OrderBy ordering_peroid = new OrderBy()
                    {
                        SortOrder = "DESCENDING",
                        FieldName = "ga:users"
                    };
                    var reportRequest6 = new ReportRequest
                    {

                        DateRanges = new List<DateRange>() { dateRange },
                        Dimensions = new List<Dimension>() { dictDimensions["fullReferrer"] },
                        Metrics = new List<Metric>() {
                            dictMetrics["sessions"],
                            dictMetrics["pageviewsPerSession"],
                            dictMetrics["newUsers"],
                            dictMetrics["users"],
                            dictMetrics["avgSessionDuration"],
                            dictMetrics["avgTimeOnPage"],
                            dictMetrics["bounceRate"],
                            dictMetrics["uniquePageviews"],
                            dictMetrics["transactionsPerSession"],
                            dictMetrics["revenuePerTransaction"],

                        },
                        FiltersExpression = "ga:users>10",
                        OrderBys = new List<OrderBy> { ordering_peroid },
                        ViewId = _configuration.GetSection(viewID).Value

                    };

                    List<ReportRequest> requests1 = new List<ReportRequest>
                    {
                        reportRequest1,
                        reportRequest2,
                        reportRequest3,
                        reportRequest4,
                        reportRequest5
                    };
                    List<ReportRequest> requests2 = new List<ReportRequest>
                    {
                        reportRequest6,

                    };

                    // Create the GetReportsRequest object.
                    GetReportsRequest getRequest1 = new GetReportsRequest() { ReportRequests = requests1 };
                    GetReportsRequest getRequest2 = new GetReportsRequest() { ReportRequests = requests2 };

                    List<GoogleAnalyricsData.GA_Reports> dataRows1 = await GetGAReport(getRequest1, svc);
                    List<GoogleAnalyricsData.GA_Reports> dataRows2 = await GetGAReport(getRequest2, svc);
                    List<GoogleAnalyricsData.ResponseGAResult> gaResult = new List<GoogleAnalyricsData.ResponseGAResult>();
                    gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows1 });
                    gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows2 });
                    //回傳
                    returnjson = JsonConvert.SerializeObject(gaResult);

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());

                //Console.WriteLine(ex.ToString());
                return (JsonConvert.SerializeObject(ex));

            }

            return returnjson;
        }

        
        public async Task<string> CustomGAReport([FromForm] Authorize.CustomGAReportParameter paras)

        {

            _logger.LogInformation("CustomGAReport");
            string viewID = string.Empty;
            if (string.IsNullOrEmpty(paras.ViewId)|| paras.ViewId=="undefined")
            {
                viewID = "GoogleAnalytics_View:DefaultID";
            }
            else
            {
                viewID = "GoogleAnalytics_View:"+paras.ViewId;;
            }
            //string eventname = "GoogleService-CustomGAReport";
            //string ecsource = "GA";
            string returnjson = string.Empty;
            List<Authorize.MemberInfo> member = await GetMemberInfo(paras.MemberKey, "Google");
            string thisPeriod7DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd");
            string thisPeriod7DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-6)).ToString("yyyy-MM-dd");
            string lastPeriod7DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-7).ToString("yyyy-MM-dd");
            string lastPeriod7DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-13)).ToString("yyyy-MM-dd");
            string lastPeriod14DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-14).ToString("yyyy-MM-dd");
            string lastPeriod14DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-19)).ToString("yyyy-MM-dd");
            string lastPeriod21DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-15).ToString("yyyy-MM-dd");
            string lastPeriod21DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-21)).ToString("yyyy-MM-dd");
            try
            {
                var token = new TokenResponse { RefreshToken = member.FirstOrDefault().refreshtoken };
                var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                    new GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = GetClientConfiguration().Secrets,
                    }), member.FirstOrDefault().userid, token);

                using (var svc = new AnalyticsReportingService(
                    new BaseClientService.Initializer
                    {
                        HttpClientInitializer = credentials,
                        ApplicationName = "My Project"
                    }))
                {
                    var dateRange = new DateRange
                    {
                        StartDate = paras.StartDate,
                        EndDate = paras.EndDate
                    };


                    var dateRange7 = new DateRange
                    {
                        StartDate = "7daysAgo",
                        EndDate = paras.EndDate
                    };

                    var dateRange14 = new DateRange
                    {
                        StartDate = "14daysAgo",
                        EndDate = paras.EndDate
                    };

                    var dateRange28 = new DateRange
                    {
                        StartDate = "28daysAgo",
                        EndDate = paras.EndDate
                    };
                    var thisPeriod7Days = new DateRange
                    {
                        StartDate = thisPeriod7DStartdate,
                        EndDate = thisPeriod7DEnddate
                    };
                    var lastPeriod7Days = new DateRange
                    {
                        StartDate = lastPeriod7DStartdate,
                        EndDate = lastPeriod7DEnddate
                    };
                    var lastPeriod14Days = new DateRange
                    {
                        StartDate = lastPeriod14DStartdate,
                        EndDate = lastPeriod14DEnddate
                    };
                    var lastPeriod21Days = new DateRange
                    {
                        StartDate = lastPeriod21DStartdate,
                        EndDate = lastPeriod21DEnddate
                    };
                    Dictionary<string, Dimension> dictDimensions = SettingGADimension();
                    Dictionary<string, Metric> dictMetrics = SettingGAMetric();
                    List<ReportRequest> requests1 = new List<ReportRequest>();
                    List<DateRange> lstDateRange = new List<DateRange> { dateRange };
                    for (int i = 1; i <= 5; i++)
                    {
                        List<string> _dimension = new List<string>();
                        List<string> _metric = new List<string>();
                        switch (i)
                        {
                            case 1:

                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric1))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension1))
                                    {
                                        _dimension = paras.ReqDimension1.Split(",").ToList();
                                    }

                                    _metric = paras.ReqMetric1.Split(",").ToList();

                                    var reportRequest1 = generateReportRequest(viewID, lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest1);
                                }

                                break;
                            case 2:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric2))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension2))
                                    {
                                        _dimension = paras.ReqDimension2.Split(",").ToList();
                                    }

                                    _metric = paras.ReqMetric2.Split(",").ToList();

                                    var reportRequest2 = generateReportRequest(viewID, lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest2);
                                }
                                break;
                            case 3:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric3))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension3))
                                    {
                                        _dimension = paras.ReqDimension3.Split(",").ToList();
                                    }

                                    _metric = paras.ReqMetric3.Split(",").ToList();

                                    var reportRequest3 = generateReportRequest(viewID, lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest3);
                                }
                                break;
                            case 4:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric4))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension4))
                                    {
                                        _dimension = paras.ReqDimension4.Split(",").ToList();
                                    }
                                    _metric = paras.ReqMetric4.Split(",").ToList();

                                    var reportRequest4 = generateReportRequest(viewID, lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest4);
                                }
                                break;
                            case 5:
                                if (!String.IsNullOrWhiteSpace(paras.ReqMetric5))
                                {
                                    if (!String.IsNullOrWhiteSpace(paras.ReqDimension5))
                                    {
                                        _dimension = paras.ReqDimension5.Split(",").ToList();
                                    }
                                    _metric = paras.ReqMetric5.Split(",").ToList();

                                    var reportRequest5 = generateReportRequest(viewID, lstDateRange, _dimension, _metric, dictDimensions, dictMetrics);
                                    requests1.Add(reportRequest5);
                                }
                                break;
                        }


                    }







                    // Create the GetReportsRequest object.
                    GetReportsRequest getRequest1 = new GetReportsRequest() { ReportRequests = requests1 };


                    List<GoogleAnalyricsData.GA_Reports> dataRows1 = await GetGAReport(getRequest1, svc);

                    List<GoogleAnalyricsData.ResponseGAResult> gaResult = new List<GoogleAnalyricsData.ResponseGAResult>();
                    gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows1 });

                    //回傳
                    returnjson = JsonConvert.SerializeObject(gaResult);

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                //Console.WriteLine(ex.ToString());
                return (JsonConvert.SerializeObject(ex));

            }

            return returnjson;
        }




        /// <summary>
        /// 電商報告
        /// </summary>
        /// <param name="paras"></param>
        /// <returns></returns>


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>")]
        public async Task<string> ECReport([FromForm] Authorize.GAReportParameter paras, [FromHeader] string SaveDB)

        {
            _logger.LogInformation("ECReport");
            _logger.LogInformation("ECReport Parameters:"+paras.ToString());
            string returnjson = string.Empty;
            string eventname = "GoogleService-ECreport";
            string ecsource = "GA";
            string viewID = string.Empty;
            string dbString = _dataInsight.LoadEventfromDB(paras.StartDate,paras.EndDate, eventname, ecsource);
            if (string.IsNullOrEmpty(paras.ViewId))
            {
                viewID = "GoogleAnalytics_View:DefaultID";
            }
            else
            {
                viewID = "GoogleAnalytics_View:"+paras.ViewId;;
            }
            if (string.IsNullOrEmpty(dbString))
            {
                List<Authorize.MemberInfo> member = await GetMemberInfo(paras.MemberKey, "Google");
                string thisPeriod7DEnddate = DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                string thisPeriod7DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-6)).ToString("yyyy-MM-dd");
                string lastPeriod7DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-7).ToString("yyyy-MM-dd");
                string lastPeriod7DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-13)).ToString("yyyy-MM-dd");
                string lastPeriod14DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-14).ToString("yyyy-MM-dd");
                string lastPeriod14DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-19)).ToString("yyyy-MM-dd");
                string lastPeriod21DEnddate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture)).AddDays(-20).ToString("yyyy-MM-dd");
                string lastPeriod21DStartdate = (DateTime.ParseExact(paras.EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture).AddDays(-26)).ToString("yyyy-MM-dd");
                try
                {
                    var token = new TokenResponse { RefreshToken = member.FirstOrDefault().refreshtoken };
                    var credentials = new UserCredential(new GoogleAuthorizationCodeFlow(
                        new GoogleAuthorizationCodeFlow.Initializer
                        {
                            ClientSecrets = GetClientConfiguration().Secrets,
                        }), member.FirstOrDefault().userid, token);

                    using (var svc = new AnalyticsReportingService(
                        new BaseClientService.Initializer
                        {
                            HttpClientInitializer = credentials,
                            ApplicationName = "My Project"
                        }))
                    {
                        var dateRange = new DateRange
                        {
                            StartDate = paras.StartDate,
                            EndDate = paras.EndDate
                        };


                        var dateRange7 = new DateRange
                        {
                            StartDate = "7daysAgo",
                            EndDate = paras.EndDate
                        };

                        var dateRange14 = new DateRange
                        {
                            StartDate = "14daysAgo",
                            EndDate = paras.EndDate
                        };

                        var dateRange28 = new DateRange
                        {
                            StartDate = "28daysAgo",
                            EndDate = paras.EndDate
                        };
                        var thisPeriod7Days = new DateRange
                        {
                            StartDate = thisPeriod7DStartdate,
                            EndDate = thisPeriod7DEnddate
                        };
                        var lastPeriod7Days = new DateRange
                        {
                            StartDate = lastPeriod7DStartdate,
                            EndDate = lastPeriod7DEnddate
                        };
                        var lastPeriod14Days = new DateRange
                        {
                            StartDate = lastPeriod14DStartdate,
                            EndDate = lastPeriod14DEnddate
                        };
                        var lastPeriod21Days = new DateRange
                        {
                            StartDate = lastPeriod21DStartdate,
                            EndDate = lastPeriod21DEnddate
                        };
                        Dictionary<string, Dimension> dictDimensions = SettingGADimension();
                        Dictionary<string, Metric> dictMetrics = SettingGAMetric();
                        //Request group  1 -->Dimension :Date //
                        var reportRequestGroup1_1 = new ReportRequest
                        {
                            DateRanges = new List<DateRange> { dateRange },
                            Dimensions = new List<Dimension> { },
                            Metrics = new List<Metric> {
                            dictMetrics["transactions"],
                            dictMetrics["transactionRevenue"],
                            dictMetrics["transactionsPerSession"],
                            dictMetrics["revenuePerTransaction"],
                            dictMetrics["goal1Completions"],
                            dictMetrics["bounceRate"]

                        },
                            ViewId = _configuration.GetSection(viewID).Value
                        };
                        var reportRequestGroup1_2 = new ReportRequest
                        {
                            DateRanges = new List<DateRange> { dateRange },
                            Dimensions = new List<Dimension> { dictDimensions["sourceMedium"] },
                            Metrics = new List<Metric> {
                            dictMetrics["transactions"],
                            dictMetrics["transactionRevenue"],
                            dictMetrics["transactionsPerSession"],
                            dictMetrics["revenuePerTransaction"],
                            dictMetrics["goal1Completions"],
                            dictMetrics["bounceRate"],
                            dictMetrics["users"],


                        },
                            ViewId = _configuration.GetSection(viewID).Value
                        };
                        var reportRequestGroup1_3 = new ReportRequest
                        {
                            DateRanges = new List<DateRange> { dateRange },
                            Dimensions = new List<Dimension> { dictDimensions["regionIsoCode"] },
                            Metrics = new List<Metric> {
                            dictMetrics["transactions"],
                            dictMetrics["transactionRevenue"],
                            dictMetrics["transactionsPerSession"],
                            dictMetrics["revenuePerTransaction"]

                        },
                            IncludeEmptyRows = false,
                            ViewId = _configuration.GetSection(viewID).Value
                        };
                        var reportRequestGroup1_4 = new ReportRequest
                        {
                            DateRanges = new List<DateRange> { dateRange },
                            Dimensions = new List<Dimension> { dictDimensions["campaign"] },
                            Metrics = new List<Metric> {
                            dictMetrics["adCost"],
                            dictMetrics["adClicks"],


                        },
                            ViewId = _configuration.GetSection(viewID).Value
                        };
                        OrderBy ordering_GP15 = new OrderBy()
                        {
                            SortOrder = "DESCENDING",
                            FieldName = "ga:itemRevenue"
                        };
                        var reportRequestGroup1_5 = new ReportRequest
                        {

                            DateRanges = new List<DateRange> { dateRange },
                            Dimensions = new List<Dimension> { dictDimensions["productName"], dictDimensions["productSku"] },

                            Metrics = new List<Metric> {
                            dictMetrics["itemRevenue"],
                            dictMetrics["productListClicks"],
                            dictMetrics["productAddsToCart"],
                            dictMetrics["productCheckouts"]



                        },

                            FiltersExpression = "ga:productListClicks>0,ga:itemRevenue>0",
                            OrderBys = new List<OrderBy> { ordering_GP15 },
                            PageSize = 10000,
                            ViewId = _configuration.GetSection(viewID).Value
                        };
                        //Request group  2 -->Dimension :Date //
                        var reportRequestGroup2_1 = new ReportRequest
                        {
                            DateRanges = new List<DateRange> { lastPeriod21Days, lastPeriod14Days },
                            Dimensions = new List<Dimension> { dictDimensions["userType"] },
                            Metrics = new List<Metric> {
                            dictMetrics["itemQuantity"],
                            dictMetrics["transactionRevenue"],
                            dictMetrics["itemsPerPurchase"],
                            dictMetrics["avgEventValue"],
                            dictMetrics["revenuePerItem"],



                        },
                            ViewId = _configuration.GetSection(viewID).Value
                        };
                        //Request group  3 -->Dimension :Date //
                        var reportRequestGroup3_1 = new ReportRequest
                        {
                            DateRanges = new List<DateRange> { lastPeriod7Days, thisPeriod7Days },
                            Dimensions = new List<Dimension> { dictDimensions["userType"] },
                            Metrics = new List<Metric> {
                            dictMetrics["itemQuantity"],
                            dictMetrics["transactionRevenue"],
                            dictMetrics["itemsPerPurchase"],
                            dictMetrics["avgEventValue"],
                            dictMetrics["revenuePerItem"],


                        },
                            ViewId = _configuration.GetSection(viewID).Value
                        };


                        List<ReportRequest> requestsGroup11 = new List<ReportRequest>
                    {
                        reportRequestGroup1_1,
                        reportRequestGroup1_2,
                        reportRequestGroup1_3,
                        reportRequestGroup1_4,
                        reportRequestGroup1_5


                    };
                        List<ReportRequest> requestsGroup12 = new List<ReportRequest>
                    {
                        reportRequestGroup2_1,

                    };
                        List<ReportRequest> requestsGroup13 = new List<ReportRequest>
                    {
                        reportRequestGroup3_1,

                    };

                        // Create the GetReportsRequest object.
                        GetReportsRequest getRequest1 = new GetReportsRequest() { ReportRequests = requestsGroup11 };
                        GetReportsRequest getRequest2 = new GetReportsRequest() { ReportRequests = requestsGroup12 };
                        GetReportsRequest getRequest3 = new GetReportsRequest() { ReportRequests = requestsGroup13 };

                        List<GoogleAnalyricsData.GA_Reports> dataRows1 = await GetGAReport(getRequest1, svc, false);
                        List<GoogleAnalyricsData.GA_Reports> dataRows2 = await GetGAReport(getRequest2, svc);
                        List<GoogleAnalyricsData.GA_Reports> dataRows3 = await GetGAReport(getRequest3, svc);
                        /*
                        List<GoogleAnalyricsData.GA_Reports> dataRows4 = salesRevenues(paras.StartDate, paras.EndDate,"brand");
                        List<GoogleAnalyricsData.GA_Reports> dataRows5 = salesRevenues(paras.StartDate, paras.EndDate, "type");
                        List<GoogleAnalyricsData.GA_Reports> dataRows6 = salesRevenues(paras.StartDate, paras.EndDate, "colorcategory");
                        List<GoogleAnalyricsData.GA_Reports> dataRows7 = salesRevenues(paras.StartDate, paras.EndDate, "channel");
                        */
                        List<GoogleAnalyricsData.GA_Reports> dataRows4 = AnalysisRevenue(paras.StartDate, paras.EndDate);
                        
                        List<GoogleAnalyricsData.GA_Reports> dataRows5 = _dataInsight.GAAnalysisRevenue(paras.StartDate, paras.EndDate, dataRows1, 4, 1);
                        List<GoogleAnalyricsData.GA_Reports> dataRows6 = AnalysisMember(paras.StartDate, paras.EndDate);
                        
                        List<GoogleAnalyricsData.ResponseGAResult> gaResult = new List<GoogleAnalyricsData.ResponseGAResult>();
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows1 });
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows2 });
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows3 });
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows4 });
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows5 });
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows6 });
                        /*
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows5});
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows6});
                        gaResult.Add(new GoogleAnalyricsData.ResponseGAResult { GA_Reports = dataRows7});
                        */
                        //回傳


                         returnjson = JsonConvert.SerializeObject(gaResult);
                        bool isSave = false;
                        if (!String.IsNullOrEmpty(SaveDB))
                        {
                            isSave = true;
                        }
                        _logger.LogInformation("SavetoDatabase:"+paras.StartDate+"to"+paras.EndDate+"-"+SaveDB);
                        if (!String.IsNullOrEmpty(returnjson) && isSave)
                        {
                            _logger.LogDebug("into save function");
                            _dataInsight.SaveEventtoDB(returnjson, eventname, ecsource, paras.StartDate, paras.EndDate);
                        }

                    }
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.ToString());
                    _logger.LogError(ex.ToString());
                    return (JsonConvert.SerializeObject(ex));

                }
               
            }
            else
            {
                returnjson = dbString;
            }
            
            return returnjson;
        }
        public static GoogleClientSecrets GetClientConfiguration()
        {
            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                return GoogleClientSecrets.FromStream(stream);
            }
        }
        /// <summary>
        /// Retrieve Membetr informations
        /// </summary>
        /// <param name="memberKey"></param>
        /// <param name="tokentype"></param>
        /// <returns></returns>
        private async Task<List<Authorize.MemberInfo>> GetMemberInfo(string memberKey, string tokentype)
        {
            _logger.LogInformation("GetMmberInfo");
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string checkSql = @"select AUTHID,MEMBERKEY,REFRESHTOKEN from 
                [SYS_THIRDPARTYAUTHORIZED] where TOKENTYPE=@tokenType and MEMBERKEY=@memberkey";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    await connection.OpenAsync();

                    lstMemberInfo = connection.Query<Authorize.MemberInfo>(checkSql, new { memberkey = memberKey, tokenType = tokentype }).ToList();
                    return lstMemberInfo;
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return lstMemberInfo;
            }


        }
        /// <summary>
        /// Batch get GA Report
        /// </summary>
        /// <param name="request"></param>
        /// <param name="svc"></param>
        /// <returns>report rows</returns>
        private async Task<List<GoogleAnalyricsData.GA_Reports>> GetGAReport(GetReportsRequest request, AnalyticsReportingService svc, bool limit = true)
        {
            _logger.LogInformation("GetGAReport");
            List<GoogleAnalyricsData.GA_Reports> lstReports = new List<GoogleAnalyricsData.GA_Reports>();

            try
            {
                GetReportsResponse response1 = await svc.Reports.BatchGet(request).ExecuteAsync().ConfigureAwait(false);
                //printResults(response1.Reports as List<Report>);
                for (int i = 0; i < response1.Reports.Count; i++)
                {
                    List<string> MetricHeaderEntries = new List<string>();
                    List<GoogleAnalyricsData.GA_Results> lstresult = new List<GoogleAnalyricsData.GA_Results>();
                    
                    int rowcount = response1.Reports[i].Data.RowCount.HasValue ? response1.Reports[i].Data.RowCount.Value : 0;
                    int reportDataRowcount = 0;
                    if (limit & rowcount>0)
                    {
                        
                        reportDataRowcount = rowcount >= 1000 ? 1000 : rowcount;
                        reportDataRowcount = (int)(response1.Reports[i].Data.RowCount == rowcount ? reportDataRowcount:response1.Reports[i].Data.RowCount);
                    }
                    else
                    {
                        reportDataRowcount = rowcount;
                    }
                    List<GoogleAnalyricsData.Metrics> zerolstMetrics = new List<GoogleAnalyricsData.Metrics>();

                    for (int j = 0; j < response1.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries.Count; j++)
                    {
                        MetricHeaderEntries.Add(response1.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries[j].Name);
                        GoogleAnalyricsData.Metrics _metric = new GoogleAnalyricsData.Metrics();
                        _metric.value = "0";
                        zerolstMetrics.Add(_metric);
                    }
                        for (int j = 0; j < reportDataRowcount; j++)
                        {

                            List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();
                        
                        if (response1.Reports[i].Data.RowCount.HasValue && response1.Reports[i].Data.RowCount > 0)
                        {
                            for (int k = 0; k < response1.Reports[i].Data.Rows[j].Metrics.Count; k++)
                            {
                                List<string> lstMetricName = new List<string>();
                                List<GoogleAnalyricsData.Metrics> lstMetrics = new List<GoogleAnalyricsData.Metrics>();
                                for (int l = 0; l < response1.Reports[i].Data.Rows[j].Metrics[k].Values.Count; l++)
                                {

                                    GoogleAnalyricsData.Metrics _metric = new GoogleAnalyricsData.Metrics();

                                    _metric.value = response1.Reports[i].Data.Rows[j].Metrics[k].Values[l];
                                    if (_metric.value.Contains("."))
                                    {

                                        _metric.value = formatString(_metric.value);
                                    }


                                    lstMetrics.Add(_metric);
                                    // lstMetricName.Add(response1.Reports[i].ColumnHeader.MetricHeader.MetricHeaderEntries[l].Name);
                                }
                                lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics });
                            }
                        }
                        else
                        {
                            
                            
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = zerolstMetrics });
                        }
                        


                            int dimCount = 0;
                            try
                            {


                                if (response1.Reports[i].Data.Rows[j].Dimensions != null)
                                {
                                    dimCount = response1.Reports[i].Data.Rows[j].Dimensions.Count;
                                }

                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex.ToString());
                            }


                            List<string> lstDimension = new List<string>();
                            if (dimCount != 0)
                            {
                                for (int k = 0; k < response1.Reports[i].Data.Rows[j].Dimensions.Count; k++)
                                {
                                    if (response1.Reports[i].ColumnHeader.Dimensions[0].ToString() == "ga:regionIsoCode")
                                    {

                                        var _val = _twISO.IsoCodeConverter(response1.Reports[i].Data.Rows[j].Dimensions[0]);
                                        if (_val.Count == 0)
                                        {
                                            lstDimension.Add(response1.Reports[i].Data.Rows[j].Dimensions[k]);
                                        }
                                        else
                                        {
                                            lstDimension.Add(_val[0].ShortName);
                                        }

                                    }
                                    else
                                    {
                                        lstDimension.Add(response1.Reports[i].Data.Rows[j].Dimensions[k]);

                                    }


                                }

                            }
                            else
                            {
                                lstDimension.Add("Date");
                            }

                            lstresult.Add(new GoogleAnalyricsData.GA_Results { metrics = lstGAMetrics, dimension = lstDimension });


                        }

                        lstReports.Add(new GoogleAnalyricsData.GA_Reports { ListResult = lstresult, MetricHead = MetricHeaderEntries });
                    
                   
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                Console.WriteLine(ex);
            }
            return lstReports;
        }
        private string formatString(string Str)
        {
            string formatted = String.Empty;
            if (Str.Contains("."))
            {
                string[] tmpStr = Str.Split(".");
                string xvalue1 = tmpStr[0];
                string xvalue2 = tmpStr[1];


                if (xvalue2.Length > 2)
                {
                    formatted = xvalue1 + "." + xvalue2.Substring(0, 2);
                }
                else
                {
                    formatted = xvalue1 + "." + xvalue2;
                }

            }
            else
            {
                formatted = Str;
            }
            return formatted;
        }
        private ReportRequest generateReportRequest(string viewID, List<DateRange> dateRange, List<string> dimension, List<string> metric, Dictionary<string, Dimension> dictDimensions, Dictionary<string, Metric> dictMetric)
        {
            IList<Dimension> reqDimension = new List<Dimension>();
            IList<Metric> reqMetric = new List<Metric>();
            for (int j = 0; j < dimension.Count; j++)
            {
                reqDimension.Add(dictDimensions[dimension[j]]);
            }
            for (int j = 0; j < metric.Count; j++)
            {
                reqMetric.Add(dictMetric[metric[j]]);
            }
            var reportRequest = new ReportRequest
            {
                DateRanges = dateRange,
                Dimensions = reqDimension,
                Metrics = reqMetric,
                PageSize=10000,
                ViewId = _configuration.GetSection(viewID).Value
            };
            return reportRequest;
        }
        public static void printResults(List<Google.Apis.AnalyticsReporting.v4.Data.Report> reports)
        {
            foreach (Google.Apis.AnalyticsReporting.v4.Data.Report report in reports)
            {
                ColumnHeader header = report.ColumnHeader;
                List<string> dimensionHeaders = (List<string>)header.Dimensions;

                List<MetricHeaderEntry> metricHeaders = (List<MetricHeaderEntry>)header.MetricHeader.MetricHeaderEntries;
                List<ReportRow> rows = (List<ReportRow>)report.Data.Rows;

                foreach (ReportRow row in rows)
                {
                    List<string> dimensions = (List<string>)row.Dimensions;
                    List<DateRangeValues> metrics = (List<DateRangeValues>)row.Metrics;

                    for (int i = 0; i < dimensionHeaders.Count() && i < dimensions.Count(); i++)
                    {
                        Console.WriteLine(dimensionHeaders[i] + ": " + dimensions[i]);
                    }

                    for (int j = 0; j < metrics.Count(); j++)
                    {
                        Console.WriteLine("Date Range (" + j + "): ");
                        DateRangeValues values = metrics[j];
                        for (int k = 0; k < values.Values.Count() && k < metricHeaders.Count(); k++)
                        {
                            Console.WriteLine(metricHeaders[k].Name + ": " + values.Values[k]);
                        }
                    }
                }
            }
        }
        private List<GoogleAnalyricsData.GA_Results> GAResult(List<ReportRow> lstreportrow)
        {
            List<GoogleAnalyricsData.GA_Results> gaDatarows = new List<GoogleAnalyricsData.GA_Results>();
            try
            {

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                Console.WriteLine(ex);
            }
            return gaDatarows;
        }

        private List<GoogleAnalyricsData.GA_Reports> AnalysisRevenue(string StartDate, string EndDate)
        {
            List<GoogleAnalyricsData.GA_Reports> lstReports = new List<GoogleAnalyricsData.GA_Reports>();

            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string selectCommand = @"SELECT COUNT(DISTINCT(startdate)) from datainsights 
            WHERE startdate>=@startdate AND enddate<=@enddate";
            StartDate = DateTime.Parse(StartDate).ToString("yyyy/MM/dd");
            EndDate = DateTime.Parse(EndDate).ToString("yyyy/MM/dd");
            var totalDays = (DateTime.Parse(EndDate) - DateTime.Parse(StartDate)).TotalDays + 1;
            string[] spAry = new string[] { "sp_query_revenue", "sp_query_revenue_datainsight" };
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {

                    var countDays = connection.ExecuteScalar<int>(selectCommand,new {startdate= StartDate,enddate= EndDate });
                    string spName = string.Empty;
                    if (countDays == totalDays)
                    {
                        spName = spAry[1];
                    }
                    else
                    {
                        spName = spAry[0];
                    }
                    var results = connection.QueryMultiple(spName
                                , new { startdate = StartDate, enddate = EndDate }
                                , commandType: CommandType.StoredProcedure);
                    if (!results.IsConsumed)
                    {
                        List<HbtData.RevenuefromHBT> revenuefromHBTs = new List<HbtData.RevenuefromHBT>();
                        while (!results.IsConsumed)
                        {
                            revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = results.Read<HbtData.SalesRevenue>().ToList() });
                        }
                        
                        /*
                        var type = results.Read<HbtData.SalesRevenue>().ToList();
                        var brand = results.Read<HbtData.SalesRevenue>().ToList();
                        var colortype = results.Read<HbtData.SalesRevenue>().ToList();
                        var channel = results.Read<HbtData.SalesRevenue>().ToList();
                        var shop = results.Read<HbtData.SalesRevenue>().ToList();
                        var product = results.Read<HbtData.SalesRevenue>().ToList();
                        var season = results.Read<HbtData.SalesRevenue>().ToList();
                        var seasontype = results.Read<HbtData.SalesRevenue>().ToList();
                        var gender = results.Read<HbtData.SalesRevenue>().ToList();
                        var genderproductcategory = results.Read<HbtData.SalesRevenue>().ToList();
                        var campaigncounts = results.Read<HbtData.SalesRevenue>().ToList();
                        var gendertypeproductcategory = results.Read<HbtData.SalesRevenue>().ToList();
                        var swimcategory = results.Read<HbtData.SalesRevenue>().ToList();

                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = type });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = brand });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = colortype });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = channel });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = shop });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = product });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = season });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = seasontype });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = gender });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = genderproductcategory });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = campaigncounts });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = gendertypeproductcategory });
                        revenuefromHBTs.Add(new HbtData.RevenuefromHBT { salesRevenues = swimcategory });
                        */
                        for (int i = 0; i < revenuefromHBTs.Count; i++)
                        {
                            List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();

                            List<GoogleAnalyricsData.GA_Results> lstresult = new List<GoogleAnalyricsData.GA_Results>();
                            List<string> lstDimension = new List<string>();
                            List<GoogleAnalyricsData.Metrics> lstMetrics1 = new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.Metrics> lstMetrics2 = new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.Metrics> lstMetrics3 = new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.Metrics> lstMetrics4 = new List<GoogleAnalyricsData.Metrics>();
                            List<GoogleAnalyricsData.Metrics> lstMetrics5 = new List<GoogleAnalyricsData.Metrics>();

                            for (int j = 0; j < revenuefromHBTs[i].salesRevenues.Count; j++)
                            {

                                GoogleAnalyricsData.Metrics _metric1 = new GoogleAnalyricsData.Metrics();
                                GoogleAnalyricsData.Metrics _metric2 = new GoogleAnalyricsData.Metrics();
                                GoogleAnalyricsData.Metrics _metric3 = new GoogleAnalyricsData.Metrics();
                                GoogleAnalyricsData.Metrics _metric4 = new GoogleAnalyricsData.Metrics();
                                GoogleAnalyricsData.Metrics _metric5 = new GoogleAnalyricsData.Metrics();
                                lstDimension.Add(revenuefromHBTs[i].salesRevenues[j].category);
                                _metric1.value = revenuefromHBTs[i].salesRevenues[j].revenue;
                                _metric2.value = formatString(revenuefromHBTs[i].salesRevenues[j].salesratio.ToString());
                                lstMetrics1.Add(new GoogleAnalyricsData.Metrics { value = _metric1.value });
                                lstMetrics2.Add(new GoogleAnalyricsData.Metrics { value = _metric2.value });
                                if (!String.IsNullOrEmpty(revenuefromHBTs[i].salesRevenues[j].id))
                                {
                                    
                                    _metric3.value = revenuefromHBTs[i].salesRevenues[j].id;
                                    

                                }
                                else
                                {
                                    _metric3.value = "0";
                                }
                                
                                if (!String.IsNullOrEmpty(revenuefromHBTs[i].salesRevenues[j].countnumber1))
                                {
                                    
                                    _metric4.value = revenuefromHBTs[i].salesRevenues[j].countnumber1;
                                    

                                }
                                else
                                {
                                    _metric4.value = "0";
                                }
                                
                                if (!String.IsNullOrEmpty(revenuefromHBTs[i].salesRevenues[j].extend1))
                                {

                                    _metric5.value = revenuefromHBTs[i].salesRevenues[j].extend1;


                                }
                                else
                                {
                                    _metric5.value = "";
                                }
                                lstMetrics3.Add(new GoogleAnalyricsData.Metrics { value = _metric3.value });
                                lstMetrics4.Add(new GoogleAnalyricsData.Metrics { value = _metric4.value });
                                lstMetrics5.Add(new GoogleAnalyricsData.Metrics { value = _metric5.value });
                            }
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics1 });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics2 });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics3 });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics4 });
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics5 });
                          
                            lstresult.Add(new GoogleAnalyricsData.GA_Results { metrics = lstGAMetrics, dimension = lstDimension });
                            lstReports.Add(new GoogleAnalyricsData.GA_Reports { ListResult = lstresult });



                        }


                    }


                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return lstReports;
            }



            
            
            return lstReports;
        }

       
        private List<GoogleAnalyricsData.GA_Reports> AnalysisMember(string StartDate, string EndDate)
        {
            List<GoogleAnalyricsData.GA_Reports> lstReports = new List<GoogleAnalyricsData.GA_Reports>();

            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string selectCommand = string.Empty;
            string startDateCurrent = string.Empty;
            string endDateCurrent = string.Empty;
            if (StartDate == EndDate)
            {
                startDateCurrent = DateTime.Parse(StartDate).AddDays(-1).ToString("yyyy-MM-dd");
            }
            else
            {
                startDateCurrent = DateTime.Parse(StartDate).ToString("yyyy-MM-dd");
            }
             
            endDateCurrent = DateTime.Parse(EndDate).ToString("yyyy-MM-dd");
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {

                    var results = connection.QueryMultiple("sp_query_member"
                                , new { eventstartdate = startDateCurrent, eventenddate = endDateCurrent }
                                , commandType: CommandType.StoredProcedure);
                   
                    if (!results.IsConsumed)
                    {
                        List<ETL.DataStock> rsultList = new List<ETL.DataStock>();
                        List<HbtData.NumberofMember> membership = new List<HbtData.NumberofMember>();
                        while (!results.IsConsumed)
                        {
                            rsultList = results.Read<ETL.DataStock>().ToList();
                           

                        }
                        List<GoogleAnalyricsData.GA_Results> lstresult = new List<GoogleAnalyricsData.GA_Results>();
                        for (int i = 0; i < rsultList.Count; i++)
                        {

                            var tmpObj = JsonConvert.DeserializeObject<HbtData.NumberofMember>(rsultList[i].eventvalue);
                            membership.Add(new HbtData.NumberofMember { regular = tmpObj.regular, vip = tmpObj.vip, svip = tmpObj.svip, eventdate = rsultList[i].startdate });
                            if (i + 1 == rsultList.Count & rsultList.Count>1)
                            {
                                int curregular = 0, curvip = 0, cursvip = 0, priregular = 0, privip = 0, prisvip = 0;
                                int.TryParse(membership[i - 1].regular, out priregular);
                                int.TryParse(membership[i - 1].vip, out privip);
                                int.TryParse(membership[i - 1].svip, out prisvip);
                                int.TryParse(membership[i].regular, out curregular);
                                int.TryParse(membership[i].vip, out curvip);
                                int.TryParse(membership[i].svip, out cursvip);
                                int compareregular = curregular - priregular;
                                int comparevip = curvip - privip;
                                int comparesvip = cursvip - prisvip;
                                membership.Add(new HbtData.NumberofMember { regular = compareregular.ToString(), vip = comparevip.ToString(), svip = comparesvip.ToString() });
                            }else if(i + 1 == rsultList.Count & rsultList.Count == 1)
                            {
                                int curregular = 0;
                                int curvip = 0;
                                int cursvip = 0;
                                int priregular = 0;
                                int privip = 0;
                                int prisvip = 0;
                                int compareregular = curregular - priregular;
                                int comparevip = curvip - privip;
                                int comparesvip = cursvip - prisvip;
                                membership.Add(new HbtData.NumberofMember { regular = compareregular.ToString(), vip = comparevip.ToString(), svip = comparesvip.ToString() });
                            }
                        }
                        

                        for (int i = 0; i < membership.Count; i++)
                        {
                            /** 改寫給值方式
                            Type type = typeof(HbtData.NumberofMember);
                            int NumberOfRecords = type.GetProperties().Length;
                            for(int j=0;j< type.GetProperties().Length; j++)
                            {
                                ;
                                
                            }**/
                            List<GoogleAnalyricsData.Metrics> lstMetrics1 = new List<GoogleAnalyricsData.Metrics>();
                            GoogleAnalyricsData.Metrics _metric1 = new GoogleAnalyricsData.Metrics
                            {
                                value = membership[i].regular
                            };
                            GoogleAnalyricsData.Metrics _metric2 = new GoogleAnalyricsData.Metrics
                            {
                                value = membership[i].vip
                            };
                            GoogleAnalyricsData.Metrics _metric3 = new GoogleAnalyricsData.Metrics
                            {
                                value = membership[i].svip
                            };
                            GoogleAnalyricsData.Metrics _metric4 = new GoogleAnalyricsData.Metrics
                            {
                                value = membership[i].eventdate
                            };
                            lstMetrics1.Add(new GoogleAnalyricsData.Metrics { value = _metric1.value });
                            lstMetrics1.Add(new GoogleAnalyricsData.Metrics { value = _metric2.value });
                            lstMetrics1.Add(new GoogleAnalyricsData.Metrics { value = _metric3.value });
                            lstMetrics1.Add(new GoogleAnalyricsData.Metrics { value = _metric4.value });
                            List<GoogleAnalyricsData.GAMetric> lstGAMetrics = new List<GoogleAnalyricsData.GAMetric>();
                            lstGAMetrics.Add(new GoogleAnalyricsData.GAMetric { metric = lstMetrics1 });
                            lstresult.Add(new GoogleAnalyricsData.GA_Results { metrics = lstGAMetrics });

                        }

                        lstReports.Add(new GoogleAnalyricsData.GA_Reports { ListResult = lstresult });



                    }


                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return lstReports;
            }





            return lstReports;
        }


        /// <summary>
        ///  Define Dimensions
        /// </summary>
        /// <returns> Dimension</returns>
        private Dictionary<string, Dimension> SettingGADimension()
        {
            Dictionary<string, Dimension> dictDimension = new Dictionary<string, Dimension>();
            dictDimension.Add("date", new Dimension { Name = "ga:date" });
            dictDimension.Add("source", new Dimension { Name = "ga:source" });
            dictDimension.Add("country", new Dimension { Name = "ga:country" });
            dictDimension.Add("adwordsCampaignID", new Dimension { Name = "ga:adwordsCampaignID" });
            dictDimension.Add("campaign", new Dimension { Name = "ga:campaign" });
            dictDimension.Add("adGroup", new Dimension { Name = "ga:adGroup" });
            dictDimension.Add("keyword", new Dimension { Name = "ga:keyword" });
            dictDimension.Add("exitPagePath", new Dimension { Name = "ga:exitPagePath" });
            dictDimension.Add("sourceMedium", new Dimension { Name = "ga:sourceMedium" });
            dictDimension.Add("medium", new Dimension { Name = "ga:medium" });
            dictDimension.Add("deviceCategory", new Dimension { Name = "ga:deviceCategory" });
            dictDimension.Add("fullReferrer", new Dimension { Name = "ga:fullReferrer" });
            dictDimension.Add("userGender", new Dimension { Name = "ga:userGender" });
            dictDimension.Add("userAgeBracket", new Dimension { Name = "ga:userAgeBracket" });
            dictDimension.Add("dayOfWeekName", new Dimension { Name = "ga:dayOfWeekName" });
            dictDimension.Add("pagePath", new Dimension { Name = "ga:pagePath" });
            dictDimension.Add("pagePath1", new Dimension { Name = "ga:pagePathLevel1" });
            dictDimension.Add("pageTitle", new Dimension { Name = "ga:pageTitle" });
            dictDimension.Add("region", new Dimension { Name = "ga:region" });
            dictDimension.Add("regionId", new Dimension { Name = "ga:regionId" });
            dictDimension.Add("regionIsoCode", new Dimension { Name = "ga:regionIsoCode" });
            dictDimension.Add("userType", new Dimension { Name = "ga:userType" });
            dictDimension.Add("productSku", new Dimension { Name = "ga:productSku" });
            dictDimension.Add("productName", new Dimension { Name = "ga:productName" });
            
       

            return dictDimension;
        }
        /// <summary>
        /// Define Metrics 
        /// </summary>
        /// <returns> Metrics </returns>
        private Dictionary<string, Metric> SettingGAMetric()
        {
            Dictionary<string, Metric> dictMetric = new Dictionary<string, Metric>();
            dictMetric.Add("pageviews", new Metric { Expression = "ga:pageviews", Alias = "瀏覽量" });
            dictMetric.Add("uniquePageviews", new Metric { Expression = "ga:uniquePageviews", Alias = "不重複瀏覽量" });
            dictMetric.Add("users", new Metric { Expression = "ga:users", Alias = "訪客數" });
            dictMetric.Add("newUsers", new Metric { Expression = "ga:newUsers", Alias = "新訪客數" });
            dictMetric.Add("bounces", new Metric { Expression = "ga:bounces", Alias = "跳出數" });
            dictMetric.Add("bounceRate", new Metric { Expression = "ga:bounceRate", Alias = "跳出率" });
            dictMetric.Add("sessionDuration", new Metric { Expression = "ga:sessionDuration", Alias = "工作階段時間長度(秒)" });
            dictMetric.Add("avgSessionDuration", new Metric { Expression = "ga:avgSessionDuration", Alias = "工作階段平均時長" });
            dictMetric.Add("exitRate", new Metric { Expression = "ga:exitRate", Alias = "離開率" });
            dictMetric.Add("exits", new Metric { Expression = "ga:exits", Alias = "離開數" });
            dictMetric.Add("hits", new Metric { Expression = "ga:hits", Alias = "匹配" });
            dictMetric.Add("timeOnPage", new Metric { Expression = "ga:timeOnPage", Alias = "網頁停留時間(秒)" });
            dictMetric.Add("avgTimeOnPage", new Metric { Expression = "ga:avgTimeOnPage", Alias = "平均網頁停留時間(秒)" });
            dictMetric.Add("sessions", new Metric { Expression = "ga:sessions", Alias = "工作階段" });
            dictMetric.Add("avgPageLoadTime", new Metric { Expression = "ga:avgPageLoadTime", Alias = "頁面平均載入時間(秒)" });
            dictMetric.Add("avgServerResponseTime", new Metric { Expression = "ga:avgServerResponseTime", Alias = "avgServerResponseTime" });
            dictMetric.Add("pageviewsPerSession", new Metric { Expression = "ga:pageviewsPerSession", Alias = "工作階段平均頁面瀏覽數" });
            dictMetric.Add("entranceRate", new Metric { Expression = "ga:entranceRate", Alias = "entranceRate" });
            dictMetric.Add("_1dayUsers", new Metric { Expression = "ga:1dayUsers", Alias = "1dayUsers" });
            dictMetric.Add("_7dayUsers", new Metric { Expression = "ga:7dayUsers", Alias = "7dayUsers" });
            dictMetric.Add("_14dayUsers", new Metric { Expression = "ga:14dayUsers", Alias = "14dayUsers" });
            dictMetric.Add("_28dayUsers", new Metric { Expression = "ga:28dayUsers", Alias = "28dayUsers" });
            dictMetric.Add("_30dayUsers", new Metric { Expression = "ga:30dayUsers", Alias = "30dayUsers" });
            dictMetric.Add("impressions", new Metric { Expression = "ga:impressions", Alias = "曝光" });
            dictMetric.Add("adClicks", new Metric { Expression = "ga:adClicks", Alias = "廣告點擊" });
            dictMetric.Add("adCost", new Metric { Expression = "ga:adCost", Alias = "廣告成本" });
            dictMetric.Add("CPM", new Metric { Expression = "ga:CPM", Alias = "CPM" });
            dictMetric.Add("CPC", new Metric { Expression = "ga:CPC", Alias = "CPC" });
            dictMetric.Add("CTR", new Metric { Expression = "ga:CTR", Alias = "CTR" });
            dictMetric.Add("costPerGoalConversion", new Metric { Expression = "ga:costPerGoalConversion", Alias = "costPerGoalConversion" });
            dictMetric.Add("costPerTransaction", new Metric { Expression = "ga:costPerTransaction", Alias = "costPerTransaction" });
            dictMetric.Add("RPC", new Metric { Expression = "ga:RPC", Alias = "RPC" });
            dictMetric.Add("ROAS", new Metric { Expression = "ga:ROAS", Alias = "ROAS" });
            dictMetric.Add("transactions", new Metric { Expression = "ga:transactions", Alias = "交易次數" });
            dictMetric.Add("totalValue", new Metric { Expression = "ga:totalValue", Alias = "總價值" });
            dictMetric.Add("transactionRevenue", new Metric { Expression = "ga:transactionRevenue", Alias = "收益" });
            dictMetric.Add("transactionsPerSession", new Metric { Expression = "ga:transactionsPerSession", Alias = "電商轉換率" });
            dictMetric.Add("revenuePerTransaction", new Metric { Expression = "ga:revenuePerTransaction", Alias = "平均訂單價值" });
            dictMetric.Add("goal1Completions", new Metric { Expression = "ga:goal1Completions", Alias = "Goal1" });
            dictMetric.Add("goal2Completions", new Metric { Expression = "ga:goal2Completions", Alias = "Goal2" });
            dictMetric.Add("goal3Completions", new Metric { Expression = "ga:goal3Completions", Alias = "Goal3" });
            dictMetric.Add("goal4Completions", new Metric { Expression = "ga:goal4Completions", Alias = "Goal4" });
            dictMetric.Add("goal5Completions", new Metric { Expression = "ga:goal5Completions", Alias = "Goal5" });
            dictMetric.Add("goal6Completions", new Metric { Expression = "ga:goal6Completions", Alias = "Goal6" });
            dictMetric.Add("goal7Completions", new Metric { Expression = "ga:goal7Completions", Alias = "Goal7" });
            dictMetric.Add("goal8Completions", new Metric { Expression = "ga:goal8Completions", Alias = "Goal8" });
            dictMetric.Add("goal9Completions", new Metric { Expression = "ga:goal9Completions", Alias = "Goal9" });
            dictMetric.Add("goal10Completions", new Metric { Expression = "ga:goal10Completions", Alias = "Goal10" });
            dictMetric.Add("itemQuantity", new Metric { Expression = "ga:itemQuantity", Alias = "購買數量" });
            dictMetric.Add("itemsPerPurchase", new Metric { Expression = "ga:itemsPerPurchase", Alias = "平均購買數量" });
            dictMetric.Add("avgEventValue", new Metric { Expression = "ga:avgEventValue", Alias = "平均訂單價值" });
            dictMetric.Add("revenuePerItem", new Metric { Expression = "ga:revenuePerItem", Alias = "平均結賬單價" });
            dictMetric.Add("itemRevenue", new Metric { Expression = "ga:itemRevenue", Alias = "產品收益" });
            dictMetric.Add("productListClicks", new Metric { Expression = "ga:productListClicks", Alias = "產品點擊次數" });
            dictMetric.Add("productCheckouts", new Metric { Expression = "ga:productCheckouts", Alias = "產品結賬次數" });
            dictMetric.Add("productAddsToCart", new Metric { Expression = "ga:productAddsToCart", Alias = "產品加入購物車次數" });
     

            return dictMetric;
        }

    }
}