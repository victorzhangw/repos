﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using RestSharp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using EnsembleCRM.Helpers;
using EnsembleCRM.Models;
using EnsembleCRM.Interfaces;
using System.IO;
using System.Data.SqlClient;
using Dapper;
using Z.Dapper.Plus;
using System.Diagnostics;

namespace EnsembleCRM.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AwsController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<AwsController> _logger { get; set; }

        public AwsController(IConfiguration config, ILogger<AwsController> logger)
        {

            _configuration = config;

            _logger = logger;

        }
        public async Task<IActionResult> GetCFTags()
        {
            string rtnStr = "";
            var dt = DateTime.Now;



            string retrieveDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            var dateAry = retrieveDate.Split("-");
            /* for test*/
            string retrieveYear = dateAry[0];
            string retrieveMonth = dateAry[1];
            string retrieveDay = dateAry[2];
            string bucketName = "auro-mapping";
            string keyName = $"{retrieveYear}-{retrieveMonth}-{retrieveDay}/CF-210800095299/ClickForceTag/{retrieveYear}{retrieveMonth}{retrieveDay}cdp_bt.csv";
            List<CFTags> cFTags = new List<CFTags>();

            RegionEndpoint bucketRegion = RegionEndpoint.APNortheast1;
            IAmazonS3 s3Client;
            s3Client = new AmazonS3Client("AKIAYFRMQKD3D3OI2LUS", "AynWTTbnco4ySFxM/U0hcVLJePgsjbuHBBPSqThv", bucketRegion);

            try
            {
                GetObjectRequest ObjectRequest = new GetObjectRequest
                {
                    BucketName = bucketName,

                    Key = keyName,

                };

                using (GetObjectResponse response = await s3Client.GetObjectAsync(ObjectRequest))
                using (Stream responseStream = response.ResponseStream)
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    string title = response.Metadata["x-amz-meta-title"]; // Assume you have "title" as medata added to the object.
                    string contentType = response.Headers["Content-Type"];

                    // Console.WriteLine("Object metadata, Title: {0}", title);
                    //  Console.WriteLine("Content type: {0}", contentType);
                    reader.ReadLine();
                    // responseBody = reader.ReadToEnd(); // Now you process the response body.
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        var _lineAry = line.Split(",");
                        cFTags.Add(new CFTags { Cft_p = _lineAry[0], Cft_uid = _lineAry[1], Label = _lineAry[2], CreateDatetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") });
                    }
                    if (cFTags.Count > 0)
                    {
                        var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
                        DapperPlusManager.Entity<CFTags>().Table("CF_LABELS");
                        try
                        {
                            using (var connection = new SqlConnection(cnStr.Value))
                            {
                                connection.Open();

                                using (var tx = connection.BeginTransaction(System.Data.IsolationLevel.Serializable))
                                {
                                    string truncateSql = "TRUNCATE TABLE CF_LABELS";
                                    string insertSql = @"INSERT INTO [dbo].[CF_LABELS] ([cft_p], [label], [cft_uid], [CreateDatetime]) 
                                                        VALUES (@Cft_p,  @Label,@Cft_uid, @CreateDatetime);";

                                    try
                                    {

                                        Stopwatch sw = new Stopwatch();
                                        sw.Start();
                                        connection.Execute(truncateSql, transaction: tx);
                                        connection.Execute(insertSql, cFTags, transaction: tx);
                                        tx.Commit();

                                        sw.Stop();
                                        rtnStr = $"ClickForce 標籤匯入 {cFTags.Count} 筆,共耗時{sw.ElapsedMilliseconds} ms";
                                        _logger.LogInformation(rtnStr);
                                    }
                                    catch (Exception e)
                                    {
                                        _logger.LogError(e.Message);
                                        return BadRequest(e.Message);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e.Message);
                            return BadRequest(e.Message);
                        }

                    }
                }

                return Ok(rtnStr);
            }
            catch (AmazonS3Exception e)
            {

                _logger.LogError(e.Message);
                return BadRequest(e.Message);

            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }



        }
    }




}







