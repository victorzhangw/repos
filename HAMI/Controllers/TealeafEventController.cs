﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EnsembleCRM.Helpers;
using Microsoft.Extensions.Configuration;
using Dapper;
using EnsembleCRM.Models;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Net.Mail;
using System.IO;
using System.Diagnostics;

using Newtonsoft.Json;


namespace EnsembleCRM.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TealeafEventController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly MailHelpers _mailhelper;
        private readonly CsvHelpers _csvhelper;
        private readonly TealeafEventHelpers _tealeafEventHelpers;


        private ILogger<TealeafEventController> _logger { get; set; }
        public TealeafEventController(IConfiguration config, TealeafEventHelpers tealeafEventHelpers, MailHelpers mailHelpers, CsvHelpers csvHelpers, ILogger<TealeafEventController> logger)
        {
            _mailhelper = mailHelpers;
            _configuration = config;
            _csvhelper = csvHelpers;
            _tealeafEventHelpers = tealeafEventHelpers;
            _logger = logger;

        }
        [HttpPost]

        public IActionResult ImportEvents([FromForm] Authorize.TealeafRetrieveParameter paras)
        {
            if (string.IsNullOrEmpty(paras.retrieveDate))
            {
                paras.retrieveDate = DateTime.Now.ToString("yyyy-MM-dd");
            }
            _logger.LogInformation("ImportTealeafEvents {0}", "ImportEvents");
            string reportType = string.IsNullOrEmpty(paras.ReportType) ? "Tealeaf" : paras.ReportType;
            string mailHeaderKeyWord = _configuration.GetSection(reportType + ":MailKeyword").Value;
            //string mailHeaderKeyWord = _configuration.GetSection("EmailConfiguration:mailHeaderKeyWord").Value;

            var isRetrieve = _mailhelper.RetrieveMail(paras.retrieveDate, mailHeaderKeyWord);
            bool imported = false;

            string returnString = $"{mailHeaderKeyWord}  + {DateTime.Now:yyyy/MM/dd}+ 資料未匯入";
            if (isRetrieve)
            {
                string recordCount = _csvhelper.ParseCsvFiles(paras.retrieveDate, reportType).ToString();
                imported = true;
                returnString = mailHeaderKeyWord + "成功:" + imported.ToString() + ",資料筆數:" + recordCount;
            }
            SendAutomatedEmail("victor.cheng@aurocore.com", $"{DateTime.Now:yyyy/MM/dd}-Tealeaf 資料匯入", returnString, "caco.tealeaf@gmail.com");
            return Ok(returnString);
        }
        [HttpPost]

        public IActionResult ImportCookiesEvents([FromForm] string retrieveDate)
        {
            if (string.IsNullOrEmpty(retrieveDate))
            {
                retrieveDate = DateTime.Now.ToString("yyyy-MM-dd");
            }
            //string mailHeaderKeyWord = _configuration.GetSection("EmailConfiguration:mailHeaderKeyWord").Value;
            string mailHeaderKeyWord = _configuration.GetSection("TealeafExport:MailKeyword").Value;
            var isRetrieve = _mailhelper.RetrieveMail(retrieveDate, mailHeaderKeyWord);
            bool imported = false;

            string returnString = $"{mailHeaderKeyWord}  + {DateTime.Now:yyyy/MM/dd}+ 資料未匯入";
            if (isRetrieve)
            {
                string recordCount = _csvhelper.ParseCsvFiles(retrieveDate, "TealeafExport").ToString();
                imported = true;
                returnString = mailHeaderKeyWord + "成功:" + imported.ToString() + ",資料筆數:" + recordCount;
            }
            SendAutomatedEmail("victor.cheng@aurocore.com", $"{DateTime.Now:yyyy/MM/dd}-Tealeaf 資料匯入", returnString, "caco.tealeaf@gmail.com");
            return Ok(returnString);
        }
        [HttpPost]
        public object TealeafReport([FromForm] Authorize.TealeafReportParameter paras)
        {
            _logger.LogInformation("TealeafEventController {0}", "TealeafReport");
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");

            string memberKey = paras.MemberKey;
            string loginID = paras.LoginId;
            DateTime startDate = Convert.ToDateTime(paras.StartDate);
            DateTime endDate = Convert.ToDateTime(paras.EndDate);
            string reportType = string.IsNullOrEmpty(paras.ReportType) ? "Tealeaf" : paras.ReportType;
            string keywordType = _configuration.GetSection(reportType + ":MailKeyword").Value;
            //string keywordType = string.IsNullOrEmpty(paras.ReportType) ? "Hami_mall" : paras.ReportType;
            //DateTime startDate= DateTime.ParseExact(paras.StartDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            //DateTime endDate= DateTime.ParseExact(paras.EndDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);

            string selectCommnad = @"SELECT eventstring,eventname,eventdate FROM tealeaf where eventdate
            between @startDate and @endDate and eventkeyword=@eventkeyword";

            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {

                    var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad, new { startDate = paras.StartDate, endDate = paras.EndDate, eventkeyword = keywordType }).ToList();
                    //var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad1).ToList();
                    var tealeafdata = _tealeafEventHelpers.EventParser(tealeafEventsData, reportType);
                    //_logger.LogInformation("Bye {0}", "TealeafReport");
                    return tealeafdata;
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return Ok();
            }

        }
        [HttpPost]
        public object TealeafRpt([FromForm] Authorize.TealeafReportParameter paras)
        {
            _logger.LogInformation("TealeafEventController {0}", "TealeafReport");
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string memberKey = paras.MemberKey;
            string loginID = paras.LoginId;
            DateTime startDate = Convert.ToDateTime(paras.StartDate);
            DateTime endDate = Convert.ToDateTime(paras.EndDate);
            //DateTime startDate= DateTime.ParseExact(paras.StartDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            //DateTime endDate= DateTime.ParseExact(paras.EndDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            string[] eventName = paras.ReportName.Split("|");
            string reportWhere = string.Empty;
            for (int i = 0; i < eventName.Length; i++)
            {
                if (i == eventName.Length - 1)
                {
                    reportWhere += $"EVENTNAME='{eventName[i]}'";
                }
                else
                {
                    reportWhere += $"EVENTNAME='{eventName[i]}' OR ";
                }
            }

            string selectCommnad = @$"SELECT eventstring,eventname,eventdate FROM tealeaf where eventdate
            BETWEEN @startDate AND @endDate AND({reportWhere})";
            string reportType = string.IsNullOrEmpty(paras.ReportType) ? "Tealeaf" : paras.ReportType;
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {

                    var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad, new { startDate = paras.StartDate, endDate = paras.EndDate }).ToList();
                    //var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad1).ToList();
                    var tealeafdata = _tealeafEventHelpers.EventParser(tealeafEventsData, reportType);
                    //_logger.LogInformation("Bye {0}", "TealeafReport");
                    return tealeafdata;
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return Ok();
            }

        }
        [HttpPost]
        public object ExportCFAwsBucket([FromForm] Authorize.TealeafReportParameter paras)
        {

            _logger.LogInformation("TealeafEventController {0}", "TealeafReport");
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");

            string memberKey = paras.MemberKey;
            string loginID = paras.LoginId;
            DateTime startDate = Convert.ToDateTime(paras.StartDate);
            DateTime endDate = Convert.ToDateTime(paras.EndDate);
            string reportType = string.IsNullOrEmpty(paras.ReportType) ? "Tealeaf" : paras.ReportType;
            string keywordType = _configuration.GetSection(reportType + ":MailKeyword").Value;
            //string keywordType = string.IsNullOrEmpty(paras.ReportType) ? "Hami_mall" : paras.ReportType;
            //DateTime startDate= DateTime.ParseExact(paras.StartDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
            //DateTime endDate= DateTime.ParseExact(paras.EndDate, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);

            string selectCommnad = @"SELECT eventstring,eventname,eventdate FROM tealeaf where eventdate
            between @startDate and @endDate and eventkeyword=@eventkeyword";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {

                    var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad, new { startDate = paras.StartDate, endDate = paras.EndDate, eventkeyword = keywordType }).ToList();
                    var tealeafdata = _tealeafEventHelpers.EventExport(tealeafEventsData, reportType);
                    //_logger.LogInformation("Bye {0}", "TealeafReport");
                    string _date = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");

                    string fileName = $"cookieMapping_{_date}.csv";
                    string filePath = "C:\\tealeafevents\\EnsembleCRM\\hami\\" + fileName;
                    using (var file = new StreamWriter(filePath))
                    {
                        foreach (var item in tealeafdata)
                        {
                            file.WriteLine(item.ToString());
                        }
                    }
                    return tealeafdata;
                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return Ok();
            }

        }
        [HttpGet]
        public async Task<IActionResult> GenerateTealeafLabels([FromForm] string retrieveDate)
        {
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string selectSql = @$"SELECT EVENTSTRING FROM TEALEAF
            WHERE EVENTDATE= @retrieveDate AND EVENTNAME='ReportHami_store瀏覽商品階段cookieInfocfPoint'";
            string insertSql = @"INSERT INTO [dbo].[TEALEAF_LABELS] ([CFT_UID], [LABEL], [TLT_ID], [CREATEDATE])
                                VALUES (@LABEL, @CFT_UID, @TLT_ID, @CREATEDATE);";
            string upsertSql = @$"
                                UPDATE [TEALEAF_LABELS] WITH (ROWLOCK)
                                SET [CFT_UID] = @CFT_UID,[TLT_ID]=@TLT_ID,[LABEL]=@LABEL,[CREATEDATE]=@CREATEDATE
                                WHERE ([TLT_ID] = @TLT_ID  OR [CFT_UID] =@CFT_UID) AND [LABEL]=@LABEL;
                                IF @@rowcount = 0
                                    BEGIN
                                        INSERT INTO [dbo].[TEALEAF_LABELS] ([CFT_UID], [LABEL], [TLT_ID], [CREATEDATE])
                                        VALUES (@CFT_UID, @LABEL, @TLT_ID, @CREATEDATE);            
                                    END";
            List<TealeafLabel> tealeafLabels = new();
            string rtnStr = "";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    try
                    {

                        var tlfLabelData = await connection.QueryFirstAsync<string>(selectSql, new { retrieveDate = retrieveDate });
                        var tealeafReport = JsonConvert.DeserializeObject<List<TealeafReport>>(tlfLabelData);


                        foreach (var ele in tealeafReport)
                        {
                            var eleAry = ele.Column1.Split(new Char[] { '~', '|' });
                            if (eleAry.Length == 16)
                            {
                                if (!string.IsNullOrEmpty(eleAry[5]) && !string.IsNullOrEmpty(eleAry[15]))
                                {
                                    string _label = "";
                                    if (eleAry[15].Length >= 1)
                                    {
                                        string _char = eleAry[15].Substring(0, 1).ToUpper();

                                        switch (_char)
                                        {
                                            case "D":
                                                _label = "桌機";

                                                break;
                                            case "T":
                                                _label = "平板";

                                                break;
                                            case "M":
                                                _label = "手機";

                                                break;

                                        }
                                    }


                                    tealeafLabels.Add(new TealeafLabel { CFT_UID = eleAry[1], TLT_ID = eleAry[5], LABEL = _label, CREATEDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") });

                                }
                            }
                        }
                        if (tealeafLabels.Count > 0)
                        {
                            Stopwatch sw = new Stopwatch();
                            sw.Start();


                            connection.Open();
                            using (var tx = connection.BeginTransaction(System.Data.IsolationLevel.Serializable))
                            {

                                try
                                {
                                    // 此處需要引用 Dapper
                                    connection.Execute(upsertSql, tealeafLabels, transaction: tx);

                                    tx.Commit();
                                }
                                catch (Exception ex)
                                {
                                    tx.Rollback();
                                    //Console.WriteLine(ex.ToString());
                                    _logger.LogError(ex.Message);
                                    return BadRequest(ex.Message);
                                }
                            }


                            sw.Stop();
                            rtnStr = $"{retrieveDate}-互動標籤匯入 {tealeafLabels.Count} 筆,共耗時{sw.ElapsedMilliseconds} ms";
                            _logger.LogInformation(rtnStr);
                        }

                        return Ok(rtnStr);
                    }
                    catch (Exception ex)
                    {

                        _logger.LogError(ex.Message);
                        return BadRequest(ex.Message);

                    }
                }

                //var tealeafEventsData = connection.Query<TealeafEvent.TealeafEvents>(selectCommnad1).ToList();

                //_logger.LogInformation("Bye {0}", "TealeafReport");



            }
            catch (Exception e)
            {

                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }


        }

        public void SendAutomatedEmail(string ReceiveMail, string Subject, string Body, string FromAddress)
        {
            var smtpServer = _configuration.GetSection("EmailConfiguration:SmtpServer").Value;
            var smtpPort = Int32.Parse(_configuration.GetSection("EmailConfiguration:SmtpPort").Value);
            var userName = _configuration.GetSection("EmailConfiguration:SmtpUsername").Value;
            var userPassword = _configuration.GetSection("EmailConfiguration:SmtpPassword").Value;

            MailMessage MyMail = new MailMessage();
            MyMail.From = new System.Net.Mail.MailAddress(FromAddress);
            MyMail.To.Add(ReceiveMail); //設定收件者Email
                                        // MyMail.Bcc.Add("密件副本的收件者Mail"); //加入密件副本的Mail          
            MyMail.Subject = Subject;
            MyMail.Body = Body; //設定信件內容
            MyMail.IsBodyHtml = true; //是否使用html格式
            SmtpClient MySMTP = new SmtpClient(smtpServer, smtpPort);
            MySMTP.EnableSsl = true;
            MySMTP.Credentials = new System.Net.NetworkCredential(userName, userPassword);
            try
            {
                MySMTP.Send(MyMail);
                MyMail.Dispose(); //釋放資源
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ex.ToString();
            }
        }




    }
}