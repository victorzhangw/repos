﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using EnsembleCRM.Models;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using EnsembleCRM.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.BigQuery.V2;
using Google.Apis.Auth.OAuth2;
using System;
using System.Globalization;
using System.Diagnostics;
using SqlBulkTools;
using System.Transactions;


namespace hami.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GoogleBigQueryController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        private ILogger<GoogleBigQueryController> _logger { get; set; }
        private CommonHelpers _common;
        public GoogleBigQueryController(IConfiguration config, ILogger<GoogleBigQueryController> logger, CommonHelpers commonHelpers)
        {

            _configuration = config;
            _logger = logger;
            _common = commonHelpers;
        }
        [HttpPost]
        public async Task<IActionResult> RetrieveCustomerbyCF([FromForm] Authorize.BigQueryParameter paras)
        {
            _logger.LogInformation("RetrieveCustomerbyCF {0}", "RetrieveCustomerbyCF");
            if ((string.IsNullOrEmpty(paras.Id_Origin)) || (string.IsNullOrEmpty(paras.retrieveDate)) || (string.IsNullOrEmpty(paras.TableName)))
            {
                _logger.LogError("RetrieveCustomerbyCF {0}", "Authorize.BigQueryParameter Missing");

                return BadRequest("Authorize.BigQueryParameter Missing");
            }
            string rtn = string.Empty;
            DateTime dt = DateTime.ParseExact(paras.retrieveDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            string _retrivevdate = dt.AddDays(-1).ToString("yyyy-MM-dd");
            string projectId = "cap-dmp-195902";
            string jsonPath = "big_query_cap-dmp-195902-ec7d59a95771.json";
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string rtnStr = string.Empty;
            try
            {
                var credentials = GoogleCredential.FromFile(jsonPath);
                var client = BigQueryClient.Create(projectId, credentials);
                // [END bigquery_client_json_credentials]

                // Use the client

                string query = $@"select * from
                (
                select  cft_uid,grade, last_value_time, row_number() over(partition by cft_uid order by last_value_time desc) as rn
                from {paras.TableName} where last_value_time >= '{_retrivevdate}'
                )A
                where rn<=1;";

                BigQueryJob job = client.CreateQueryJob(
                    sql: query,
                    parameters: null,
                    options: new QueryOptions { UseQueryCache = false });
                // Wait for the job to complete.
                job = job.PollUntilCompleted().ThrowOnAnyError();
                var result = client.GetQueryResults(job.Reference);
                List<Cdp_Contact> cFMembers = new();
                foreach (BigQueryRow row in result)
                {
                    cFMembers.Add(new Cdp_Contact()
                    {
                        ID = row["cft_uid"].ToString(),

                        GRADE = row["grade"].ToString(),
                        LASTVALUETIME = ((DateTime)row["last_value_time"]).ToString("yyyy-MM-dd HH:mm:ss"),
                        ID_ORIGIN = paras.Id_Origin,
                        CREATETIME = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        MODIFIEDTIME = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        CREATEUSER = "SERVICE",
                        MODIFIEDUSER = "SERVICE"

                    });
                }
                if (result.TotalRows > 0)
                {
                    string _dtnow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    var tempSql = @"CREATE TABLE #TmpTable([ID] varchar(100), [GRADE] varchar(50), [LASTVALUETIME] varchar(50), [ID_ORIGIN] varchar(50), [CREATETIME] datetime,
                    [MODIFIEDTIME] datetime, [CREATEUSER] varchar(50), [MODIFIEDUSER] varchar(50));";
                    var insertTmpSql = @$"insert  #TmpTable ([ID] , [GRADE] , [LASTVALUETIME] , 
                    [ID_ORIGIN] , [CREATETIME] , [MODIFIEDTIME] , [CREATEUSER] , [MODIFIEDUSER] )
                    VALUES(@ID,@GRADE,@LASTVALUETIME,@ID_ORIGIN,@CREATETIME,@MODIFIEDTIME,@CREATEUSER,@MODIFIEDUSER)";
                    var mergeSql = $@"MERGE INTO [AuroEnsembleCRM].[dbo].[CDP_CONTACTS] WITH (HOLDLOCK) AS Target USING #TmpTable AS Source ON [Target].[ID] = [Source].[ID] AND [Target].[ID_ORIGIN] = [Source].[ID_ORIGIN]
                    WHEN MATCHED THEN UPDATE SET [Target].[ID] = [Source].[ID], [Target].[GRADE] = [Source].[GRADE], [Target].[LASTVALUETIME] = [Source].[LASTVALUETIME], [Target].[ID_ORIGIN] = [Source].[ID_ORIGIN], [Target].[MODIFIEDTIME] = [Source].[MODIFIEDTIME], [Target].[MODIFIEDUSER] = [Source].[MODIFIEDUSER] 
                    WHEN NOT MATCHED BY TARGET THEN INSERT ([ID], [GRADE], [LASTVALUETIME], [ID_ORIGIN], [CREATETIME], [CREATEUSER]) values ([Source].[ID], [Source].[GRADE], [Source].[LASTVALUETIME], [Source].[ID_ORIGIN], [Source].[CREATETIME], [Source].[CREATEUSER]) ;
                    DROP TABLE #TmpTable;";

                    var upsertSql = @$"
                                UPDATE CDP_CONTACTS WITH (ROWLOCK)
                                SET [ID_ORIGIN] = {"'" + paras.Id_Origin + "'"},[ID]=@cft_uid,[GRADE]=@grade,[LASTVALUETIME]=@last_value_time,[MODIFIEDTIME]={"'" + _dtnow + "'"},[MODIFIEDUSER]='Service'
                                WHERE [ID] = @cft_uid AND [ID_ORIGIN]={"'" + paras.Id_Origin + "'"};
                                IF @@rowcount = 0
                                    BEGIN
                                        INSERT INTO CDP_CONTACTS([ID_ORIGIN],[ID],[GRADE],[LASTVALUETIME],[CREATEUSER],[CREATETIME])
                                        VALUES ({"'" + paras.Id_Origin + "'"},@cft_uid,@grade,@last_value_time,'Service',{"'" + _dtnow + "'"});             
                                    END";


                    using (var connection = new SqlConnection(cnStr.Value))
                    {
                        connection.Open();

                        using (var tx = connection.BeginTransaction())
                        {
                            try
                            {
                                Stopwatch sw = new Stopwatch();
                                sw.Start();
                                // 此處需要引用 Dapper
                                connection.Execute(tempSql, transaction: tx);
                                var _contact = connection.Execute(insertTmpSql, cFMembers, transaction: tx);
                                connection.Execute(mergeSql, transaction: tx);

                                tx.Commit();
                                sw.Stop();
                                rtnStr = $"聯絡人匯入 {_contact} 筆,共耗時{sw.ElapsedMilliseconds} ms";
                                _logger.LogInformation(rtnStr);
                            }
                            catch (Exception e)
                            {
                                tx.Rollback();
                                //Console.WriteLine(ex.ToString());
                                _logger.LogError(e.Message);
                                return BadRequest(e.Message);
                            }
                        }
                    }

                    /*
                    var bulk = new BulkOperations();
                    using (TransactionScope trans = new TransactionScope())
                    {
                        using (var connection = new SqlConnection(cnStr.Value))
                        {
                            try
                            {
                                Stopwatch sw = new Stopwatch();
                                sw.Start();
                                bulk.Setup<Cdp_Contact>(x => x.ForCollection(cFMembers))

                                .WithTable("CDP_CONTACTS")
                                .AddAllColumns()

                                .BulkInsertOrUpdate()
                                .MatchTargetOn(x => x.ID);

                                //bulk.CommitTransaction(connection);
                                bulk.CommitTransaction(connection);
                                sw.Stop();
                                rtnStr = $"聯絡人匯入 {cFMembers.Count} 筆,共耗時{sw.ElapsedMilliseconds} ms";
                                _logger.LogInformation(rtnStr);
                            }
                            catch (Exception e)
                            {
                                trans.Dispose();
                                //Console.WriteLine(ex.ToString());
                                _logger.LogError(e.Message);
                                return BadRequest(e.Message);
                            }

                        }

                        trans.Complete();
                    }
                    */
                }

            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(e.Message);
            }

            return Ok(rtnStr);
        }
    }
}
