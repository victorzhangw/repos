﻿using EnsembleCRM.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using EnsembleCRM.Models;
using System.Data.SqlClient;
using Dapper;
using EnsembleCRM.Enums;
namespace EnsembleCRM.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        private ILogger<CustomerController> _logger { get; set; }
        private CommonHelpers _common;
        public CustomerController(IConfiguration config, TwISOCodeHelpers twISO, ILogger<CustomerController> logger, CommonHelpers commonHelpers)
        {

            _configuration = config;
            _logger = logger;
            _common = commonHelpers;
        }
        [HttpPost]
        public async Task<List<CustomerTags>> CustomerTagList([FromForm] Authorize.GridParameter paras)
        {
            List<CustomerTags> customerTags = new();
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string eventSql = @"SELECT * FROM CUSTOMERTAGS ";
            string[] condArray;
            List<string> conditions = new List<string>();
            if (!string.IsNullOrEmpty(paras.Operation) && !string.IsNullOrEmpty(paras.Clause))
            {
                string _whereStr = string.Empty;
                if (paras.Operation == "AND" || paras.Operation == "&")
                {
                    condArray = paras.Clause.Split("&");

                    for (int i = 0; i < condArray.Length; i++)
                    {

                        string _value = " TAGS like '%" + condArray[i] + "%'";

                        if (i == condArray.Length - 1)
                        {
                            _whereStr = _whereStr + _value;
                        }
                        else
                        {
                            _whereStr = _whereStr + _value + " AND ";
                        }

                        //myCommand.Parameters.Add($"@label{i}", MySqlDbType.String).Value = condArray[i];

                        //parameters.Add($"@label{i}", "%"+condArray[i]+"%");
                    }
                    eventSql += "WHERE " + _whereStr;
                }
                if (paras.Operation == "OR" || paras.Operation == "|")
                {
                    condArray = paras.Clause.Split("|");

                    for (int i = 0; i < condArray.Length; i++)
                    {
                        string _value = " TAGS like '%" + condArray[i] + "%'";
                        if (i == condArray.Length - 1)
                        {
                            _whereStr = _whereStr + _value;
                        }
                        else
                        {
                            _whereStr = _whereStr + _value + " OR ";
                        }

                        //myCommand.Parameters.Add($"@label{i}", MySqlDbType.String).Value = condArray[i];

                        //parameters.Add($"@label{i}", "%"+condArray[i]+"%");
                    }
                    eventSql += "WHERE " + _whereStr;
                }
            }
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    await connection.OpenAsync();
                    if (!eventSql.Contains("WHERE"))
                    {
                        eventSql = "SELECT Top 11000 * FROM CUSTOMERTAGS";
                    }
                    eventSql += " ORDER BY  MODIFIEDTIME DESC";

                    var customers = await connection.QueryAsync<CustomerTags>(eventSql);
                    customerTags = customers.ToList();

                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return customerTags;
            }
            return customerTags;

        }
        [HttpPost]
        public async Task<List<CustomerRank>> CustomerRanks([FromForm] Authorize.CustomerParameter paras)
        {
            List<CustomerRank> customerRanks = new();

            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            var selectSql =
                $@"
                    select * from VIEW_CUSTOMERRANKS;
                    select * from VIEW_SEGMENTCUSTOMERWEEKRANK;
                    select * from VIEW_SEGMENTCUSTOMERRANK;
                    select * from VIEW_WEEKCUSTOMERRANK;
                    select * from VIEW_CUSTOMER_AGE_LOCATION_GENDER;
                    
                    ;with CTE as(
                    SELECT tid,JSON_VALUE(t.value,'$.Column1')as Col1, 
                    JSON_VALUE(t.value,'$.Column2') as Col2 from TEALEAF 
                    CROSS APPLY OPENJSON(EVENTSTRING, '$') t
                    WHERE EVENTNAME='ReportHami_store有點數級距會員網站瀏覽次數' and EVENTDATE between '{paras.StartDate}'   and  '{paras.EndDate}'
                    )
                    SELECT CTE.Col1 As Category,Count(T2.GRADE) as Name,sum(Cast(CTE.Col2 as int)) as Value FROM CTE 
                    INNER JOIN
                    (SELECT GRADE FROM CDP_CONTACTS  ) AS T2
                     ON CTE.Col1 = T2.GRADE GROUP BY CTE.Col1
                    ;with CTE as(
                    SELECT tid,JSON_VALUE(t.value,'$.Column1')as Col1, 
                    JSON_VALUE(t.value,'$.Column2') as Col2 from TEALEAF 
                    CROSS APPLY OPENJSON(EVENTSTRING, '$') t
                    WHERE EVENTNAME='ReportHami_store有點數級距會員網站瀏覽次數' and EVENTDATE = '{paras.EndDate}'   
                    )
                    SELECT CTE.Col1 As Category,Count(T2.GRADE) as Name,sum(Cast(CTE.Col2 as int)) as Value FROM CTE 
                    INNER JOIN
                    (SELECT GRADE FROM CDP_CONTACTS  ) AS T2
                     ON CTE.Col1 = T2.GRADE GROUP BY CTE.Col1
                ";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    await connection.OpenAsync();
                    List<CustomerCate> genderCate = new();
                    List<CustomerCate> ageCate = new();
                    List<CustomerCate> locationCate = new();
                    List<CustomerCate> dayCate = new();
                    List<CustomerCate> weekCate = new();
                    var multi = connection.QueryMultiple(selectSql);
                    var customerGrade = multi.Read<CustomerGrade>().ToList();
                    var segmentCustomerWeek = multi.Read<SegmentCustomerWeek>().ToList();
                    var segmentCustomerWeekGrade = multi.Read<SegmentCustomerWeekGrade>().ToList();
                    var customerWeekCount = multi.Read<CustomerWeekCount>().ToList();
                    var customerCate = multi.Read<CustomerCate>().ToList();

                    foreach (var customer in customerCate)
                    {
                        if (customer.Category == nameof(CustomerCateEnum.Age))
                        {
                            ageCate.Add(new CustomerCate { Name = customer.Name, Value = customer.Value });
                        }
                        if (customer.Category == nameof(CustomerCateEnum.Location))
                        {
                            locationCate.Add(new CustomerCate { Name = customer.Name, Value = customer.Value });
                        }
                        if (customer.Category == nameof(CustomerCateEnum.Gender))
                        {
                            genderCate.Add(new CustomerCate { Name = customer.Name, Value = customer.Value });
                        }
                    }
                    weekCate = multi.Read<CustomerCate>().ToList();
                    dayCate = multi.Read<CustomerCate>().ToList();
                    customerRanks.Add(new CustomerRank
                    {
                        customerGrades = customerGrade,
                        segmentCustomerWeekGrades = segmentCustomerWeekGrade,
                        segmentCustomerWeeks = segmentCustomerWeek,
                        customerWeekCounts = customerWeekCount,
                        AgeCates = ageCate,
                        GenderCates = genderCate,
                        LocationCates = locationCate,
                        DayCates = dayCate,
                        WeekCates = weekCate,

                    });

                }

            }
            catch (Exception ex)
            {

                _logger.LogError(ex.ToString());
                return customerRanks;
            }
            return customerRanks;
        }
    }
}
