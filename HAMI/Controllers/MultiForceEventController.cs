﻿using EnsembleCRM.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using EnsembleCRM.Helpers;
using RestSharp;
using Newtonsoft.Json;
namespace hami.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MultiForceEventController : ControllerBase
    {
        private readonly IConfiguration _configuration;
       
        private ILogger<MultiForceEventController> _logger { get; set; }
        private CommonHelpers _common;
        public MultiForceEventController(IConfiguration config, TwISOCodeHelpers twISO, ILogger<MultiForceEventController> logger, CommonHelpers commonHelpers)
        {
       
            _configuration = config;
            _logger = logger;
            _common = commonHelpers;
        }
        [HttpPost]
        public MFDaily MFDailyReport(MfParameters mfParameters)
        {
            MFDaily mFDaily = new();
            
            string key = _configuration.GetSection("ClickForce:Key").Value;
            if (!string.IsNullOrEmpty(key))
            {
                if (string.IsNullOrEmpty(mfParameters.StartDateTime))
                {
                    mfParameters.StartDateTime = DateTime.Now.AddDays(-1).ToString("yy-MM-dd");
                }
                string clientUrl = "https://api.holmesmind.com/report/daily?key="+key+"&date="+mfParameters.StartDateTime;
                var client = new RestClient(clientUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if (!string.IsNullOrEmpty(response.Content)) {

                    mFDaily = JsonConvert.DeserializeObject<MFDaily>(response.Content);
                    if (mFDaily.report == null)
                    {
                        return mFDaily;
                    }
                    foreach(var item in mFDaily.report)
                    {
                        int click = int.Parse(item.click);
                        int impress = int.Parse(item.impress);
                        decimal budget = Decimal.Parse(item.budget);

                        if (click == 0)
                        {
                            item.ecpc = "0";
                        }
                        else
                        {
                            item.ecpc = Decimal.Round((budget / click), 2).ToString();
                        }
                        if (impress == 0)
                        {
                            item.ecpm = "0";
                            item.clickrate = "0";
                        }
                        else
                        {
                            item.ecpm = Decimal.Round((budget / impress) * 1000, 2).ToString();
                            item.clickrate = Decimal.Round((click / impress) * 100, 2).ToString();
                        }


                    }
                }
                
            }
            return mFDaily;

        }
        [HttpPost]
        public MFMonthly MFMonthlyReport(MfParameters mfParameters)
        {
            MFMonthly mFMonthly = new();
            string key = _configuration.GetSection("ClickForce:Key").Value;
            if (!string.IsNullOrEmpty(key))
            {
                if (string.IsNullOrEmpty(mfParameters.StartDateTime))
                {
                    mfParameters.StartDateTime = DateTime.Now.AddDays(-1).ToString("yy-MM-dd");
                }
                if (string.IsNullOrEmpty(mfParameters.EndDateTime))
                {
                    mfParameters.StartDateTime = DateTime.Now.AddDays(-32).ToString("yy-MM-dd");
                }
                string clientUrl = "https://api.holmesmind.com/report/monthly?key=" + key + "&start=" + mfParameters.StartDateTime+ "&end=" + mfParameters.EndDateTime ;
                var client = new RestClient(clientUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if (!string.IsNullOrEmpty(response.Content))
                {

                    mFMonthly = JsonConvert.DeserializeObject<MFMonthly>(response.Content);
                    foreach (var item in mFMonthly.report)
                    {
                        int click = int.Parse(item.click);
                        int impress = int.Parse(item.impress);
                        Decimal budget = Decimal.Parse(item.budget);
                        
                        
                        
                        if (click == 0)
                        {
                            item.ecpc = "0";
                        }
                        else
                        {
                            item.ecpc = Decimal.Round((budget / click), 2).ToString();
                        }
                        if (impress == 0)
                        {
                            item.ecpm = "0";
                            item.clickrate = "0";
                        }
                        else
                        {
                            item.ecpm = Decimal.Round((budget / impress) * 1000, 2).ToString();
                            item.clickrate = Decimal.Round((click / impress) * 100, 2).ToString();
                        }

                    }


                }

            }
            return mFMonthly;

        }
    }
}
