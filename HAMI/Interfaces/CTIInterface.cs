﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnsembleCRM.Models;
namespace EnsembleCRM.Interfaces
{
    interface CTIInterface
    {
    }
    public interface IInboundCall
    {
        List<CTI.InfoACDCalls> ReadCalls(int days);
        List<CTI.InfoACDCalls> DateDateRangeCalls(DateTime startdate, DateTime enddate);
    }
}
