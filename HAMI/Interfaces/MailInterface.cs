﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnsembleCRM.Interfaces
{
    public interface MailInterface
    {
        bool RetrieveMail();
        void SendEmail(string ReceiveMail, string Subject, string Body, string FromAddress, string bcc="");
    }
}
