using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.Models;
using Aurocore.Email.Core;
using Aurocore.Security.Models;
using System.Collections.Generic;
using Aurocore.Commons.Log;
using Dapper;
using Aurocore.Commons.Helpers;
using System.Security.Claims;
using System.Linq;
using Newtonsoft.Json;
using Aurocore.Commons.Options;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Aurocore.WorkOrders.Enums;
using Aurocore.Commons;

namespace Aurocore.WorkOrders.Repositories
{
    /// <summary>
    /// 案件倉儲接口實現
    /// </summary>
    public class OutBoundRepository : BaseRepository<OutBound, string>, IOutBoundRepository
    {
        public OutBoundRepository()
        {
            this.tableName = "CRM_OutBound";
            this.primaryKey = "Id";
        }

        public OutBoundRepository(IDbContextCore context) : base(context)
        {
        }
        

    }
}