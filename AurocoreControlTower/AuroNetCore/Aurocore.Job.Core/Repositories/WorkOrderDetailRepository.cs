using System;
using System.Collections.Generic;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.Enums;
using Aurocore.WorkOrders.Dtos;
using Dapper;
using System.Linq;
using System.IO;
using Spire.Pdf;
using Spire.Pdf.Widget;
using Spire.Pdf.Fields;
using System.Drawing;
using Spire.Pdf.Graphics;
using Aurocore.Security.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Json;
using System.Threading.Tasks;
using Aurocore.Commons.Pages;

namespace Aurocore.WorkOrders.Repositories
{
	/// <summary>
	/// 倉儲接口的實現
	/// </summary>
	public class WorkOrderDetailRepository : BaseRepository<WorkOrderDetail, string>, IWorkOrderDetailRepository
	{
		
		public WorkOrderDetailRepository()
		{
			this.tableName = "CRM_WorkOrderDetail";
			this.primaryKey = "Id";
		}

		public WorkOrderDetailRepository(IDbContextCore context) : base(context)
		{
			
		}

		public async Task<string> CreateWorkOrderDetailNo(WorkOrderDetail workOrderDetail)
		{
			string formNo = string.Empty;
			
			if (!string.IsNullOrEmpty(workOrderDetail.Id))
			{
				workOrderDetail.Id = string.Empty;
			}

			string selectSql = @$"SELECT '{workOrderDetail.WorkOrderId}'+'-' + CONVERT(VARCHAR,(count(Id)+1)) as formno from CRM_WorkOrderDetail WHERE LEFT(Id, 
			case 
			when CHARINDEX ('-' , Id)-1 >0 then CHARINDEX ('-' , Id)-1
			when CHARINDEX ('-' , Id)-1 <=0 then 0
			end)='{workOrderDetail.WorkOrderId}'";
			formNo = await DapperConn.ExecuteScalarAsync<string>(selectSql);
			
			return formNo;
		}

		public async Task<List<WorkOrderDetail>> GetHistorywithPager(string sql, PagerInfo info)
		{
			List<WorkOrderDetail> orders = null;
			if (!string.IsNullOrEmpty(sql))
			{
				var reader = await DapperConn.QueryMultipleAsync(sql);
				info.RecordCount = reader.ReadFirst<int>();
				orders = reader.Read<WorkOrderDetail>().AsList();
				
				
				
			}


			return orders;
		}

		public List<WorkOrderBrief> GetWorkOrderBrief(string sql)
		{
			List<WorkOrderBrief> orderBrief = null;
			if (!string.IsNullOrEmpty(sql))
			{
				orderBrief = DapperConn.Query<WorkOrderBrief>(sql).ToList();
			}


			return orderBrief;
		}

		public bool MergeEndorsement(EndorsementForm endorsementForm, string filePath)
		{
			if (string.IsNullOrEmpty(endorsementForm.FilePath) || 
				string.IsNullOrEmpty(endorsementForm.BeforeInsertText)|| 
				string.IsNullOrEmpty(endorsementForm.WorkOrderDetailId))
			{
				throw new NotImplementedException("缺少欄位");

			}
			PdfDocument doc = new PdfDocument();
			var _bool = File.Exists(filePath);
			if (_bool)
			{
				doc.LoadFromFile(filePath);
				PdfPageBase page = doc.Pages[0];
				PdfFormWidget formWidget = doc.Form as PdfFormWidget;
				PdfTextBoxFieldWidget pdfText;
				Font font0 = new Font("標楷體", 9, FontStyle.Regular);   // 中文字體 不可超過18，19開始會變成空白
				PdfTrueTypeFont font = new PdfTrueTypeFont(font0, true);
				PdfBrush brush = new PdfSolidBrush(Color.DarkBlue);
				string newText = endorsementForm.BeforeInsertText;
				switch (endorsementForm.InsureTypeName)
				{
					case nameof(InsureType.住火險):
						foreach (PdfFieldWidget fieldWidget in formWidget.FieldsWidget)
						{
							if (fieldWidget.Name == "topmostSubform[0].MainPage[0].Subform3[0].#field[0]")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(newText, font, brush, pdfText.Bounds);

							}
						}
						break;
					case nameof(InsureType.汽車險):
						foreach (PdfFieldWidget fieldWidget in formWidget.FieldsWidget)
						{
							if (fieldWidget.Name == "PDF[0].MainPage[0].TextField1[1]")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(newText, font, brush, pdfText.Bounds);

							}
						}
						break;
					case nameof(InsureType.傷害險):
						foreach (PdfFieldWidget fieldWidget in formWidget.FieldsWidget)
						{
							if (fieldWidget.Name == "untitled9")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(newText, font, brush, pdfText.Bounds);

							}
						}
						break;

				}
				doc.SaveToFile(filePath, FileFormat.PDF);

				_bool = true;

			}
			
			
			
			
			
			return _bool;
		}
		public string MergeReissue(ReissueForm reissueForm, string filePath)
		{
			AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
			SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
			if (string.IsNullOrEmpty(reissueForm.FilePath) ||
				string.IsNullOrEmpty(reissueForm.ApplicantName) ||
				string.IsNullOrEmpty(reissueForm.WorkOrderDetailId))
			{
				throw new NotImplementedException("缺少欄位");

			}
			var twCalendar = new System.Globalization.TaiwanCalendar();
			var twYear = twCalendar.GetYear(DateTime.Now);
			var twMonth = DateTime.Now.Month;
			var twDay = DateTime.Now.Day;
			PdfDocument doc = new PdfDocument();
			switch (reissueForm.InsureTypeName)
			{
				case nameof(InsureType.住火險):
					reissueForm.FilePath += "住火險保險單據補發申請書範本.pdf";
					break;
				case nameof(InsureType.汽車險):
					reissueForm.FilePath += "車險保險單據補發申請書範本.pdf";
					break;

			}
			var _bool = File.Exists(reissueForm.FilePath );
			if (_bool)
			{
				
				doc.LoadFromFile(reissueForm.FilePath);
				PdfPageBase page = doc.Pages[0];
				PdfFormWidget formWidget = doc.Form as PdfFormWidget;
				PdfTextBoxFieldWidget pdfText;
				PdfRadioButtonListFieldWidget pdfRadioButtonWidgetItem;
				PdfCheckBoxWidgetFieldWidget pdfCheckBoxField;
				Font font0 = new Font("標楷體", 12, FontStyle.Regular);   // 中文字體 不可超過18，19開始會變成空白
				PdfTrueTypeFont font = new PdfTrueTypeFont(font0, true);
				PdfBrush brush = new PdfSolidBrush(Color.DarkBlue);
				string ApplicantId = reissueForm.ApplicantId;
				string itag = reissueForm.Itag;
				switch (reissueForm.InsureTypeName)
				{
					case nameof(InsureType.住火險):
						foreach (PdfFieldWidget fieldWidget in formWidget.FieldsWidget)
						{
							if (fieldWidget.Name == "Itag")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.Itag, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "ApplicantName")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.ApplicantName, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Recipient")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.deliveryInfo.Recipient, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Address")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.deliveryInfo.Address, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Year")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(twYear.ToString(), font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Month")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(twMonth.ToString(), font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Day")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(twDay.ToString(), font, brush, pdfText.Bounds);

							}
							
							if (fieldWidget.Name == "checkbox1")
							{

								if (reissueForm.ReissueReson == "郵局遺失")
								{
									pdfCheckBoxField= fieldWidget as PdfCheckBoxWidgetFieldWidget;
									pdfCheckBoxField.Checked = true;
								}

							}
							if (fieldWidget.Name == "checkbox2")
							{

								if (reissueForm.ReissueReson == "遺失補發")
								{
									pdfCheckBoxField = fieldWidget as PdfCheckBoxWidgetFieldWidget;
									pdfCheckBoxField.Checked = true;
								}

							}
							if (fieldWidget.Name == "checkbox3")
							{

								if (reissueForm.ReissueReson == "補發其他")
								{
									pdfCheckBoxField = fieldWidget as PdfCheckBoxWidgetFieldWidget;
									pdfCheckBoxField.Checked = true;
								}

							}
							if (fieldWidget.Name == "Recipient")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.deliveryInfo.Recipient, font, brush, pdfText.Bounds);

							}

							if (fieldWidget.Name == "Address")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.deliveryInfo.Address, font, brush, pdfText.Bounds);

							}
							
							if (fieldWidget.Name == "radio1")
							{

								if (reissueForm.deliveryInfo.DeliveryType == "掛號")
								{
									pdfRadioButtonWidgetItem = fieldWidget as PdfRadioButtonListFieldWidget;
									pdfRadioButtonWidgetItem.Value = "Yes";
									pdfRadioButtonWidgetItem.SelectedIndex = 0;
								}

							}
							if (fieldWidget.Name == "radio2")
							{

								if (reissueForm.deliveryInfo.DeliveryType == "限時")
								{
									pdfRadioButtonWidgetItem = fieldWidget as PdfRadioButtonListFieldWidget;
									pdfRadioButtonWidgetItem.Value = "Yes";
									pdfRadioButtonWidgetItem.SelectedIndex = 0;
								}

							}
							if (fieldWidget.Name == "radio3")
							{

								if (reissueForm.deliveryInfo.DeliveryType == "平信")
								{
									pdfRadioButtonWidgetItem = fieldWidget as PdfRadioButtonListFieldWidget;
									pdfRadioButtonWidgetItem.Value = "Yes";
									pdfRadioButtonWidgetItem.SelectedIndex = 0;
								}

							}


						}
						break;
					case nameof(InsureType.汽車險):
						foreach (PdfFieldWidget fieldWidget in formWidget.FieldsWidget)
						{
							if (fieldWidget.Name == "ApplicantName")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.ApplicantId, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Itag")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.ApplicantId, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Year")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(twYear.ToString(), font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Month")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(twMonth.ToString(), font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Day")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(twDay.ToString(), font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "checkbox1")
							{

								if (reissueForm.ReissueReson == "郵局遺失")
								{
									pdfCheckBoxField = fieldWidget as PdfCheckBoxWidgetFieldWidget;
									pdfCheckBoxField.Checked = true;
								}

							}
							if (fieldWidget.Name == "checkbox2")
							{

								if (reissueForm.ReissueReson == "遺失補發")
								{
									pdfCheckBoxField = fieldWidget as PdfCheckBoxWidgetFieldWidget;
									pdfCheckBoxField.Checked = true;
								}

							}
							if (fieldWidget.Name == "checkbox3")
							{

								if (reissueForm.ReissueReson == "補發其他")
								{
									pdfCheckBoxField = fieldWidget as PdfCheckBoxWidgetFieldWidget;
									pdfCheckBoxField.Checked = true;
								}

							}
							if (fieldWidget.Name == "Recipient")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.deliveryInfo.Recipient, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "Address")
							{

								pdfText = fieldWidget as PdfTextBoxFieldWidget;
								page.Canvas.DrawString(reissueForm.deliveryInfo.Address, font, brush, pdfText.Bounds);

							}
							if (fieldWidget.Name == "radio1")
							{

								if (reissueForm.deliveryInfo.DeliveryType == "掛號")
								{
									pdfRadioButtonWidgetItem = fieldWidget as PdfRadioButtonListFieldWidget;
									pdfRadioButtonWidgetItem.Value = "Yes";
									pdfRadioButtonWidgetItem.SelectedIndex = 0;
								}

							}
							if (fieldWidget.Name == "radio2")
							{

								if (reissueForm.deliveryInfo.DeliveryType == "限時")
								{
									pdfRadioButtonWidgetItem = fieldWidget as PdfRadioButtonListFieldWidget;
									pdfRadioButtonWidgetItem.Value = "Yes";
									pdfRadioButtonWidgetItem.SelectedIndex = 0;
								}

							}
							if (fieldWidget.Name == "radio3")
							{

								if (reissueForm.deliveryInfo.DeliveryType == "平信")
								{
									pdfRadioButtonWidgetItem = fieldWidget as PdfRadioButtonListFieldWidget;
									pdfRadioButtonWidgetItem.Value = "Yes";
									pdfRadioButtonWidgetItem.SelectedIndex = 0;
								}

							}
						}
						break;
					

				}
				string folder = DateTime.Now.ToString("yyyyMMdd");
				

				if (sysSetting.Filesave == "1")
				{
					filePath = filePath + "/" + folder + "/";
				}
				if (sysSetting.Filesave == "2")
				{
					DateTime date = DateTime.Now;
					filePath = filePath + "/" + date.Year + "/" + date.Month + "/" + date.Day + "/";
				}

				
				if (sysSetting.Fileserver == "localhost")
				{
					if (!Directory.Exists(filePath))
					{
						Directory.CreateDirectory(filePath);
					}
				}
				string newName = GuidUtils.CreateShortNo();
				string newfileName = newName + ".pdf";
				filePath += newfileName;
				doc.SaveToFile(filePath, FileFormat.PDF);
				string _path = sysSetting.Filepath+"/"+ folder + "/" + newfileName;
				return _path;
			

			}





			return string.Empty;
		}



	}
}
