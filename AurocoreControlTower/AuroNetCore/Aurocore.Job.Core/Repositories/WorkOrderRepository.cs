using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.Models;
using Aurocore.Email.Core;
using Aurocore.Security.Models;
using System.Collections.Generic;
using Aurocore.Commons.Log;
using Dapper;
using Aurocore.Commons.Helpers;
using System.Security.Claims;
using System.Linq;
using Newtonsoft.Json;
using Aurocore.Commons.Options;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Aurocore.WorkOrders.Enums;
using Aurocore.Commons;
using Aurocore.AspNetCore.Models;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using System.Data;
using System.Text;
using IdentityModel;

namespace Aurocore.WorkOrders.Repositories
{
    /// <summary>
    /// 案件倉儲接口實現
    /// </summary>
    public class WorkOrderRepository : BaseRepository<WorkOrder, string>, IWorkOrderRepository
    {
        public WorkOrderRepository()
        {
            this.tableName = "CRM_WorkOrder";
            this.primaryKey = "Id";
        }

        public WorkOrderRepository(IDbContextCore context) : base(context)
        {
        }
        public async Task<List<WorkOrder>> RetrieveEmail(string ItemCode,IWebHostEnvironment  hostingEnvironment)
        {
            ReceiveResultEntity receiveResultEntity = new ReceiveResultEntity();
            
            
            if (HasInjectionData(ItemCode))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", ItemCode));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(ItemCode))
            {
                ItemCode = "1";
            }
           
            string selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemCode='{0}'", ItemCode);
            ItemsDetail dictSource = DapperConn.QueryFirstOrDefault<ItemsDetail>(selectSql);
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemCode='{0}'", nameof(WorkflowStatus.等待分配));
            ItemsDetail dictSource2 = DapperConn.QueryFirstOrDefault<ItemsDetail>(selectSql);
            string formType = dictSource.ItemName;
            receiveResultEntity = await EWSMailHelper.CheckInbox(formType, hostingEnvironment).ConfigureAwait(false);
            if (receiveResultEntity.ResultStatus == false)
            {
                throw new ArgumentNullException();
                //return false;
            }
    
            List<MailBodyEntity> mailBodies = receiveResultEntity.bodyEntities;
            List<WorkOrder> lstJobs = new List<WorkOrder>();
            bool parseSuccess = Int32.TryParse(dictSource2.Description, out int limitHours);
            if (!parseSuccess)
            {
                Log4NetHelper.Error(DateTime.Now + " : 轉換" +formType +"天數失敗");
                throw new Exception("轉換"+ formType+ "天數失敗");

            }
            if (mailBodies.Count > 0)
            {
                foreach (MailBodyEntity item in mailBodies)
                {
                    string joinedPath = string.Empty;
                    WorkOrder job = new WorkOrder();
                    job.FormTypeName = dictSource.ItemName;
                    job.FormTypeId = dictSource.Id;
                    job.ParentFormTypeName = dictSource.ItemName;
                    job.ParentFormTypeId = dictSource.Id;
                    job.StatusName = dictSource2.ItemName;
                    job.StatusId = dictSource2.Id;
                    if (!string.IsNullOrEmpty(item.Subject))
                    {
                        job.Header = item.Subject;
                    }
                    else
                    {
                        job.Header = "";
                    }
                    if (!string.IsNullOrEmpty(item.Body))
                    {
                        job.Body = item.Body;
                    }
                    else
                    {
                        job.Body = "";
                    }
                   
                    if (item.FromAddresses != null)
                    {
                        job.ApplicantName = item.FromAddresses.First().Name;
                        job.ApplicantEmail = item.FromAddresses.First().Address;
                    }
                    
                    if (item.MailFiles != null&& item.MailFiles.Any())
                    {
                        joinedPath = string.Join(",", item.MailFiles.Select(p => p.MailFilePath.ToString()));
                        job.Attachment = joinedPath;
                    }
                    job.SourceId = item.EWSItemId;
                    job.SourceName = "Exchange ItemId Object";
                    job.CaseOpenTime = DateTime.Now;
                    job.CaseCloseTime = DateTime.Now.AddHours(limitHours);
                    job.DeleteMark = false;
                    job.SetAccountStatus = false;
                    job.SetAccountRelation = "";
                    job.SetAccountStatusDesc = ErrCode.err70003;
                    lstJobs.Add(job);
                }
                
            }

            return lstJobs;
        }
        public List<WorkOrder> ImportMemo(Memo memo)
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            AppSetting sysSetting = System.Text.Json.JsonSerializer.Deserialize<AppSetting>(AurocoreCacheHelper.Get("SysSetting").ToJson());
            string key = sysSetting.MemoKey;
            string iv = sysSetting.MemoIV;
            List<WorkOrder> lstJobs = new List<WorkOrder>();	
            string selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemName='{0}'", nameof(WorkOrderType.Memo));
            ItemsDetail parentWOType = DapperConn.Query<ItemsDetail>(selectSql).FirstOrDefault();
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ParentId ='{0}'", parentWOType.Id);
            List<ItemsDetail> dictSourceList = DapperConn.Query<ItemsDetail>(selectSql).ToList();
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemName='{0}'", nameof(WorkflowStatus.等待分配));
            ItemsDetail dictSource2 = DapperConn.QueryFirstOrDefault<ItemsDetail>(selectSql);
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemName='{0}'", nameof(WorkflowStatus.等待分配));
           
            foreach (var caseInfo in memo.case_info)
            {
                string[] nclaims = caseInfo.nclaim_item.Split("/");
                MemoFormType memoFormType = (MemoFormType)Enum.Parse(typeof(MemoFormType), nclaims[0], true);
                string f = memoFormType.ToString();
                var dictSource = dictSourceList.Where(d=>d.ItemName==f).SingleOrDefault();
                WorkOrder job = new WorkOrder();
                string caseString = JsonConvert.SerializeObject(caseInfo);

                bool parseSuccess = Int32.TryParse(dictSource.ItemProperty1, out int limitHours);
                if (!parseSuccess)
                {
                    Log4NetHelper.Error(DateTime.Now + " : 轉換 Memo 天數失敗");
                    throw new Exception("轉換 Memo 天數失敗");
                    
                }
                string[] insureType = new string[2];
                
                switch (caseInfo.nnsure_type)
                {
                    case "車險":
                        job.InsuranceTypeName = nameof(InsureType.汽車險);
                        break;
                    case "火險":
                        job.InsuranceTypeName = nameof(InsureType.住火險);
                        break;
                    case "":
                        job.InsuranceTypeName = nameof(InsureType.傷害險);
                        break;
                      


                }
                
                string decryptStr = Cryptography.AesCBCDecrypt(key, iv, memo.Token);
                job.ParentFormTypeId = parentWOType.Id;
                job.ParentFormTypeName = parentWOType.ItemName;
                job.FormTypeName = dictSource.ItemName;
                job.FormTypeId = dictSource.Id;
                job.StatusName = dictSource2.ItemName;
                job.StatusId = dictSource2.Id;
                job.SourceName = "MEMO案件編號";
                job.SourceId = caseInfo.inumber;
                job.Header = decryptStr;
                job.Body = caseInfo.nbusiness_prop; 
                job.Supplement = caseString;
                job.ApplicantPolicyNO = caseInfo.ipolicy;
                job.Itag = caseInfo.itag;
                
                job.ApplicantName = caseInfo.nnotify;
                job.ApplicantCTIPhone = caseInfo.itel_h_nnotify;
                job.ApplicantLocalPhone = caseInfo.itel_o_nnotify;
                job.ApplicantMobilePhone = caseInfo.itel_m_nnotify;
                job.CaseOpenTime = DateTime.Now;
                job.CaseCloseTime = DateTime.Now.AddHours(limitHours);
                job.DeleteMark = false;
                if (caseInfo.nnotify == caseInfo.nassured)
                {
                    job.SetAccountStatus = true;
                    job.SetAccountRelation = "被保人";
                    job.ApplicantId = caseInfo.iassured;
                    job.SetAccountStatusDesc =  DateTime.Now.ToString("yyyy/MM/dd") + "歸戶"; 
                }
                if (caseInfo.nnotify == caseInfo.napplicant)
                {
                    job.SetAccountStatus = true;
                    job.SetAccountRelation = "要保人";
                    job.ApplicantId = caseInfo.iapplicant;
                    job.SetAccountStatusDesc = DateTime.Now.ToString("yyyy/MM/dd") + "歸戶";
                }
                else
                {
                    job.SetAccountStatus = false;
                    job.SetAccountRelation = "";
                    job.SetAccountStatusDesc = ErrCode.err70001 ;
                }
                
                lstJobs.Add(job);
            }
            

            return lstJobs;
        }
        public List<WorkOrder> ImportOutBound(string OutBoundId)
        {
            List<WorkOrder> outBoundList = new List<WorkOrder>();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            
            AppSetting sysSetting =System.Text.Json.JsonSerializer.Deserialize<AppSetting>(AurocoreCacheHelper.Get("SysSetting").ToJson());
            if (sysSetting != null)
            {
               

            }
            else
            {
              
            }
            return outBoundList;
        }
        public List<WorkOrderMenu> GetWorkOrderMenuInfo(string menuSql)
        {
            List<WorkOrderMenu> workOrderMenu = null;
            if (!string.IsNullOrEmpty(menuSql))
            {
                workOrderMenu =  DapperConn.Query<WorkOrderMenu>(menuSql).ToList();
            }
            

            return workOrderMenu;
        }
        /// <summary>
        /// 建立進線工作案件
        /// </summary>
        /// <returns></returns>
        public WorkOrder CreateSingleOrder(string ItemCode)
        {
            if (HasInjectionData(ItemCode))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", ItemCode));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(ItemCode))
            {
                ItemCode = "1";
            }

            string selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemCode='{0}'", ItemCode);
            ItemsDetail dictSource = DapperConn.QueryFirstOrDefault<ItemsDetail>(selectSql);
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemCode='{0}'", nameof(WorkflowStatus.等待分配));
            ItemsDetail dictSource2 = DapperConn.QueryFirstOrDefault<ItemsDetail>(selectSql);
            string formType = dictSource.ItemName;
            WorkOrder job = new WorkOrder();
            job.FormTypeName = dictSource.ItemName;
            job.FormTypeId = dictSource.Id;
            job.StatusName = dictSource2.ItemName;
            job.StatusId = dictSource2.Id;
            job.CaseOpenTime = DateTime.Now;
            job.DeleteMark = false;
            job.SetAccountStatus = false;
            job.SetAccountRelation = "";
            job.SetAccountStatusDesc = ErrCode.err70003;
            return job;
        }

        public List<WorkOrder> ImportEstimate(string[] estimateIds, string estimateType, string currentUser)
        {
            string sql = "SELECT * FROM BTC_Estimate WHERE EstimateId  IN @estimateIds And IsCheckCase=0";
            List<Vehicle_Estimate> estimates = DapperConn.Query<Vehicle_Estimate>(sql, new { estimateIds = estimateIds }).ToList();

            List<WorkOrder> lstJobs = new List<WorkOrder>();
            string selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemCode='案件分類' AND ItemName='{0}'", nameof(WorkOrderType.汽車險案件));
            ItemsDetail parentWOType = DapperConn.Query<ItemsDetail>(selectSql).FirstOrDefault();
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ParentId ='{0}'", parentWOType.Id);
            List<ItemsDetail> dictSourceList = DapperConn.Query<ItemsDetail>(selectSql).ToList();
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemName='{0}'", nameof(WorkflowStatus.等待分配));
            ItemsDetail dictSource2 = DapperConn.QueryFirstOrDefault<ItemsDetail>(selectSql);
            selectSql = string.Format("SELECT * FROM Sys_ItemsDetail WHERE ItemName='{0}'", nameof(WorkflowStatus.等待分配));
            try
            {

                foreach (Vehicle_Estimate estimate in estimates)
                {
                    /* 先不做 不知道 車險子分類在資料模型的那部分
                    VehicleFormType  = (VehicleFormType)Enum.Parse(typeof(VehicleFormType), estimate., true);
                    string f = memoFormType.ToString();
                    var dictSource = dictSourceList.Where(d => d.ItemName == f).SingleOrDefault();
                    */
                    WorkOrder job = new WorkOrder();


                    bool parseSuccess = Int32.TryParse(parentWOType.ItemProperty1, out int limitHours);
                    if (!parseSuccess)
                    {
                        Log4NetHelper.Error(DateTime.Now + " : 轉換 要保 天數失敗");
                        throw new Exception("轉換 要保 天數失敗");

                    }

                    job.ParentFormTypeId = parentWOType.Id;
                    job.ParentFormTypeName = parentWOType.ItemName;
                    job.FormTypeId = "2204217392665";
                    job.FormTypeName = "單強";

                    job.StatusName = dictSource2.ItemName;
                    job.StatusId = dictSource2.Id;
                    job.SourceName = "車險要保書 Id";
                    job.SourceId = estimate.EstimateId;
                    job.Header = "車險要保";


                    job.Itag = estimate.TagId;
                    job.ApplicantId = estimate.ApplicantID;
                    job.ApplicantName = estimate.ApplicantName;
                    job.Attachment = estimate.Attachment;
                    job.ApplicantLocalPhone = estimate.ApplicantTelDay;
                    job.ApplicantMobilePhone = estimate.ApplicantMobilPhone;
                    job.CaseOpenTime = DateTime.Now;
                    job.CaseCloseTime = DateTime.Now.AddHours(limitHours);
                    job.DeleteMark = false;
                    job.SetAccountStatus = false;
                    job.SetAccountRelation = "要保人";
                    job.SetAccountStatusDesc = $"{DateTime.Now.Year}/{DateTime.Now.Month}/{DateTime.Now.Day} 歸戶";
                    job.CreatorUserId = currentUser;
                    lstJobs.Add(job);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());

            }


            return lstJobs;
        }
        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        public  async Task<List<object>> FindWithPagerRelationWorkOrderDetailAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始記錄
            int endNum = info.CurrenetPageIndex * info.PageSize;//結束記錄
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select t1.{0} FROM {1} t1 left join (SELECT WorkOrderId,CurrentEditorId FROM CRM_WorkOrderDetail WHERE  ResponseTypeName='核保審批') t2 on t1.Id = t2.WorkOrderId where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM (SELECT ROW_NUMBER() OVER (order by  {0}) AS rows ,t1.{1},t2.CurrentEditorId as CurrentEditorName  FROM {2} t1 left join (SELECT WorkOrderId,CurrentEditorId FROM CRM_WorkOrderDetail WHERE  ResponseTypeName='核保審批')  t2 on t1.Id = t2.WorkOrderId " +
                "where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);

            var reader = await DapperConn.QueryMultipleAsync(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            List<object> list = reader.Read<object>().AsList();
            return list;
        }
    }
}