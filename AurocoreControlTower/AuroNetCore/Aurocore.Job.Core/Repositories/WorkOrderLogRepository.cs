using System;
using Dapper;
using System.Linq;
using System.Collections.Generic;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.Repositories
{
	/// <summary>
	/// 倉儲接口的實現
	/// </summary>
	public class WorkOrderLogRepository : BaseRepository<WorkOrderLog, string>, IWorkOrderLogRepository
	{
		public WorkOrderLogRepository()
		{
		}

		public WorkOrderLogRepository(IDbContextCore context) : base(context)
		{
		}

		public List<WorkOrderLogBrief> GetWorkOrderLogBrief(string sql)
		{
			List<WorkOrderLogBrief> orderLogBrief = null;
			if (!string.IsNullOrEmpty(sql))
			{
				orderLogBrief = DapperConn.Query<WorkOrderLogBrief>(sql).ToList();
			}


			return orderLogBrief;
		}

		
	}
}