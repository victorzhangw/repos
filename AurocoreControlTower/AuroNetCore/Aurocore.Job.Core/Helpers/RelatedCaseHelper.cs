﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.WorkOrders.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Dapper;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Commons.Core.Dapper;
using Aurocore.Commons.Core.DataManager;
using System.Data.Common;
using Dapper;
using Aurocore.Commons.Log;
using System.Threading;


namespace Aurocore.WorkOrders.Helpers
{
	/// <summary>
	/// 建立相關單據（號碼連動案件編號）
	/// </summary>
	
	public sealed class RelatedCaseHelper
	{
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="workOrderDetail"> 單據實體</param>
		/// <param name="retryCount">重試次數</param>
		/// <param name="waitSecond">等待時間</param>
		/// <returns></returns>
		public static async Task<int> CreateForm(WorkOrderDetail workOrderDetail, int retryCount=3 , int waitSecond = 1)
		{

			int recordCount = 0;
			if (!string.IsNullOrEmpty(workOrderDetail.Id))
			{
				workOrderDetail.Id = string.Empty;
			}
			var newDateTime = (DateTime)workOrderDetail.CreatorTime;
			string createDateTime = newDateTime.ToString("yyyy-MM-dd HH:mm:ss");
			string insertSql = @$"INSERT INTO CRM_WorkOrderDetail (
			[Id],
			[WorkOrderId],
			[ResponseTypeId],
			[ResponseTypeName],
			[InsuranceTypeId],
			[InsuranceTypeName],
			[CaseCategoryLayer1Id],
			[CaseCategoryLayer1Name],
			[CaseCategoryLayer2Id],
			[CaseCategoryLayer2Name],
			[CustomerName],
			[PolicyNo],
			[RelatedPolicyNo],
			[Timeliness],
			[StatusId],
			[StatusName],
			[CoDeptId],
			[CoOwnerId],
			[CoOwnerName],
			[Header],
			[Body],
			[CurrentEditorId],
			[FormLog],
			[Attachment],
			[ContactTime],
			[ContactId],
			[ContactName],
			[ContactEmail],
			[ContactPhone],
			[ContactRelation],
			[ContactStatus],
			[ContactStatusDesc],
			[PlateNumber],
			[AnswerTime],
			[EnabledMark],
			[DeleteMark],
			[CreatorTime],
			[CreatorUserId],
			[CreatorUserName],
			[CaseCategoryLayer3Id],
			[CaseCategoryLayer3Name],
			[ContactSpecialNotes],
			[IsNotified] 

			 )
			SELECT
			'{workOrderDetail.WorkOrderId}'+'-'+convert(varchar,t.maxId), 
			'{workOrderDetail.WorkOrderId}',
			'{workOrderDetail.ResponseTypeId}',
			'{workOrderDetail.ResponseTypeName}',
			'{workOrderDetail.InsuranceTypeId}',
			'{workOrderDetail.InsuranceTypeName}',
			'{workOrderDetail.CaseCategoryLayer1Id}',
			'{workOrderDetail.CaseCategoryLayer1Name}',
			'{workOrderDetail.CaseCategoryLayer2Id}',
			'{workOrderDetail.CaseCategoryLayer2Name}',
			'{workOrderDetail.CustomerName}',
			'{workOrderDetail.PolicyNo}',
			'{workOrderDetail.RelatedPolicyNo}',
			'{workOrderDetail.Timeliness}',
			'{workOrderDetail.StatusId}',
			'{workOrderDetail.StatusName}',
			'{workOrderDetail.CoDeptId}',
			'{workOrderDetail.CoOwnerId}',
			'{workOrderDetail.CoOwnerName}',
			'{workOrderDetail.Header}',
			'{workOrderDetail.Body}',
			'{workOrderDetail.CurrentEditorId}',
			'{workOrderDetail.FormLog}',
			'{workOrderDetail.Attachment}',
			'{workOrderDetail.ContactTime}',
			'{workOrderDetail.ContactId}',
			'{workOrderDetail.ContactName}',
			'{workOrderDetail.ContactEmail}',
			'{workOrderDetail.ContactPhone}',
			'{workOrderDetail.ContactRelation}',
			'{workOrderDetail.ContactStatus}',
			'{workOrderDetail.ContactStatusDesc}',
			'{workOrderDetail.PlateNumber}',
			'{workOrderDetail.AnswerTime}',
			'{workOrderDetail.EnabledMark}',
			'{workOrderDetail.DeleteMark}',
			'{createDateTime}',
			'{workOrderDetail.CreatorUserId}',
			'{workOrderDetail.CreatorUserName}',
			'{workOrderDetail.CaseCategoryLayer3Id}',
			'{workOrderDetail.CaseCategoryLayer3Name}',
			'{workOrderDetail.ContactSpecialNotes}',
			'{workOrderDetail.IsNotified}' 
			from 
			(SELECT count(Id)+1 as maxId from CRM_WorkOrderDetail WHERE LEFT(Id, 
			case 
			when CHARINDEX ('-' , Id)-1 >0 then CHARINDEX ('-' , Id)-1
			when CHARINDEX ('-' , Id)-1 <=0 then 0
			end)='{workOrderDetail.WorkOrderId}') AS t";
			try
			{
				using (DbConnection conn = OpenSharedConnectionHelper.OpenConnection())
				{
					while (true)
					{
						try
						{
							recordCount = await conn.ExecuteAsync(insertSql);
							if (recordCount > 0) {
								break;

							}
							
						}
						catch
						{
							if (--retryCount == 0)
							{
								Log4NetHelper.Error("新增單據異常,已達新增次數上限");
								throw;
							}

							var seconds = TimeSpan.FromSeconds(waitSecond);
							Thread.Sleep(seconds);
						}
					}
					
				}



			}
			catch (Exception e)
			{

				Log4NetHelper.Error("新增單據異常", e);

			}
			return recordCount;
		}
		
	}
}
