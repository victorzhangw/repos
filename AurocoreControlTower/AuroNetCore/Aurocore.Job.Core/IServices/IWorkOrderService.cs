using System;
using Aurocore.Commons.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.Email.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Aurocore.Commons.Pages;

namespace Aurocore.WorkOrders.IServices
{
    /// <summary>
    /// 定義案件服務接口
    /// </summary>
    public interface IWorkOrderService:IService<WorkOrder,WorkOrderOutputDto,string>
    {
        Task<List<string>> RetrieveEmail(string ItemCode, IWebHostEnvironment hostingEnvironment);
        Task<List<string>> ImportMemo(Memo memo);
        Task<List<string>> ImportEstimate(string[] Ids,string type,string currentUser);
        Task<bool> OutBoundListtoWorkOrder(string OutBoundId, string userId,string FormTypeId,string FormTypeName,DateTime CaseCloseTime);
        List<WorkOrderTreeOutputDto>WorkOrderTree(string WorkOrderId);
        Task<WorkOrderRealateList> WorkOrderTypeTree(string WorkOrderId);
        WorkOrderListTableSchema GetworkOrderListTableHeader();
        Task <string> CreateSingleOrder(string ItemCode);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns>ָļ</returns>
        Task<PageResult<WorkOrderOutputDto>> FindWithPagerSearchAsync(SeachWorkOrder search);
        

    }
    
}
