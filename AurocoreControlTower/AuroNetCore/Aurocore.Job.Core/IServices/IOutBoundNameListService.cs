using System;
using Aurocore.Commons.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.Email.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace Aurocore.WorkOrders.IServices
{
    /// <summary>
    /// 定義案件服務接口
    /// </summary>
    public interface IOutBoundNameListService:IService<OutBoundDbNameList,OutBoundDbNameListOutputDto,string>
    {
         Task<int> AddNameLists(List<OutBoundDbNameList> outBoundDbNameList);
    }
}
