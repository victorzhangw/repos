using System;
using Aurocore.Commons.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface IWorkOrderLogService:IService<WorkOrderLog,WorkOrderLogOutputDto, string>
    {

    }
}
