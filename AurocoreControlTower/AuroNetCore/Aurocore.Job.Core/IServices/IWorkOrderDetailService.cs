using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.IServices;
using Aurocore.Commons.Pages;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface IWorkOrderDetailService:IService<WorkOrderDetail,WorkOrderDetailOutputDto, string>
    {
        bool MergeEndorsement(EndorsementForm endorsementForm, string filePath);
        string MergeReissue(ReissueForm reissueForm, string filePath);
        Task<PageResult<WorkOrderDetailOutputDto>> FindWithPagerSearchAsync(SearchWorkOrderDetail dsearch, SeachWorkOrder search);
        Task<WorkOrderRealateList> WorkOrderDetailTypeTree(string Id);
        Task<PageResult<WorkOrderDetailOutputDto>> FindWorkOrderDerailsWithPagerAsync(SearchWorkOrderDetail search);
        /// <summary>
        /// 建立單一工作單
        /// </summary>
        /// <param name="workOrderDetail"></param>
        /// <param name="retryCount">重試次數</param>
        /// <param name="waitSecond">等待時間</param>
        /// <returns></returns>
        Task<long> CreateSingleWorkOrderDetail(WorkOrderDetail workOrderDetail,int retryCount=3 , int waitSecond=1 );
    }
}
