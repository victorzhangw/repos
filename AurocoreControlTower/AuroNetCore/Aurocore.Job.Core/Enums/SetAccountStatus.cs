﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Enums
{
    public enum SetAccountStatus
    {
       已歸戶=0,
       未歸戶=1
    }
    public enum SetAccountRealation
    {
        聯絡人 = 0,
        被保人 = 1,
        要保人=2,
        意向客戶=3
    }
}
