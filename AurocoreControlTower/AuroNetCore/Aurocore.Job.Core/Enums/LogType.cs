﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Enums
{
    public enum LogType
    {
        建立單據=0,
        修改單據=1,
        回覆訊息=2,
        發送訊息=3,
        結案單據=4,
        客戶聯繫=5,
        回覆MEMO=6,
        管理者備註=7,
        審核備註=8

    }
    public enum LogMessageType
    {
        外部聯繫 = 0,
        內部聯繫 = 1,
        不分=2 //如結案

    }
}
 