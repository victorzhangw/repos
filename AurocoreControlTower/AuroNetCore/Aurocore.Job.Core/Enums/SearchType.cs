﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Enums
{
    public enum SearchType
    {
        /// <summary>
        /// 全部案件
        /// </summary>
        allcase = 0,
        /// <summary>
        /// 個人案件
        /// </summary>
        personalcase = 1
        
    }
}
