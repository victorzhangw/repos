﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Enums
{
    public enum WorkOrderType
    {
        /// <summary>
        /// 郵件
        /// </summary>
        EzGo = 0,
        /// <summary>
        /// 傳真
        /// </summary>
        EFax,
        
        /// <summary>
        /// 進線(話後)
        /// </summary>
        InBound,
        /// <summary>
        /// 外撥
        /// </summary>
        OutBound,
        /// <summary>
        /// MEMO 單
        /// </summary>
        Memo,
        /// <summary>
        /// 虛擬項目-未結案單據 
        /// </summary>
        待辦單據,
        /// <summary>
        /// 虛擬項目-全部案件
        /// </summary>
        全部案件,
        /// <summary>
        /// 虛擬項目-個人案件 
        /// </summary>
        個人案件,
        /// <summary>
        /// MEMO-子項目 
        /// </summary>
        理賠,
        /// <summary>
        /// MEMO-子項目 
        /// </summary>
        營業,
        /// <summary>
        /// MEMO-子項目 
        /// </summary>
        其他,
        /// <summary>
        /// MEMO-子項目 
        /// </summary>
        批改,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        新投保電訪＿汽車險,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        新投保電訪＿機車險,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        新投保電訪＿傷害險,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        新投保電訪＿住火險,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        要保書追蹤,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        續保關懷,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        住火重複投保,
        /// <summary>
        /// OutBound-子項目 
        /// </summary>
        肇責異動,
        /// <summary>
        ///  汽車險
        /// </summary>
        汽車險案件,
        /// <summary>
        /// 汽車險-子項目
        /// </summary>
        車責,
        /// <summary>
        /// 汽車險-子項目
        /// </summary>
        單強,
        /// <summary>
        ///汽車險-子項目
        /// </summary>
        車體


    }
    public enum WorkOrderTypeforBulletin
    {
        /// <summary>
        /// 郵件
        /// </summary>
        EzGo = 0,
        /// <summary>
        /// 傳真
        /// </summary>
        EFax,

        /// <summary>
        /// 進線(話後)
        /// </summary>
        InBound,
        /// <summary>
        /// 外撥
        /// </summary>
        OutBound,
        /// <summary>
        /// MEMO 單
        /// </summary>
        Memo,
        /// <summary>
        /// 虛擬項目-未結案單據 
        /// </summary>
        待辦單據,
        /// <summary>
        /// 虛擬項目-全部案件
        /// </summary>
        全部案件,
        /// <summary>
        /// 虛擬項目-個人案件 
        /// </summary>
        個人案件
        

    }
}
