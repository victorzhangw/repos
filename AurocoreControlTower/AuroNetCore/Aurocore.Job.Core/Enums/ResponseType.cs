﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Enums
{
    public enum ResponseType
    {
        /// <summary>
        /// 收發至企業內部郵件
        /// </summary>
        InternalEMail = 0,
        /// <summary>
        /// 收發來自企業外部郵件
        /// </summary>
        ExternalEMail = 1,
        /// <summary>
        /// 發送企業內簡訊
        /// </summary>
        InternalSMS = 2,
        /// <summary>
        /// 發送至外部簡訊
        /// </summary>
        ExternalSMS = 3,
        /// <summary>
        /// 接受企業外部發送傳真郵件
        /// </summary>
        ExternalSendedFaxMail = 4,
        /// <summary>
        /// 話務記錄
        /// </summary>
        話務記錄 = 5,
        一般問題單 =6,
        批改申請單=7,
        補發申請單=8,
        信封套印=9,
        /// <summary>
        /// 回覆 Memo 至原始發單單位
        /// </summary>
        ReplyMemotoSender = 10,
        /// <summary>
        /// 回覆 Memo 至原始發單單位
        /// </summary>
        待辦單據 = 11,
        /// <summary>
        /// 核保
        /// </summary>
        核保審批 = 12,
        Inbound話務記錄=13,
        OutBound話務記錄=14,


    }
   
}
