﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Enums
{
    public enum WorkflowStatus
    {
        等待分配,
        處理中,
        協辦中,
        已結案,
        核保審批中,
        全部狀態
    }
}
