using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using Aurocore.Commons.Helpers;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 案件，數據實體物件
    /// </summary>
    [Table("CRM_WorkOrder")]
    [Serializable]
    public class WorkOrder: BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        public WorkOrder() {
            this.Id = GuidUtils.CreateShortNo();
            this.EnabledMark = true;
            this.DeleteMark = false;
            this.CreatorTime = DateTime.Now;


        }
        /// <summary>
        ///  上階案件分類Id
        /// </summary>
        public string ParentFormTypeId { get; set; }
        /// <summary>
        ///  上階案件分類
        /// </summary>
        public string ParentFormTypeName { get; set; }
        /// <summary>
        /// 工作單類型 Id
        /// </summary>
        public string FormTypeId { get; set; }
        /// <summary>
        /// 工作單名稱
        /// </summary>
        public string FormTypeName { get; set; }
        /// <summary>
        /// 來源唯一識別碼
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 來源名稱
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        ///  狀態碼編號
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// 狀態碼
        /// </summary>
        public string StatusName { get; set; }


        /// <summary>
        /// 開單日
        /// </summary>
        public DateTime? CaseOpenTime { get; set; }

        /// <summary>
        /// 截單日
        /// </summary>
        public DateTime? CaseCloseTime { get; set; }

        /// <summary>
        /// 主辦者姓名
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// 主辦者Id
        /// </summary>
        public string OwnerId { get; set; }
        /// <summary>
        /// 協辦者姓名
        /// </summary>
        public string CoOwnerName { get; set; }

        /// <summary>
        /// 協辦者Id
        /// </summary>
        public string CoOwnerId { get; set; }
        /// <summary>
        /// 案件第一層分類
        /// </summary>
        public string CaseCategoryLayer1Name { get; set; }

        /// <summary>
        /// 案件第一層分類Id
        /// </summary>
        public string CaseCategoryLayer1Id { get; set; }
        /// <summary>
        /// 案件第二層分類
        /// </summary>
        public string CaseCategoryLayer2Name { get; set; }

        /// <summary>
        /// 案件第二層分類Id
        /// </summary>
        public string CaseCategoryLayer2Id { get; set; }
        /// <summary>
        ///  申請者（要保人） Id
        /// </summary>
        public string ApplicantId { get; set; }

        /// <summary>
        ///  申請者（要保人）名稱
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        ///  申請者（要保人）行動電話
        /// </summary>
        public string ApplicantMobilePhone { get; set; }
        /// <summary>
        ///  申請者市話
        /// </summary>
        public string ApplicantLocalPhone { get; set; }
        /// <summary>
        ///  申請者 CTI 進線電話
        /// </summary>
        public string ApplicantCTIPhone { get; set; }

        /// <summary>
        ///  申請者EMail
        /// </summary>
        public string ApplicantEmail { get; set; }

        /// <summary>
        ///  申請者地址
        /// </summary>
        public string ApplicantAddress { get; set; }
        /// <summary>
        ///  申請者保單
        /// </summary>
        public string ApplicantPolicyNO { get; set; }
        /// <summary>
        ///  申請者保單類別
        /// </summary>
        public string InsuranceTypeName { get; set; }
        /// <summary>
        ///  車牌號碼
        /// </summary>
        public string Itag { get; set; }
        
        /// <summary>
        ///  歸戶關係
        /// </summary>
        public string SetAccountRelation { get; set; }
        /// <summary>
        ///  歸戶狀態  已歸戶:0,  未歸戶:1
        /// </summary>
        public bool? SetAccountStatus { get; set; }
        /// <summary>
        ///  歸戶說明
        /// </summary>
        public string SetAccountStatusDesc { get; set; }

        /// <summary>
        ///  單據頭部
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        ///  單據本體
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        ///  補充資料
        /// </summary>
        public string Supplement  { get; set; }
        /// <summary>
        ///  附檔
        /// </summary>
        public string Attachment { get; set; }

        /// <summary>
        ///  重要性
        /// </summary>
        public bool? IsHot { get; set; }

        /// <summary>
        ///  通知
        /// </summary>
        public bool? IsNotified { get; set; } 

        /// <summary>
        ///  結案碼 0：未結案 1：已結案 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  刪除碼
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        ///  建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        ///  建立者 Id
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        ///  公司ID
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  刪除者Id
        /// </summary>
        public string DeleteUserId { get; set; }
        /// <summary>
        ///  是否閱讀
        /// </summary>
        public string IsRead { get; set; }

    }
}
