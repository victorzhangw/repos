﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 批改與補發申請書
    /// </summary>
    public class EndorsementForm
    {
        /// <summary>
        /// 單據編號
        /// </summary>
        public string WorkOrderDetailId{get;set;}
        /// <summary>
        /// 報險種類
        /// </summary>
        public string InsureTypeName { get;set;}
        /// <summary>
        /// 檔案路徑
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 範本文字
        /// </summary>
        public string BeforeInsertText { get; set; }
    }
    public class ReissueForm
    {
        /// <summary>
        /// 單據編號
        /// </summary>
        public string WorkOrderDetailId { get; set; }
        /// <summary>
        /// 報險種類
        /// </summary>
        public string InsureTypeName { get; set; }
        /// <summary>
        /// 檔案路徑
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 申請人姓名
        /// </summary>
        public string ApplicantName { get; set; }
        /// <summary>
        ///  申請人Id
        /// </summary>
        public string ApplicantId { get; set; }
        /// <summary>
        /// 車牌號碼
        /// </summary>
        public string Itag { get; set; }
        /// <summary>
        /// 補發理由
        /// </summary>
        public string ReissueReson { get; set; }
        /// <summary>
        /// 遞送方式
        /// </summary>
        public DeliveryInfo deliveryInfo { get; set; }
    }
    /// <summary>
    /// 遞送所需資料
    /// </summary>
    public class DeliveryInfo
    {
        /// <summary>
        /// 遞送方式
        /// </summary>
        public string DeliveryType { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string Recipient { get; set; }
        /// <summary>
        /// 收件地址
        /// </summary>
        public string Address { get; set; }
    }
}
