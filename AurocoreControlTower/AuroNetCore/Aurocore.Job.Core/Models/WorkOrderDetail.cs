using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using System.ComponentModel.DataAnnotations;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 案件單據資料
    /// </summary>
    [Table("CRM_WorkOrderDetail")]
    [Serializable]
    public class WorkOrderDetail:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {

        /// <summary>
        /// 案件編號
        /// </summary>
        public string WorkOrderId { get; set; }

        /// <summary>
        /// 回應方式代碼
        /// </summary>
        public string ResponseTypeId { get; set; }

        /// <summary>
        /// 回應方式名稱
        /// </summary>
        public string ResponseTypeName { get; set; }


        /// <summary>
        /// 保險類別 Id
        /// </summary>
        [Display(Name = "險種")]
        public string InsuranceTypeId { get; set; }

        /// <summary>
        /// 保險類別
        /// </summary>
        public string InsuranceTypeName { get; set; }


        /// <summary>
        /// 案件第一層類別Id
        /// </summary>
        [Display(Name = "問題分類")]
        public string CaseCategoryLayer1Id { get; set; }

        /// <summary>
        /// 案件第一層類別
        /// </summary>
        public string CaseCategoryLayer1Name { get; set; }

        /// <summary>
        /// 案件第二層類別Id
        /// </summary>
        [Display(Name = "子類型")]
        public string CaseCategoryLayer2Id { get; set; }

        /// <summary>
        /// 案件第二層類別
        /// </summary>
        public string CaseCategoryLayer2Name { get; set; }
        /// <summary>
        /// 案件第三層類別Id
        /// </summary>
        public string CaseCategoryLayer3Id { get; set; }

        /// <summary>
        /// 案件第三層類別
        /// </summary>
        public string CaseCategoryLayer3Name { get; set; }

        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }

        /// <summary>
        /// 關聯保單號碼
        /// </summary>
        public string RelatedPolicyNo { get; set; }

        /// <summary>
        /// 時限
        /// </summary>
        public DateTime? Timeliness { get; set; }

        /// <summary>
        /// 狀態碼Id
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// 狀態碼
        /// </summary>
        public string StatusName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ContactStatusDesc { get; set; }

        /// <summary>
        /// 協辦者Id
        /// </summary>
        public string CoOwnerId { get; set; }

        /// <summary>
        /// 協辦者
        /// </summary>
        public string CoOwnerName { get; set; }

        /// <summary>
        /// 主旨/簡述
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// 內文
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 目前單據擁有者
        /// </summary>
        public string CurrentEditorId { get; set; }

        /// <summary>
        /// 設定或獲取列印PDF 使用JSON儲存 
        /// </summary>
        public string FormLog { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        public string Attachment { get; set; }
        /// <summary>
        ///  協辦者部門Id
        /// </summary>
        public string CoDeptId { get; set; }
        /// <summary>
        /// 聯絡Id
        /// </summary>
        public string ContactId { get; set; }

        /// <summary>
        /// 聯絡姓名
        /// </summary>
        [Display(Name = "客戶姓名")]
        public string ContactName { get; set; }

        /// <summary>
        /// 聯絡郵件
        /// </summary>
        public string ContactEmail { get; set; }

        /// <summary>
        /// 聯絡電話
        /// </summary>
        [Display(Name = "聯絡電話")]
        public string ContactPhone { get; set; }
        /// <summary>
        /// 車牌號碼
        /// </summary>
        public string PlateNumber { get; set; }

        /// <summary>
        /// 客戶姓名
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 聯絡時間
        /// </summary>
        public DateTime? ContactTime { get; set; }
        /// <summary>
        /// 聯絡人關係
        /// </summary>
      
        [Display(Name = "客戶身份")]
        public string ContactRelation { get; set; }

        /// <summary>
        /// 聯絡狀態
        /// </summary>
        public string ContactStatus { get; set; }
        /// <summary>
        /// 聯絡者特殊備註（要寫回客戶資料庫）
        /// </summary>
        public string ContactSpecialNotes { get; set; }

        /// <summary>
        /// 進線時間
        /// </summary>
        public DateTime? AnswerTime { get; set; }

        /// <summary>
        /// 生效碼
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatorUserId { get; set; }
        /// <summary>
        ///  單據建立者姓名
        /// </summary>
        public string CreatorUserName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        ///  重要性
        /// </summary>
        public bool? IsHot { get; set; }
        /// <summary>
        ///  通知
        /// </summary>
        public bool? IsNotified { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DeleteUserId { get; set; }
        /// <summary>
        ///  是否閱讀
        /// </summary>
        public string IsRead { get; set; }

    }
}
