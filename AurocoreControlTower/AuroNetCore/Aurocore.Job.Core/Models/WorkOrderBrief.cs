﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Aurocore.Commons.Cache;
using Aurocore.Security.Models;
using Aurocore.Commons.Json;
namespace Aurocore.WorkOrders.Models
{
	[Serializable]
	public class WorkOrderBrief
	{
		
		/// <summary>
		/// 工作單編號
		/// </summary>
		public string Id { get; set; }

		/// <summary>
		/// 案件編號
		/// </summary>
		public string WorkOrderId { get; set; }

		/// <summary>
		/// 原始來源編號（如 memo 事件編號）
		/// </summary>
		public string SourceId { get; set; }
		/// <summary>
		/// 上層工作單Id
		/// </summary>
		public string ParentResponseTypeId { get; set; }
		/// <summary>
		/// 工作單
		/// </summary>
		public string ParentResponseTypeName { get; set; }
		/// <summary>
		/// 工作單Id
		/// </summary>
		public string ResponseTypeId { get; set; }
		/// <summary>
		/// 工作單
		/// </summary>
		public string ResponseTypeName { get; set; }

	

		/// <summary>
		/// 
		/// </summary>
		public string InsuranceTypeName { get; set; }


		/// <summary>
		/// 
		/// </summary>
		public string CaseCategoryLayer1Name { get; set; }

	   

		/// <summary>
		/// 
		/// </summary>
		public string CaseCategoryLayer2Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string PolicyNo { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public string AnswerTime { get; set; }
		/// <summary>
		/// 
		/// </summary>
		public DateTime? Timeliness { get; set; }


		/// <summary>
		/// 
		/// </summary>
		public string StatusName { get; set; }

		/// <summary>
		/// 協辦
		/// </summary>
		public string CoOwnerName { get; set; }

		/// <summary>
		/// 主旨
		/// </summary>
		public string Header { get; set; }

		/// <summary>
		/// 本文
		/// </summary>
		public string Body { get; set; }

		/// <summary>
		/// 頁尾
		/// </summary>
		public string Footer { get; set; }

		/// <summary>
		/// 建立時間
		/// </summary>
		public DateTime? CreatorTime { get; set; }
		public string ContactName { get; set; }
		public string ContactEmail { get; set; }
		public string ContactPhone { get; set; }
		public string ContactStatus { get; set; }
		public string ContactStatusDesc { get; set; }
		public string Attachment { get; set; }
		/// <summary>
		/// 作者
		/// </summary>
		public string RealName { get; set; }
		
		

		

	}
	public class WorkOrderLogBrief
	{
		/// <summary>
		/// Log Id
		/// </summary>
		public string Id { get; set; }
		/// <summary>
		/// 設定或獲取子工作單編號
		/// </summary>
		public string WorkOrderDetailId { get; set; }

		/// <summary>
		/// 設定或獲取回覆狀態(0:reject 1:approve)
		/// </summary>
		public bool? ReplyStatus { get; set; }

		/// <summary>
		/// 設定或獲取回覆訊息
		/// </summary>
		public string ReplyMessage { get; set; }

  
   

		/// <summary>
		/// 設定或獲取建立時間
		/// </summary>
		public DateTime? CreatorTime { get; set; }

		/// <summary>
		/// 作者
		/// </summary>
		public string RealName { get; set; }

	   
	}
	public class WorkOrderRealateList
	{
		public Dictionary<string,string> FormType { get; set; }
		/// <summary>
		/// 聯絡記錄
		/// </summary>
		public List<ContactHistory> ContactHistory { get; set; }
		/// <summary>
		/// 開單記錄
		/// </summary>
		public List<CreateDetailHistory> CreateDetailHistory { get; set; }
		/// <summary>
		/// 回報記錄
		/// </summary>
		public List<ResponMemoHistory> ResponMemoHistory { get; set; }
		/// <summary>
		/// 處理記錄
		/// </summary>
		public List<DetailProcessHistory> DetailProcessHistory { get; set; }
	}
	
	public class ContactHistory
	{
		public ContactHistory()
		{
			this.ColumnHead = new string[] { "聯絡時間", "聯絡方式", "負責人", "收件人郵箱", "收件人電話", "主旨","聯繫結果","聯繫狀況","附件" };
		}
		private string attachStr = string.Empty;
		/// <summary>
		/// 表格標題
		/// </summary>
		public string[] ColumnHead { get; set; }
		/// <summary>
		/// 聯絡時間
		/// </summary>
		public string ContactTime { get; set; }
		/// <summary>
		/// 聯絡方式
		/// </summary>
		public string ContactType { get; set; }
		/// <summary>
		/// 負責人
		/// </summary>
		public string CaseOwner { get; set; }

		/// <summary>
		/// 收件人郵箱
		/// </summary>
		public string ContactEmail { get; set; }
		/// <summary>
		/// 收件人電話
		/// </summary>
		public string ContactPhone { get; set; }
		/// <summary>
		/// 主旨
		/// </summary>
		public string Header { get; set; }
		/// <summary>
		/// 聯繫結果
		/// </summary>
		public string ContactStatus { get; set; }
		/// <summary>
		/// 聯繫狀況
		/// </summary>
		public string ContactStatusDesc { get; set; }
		/// <summary>
		/// 附件
		/// </summary>
		public string Attachment {
			set { attachStr = value; }
			get
			{
				List<string> _tmpList = new List<string>();
				if (!string.IsNullOrEmpty(attachStr))
				{
					var attachAry = attachStr.Split(",");

					AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
					SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
					foreach (var i in attachAry)
					{
						_tmpList.Add(sysSetting.WebUrl + "/" + i);
					}

				}


				return string.Join(",", _tmpList);
			}
		}


	}
	public class CreateDetailHistory
	{
		public CreateDetailHistory()
		{
			this.ColumnHead = new string[] { "開單時間", "開單類別Id","開單類別", "開單人", "協辦人", "開單編號", "狀態", "簡述", "附件" };
		}
		private string attachStr = string.Empty;
		/// <summary>
		/// 表格標題
		/// </summary>
		public string[] ColumnHead { get; set; }
		/// <summary>
		/// 開單時間
		/// </summary>
		public string DetailOpenTime { get; set; }
		/// <summary>
		/// 上層開單類別Id
		/// </summary>
		public string ParentDetailTypeId { get; set; }
		/// <summary>
		/// 上層開單類別
		/// </summary>
		public string ParentDetailType { get; set; }
		/// <summary>
		/// 開單類別Id
		/// </summary>
		public string DetailTypeId { get; set; }
		/// <summary>
		/// 開單類別
		/// </summary>
		public string DetailType { get; set; }
		/// <summary>
		/// 開單人
		/// </summary>
		public string CreatorName { get; set; }
		/// <summary>
		/// 協辦人
		/// </summary>
		public string CoOwnerName { get; set; }
		/// <summary>
		/// 開單編號
		/// </summary>
		public string DetailId { get; set; }
		/// <summary>
		/// 狀態
		/// </summary>
		public string DetailIdCurrentStatus { get; set; }
		/// <summary>
		/// 簡述
		/// </summary>
		public string DetailDesc { get; set; }
		/// <summary>
		/// 附件
		/// </summary>
		public string Attachment {
			set { attachStr = value; }
			get
			{
				List<string> _tmpList = new List<string>();
				if (!string.IsNullOrEmpty(attachStr))
				{
					var attachAry = attachStr.Split(",");

					AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
					SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
					foreach (var i in attachAry)
					{
						_tmpList.Add(sysSetting.WebUrl + "/" + i);
					}

				}


				return string.Join(",", _tmpList);
			}
		}

	}
	public class ResponMemoHistory
	{
		public ResponMemoHistory()
		{
			this.ColumnHead = new string[] { "回報時間", "事故編號", "聯絡人", "客戶名稱", "案件分類", "簡述(其他)" };
		}
		public string[] ColumnHead { get; set; }
		/// <summary>
		/// 回報時間
		/// </summary>
		public string DetailOpenTime { get; set; }
		/// <summary>
		/// 事故編號
		/// </summary>
		public string inumber { get; set; }
		/// <summary>
		/// 聯絡人
		/// </summary>
		public string CreatorName { get; set; }
		/// <summary>
		/// 協辦人
		/// </summary>
		public string CustomerName { get; set; }
		/// <summary>
		/// 案件分類
		/// </summary>
		public string CaseTypeName { get; set; }
		/// <summary>
		/// 簡述
		/// </summary>
		public string DetailDesc { get; set; }
	}

	public class DetailProcessHistory
	{
		public DetailProcessHistory()
		{
			this.ColumnHead = new string[] { "時間", "人員", "簡述", "回覆", "處理狀態" };
		}
		public string[] ColumnHead { get; set; }
		/// <summary>
		/// 回報時間
		/// </summary>
		public string DetailOpenTime { get; set; }
		
		/// <summary>
		/// 聯絡人
		/// </summary>
		public string CreatorName { get; set; }
		
		/// <summary>
		/// 簡述
		/// </summary>
		public string DetailDesc { get; set; }
		/// <summary>
		/// 回覆
		/// </summary>
		public string Header { get; set; }
		/// <summary>
		/// 處理狀態
		/// </summary>
		public string FormStatus { get; set; }
	}

}
