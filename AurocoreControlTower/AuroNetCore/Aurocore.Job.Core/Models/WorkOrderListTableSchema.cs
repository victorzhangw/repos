﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    public  class WorkOrderListTableSchema
    {
        [JsonInclude]
        public  MemoHeader MemoHeader;
        [JsonInclude]
        public  EzGoHeader EzGoHeader;
        [JsonInclude]
        public  EFaxHeader EFaxHeader;
        [JsonInclude]
        public  TodoOrderHeader TodoOrderHeader;
        [JsonInclude]
        public  InboundHeader InboundHeader;
    }
    public class Header
    {
        public  Header(){
            this.Id = "案件編號";
            this.insurance_type = "險別";
            this.ApplicantId = "要保人ID";
            this.ApplicantName = "要保人姓名";
            this.CaseTypeLayer1Name = "案件類型";
            this.CaseTypeLayer2Name = "子分類";
            this.CreatorTime = "進件時間";
            this.StatusName = "處理狀態";
            this.OwnerName = "案件擁有者";
        }
        [JsonInclude]
        public string Id { get; set; }
        [JsonInclude]
        public string insurance_type { get; set; }
        [JsonInclude]
        public string ApplicantId { get; set; }
        [JsonInclude]
        public string ApplicantName { get; set; }
        public string CaseTypeLayer1Name { get; set; }
        public string CaseTypeLayer2Name { get; set; }
        public string CreatorTime { get; set; }
        public string StatusName { get; set; }
        public string OwnerName { get; set; }

    }
    public class MemoHeader:Header
    {
        public MemoHeader()
        {
            this.inumber = "MEMO單號";
            this.itag = "車號";
            this.nbusiness_prop = "業務屬性";
            this.nnotify = "通知人";
            this.nassured = "被保人姓名";
        }
        public string inumber { get; set; }
        public string itag { get; set; }
        public string nbusiness_prop { get; set; }
        public string nnotify { get; set; }
        public string nassured { get; set; }

}
    public class EzGoHeader : Header
    {
        public EzGoHeader()
        {
            this.ApplicantPolicyNO = "保單編號/訂單編號";
            this.itag = "車號";
            this.ApplicantEmail = "來信信箱";

        }
        public string ApplicantPolicyNO { get; set; }
        public string itag { get; set; }
        public string ApplicantEmail { get; set; }
 

    }
    public class EFaxHeader : Header
    {
        public EFaxHeader()
        {
            this.ApplicantPolicyNO = "保單編號/訂單編號";
            this.itag = "車號";
            

        }
        public string ApplicantPolicyNO { get; set; }
        public string itag { get; set; }
        


    }
    public class TodoOrderHeader 
    {
        public TodoOrderHeader()
        {
            this.InsuranceTypeName = "險別";
            this.Id = "案件編號";
            this.PolicyNo = "保單編號/訂單編號";
            this.CaseCategoryLayer1Name = "案件類型";
            this.CaseCategoryLayer2Name = "子分類";
            this.CreatorTime = "進件時間";
            this.StatusName = "處理狀態";
            this.CreatorName = "案件擁有者";
            this.CoOwnerName = "協辦";


        }
        public string Id { get; set; }
        public string InsuranceTypeName { get; set; }
        public string PolicyNo { get; set; }
        public string StatusName { get; set; }
        public string CaseCategoryLayer1Name { get; set; }
        public string CaseCategoryLayer2Name { get; set; }
        public string CreatorName { get; set; }
        public string CoOwnerName { get; set; }
        public string CreatorTime { get; set; }


                


    }
    public class InboundHeader : Header
    {
        public InboundHeader()
        {
            this.ApplicantPolicyNO = "保單編號/訂單編號";
            this.itag = "車號";


        }
        public string ApplicantPolicyNO { get; set; }
        public string itag { get; set; }



    }

}
