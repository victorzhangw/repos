﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    public class WorkOrderAccount
    {
        
        
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }
        /// <summary>
        /// 保單類別
        /// </summary>
        public string PolicyType { get; set; }
        /// <summary>
        /// 牌照號碼
        /// </summary>
        public string Itag { get; set; }
        /// <summary>
        /// 要保人
        /// </summary>
        [JsonInclude]
        public Policyholder Policyholder { get; set; }
        /// <summary>
        /// 被保人
        /// </summary>
        [JsonInclude]
        public Insured Insured { get; set; }
        /// <summary>
        /// 聯絡人
        /// </summary>
        [JsonInclude]
        public Contact Contact { get; set; }
    }
    public  class AccountInfo
    {

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 個人識別碼
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 行動電話
        /// </summary>
        public string ContactPhone { get; set; }
        /// <summary>
        /// 電子郵件
        /// </summary>
        public string Email { get; set; }
       
    }
    /// <summary>
    /// 要保人
    /// </summary>
    public class Policyholder : AccountInfo
    {
        public Policyholder ShallowCopy()
        {
            return (Policyholder)this.MemberwiseClone();
        }
    }
    /// <summary>
    /// 被保人
    /// </summary>
    public class Insured : AccountInfo
    {
        public Insured ShallowCopy()
        {
            return (Insured)this.MemberwiseClone();
        }
    }
    /// <summary>
    /// 聯絡人
    /// </summary>
    public class Contact : AccountInfo
    {
        public Contact ShallowCopy()
        {
            return (Contact)this.MemberwiseClone();
        }
        /// <summary>
        /// 關係人
        /// </summary>
        public string RelatedPerson { get; set; }
        /// <summary>
        /// 關係
        /// </summary>
        public string Relation { get; set; }
    }
    public class WorkOrderSetAccount
    {
        /// <summary>
        /// 身份證號
        /// </summary>
        public string ApplicantId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string ApplicantMobilePhone { get; set; }
        
        /// <summary>
        ///  保單單號
        /// </summary>
        public string ApplicantPolicyNO { get; set; }
        /// <summary>
        ///  歸戶身份
        /// </summary>
        public string ApplicantPolicyHolder { get; set; }
        /// <summary>
        /// 關係人
        /// </summary>
        public string RelatedPerson { get; set; }
        /// <summary>
        /// 關係
        /// </summary>
        public string Relation { get; set; }


    }

}
