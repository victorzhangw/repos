﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    
    /// <summary>
    /// Memo 進線資料
    /// </summary>
    public class CaseInfo

    {
        /// <summary>
        /// 事故編號
        /// </summary>
        public string inumber { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public string fstatus { get; set; }
        /// <summary>
        /// 狀態名稱
        /// </summary>
        public string nstatus { get; set; }
        /// <summary>
        /// 接聽人代號
        /// </summary>
        public string iuser { get; set; }
        /// <summary>
        /// 接聽人名稱
        /// </summary>
        public string nuser { get; set; }
        /// <summary>
        /// 通知人姓名
        /// </summary>
        public string nnotify { get; set; }
        /// <summary>
        /// 通知人行動電話
        /// </summary>
        public string itel_m_nnotify { get; set; }
        /// <summary>
        /// 通知人市話
        /// </summary>
        public string itel_o_nnotify { get; set; }
        /// <summary>
        /// CTI來電號碼
        /// </summary>
        public string itel_h_nnotify { get; set; }
        /// <summary>
        /// 被保險人
        /// </summary>
        public string nassured { get; set; }
        /// <summary>
        /// 被保險人身分證
        /// </summary>
        public string iassured { get; set; }
        /// <summary>
        /// 要保人
        /// </summary>
        public string napplicant { get; set; }
        /// <summary>
        /// 要保人身分證
        /// </summary>
        public string iapplicant { get; set; }
        
        /// <summary>
        /// 牌照號碼
        /// </summary>
        public string itag { get; set; }
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string ipolicy { get; set; }
        /// <summary>
        /// 保險證號
        /// </summary>
        public string icard { get; set; }
        /// <summary>
        /// 險別-代碼/名稱
        /// </summary>
        public string insure_type { get; set; }
        /// <summary>
        /// 險別名稱
        /// </summary>
        public string nnsure_type { get; set; }
        /// <summary>
        /// 服務需求
        /// </summary>
        public string iservice_need { get; set; } 
        /// <summary>
        /// 業務內容-代碼/名稱
        /// </summary>
        public string iassort { get; set; }
        /// <summary>
        /// 業務內容名稱
        /// </summary>
        public string nassort { get; set; }
        /// <summary>
        /// 通知時間
        /// </summary>
        public string dnotify { get; set; }
        /// <summary>
        /// 業務屬性
        /// </summary>
        public string ibusiness_prop { get; set; }
        /// <summary>
        /// 業務屬性名稱
        /// </summary>
        public string nbusiness_prop { get; set; }
        /// <summary>
        /// 客服自行備註
        /// </summary>
        public string iremark { get; set; }
        /// <summary>
        /// nremark
        /// </summary>
        public string nremark { get; set; }
        /// <summary>
        /// 道路救援通知時間
        /// </summary>
        public string dnotify_salve { get; set; }
        /// <summary>
        /// 道路救援抵達時間
        /// </summary>
        public string darrival_salve { get; set; }
        /// <summary>
        /// 拖吊原因代號
        /// </summary>
        public string itow { get; set; }
        /// <summary>
        /// 拖吊原因名稱
        /// </summary>
        public string ntow { get; set; }

        /// <summary>
        /// 分發項目
        /// </summary>
        public string nclaim_item { get; set; }
        /// <summary>
        /// 分發項目名稱
        /// </summary>
        public string nclaim_item_name { get; set; }
        /// <summary>
        /// 分發單位
        /// </summary>
        public string nclaim_unit { get; set; }
        /// <summary>
        /// 分發單位名稱
        /// </summary>
        public string nclaim_unit_name { get; set; }
        /// <summary>
        /// 肇事經過
        /// </summary>
        public string note { get; set; }
}
    
    /// <summary>
    /// 回覆 Memo 
    /// </summary>
    public class ResponseCaseInfo
    {
        public ResponseCaseInfo()
        {

        }
        /// <summary>
        /// 事故編號
        /// </summary>
        public string inumber { get; set; } 
        /// <summary>
        /// 聯絡人(回報時用)
        /// </summary>
        public string nconnecter { get; set; }
        /// <summary>
        /// 客戶名稱(回報時用)
        /// </summary>
        public string ncustomer { get; set; } 
        /// <summary>
        /// 其他（聯繫結果）
        /// </summary>
        public string nresult_other { get; set; }
        /// <summary>
        /// 聯絡時間
        /// </summary>
        public string dconnect { get; set; }
        
        /// <summary>
        /// 回報人員
        /// </summary>
        public string answer_user { get; set; }
        /// <summary>
        /// 單位回報內容
        /// ThinkClaim2：已聯繫上客戶 
        /// NotAnswerToMMS：未聯繫上客戶，已發簡訊通知 
        /// OtherThing2： 其他(聯繫結果必填)
        /// </summary>
        public string ireply { get; set; }
        /// <summary>
        /// 處理結果(回報時用)
        /// </summary>
        public string nprocess { get; set; }
    }

    public class Memo
    {
        
        public string Token { get; set; }
        public List<CaseInfo> case_info { get; set; }
    }
    public class ResponseMemo
    {
       
        public string Token { get; set; }
        public  List<ResponseCaseInfo> case_info { get; set; }
        
    }
    public class ResponseMemoStatus
    {
        public string msg { get; set; }
        public string status { get; set; }
    }

    public static class MemoCode
    {
        public static IDictionary<string, string> InsuranceCodeDic;
        public static IDictionary<string, string> AssortCodeDic;
        public static IDictionary<string, string> ServiceNeedCodeDic;
        public static IDictionary<string, string> ClaimItemCodeDic;
        public static IDictionary<string, string> ClaimUnitCodeDic;
        public static IDictionary<string, string> ReplyCodeDic;
        static MemoCode()
        {
            InsuranceCodeDic = new Dictionary<string, string>()
            { {"CAR", "車險"},{"A&H","傷害保險(含旅平險)" },{"FRE","火險-住火" } };
            AssortCodeDic = new Dictionary<string, string>()
            { {"PolicyNext", "投保/續保"},{"ClaimUpdate","批改更正" },{"InsuranceSearch","承保查詢" },{"Marketing","行銷活動" } ,{"Other","其他" }  };
            ServiceNeedCodeDic = new Dictionary<string, string>()
            { {"NetPolicy", "網路投保"}};
            ClaimUnitCodeDic = new Dictionary<string, string>()
            { {"000014", "網投-批改"},{"000016","網投-問題" },{"000017","網投-客訴" } ,{"000018","網投-行銷" } ,{"000022","網投-文批" }};
            ClaimItemCodeDic = new Dictionary<string, string>()
            { {"1", "理賠"},{"2","批改" },{"3","營業" } ,{"4","5" } };
            ReplyCodeDic = new Dictionary<string, string>()
            { {"ThinkClaim2", "已聯繫上客戶"},{"NotAnswerToMMS","未聯繫上客戶，已發簡訊通知" },{"OtherThing2","其他(聯繫結果必填)" }  };
        }


    }
}
   

