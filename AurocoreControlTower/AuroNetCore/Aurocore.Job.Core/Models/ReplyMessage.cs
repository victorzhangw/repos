﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    public class ReplyMessage
    {

        /// <summary>
        /// 區分訊息為內部/外部
        /// </summary>
        public string MessageType { get; set; }
        /// <summary>
        /// 聯繫方式 (ItemCode:InternalEMail,ExternalEMail,ExternalSMS,話務記錄)
        /// </summary>
        public string ContactCategory { get; set; }
        /// <summary>
        /// 聯繫對象Id
        /// </summary>
        public string ContactId { get; set; }
        /// <summary>
        /// 聯繫對象Id（電話;郵箱）
        /// </summary>
        public string ContactWay { get; set; }
        /// <summary>
        /// 聯繫對象姓名
        /// </summary>
        public string ContactName { get; set; }
        /// <summary>
        /// 回覆訊息/郵件主旨/簡訊/話後紀錄-外撥結果
        /// </summary>
        public string Header { get; set; }
        /// <summary>
        /// 郵件內文/話後紀錄-聯繫結果
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        /// 備註/簡述
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public string Attachment { get; set; }

    }
}
