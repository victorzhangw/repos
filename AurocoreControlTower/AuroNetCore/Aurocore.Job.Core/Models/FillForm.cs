﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 表單
    /// </summary>
    public class FillForm
    {
        public string Filename { get; set; }
        public string TemplateString { get; set; }
    }
    /// <summary>
    /// 汽車險
    /// </summary>
    public class CarEndorsement : FillForm
    {

    }
    /// <summary>
    /// 
    /// </summary>
    public class TravelEndorsement : FillForm
    {

    }
}
