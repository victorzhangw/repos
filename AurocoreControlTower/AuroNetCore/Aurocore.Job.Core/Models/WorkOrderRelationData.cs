﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 關聯回覆資訊
    /// </summary>
   public  class WorkOrderRelationData
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 電郵
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 電話
        /// </summary>
        public string Phone { get; set; }
    }
}
