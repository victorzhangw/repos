using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 資料實體物件
    /// </summary>
    [Table("CRM_WorkOrderLog")]
    [Serializable]
    public class WorkOrderLog:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {

        /// <summary>
        /// 設定或獲取父來源工作單編號
        /// </summary>
        public string WorkOrderId { get; set; }

        /// <summary>
        /// 設定或獲取子工作單編號
        /// </summary>
        public string WorkOrderDetailId { get; set; }
        /// <summary>
        /// Log 類別
        /// </summary>
        public string LogTypeName { get; set; }
        /// <summary>
        /// 設定或獲取回覆狀態(0:reject 1:approve)
        /// </summary>
        public bool? ReplyStatus { get; set; }

        /// <summary>
        /// 設定或獲取回覆訊息
        /// </summary>
        public string ReplyMessage { get; set; }

        /// <summary>
        /// 設定或獲取當前子工作單記錄
        /// </summary>
        public string CurrentFormLog { get; set; }
        /// <summary>
        /// 接受對象Id
        /// </summary>
        public string ToUserId { get; set; }
        /// <summary>
        /// 設定或獲取啟用
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DeleteUserId { get; set; }

    }
}
