﻿using Aurocore.Commons.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
    /// <summary>
    /// 外撥案件上傳名單
    /// </summary>
    public class OutBoundUploadNameList
    {

        /// <summary>
        /// 要保序號/保險單號
        /// </summary>
        public string PolicyNo { get; set; }
        /// <summary>
        /// 保戶類別
        /// </summary>
        public string PolicyholderType { get; set; }
        /// <summary>
        /// 要保人姓名
        /// </summary>
        public string ProposerName { get; set; }
        /// <summary>
        /// 要保人ID
        /// </summary>
        public string ProposerId { get; set; }
        /// <summary>
        /// 要保人手機號碼
        /// </summary>
        public string ProposerPhone { get; set; }
        /// <summary>
        /// 被保人姓名
        /// </summary>
        public string InsuredName { get; set; }
        /// <summary>
        /// 被保人ID
        /// </summary>
        public string InsuredId { get; set; }
        /// <summary>
        /// 車號/牌照號碼
        /// </summary>
        public string Itag { get; set; }
        /// <summary>
        /// 標的物地址
        /// </summary>
        public string SubjectMatterInsuredAddr { get; set; }
        /// <summary>
        /// 保單起始日
        /// </summary>
        public string PolicyStartDate { get; set; }
        /// <summary>
        /// 保單到期日
        /// </summary>
        public string PolicyEndDate { get; set; }
        /// <summary>
        /// 險種代號
        /// </summary>
        public string InsuranceTypeId { get; set; }
        /// <summary>
        /// 險種名稱
        /// </summary>
        public string InsuranceTypeName { get; set; }
        /// <summary>
        /// 總保額
        /// </summary>
        public string InsuredAmount { get; set; }
        /// <summary>
        /// 退費金額
        /// </summary>
        public string RefundAmount { get; set; }
        /// <summary>
        /// 續保比對
        /// </summary>
        public string RenewCheck { get; set; }
        /// <summary>
        /// 查詢狀況
        /// </summary>
        public string InqueryStatus { get; set; }
        /// <summary>
        /// 負責追蹤客服
        /// </summary>
        public string AgentinCharge { get; set; }
        
    }
    [Table("CRM_OutBoundNameList")]
    [Serializable]
    public class OutBoundDbNameList : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
       
        /// <summary>
        /// OutBound Primary Key
        /// </summary>
        public string OutBoundId { get; set; }
        /// <summary>
        /// 要保人 Id
        /// </summary>
       public string ApplicantId { get; set; }
        /// <summary>
        /// 要保人姓名
        /// </summary>
        public string ApplicantName { get; set; }
        /// <summary>
        /// 要保人手機號碼
        /// </summary>
        public string ApplicantMobilePhone { get; set; }
        /// <summary>
        /// 要保人保單號碼
        /// </summary>
        public string ApplicantPolicyNo { get; set; }
        /// <summary>
        /// 案件補充/詳盡資料
        /// </summary>
        public string Supplement { get; set; }
        /// <summary>
        /// 案件擁有者 Id
        /// </summary>
        public string OwnerId { get; set; }
        /// <summary>
        /// 案件擁有者姓名
        /// </summary>
        public string OwnerName { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 建立者 Id
        /// </summary>
        public string CreatorUserId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string LastModifyUserId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  刪除者Id
        /// </summary>
        public string DeleteUserId { get; set; }
        /// <summary>
        ///  刪除碼
        /// </summary>
        public bool? DeleteMark { get; set; }
    }
    /// <summary>
    /// 外撥案件總表
    /// </summary>
    [Table("CRM_OutBound")]
    [Serializable]
    public class OutBound : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        ///  專案名稱
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        ///  專案名稱
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        ///  檔案名稱
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 上傳筆數
        /// </summary>
        public int? TotalRecord { get; set; }
        /// <summary>
        /// 成功上傳筆數
        /// </summary>
        public int? ImportedRecord { get; set; }
        /// <summary>
        /// 案件類別 Id
        /// </summary>
        public string WorkOrderTypeId { get; set; }
        /// <summary>
        /// 案件類別
        /// </summary>
        public string  WorkOrderTypeName { get; set; }
        /// <summary>
        /// 預計撥打完成時間
        /// </summary>
        public DateTime? CaseCloseTime { get; set; }
        /// <summary>
        ///  傳輸案件碼 0：未傳輸 1：已傳輸
        /// </summary>
        public bool? TransportMark { get; set; }
        /// <summary>
        ///  傳輸至案件時間
        /// </summary>
        public DateTime? TransportTime { get; set; }
        /// <summary>
        ///  啟用碼 0：未啟用 1：已啟用
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  刪除碼
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        ///  建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }
        
     
        /// <summary>
        ///  建立者 Id
        /// </summary>
        public string CreatorUserId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  刪除者Id
        /// </summary>
        public string DeleteUserId { get; set; }

    }

    /// <summary>
    /// Outbound 統計
    /// </summary>
    public class OutBoundStatistics
    {
        
        /// <summary>
        /// 指標說明
        /// </summary>
        public List<Dictionary<string,string>> ColumnHead { get; set; }
        public string Indicator1 { get; set; }
        public string Indicator2 { get; set; }
        public string Indicator3 { get; set; }
        public string Indicator4 { get; set; }
        public string Indicator5 { get; set; }
        public string Indicator6 { get; set; }
        public string Indicator7 { get; set; }
        public string Indicator8 { get; set; }
        public OutBoundStatistics()
        {
            
            var dict1 = new Dictionary<string, string>();
            dict1.Add("Indicator1", "新件母數");
            var dict2 = new Dictionary<string, string>();
            dict2.Add("Indicator2", "電訪數");
            var dict3 = new Dictionary<string, string>();
            dict3.Add("Indicator3", "新保戶抽樣比例");
            var dict4 = new Dictionary<string, string>();
            dict4.Add("Indicator4", "已成功電訪數");
            var dict5 = new Dictionary<string, string>();
            dict5.Add("Indicator5", "既有保戶");
            var dict6 = new Dictionary<string, string>();
            dict6.Add("Indicator6", "既有保戶抽樣比例");
            var dict7 = new Dictionary<string, string>();
            dict7.Add("Indicator7", "電訪成功比例");
            var dict8 = new Dictionary<string, string>();
            dict8.Add("Indicator8", "期間總投保數");
            this.ColumnHead = new();
            this.ColumnHead.Add(new Dictionary<string, string>(dict1));
            this.ColumnHead.Add(new Dictionary<string, string>(dict2));
            this.ColumnHead.Add(new Dictionary<string, string>(dict3));
            this.ColumnHead.Add(new Dictionary<string, string>(dict4));
            this.ColumnHead.Add(new Dictionary<string, string>(dict5));
            this.ColumnHead.Add(new Dictionary<string, string>(dict6));
            this.ColumnHead.Add(new Dictionary<string, string>(dict7));
            this.ColumnHead.Add(new Dictionary<string, string>(dict8));


        }
    }

    public class OutBoundContactColumn
    {
        public string title { get; set; }
        public string key { get; set; }
        public string align { get; set; }
        public int minWidth { get; set; }
    }

    
}

