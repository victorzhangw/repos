﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Security.Models;
using Aurocore.WorkOrders.Dtos;
namespace Aurocore.WorkOrders.Models
{
    public class SeachWorkOrder : SearchInputDto<WorkOrderInputDto>
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public string RoleId
        {
            get; set;
        }
        /// <summary>
        /// 部門Id
        /// </summary>
        public string DepartmentId
        {
            get; set;
        }
        /// <summary>
        /// 操作者Id
        /// </summary>
        public string CurrentUserId
        {
            get; set;
        }
        /// <summary>
        /// 搜索狀態 （0:all ,1:personal）
        /// </summary>
        public string CurrentSearchStatus
        {
            get; set;
        }

       
    }
}
