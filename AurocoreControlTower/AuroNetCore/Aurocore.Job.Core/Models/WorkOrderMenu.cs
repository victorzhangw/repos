﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WorkOrders.Models
{
   
  
    public class WorkOrderMenu
    {
        /// <summary>
        /// 案件分類 Id
        /// </summary>
        public string FormTypeId { get; set; }
        /// <summary>
        /// 案件類別
        /// </summary>
        public string FormTypeName { get; set; }

        /// <summary>
        /// 數量 1
        /// </summary>
        public string FormCounts{ get; set; }
        /// <summary>
        /// 數量 2
        /// </summary>
        public string FormCounts2 { get; set; }
    }

}
