using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.WorkOrders.Models;
using Aurocore.Security.Models;

namespace Aurocore.WorkOrders.Dtos
{
	public class WorkOrderProfile : Profile
	{
		public WorkOrderProfile()
		{
		    CreateMap<WorkOrder, WorkOrderOutputDto>();
		    CreateMap<WorkOrderInputDto, WorkOrder>();
		    CreateMap<WorkOrderDetail, WorkOrderDetailOutputDto>();
		    CreateMap<WorkOrderDetailInputDto, WorkOrderDetail>();
			
		    CreateMap<WorkOrderLog, WorkOrderLogOutputDto>();
		    CreateMap<WorkOrderLogInputDto, WorkOrderLog>();
			CreateMap<WorkOrderBrief, ContactHistory>()
				.ForMember(d => d.ContactTime, s => s.MapFrom(o => o.CreatorTime))
				.ForMember(d => d.ContactStatus, s => s.MapFrom(o => o.ContactStatus))
				.ForMember(d => d.Attachment, s => s.MapFrom(o => o.Attachment))
				.ForMember(d => d.ContactEmail, s => s.MapFrom(o => o.ContactEmail))
				.ForMember(d => d.ContactPhone, s => s.MapFrom(o => o.ContactPhone))
				.ForMember(d => d.ContactStatus, s => s.MapFrom(o => o.CaseCategoryLayer1Name))
				.ForMember(d => d.ContactStatusDesc, s => s.MapFrom(o => o.Body))
				.ForMember(d => d.ContactType, s => s.MapFrom(o => o.ResponseTypeName))
				.ForMember(d => d.CaseOwner, s => s.MapFrom(o => o.RealName));

			CreateMap<WorkOrderBrief, CreateDetailHistory>()
				.ForMember(d => d.Attachment ,s => s.MapFrom(o => o.Attachment))
				.ForMember(d => d.CoOwnerName, s => s.MapFrom(o => o.CoOwnerName))
				.ForMember(d => d.CreatorName, s => s.MapFrom(o => o.RealName))
				.ForMember(d => d.DetailDesc, s => s.MapFrom(o => o.Body))
				.ForMember(d => d.DetailId, s => s.MapFrom(o => o.Id))
				.ForMember(d => d.DetailIdCurrentStatus, s => s.MapFrom(o => o.StatusName))
				.ForMember(d => d.DetailOpenTime, s => s.MapFrom(o => o.CreatorTime))
				.ForMember(d => d.ParentDetailTypeId, s => s.MapFrom(o => o.ParentResponseTypeId))
				.ForMember(d => d.ParentDetailType, s => s.MapFrom(o => o.ParentResponseTypeName))
				.ForMember(d => d.DetailType, s => s.MapFrom(o => o.ResponseTypeName))
				.ForMember(d => d.DetailTypeId, s => s.MapFrom(o => o.ResponseTypeId));
			CreateMap<WorkOrderBrief, ResponMemoHistory>()
				.ForMember(d => d.inumber, s => s.MapFrom(o => o.SourceId))
				.ForMember(d => d.CaseTypeName, s => s.MapFrom(o => o.CaseCategoryLayer1Name))
				.ForMember(d => d.CreatorName, s => s.MapFrom(o => o.RealName))
				.ForMember(d => d.CustomerName, s => s.MapFrom(o => o.ContactName))
				.ForMember(d => d.DetailDesc, s => s.MapFrom(o => o.Header))
				.ForMember(d => d.DetailOpenTime, s => s.MapFrom(o => o.CreatorTime));

			CreateMap<OutBoundDbNameList, WorkOrder>()
				.ForMember(d => d.ApplicantId, s => s.MapFrom(o => o.ApplicantId))
				.ForMember(d => d.ApplicantMobilePhone, s => s.MapFrom(o => o.ApplicantMobilePhone))
				.ForMember(d => d.ApplicantName, s => s.MapFrom(o => o.ApplicantName))
				.ForMember(d => d.ApplicantPolicyNO, s => s.MapFrom(o => o.ApplicantPolicyNo))
				.ForMember(d => d.OwnerId, s => s.MapFrom(o => o.OwnerId))
				.ForMember(d => d.OwnerName, s => s.MapFrom(o => o.OwnerName))
				.ForMember(d => d.Supplement, s => s.MapFrom(o => o.Supplement));

				

			CreateMap<OutBoundDbNameListInputDto, OutBoundDbNameList>();
			CreateMap<OutBoundDbNameList, OutBoundDbNameListOutputDto>();
			CreateMap<OutBoundInputDto, OutBound>();
			CreateMap<OutBound, OutBoundOutputDto>();




		}
	}
}
