﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.WorkOrders.Models;


namespace Aurocore.WorkOrders.Dtos
{
    public class WorkOrderTreeOutputDto
    {
        public WorkOrderTreeOutputDto()
        {
           // this.ColumnHeader = new string[] { "Id", "WorkOrderId", "案件類型", "險種", "案件分類", "子類型","保險單號","處理時效","單據狀態","主旨","內文","建立時間","開單人" };
            

        }
        public string formType;
        public  string Id { get; set; }
        /// <summary>
        /// 標籤 ： 開單 / 聯絡 / 回報
        /// </summary>
        /// 
        public string label { get; set; }
        /// <summary>
        ///  log 資料總數
        /// </summary>
        public string rowCount { get; set; }
        /// <summary>
        ///  父欄位標頭敘述
        /// </summary>
        public string[] ColumnHeader { get; set; }

        /// <summary>
        /// 工作單
        /// </summary>
        public  WorkOrderBrief orderBrief { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Children children { get; set; }
    }
    public class Children
    {
        public Children()
        {
            this.ChildColumnHeader = new string[]{"LogID","WorkOrderDetailId","回覆狀態","回覆訊息","開單人","建立時間" };
          

        }
        /// <summary>
        /// 子欄位標頭敘述
        /// </summary>
        public string[] ChildColumnHeader {get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<WorkOrderLogBrief> LogOutput { get; set; }
        
    }
    
}
