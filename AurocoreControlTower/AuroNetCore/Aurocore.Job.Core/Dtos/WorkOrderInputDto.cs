using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.WorkOrders.Models;
using AutoMapper.Configuration.Conventions;
using AutoMapper.Configuration.Annotations;

namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 案件輸入物件模型
    /// </summary>
    [AutoMap(typeof(WorkOrder))]
    [Serializable]
    public class WorkOrderInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵
        /// </summary>
  
        public string Id { get; set; }
        /// <summary>
        /// 工作單編碼
        /// </summary>
        public string FormTypeId { get; set; }
        /// <summary>
        /// 工作單名稱
        /// </summary>
        public string FormTypeName { get; set; }

        /// <summary>
        /// 來源編號
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// 來源
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// 狀態碼編號
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// 狀態碼
        /// </summary>
        public string StatusName { get; set; }


        /// <summary>
        /// 開單日
        /// </summary>
        public string CaseOpenTime { get; set; }

        /// <summary>
        /// 截單日
        /// </summary>
        public string CaseCloseTime { get; set; }

        /// <summary>
        ///  擁有者組織
        /// </summary>
        public string OwnerName { get; set; }

        /// <summary>
        /// 擁有者Id
        /// </summary>
        public string OwnerId { get; set; }
        /// <summary>
        /// 案件第一層分類
        /// </summary>
        public string CaseCategoryLayer1Name { get; set; }

        /// <summary>
        /// 案件第一層分類Id
        /// </summary>
        public string CaseCategoryLayer1Id { get; set; }
        /// <summary>
        /// 案件第二層分類
        /// </summary>
        public string CaseCategoryLayer2Name { get; set; }

        /// <summary>
        /// 案件第二層分類Id
        /// </summary>
        public string CaseCategoryLayer2Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ApplicantId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ApplicantMobilePhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ApplicantLocalPhone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ApplicantCTIPhone { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string ApplicantEmail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ApplicantAddress { get; set; }
        /// <summary>
        ///  申請者保單
        /// </summary>
        public string ApplicantPolicyNO { get; set; }
        /// <summary>
        ///  保單類別
        /// </summary>
        public string InsuranceTypeName { get; set; }
        /// <summary>
        ///  車牌號碼
        /// </summary>
        public string Itag { get; set; }
        
        /// <summary>
        ///  歸戶關係
        /// </summary>
        public string SetAccountRelation { get; set; }
        /// <summary>
        ///  歸戶狀態  已歸戶:0,  未歸戶:1
        /// </summary>
        public bool? SetAccountStatus { get; set; }
        /// <summary>
        ///  歸戶說明
        /// </summary>
        public string SetAccountStatusDesc { get; set; }

        /// <summary>
        /// 頭部/主旨
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// 本文
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        ///  補充資料
        /// </summary>
        public string Supplement { get; set; }
        /// <summary>
        ///  附件（使用逗號分開）
        /// </summary>
        public string Attachment { get; set; } 
        /// <summary>
        /// 重要
        /// </summary>
        public bool? IsHot { get; set; }
        /// <summary>
        ///  通知
        /// </summary>
        public bool? IsNotified { get; set; }
        /// <summary>
        /// 生效碼
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 協辦者姓名
        /// </summary>
        public string CoOwnerName { get; set; }

        /// <summary>
        /// 協辦者Id
        /// </summary>
        public string CoOwnerId { get; set; }
        /// <summary>
        ///  是否閱讀
        /// </summary>
        public string IsRead { get; set; }
    }
}
