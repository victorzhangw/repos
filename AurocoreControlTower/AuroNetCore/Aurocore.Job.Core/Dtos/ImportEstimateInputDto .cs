using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;


namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸入要保書模型
    /// </summary>
   
    [Serializable]
    public class ImportEstimateInputDto : IInputDto<string>
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 要保書 Id
        /// </summary>
        public string EstimateIds { get; set; }
        /// <summary>
        /// 要保書 類別
        /// </summary>
        public string EstimateType { get; set; }




    }
}
