﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(OutBoundDbNameList))]
    [Serializable]
    public class OutBoundDbNameListInputDto : IInputDto<string>
    {
        /// <summary>
        ///  主鍵
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 要保人 Id
        /// </summary>
        public string ApplicantId { get; set; }
        /// <summary>
        /// 要保人姓名
        /// </summary>
        public string ApplicantName { get; set; }
        /// <summary>
        /// 要保人手機號碼
        /// </summary>
        public string ApplicantMobilePhone { get; set; }
        /// <summary>
        /// 要保人保單號碼
        /// </summary>
        public string ApplicantPolicyNo { get; set; }
        /// <summary>
        /// 案件補充/詳盡資料
        /// </summary>
        public string Supplement { get; set; }
        /// <summary>
        /// 案件擁有者 Id
        /// </summary>
        public string OwnerId { get; set; }
        /// <summary>
        /// 案件擁有者姓名
        /// </summary>
        public string OwnerName { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public string CreatorTime { get; set; }
        /// <summary>
        /// 建立者 Id
        /// </summary>
        public string CreatorUserId { get; set; }

    }
}
