using AutoMapper.Configuration.Conventions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Aurocore.Commons.Cache;
using Aurocore.Security.Models;
using Aurocore.Commons.Json;

using System.Globalization;
using Aurocore.Commons.Log;

namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 案件輸出物件模型
    /// </summary>
    [Serializable]
    public class WorkOrderOutputDto
    {
        private string attachStr = string.Empty;
       
        
        /// <summary>
        /// 主鍵
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }
        /// <summary>
        ///  上階案件分類Id
        /// </summary>
        public string ParentFormTypeId { get; set; }
        /// <summary>
        ///  上階案件分類名稱
        /// </summary>
        public string ParentFormTypeName { get; set; }
        /// <summary>
        /// 工作單類型 Id
        /// </summary>
        public string FormTypeId { get; set; }
        /// <summary>
        /// 工作單名稱
        /// </summary>
        public string FormTypeName { get; set; }
        /// <summary>
        /// 來源Id
        /// </summary>
        [MaxLength(600)]
        public string SourceId { get; set; }

        /// <summary>
        /// 來源
        /// </summary>
        [MaxLength(600)]
        public string SourceName { get; set; }

        /// <summary>
        ///  狀態碼Id
        /// </summary>
        [MaxLength(100)]
        public string StatusId { get; set; }

        /// <summary>
        /// 狀態碼
        /// </summary>
        [MaxLength(100)]
        public string StatusName { get; set; }


        /// <summary>
        /// 開單日
        /// </summary>
        public DateTime CaseOpenTime { get; set; }
        


        /// <summary>
        /// 截單日
        /// </summary>
        public DateTime? CaseCloseTime { get; set; }
        /// <summary>
        ///  負責人
        /// </summary>
        [MaxLength(100)]
        public string OwnerName { get; set; }

        /// <summary>
        ///  負責人Id
        /// </summary>
        [MaxLength(100)]
        public string OwnerId { get; set; }

        /// <summary>
        ///  要保書目前處理者
        /// </summary>
        [MaxLength(100)]
        public string CurrentEditorName { get; set; }
        /// <summary>
        ///  要保書目前處理者
        /// </summary>
        [MaxLength(100)]
        public string CurrentEditorDept { get; set; }


        /// <summary>
        /// 案件第一層分類
        /// </summary>
        public string CaseCategoryLayer1Name { get; set; }

        /// <summary>
        /// 案件第一層分類Id
        /// </summary>
        public string CaseCategoryLayer1Id { get; set; }
        /// <summary>
        /// 案件第二層分類
        /// </summary>
        public string CaseCategoryLayer2Name { get; set; }

        /// <summary>
        /// 案件第二層分類Id
        /// </summary>
        public string CaseCategoryLayer2Id { get; set; }

        [MaxLength(100)]
        public string ApplicantId { get; set; }

        /// <summary>
        ///  進線者姓名
        /// </summary>
        [MaxLength(100)]
        public string ApplicantName { get; set; }


        /// <summary>
        ///  進線者行動電話
        /// </summary>
        public string ApplicantMobilePhone { get; set; }
        /// <summary>
        ///  進線者本地電話
        /// </summary>
        public string ApplicantLocalPhone { get; set; }
        /// <summary>
        ///  進線者 CTI 進線電話
        /// </summary>
        public string ApplicantCTIPhone { get; set; }


        /// <summary>
        ///  進線者電子郵件
        /// </summary>
        [MaxLength(200)]
        public string ApplicantEmail { get; set; }

        /// <summary>
        ///  進線者地址
        /// </summary>
        [MaxLength(600)]
        public string ApplicantAddress { get; set; }
        /// <summary>
        ///  申請者保單
        /// </summary>
        public string ApplicantPolicyNO { get; set; }
        /// <summary>
        ///  保單類別
        /// </summary>
        public string InsuranceTypeName { get; set; }
        /// <summary>
        ///  車牌號碼
        /// </summary>
        public string Itag { get; set; }
  
        /// <summary>
        ///  歸戶關係
        /// </summary>
        public string SetAccountRelation { get; set; }
        /// <summary>
        ///  歸戶狀態  已歸戶:0,  未歸戶:1
        /// </summary>
        public bool? SetAccountStatus { get; set; }
        /// <summary>
        ///  歸戶說明
        /// </summary>
        public string SetAccountStatusDesc { get; set; }

        /// <summary>
        ///  進線主旨/單頭資料
        /// </summary>
        [MaxLength(400)]
        public string Header { get; set; }

        /// <summary>
        ///  進線資料本文/單身資料/格式有 JSON / Html /Text
        /// </summary>
        [MaxLength(-1)]
        public string Body { get; set; }
        /// <summary>
        ///  補充資料
        /// </summary>
        public string Supplement { get; set; }
        /// <summary>
        ///  附檔（逗號分開）
        /// </summary>
        public string Attachment {
            set { attachStr = value; }
            get {
                List<string> _tmpList = new List<string>();
                if (!string.IsNullOrEmpty(attachStr))
                {
                    var attachAry = attachStr.Split(",");
                    
                    AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                    SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
                    foreach (var i in attachAry)
                    {
                        _tmpList.Add(sysSetting.WebUrl + "/" + i);
                    }

                }


                return  string.Join(",", _tmpList); 
            }

        }
        /// <summary>
        ///  重要性
        /// </summary>
        public bool? IsHot { get; set; }
        /// <summary>
        ///  通知
        /// </summary>
        public bool? IsNotified { get; set; }
        /// <summary>
        ///  是否顯示處理區塊
        /// </summary>
        public bool? IsBlockDisplay { get; set; } = true;
            
        /// <summary>
        ///  生效碼
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  刪除標記
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        ///  建立時間
        /// </summary>
        public string CreatorTime
        {
            set;get;
            
        }
        /// <summary>
        ///  建立時限
        /// </summary>
        public DateTime? Timeless {get;set; }





        /// <summary>
        ///  建立者 Id
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }

        /// <summary>
        ///  建立者 姓名
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserName { get; set; }


        /// <summary>
        ///  最後編輯時間
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  最後編輯者 Id
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  刪除時間
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  刪除者 Id
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }
        /// <summary>
        ///  欄位名
        /// </summary>
        [MaxLength(200)]
        public string CoulumnHead { get; set; }

        /// <summary>
        ///  是否閱讀
        /// </summary>
        public string IsRead { get; set; }

        /// <summary>
        ///  是否閱讀
        /// </summary>
        public bool IsNewWindow { get; set; }=false;
    }
}
