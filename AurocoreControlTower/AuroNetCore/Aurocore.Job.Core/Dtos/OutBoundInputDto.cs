﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.WorkOrders.Models;


namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(OutBound))]
    [Serializable]
    class OutBoundInputDto : IInputDto<string>
    {
        /// <summary>
        ///  主鍵
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        ///  專案類別 Id
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        ///  專案名稱
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        ///  檔案名稱
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 上傳筆數
        /// </summary>
        public int TotalRecord { get; set; }
        /// <summary>
        /// 成功上傳筆數
        /// </summary>
        public int ImportedRecord { get; set; }
        /// <summary>
        /// 案件類別 Id
        /// </summary>
        public string WorkOrderTypeId { get; set; }
        /// <summary>
        /// 案件類別
        /// </summary>
        public string WorkOrderTypeName { get; set; }
        /// <summary>
        /// 預計撥打完成時間
        /// </summary>
        public DateTime? CaseCloseTime { get; set; }
        /// <summary>
        ///  傳輸案件碼 0：未傳輸 1：已傳輸
        /// </summary>
        public bool? TransportMark { get; set; }
        /// <summary>
        /// 傳輸至案件時間
        /// </summary>
        public bool? TransportTime { get; set; }
        /// <summary>
        ///  啟用碼 0：未啟用 1：已啟用
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  刪除碼
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        ///  建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }

   
        /// <summary>
        ///  建立者 Id
        /// </summary>
        public string CreatorUserId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  刪除者Id
        /// </summary>
        public string DeleteUserId { get; set; }
    }
}
