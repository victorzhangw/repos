using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(WorkOrderDetail))]
    [Serializable]
    public class WorkOrderDetailInputDto: IInputDto<string>
    {
        private int boolnum;
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

  
        public string SourceId { get; set; }
        /// <summary>
        /// 要保書 Id
        /// </summary>
        public string EstimateId { get; set; }
        /// <summary>
        /// 回覆狀態(0:reject 1:approve, 空值不寄送通知) 
        /// </summary>
        public bool? ReplyStatus {get; set; }
            

        /// <summary>
        /// 案件Id
        /// </summary>
        public string WorkOrderId { get; set; }

        /// <summary>
        /// 設定或獲取案件處理類別代碼
        /// </summary>
        public string ResponseTypeId { get; set; }

        /// <summary>
        /// 設定或獲取案件處理類別代碼
        /// </summary>
        public string ResponseTypeName { get; set; }

        /// <summary>
        /// 設定或獲取保險代碼 
        /// </summary>
        public string InsuranceTypeId { get; set; }

        /// <summary>
        /// 保險名稱
        /// </summary>
        public string InsuranceTypeName { get; set; }

        /// <summary>
        /// 設定或獲取案件第一層類別 Id
        /// </summary>
        public string CaseCategoryLayer1Id { get; set; }

        /// <summary>
        /// 案件第一層類別名稱
        /// </summary>
        public string CaseCategoryLayer1Name { get; set; }

        /// <summary>
        /// 案件第二層類別 Id
        /// </summary>
        public string CaseCategoryLayer2Id { get; set; }

        /// <summary>
        /// 案件第二層類別 名稱
        /// </summary>
        public string CaseCategoryLayer2Name { get; set; }
        /// <summary>
        /// 案件第三層類別 Id
        /// </summary>
        public string CaseCategoryLayer3Id { get; set; }

        /// <summary>
        /// 案件第三層類別 名稱
        /// </summary>
        public string CaseCategoryLayer3Name { get; set; }

        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }

        /// <summary>
        /// 關聯保單號碼
        /// </summary>
        public string RelatedPolicyNo { get; set; }

        /// <summary>
        /// 時效
        /// </summary>
        public DateTime? Timeliness { get; set; }

        /// <summary>
        /// 處理狀態 Id
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// 處理狀態名
        /// </summary>
        public string StatusName { get; set; }
        /// <summary>
        /// 狀態描述
        /// </summary>
        public string ContactStatusDesc { get; set; }
        /// <summary>
        ///  協辦者部門Id
        /// </summary>
        public string CoDeptId { get; set; }
        /// <summary>
        /// 協辦員工Id
        /// </summary>
        public string CoOwnerId { get; set; }

        /// <summary>
        /// 協辦員工姓名
        /// </summary>
        public string CoOwnerName { get; set; }

        /// <summary>
        /// 文件主旨
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// 文件內文
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 目前單據擁有者
        /// </summary>
        public string CurrentEditorId { get; set; }
        /// <summary>
        /// 目前單據擁有者
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        /// 設定或獲取列印PDF 使用JSON儲存 
        /// </summary>
        public string FormLog { get; set; }

        /// <summary>
        /// 附件（使用逗號分隔","）
        /// </summary>
        public string Attachment { get; set; }

        /// <summary>
        /// 被聯絡人Id
        /// </summary>
        public string ContactId { get; set; }

        /// <summary>
        /// 被聯絡人姓名
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// 被聯絡人電子郵件
        /// </summary>
        public string ContactEmail { get; set; }

        /// <summary>
        /// 被聯絡人電話
        /// </summary>
        public string ContactPhone { get; set; }

        /// <summary>
        /// 客戶姓名
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 聯絡時間
        /// </summary>
        public DateTime? ContactTime { get; set; }
        /// <summary>
        /// 聯絡人關係
        /// </summary>
        public string ContactRelation { get; set; }
        /// <summary>
        ///  重要性
        /// </summary>
        public bool? IsHot { get; set; }
        /// <summary>
        ///  通知
        /// </summary>
        public bool? IsNotified { get; set; }
        /// <summary>
        /// 聯絡狀態
        /// </summary>
        public string ContactStatus { get; set; }
        /// <summary>
        /// 聯絡者特殊備註（要寫回客戶資料庫）
        /// </summary>
        public string ContactSpecialNotes { get; set; }
        /// <summary>
        /// 回應時間
        /// </summary>
        public DateTime? AnswerTime { get; set; }
        /// <summary>
        /// 車牌號碼
        /// </summary>
        public string PlateNumber { get; set; }
        /// <summary>
        /// 生效碼
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 結束日
        /// </summary>
        public string CaseCloseTime
        {
            get; set;
        }
        /// <summary>
        /// 起始日
        /// </summary>
        public string CaseOpenTime
        {
            get; set;
        }
        /// <summary>
        ///  是否閱讀
        /// </summary>
        public string IsRead { get; set; }
    }
}
