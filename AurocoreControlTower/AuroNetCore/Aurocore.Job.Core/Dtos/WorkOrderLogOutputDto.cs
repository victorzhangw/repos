using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class WorkOrderLogOutputDto
    {
        /// <summary>
        ///  
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }

        /// <summary>
        /// 父來源工作單編號
        /// </summary>
        [MaxLength(100)]
        public string WorkOrderId { get; set; }

        /// <summary>
        /// 子工作單編號
        /// </summary>
        [MaxLength(100)]
        public string WorkOrderDetailId { get; set; }
        /// <summary>
        /// Log 類別
        /// </summary>
        public string LogTypeName { get; set; }
        /// <summary>
        ///  回覆狀態(0:reject 1:approve)
        /// </summary>
        [MaxLength(100)]
        public bool? ReplyStatus { get; set; }

        /// <summary>
        /// 回覆訊息
        /// </summary>
        [MaxLength(8000)]
        public string ReplyMessage { get; set; }

        /// <summary>
        /// 當前子工作單記錄
        /// </summary>
        [MaxLength(-1)]
        public string CurrentFormLog { get; set; }
        /// <summary>
        /// 接受對象Id
        /// </summary>
        public string ToUserId { get; set; }
        /// <summary>
        /// 啟用
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

    }
}
