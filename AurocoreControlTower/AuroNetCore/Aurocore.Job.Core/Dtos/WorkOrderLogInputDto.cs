using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(WorkOrderLog))]
    [Serializable]
    public class WorkOrderLogInputDto: IInputDto<string>
    {
        /// <summary>
        ///  Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 父來源工作單編號
        /// </summary>
        public string WorkOrderId { get; set; }

        /// <summary>
        /// 子工作單編號
        /// </summary>
        public string WorkOrderDetailId { get; set; }
        /// <summary>
        /// Log 類別
        /// </summary>
        public string LogTypeName { get; set; }
        /// <summary>
        /// 回覆狀態(0:reject 1:approve, 空值不寄送通知) 
        /// </summary>
        public bool?  ReplyStatus { get; set; }

        /// <summary>
        /// 回覆訊息
        /// </summary>
        public string ReplyMessage { get; set; }

        /// <summary>
        /// 當前子工作單記錄
        /// </summary>
        public string CurrentFormLog { get; set; }
        /// <summary>
        /// 接受對象Id
        /// </summary>
        public string ToUserId { get; set; }
        /// <summary>
        /// 啟用
        /// </summary>
        public bool? EnabledMark { get; set; }
        
    }
}
