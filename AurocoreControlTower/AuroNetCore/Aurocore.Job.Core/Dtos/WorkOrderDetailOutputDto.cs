using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Aurocore.Commons.Cache;
using Aurocore.Security.Models;
using Aurocore.Commons.Json;
namespace Aurocore.WorkOrders.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class WorkOrderDetailOutputDto
    {
        private string attachStr;
        private string cateIdStr;
        private string cateNameStr;
        /// <summary>
        /// 獲取案件工作單 Id
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }

        /// <summary>
        /// 父案件 Id
        /// </summary>
        [MaxLength(100)]
        public string WorkOrderId { get; set; }

        /// <summary>
        /// 設定或獲取工作單類型代碼
        /// </summary>
        [MaxLength(100)]
        public string ResponseTypeId { get; set; }

        /// <summary>
        /// 設定或獲取工作單類型名稱
        /// </summary>
        [MaxLength(100)]
        public string ResponseTypeName { get; set; }

        /// <summary>
        /// 保險類別代碼
        /// </summary>
        [MaxLength(100)]
        public string InsuranceTypeId { get; set; }

        /// <summary>
        ///  保險類別名稱
        /// </summary>
        [MaxLength(100)]
        public string InsuranceTypeName { get; set; }

        /// <summary>
        /// 設定或獲取案件第一層類別 Id
        /// </summary>
        [MaxLength(100)]
        public string CaseCategoryLayer1Id { get; set; }

        /// <summary>
        /// 案件第一層類別名稱
        /// </summary>
        [MaxLength(100)]
        public string CaseCategoryLayer1Name { get; set; }

        /// <summary>
        /// 案件第二層類別 Id
        /// </summary>
        [MaxLength(100)]
        public string CaseCategoryLayer2Id {
            set { cateIdStr = value; }
            get
            {
                if (string.IsNullOrEmpty(cateIdStr))
                {
                    cateIdStr = string.Empty;
                }
                else
                {
                    cateIdStr.Trim();
                }
                return cateIdStr;
            }
        }

        /// <summary>
        /// 案件第一層類別 名稱
        /// </summary>
        [MaxLength(20)]
        public string CaseCategoryLayer2Name {
            set { cateNameStr = value; }
            get
            {
                if (string.IsNullOrEmpty(cateNameStr))
                {
                    cateNameStr = string.Empty;
                }
                else
                {
                    cateNameStr.Trim();
                }
                return cateNameStr;
            }
        }
        /// <summary>
        /// 案件第三層類別 Id
        /// </summary>
        [MaxLength(100)]
        public string CaseCategoryLayer3Id
        {
            set { cateIdStr = value; }
            get
            {
                if (string.IsNullOrEmpty(cateIdStr))
                {
                    cateIdStr = string.Empty;
                }
                else
                {
                    cateIdStr.Trim();
                }
                return cateIdStr;
            }
        
        }

        /// <summary>
        /// 案件第三層類別 名稱
        /// </summary>
        [MaxLength(20)]
        public string CaseCategoryLayer3Name {

            set { cateNameStr = value; }
            get
            {
                if (string.IsNullOrEmpty(cateNameStr))
                {
                    cateNameStr = string.Empty;
                }
                else
                {
                    cateNameStr.Trim();
                }
                return cateNameStr;
            }

        }
        /// <summary>
        /// 保單編號
        /// </summary>
        [MaxLength(200)]
        public string PolicyNo { get; set; }

        /// <summary>
        /// 關聯保單編號
        /// </summary>
        [MaxLength(200)]
        public string RelatedPolicyNo { get; set; }

        /// <summary>
        /// 單據期限
        /// </summary>
        public DateTime? Timeliness { get; set; }

        /// <summary>
        /// 單據狀態 Id(協辦中/處理中/結案)
        /// </summary>
        [MaxLength(100)]
        public string StatusId { get; set; }

        /// <summary>
        /// 單據狀態名稱(協辦中/處理中/結案)
        /// </summary>
        [MaxLength(100)]
        public string StatusName { get; set; }
        /// <summary>
        /// 單據狀態描述
        /// </summary>
        [MaxLength(100)]
        public string ContactStatusDesc { get; set; }

        /// <summary>
        /// 協辦Id
        /// </summary>
        [MaxLength(100)]
        public string CoOwnerId { get; set; }

        /// <summary>
        /// 協辦姓名
        /// </summary>
        [MaxLength(100)]
        public string CoOwnerName { get; set; }

        /// <summary>
        /// 回覆文件主旨
        /// </summary>
        [MaxLength(600)]
        public string Header { get; set; }

        /// <summary>
        /// 回覆文件內文
        /// </summary>
        [MaxLength(-1)]
        public string Body { get; set; }

        /// <summary>
        /// 目前單據擁有者
        /// </summary>
        public string CurrentEditorId { get; set; }
        /// <summary>
        /// 目前單據擁有者姓名
        /// </summary>
        public string CurrentEditorName { get; set; }

        /// <summary>
        /// 設定或獲取PDF資料 使用JSON儲存 （套印用）
        /// </summary>
        [MaxLength(8000)]
        public string FormLog { get; set; }
        /// <summary>
        /// 設定區塊是否顯示
        /// </summary>
        
        public bool? IsBlockDisplay { get; set; }

        /// <summary>
        /// 附件（使用逗號分開" , "）
        /// </summary>
        [MaxLength(8000)]
        public string Attachment {


            set { attachStr = value; }
            get
            {
                List<string> _tmpList = new List<string>();
                if (!string.IsNullOrEmpty(attachStr))
                {
                    var attachAry = attachStr.Split(",");

                    AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                    SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
                    foreach (var i in attachAry)
                    {
                        _tmpList.Add(sysSetting.WebUrl + "/" + i);
                    }

                }


                return string.Join(",", _tmpList);
            }
        }

        /// <summary>
        /// 被聯絡人 Id
        /// </summary>
        [MaxLength(100)]
        public string ContactId { get; set; }

        /// <summary>
        /// 被聯絡人姓名
        /// </summary>
        [MaxLength(100)]
        public string ContactName { get; set; }

        /// <summary>
        /// 被聯絡人電郵
        /// </summary>
        [MaxLength(100)]
        public string ContactEmail { get; set; }

        /// <summary>
        /// 被聯絡人電話
        /// </summary>
        [MaxLength(100)]
        public string ContactPhone { get; set; }

        /// <summary>
        /// 客戶姓名
        /// </summary>
        [MaxLength(100)]
        public string CustomerName { get; set; }
        /// <summary>
        /// 車牌號碼
        /// </summary>
        public string PlateNumber { get; set; }
        /// <summary>
        /// 聯絡人關係
        /// </summary>
        public string ContactRelation { get; set; }
        /// <summary>
        /// 聯絡時間
        /// </summary>
        public DateTime? ContactTime { get; set; }

        /// <summary>
        /// 聯絡狀態
        /// </summary>
        [MaxLength(200)]
        public string ContactStatus { get; set; }
        /// <summary>
        /// 聯絡者特殊備註
        /// </summary>
        public string ContactSpecialNotes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AnswerTime { get; set; }

        /// <summary>
        /// 生效碼 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }
        /// <summary>
        ///  單據建立者姓名
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }

        /// <summary>
        /// 協辦者部門 Id
        /// </summary>
        [MaxLength(100)]
        public string CoDeptId { get; set; }
        /// <summary>
        ///  重要性
        /// </summary>
        public bool? IsHot { get; set; }
        /// <summary>
        ///  通知
        /// </summary>
        public bool? IsNotified { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }
        /// <summary>
        ///  是否閱讀
        /// </summary>
        public string IsRead { get; set; }
    }
}
