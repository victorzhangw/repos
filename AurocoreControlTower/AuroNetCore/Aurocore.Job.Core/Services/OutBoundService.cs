using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Json;
using Aurocore.Commons.Pages;
using Aurocore.Security.Models;
using Aurocore.Commons.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.Security.IRepositories;
using Aurocore.Commons.Helpers;
using System.Security.Claims;
using System.Linq;
using Aurocore.Commons.Extensions;
using Microsoft.AspNetCore.Hosting;
using Aurocore.WorkOrders.Enums;
using Aurocore.Security.IServices;
using Aurocore.Commons.Log;

namespace Aurocore.WorkOrders.Services
{
	/// <summary>
	/// 案件服務接口實現
	/// </summary>
	public class OutBoundService: BaseService<OutBound,OutBoundOutputDto, string>, IOutBoundService
	{
		private readonly IOutBoundRepository _repository;


		public OutBoundService(IOutBoundRepository repository) : base(repository)
		{
			_repository = repository;
		}
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public override async Task<PageResult<OutBoundOutputDto>> FindWithPagerAsync(SearchInputDto<OutBound> search)
		{
			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			if (string.IsNullOrEmpty(search.Filter.EnabledMark.ToString()))
			{
				search.Filter.EnabledMark = true;
			}
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<OutBound> list = await _repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);

			PageResult<OutBoundOutputDto> pageResult = new PageResult<OutBoundOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<OutBoundOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount,


			};
			return pageResult;
		}


	}
}