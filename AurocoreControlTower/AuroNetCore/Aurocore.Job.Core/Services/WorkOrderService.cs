using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Json;
using Aurocore.Commons.Pages;
using Aurocore.Security.Models;
using Aurocore.Commons.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.Security.IRepositories;
using Aurocore.Commons.Helpers;
using System.Security.Claims;
using System.Linq;
using Aurocore.Commons.Extensions;
using Microsoft.AspNetCore.Hosting;
using Aurocore.WorkOrders.Enums;
using Aurocore.Security.IServices;
using Aurocore.CMS.IRepositories;
using Aurocore.Commons.Log;

namespace Aurocore.WorkOrders.Services
{
	/// <summary>
	/// 案件服務接口實現
	/// </summary>
	public class WorkOrderService : BaseService<WorkOrder, WorkOrderOutputDto, string>, IWorkOrderService
	{
		private readonly IWorkOrderRepository _repository;
		private readonly IWorkOrderDetailRepository _idetailrepository;
		private readonly IWorkOrderLogRepository _iorderlogrepository;
		private readonly IUserRepository _iuserRepository;
		private readonly IItemsDetailRepository _itemsDetailRepository;
		private readonly IOutBoundNameListRepository _ioutboundnamelistRepository;
		private readonly IOutBoundRepository _ioutboundRepository;
		private readonly IOrganizeRepository _iorganizeRepository;
		private readonly IEstimateRepository _estimateRpository;
		public WorkOrderService(IWorkOrderRepository repository, IUserRepository userRepository,
			IWorkOrderDetailRepository detailRepository, IWorkOrderLogRepository workOrderLogRepository,
			IOutBoundNameListRepository nameListRepository, IOutBoundRepository outBoundRepository, IOrganizeRepository organizeRepository,
			IItemsDetailRepository itemsDetailRepository, IEstimateRepository estimateRepository) : base(repository)
		{
			_repository = repository;
			_iuserRepository = userRepository;
			_idetailrepository = detailRepository;
			_iorderlogrepository = workOrderLogRepository;
			_itemsDetailRepository = itemsDetailRepository;
			_ioutboundnamelistRepository = nameListRepository;
			_ioutboundRepository = outBoundRepository;
			_iorganizeRepository = organizeRepository;
			_estimateRpository = estimateRepository;
		}
		/// <summary>
		/// 取得郵件
		/// </summary>
		/// <param name="ItemCode"></param>
		/// <param name="hostingEnvironment"></param>
		/// <returns></returns>
		public async Task<List<string>> RetrieveEmail(string ItemCode, IWebHostEnvironment hostingEnvironment)
		{

			List<WorkOrder> workOrders = await _repository.RetrieveEmail(ItemCode, hostingEnvironment);
			List<string> stringList = new List<string>();
			if (HttpContextHelper.HttpContext == null)
			{
				return stringList;
			}

			//string userId = claimlist[0].Value;
			if (workOrders.Count != 0 && workOrders != null)
			{
				await repository.AddRangeAsync(workOrders).ConfigureAwait(false);
			}
			else
			{
				return stringList;
			}
			foreach (var item in workOrders)
			{
				stringList.Add(item.Id);
			}
			return stringList;
		}
		/// <summary>
		/// 匯入 Memo
		/// </summary>
		/// <param name="memo"></param>
		/// <returns></returns>
		public async Task<List<string>> ImportMemo(Memo memo)
		{
			List<WorkOrder> workOrders = _repository.ImportMemo(memo);
			List<string> stringList = new List<string>();
			if (HttpContextHelper.HttpContext == null)
			{
				return stringList;
			}

			//string userId = claimlist[0].Value;
			if (workOrders.Count != 0 && workOrders != null)
			{
				//repository.BulkInsert(workOrders, "CRM_WorkOrder");
				await repository.AddRangeAsync(workOrders).ConfigureAwait(false);

			}
			else
			{
				return stringList;
			}

			foreach (var item in workOrders)
			{
				stringList.Add(item.Id);
			}
			return stringList;
		}
		/*
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		/*
		public  async Task<PageResult<WorkOrderOutputDto>> FindWithPagerSearchAsync(SeachWorkOrderModel search)
		{
			
			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			string forMenuWhere = GetDataPrivilege(false);
			string forMenuWhere2 = GetDataPrivilege(false);
			string forMenuWhere3 = GetDataPrivilege(false);
			string forMenuWhere4 = GetDataPrivilege(false);
			string formWhere = string.Format("ItemName='{0}' ", nameof(WorkOrderType.待辦單據)); ;
			var formItems = await _itemsDetailRepository.GetWhereAsync(formWhere).ConfigureAwait(false);
			string formWhere2 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.全部案件)); ;
			var formItems2 = await _itemsDetailRepository.GetWhereAsync(formWhere2).ConfigureAwait(false);
			string formWhere3 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.Memo)); ;
			var formItems3 = await _itemsDetailRepository.GetWhereAsync(formWhere3).ConfigureAwait(false);
			
			List<WorkOrderMenu> workOrderMenu1 = null;
			List<WorkOrderMenu> workOrderMenu2 = null;
			// 檢查狀態碼
			if (!string.IsNullOrEmpty(search.Filter.StatusName))
			{
				if (search.Filter.StatusName != nameof(WorkflowStatus.全部狀態))
				{
					where += " and StatusName='" + search.Filter.StatusName + "'";
				}

				
			}
			switch (search.Filter.StatusName)
			{
				case nameof(WorkflowStatus.已結案):
					if (!string.IsNullOrEmpty(search.Filter.CoOwnerId) || !string.IsNullOrEmpty(search.Filter.OwnerId))
					{
						where += " and ( OwnerId = '" + search.Filter.OwnerId + "' or CoOwnerId = '" + search.Filter.OwnerId + "')";
						where += " AND CONVERT(varchar(10), CaseCloseTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					}
					break;
				default:
					// 檢查案件類別,若為全部案件,則取消條件
					if (!string.IsNullOrEmpty(search.Filter.FormTypeId) && (search.Filter.FormTypeId != formItems2.Id))
					{
						where += " and FormTypeId='" + search.Filter.FormTypeId + "'";
					}
					if (!string.IsNullOrEmpty(search.Filter.IsHot.ToString()))
					{
						where += " and IsHot=" + Convert.ToInt32(search.Filter.IsHot);
					}
					if (!string.IsNullOrEmpty(search.Filter.CoOwnerId) || !string.IsNullOrEmpty(search.Filter.OwnerId))
					{
						where += " and ( OwnerId like '%" + search.Filter.OwnerId + "%' or CoOwnerId like '%" + search.Filter.OwnerId + "%')";
					}

					if (!string.IsNullOrEmpty(search.Filter.CaseOpenTime.ToString()) || !string.IsNullOrEmpty(search.Filter.CaseCloseTime.ToString()))
					{
						string _date1 = "'" + search.Filter.CaseOpenTime.ToString("yyyy/MM/dd", "yyyy/MM/dd") + " 00:00:00'";
						string _date2 = string.Empty;
						if (string.IsNullOrEmpty(search.Filter.CaseCloseTime.ToString()))
						{
							_date2 = "'" + search.Filter.CaseOpenTime.ToString("yyyy/MM/dd", "yyyy/MM/dd") + " 23:59:59'";

						}
						else
						{
							_date2 = "'" + search.Filter.CaseCloseTime.ToString("yyyy/MM/dd", "yyyy/MM/dd") + " 23:59:59'";
						}

						where += " and (CaseOpenTime between " + _date1 + " and " + _date2 + ")  ";
					}
					if (!string.IsNullOrEmpty(search.Keywords))
					{
						where += $" AND (ApplicantEmail like'%{search.Keywords}%' ";
						where += $" OR ApplicantId like'%{search.Keywords}%' ";
						where += $" OR ApplicantMobilePhone like'%{search.Keywords}%' ";
						where += $" OR SourceId like '%{search.Keywords}%' ";
						where += $" OR Itag like '%{search.Keywords}%' ";
						where += $" OR ApplicantName like'%{search.Keywords}%' ";
						where += $" OR Id like'%{search.Keywords}%') ";
					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantEmail))
					{
						where += $" AND ApplicantEmail like'%{search.Filter.ApplicantEmail}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantName))
					{
						where += $" AND ApplicantName like'%{search.Filter.ApplicantName}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantId))
					{
						where += $" AND ApplicantId like'%{search.Filter.ApplicantId}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantMobilePhone))
					{
						where += $" AND ApplicantMobilePhone like'%{search.Filter.ApplicantMobilePhone}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.InsuranceTypeName))
					{
						where += $" AND InsuranceTypeName like '%{search.Filter.InsuranceTypeName}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.SourceId))
					{
						where += $" AND SourceId like '%{search.Filter.SourceId}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.Itag))
					{
						where += $" AND Itag like '%{search.Filter.Itag}%'";

					}
					break;

			}
			
			//組選單統計數字 SQL Where //
			if (!string.IsNullOrEmpty(search.Filter.StatusName))
			{
				forMenuWhere += " and StatusName='" + search.Filter.StatusName + "'";
				if(search.Filter.StatusName== "等待分配")
				{
					forMenuWhere2 += " and (StatusName='等待分配') ";
					forMenuWhere2 += " and (CoOwnerId ='' OR  CoOwnerId=null) ";
					forMenuWhere2 += " and (CoDeptId='"+search.DepartmentId+"') ";
				}
				else if (search.Filter.StatusName == "處理中")
				{
					forMenuWhere2 += " and (StatusName='處理中' OR StatusName='協辦中')";
					
				}
				else
				{
					forMenuWhere2 += " and StatusName='" + search.Filter.StatusName + "'";
				}
				
				forMenuWhere3 += " and StatusName='處理中'";
				forMenuWhere4 += " and StatusName='已結案'";
				if (!string.IsNullOrEmpty(search.Filter.CoOwnerId) || !string.IsNullOrEmpty(search.Filter.OwnerId))
				{
					forMenuWhere3 = GetDataPrivilege(false);
					forMenuWhere4 = GetDataPrivilege(false);
					forMenuWhere3 += " AND StatusName='已結案' AND CONVERT(varchar(10), LastModifyTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					forMenuWhere4 += " AND StatusName='已結案' AND CONVERT(varchar(10), LastModifyTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					forMenuWhere += " and ( OwnerId = '" + search.Filter.OwnerId + "' or CoOwnerId = '" + search.Filter.CoOwnerId + "')";
					forMenuWhere2 += " and ( CurrentEditorId = '" + search.Filter.OwnerId + "' )";
					forMenuWhere3 += " and ( OwnerId = '" + search.Filter.OwnerId + "' or CoOwnerId = '" + search.Filter.CoOwnerId + "')";
					forMenuWhere4 += " and ( CurrentEditorId = '" + search.Filter.OwnerId + "' )";

				}
				string menuSql1 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere + "GROUP BY FormTypeId,FormTypeName UNION " +
				" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
				" WHERE " + forMenuWhere + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
				" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
				" WHERE " + forMenuWhere + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
				" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
				" WHERE " + forMenuWhere2;

				string menuSql2 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere3 + "GROUP BY FormTypeId,FormTypeName UNION " +
				" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
				" WHERE " + forMenuWhere3 + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
				" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
				" WHERE " + forMenuWhere3 + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
				" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
				" WHERE " + forMenuWhere4;

				workOrderMenu1 =   _repository.GetWorkOrderMenuInfo(menuSql1);
				workOrderMenu2 =   _repository.GetWorkOrderMenuInfo(menuSql2);
				int totalCounts=0;
				int totalCounts2=0;
				var workOrderAsArray = Enum.GetNames(typeof(WorkOrderType));
				foreach (var item in workOrderAsArray)
				{
					if(item != "Memo" && item != "待辦單據" && item != "OutBound")
					{
						foreach(var wm1 in workOrderMenu1)
						{
							if (wm1.FormTypeName == item)
							{
								totalCounts += Int32.Parse(wm1.FormCounts);
							}
						}
						foreach (var wm2 in workOrderMenu2)
						{
							if (wm2.FormTypeName == item)
							{
								totalCounts2 += Int32.Parse(wm2.FormCounts);
							}
						}
					}
				}
					//
					foreach (var item in workOrderMenu1)
					{

						if (item.FormTypeName != "Memo" && item.FormTypeName != "待辦單據" && item.FormTypeName != "OutBound")
						{
							if (workOrderMenu2 != null)
							{
								foreach (var subitem in workOrderMenu2)
								{
									if (item.FormTypeId == subitem.FormTypeId)
									{
										item.FormCounts2 = subitem.FormCounts;
										totalCounts2 += Int32.Parse(subitem.FormCounts);

									}


								}
							}
							totalCounts += Int32.Parse(item.FormCounts);
						}


					}//
					if (formItems2.Id == search.Filter.FormTypeId)
				{
					workOrderMenu1.Add(new WorkOrderMenu()
					{
						
						FormCounts = totalCounts.ToString(),
						FormCounts2 = totalCounts2.ToString(),


					});
				}
				else
				{
					workOrderMenu1.Add(new WorkOrderMenu()
					{
						FormTypeId = formItems2.Id,
						FormTypeName = formItems2.ItemName,
						FormCounts = totalCounts.ToString(),
						FormCounts2 = totalCounts2.ToString(),


					});
				}
				

			}
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<WorkOrder> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);

			PageResult<WorkOrderOutputDto> pageResult = new PageResult<WorkOrderOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<WorkOrderOutputDto>(),
				Context = workOrderMenu1,
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount,
				
				
			};
			return pageResult;
		}
		*/
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public async Task<PageResult<WorkOrderOutputDto>> FindWithPagerSearchAsync(SeachWorkOrder search)
		{

			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			string forMenuWhere = GetDataPrivilege(false);
			string forMenuWhere2 = GetDataPrivilege(false);
			string forMenuWhere3 = GetDataPrivilege(false);
			string forMenuWhere4 = GetDataPrivilege(false);
			string formWhere = string.Format("ItemName='{0}' ", nameof(WorkOrderType.待辦單據));
			var formItems = await _itemsDetailRepository.GetWhereAsync(formWhere).ConfigureAwait(false);
			string formWhere2 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.全部案件));
			var formItems2 = await _itemsDetailRepository.GetWhereAsync(formWhere2).ConfigureAwait(false);
			string formWhere3 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.Memo));
			var formItems3 = await _itemsDetailRepository.GetWhereAsync(formWhere3).ConfigureAwait(false);
			string formWhere4 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.汽車險案件));
			var formItems4 = await _itemsDetailRepository.GetWhereAsync(formWhere4).ConfigureAwait(false);
			string menuSql1 = string.Empty;
			string menuSql2 = string.Empty;
			List<WorkOrderMenu> workOrderMenu1 = null;
			List<WorkOrderMenu> workOrderMenu2 = null;
			// 檢查狀態碼
			if (!string.IsNullOrEmpty(search.Filter.StatusName))
			{
				switch (search.Filter.StatusName)
				{
					case nameof(WorkflowStatus.等待分配):
						where += $" AND (StatusName='{search.Filter.StatusName}' ) ";
						break;
					case nameof(WorkflowStatus.全部狀態):
						where = where;
						break;
					default:
						where += $" AND (StatusName='{search.Filter.StatusName}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}') ";
						break;


				}

			}
			switch (search.Filter.StatusName)
			{
				case nameof(WorkflowStatus.已結案):
					if (!string.IsNullOrEmpty(search.Filter.CoOwnerId) || !string.IsNullOrEmpty(search.Filter.OwnerId))
					{
						where += " and ( OwnerId = '" + search.Filter.OwnerId + "' or CoOwnerId = '" + search.Filter.OwnerId + "')";
						where += " AND CONVERT(varchar(10), CaseCloseTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					}
					break;
				default:
					// 檢查案件類別,若為全部案件,則取消條件
					if (!string.IsNullOrEmpty(search.Filter.FormTypeId) && (search.Filter.FormTypeId != formItems2.Id))
					{
						where += " and FormTypeId='" + search.Filter.FormTypeId + "'";
					}
					if (!string.IsNullOrEmpty(search.Filter.IsHot.ToString()))
					{
						where += " and IsHot=" + Convert.ToInt32(search.Filter.IsHot);
					}
					if (!string.IsNullOrEmpty(search.Filter.CoOwnerId) || !string.IsNullOrEmpty(search.Filter.OwnerId))
					{
						// 搜索狀態 0：全部 1：個人
						if (string.IsNullOrEmpty(search.EnCode) || (search.EnCode == "1"))
						{
							where += " and ( OwnerId like '%" + search.Filter.OwnerId + "%' or CoOwnerId like '%" + search.Filter.OwnerId + "%')";
						}

					}

					if (!string.IsNullOrEmpty(search.Filter.CaseOpenTime) || !string.IsNullOrEmpty(search.Filter.CaseCloseTime))
					{
						string _date1 = "'" + search.Filter.CaseOpenTime + " 00:00:00'";
						string _date2 = string.Empty;
						if (string.IsNullOrEmpty(search.Filter.CaseCloseTime.ToString()))
						{
							_date2 = "'" + search.Filter.CaseOpenTime + " 23:59:59'";

						}
						else
						{
							_date2 = "'" + search.Filter.CaseCloseTime + " 23:59:59'";
						}

						where += " and (CaseOpenTime between " + _date1 + " and " + _date2 + ")  ";
					}
					if (!string.IsNullOrEmpty(search.Keywords))
					{
						where += $" AND (ApplicantEmail like'%{search.Keywords}%' ";
						where += $" OR ApplicantId like'%{search.Keywords}%' ";
						where += $" OR ApplicantMobilePhone like'%{search.Keywords}%' ";
						where += $" OR ApplicantLocalPhone like'%{search.Keywords}%' ";
						where += $" OR SourceId like '%{search.Keywords}%' ";
						where += $" OR Itag like '%{search.Keywords}%' ";
						where += $" OR ApplicantName like'%{search.Keywords}%' ";
						where += $" OR ApplicantEmail like'%{search.Keywords}%' ";
						where += $" OR ApplicantPolicyNO like'%{search.Keywords}%' ";
						where += $" OR InsuranceTypeName like'%{search.Keywords}%' ";
						where += $" OR OwnerName like'%{search.Keywords}%' ";
						where += $" OR Id like'%{search.Keywords}%') ";
					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantEmail))
					{
						where += $" AND ApplicantEmail like'%{search.Filter.ApplicantEmail}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantName))
					{
						where += $" AND ApplicantName like'%{search.Filter.ApplicantName}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantId))
					{
						where += $" AND ApplicantId like'%{search.Filter.ApplicantId}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantMobilePhone))
					{
						where += $" AND ApplicantMobilePhone like'%{search.Filter.ApplicantMobilePhone}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.ApplicantPolicyNO))
					{
						where += $" AND ApplicantPolicyNO like'%{search.Filter.ApplicantPolicyNO}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.InsuranceTypeName))
					{
						where += $" AND InsuranceTypeName like '%{search.Filter.InsuranceTypeName}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.SourceId))
					{
						where += $" AND SourceId like '%{search.Filter.SourceId}%'";

					}
					if (!string.IsNullOrEmpty(search.Filter.Itag))
					{
						where += $" AND Itag like '%{search.Filter.Itag}%'";

					}
					break;

			}
			switch (search.Filter.StatusName)
			{
				case nameof(WorkflowStatus.等待分配):
					forMenuWhere += " and StatusName='" + search.Filter.StatusName + "'";
					forMenuWhere2 += $" and (StatusName='{nameof(WorkflowStatus.等待分配)}') ";
					forMenuWhere2 += " and (CoOwnerId ='' OR  CoOwnerId=null) ";
					forMenuWhere2 += " and (CoDeptId='" + search.DepartmentId + "') ";
					forMenuWhere3 += $" and (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}')";
					forMenuWhere4 += $" and (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}')";

					menuSql2 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere2;

					menuSql1 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere3 + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere4;


					break;
				default:

					forMenuWhere3 = GetDataPrivilege(false);
					forMenuWhere4 = GetDataPrivilege(false);


					//判斷個人單據量
					forMenuWhere += " and ( OwnerId = '" + search.Filter.OwnerId + "' or CoOwnerId = '" + search.Filter.CoOwnerId + "')";
					forMenuWhere2 += " and ( CurrentEditorId = '" + search.Filter.OwnerId + "' )";
					forMenuWhere3 += " and ( OwnerId = '" + search.Filter.OwnerId + "' or CoOwnerId = '" + search.Filter.CoOwnerId + "')";
					forMenuWhere4 += " and ( CurrentEditorId = '" + search.Filter.OwnerId + "' )";
					//判斷單據狀態
					forMenuWhere += $" AND (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}')";
					forMenuWhere2 += $" AND (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}')";
					forMenuWhere3 += $" AND StatusName='{nameof(WorkflowStatus.已結案)}' AND CONVERT(varchar(10), LastModifyTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					forMenuWhere4 += $" AND StatusName='{nameof(WorkflowStatus.已結案)}' AND CONVERT(varchar(10), LastModifyTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					menuSql1 = @$"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere2;

					menuSql2 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere3 + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere4;
					break;

			}

			workOrderMenu1 = _repository.GetWorkOrderMenuInfo(menuSql1);
			workOrderMenu2 = _repository.GetWorkOrderMenuInfo(menuSql2);
			List<WorkOrderMenu> resultworkOrderMenu = new List<WorkOrderMenu>();
			int totalCounts = 0;
			int totalCounts2 = 0;
			var workOrderAsArray = Enum.GetNames(typeof(WorkOrderType));
			foreach (var item in workOrderAsArray)
			{
				if (item != "Memo" && item != "待辦單據" && item != "OutBound" && item != nameof(WorkOrderType.汽車險案件))
				{
					foreach (var wm1 in workOrderMenu1)
					{
						if (wm1.FormTypeName == item)
						{
							totalCounts += Int32.Parse(wm1.FormCounts);
						}

					}
					foreach (var wm2 in workOrderMenu2)
					{
						if (wm2.FormTypeName == item)
						{
							totalCounts2 += Int32.Parse(wm2.FormCounts);
						}

					}

				}
			}
			foreach (var item in workOrderAsArray)
			{
				string _count1 = "0";
				string _count2 = "0";
				string _formtypeID = string.Empty;
				foreach (var wm1 in workOrderMenu1)
				{
					if (wm1.FormTypeName == item)
					{
						_count1 = wm1.FormCounts;
						_formtypeID = wm1.FormTypeId;

					}
				}
				foreach (var wm2 in workOrderMenu2)
				{
					if (wm2.FormTypeName == item)
					{
						_count2 = wm2.FormCounts;
						_formtypeID = wm2.FormTypeId;
					}
				}
				if (string.IsNullOrEmpty(_formtypeID))
				{
					string _formWhere = string.Format("ItemName='{0}' ", item); ;
					var _formItems = await _itemsDetailRepository.GetWhereAsync(_formWhere).ConfigureAwait(false);
					if (_formItems != null)
					{
						_formtypeID = _formItems.Id;
						if (_formItems.ItemName == nameof(WorkOrderType.全部案件))
						{
							_count1 = totalCounts.ToString();
							_count2 = totalCounts2.ToString();

						}

					}

				}
				resultworkOrderMenu.Add(new WorkOrderMenu()
				{
					FormTypeId = _formtypeID,
					FormTypeName = item,
					FormCounts = _count1,
					FormCounts2 = _count2,


				});

			}
			/*
			if (formItems2.Id == search.Filter.FormTypeId)
			{
				resultworkOrderMenu.Add(new WorkOrderMenu()
				{

					FormCounts = totalCounts.ToString(),
					FormCounts2 = totalCounts2.ToString(),


				});
			}
			else
			{
				resultworkOrderMenu.Add(new WorkOrderMenu()
				{
					FormTypeId = formItems2.Id,
					FormTypeName = formItems2.ItemName,
					FormCounts = totalCounts.ToString(),
					FormCounts2 = totalCounts2.ToString(),


				});
			}*/
			/*
			if (search.Filter.StatusName == nameof(WorkflowStatus.等待分配))
			{
				foreach(var item2 in workOrderMenu2)
				{
					if(item2.FormTypeName== "待辦單據")
					{
						foreach(var item in workOrderMenu1)
						{
							if (item.FormTypeName == "待辦單據")
							{
								item.FormCounts2 = item2.FormCounts;
							}

						}
					}
				}
			}*/


			/***組選單統計數字 SQL Where **/

			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<WorkOrderOutputDto> list = new List<WorkOrderOutputDto>();

			if (search.Filter.FormTypeId == "2204217392665")
			{
				var _wolist = await _repository.FindWithPagerRelationWorkOrderDetailAsync(where, pagerInfo, search.Sort, order);

				list = _wolist.MapTo<WorkOrderOutputDto>();
				foreach (var item in list)
				{
					/* 決定是否開啟新視窗 */
					if (item.ParentFormTypeName == nameof(WorkOrderType.汽車險案件))
					{
						item.IsNewWindow = true;
					}
					
					if (!string.IsNullOrEmpty(item.CurrentEditorName))
					{
						var _user = _iuserRepository.Get(item.CurrentEditorName);
						if (!string.IsNullOrEmpty(item.CreatorUserId)){
                            var _creatoruser = _iuserRepository.Get(item.CreatorUserId);
                            item.CreatorUserName = _creatoruser.RealName;
                        }

                        
						item.CurrentEditorName = _user.RealName;
						
						//item.CurrentEditorDept = _user.DepartmentName;
						item.CurrentEditorDept = _iorganizeRepository.Get(_user.DepartmentId).FullName;
					}

					else
					{
                        if (!string.IsNullOrEmpty(item.OwnerId))
                        {
							var _user = _iuserRepository.Get(item.OwnerId);
                            if (!string.IsNullOrEmpty(item.CreatorUserId))
                            {
                                var _creatoruser = _iuserRepository.Get(item.CreatorUserId);
                                item.CreatorUserName = _creatoruser.RealName;
                            }
                            item.CurrentEditorName = _user.RealName;
                            
                            item.CurrentEditorDept = _iorganizeRepository.Get(_user.DepartmentId).FullName; ;
						}
						

					}
				}

			}
			else
			{

				var _wolist = await _repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
				list = _wolist.MapTo<WorkOrderOutputDto>();
			}
			/* 透過單據反向變動案件擁有者
			//list = ChangeWorkOrderCurrentOwnerDisplay(list);
			*/
			PageResult<WorkOrderOutputDto> pageResult = new PageResult<WorkOrderOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list,
				Context = resultworkOrderMenu,
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount,


			};
			return pageResult;
		}
		/// <summary>
		/// 案件編號
		/// </summary>
		/// <param name="WorkOrderId"></param>
		/// <returns></returns>
		public List<WorkOrderTreeOutputDto> WorkOrderTree(string WorkOrderId)
		{
			List<WorkOrderTreeOutputDto> workOrderTrees = new();
			string orderDetailBriefSql = $@"SELECT T1.Id,T1.WorkOrderId,T1.ResponseTypeName,T1.InsuranceTypeName, 
					T1.CaseCategoryLayer1Name,T1.CaseCategoryLayer2Name,T1.PolicyNo,
					T1.Timeliness,T1.StatusName,T1.Header,T1.Body,T1.CreatorTime,
					T1.ContactName,T1.ContactEmail,T1.ContactPhone,
					T1.ContactStatus,T1.ContactStatusDesc,T2.RealName,T1.Attachment
					FROM CRM_WorkOrderDetail AS T1 
					INNER JOIN Sys_User T2 ON T1.CreatorUserID=T2.Id
					WHERE T1.WorkOrderId='{WorkOrderId}' AND T1.EnabledMark=1
					ORDER BY T1.CreatorTime";



			List<WorkOrderBrief> detailList = _idetailrepository.GetWorkOrderBrief(orderDetailBriefSql);
			if (detailList != null)
			{

				foreach (var detail in detailList)
				{

					string orderLogBriefSql = $@"SELECT T1.Id,T1.WorkOrderDetailId,ReplyStatus,ReplyMessage,T2.RealName ,T1.CreatorTime 
					FROM CRM_WorkOrderLog T1
					INNER JOIN Sys_User T2 ON T1.CreatorUserID=T2.Id
					WHERE T1.WorkOrderId='{WorkOrderId}' AND T1.WorkOrderDetailId='{detail.Id}' AND T1.EnabledMark=1
					ORDER BY T1.CreatorTime ";
					List<WorkOrderLogBrief> logList = _iorderlogrepository.GetWorkOrderLogBrief(orderLogBriefSql);
					if (logList != null && logList.Count != 0)
					{

						Dtos.Children children = new Children();
						children.LogOutput = logList;
						workOrderTrees.Add(new WorkOrderTreeOutputDto { Id = WorkOrderId, orderBrief = detail, rowCount = logList.Count().ToString(), label = "1", children = children });
					}
					else
					{

						workOrderTrees.Add(new WorkOrderTreeOutputDto { Id = WorkOrderId, orderBrief = detail, rowCount = "0", label = "0" });
					}

				}

			}
			return workOrderTrees;
		}
		public async Task<WorkOrderRealateList> WorkOrderTypeTree(string WorkOrderId)
		{
			WorkOrderRealateList workOrderRealateList = new();

			string _internalwhere = string.Format("ItemCode='{0}' OR ItemCode='{1}' OR ItemCode='{2}' OR ItemCode='{3}'",
				nameof(ResponseType.一般問題單), nameof(ResponseType.批改申請單), nameof(ResponseType.補發申請單), nameof(ResponseType.核保審批));

			string _outterwhere = string.Format("ItemCode='{0}' OR ItemCode='{1}' OR  ItemCode='{2}' OR  ItemCode='{3}' OR  ItemCode='{4}'",
				nameof(ResponseType.ExternalEMail), nameof(ResponseType.ExternalSMS), nameof(ResponseType.話務記錄), nameof(ResponseType.Inbound話務記錄), nameof(ResponseType.OutBound話務記錄));
			string _memowhere = string.Format("ItemName='{0}' ", nameof(ResponseType.ReplyMemotoSender));
			string _parentWhere = string.Format("ItemName='{0}' ", nameof(WorkOrderType.待辦單據));
			IEnumerable<ItemsDetail> internalItems = await _itemsDetailRepository.GetListWhereAsync(_internalwhere);
			IEnumerable<ItemsDetail> outterItems = await _itemsDetailRepository.GetListWhereAsync(_outterwhere);
			IEnumerable<ItemsDetail> memoItems = await _itemsDetailRepository.GetListWhereAsync(_memowhere);
			ItemsDetail ParentFormItems = await _itemsDetailRepository.GetWhereAsync(_parentWhere);
			string _internalCond = "ResponseTypeId =";
			string _outterCond = "ResponseTypeId =";
			string _memoCond = "ResponseTypeId =";
			if (internalItems.Count() > 0 && outterItems.Count() > 0 && memoItems.Count() > 0)
			{

				for (int i = 0; i < internalItems.Count(); i++)
				{
					if (i == 0)
					{
						_internalCond += "'" + internalItems.ElementAt(i).Id + "'";
					}
					else
					{

						_internalCond += $"OR ResponseTypeId= '{internalItems.ElementAt(i).Id}'";
					}

				}
				for (int i = 0; i < outterItems.Count(); i++)
				{
					if (i == 0)
					{
						_outterCond += "'" + outterItems.ElementAt(i).Id + "'";
					}
					else
					{

						_outterCond += $"OR ResponseTypeId= '{outterItems.ElementAt(i).Id}'";
					}

				}
				_memoCond += $"'{memoItems.First().Id}'";
			}
			else
			{
				Log4NetHelper.Error("資料字典無此單據");
				throw new Exception("資料字典無此單據");

			}




			string internalBriefSql = $@"SELECT T1.Id,T1.WorkOrderId,'{ParentFormItems.Id}' as 'ParentResponseTypeId','{ParentFormItems.ItemName}' as 'ParentResponseTypeName',
					T1.ResponseTypeId,T1.ResponseTypeName,T1.InsuranceTypeName, 
					T1.CaseCategoryLayer1Name,T1.CaseCategoryLayer2Name,T1.PolicyNo,
					T1.Timeliness,T1.StatusName,T1.Header,T1.Body,T1.CreatorTime,
					T1.ContactName,T1.ContactEmail,T1.ContactPhone,T1.AnswerTime,
					T1.ContactStatus,T1.ContactStatusDesc,T2.RealName,T3.RealName as CoOwnerName,T1.Attachment
					FROM CRM_WorkOrderDetail AS T1 
					INNER JOIN Sys_User T2 ON T1.CreatorUserID=T2.Id
					LEFT JOIN Sys_User T3 ON T1.CoOwnerId=T3.Id
					WHERE T1.WorkOrderId='{WorkOrderId}' AND T1.EnabledMark=1
					AND({_internalCond} )
					ORDER BY T1.CreatorTime DESC";
			string outterBriefSql = $@"SELECT T1.Id,T1.WorkOrderId,'{ParentFormItems.Id}' as 'ParentResponseTypeId','{ParentFormItems.ItemName}' as 'ParentResponseTypeName',
					T1.ResponseTypeId,T1.ResponseTypeName,T1.InsuranceTypeName, 
					T1.CaseCategoryLayer1Name,T1.CaseCategoryLayer2Name,T1.PolicyNo,
					T1.Timeliness,T1.StatusName,T1.Header,T1.Body,T1.CreatorTime,
					T1.ContactName,T1.ContactEmail,T1.ContactPhone,T1.AnswerTime,
					T1.ContactStatus,T1.ContactStatusDesc,T2.RealName,T3.RealName as CoOwnerName,T1.Attachment
					FROM CRM_WorkOrderDetail AS T1 
					INNER JOIN Sys_User T2 ON T1.CreatorUserID=T2.Id
					LEFT JOIN Sys_User T3 ON T1.CoOwnerId=T3.Id
					WHERE T1.WorkOrderId='{WorkOrderId}' AND T1.EnabledMark=1
					AND({_outterCond} )
					ORDER BY T1.CreatorTime DESC";
			string memoBriefSql = $@"SELECT T1.Id,T1.WorkOrderId,T1.ResponseTypeId,T1.ResponseTypeName,T1.CreatorTime, T3.ReplyMessage as Body,
					T2.RealName as CreatorName,T4.ApplicantName as ContactName,
					T4.SourceId as SourceId,T4.CaseCategoryLayer1Name as CaseCategoryLayer1Name,T4.Body as DetailDesc
					FROM CRM_WorkOrderDetail AS T1 
					INNER JOIN Sys_User T2 ON T1.CreatorUserID=T2.Id
					INNER JOIN CRM_WorkOrderLog T3 ON T1.Id = T3.WorkOrderDetailId 
					INNER JOIN CRM_WorkOrder T4 ON T1.WorkOrderId =T4.Id
					WHERE T1.WorkOrderId='{WorkOrderId}' AND T1.EnabledMark=1
					AND({_memoCond} )
					ORDER BY T1.CreatorTime	DESC	";



			List<WorkOrderBrief> internallList = _idetailrepository.GetWorkOrderBrief(internalBriefSql);
			List<WorkOrderBrief> outterlList = _idetailrepository.GetWorkOrderBrief(outterBriefSql);
			List<WorkOrderBrief> memolList = _idetailrepository.GetWorkOrderBrief(memoBriefSql);
			List<ContactHistory> contactHistories = outterlList.MapTo<ContactHistory>();
			List<CreateDetailHistory> createDetailHistories = internallList.MapTo<CreateDetailHistory>();
			List<ResponMemoHistory> createMemoHistories = memolList.MapTo<ResponMemoHistory>();
			workOrderRealateList.ContactHistory = new List<ContactHistory>(contactHistories);
			workOrderRealateList.CreateDetailHistory = new List<CreateDetailHistory>(createDetailHistories);
			workOrderRealateList.ResponMemoHistory = new List<ResponMemoHistory>(createMemoHistories);
			/*
			if (memolList.Count > 0)
			{
				foreach (var item in memolList)
				{
					var memoObj = JsonHelper.ToObject<ResponseCaseInfo>(item.Body);
					workOrderRealateList.ResponMemoHistory.Add(new ResponMemoHistory {
						DetailOpenTime = item.CreatorTime.ToString("yyyy/MM/dd"),
						inumber =memoObj.inumber,
						CreatorName = item.RealName,
						CustomerName=memoObj.ncustomer,
						CaseTypeName=memoObj.ireply,
						DetailDesc=memoObj.nprocess



					});
				}
			}*/


			return workOrderRealateList;
		}
		/// <summary>
		/// 查詢案件表頭
		/// </summary>
		/// <returns></returns>
		public WorkOrderListTableSchema GetworkOrderListTableHeader()
		{
			WorkOrderListTableSchema workOrderListTableSchema = new WorkOrderListTableSchema
			{
				MemoHeader = new MemoHeader(),
				EFaxHeader = new EFaxHeader(),
				EzGoHeader = new EzGoHeader(),
				InboundHeader = new InboundHeader(),
				TodoOrderHeader = new TodoOrderHeader()
			};
			return workOrderListTableSchema;
		}

		public async Task<string> CreateSingleOrder(string ItemCode)
		{
			WorkOrder workOrder = _repository.CreateSingleOrder(ItemCode);
			string workorderId = string.Empty;
			if (HttpContextHelper.HttpContext == null)
			{
				return workorderId;
			}

			//string userId = claimlist[0].Value;
			if (workOrder != null)
			{

				await repository.AddAsync(workOrder).ConfigureAwait(false);

			}
			else
			{
				return workorderId;
			}


			return workorderId;
		}

		public async Task<bool> OutBoundListtoWorkOrder(string OutBoundId, string userId, string FormTypeId, string FormTypeName, DateTime CaseCloseTime)
		{
			string nameListSql = @$"SELECT * FROM CRM_OutBoundNameList WHERE OutBoundId='{OutBoundId}'";
			List<OutBoundDbNameList> outBoundDbNameLists = _ioutboundnamelistRepository.GetBySql(nameListSql);
			var _outbound = _ioutboundRepository.Get(OutBoundId);
			List<WorkOrder> workOrders = new();
			string formWhere = string.Format("ItemName='{0}' ", nameof(WorkOrderType.OutBound)); ;
			var formItems = await _itemsDetailRepository.GetWhereAsync(formWhere).ConfigureAwait(false);
			string statusWhere1 = string.Format("ItemName='{0}' ", nameof(WorkflowStatus.等待分配)); ;
			var statusItems1 = await _itemsDetailRepository.GetWhereAsync(statusWhere1).ConfigureAwait(false);
			string statusWhere2 = string.Format("ItemName='{0}' ", nameof(WorkflowStatus.處理中)); ;
			var statusItems2 = await _itemsDetailRepository.GetWhereAsync(statusWhere2).ConfigureAwait(false);
			if (outBoundDbNameLists.Count > 0)
			{
				foreach (var item in outBoundDbNameLists)
				{
					WorkOrder order = new();
					order = item.MapTo<WorkOrder>();
					order.Id = GuidUtils.CreateShortNo();
					order.FormTypeName = FormTypeName;
					order.FormTypeId = FormTypeId;
					order.ParentFormTypeName = formItems.ItemName;
					order.ParentFormTypeId = formItems.Id;
					if (string.IsNullOrEmpty(item.OwnerId))
					{
						order.StatusName = statusItems1.ItemName;
						order.StatusId = statusItems1.Id;
					}
					else
					{
						order.StatusName = statusItems2.ItemName;
						order.StatusId = statusItems2.Id;
					}
					order.CaseOpenTime = DateTime.Now;
					order.CaseCloseTime = CaseCloseTime;
					order.EnabledMark = true;
					order.CreatorUserId = userId;
					order.CreatorTime = DateTime.Now;
					order.IsNotified = false;
					/* Outbound 直接歸戶*/
					order.SetAccountStatus = true;
					order.SetAccountRelation = "要保人";
					order.SetAccountStatusDesc = DateTime.Now.ToString("yyyy/MM/dd") + "歸戶";
					workOrders.Add(order);
				}
				var wo = await _repository.AddRangeAsync(workOrders);
				if (wo > 0)
				{
					_outbound.TransportMark = true;
					_outbound.TransportTime = DateTime.Now;
					_outbound.LastModifyTime = DateTime.Now;
					_outbound.LastModifyUserId = userId;
					var ob = await _ioutboundRepository.UpdateAsync(_outbound, OutBoundId);
					return ob;
				}
			}
			return false;
		}

		public async Task<List<string>> ImportEstimate(string[] Ids, string type, string currentUser)
		{
			List<WorkOrder> workOrders = _repository.ImportEstimate(Ids, type,currentUser);
			List<string> stringList = new List<string>();
			if (HttpContextHelper.HttpContext == null)
			{
				return stringList;
			}

			//string userId = claimlist[0].Value;
			if (workOrders.Count != 0 && workOrders != null)
			{
				//repository.BulkInsert(workOrders, "CRM_WorkOrder");
				await repository.AddRangeAsync(workOrders).ConfigureAwait(false);

			}
			else
			{
				return stringList;
			}

			foreach (var item in workOrders)
			{
				stringList.Add(item.Id);
				try
				{
					string where = string.Format("EstimateId='{0}'", item.SourceId);
					await _estimateRpository.UpdateTableFieldAsync("IsCheckCase", 1, where);
					await _estimateRpository.UpdateTableFieldAsync("WorkOrderId", item.Id, where);
				}
				catch
				{
					throw new Exception("更新要保書轉案件失敗");
				}
			}

			return stringList;
		}
		private List<WorkOrderOutputDto> ChangeWorkOrderCurrentOwnerDisplay(List<WorkOrderOutputDto> workOrders)
		{
			foreach (var workOrder in workOrders)
			{
				switch (workOrder.ParentFormTypeName)
				{
					case nameof(WorkOrderType.汽車險案件):
						string _where = $"WorkOrderId='{workOrder.Id}' AND ResponseTypeName='{ResponseType.核保審批}'";
						var workOrderDetail = _idetailrepository.GetWhere(_where);
						if (workOrderDetail != null)
						{
							if (!string.IsNullOrEmpty(workOrderDetail.CurrentEditorId))
							{
								var _user = _iuserRepository.Get(workOrderDetail.CurrentEditorId);
								if (_user != null)
								{
									workOrder.OwnerId = workOrderDetail.CurrentEditorId;
									workOrder.OwnerName = _user.RealName;
								}


							}

						}
						break;
					default:
						return workOrders;

				}
			}
			return workOrders;

		}
	}

}