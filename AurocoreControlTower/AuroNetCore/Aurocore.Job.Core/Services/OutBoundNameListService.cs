using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Json;
using Aurocore.Commons.Pages;
using Aurocore.Security.Models;
using Aurocore.Commons.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.Security.IRepositories;
using Aurocore.Commons.Helpers;
using System.Security.Claims;
using System.Linq;
using Aurocore.Commons.Extensions;
using Microsoft.AspNetCore.Hosting;
using Aurocore.WorkOrders.Enums;
using Aurocore.Security.IServices;
using Aurocore.Commons.Log;

namespace Aurocore.WorkOrders.Services
{
	/// <summary>
	/// 案件服務接口實現
	/// </summary>
	public class OutBoundNameListService: BaseService<OutBoundDbNameList,OutBoundDbNameListOutputDto, string>, IOutBoundNameListService
	{
		private readonly IOutBoundNameListRepository _repository;


		public OutBoundNameListService(IOutBoundNameListRepository repository) : base(repository)
		{
			_repository = repository;
		}

		public async Task<int> AddNameLists(List<OutBoundDbNameList> outBoundDbNameList)
		{
			var beforeCnt =_repository.ImportOutBoundNameList(outBoundDbNameList);
			if (beforeCnt > 0)
			{
				int _rst = await _repository.AddRangeAsync(outBoundDbNameList);
				return _rst;
			}
			return 0;
			
		}
	}
}