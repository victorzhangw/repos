using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.Enums;
using Aurocore.Security.IRepositories;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using System.Threading;

namespace Aurocore.WorkOrders.Services
{
	/// <summary>
	/// 服務接口實現
	/// </summary>
	public class WorkOrderDetailService: BaseService<WorkOrderDetail,WorkOrderDetailOutputDto, string>, IWorkOrderDetailService
	{
		private readonly IWorkOrderDetailRepository _repository;
		private readonly IWorkOrderRepository _workOrderRepository;
		private readonly IWorkOrderLogRepository _workOrderLogRepository;
		private readonly IItemsDetailRepository _itemsDetailRepository;
		private readonly IUserRepository _userRepository;
		public WorkOrderDetailService(IWorkOrderDetailRepository repository , IWorkOrderLogRepository workOrderLogRepository,
			IWorkOrderRepository workOrderRepository,IItemsDetailRepository itemsDetailRepository, IUserRepository userRepository) : base(repository)
		{
			_repository=repository;
			_workOrderRepository = workOrderRepository;
			_itemsDetailRepository = itemsDetailRepository;
			_workOrderLogRepository = workOrderLogRepository;
			_userRepository = userRepository;
		}
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public  async Task<PageResult<WorkOrderDetailOutputDto>> FindWorkOrderDerailsWithPagerAsync(SearchWorkOrderDetail search)
		{
			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<WorkOrderDetail> list = new();
			switch (search.Keywords)
			{
				case "History":
					int startPageIndex = search.CurrenetPageIndex == 1 ? 1 : (search.CurrenetPageIndex * search.PageSize) + 1;
					string historyMails = $@"select count(*) as Total from (select Id as WorkOrderId,Header,Body,CreatorTime from CRM_WorkOrder 
									WHERE ApplicantEmail ='{search.Filter.ContactEmail}' 
									UNION 
									select WorkOrderId,Header,Body,CreatorTime from CRM_WorkOrderDetail
									WHERE ContactEmail ='{search.Filter.ContactEmail}')  as A  ;With Paging AS
									( SELECT ROW_NUMBER() OVER ( order by  A.CreatorTime) as RowNumber,  *  FROM
									(select Id as WorkOrderId,Header,Body,CreatorTime,'IN' as ContactStatus from CRM_WorkOrder 
									WHERE ApplicantEmail ='{search.Filter.ContactEmail}' 
									UNION 
									select WorkOrderId,Header,Body,CreatorTime,'OUT' as ContactStatus from CRM_WorkOrderDetail 
									WHERE ContactEmail ='{search.Filter.ContactEmail}')as A  )
									SELECT * FROM Paging WHERE RowNumber Between {startPageIndex} and {startPageIndex + search.PageSize - 1}";
					list = await _repository.GetHistorywithPager(historyMails, pagerInfo);
					break;
				default:
					if (!string.IsNullOrEmpty(search.Filter.StatusId))
					{

						where += $"  AND StatusId='{search.Filter.StatusId}'";
					}
					if (!string.IsNullOrWhiteSpace(search.Keywords))
					{
						where += $" AND Id='{search.Keywords}' ";
						where += $" OR CoOwnerName='{search.Keywords}'";
						where += $" OR CreatorUserName='{search.Keywords}'";
						where += $" OR ContactEmail='{search.Keywords}'";
						where += $" OR ContactPhone='{search.Keywords}'";


					}
					if (!string.IsNullOrEmpty(search.StartDateTime) && !string.IsNullOrEmpty(search.EndDateTime))
					{
						where += @$" AND CAST(CreatorTime AS DATE) BETWEEN '{search.StartDateTime}' AND '{search.EndDateTime}' ";
					}
					list = await _repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
					break;
			}
			
			
			var itemList = list.MapTo<WorkOrderDetailOutputDto>();
			
			PageResult<WorkOrderDetailOutputDto> pageResult = new PageResult<WorkOrderDetailOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = itemList,
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="wsearch">查詢的條件</param>
		/// <param name="dsearch">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public async Task<PageResult<WorkOrderDetailOutputDto>> FindWithPagerSearchAsync(SearchWorkOrderDetail dsearch, SeachWorkOrder wsearch)
		{
			bool order = dsearch.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			string woWhere = GetDataPrivilege(false);
			string forMenuWhere = GetDataPrivilege(false);
			string forMenuWhere2 = GetDataPrivilege(false);
			string forMenuWhere3 = GetDataPrivilege(false);
			string forMenuWhere4 = GetDataPrivilege(false);
			string menuSql1 = string.Empty;
			string menuSql2 = string.Empty;
			string formWhere = string.Format("ItemName='{0}' ", nameof(WorkOrderType.待辦單據)); ;
			var formItems = await _itemsDetailRepository.GetWhereAsync(formWhere).ConfigureAwait(false);
			string formWhere2 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.全部案件)); ;
			var formItems2 = await _itemsDetailRepository.GetWhereAsync(formWhere2).ConfigureAwait(false);
			string formWhere3 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.Memo)); ;
			var formItems3 = await _itemsDetailRepository.GetWhereAsync(formWhere3).ConfigureAwait(false);
			string formWhere4 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.汽車險案件)); ;
			var formItems4 = await _itemsDetailRepository.GetWhereAsync(formWhere4).ConfigureAwait(false);
			List<WorkOrderMenu> workOrderMenu1 = null;
			List<WorkOrderMenu> workOrderMenu2 = null;
			string historyMails = string.Empty;
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = dsearch.CurrenetPageIndex,
				PageSize = dsearch.PageSize
			};
			switch (dsearch.Keywords)
			{
				case "History":
					if (!string.IsNullOrEmpty(dsearch.Filter.ResponseTypeId))
					{
						where += " and ResponseTypeId='" + dsearch.Filter.ResponseTypeId + "'";
					}
					
					if (!string.IsNullOrEmpty(dsearch.Filter.ContactEmail))
					{
						int startPageIndex = dsearch.CurrenetPageIndex == 1 ? 1 : (dsearch.CurrenetPageIndex * dsearch.PageSize)+1;
						historyMails = $@"select count(*) as Total from (select Id as WorkOrderId,Header,Body,CreatorTime from CRM_WorkOrder 
									WHERE ApplicantEmail ='{dsearch.Filter.ContactEmail}' 
									UNION 
									select WorkOrderId,Header,Body,CreatorTime from CRM_WorkOrderDetail
									WHERE ContactEmail ='{dsearch.Filter.ContactEmail}')  as A  ;With Paging AS
									( SELECT ROW_NUMBER() OVER ( order by  A.CreatorTime) as RowNumber,  *  FROM (select Id as WorkOrderId,Header,Body,CreatorTime from CRM_WorkOrder 
									WHERE ApplicantEmail ='{dsearch.Filter.ContactEmail}' 
									UNION 
									select WorkOrderId,Header,Body,CreatorTime from CRM_WorkOrderDetail 
									WHERE ContactEmail ='{dsearch.Filter.ContactEmail}')as A  )
									SELECT * FROM Paging WHERE RowNumber Between {startPageIndex} and {startPageIndex+dsearch.PageSize-1}";

					}
					
					break;
				default:
					if (!string.IsNullOrEmpty(dsearch.Filter.WorkOrderId))
					{
						where += " and WorkOrderId='" + dsearch.Filter.WorkOrderId + "'";
					}
					if (!string.IsNullOrEmpty(dsearch.Filter.ResponseTypeId))
					{
						where += " and ResponseTypeId='" + dsearch.Filter.ResponseTypeId + "'";
					}
					if (!string.IsNullOrWhiteSpace(dsearch.Keywords))
					{
						where += $" AND Id='{dsearch.Keywords}' ";
						where += $" OR CoOwnerName='{dsearch.Keywords}'";
						where += $" OR CreatorUserName='{dsearch.Keywords}'";
						where += $" OR ContactEmail='{dsearch.Keywords}'";
						where += $" OR ContactPhone='{dsearch.Keywords}'";


					}
					// 檢查狀態
					if (!string.IsNullOrEmpty(dsearch.Filter.StatusName))
					{

						switch (dsearch.Filter.StatusName)
						{
							case nameof(WorkflowStatus.全部狀態):
								// 若是全部狀態，清除之前所有條件
								where = where;
								break;
							case nameof(WorkflowStatus.處理中):
								where += " and (StatusName='" + dsearch.Filter.StatusName + "' OR StatusName='" + nameof(WorkflowStatus.協辦中)  + "' OR StatusName='" + nameof(WorkflowStatus.核保審批中) + "')";
								break;
							case nameof(WorkflowStatus.協辦中):
								where += " and (StatusName='" + dsearch.Filter.StatusName + "' OR StatusName='" + nameof(WorkflowStatus.協辦中)  + "' OR StatusName='" + nameof(WorkflowStatus.核保審批中) + "')";
								break;

							case nameof(WorkflowStatus.等待分配):
								// 判斷部門
								if (!string.IsNullOrEmpty(wsearch.DepartmentId))
								{
									where += " and StatusName='" + dsearch.Filter.StatusName + "'  AND (CoDeptId='" + wsearch.DepartmentId + "') ";
								}
								else
								{
									where += " and StatusName='" + dsearch.Filter.StatusName + "'  ";
								}
								
								break;


						}
						
						
						

					}
					/*
					if (!string.IsNullOrEmpty(dsearch.Filter.CoOwnerId) || !string.IsNullOrEmpty(dsearch.Filter.CreatorUserId))
					{
						where += " and ( CreatorUserId = '" + dsearch.Filter.CreatorUserId + "' or CoOwnerId = '" + dsearch.Filter.CreatorUserId + "')";

					}*/
					//判斷目前 帳號/群組 持有單據
					if (!string.IsNullOrEmpty(dsearch.Filter.CurrentEditorId) && dsearch.Filter.StatusName != nameof(WorkflowStatus.等待分配))
					{
						if ((wsearch.EnCode == "1"))
						{
							where += " and  CurrentEditorId = '" + dsearch.Filter.CurrentEditorId + "'";
						}
						
						
					}
					if (!string.IsNullOrEmpty(dsearch.StartDateTime) && !string.IsNullOrEmpty(dsearch.EndDateTime))
					{
						where += @$" AND CAST(CreatorTime AS DATE) BETWEEN '{dsearch.StartDateTime}' AND '{dsearch.EndDateTime}' ";
					}
					if (!string.IsNullOrEmpty(dsearch.Filter.ContactEmail))
					{
						where = GetDataPrivilege(false);
						where += " AND ContactEmail='" + dsearch.Filter.ContactEmail + "'";
					}

					break;
			}
			//string where1 = GetDataPrivilege(false);
			switch (wsearch.Filter.StatusName)
			{
				case nameof(WorkflowStatus.等待分配):
					forMenuWhere += " and StatusName='" + wsearch.Filter.StatusName + "'";
					forMenuWhere2 += $" and (StatusName='{nameof(WorkflowStatus.等待分配)}') ";
					forMenuWhere2 += " and (CoOwnerId ='' OR  CoOwnerId=null) ";
					forMenuWhere2 += " and (CoDeptId='" + wsearch.DepartmentId + "') ";
                    forMenuWhere3 += $" and (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}') ";

                    forMenuWhere4 += $" and (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}')  ";

					menuSql1 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere2;

					menuSql2 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere3 + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere4;


					break;
				default:

					forMenuWhere3 = GetDataPrivilege(false);
					forMenuWhere4 = GetDataPrivilege(false);


					//判斷個人單據量
					forMenuWhere += " and ( OwnerId = '" + wsearch.Filter.OwnerId + "' or CoOwnerId = '" + wsearch.Filter.CoOwnerId + "')";
					forMenuWhere2 += " and ( CurrentEditorId = '" + wsearch.Filter.OwnerId + "' )";
					forMenuWhere3 += " and ( OwnerId = '" + wsearch.Filter.OwnerId + "' or CoOwnerId = '" + wsearch.Filter.CoOwnerId + "')";
					forMenuWhere4 += " and ( CurrentEditorId = '" + wsearch.Filter.OwnerId + "' )";
					//判斷單據狀態
					forMenuWhere += $" AND (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}')";
					forMenuWhere2 += $" AND (StatusName='{nameof(WorkflowStatus.處理中)}' OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.核保審批中)}' )";
					forMenuWhere3 += $" AND StatusName='{nameof(WorkflowStatus.已結案)}' AND CONVERT(varchar(10), LastModifyTime, 102) = CONVERT(varchar(10), GETDATE(), 102)";
					forMenuWhere4 += $" AND StatusName='{nameof(WorkflowStatus.已結案)}' AND CONVERT(varchar(10), LastModifyTime, 102) = CONVERT(varchar(10), GETDATE(), 102) ";
					menuSql2 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere2;

					menuSql1 = @"SELECT FormTypeId ,FormTypeName,COUNT(FormTypeId) as FormCounts from CRM_WorkOrder
				  WHERE " + forMenuWhere3 + "GROUP BY FormTypeId,FormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='Memo' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='OutBound' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT ParentFormTypeId as FormTypeId ,ParentFormTypeName as FormTypeName ,COUNT(ParentFormTypeId) as FormCounts from CRM_WorkOrder" +
					" WHERE " + forMenuWhere3 + " and ParentFormTypeName='汽車險案件' GROUP BY ParentFormTypeId,ParentFormTypeName UNION " +
					" SELECT '" + formItems.Id + "' as  FormTypeId,'待辦單據' as FormTypeName,COUNT(Id) as FormCounts from CRM_WorkOrderDetail " +
					" WHERE " + forMenuWhere4;
					break;

			}

			workOrderMenu1 = _workOrderRepository.GetWorkOrderMenuInfo(menuSql1);
			workOrderMenu2 = _workOrderRepository.GetWorkOrderMenuInfo(menuSql2);
			List<WorkOrderMenu> resultworkOrderMenu = new List<WorkOrderMenu>();
			int totalCounts = 0;
			int totalCounts2 = 0;
			var workOrderAsArray = Enum.GetNames(typeof(WorkOrderType));
			foreach (var item in workOrderAsArray)
			{
				if (item != "Memo" && item != "待辦單據" && item != "OutBound" &&item!= "汽車險案件")
				{
					foreach (var wm1 in workOrderMenu1)
					{
						if (wm1.FormTypeName == item)
						{
							totalCounts += Int32.Parse(wm1.FormCounts);
						}

					}
					foreach (var wm2 in workOrderMenu2)
					{
						if (wm2.FormTypeName == item)
						{
							totalCounts2 += Int32.Parse(wm2.FormCounts);
						}

					}

				}
			}
			foreach (var item in workOrderAsArray)
			{
				string _count1 = "0";
				string _count2 = "0";
				string _formtypeID = string.Empty;
				foreach (var wm1 in workOrderMenu1)
				{
					if (wm1.FormTypeName == item)
					{
						_count1 = wm1.FormCounts;
						_formtypeID = wm1.FormTypeId;

					}
				}
				foreach (var wm2 in workOrderMenu2)
				{
					if (wm2.FormTypeName == item)
					{
						_count2 = wm2.FormCounts;
						_formtypeID = wm2.FormTypeId;
					}
				}
				if (string.IsNullOrEmpty(_formtypeID))
				{
					string _formWhere = string.Format("ItemName='{0}' ", item); ;
					var _formItems = await _itemsDetailRepository.GetWhereAsync(_formWhere).ConfigureAwait(false);
					if (_formItems != null)
					{
						_formtypeID = _formItems.Id;
						if (_formItems.ItemName == nameof(WorkOrderType.全部案件))
						{
							_count1 = totalCounts.ToString();
							_count2 = totalCounts2.ToString();

						}

					}

				}
				resultworkOrderMenu.Add(new WorkOrderMenu()
				{
					FormTypeId = _formtypeID,
					FormTypeName = item,
					FormCounts = _count1,
					FormCounts2 = _count2,


				});

			}


			List<WorkOrderDetail> list = new();
			switch (dsearch.Keywords)
			{
				case "History":
					list = await _repository.GetHistorywithPager(historyMails, pagerInfo);
					break;
				default:
					list = await _repository.FindWithPagerAsync(where, pagerInfo, dsearch.Sort, order);
					break;
			}
			
			var itemList = list.MapTo<WorkOrderDetailOutputDto>();
			foreach (var item in itemList)
            {
				/* 移除核保審批單據，但目前未移除數量，目前加入解決方式加入 核保審批中狀態
				if(item != null && item.ResponseTypeName == nameof(ResponseType.核保審批))
                {
					itemList.Remove(item);
				


				}*/
				if (!string.IsNullOrEmpty(item.CurrentEditorId))
				{
					if (item.CurrentEditorId == item.CoOwnerId)
					{
						item.CurrentEditorName = item.CoOwnerName;
					}
					if (item.CurrentEditorId == item.CreatorUserId)
					{
						item.CurrentEditorName = item.CreatorUserName;
					}

				}
			}
			PageResult<WorkOrderDetailOutputDto> pageResult = new PageResult<WorkOrderDetailOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = itemList,
				ItemsPerPage = pagerInfo.PageSize,
				Context = workOrderMenu1,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}
		/// <summary>
		/// 合併批改
		/// </summary>
		/// <param name="endorsementForm"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public bool MergeEndorsement(EndorsementForm endorsementForm, string filePath)
		{
			var _wod = _repository.Get(endorsementForm.WorkOrderDetailId);
			string insureType = string.Empty;
			if (_wod == null)
			{
				throw new Exception("無此單據編號");
			}
			if (string.IsNullOrEmpty(_wod.InsuranceTypeName))
			{
				var _wo = _workOrderRepository.Get(_wod.WorkOrderId);
				if (!string.IsNullOrEmpty(_wo.InsuranceTypeName))
				{
					insureType = _wo.InsuranceTypeName;

				}
				throw new Exception("未提供保險種類");
			}
			else
			{
				insureType = _wod.InsuranceTypeName;

			}

			endorsementForm.InsureTypeName = insureType;
			var rst = _repository.MergeEndorsement(endorsementForm, filePath);
			if (rst)
			{
				WorkOrderDetail orderDetail = _wod.MapTo<WorkOrderDetail>();
				orderDetail.Attachment = endorsementForm.FilePath;
				_repository.Update(orderDetail,endorsementForm.WorkOrderDetailId);
			}
			return rst;
		}
		/// <summary>
		/// 合併補發
		/// </summary>
		/// <param name="reissueForm"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public string MergeReissue(ReissueForm reissueForm, string filePath)
		{
			var _wod = _repository.Get(reissueForm.WorkOrderDetailId);
			string insureType = string.Empty;
			if (_wod == null)
			{
				throw new Exception("無此單據編號");
			}
			if (string.IsNullOrEmpty(_wod.InsuranceTypeName))
			{
				var _wo = _workOrderRepository.Get(_wod.WorkOrderId);
				if (!string.IsNullOrEmpty(_wo.InsuranceTypeName))
				{
					insureType = _wo.InsuranceTypeName;

				}
				throw new Exception("未提供保險種類");
			}
			else
			{
				insureType = _wod.InsuranceTypeName;

			}
			if (reissueForm.deliveryInfo == null)
			{
				throw new Exception("未提供遞送資訊");
			}
			reissueForm.InsureTypeName = insureType;
			var rst = _repository.MergeReissue(reissueForm, filePath);
			if (!string.IsNullOrEmpty(rst))
			{
				WorkOrderDetail orderDetail = _wod.MapTo<WorkOrderDetail>();

				orderDetail.Attachment = rst;
				var updated =  _repository.Update(orderDetail, reissueForm.WorkOrderDetailId);

			}
			return rst;
			
		}

		public  async Task<WorkOrderRealateList> WorkOrderDetailTypeTree(string Id)
		{
			WorkOrderRealateList realateList = new();
			var _wod = _repository.Get(Id);
			realateList.FormType = new Dictionary<string, string>();
			string where = " 1=1 ";
			if(_wod != null)
			{
				realateList.FormType.Add(_wod.ResponseTypeId,_wod.ResponseTypeName);
				
				where += @$" AND WorkOrderId='{_wod.WorkOrderId}' AND WorkOrderDetailId='{_wod.Id}' ORDER BY CreatorTime DESC";
				var logs =(List<WorkOrderLog>)await _workOrderLogRepository.GetListWhereAsync(where);
				List<DetailProcessHistory> detailProcessHisties = new();
				
				if (logs.Count>0) {
				
				foreach(var item in logs)
					{
						string _head = string.Empty;
						string _openTime = string.Empty;
						ReplyMessage replyMessage = new();
						var user = _userRepository.Get(item.CreatorUserId);
						if (item.ReplyStatus !=null && (bool)item.ReplyStatus )
						{
							_head = "結案";
						}
						else if(item.ReplyStatus != null)
						{
							_head = "回覆";
						}
						else if(item.ReplyStatus == null)

						{
							_head = $"{_wod.ResponseTypeName}單據:{item.LogTypeName}";
						}
						if (!string.IsNullOrEmpty(item.CreatorTime.ToString()))
						{
							_openTime = item.CreatorTime.ToString("yyyy/MM/dd");

						}
						if (!string.IsNullOrEmpty(item.CreatorTime.ToString()))
						{
							replyMessage = JsonHelper.ToObject<ReplyMessage>(item.ReplyMessage);
						}
						
						
						
						detailProcessHisties.Add(new DetailProcessHistory
						{
							CreatorName = user.RealName,
							
							DetailOpenTime = _openTime,
							DetailDesc =replyMessage.Header,
							FormStatus = _wod.StatusName,
							Header = _head
						}) ;

					}
				
				}
				
				realateList.DetailProcessHistory = detailProcessHisties;
			}
			
			return realateList;


		}

		public async Task<long> CreateSingleWorkOrderDetail(WorkOrderDetail workOrderDetail, int retryCount = 3, int waitSecond = 1)
		{
			long rc = 0;
			try
			{
				while (true)
				{
					try
					{
						string formNo = await _repository.CreateWorkOrderDetailNo(workOrderDetail);
						
						if (string.IsNullOrEmpty(formNo))
						{

							Log4NetHelper.Error("新增單據異常:未能取得新增單據編號");
							return rc;
						}
						else
						{
							workOrderDetail.Id = formNo;
						}
						rc = await _repository.InsertAsync(workOrderDetail);
						if (rc > 0)
						{
							break;

						}

					}
					catch
					{
						if (--retryCount == 0)
						{
							Log4NetHelper.Error("新增單據異常,已達嘗試次數上限");
							return rc;
						}

						var seconds = TimeSpan.FromSeconds(waitSecond);
						Thread.Sleep(seconds);
					}
				}



			}
			catch (Exception e)
			{

				Log4NetHelper.Error("新增單據異常", e);

			}
			
			
			return rc;
		}
	}
}