using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.Services
{
	/// <summary>
	/// 服務接口實現
	/// </summary>
	public class WorkOrderLogService: BaseService<WorkOrderLog,WorkOrderLogOutputDto, string>, IWorkOrderLogService
	{
		private readonly IWorkOrderLogRepository _repository;
		public WorkOrderLogService(IWorkOrderLogRepository repository) : base(repository)
		{
			_repository=repository;
		}

	}
}