using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.WorkOrders.Models;
using Microsoft.AspNetCore.Hosting;

namespace Aurocore.WorkOrders.IRepositories
{
    /// <summary>
    /// 定義案件倉儲接口
    /// </summary>
    public interface IOutBoundRepository : IRepository<OutBound, string>
    {
        
        

    }
}