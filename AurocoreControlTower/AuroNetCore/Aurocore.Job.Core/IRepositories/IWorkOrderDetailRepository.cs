using System;
using System.Collections.Generic;
using Aurocore.Commons.IRepositories;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.Enums;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using Aurocore.Commons.Pages;

namespace Aurocore.WorkOrders.IRepositories
{
    /// <summary>
    /// 定義倉儲接口
    /// </summary>
    public interface IWorkOrderDetailRepository:IRepository<WorkOrderDetail, string>
    {
        List<WorkOrderBrief> GetWorkOrderBrief(string sql);
        Task<List<WorkOrderDetail>> GetHistorywithPager(string sql, PagerInfo info);
        bool MergeEndorsement(EndorsementForm endorsementForm, string filePath);
        string MergeReissue(ReissueForm reissueForm, string filePath);
        Task<string> CreateWorkOrderDetailNo(WorkOrderDetail workOrderDetail);
        
    }
}