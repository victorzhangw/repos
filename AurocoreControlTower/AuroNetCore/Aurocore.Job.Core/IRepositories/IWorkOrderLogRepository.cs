using System;
using System.Collections.Generic;
using Aurocore.Commons.IRepositories;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WorkOrders.IRepositories
{
    /// <summary>
    /// 定義倉儲接口
    /// </summary>
    public interface IWorkOrderLogRepository:IRepository<WorkOrderLog,string>
    {
        List<WorkOrderLogBrief> GetWorkOrderLogBrief(string sql);
    }
}