using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.Commons.Pages;
using Aurocore.WorkOrders.Models;
using Microsoft.AspNetCore.Hosting;

namespace Aurocore.WorkOrders.IRepositories
{
    /// <summary>
    /// 定義案件倉儲接口
    /// </summary>
    public interface IWorkOrderRepository:IRepository<WorkOrder, string>
    {
        /// <summary>
        /// 收取
        /// </summary>
        /// <param name="ItemCode"></param>
        /// <param name="hostingEnvironment">主機參數</param>
        /// <returns></returns>
        Task<List<WorkOrder>> RetrieveEmail(string ItemCode,IWebHostEnvironment hostingEnvironment);
        List<WorkOrder> ImportMemo( Memo memo);
        List<WorkOrder> ImportOutBound(string OutBoundId);
        WorkOrder CreateSingleOrder(string ItemCode);
        List<WorkOrderMenu> GetWorkOrderMenuInfo(string menuSql);
        List<WorkOrder> ImportEstimate(string[] estimateIds, string estimateType,string currentUser);
        Task<List<object>> FindWithPagerRelationWorkOrderDetailAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans=null );



    }
}