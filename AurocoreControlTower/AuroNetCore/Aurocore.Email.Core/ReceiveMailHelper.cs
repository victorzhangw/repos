using MailKit;
using MailKit.Net.Smtp;
using MailKit.Net.Pop3;
using MailKit.Net.Imap;
using MailKit.Search;
using MailKit.Security;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Json;
using Aurocore.Commons.Options;
using System.Linq;
using MimeKit;

namespace Aurocore.Email.Core
{
    /// <summary>
    /// 發送郵件
    /// </summary>
    public static class ReceiveMailHelper
    {
        

        /// <summary>
        /// 發送郵件
        /// </summary>
        /// <param name="receiveServerConfigurationEntity">郵件基礎資訊</param>
        public static ReceiveResultEntity  ReceiveMail()
        {
            var receiveResultEntity = new ReceiveResultEntity();
            ReceiveServerConfigurationEntity receiveServerConfiguration = new ReceiveServerConfigurationEntity();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            AppSetting sysSetting = JsonSerializer.Deserialize<AppSetting>(AurocoreCacheHelper.Get("SysSetting").ToJson());
            if (sysSetting != null)
            {
               // receiveServerConfiguration.Pop3Host = DEncrypt.Decrypt(sysSetting.ReceiveEmailimap);
                receiveServerConfiguration.ImapHost = sysSetting.ReceiveEmailimap;
                receiveServerConfiguration.ReceiveAccount = sysSetting.ReceiveEmailusername;
                // receiveServerConfiguration.ReceivePassword = DEncrypt.Decrypt(sysSetting.ReceiveEmailpassword);
                receiveServerConfiguration.ReceivePassword = sysSetting.ReceiveEmailpassword;
                receiveServerConfiguration.ImapPort = sysSetting.ReceiveEmailport.ToInt();
                receiveServerConfiguration.IsSsl = sysSetting.ReceiveEmailssl.ToBool();
                receiveServerConfiguration.MailEncoding = "utf-8";

            }
            else
            {
                receiveResultEntity.ResultInformation = "郵件伺服器未設定";
                receiveResultEntity.ResultStatus = false;
                throw new ArgumentNullException();
            }
            receiveResultEntity = ReceiveMail(receiveServerConfiguration);
            return receiveResultEntity;
        }
        /// <summary>
        /// 接收郵件
        /// </summary>
        /// <param name="mailBodyEntity">郵件內容</param>
        /// <param name="receiveServerConfiguration">發送設定</param>
        /// <param name="client">客戶端物件</param>
        /// <param name="receiveResultEntity">發送結果</param>
        public static ReceiveResultEntity ReceiveMail(ReceiveServerConfigurationEntity receiveServerConfiguration)
        {
            var receiveResultEntity = new ReceiveResultEntity();
            if (receiveServerConfiguration == null)
            {
                receiveResultEntity.ResultInformation = "郵件伺服器未設定";
                receiveResultEntity.ResultStatus = false;
                throw new ArgumentNullException();
            }
            using (var client = new ImapClient(new ProtocolLogger(MailMessage.CreateMailLog())))
            {


                Connection(receiveServerConfiguration, client, receiveResultEntity);

                if (receiveResultEntity.ResultStatus == false)
                {
                    return receiveResultEntity;
                }


                client.AuthenticationMechanisms.Remove("XOAUTH2");

                Authenticate(receiveServerConfiguration, client, receiveResultEntity);

                if (receiveResultEntity.ResultStatus == false)
                {
                    return receiveResultEntity;
                }

                Receive(receiveServerConfiguration, client, receiveResultEntity);

                if (receiveResultEntity.ResultStatus == false)
                {
                    return receiveResultEntity;
                }
                client.Disconnect(true);
                return receiveResultEntity;
            }
        }
        /// <summary>
        /// 連線伺服器
        /// </summary>
        /// <param name="receiveServerConfiguration">發送設定</param>
        /// <param name="client">客戶端物件</param>
        /// <param name="receiveResultEntity">發送結果</param>
        public static void Connection( ReceiveServerConfigurationEntity receiveServerConfiguration,
            ImapClient client, ReceiveResultEntity receiveResultEntity)
        {
            try
            {
                client.Connect(receiveServerConfiguration.ImapHost, receiveServerConfiguration.ImapPort, receiveServerConfiguration.IsSsl);
            }
            catch (ImapCommandException ex)
            {
                receiveResultEntity.ResultInformation = $"嘗試連線時出錯:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
            catch (ImapProtocolException ex)
            {
                receiveResultEntity.ResultInformation = $"嘗試連線時的協議錯誤:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
            catch (Exception ex)
            {
                receiveResultEntity.ResultInformation = $"伺服器連線錯誤:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
        }

        /// <summary>
        /// 帳戶認證
        /// </summary>
        /// <param name="mailBodyEntity">郵件內容</param>
        /// <param name="sendServerConfiguration">發送設定</param>
        /// <param name="client">客戶端物件</param>
        /// <param name="receiveResultEntity">發送結果</param>
        public static void Authenticate( ReceiveServerConfigurationEntity receiveServerConfiguration,
            ImapClient client, ReceiveResultEntity receiveResultEntity)
        {
            try
            {
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(receiveServerConfiguration.ReceiveAccount, receiveServerConfiguration.ReceivePassword);
            }
            catch (AuthenticationException ex)
            {
                receiveResultEntity.ResultInformation = $"無效的使用者名稱或密碼:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
            catch (SmtpCommandException ex)
            {
                receiveResultEntity.ResultInformation = $"嘗試驗證錯誤:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
            catch (SmtpProtocolException ex)
            {
                receiveResultEntity.ResultInformation = $"嘗試驗證時的協議錯誤:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
            catch (Exception ex)
            {
                receiveResultEntity.ResultInformation = $"帳戶認證錯誤:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
            }
        }
        /// <summary>
        /// 接收郵件
        /// </summary>
        /// <returns></returns>
        public  static void Receive(ReceiveServerConfigurationEntity receiveServerConfiguration,
            ImapClient client, ReceiveResultEntity receiveResultEntity)
        {
            var inbox = client.Inbox;
            inbox.Open(FolderAccess.ReadWrite);

            var uids = inbox.Search(SearchQuery.NotSeen);
            foreach (var uid in inbox.Search(SearchQuery.NotSeen))
            {
                var message = inbox.GetMessage(uid);
                inbox.AddFlags(uid, MessageFlags.Seen, true);
                var mailBody = new MailBodyEntity

                {
                    Body = !string.IsNullOrEmpty(message.HtmlBody) ? message.HtmlBody : message.TextBody,
                    Subject = message.Subject,
               

                };

                mailBody.FromAddresses.AddRange(message.From.Select(x => (MailboxAddress)x).Select(x => new FromEmailAddress { Address = x.Address, Name = x.Name }));
                receiveResultEntity.bodyEntities.Add(mailBody);
            }
           


        }





    }
}
