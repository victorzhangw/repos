using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Exchange.WebServices.Data;

namespace Aurocore.Email.Core
{
    /// <summary>
    /// 郵件內容實體
    /// </summary>
    public class MailBodyEntity
    {
        public MailBodyEntity()
        {

            FromAddresses = new List<FromEmailAddress>();
            MailFiles = new List<MailFile>();
        }
        ///// <summary>
        ///// 郵件文字內容
        ///// </summary>
        //public string MailTextBody { get; set; }

        /// <summary>
        /// 郵件內容型別
        /// </summary>
        public TextFormat MailBodyType { get; set; } = TextFormat.Html;

        /// <summary>
        /// 郵件附件集合
        /// </summary>
        public List<MailFile> MailFiles { get; set; }

        /// <summary>
        /// 收件人
        /// </summary>
        public List<string> Recipients { get; set; }

        /// <summary>
        /// 抄送
        /// </summary>
        public List<string> Cc { get; set; }

        /// <summary>
        /// 密送
        /// </summary>
        public List<string> Bcc { get; set; }
        /// <summary>
        /// 發信帳號別名 1：EzGo 2.：EFax
        /// </summary>
        public string BoxType { get; set; }

        /// <summary>
        /// 發件人
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// 發件人地址
        /// </summary>
        public string SenderAddress { get; set; }

        /// <summary>
        /// 郵件主題
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 郵件內容
        /// </summary>
        public string Body { get; set; }
        /// <summary>
        /// 儲存 exchange 記錄物件 Id
        /// </summary>
        public string EWSItemId { get; set; }
        /// <summary>
        /// 郵件地址
        /// </summary>
        public List<FromEmailAddress> FromAddresses { get; set; }
       
       
    }
    public class FromEmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class MailFile
    {
        /// <summary>
        /// 郵件附件檔案型別 例如：圖片 MailFileType="image"
        /// </summary>
        public string MailFileType { get; set; }

        /// <summary>
        /// 郵件附件檔案子型別 例如：圖片 MailFileSubType="png"
        /// </summary>
        public string MailFileSubType { get; set; }

        /// <summary>
        /// 郵件附件檔案路徑  例如：圖片 MailFilePath=@"C:\Files\123.png"
        /// </summary>
        public string MailFilePath { get; set; }
    }

    /// <summary>
    /// 郵件伺服器基礎資訊
    /// </summary>
    public class MailServerInformation
    {
        /// <summary>
        /// SMTP伺服器支援SASL機制型別
        /// </summary>
        public bool Authentication { get; set; }

        /// <summary>
        /// SMTP伺服器對訊息的大小
        /// </summary>
        public uint Size { get; set; }

        /// <summary>
        /// SMTP伺服器支援傳遞狀態通知
        /// </summary>
        public bool Dsn { get; set; }

        /// <summary>
        /// SMTP伺服器支援Content-Transfer-Encoding
        /// </summary>
        public bool EightBitMime { get; set; }

        /// <summary>
        /// SMTP伺服器支援Content-Transfer-Encoding
        /// </summary>
        public bool BinaryMime { get; set; }

        /// <summary>
        /// SMTP伺服器在訊息頭中支援UTF-8
        /// </summary>
        public string UTF8 { get; set; }
    }

    /// <summary>
    /// 郵件發送結果
    /// </summary>
    public class SendResultEntity
    {
        /// <summary>
        /// 結果資訊
        /// </summary>
        public string ResultInformation { get; set; } = "發送成功！";

        /// <summary>
        /// 結果狀態
        /// </summary>
        public bool ResultStatus { get; set; } = true;
    }
    /// <summary>
    /// 郵件接收
    /// </summary>
    public class ReceiveResultEntity
    {
        /// <summary>
        /// 結果資訊
        /// </summary>
        public string ResultInformation { get; set; } = "接收成功！";

        /// <summary>
        /// 結果狀態
        /// </summary>
        public bool ResultStatus { get; set; } = true;
        public ReceiveResultEntity()
        {

            bodyEntities = new List<MailBodyEntity>();
        }
        public List<MailBodyEntity> bodyEntities{ get; set; }
    }

    /// <summary>
    /// 郵件發送伺服器設定
    /// </summary>
    public class SendServerConfigurationEntity
    {
        /// <summary>
        /// 郵箱SMTP伺服器地址
        /// </summary>
        public string SmtpHost { get; set; }

        /// <summary>
        /// 郵箱SMTP伺服器埠
        /// </summary>
        public int SmtpPort { get; set; }

        /// <summary>
        /// 是否啟用IsSsl
        /// </summary>
        public bool IsSsl { get; set; }

        /// <summary>
        /// 郵件編碼
        /// </summary>
        public string MailEncoding { get; set; }

        /// <summary>
        /// 郵箱帳號
        /// </summary>
        public string SenderAccount { get; set; }

        /// <summary>
        /// 郵箱密碼
        /// </summary>
        public string SenderPassword { get; set; }

    }
    /// <summary>
    /// 郵件接受伺服器設定
    /// </summary>
    public class ReceiveServerConfigurationEntity
    {
        /// <summary>
        /// 郵箱SMTP伺服器地址
        /// </summary>
        public string ImapHost { get; set; }

        /// <summary>
        /// 郵箱SMTP伺服器埠
        /// </summary>
        public int ImapPort { get; set; }

        /// <summary>
        /// 是否啟用IsSsl
        /// </summary>
        public bool IsSsl { get; set; }

        /// <summary>
        /// 郵件編碼
        /// </summary>
        public string MailEncoding { get; set; }

        /// <summary>
        /// 郵箱帳號
        /// </summary>
        public string ReceiveAccount { get; set; }

        /// <summary>
        /// 郵箱密碼
        /// </summary>
        public string ReceivePassword { get; set; }

    }
}
