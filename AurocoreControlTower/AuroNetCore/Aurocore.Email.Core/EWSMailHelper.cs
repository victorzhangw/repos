﻿using Microsoft.Exchange.WebServices.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Cache;
using Aurocore.Security.Models;
using Aurocore.Commons.Log;
using Aurocore.Commons.Json;
using Aurocore.Commons.Extensions;

namespace Aurocore.Email.Core
{
    /// <summary>
    /// 發送郵件
    /// </summary>
    public static class EWSMailHelper
    {
        private static string _filePath;
        private static string _dbFilePath;   //資料庫中的檔案路徑
        private static string _dbThumbnail;   //資料庫中的縮圖路徑
        private static string _belongApp;//所屬應用
        private static string _belongAppId;//所屬應用ID 
        private static string _fileName;//檔名稱
       // private static readonly IWebHostEnvironment _hostingEnvironment;
        public static ExchangeService Connection(string boxType)
        {


            var ews = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
            string userAccount = string.Empty;
            string loginPw = string.Empty;
            //或使用執行程式的AD帳號自動登入
            //ews.UseDefaultCredentials = true;

            //Office 365 登入方式
            //AppContants.Account 即為 Email 地址，如 "email@xxx.onmicrosoft.com"
            //  ews.Credentials = new WebCredentials(AppContants.Account, AppContants.Password);
            //  var sw = new Stopwatch();
            // sw.Start();
            // ews.AutodiscoverUrl(AppContants.Account, RedirectUrlVaidationCallback);
            //   sw.Stop();
            //   Console.WriteLine($"AutodiscoverUrl() in {sw.ElapsedMilliseconds:n0}ms");
            try
            {
                AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
                
                if(boxType == "EzGo")
                {
                    userAccount = sysSetting.EwsEzgoAccount;
                    loginPw = sysSetting.EwsEzgoPassword;
                }else if(boxType == "EFax")
                {
                    userAccount = sysSetting.EwsEfaxAccount;
                    loginPw = sysSetting.EwsEfaxPassword;
                }

                ews.Credentials = new NetworkCredential(userAccount, loginPw, "tmnewa");
                ews.Url = new Uri("https://webmail.tmnewa.com.tw/ews/exchange.asmx");
            }
            catch (ServiceResponseException e)
            {
                Log4NetHelper.Error(e.Message);
                ews.AutodiscoverUrl(userAccount);
            }
            return ews;
        }
        public static async Task<ReceiveResultEntity> CheckInbox(string boxType, IWebHostEnvironment hostingEnvironment)
        {
            var ews = Connection(boxType);
            var receiveResultEntity = new ReceiveResultEntity();
            try
            {
                // 繫結到收件匣
                var inbox = await Folder.Bind(ews, WellKnownFolderName.Inbox);
                // 篩選未讀信件
                SearchFilter sf = new SearchFilter.SearchFilterCollection(LogicalOperator.And,
                    new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false));

                ItemView view = new ItemView(10);
                // Fire the query for the unread items.
                // This method call results in a FindItem call to EWS.
                //FindItemsResults<Item> findResults = ews.FindItems(WellKnownFolderName.Inbox, sf, view);
                var findResults = await ews.FindItems(WellKnownFolderName.Inbox, sf, view);
                foreach (var item in findResults.Items)
                {
                    MailFile mailFile = new MailFile();
                    string subject = item.Subject;
                    //if (item is EmailMessage )正式版使用
                    if (item is EmailMessage && subject.StartsWith("#AUROBOT"))
                    {
                        //信件內容及附件要 Load() 才會載入
                        await item.Load();
                        var mail = item as EmailMessage;
                       
                        var text = mail.Body.Text;
                        List<MailFile> lstMailfile = new List<MailFile>();
                        if (mail.HasAttachments)
                        {
                            foreach (Attachment attachment in mail.Attachments)
                            {
                                if (attachment is FileAttachment)
                                {
                                    FileAttachment fileAttachment = attachment as FileAttachment;
                                    // Load the attachment into a file.
                                    // This call results in a GetAttachment call to EWS.
                                    await fileAttachment.Load();
                                    mailFile = UploadFile(fileAttachment.Name, fileAttachment.Content, hostingEnvironment);
                                    //Console.WriteLine("File attachment name: " + fileAttachment.Name);
                                }
                                else // Attachment is an item attachment.
                                {
                                    ItemAttachment itemAttachment = attachment as ItemAttachment;
                                    // Load attachment into memory and write out the subject.
                                    // This does not save the file like it does with a file attachment.
                                    // This call results in a GetAttachment call to EWS.
                                    await itemAttachment.Load();
                                    //Console.WriteLine("Item attachment name: " + itemAttachment.Name);
                                }

                                lstMailfile.Add(new MailFile { MailFilePath = mailFile.MailFilePath, MailFileType = mailFile.MailFileType, MailFileSubType = mailFile.MailFileSubType });
                            }

                        }

                        var mailBody = new MailBodyEntity

                        {
                            Body = !string.IsNullOrEmpty(mail.Body) ? (mail.Body) : mail.Body.Text,
                            Subject = mail.Subject,
                            EWSItemId = JsonSerializer.Serialize(mail.Id)
                    };
                      


                        if ((lstMailfile != null) && (lstMailfile.Any()))
                        {
                            mailBody.MailFiles.AddRange(lstMailfile);
                        }
                        mailBody.FromAddresses.Add(new FromEmailAddress { Address = mail.From.Address, Name = mail.From.Name });
                        receiveResultEntity.bodyEntities.Add(mailBody);
                        mail.IsRead = true;
                        await mail.Update(ConflictResolutionMode.AutoResolve);

                    }
                }
                receiveResultEntity.ResultInformation = $"接收完成:{0}" + findResults.Items.Count;
                receiveResultEntity.ResultStatus = true;

                return receiveResultEntity;
            }
            catch(Exception ex)
            {
                Log4NetHelper.Error(ex.Message);
                receiveResultEntity.ResultInformation = $"接收錯誤:{0}" + ex.Message;
                receiveResultEntity.ResultStatus = false;
                return receiveResultEntity;
            }
            

        }

        public static bool SendEmail(MailBodyEntity mailBodyEntity)
        {
            try
            {
                var ews = Connection(mailBodyEntity.BoxType);
                var mail = new EmailMessage(ews);
                //var inbox =  Folder.Bind(ews, WellKnownFolderName.Inbox);
              
                foreach (var recipient in mailBodyEntity.Recipients)
                {
                    if (!string.IsNullOrEmpty(recipient))
                        mail.ToRecipients.Add(recipient);
                }
                //mail.ToRecipients.Add("victor.cheng@aurocore.com");
                mail.Subject = mailBodyEntity.Subject;
                mail.Body = new MessageBody(BodyType.HTML, mailBodyEntity.Body);
                if((mailBodyEntity.MailFiles != null) && (mailBodyEntity.MailFiles.Any()))
                {
                    foreach (var file in mailBodyEntity.MailFiles)
                    {
                        if (!string.IsNullOrEmpty(file.MailFilePath))
                            mail.Attachments.AddFileAttachment(file.MailFilePath);
                    }
                }


                 mail.Send();  
                //或是 mail.SendAndSaveCopy();
                //mail.SendAndSaveCopy();
                return true;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error(ex.Message);
                return false;


            }



        }
        public static async Task<bool> ReplyEmail(MailBodyEntity mailBodyEntity)
        {
            try
            {
                var ews = Connection(mailBodyEntity.Sender);
                var mail = new EmailMessage(ews);
                ItemId itemId = JsonSerializer.Deserialize<ItemId>(mailBodyEntity.EWSItemId);
                var message = await EmailMessage.Bind(ews, itemId, BasePropertySet.IdOnly);
    
                bool replyToAll = true;
                ResponseMessage responseMessage = message.CreateReply(replyToAll);
             
                responseMessage.BodyPrefix = new MessageBody(BodyType.HTML, mailBodyEntity.Body);
                var reply = await responseMessage.Save();
                foreach (var file in mailBodyEntity.MailFiles)
                {
                    if (!string.IsNullOrEmpty(file.MailFilePath))
                       
                        reply.Attachments.AddFileAttachment(file.MailFilePath);
                }

                await reply.Update(ConflictResolutionMode.AutoResolve);
                await reply.SendAndSaveCopy();
 
               
                var boolResult=  await FindRecentlySent(ews,message);
                if (boolResult)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception e)
            {
                Log4NetHelper.Error(e.Message);
                return false;


            }



        }
        private static async Task<bool> FindRecentlySent(ExchangeService service, EmailMessage messageToCheck)
        {
            // Create extended property definitions for PidTagLastVerbExecuted and PidTagLastVerbExecutionTime.
            ExtendedPropertyDefinition PidTagLastVerbExecuted = new ExtendedPropertyDefinition(0x1081, MapiPropertyType.Integer);
            ExtendedPropertyDefinition PidTagLastVerbExecutionTime = new ExtendedPropertyDefinition(0x1082, MapiPropertyType.SystemTime);
            PropertySet propSet = new PropertySet(BasePropertySet.IdOnly, EmailMessageSchema.Subject, PidTagLastVerbExecutionTime, PidTagLastVerbExecuted);
            messageToCheck = await EmailMessage .Bind(service, messageToCheck.Id, propSet);
            // Determine the last verb executed on the message and display output.
            //responseString = string.Empty;
            object responseType;
            if (messageToCheck.TryGetProperty(PidTagLastVerbExecuted, out responseType))
            {
                object ReplyTime = null;
                switch (((Int32)responseType))
                {
                    case 102:
                        //responseString = "A reply was sent to the '" + messageToCheck.Subject.ToString() + "' email message at";

                        break;
                       
                    case 103:
                       // responseString = ("A reply all was sent to the '" + messageToCheck.Subject.ToString() + "' email message at");
                        break;

                    case 104:
                      //  responseString = ("The '" + messageToCheck.Subject.ToString() + "' email message was forwarded at");
                       break;
                }
                if (messageToCheck.TryGetProperty(PidTagLastVerbExecutionTime, out ReplyTime))
                {
                 //   responseString = responseString+(((DateTime)ReplyTime).ToString() + ".");
                    
                }
                return true;
            }
            else
            {

               // responseString = ("No changes were made to  '" + messageToCheck.Subject.ToString() + "'.");
                return false;
            }
        }
  
        private static MailFile UploadFile(string fileName, byte[] fileBuffers,IWebHostEnvironment hostingEnvironment)
        {
            MailFile mailFile = new MailFile();
            //判斷檔案是否為空
            if (string.IsNullOrEmpty(fileName))
            {
                Log4NetHelper.Info("檔名不能為空");
                throw new Exception("檔名不能為空");
            }

            //判斷檔案是否為空
            if (fileBuffers.Length < 1)
            {
                Log4NetHelper.Info("檔案不能為空");
                throw new Exception("檔案不能為空");
            }

            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            string folder = DateTime.Now.ToString("yyyyMMdd");
            _filePath = hostingEnvironment.WebRootPath;
            var _tempfilepath = sysSetting.Filepath;

            if (!string.IsNullOrEmpty(_belongApp))
            {
                _tempfilepath += "/" + _belongApp;
            }
            if (!string.IsNullOrEmpty(_belongAppId))
            {
                _tempfilepath += "/" + _belongAppId;
            }
            if (sysSetting.Filesave == "1")
            {
                _tempfilepath = _tempfilepath + "/" + folder + "/";
            }
            if (sysSetting.Filesave == "2")
            {
                DateTime date = DateTime.Now;
                _tempfilepath = _tempfilepath + "/" + date.Year + "/" + date.Month + "/" + date.Day + "/";
            }

            var uploadPath = _filePath + "/" + _tempfilepath;
            if (sysSetting.Fileserver == "localhost")
            {
                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }
            }
            string ext = Path.GetExtension(fileName).ToLower();
            string newName = GuidUtils.CreateNo();
            string newfileName = newName + ext;

            using (var fs = new FileStream(uploadPath + newfileName, FileMode.Create))
            {
                fs.Write(fileBuffers, 0, fileBuffers.Length);
                fs.Close();
                //產生縮圖
                if (ext.Contains(".jpg") || ext.Contains(".jpeg") || ext.Contains(".png") || ext.Contains(".bmp") || ext.Contains(".gif"))
                {
                    string thumbnailName = newName + "_" + sysSetting.Thumbnailwidth + "x" + sysSetting.Thumbnailheight + ext;
                    ImgHelper.MakeThumbnail(uploadPath + newfileName, uploadPath + thumbnailName, sysSetting.Thumbnailwidth.ToInt(), sysSetting.Thumbnailheight.ToInt());
                    _dbThumbnail = _tempfilepath + thumbnailName;
                }
                _dbFilePath = _tempfilepath + newfileName;
                string mime = MimeKit.MimeTypes.GetMimeType(fileName);
                //string ext = System.IO.Path.GetExtension(attachment).ToLower();
                mailFile.MailFilePath = _dbFilePath;
                mailFile.MailFileSubType = ext;
                mailFile.MailFileType = mime;
                return mailFile;
            }
            

        }
    }

}
