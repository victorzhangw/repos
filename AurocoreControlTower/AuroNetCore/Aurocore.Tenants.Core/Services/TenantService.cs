using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Tenants.Dtos;
using Aurocore.Tenants.IRepositories;
using Aurocore.Tenants.IServices;
using Aurocore.Tenants.Models;

namespace Aurocore.Tenants.Services
{
	/// <summary>
	/// 租戶服務介面實現
	/// </summary>
	public class TenantService: BaseService<Tenant,TenantOutputDto, string>, ITenantService
	{
		private readonly ITenantRepository _repository;
		public TenantService(ITenantRepository repository) : base(repository)
		{
			_repository=repository;
		}

		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public override async Task<PageResult<TenantOutputDto>> FindWithPagerAsync(SearchInputDto<Tenant> search)
		{
			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " and (TenantName like '%" + search.Keywords + "%' or CompanyName like '%" + search.Keywords + "%')";
			};
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<Tenant> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
			PageResult<TenantOutputDto> pageResult = new PageResult<TenantOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<TenantOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}
	}
}