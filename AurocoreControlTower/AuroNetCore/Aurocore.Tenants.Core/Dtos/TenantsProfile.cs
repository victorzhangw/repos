using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Tenants.Models;

namespace Aurocore.Tenants.Dtos
{
    public class TenantsProfile : Profile
    {
        public TenantsProfile()
        {
           CreateMap<Tenant, TenantOutputDto>();
           CreateMap<TenantInputDto, Tenant>();

        }
    }
}
