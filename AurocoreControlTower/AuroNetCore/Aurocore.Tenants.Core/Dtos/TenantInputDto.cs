using AutoMapper;
using System;
using Aurocore.Commons.Dtos;
using Aurocore.Tenants.Models;

namespace Aurocore.Tenants.Dtos
{
    /// <summary>
    /// 租戶輸入物件模型
    /// </summary>
    [AutoMap(typeof(Tenant))]
    [Serializable]
    public class TenantInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取租戶名稱
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// 設定或獲取公司名稱
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 設定或獲取職位問域名
        /// </summary>
        public string HostDomain { get; set; }

        /// <summary>
        /// 設定或獲取聯繫人
        /// </summary>
        public string LinkMan { get; set; }

        /// <summary>
        /// 設定或獲取聯繫電話
        /// </summary>
        public string Telphone { get; set; }

        /// <summary>
        /// 設定或獲取資料源，分庫使用
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// 設定或獲取租戶介紹
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否可用
        /// </summary>
        public bool? EnabledMark { get; set; }


    }
}
