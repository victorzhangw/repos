using System;
using Aurocore.Commons.IServices;
using Aurocore.Tenants.Dtos;
using Aurocore.Tenants.Models;

namespace Aurocore.Tenants.IServices
{
    /// <summary>
    /// 定義租戶服務介面
    /// </summary>
    public interface ITenantService:IService<Tenant,TenantOutputDto, string>
    {
    }
}
