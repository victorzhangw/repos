using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Tenants.Models;

namespace Aurocore.Tenants.IRepositories
{
    /// <summary>
    /// 定義租戶倉儲介面
    /// </summary>
    public interface ITenantRepository:IRepository<Tenant, string>
    {
    }
}