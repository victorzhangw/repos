using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Tenants.IRepositories;
using Aurocore.Tenants.Models;

namespace Aurocore.Tenants.Repositories
{
	/// <summary>
	/// 租戶倉儲介面的實現
	/// </summary>
	public class TenantRepository : BaseRepository<Tenant, string>, ITenantRepository
	{
		public TenantRepository()
		{
		}
		/// <summary>
		/// 注入EF
		/// </summary>
		/// <param name="context"></param>
		public TenantRepository(IDbContextCore context) : base(context)
		{
		}
	}
}