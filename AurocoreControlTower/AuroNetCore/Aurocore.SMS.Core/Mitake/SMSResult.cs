using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.SMS.Mitake
{
    public class SMSResult
    {
        /// <summary>
        /// 發送客戶ID
        /// </summary>
        public string Clientid { get; set; }
        /// <summary>
        /// 簡訊序號
        /// </summary>
        public string Msgid { get; set; }
        /// <summary>
        /// 狀態碼描述。
        /// </summary>
        public string Statuscode { get; set; }
        /// <summary>
        /// 請求ID。
        /// </summary>
        public string AccountPoint { get; set; }
        /// <summary>
        /// 是否重複
        /// </summary>
        public string Duplicate { get; set; }
    }
}
