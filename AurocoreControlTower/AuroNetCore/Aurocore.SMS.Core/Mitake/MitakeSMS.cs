
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Options;
using Aurocore.Commons.Extend;
using Aurocore.SMS.Mitake;
using RestSharp;


namespace Aurocore.SMS.Mitake
{
    /// <summary>
    /// 三竹簡訊介面
    /// </summary>
    public class MitakeSMS
    {
      
        //簡訊API產品域名
        private const string domain = "https://smsb2c.mitake.com.tw";

        public MitakeSMS()
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            AppSetting sysSetting = JsonConvert.DeserializeObject<AppSetting>(AurocoreCacheHelper.Get("SysSetting").ToJson());
            if (sysSetting != null)
            {
                this.Appkey = sysSetting.Smsusername;
                this.Appsecret = sysSetting.Smspassword;
                this.SignName = sysSetting.SmsSignName;
            }
        }


        



        /// <summary>
        /// 簡訊發送
        /// </summary>
        /// <param name="cellPhone">必填:待發送手機號。支援以逗號分隔的形式進行批量呼叫，批量上限為1000個手機號碼,批量呼叫相對於單條呼叫及時性稍有延遲,驗證碼型別的簡訊推薦使用單條呼叫的方式，發送國際/港澳臺訊息時，接收號碼格式為00+國際區號+號碼，如「0085200000000」</param>
        /// <param name="templateCode">模板code</param>
        /// <param name="message">可選:模板中的變數替換JSON串,如模板內容為"親愛的${name},您的驗證碼為${code}"時,此處的值為 "{\"name\":\"Tom\"， \"code\":\"123\"}"</param>
        /// <param name="returnMsg">回傳資料</param>
        /// <returns></returns>
        public  bool Send(string cellPhone, string templateCode, string message, string customerId, out string returnMsg)
        {
            if ((string.IsNullOrEmpty(cellPhone) || (cellPhone.Trim().Length == 0)))
            {
                returnMsg = "手機號碼和訊息內容不能為空";
                return false;
            }
            if (string.IsNullOrEmpty(message))
            {
                message = "";
            }else if (!string.IsNullOrEmpty(templateCode))
            {
                var dynamicMessage = JsonConvert.DeserializeObject<dynamic>(message);
                message = ExtString.TemplateStringFormat(templateCode, dynamicMessage);

            }
            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //message = HttpUtility.UrlEncode(message, Encoding.GetEncoding(950));


            try
            {
                string smsURL = $"https://smsb2c.mitake.com.tw/b2c/mtk/" +
                    $"SmSend?username={Appkey}&password={Appsecret}&dstaddr={cellPhone}&CharsetURL=UTF8&smbody={message}";

                var client = new RestClient(smsURL);

                /*
                 var client = new RestClient(domain);
                // client.Authenticator = new HttpBasicAuthenticator(username, password);
                var request = new RestRequest("b2c/mtk/SmSend", Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
               // request.AddQueryParameter("CharsetURL", "UTF-8");
                request.AddParameter("username", Appkey);
                request.AddParameter("password", Appsecret);
                request.AddParameter("dstaddr", cellPhone);
                
                request.AddParameter("smbody", message);
                request.AddParameter("clientid", customerId);
                */
                //RestResponse<SMSResult> sMSResult = (RestResponse<SMSResult>)client.Execute<SMSResult>(request);
                var request = new RestRequest(Method.POST);
                IRestResponse sMSResult = client.Execute(request);
                string[] sMSreader = sMSResult.Content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                returnMsg = sMSreader[2];
                if (sMSreader[1] == "0")
                {
                    return true;
                }
                else
                {
                    Log4NetHelper.Error("發送簡訊錯誤,錯誤代碼："+ sMSreader[2]); 
                    return false;
                }


            }
            catch (Exception ex)
            {
                returnMsg = "未知錯誤(ServerException )" + ex.Message;
                return false;
            }
            
        }
        /// <summary>
        /// 簡訊發送
        /// </summary>
        /// <param name="cellPhone">必填:待發送手機號</param>
        /// <param name="message">訊息</param>
        /// <param name="returnMsg">回傳資料</param>
        /// <returns></returns>
        public bool  Send(string cellPhone, string message, out string returnMsg)
        {
            if ((string.IsNullOrEmpty(cellPhone) || (cellPhone.Trim().Length == 0)))
            {
                returnMsg = "手機號碼和訊息內容不能為空";
                return false;
            }
            if (string.IsNullOrEmpty(message))
            {
                message = "";
            }
           

            try
            {
                string smsURL = $"https://smsb2c.mitake.com.tw/b2c/mtk/" +
                    $"SmSend?username={Appkey}&password={Appsecret}&dstaddr={cellPhone}&CharsetURL=UTF8&smbody={message}";

                var client = new RestClient(smsURL);

                
                var request = new RestRequest(Method.POST);
                IRestResponse sMSResult = client.Execute(request);
                string[] sMSreader = sMSResult.Content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                returnMsg = sMSreader[3];
                if (sMSreader[2] == "0")
                {
                    return true;
                }
                else
                {
                    Log4NetHelper.Error("發送簡訊錯誤,錯誤代碼：" + sMSreader[2]);
                    return false;
                }


            }
            catch (Exception ex)
            {
                returnMsg = "未知錯誤(ServerException )" + ex.Message;
                return false;
            }

        }
        /// <summary>
        /// Appkey 應用Id
        /// </summary>
        public string Appkey { get; set; }
        /// <summary>
        /// Appsecret應用金鑰
        /// </summary>
        public string Appsecret { get; set; }

        /// <summary>
        /// 簽名
        /// </summary>
        public string SignName { get; set; }

    }
}
