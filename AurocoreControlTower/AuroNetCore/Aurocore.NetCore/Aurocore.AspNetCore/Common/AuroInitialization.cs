using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Helpers;
using Aurocore.Security.Models;

namespace Aurocore.AspNetCore.Common
{
    /// <summary>
    /// 系統初始化內容
    /// </summary>
    public static class AurocoreInitialization
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public  static void Initial()
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            if (sysSetting != null)
            {
                AurocoreCacheHelper.Add("SysSetting", sysSetting);
            }
       }
    }
}
