using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Aurocore.AspNetCore.Common
{
    /// <summary>
    /// 定義常用功能的控制ID，方便基類控制器對使用者許可權的控制
    /// </summary>
    [DataContract]
    [Serializable]
    public class AuthorizeKey
    {
        #region 常規功能控制ID
        /// <summary>
        /// 新增記錄的功能控制ID
        /// </summary>
        public string InsertKey { get; set; }

        /// <summary>
        /// 更新記錄的功能控制ID
        /// </summary>
        public string UpdateKey { get; set; }
        /// <summary>
        /// 啟用/禁用記錄的功能控制ID
        /// </summary>
        public string UpdateEnableKey { get; set; }
        /// <summary>
        /// 禁用記錄的功能控制ID
        /// </summary>
        public string UpdateIsEnableKey { get; set; }

        /// <summary>
        /// 啟用記錄的功能控制ID
        /// </summary>
        public string UpdateNoEnableKey { get; set; }
        /// <summary>
        /// 刪除記錄的功能控制ID
        /// </summary>
        public string DeleteKey { get; set; }

        /// <summary>
        /// 軟刪除記錄的功能控制ID
        /// </summary>
        public string DeleteSoftKey { get; set; }

        /// <summary>
        /// 檢視列表的功能控制ID
        /// </summary>
        public string ListKey { get; set; }

        /// <summary>
        /// 檢視明細的功能控制ID
        /// </summary>
        public string ViewKey { get; set; }

        /// <summary>
        /// 導出記錄的功能控制ID
        /// </summary>
        public string ExportKey { get; set; }
        /// <summary>
        /// 匯入記錄的功能控制ID
        /// </summary>
        public string ImportKey { get; set; }

        /// <summary>
        /// 擴充套件的功能控制IDS
        /// </summary>
        public string ExtendKey { get; set; }
        #endregion

        #region 常規許可權判斷
        /// <summary>
        /// 判斷是否具有插入許可權
        /// </summary>
        public bool CanInsert { get; set; }

        /// <summary>
        /// 判斷是否具有更新許可權
        /// </summary>
        public bool CanUpdate { get; set; }

        /// <summary>
        /// 判斷是否具有啟用/禁用許可權
        /// </summary>
        public bool CanUpdateEnable { get; set; }
        /// <summary>
        /// 判斷是否具有禁用許可權
        /// </summary>
        public bool CanUpdateIsEnable { get; set; }
        /// <summary>
        /// 判斷是否具有啟用許可權
        /// </summary>
        public bool CanUpdateNoEnable { get; set; }
        /// <summary>
        /// 判斷是否具有刪除許可權
        /// </summary>
        public bool CanDelete { get; set; }

        /// <summary>
        /// 判斷是否具有軟刪除許可權
        /// </summary>
        public bool CanDeleteSoft { get; set; }
        /// <summary>
        /// 判斷是否具有列表許可權
        /// </summary>
        public bool CanList { get; set; }

        /// <summary>
        /// 判斷是否具有檢視許可權
        /// </summary>
        public bool CanView { get; set; }

        /// <summary>
        /// 判斷是否具有導出許可權
        /// </summary>
        public bool CanExport { get; set; }
        /// <summary>
        /// 判斷是否具有匯入許可權
        /// </summary>
        public bool CanImport { get; set; }
        /// <summary>
        /// 判斷是否具有擴充套件功能許可權
        /// </summary>
        public bool CanExtend { get; set; }

        #endregion

        /// <summary>
        /// 預設建構函式
        /// </summary>
        public AuthorizeKey() { }

        /// <summary>
        /// 常用建構函式
        /// </summary>
        public AuthorizeKey(string insert, string update, string delete, string view = "")
        {
            this.InsertKey = insert;
            this.UpdateKey = update;
            this.DeleteKey = delete;
            this.ViewKey = view;
        }
        /// <summary>
        /// 常用建構函式
        /// </summary>
        public AuthorizeKey(string insert, string update, string delete, string deletesoft, string updateenable, string import, string view = "")
        {
            this.InsertKey = insert;
            this.UpdateKey = update;
            this.DeleteKey = delete;
            this.UpdateEnableKey = updateenable;
            this.DeleteSoftKey = deletesoft;
            this.ImportKey = import;
            this.ViewKey = view;
        }
    }
}
