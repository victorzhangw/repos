using IdentityModel;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Common;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Models;
using Aurocore.Commons.Options;
using Aurocore.Security.Models;

namespace Aurocore.AspNetCore.Mvc
{
    /// <summary>
    /// Token令牌提供類
    /// </summary>
    public class TokenProvider
    {
        JwtOption _jwtModel=IoCContainer.Resolve<JwtOption>();
        private Type type = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
        /// <summary>
        /// 建構函式
        /// </summary>
        public TokenProvider() { }
        /// <summary>
        /// 建構函式，初花jwtmodel
        /// </summary>
        /// <param name="jwtModel"></param>
        public TokenProvider(JwtOption jwtModel)
        {
            _jwtModel = jwtModel;
        }
        /// <summary>
        /// 直接通過appid和加密字串獲取職位問令牌介面
        /// </summary>
        /// <param name="granttype">獲取access_token填寫client_credential</param>
        /// <param name="appid">使用者唯一憑證</param>
        /// <param name="secret">使用者唯一憑證金鑰，即appsecret</param>
        /// <returns></returns>
        public TokenResult GenerateToken(string granttype, string appid, string secret)
        {
            var keyByteArray = Encoding.UTF8.GetBytes(secret);
            var signingKey = new SymmetricSecurityKey(keyByteArray);
            var expires = DateTime.UtcNow.Add(TimeSpan.FromMinutes(_jwtModel.Expiration));
            var signingCredentials=new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            var tokenDescripor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(JwtClaimTypes.Audience,appid),
                    new Claim(JwtClaimTypes.Issuer,_jwtModel.Issuer),
                    new Claim(JwtClaimTypes.Subject, GrantType.ClientCredentials)
                }, granttype),
                Expires = expires,
                //對稱秘鑰SymmetricSecurityKey
                //簽名證書(秘鑰，加密演算法)SecurityAlgorithms
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyByteArray), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescripor);
            var tokenString = tokenHandler.WriteToken(token);
            TokenResult result = new TokenResult();
            result.AccessToken = tokenString;
            result.ExpiresIn = (int)TimeSpan.FromMinutes(_jwtModel.Expiration).TotalMinutes;
            return  result;
        }
        /// <summary>
        /// 檢查使用者的Token有效性
        /// </summary>
        /// <param name="token">token令牌</param>
        /// <returns></returns>
        public CommonResult ValidateToken(string token)
        {
            //返回的結果物件
            CommonResult result = new CommonResult();
            if (!string.IsNullOrEmpty(token))
            {
                try
                {
                    JwtSecurityToken jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
                    if (jwtToken!=null)
                    {
                        #region 檢查令牌物件內容
                        DateTime now = DateTime.UtcNow;
                        DateTime refreshTime = jwtToken.ValidFrom;
                        refreshTime= refreshTime.Add(TimeSpan.FromMinutes(_jwtModel.refreshJwtTime));
                        if (now > refreshTime && jwtToken.Issuer== _jwtModel.Issuer)
                        {
                            result.ErrMsg = ErrCode.err40005;
                            result.ErrCode = "40005";
                        }
                        else
                        {
                            if (jwtToken.Subject == GrantType.Password)
                            {
                                var claimlist = jwtToken?.Payload.Claims as List<Claim>;
                                result.ResData= claimlist;
                            }
                            result.ErrMsg = ErrCode.err0;
                            result.ErrCode = ErrCode.successCode;
                            
                        }
                        #endregion
                    }
                    else
                    {
                        result.ErrMsg = ErrCode.err40004;
                        result.ErrCode = "40004";
                    }
                }
                catch (Exception ex)
                {
                    Log4NetHelper.Error(type, "驗證token異常", ex);
                    throw new MyApiException(ErrCode.err40004, "40004");
                }
            }
            else
            {
                result.ErrMsg = ErrCode.err40004;
                result.ErrCode = "40004";
            }
            return result;
        }

        /// <summary>
        /// 根據使用者獲取token
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="appid"></param>
        /// <returns></returns>
        public TokenResult LoginToken(User userInfo,string appid)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_jwtModel.Secret);
            var authTime = DateTime.UtcNow;//授權時間
            var expires = authTime.Add(TimeSpan.FromMinutes(_jwtModel.Expiration));//過期時間
            var tokenDescripor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(JwtClaimTypes.Audience,appid),
                    new Claim(JwtClaimTypes.Issuer,_jwtModel.Issuer),
                    new Claim(JwtClaimTypes.Name, userInfo.Account),
                    new Claim(JwtClaimTypes.Id, userInfo.Id),
                    new Claim(JwtClaimTypes.Role, userInfo.RoleId),
                    new Claim(JwtClaimTypes.Subject, GrantType.Password)
                }),
                Expires = expires,
                //對稱秘鑰SymmetricSecurityKey
                //簽名證書(秘鑰，加密演算法)SecurityAlgorithms
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescripor);
            var tokenString = tokenHandler.WriteToken(token);
            TokenResult result = new TokenResult();
            result.AccessToken = tokenString;
            result.ExpiresIn = (int)TimeSpan.FromMinutes(_jwtModel.Expiration).TotalMinutes;
            return result;
        }
    }
}
