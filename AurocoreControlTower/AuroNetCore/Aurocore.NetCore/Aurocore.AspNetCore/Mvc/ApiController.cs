using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using Aurocore.AspNetCore.Common;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;
using Aurocore.Security.Repositories;

namespace Aurocore.AspNetCore.Controllers
{
    /// <summary>
    /// WebApi控制器基類
    /// </summary>
    [ApiController]
    [EnableCors("aurocoreCors")]
    public class ApiController : Controller
    {
        /// <summary>
        /// 目前登錄的使用者屬性
        /// </summary>
        public AurocoreCurrentUser CurrentUser;
        private ILogRepository service = new LogRepository();
        #region 
        /// <summary>
        /// 重寫基類在Action執行之前的事情
        /// 根據token獲得目前使用者，允許匿名的不需要獲取使用者
        /// </summary>
        /// <param name="filterContext">重寫方法的參數</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerActionDescriptor = filterContext.ActionDescriptor as ControllerActionDescriptor;

            try
            {   //匿名不需要token認證
                var allowanyone = controllerActionDescriptor.ControllerTypeInfo.GetCustomAttributes(typeof(IAllowAnonymous), true).Any()
                || controllerActionDescriptor.MethodInfo.GetCustomAttributes(typeof(IAllowAnonymous), true).Any();
                if (!allowanyone)
                {
                    var identities = filterContext.HttpContext.User.Identities;
                    var claimsIdentity = identities.First<ClaimsIdentity>();
                    if (claimsIdentity != null)
                    {
                        List<Claim> claimlist= claimsIdentity.Claims as List<Claim>;
                        if (claimlist.Count > 0)
                        {
                            string userId = claimlist[0].Value;
                            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                            var user = AurocoreCacheHelper.Get("login_user_" + userId).ToJson().ToObject<AurocoreCurrentUser>();
                            if (user != null)
                            {
                                CurrentUser = user;
                            }
                        }
                    }
                }
                Log logEntity = new Log();
                if (CurrentUser != null)
                {
                    logEntity.Account = CurrentUser.Account;
                    logEntity.NickName = CurrentUser.NickName;
                    logEntity.IPAddressName = CurrentUser.IPAddressName;
                }
                logEntity.IPAddress = filterContext.HttpContext.Connection.RemoteIpAddress.ToString();
                logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                logEntity.Result = false;
                logEntity.Description = filterContext.HttpContext.Request.Path + filterContext.HttpContext.Request.QueryString;
                logEntity.Type = "Visit";
                service.Insert(logEntity);
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("", ex);
                //throw new MyApiException("", "", ex);
            }
        }
        #endregion

        /// <summary>
        /// 把object物件轉換為ContentResult
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ToJsonContent")]
        protected IActionResult ToJsonContent(object obj)
        {
            return Content(obj.ToJson());
        }


        /// <summary>
        /// 把object物件轉換為ContentResult
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="isNull"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ToJsonContent")]
        protected IActionResult ToJsonContent(object obj,bool isNull=false)
        {
            JsonSerializerOptions options = new JsonSerializerOptions()
            {
                WriteIndented = true,                                   //格式化json字串
                AllowTrailingCommas = true,                             //可以結尾有逗號
                IgnoreNullValues = true,                              //可以有空值,轉換json去除空值屬性
                IgnoreReadOnlyProperties = true,                        //忽略只讀屬性
                PropertyNameCaseInsensitive = true,                     //忽略大小寫
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All)
            };
            options.Converters.Add(new DateTimeJsonConverter());
            return Content(JsonSerializer.Serialize(obj, options));
        }

        /// <summary>
        /// 根據Request參數獲取分頁物件資料
        /// </summary>
        /// <returns></returns>
        protected virtual PagerInfo GetPagerInfo()
        {
            int pageSize = Request.Query["length"].ToString() == null ? 1 : Request.Query["length"].ToString().ToInt();
            int pageIndex = 1;
            string currentPage = Request.Query["CurrentPage"].ToString();
            if (string.IsNullOrWhiteSpace(currentPage))
            {
                string start = Request.Query["start"].ToString();
                if (!string.IsNullOrWhiteSpace(start))
                {
                    pageIndex = (start.ToInt() / pageSize) + 1;
                }
            }
            else
            {
                pageIndex = currentPage.ToInt();
            }
            PagerInfo pagerInfo = new PagerInfo();
            pagerInfo.CurrenetPageIndex = pageIndex;
            pagerInfo.PageSize = pageSize;
            return pagerInfo;
        }

        /// <summary>
        /// 獲取token
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetToken")]
        [HiddenApi]
        public string GetToken()
        {
            string token = HttpContext.Request.Query["Token"];
            if (!String.IsNullOrEmpty(token)) return token;
            string authHeader = HttpContext.Request.Headers["Authorization"];//Header中的token
            if (authHeader != null && authHeader.StartsWith("Bearer"))
            {
                token = authHeader.Substring("Bearer ".Length).Trim();
                return token;
            }
            var cookie = HttpContext.Request.Cookies["Token"];
            return cookie == null ? String.Empty : cookie;
        }

    }
}
