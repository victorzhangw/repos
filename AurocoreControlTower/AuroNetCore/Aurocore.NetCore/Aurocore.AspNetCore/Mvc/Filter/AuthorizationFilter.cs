using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Common;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Json;
using Aurocore.Commons.Models;
using Aurocore.Security.Application;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.AspNetCore.Mvc
{
    /// <summary>
    /// 功能許可權授權驗證篩選
    /// </summary>
    public class AurocoreAuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// 授權驗證
        /// </summary>
        /// <param name="context"></param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (!(context.ActionDescriptor is ControllerActionDescriptor))
            {
                return;
            }
            //匿名職位問不需要token認證
            var allowanyone = controllerActionDescriptor.ControllerTypeInfo.GetCustomAttributes(typeof(IAllowAnonymous), true).Any()
            || controllerActionDescriptor.MethodInfo.GetCustomAttributes(typeof(IAllowAnonymous), true).Any();
            if (allowanyone)
            {
                return;
            }
            CommonResult result = new CommonResult();
            JsonSerializerOptions options = new JsonSerializerOptions()
            {
                WriteIndented = true,                                   //格式化json字串
                AllowTrailingCommas = true,                             //可以結尾有逗號
                //IgnoreNullValues = true,                              //可以有空值,轉換json去除空值屬性
                IgnoreReadOnlyProperties = true,                        //忽略只讀屬性
                PropertyNameCaseInsensitive = true,                     //忽略大小寫
                                                                        //PropertyNamingPolicy = JsonNamingPolicy.CamelCase     //命名方式是預設還是CamelCase
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.All)
            };
            options.Converters.Add(new DateTimeJsonConverter());
            //需要token認證
            string authHeader = context.HttpContext.Request.Headers["Authorization"];//Header中的token
            if (string.IsNullOrEmpty(authHeader))
            {

                result.ErrCode = "40004";
                result.ErrMsg = ErrCode.err40004;
                context.Result = new JsonResult(result, options);
                return;
            }
            else
            {
                string token = string.Empty;
                if (authHeader != null && authHeader.StartsWith("Bearer",StringComparison.Ordinal) && authHeader.Length > 10)
                {
                    token = authHeader.Substring("Bearer ".Length).Trim();
                }
                TokenProvider tokenProvider = new TokenProvider();
                result = tokenProvider.ValidateToken(token);
                //token驗證失敗
                if (!result.Success)
                {
                    context.Result = new JsonResult(result);
                }
                else
                {
                    //是否需要使用者登錄
                    var isDefined = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                          .Any(a => a.GetType().Equals(typeof(NoPermissionRequiredAttribute))) ;
                    //不需要登錄
                    if (isDefined)
                    {
                        return;
                    }
                    //需要登錄和驗證功能許可權
                    if (result.ResData != null)
                    {
                        List<Claim> claimlist = result.ResData as List<Claim>;
                        string userId = claimlist[3].Value;
                        AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                        var user = JsonSerializer.Deserialize<AurocoreCurrentUser>(AurocoreCacheHelper.Get("login_user_" + userId).ToJson(), options);

                        if (user == null)
                        {
                            result.ErrCode = "40008";
                            result.ErrMsg = ErrCode.err40008;
                            context.Result = new JsonResult(result, options);
                            return;
                        }
                        var claims = new[] {
                           new Claim(AurocoreClaimTypes.UserId,userId),
                           new Claim(AurocoreClaimTypes.UserName,claimlist[2].Value),
                           new Claim(AurocoreClaimTypes.Role,claimlist[4].Value),
                           new Claim(AurocoreClaimTypes.TenantId,user.TenantId)
                        };
                        var identity = new ClaimsIdentity(claims);
                        var principal = new ClaimsPrincipal(identity);
                        context.HttpContext.User = principal;
                        bool isAdmin = Permission.IsAdmin(user);
                        if (!isAdmin)
                        {
                            var authorizeAttributes = controllerActionDescriptor.MethodInfo.GetCustomAttributes(typeof(AurocoreAuthorizeAttribute), true).OfType<AurocoreAuthorizeAttribute>();
                            if (authorizeAttributes.FirstOrDefault() != null)
                            {
                                string function = authorizeAttributes.First().Function;
                                if (!string.IsNullOrEmpty(function))
                                {
                                    string functionCode = controllerActionDescriptor.ControllerName + "/" + function;

                                    bool bl = Permission.HasFunction(functionCode, userId);
                                    if (!bl)
                                    {
                                        result.ErrCode = "40006";
                                        result.ErrMsg = ErrCode.err40006;
                                        context.Result = new JsonResult(result, options);
                                    }
                                }
                            }
                        }
                        return;
                    }
                    else
                    {
                        result.ErrCode = "40008";
                        result.ErrMsg = ErrCode.err40008;
                        context.Result = new JsonResult(result, options);
                    }
                }
                return;
            }

        }
    }
}
