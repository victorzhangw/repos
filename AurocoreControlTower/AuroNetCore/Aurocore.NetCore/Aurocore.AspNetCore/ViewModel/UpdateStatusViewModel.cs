using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aurocore.AspNetCore.ViewModel
{
    /// <summary>
    /// 批量更新操作傳入參數，如設為禁用、有效、軟刪除；
    /// 物理刪除操作是Flag無效不用傳參
    /// </summary>
    [Serializable]
    public class UpdateStatusViewModel
    {
        /// <summary>
        /// 主鍵Id集合
        /// </summary>
        public dynamic[] Ids { get; set; }
        /// <summary>
        /// 擁有者
        /// </summary>
        public string OwnerID { get; set; }
        /// <summary>
        /// 狀態 ID
        /// </summary>
        public string StatusId { get; set; }
        /// <summary>
        /// 狀態名
        /// </summary>
        public string StatusName { get; set; }
        /// <summary>
        /// 是否全部記錄
        /// </summary>
        public bool? All { get; set; }
        /// <summary>
        /// 案件/單據類型
        /// </summary>
        public string FormTypeId { get; set; }
        /// <summary>
        /// 是否重要
        /// </summary>
        public bool Ishot { get; set; }

    }
}
