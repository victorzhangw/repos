using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Aurocore.AspNetCore.ViewModel
{
    /// <summary>
    /// 資料庫連線字串實體
    /// </summary>
    [Serializable]
    public class DbConnInfo
    {
        /// <summary>
        /// 職位問地址
        /// </summary>
        [DataMember]
        public string DbAddress { get; set; }
        /// <summary>
        /// 資料庫名稱
        /// </summary>
        [DataMember]
        public string DbName { get; set; }
        /// <summary>
        /// 使用者名稱
        /// </summary>
        [DataMember]
        public string DbUserName { get; set; }
        /// <summary>
        /// 職位問密碼
        /// </summary>
        [DataMember]
        public string DbPassword { get; set; }
        /// <summary>
        /// 資料庫型別
        /// </summary>
        [DataMember]
        public string DbType { get; set; }

    }
}
