using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.Commons.Pages;

namespace Aurocore.AspNetCore.ViewModel
{
    /// <summary>
    /// 查詢條件公共實體類
    /// </summary>
    [Serializable]
    public class SearchModel: PagerInfo
    {
        /// <summary>
        /// 關鍵詞
        /// </summary>
        public string Keywords
        {
            get; set;
        }
        /// <summary>
        /// 編碼/程式碼
        /// </summary>
        public string EnCode
        {
            get; set;
        }
        /// <summary>
        /// 排序方式 預設asc 
        /// </summary>
        public string Order
        {
            get; set;
        }
        /// <summary>
        /// 排序欄位 預設Id
        /// </summary>
        public string Sort
        {
            get; set;
        }
    }
}
