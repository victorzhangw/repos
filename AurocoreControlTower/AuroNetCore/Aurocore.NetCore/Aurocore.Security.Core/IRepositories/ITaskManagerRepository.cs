using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    /// <summary>
    /// 定義定時任務倉儲介面
    /// </summary>
    public interface ITaskManagerRepository:IRepository<TaskManager, string>
    {
    }
}