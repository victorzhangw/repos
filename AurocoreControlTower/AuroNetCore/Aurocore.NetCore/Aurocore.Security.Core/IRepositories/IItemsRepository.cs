using System;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface IItemsRepository:IRepository<Items, string>
    {
        /// <summary>
        /// ݱѯֵ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
       Task<Items> GetByEnCodAsynce(string enCode);
    }
}