using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUserDefinedRepository:IRepository<UserDefined, string>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<UserDefined> GetAllByUserId(string userId);
        List<UserDefined> GetCustoms(string userId,string dictId);

    }
}