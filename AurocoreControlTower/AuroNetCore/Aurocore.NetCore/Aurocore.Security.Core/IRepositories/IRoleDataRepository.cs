using System;
using System.Collections.Generic;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface IRoleDataRepository:IRepository<RoleData, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
       List<string> GetListDeptByRole(string roleIds);
    }
}