using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface IFilterIPRepository:IRepository<FilterIP, string>
    {
        /// <summary>
        /// ֤IPַ񱻾ܾ
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        bool ValidateIP(string ip);
    }
}