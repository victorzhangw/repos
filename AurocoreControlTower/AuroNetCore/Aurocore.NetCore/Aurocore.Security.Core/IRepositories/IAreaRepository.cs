using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface IAreaRepository:IRepository<Area,string>
    {
    }
}