using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    /// <summary>
    /// 定義序號編碼規則表倉儲介面
    /// </summary>
    public interface ISequenceRuleRepository:IRepository<SequenceRule, string>
    {
    }
}