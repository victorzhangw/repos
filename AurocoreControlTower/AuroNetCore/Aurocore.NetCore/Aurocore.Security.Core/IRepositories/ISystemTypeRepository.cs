using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface ISystemTypeRepository : IRepository<SystemType, string>
    {
        /// <summary>
        /// 根據系統編碼查詢系統物件
        /// </summary>
        /// <param name="appkey">系統編碼</param>
        /// <returns></returns>
        SystemType GetByCode(string appkey);
    }
}
