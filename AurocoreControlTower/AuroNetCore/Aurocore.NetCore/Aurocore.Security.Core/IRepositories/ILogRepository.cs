using System;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface ILogRepository:IRepository<Log, string>
    {
        long InsertTset(int len);
    }
}