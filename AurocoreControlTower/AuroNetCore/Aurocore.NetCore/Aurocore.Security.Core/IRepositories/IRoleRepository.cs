using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface IRoleRepository:IRepository<Role, string>
    {
    }
}