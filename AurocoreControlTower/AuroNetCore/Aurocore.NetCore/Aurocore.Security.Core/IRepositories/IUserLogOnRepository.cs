using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    public interface IUserLogOnRepository:IRepository<UserLogOn, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserLogOn GetByUserId(string userId);
    }
}