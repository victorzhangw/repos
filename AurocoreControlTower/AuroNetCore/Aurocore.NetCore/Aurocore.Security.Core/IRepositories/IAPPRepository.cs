using System;
using System.Collections.Generic;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAPPRepository:IRepository<APP,string>
    {
        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ID</param>
        /// <param name="secret">ԿAppSecret</param>
        /// <returns></returns>
        APP GetAPP(string appid, string secret);

        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ID</param>
        /// <returns></returns>
        APP GetAPP(string appid);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppOutputDto> SelectApp();
    }
}