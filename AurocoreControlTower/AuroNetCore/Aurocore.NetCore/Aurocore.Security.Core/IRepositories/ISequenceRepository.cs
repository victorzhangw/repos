using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    /// <summary>
    /// 定義單據編碼倉儲介面
    /// </summary>
    public interface ISequenceRepository:IRepository<Sequence, string>
    {
    }
}