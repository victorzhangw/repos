using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.IRepositories
{
    /// <summary>
    /// ִ֯ӿ
    /// õOrganizeҵ
    /// </summary>
    public interface IOrganizeRepository:IRepository<Organize, string>
    {
        /// <summary>
        /// ȡڵ֯
        /// </summary>
        /// <param name="id">֯Id</param>
        /// <returns></returns>
        Organize GetRootOrganize(string id);
    }
}