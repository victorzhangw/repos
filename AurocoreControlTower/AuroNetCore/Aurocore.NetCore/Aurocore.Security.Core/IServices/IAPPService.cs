using System;
using System.Collections.Generic;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAPPService:IService<APP, AppOutputDto, string>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="appid">ID</param>
        /// <param name="secret">ԿAppSecret</param>
        /// <returns></returns>
        APP GetAPP(string appid, string secret);

        /// <summary>
        ///
        /// </summary>
        /// <param name="appid">ID</param>
        /// <returns></returns>
        APP GetAPP(string appid);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppOutputDto> SelectApp();
    }
}
