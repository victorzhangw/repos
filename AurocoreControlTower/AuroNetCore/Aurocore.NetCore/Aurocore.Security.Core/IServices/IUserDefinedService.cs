using System;
using System.Collections.Generic;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    public interface IUserDefinedService : IService<UserDefined, UserDefinedOutputDto, string>
    {

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<UserDefined> GetAllByUserId(string userId);
        List<UserDefined> GetCustoms(string userId, string dictId);
    }
}
