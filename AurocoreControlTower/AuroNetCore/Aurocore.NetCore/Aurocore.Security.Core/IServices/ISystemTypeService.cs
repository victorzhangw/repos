using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// 子系統服務介面
    /// </summary>
    public interface ISystemTypeService : IService<SystemType, SystemTypeOutputDto, string>
    {

        /// <summary>
        /// 根據系統編碼查詢系統物件
        /// </summary>
        /// <param name="appkey">系統編碼</param>
        /// <returns></returns>
        SystemType GetByCode(string appkey);

        /// <summary>
        /// 根據角色獲取可以職位問子系統
        /// </summary>
        /// <param name="roleIds">角色Id，用','隔開</param>
        /// <returns></returns>
        List<SystemTypeOutputDto> GetSubSystemList(string roleIds);
    }
}
