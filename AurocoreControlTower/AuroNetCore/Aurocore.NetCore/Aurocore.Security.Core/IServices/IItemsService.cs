using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    public interface IItemsService:IService<Items, ItemsOutputDto, string>
    {

        /// <summary>
        /// ȡܲ˵Vue б
        /// </summary>
        /// <returns></returns>
        Task<List<ItemsOutputDto>> GetAllItemsTreeTable();

        /// <summary>
        /// ݱѯֵ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        Task<Items> GetByEnCodAsynce(string enCode);
    }
}
