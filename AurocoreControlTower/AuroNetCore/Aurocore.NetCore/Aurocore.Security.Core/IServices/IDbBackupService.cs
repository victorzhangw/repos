using System;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    public interface IDbBackupService:IService<DbBackup, DbBackupOutputDto, string>
    {
    }
}
