using System;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    public interface IUserLogOnService:IService<UserLogOn, UserLogOnOutputDto, string>
    {

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserLogOn GetByUserId(string userId);
    }
}
