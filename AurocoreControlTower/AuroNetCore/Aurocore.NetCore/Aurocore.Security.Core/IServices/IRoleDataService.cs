using System;
using System.Collections.Generic;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    public interface IRoleDataService:IService<RoleData, RoleDataOutputDto, string>
    {

        /// <summary>
        /// 依據角色取得 Role
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        List<string> GetListDeptByRole(string roleIds);
    }
}
