using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// ֵϸ
    /// </summary>
    public interface IItemsDetailService:IService<ItemsDetail, ItemsDetailOutputDto, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        Task<List<ItemsDetailOutputDto>> GetItemDetailsByItemCode(string itemCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemId">Id</param>
        /// <returns></returns>
       Task<List<ItemsDetailOutputDto>> GetAllItemsDetailTreeTable(string itemId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemId">Id</param>
        /// <returns></returns>
        Task<List<ItemsDetailFormOutputDto>> GetAllItemsDetailFormTreeTable(string itemId);
    }
}
