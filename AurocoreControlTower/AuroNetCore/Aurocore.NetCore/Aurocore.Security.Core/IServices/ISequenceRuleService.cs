using System;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// 定義序號編碼規則表服務介面
    /// </summary>
    public interface ISequenceRuleService:IService<SequenceRule,SequenceRuleOutputDto, string>
    {
    }
}
