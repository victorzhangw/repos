using System;
using System.Threading.Tasks;
using Aurocore.Commons.IServices;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILogService:IService<Log, LogOutputDto, string>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="operationType"></param>
        /// <param name="note">ϸ</param>
        /// <returns></returns>
         bool OnOperationLog(string tableName, string operationType, string note);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="module">ģ</param>
        /// <param name="operationType"></param>
        /// <param name="note">ϸ</param>
        /// <param name="currentUser">û</param>
        /// <returns></returns>
        bool OnOperationLog(string module, string operationType,  string note, AurocoreCurrentUser currentUser);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns>ָļ</returns>
        Task<PageResult<LogOutputDto>> FindWithPagerSearchAsync(SearchLogModel search);
    }
}
