using System;
using System.Collections.Generic;
using Aurocore.Commons.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRoleService:IService<Role, RoleOutputDto, string>
    {
        /// <summary>
        /// ݽɫȡɫ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        Role GetRole(string enCode);


        /// <summary>
        /// ûɫIDȡɫ
        /// </summary>
        /// <param name="ids">ɫIDַá,ָ</param>
        /// <returns></returns>
        string GetRoleEnCode(string ids);


        /// <summary>
        /// ûɫIDȡɫ
        /// </summary>
        /// <param name="ids">ɫIDַá,ָ</param>
        /// <returns></returns>
       string GetRoleNameStr(string ids);
    }
}
