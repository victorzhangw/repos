using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurocore.Commons.Core.Dtos;
using Aurocore.Commons.IServices;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOrganizeService:IService<Organize, OrganizeOutputDto, string>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<OrganizeOutputDto>> GetAllOrganizeTreeTable();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">֯Id</param>
        /// <returns></returns>
        Organize GetRootOrganize(string id);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids">Id</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        CommonResult DeleteBatchWhere(DeletesInputDto ids, IDbTransaction trans = null);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids">Id</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<CommonResult> DeleteBatchWhereAsync(DeletesInputDto ids, IDbTransaction trans = null);
    }
}
