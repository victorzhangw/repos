
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Options;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;
using Aurocore.Security.Services;

namespace Aurocore.Security.Application
{
    /// <summary>
    /// 檔案上傳
    /// </summary>
    public class UploadFileApp
    {
        private ILogger<UploadFileApp> _logger;
        private string _filePath;
        private string _dbFilePath;   //資料庫中的檔案路徑
        private string _dbThumbnail;   //資料庫中的縮圖路徑
        private string _belongApp;//所屬應用
        private string _belongAppId;//所屬應用ID
        IUploadFileService service = IoCContainer.Resolve<IUploadFileService>();
        /// <summary>
        /// 
        /// </summary>
        public UploadFileApp()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="setOptions"></param>
        /// <param name="logger"></param>
        public UploadFileApp(IOptions<AppSetting> setOptions, ILogger<UploadFileApp> logger)
        {
            _logger = logger;
            _filePath = setOptions.Value.LocalPath;
            if (string.IsNullOrEmpty(_filePath))
            {
                _filePath = AppContext.BaseDirectory;
            }
        }
        /// <summary>
        /// 根據應用Id和應用標識批量更新資料
        /// </summary>
        /// <param name="belongAppId">應用Id</param>
        /// <param name="oldBeLongAppId">更新前舊的應用Id</param>
        /// <param name="beLongApp">應用標識</param>
        /// <returns></returns>
        public bool UpdateByBeLongAppId(string belongAppId, string oldBeLongAppId, string beLongApp = null)
        {
            return service.UpdateByBeLongAppId(belongAppId, oldBeLongAppId, beLongApp);
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public long Insert(UploadFile info)
        {
            return service.Insert(info);
        }
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public UploadFile Get(string id)
        {
            return service.Get(id);
        }
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <returns>指定物件的集合</returns>
        public List<UploadFileOutputDto> FindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc)
        {
            return service.FindWithPager(condition,info,fieldToSort, desc,null).MapTo<UploadFileOutputDto>();
        }
        /// <summary>
        /// 批量上傳檔案
        /// </summary>
        /// <param name="files">檔案</param>
        /// <param name="belongApp">所屬應用，如文章article</param>
        /// <param name="belongAppId">所屬應用ID，如文章id</param>
        /// <returns></returns>
        public List<UploadFileResultOuputDto> Adds(IFormFileCollection files,string belongApp, string belongAppId)
        {
            List<UploadFileResultOuputDto> result = new List<UploadFileResultOuputDto>();
            foreach (var file in files)
            {
                if (file != null)
                {
                    result.Add(Add(file, belongApp, belongAppId));
                }
            }

            return result;
        }
        /// <summary>
        /// 單個上傳檔案
        /// </summary>
        /// <param name="file"></param>
        /// <param name="belongApp">所屬應用，如文章article</param>
        /// <param name="belongAppId">所屬應用ID，如文章id</param>
        /// <returns></returns>
        public UploadFileResultOuputDto Add(IFormFile file, string belongApp, string belongAppId)
        {
            _belongApp = belongApp;
            _belongAppId = belongAppId;
            if (file != null && file.Length > 0 && file.Length < 10485760)
            {
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var data = binaryReader.ReadBytes((int)file.Length);
                    UploadFile(fileName, data);

                    UploadFile filedb = new UploadFile
                    {
                        FilePath = _dbFilePath,
                        Thumbnail = _dbThumbnail,
                        FileName = fileName,
                        FileSize = file.Length.ToInt(),
                        FileType = Path.GetExtension(fileName),
                        Extension = Path.GetExtension(fileName),
                        BelongApp=_belongApp,
                        BelongAppId=_belongAppId
                    };
                    service.Insert(filedb);
                    return filedb.MapTo<UploadFileResultOuputDto>();
                }
            }
            else
            {
                Log4NetHelper.Error("檔案過大");
                throw new Exception("檔案過大");
            }
        }
        /// <summary>
        /// 實現檔案上傳到伺服器儲存，並產生縮圖
        /// </summary>
        /// <param name="fileName">檔名稱</param>
        /// <param name="fileBuffers">檔案位元組流</param>
        private void UploadFile(string fileName, byte[] fileBuffers)
        {
            string folder = DateTime.Now.ToString("yyyyMMdd");

            //判斷檔案是否為空
            if (string.IsNullOrEmpty(fileName))
            {
                Log4NetHelper.Error("檔名不能為空");
                throw new Exception("檔名不能為空");
            }

            //判斷檔案是否為空
            if (fileBuffers.Length < 1)
            {
                Log4NetHelper.Error("檔案不能為空");
                throw new Exception("檔案不能為空");
            }
            var _tempfilepath = "/upload/" + _belongApp + "/" + folder + "/";
            var uploadPath = _filePath + _tempfilepath;
            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
            }
            var ext = Path.GetExtension(fileName).ToLower();
            string newName = GuidUtils.CreateNo() + ext;

            using (var fs = new FileStream(uploadPath + newName, FileMode.Create))
            {
                fs.Write(fileBuffers, 0, fileBuffers.Length);
                fs.Close();
                //產生縮圖
                if (ext.Contains(".jpg") || ext.Contains(".jpeg") || ext.Contains(".png") || ext.Contains(".bmp") || ext.Contains(".gif"))
                {
                    string thumbnailName = GuidUtils.CreateNo() + ext;
                    ImgHelper.MakeThumbnail(uploadPath + newName, uploadPath + thumbnailName);
                    _dbThumbnail = folder + "/" + thumbnailName;
                }
                _dbFilePath = _tempfilepath + "/" + newName;
            }
        }

        /// <summary>
        /// 統計上傳內容數
        /// </summary>
        /// <returns></returns>
        public int GetCountTotal()
        {
            return service.GetCountByWhere("1=1");
        }
    }
}
