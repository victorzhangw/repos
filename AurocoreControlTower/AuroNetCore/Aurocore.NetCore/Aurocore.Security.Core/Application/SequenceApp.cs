using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Models;
using Aurocore.Security.IServices;

namespace Aurocore.Security.Application
{
    /// <summary>
    /// 業務編碼規則
    /// </summary>
    public class  SequenceApp
    {
        ISequenceService iService = IoCContainer.Resolve<ISequenceService>();
        static object locker = new object(); 
        /// <summary>
        /// 獲取新的業務單據編碼
        /// </summary>
        /// <param name="name">單據編碼規則名稱</param>
        public  string GetSequenceNext(string name)
        {
            lock (locker)
            {
                CommonResult commonResult = new CommonResult();
                commonResult = iService.GetSequenceNext(name);
                if (commonResult.Success)
                {
                    return commonResult.ResData.ToString();
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
