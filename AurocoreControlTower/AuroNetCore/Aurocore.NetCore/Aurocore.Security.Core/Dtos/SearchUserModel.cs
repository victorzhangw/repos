using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{

    /// <summary>
    /// 使用者搜索條件
    /// </summary>
    public class SearchUserModel : SearchInputDto<User>
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public string RoleId
        {
            get; set;
        }
        /// <summary>
        /// 部門Id
        /// </summary>
        public string DepartmentId
        {
            get; set;
        }
        /// <summary>
        /// 註冊或新增時間 開始
        /// </summary>
        public string CreatorTime1
        {
            get; set;
        }
        /// <summary>
        /// 註冊或新增時間 結束
        /// </summary>
        public string CreatorTime2
        {
            get; set;
        }

    }
}
