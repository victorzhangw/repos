using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Organize))]
    [Serializable]
    public class OrganizeInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CategoryId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ManagerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TelePhone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MobilePhone { get; set; }

        
        /// <summary>
        /// 
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }


    }
}
