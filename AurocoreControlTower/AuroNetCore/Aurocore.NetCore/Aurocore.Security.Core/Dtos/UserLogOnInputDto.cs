using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(UserLogOn))]
    [Serializable]
    public class UserLogOnInputDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserSecretkey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? AllowStartTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? AllowEndTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LockStartDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LockEndDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? FirstVisitTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? PreviousVisitTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastVisitTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ChangePasswordDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? MultiUserLogin { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? LogOnCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? UserOnLine { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AnswerQuestion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? CheckIPAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Theme { get; set; }


    }
}
