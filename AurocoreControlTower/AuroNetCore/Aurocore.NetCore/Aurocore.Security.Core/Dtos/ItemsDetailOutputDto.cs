using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
	/// <summary>
	/// 輸出物件模型
	/// </summary>
	[Serializable]
	public class ItemsDetailOutputDto
	{
		/// <summary>
		///  Id
		/// </summary>
		[MaxLength(50)]
		public string Id { get; set; }

		/// <summary>
		/// 字典 Id
		/// </summary>
		[MaxLength(50)]
		public string ItemId { get; set; }

		/// <summary>
		/// 父級 Id
		/// </summary>
		[MaxLength(50)]
		public string ParentId { get; set; }


		/// <summary>
		/// 父級名稱
		/// </summary>
		[MaxLength(50)]
		public string ParentName { get; set; }

		/// <summary>
		/// Item 編碼
		/// </summary>
		[MaxLength(50)]
		public string ItemCode { get; set; }

		/// <summary>
		/// Item 名稱
		/// </summary>
		[MaxLength(50)]
		public string ItemName { get; set; }
		/// <summary>
		/// 屬性1
		/// </summary>
		public virtual string ItemProperty1 { get; set; }
		/// <summary>
		/// 屬性說明
		/// </summary>
		public virtual string ItemProperty1Desc { get; set; }
		/// <summary>
		/// 屬性2
		/// </summary>
		public virtual string ItemProperty2 { get; set; }
		/// <summary>
		/// 屬性說明
		/// </summary>
		public virtual string ItemProperty2Desc { get; set; }
		/// <summary>
		/// 屬性3
		/// </summary>
		public virtual string ItemProperty3 { get; set; }
		/// 屬性說明
		/// </summary>
		public virtual string ItemProperty3Desc { get; set; }
		/// <summary>
		/// 
		/// </summary>
		///  [MaxLength(500)]
		///   public string SimpleSpelling { get; set; }

		/// <summary>
		/// 預設
		/// </summary>
		public bool? IsDefault { get; set; }

		/// <summary>
		/// 層級
		/// </summary>
		public int? Layers { get; set; }

		/// <summary>
		/// 排序碼
		/// </summary>
		public int? SortCode { get; set; }

		/// <summary>
		/// 刪除碼
		/// </summary>
	   /// public bool? DeleteMark { get; set; }

		/// <summary>
		/// 啟用碼
		/// </summary>
		public bool? EnabledMark { get; set; }

		/// <summary>
		/// 簡述/範本
		/// </summary>
		[MaxLength(500)]
		public string Description { get; set; }

		/// <summary>
		/// 
		/// </summary>
	   /// public DateTime? CreatorTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
	  ///  [MaxLength(50)]
	 ///   public string CreatorUserId { get; set; }

		/// <summary>
		/// 
		/// </summary>
	   /// public DateTime? LastModifyTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
	   /// [MaxLength(50)]
	   /// public string LastModifyUserId { get; set; }

		/// <summary>
		/// 
		/// </summary>
	  ///  public DateTime? DeleteTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
	  ///  [MaxLength(50)]
		///public string DeleteUserId { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public List<ItemsDetailOutputDto> Children { get; set; }
	}
}
