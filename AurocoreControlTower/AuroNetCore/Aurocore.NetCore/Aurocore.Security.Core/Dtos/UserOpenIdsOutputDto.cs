using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class UserOpenIdsOutputDto
    {
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(256)]
        public string OpenIdType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(128)]
        public string OpenId { get; set; }


    }
}
