using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(FilterIP))]
    [Serializable]
    public class FilterIPInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? FilterType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StartIP { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EndIP { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }


    }
}
