using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class UserLogOnOutputDto
    {
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string UserPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string UserSecretkey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? AllowStartTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? AllowEndTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LockStartDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LockEndDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? FirstVisitTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? PreviousVisitTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastVisitTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ChangePasswordDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? MultiUserLogin { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? LogOnCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? UserOnLine { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Question { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(500)]
        public string AnswerQuestion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? CheckIPAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Language { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Theme { get; set; }


    }
}
