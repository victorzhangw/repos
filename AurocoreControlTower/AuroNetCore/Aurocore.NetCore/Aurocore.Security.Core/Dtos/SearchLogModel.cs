using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 日誌搜索條件
    /// </summary>
    public class SearchLogModel : SearchInputDto<Log>
    {
        /// <summary>
        /// 新增開始時間 
        /// </summary>
        public string CreatorTime1
        {
            get; set;
        }
        /// <summary>
        /// 新增結束時間 
        /// </summary>
        public string CreatorTime2
        {
            get; set;
        }
    }
}
