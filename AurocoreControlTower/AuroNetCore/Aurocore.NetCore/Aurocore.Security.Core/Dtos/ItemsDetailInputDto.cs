using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
	/// <summary>
	/// 輸入物件模型
	/// </summary>
	[AutoMap(typeof(ItemsDetail))]
	[Serializable]
	public class ItemsDetailInputDto: IInputDto<string>
	{
		/// <summary>
		/// Id
		/// </summary>
		public string Id { get; set; }

		/// <summary>
		/// 字典Id
		/// </summary>
		public string ItemId { get; set; }

		/// <summary>
		/// 父級 Id
		/// </summary>
		public string ParentId { get; set; }

		/// <summary>
		/// Item 編碼
		/// </summary>
		public string ItemCode { get; set; }

		/// <summary>
		/// 名稱
		/// </summary>
		public string ItemName { get; set; }
		/// <summary>
		/// 屬性1
		/// </summary>
		public virtual string ItemProperty1 { get; set; }
		/// <summary>
		/// 屬性說明
		/// </summary>
		public virtual string ItemProperty1Desc { get; set; }
		/// <summary>
		/// 屬性2
		/// </summary>
		public virtual string ItemProperty2 { get; set; }
		/// <summary>
		/// 屬性說明
		/// </summary>
		public virtual string ItemProperty2Desc { get; set; }
		/// <summary>
		/// 屬性3
		/// </summary>
		public virtual string ItemProperty3 { get; set; }
		/// 屬性說明
		/// </summary>
		public virtual string ItemProperty3Desc { get; set; }

		/// <summary>
		/// 簡述
		/// </summary>
		public string SimpleSpelling { get; set; }

		/// <summary>
		/// 是否預設
		/// </summary>
		public bool? IsDefault { get; set; }

		/// <summary>
		/// 層級
		/// </summary>
		public int? Layers { get; set; }

		/// <summary>
		/// 排序碼
		/// </summary>
		public int? SortCode { get; set; }

		/// <summary>
		/// 啟用
		/// </summary>
		public bool EnabledMark { get; set; }

		/// <summary>
		/// 長文本/範本
		/// </summary>
		public string Description { get; set; }


	}
}
