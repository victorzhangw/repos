using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(DbBackup))]
    [Serializable]
    public class DbBackupInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BackupType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? BackupTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }


    }
}
