using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class UserOutputDto
    {

        #region Property Members

        /// <summary>
        /// 使用者主鍵
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// 帳戶
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string RealName { get; set; }

        /// <summary>
        /// 呢稱
        /// </summary>
        public virtual string NickName { get; set; }

        /// <summary>
        /// 頭像
        /// </summary>
        public virtual string HeadIcon { get; set; }

       
        /// <summary>
        /// 密碼
        /// </summary>
        public virtual string UserPassword { get; set; }

        /// <summary>
        /// 郵箱
        /// </summary>
        public virtual string Email { get; set; }

       

      

        /// <summary>
        /// 主管主鍵
        /// </summary>
        public virtual string ManagerId { get; set; }

        /// <summary>
        /// 安全級別
        /// </summary>
        public virtual int? SecurityLevel { get; set; }

        /// <summary>
        /// 個性簽名
        /// </summary>
        public virtual string Signature { get; set; }

        /// <summary>
        /// 組織主鍵
        /// </summary>
        public virtual string OrganizeId { get; set; }

        /// <summary>
        /// 組織名稱
        /// </summary>
        public virtual string OrganizeName { get; set; }

        /// <summary>
        /// 部門主鍵
        /// </summary>
        public virtual string DepartmentId { get; set; }

        /// <summary>
        /// 部門名稱
        /// </summary>
        public virtual string DepartmentName { get; set; }

        /// <summary>
        /// 角色主鍵
        /// </summary>
        public virtual string RoleId { get; set; }

        /// <summary>
        /// 角色名稱
        /// </summary>
        public virtual string RoleName { get; set; }

        /// <summary>
        /// 職位主鍵
        /// </summary>
        public virtual string DutyId { get; set; }

        /// <summary>
        /// 職位名稱
        /// </summary>
        public virtual string DutyName { get; set; }

        /// <summary>
        /// 是否管理員
        /// </summary>
        public virtual bool? IsAdministrator { get; set; }

        /// <summary>
        /// 排序碼
        /// </summary>
        public virtual int? SortCode { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// 刪除標誌
        /// </summary>
        public virtual bool? DeleteMark { get; set; }

        /// <summary>
        /// 有效標誌
        /// </summary>
        public virtual bool EnabledMark { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 建立使用者主鍵
        /// </summary>
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 最後修改時間
        /// </summary>
        public virtual DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 最後修改使用者
        /// </summary>
        public virtual string LastModifyUserId { get; set; }

        /// <summary>
        /// 刪除時間
        /// </summary>
        public virtual DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        public virtual string DeleteUserId { get; set; }
        #endregion

    }
}
