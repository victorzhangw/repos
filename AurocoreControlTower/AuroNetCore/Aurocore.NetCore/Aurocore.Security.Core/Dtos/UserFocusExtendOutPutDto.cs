using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Mapping;
using Aurocore.Security.Models;
using Aurocore.Commons.Dtos;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    [AutoMap(typeof(User))]
    [Serializable]
    public class UserFocusExtendOutPutDto : IOutputDto
    {

        #region Property Members

        /// <summary>
        /// 使用者主鍵
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// 關注的使用者ID
        /// </summary>
        public virtual string FocusUserId { get; set; }

        /// <summary>
        /// 關注人
        /// </summary>
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 關注時間
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 關注的使用者別名
        /// </summary>
        public virtual string FUserNickName { get; set; }

        /// <summary>
        /// 關注的使用者頭像
        /// </summary>
        public virtual string FUserHeadIcon { get; set; }

        /// <summary>
        /// 關注的使用者手機
        /// </summary>
        public virtual string FUserMobilePhone { get; set; }

        /// <summary>
        /// 關注的使用者資料開放程式
        /// </summary>
        public virtual string FUserOpenType { get; set; }

        /// <summary>
        /// 記錄數
        /// </summary>
        public virtual int RecordCount { get; set; }


        /// <summary>
        /// 關注時間
        /// </summary>
        public virtual string ShowAddTime { get; set; }
        #endregion
    }
}
