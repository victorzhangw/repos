using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Menu))]
    [Serializable]
    public class MenuInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SystemTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 設定或獲取路由
        /// </summary>
        public virtual string UrlAddress { get; set; }

        /// <summary>
        /// 設定或獲取目標打開方式
        /// </summary>
        public virtual string Target { get; set; }

        /// <summary>
        /// 設定或獲取菜單型別（C目錄 M菜單 F按鈕）
        /// </summary>
        public virtual string MenuType { get; set; }
        /// <summary>
        /// 設定或獲取元件路徑
        /// </summary>
        public virtual string Component { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? IsExpand { get; set; }
        /// <summary>
        /// 是否顯示
        /// </summary>
        public bool? IsShow { get; set; }
        /// <summary>
        /// 設定或獲取是否快取
        /// </summary>
        public bool? IsCache { get; set; }
        /// <summary>
        /// 是否外鏈
        /// </summary>
        public bool? IsFrame { get; set; }
        /// <summary>
        /// 批量新增
        /// </summary>
        public bool IsBatch { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? IsPublic { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }



        /// <summary>
        /// 
        /// </summary>
        public string SystemTypeName { get; set; }
    }
}
