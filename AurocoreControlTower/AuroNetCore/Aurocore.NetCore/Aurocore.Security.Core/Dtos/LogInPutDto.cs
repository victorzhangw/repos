using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Log))]
    [Serializable]
    public class LogInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IPAddressName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModuleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? Result { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

    }
}
