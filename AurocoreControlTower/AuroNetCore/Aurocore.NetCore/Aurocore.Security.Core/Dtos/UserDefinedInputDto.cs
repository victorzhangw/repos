using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(UserDefined))]
    [Serializable]
    public class UserDefinedInputDto
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取使用者Id
        /// </summary>
      
        public string UserId { get; set; }
        /// <summary>
        /// 資料字典索引 Id
        /// </summary>
        public virtual string DictId { get; set; }
        /// <summary>
        /// 資料字典設定值
        /// 設定字串之間以“;”分開
        /// </summary>
        public virtual string CustomSetting { get; set; }
        /// <summary>
        /// 描述設定值
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool EnabledMark { get; set; }


    }
}
