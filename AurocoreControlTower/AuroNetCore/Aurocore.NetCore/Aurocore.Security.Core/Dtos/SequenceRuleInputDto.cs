using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 序號編碼規則表輸入物件模型
    /// </summary>
    [AutoMap(typeof(SequenceRule))]
    [Serializable]
    public class SequenceRuleInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取編碼規則名稱
        /// </summary>
        public string SequenceName { get; set; }

        /// <summary>
        /// 設定或獲取規則排序
        /// </summary>
        public int RuleOrder { get; set; }

        /// <summary>
        /// 設定或獲取規則類別，timestamp、const、bumber
        /// </summary>
        public string RuleType { get; set; }

        /// <summary>
        /// 設定或獲取規則參數，如YYMMDD
        /// </summary>
        public string RuleValue { get; set; }

        /// <summary>
        /// 設定或獲取補齊方向，left或right
        /// </summary>
        public string PaddingSide { get; set; }

        /// <summary>
        /// 設定或獲取補齊寬度
        /// </summary>
        public int PaddingWidth { get; set; }

        /// <summary>
        /// 設定或獲取填充字元
        /// </summary>
        public string PaddingChar { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否可用
        /// </summary>
        public bool EnabledMark { get; set; }


    }
}
