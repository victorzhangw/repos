using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class MenuOutputDto
    {
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string SystemTypeId { get; set; }
        public string SystemTypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string ParentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string EnCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string FullName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Icon { get; set; }

        /// <summary>
        /// 設定或獲取路由
        /// </summary>
        public virtual string UrlAddress { get; set; }

        /// <summary>
        /// 設定或獲取目標打開方式
        /// </summary>
        public virtual string Target { get; set; }

        /// <summary>
        /// 設定或獲取菜單型別（C目錄 M菜單 F按鈕）
        /// </summary>
        public virtual string MenuType { get; set; }
        /// <summary>
        /// 設定或獲取元件路徑
        /// </summary>
        public virtual string Component { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? IsExpand { get; set; }

        /// <summary>
        /// 是否顯示
        /// </summary>
        public bool IsShow { get; set; }
        /// <summary>
        /// 是否外鏈
        /// </summary>
        public bool IsFrame { get; set; }
        /// <summary>
        /// 設定或獲取是否快取
        /// </summary>
        public bool IsCache { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? IsPublic { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

        /// <summary>
        /// 子菜單集合
        /// </summary>
        public List<MenuOutputDto> SubMenu { get; set; }


    }
}
