using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Security.Models;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(APP))]
    [Serializable]
    public class APPInputDto: IInputDto<string>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string EncodingAESKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOpenAEKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }


    }
}
