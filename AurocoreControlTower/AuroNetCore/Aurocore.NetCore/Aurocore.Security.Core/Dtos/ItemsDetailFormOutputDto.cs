using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
	/// <summary>
	/// 輸出物件模型
	/// </summary>
	[Serializable]
	public class ItemsDetailFormOutputDto
	{
		/// <summary>
		///  Id
		/// </summary>
		/// 
		
		[MaxLength(50)]
		public string Id { get; set; }

		/*
		/// <summary>
		/// 父級 Id
		/// </summary>
		[MaxLength(50)]
		public string ParentId { get; set; }
		*/
		
		
		/// <summary>
		/// 名稱
		/// </summary>
		[MaxLength(50)]
		public string Alias { get; set; }

		/// <summary>
		/// 顯示名稱
		/// </summary>
		[MaxLength(50)]
		public string Name { get; set; }
		/// <summary>
		/// 屬性1
		/// </summary>
		public virtual string InputType { get; set; }
		/// <summary>
		/// 表單選項，以逗號“,” 分割
		/// </summary>
		public virtual string Value { get; set; }
		
		public bool? Required { get; set; }
		/*
		/// <summary>
		/// 層級
		/// </summary>
		public int? Layers { get; set; }
		*/
		/// <summary>
		/// 排序碼
		/// </summary>
		public int? SortCode { get; set; }
		
		/// <summary>
		/// 刪除碼
		/// </summary>
	   /// public bool? DeleteMark { get; set; }

		

		public List<ItemsDetailFormOutputDto> Children { get; set; }
	}
}
