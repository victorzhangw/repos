using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class FilterIPOutputDto
    {
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? FilterType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string StartIP { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string EndIP { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MaxLength(500)]
        public string DeleteUserId { get; set; }


    }
}
