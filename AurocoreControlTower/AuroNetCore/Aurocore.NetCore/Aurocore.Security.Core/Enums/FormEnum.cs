﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.Security.Enums
{
    /// <summary>
    /// 自定義表單類型
    /// </summary>
    public enum CustomFieldType
    {
        /// <summary>
        /// 下拉
        /// </summary>
        Select = 1,
        /// <summary>
        /// 單選
        /// </summary>
        Radio = 2,
        /// <summary>
        /// 複選框
        /// </summary>
        Checkbox = 3,
        /// <summary>
        /// 單行
        /// </summary>
        Input = 4,
        /// <summary>
        /// 開關
        /// </summary>
        Switch = 5,
        /// <summary>
        /// 密碼
        /// </summary>
        Password = 6,
        /// <summary>
        /// 日期
        /// </summary>
        Date = 7,
        /// <summary>
        /// 時間
        /// </summary>
        Time = 8,
        /// <summary>
        /// 日期時間
        /// </summary>
        DateTime = 9,
        /// <summary>
        /// 多行
        /// </summary>
        Textarea = 10,
        /// <summary>
        /// 按鈕群組
        /// </summary>
        ButtonGroup = 11,
        /// <summary>
        /// 圖片
        /// </summary>
        Image = 15,
        /// <summary>
        /// 多圖
        /// </summary>
        Images = 18,
        /// <summary>
        /// 文件
        /// </summary>
        File = 20,
        /// <summary>
        /// 編輯器
        /// </summary>
        Editor = 25,
        /// <summary>
        /// 隱藏輸入框
        /// </summary>
        Hidden = 30
    }
}
