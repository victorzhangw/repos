using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.Repositories
{
    public class ItemsDetailRepository : BaseRepository<ItemsDetail, string>, IItemsDetailRepository
    {
        public ItemsDetailRepository()
        {
        }

        public ItemsDetailRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }

    }
}