using Dapper;
using System.Collections.Generic;
using System.Linq;
using Aurocore.Commons.DbContextCore;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.Repositories
{
    /// <summary>
    /// òִ
    /// </summary>
    public class APPRepository : BaseRepository<APP,string>, IAPPRepository
    {
        public APPRepository()
        {
        }
        public APPRepository(IDbContextCore context) : base(context)
        {
        }
        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">App ID</param>
        /// <param name="secret">App Secret</param>
        /// <returns></returns>
        public APP GetAPP(string appid, string secret)
        {

            string sql = @"SELECT * FROM Sys_APP t WHERE t.AppId = @AppId and AppSecret=@AppSecret and EnabledMark=1";
            return DapperConn.QueryFirstOrDefault<APP>(sql, new { AppId = appid, AppSecret = secret });
        }

        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ID</param>
        /// <returns></returns>
        public APP GetAPP(string appid)
        {
            string sql = @"SELECT * FROM Sys_APP t WHERE t.AppId = @AppId and EnabledMark=1";
            return DapperConn.QueryFirstOrDefault<APP>(sql, new { AppId = appid });

        }
        public IList<AppOutputDto> SelectApp()
        {
            const string query = @"select a.*,u.id as Id,u.NickName,u.Account,u.HeadIcon from Sys_APP a,Sys_User u where a.CreatorUserId=u.Id ";
            return DapperConn.Query<AppOutputDto, User, AppOutputDto>(query, (app, user) => { app.UserInfo = user; return app; }, null, splitOn: "Id").ToList<AppOutputDto>();
        }
    }
}