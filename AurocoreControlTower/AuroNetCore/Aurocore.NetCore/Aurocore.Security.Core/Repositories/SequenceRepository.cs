using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.Repositories
{
    /// <summary>
    /// 單據編碼倉儲介面的實現
    /// </summary>
    public class SequenceRepository : BaseRepository<Sequence, string>, ISequenceRepository
    {
		public SequenceRepository()
        {
        }

        public SequenceRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}