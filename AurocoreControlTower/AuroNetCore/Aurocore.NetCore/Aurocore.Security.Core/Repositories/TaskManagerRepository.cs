using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Log;
using Aurocore.Commons.Repositories;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.Repositories
{
    /// <summary>
    /// 定時任務倉儲介面的實現
    /// </summary>
    public class TaskManagerRepository : BaseRepository<TaskManager, string>, ITaskManagerRepository
    {
		public TaskManagerRepository()
        {
        }
        /// <summary>
        /// EF 資料操作注入
        /// </summary>
        /// <param name="context"></param>
        public TaskManagerRepository(IDbContextCore context) : base(context)
        {
        }



        /// <summary>
        /// 非同步按條件設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// 禁用時會將狀態更新為暫停
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public override async Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "EnabledMark=1 ";
            }
            else
            {
                sql += "EnabledMark=0 ";
                sql += ",Status=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime where " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @LastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }


        /// <summary>
        /// 非同步批量軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public override async Task<bool> DeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "DeleteMark=0 ";
            }
            else
            {
                sql += "DeleteMark=1 ";
                sql += ",Status=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }

    }
}