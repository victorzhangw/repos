using Dapper;
using System;
using System.Data;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Options;
using Aurocore.Commons.Repositories;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;
using System.Collections.Generic;
using System.Linq;

namespace Aurocore.Security.Repositories
{
    public class UserDefinedRepository : BaseRepository<UserDefined, string>,IUserDefinedRepository
    {
        public UserDefinedRepository()
        {
        }

        public UserDefinedRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }

        public List<UserDefined> GetAllByUserId(string userId)
        {
            string sql = string.Format("SELECT * FROM Sys_UserDefined t WHERE  1=1 and t.UserId = '{0}'", userId);
            List<UserDefined> result = DapperConn.Query<UserDefined>(sql).ToList();
            return result;

            
        }
        public List<UserDefined> GetCustoms(string userId,string dictId)
        {
            string sql = string.Format("SELECT * FROM Sys_UserDefined t WHERE 1=1 and t.UserId = '{0}' ", userId);
            List<UserDefined> result = DapperConn.Query<UserDefined>(sql).ToList();
            return result;


        }
    }
}