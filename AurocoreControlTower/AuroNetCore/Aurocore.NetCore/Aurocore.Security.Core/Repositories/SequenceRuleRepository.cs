using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Security.IRepositories;
using Aurocore.Security.Models;

namespace Aurocore.Security.Repositories
{
    /// <summary>
    /// 序號編碼規則表倉儲介面的實現
    /// </summary>
    public class SequenceRuleRepository : BaseRepository<SequenceRule, string>, ISequenceRuleRepository
    {
		public SequenceRuleRepository()
        {
        }

        public SequenceRuleRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}