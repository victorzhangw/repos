using System;
using Aurocore.Commons.Services;
using Aurocore.Security.IServices;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;
using System.Threading.Tasks;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Dtos;
using System.Collections.Generic;
using Aurocore.Commons.Mapping;

namespace Aurocore.Security.Services
{
    /// <summary>
    /// 序號編碼規則表服務介面實現
    /// </summary>
    public class SequenceRuleService: BaseService<SequenceRule,SequenceRuleOutputDto, string>, ISequenceRuleService
    {
		private readonly ISequenceRuleRepository _repository;
        private readonly ILogService _logService;
        public SequenceRuleService(ISequenceRuleRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }


        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定物件的集合</returns>
        public override async Task<PageResult<SequenceRuleOutputDto>> FindWithPagerAsync(SearchInputDto<SequenceRule> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege(false);
            if (!string.IsNullOrEmpty(search.Keywords))
            {
                where += string.Format(" and SequenceName like '%{0}%' ", search.Keywords);
            };
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<SequenceRule> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
            PageResult<SequenceRuleOutputDto> pageResult = new PageResult<SequenceRuleOutputDto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<SequenceRuleOutputDto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }
    }
}