using System;
using System.Collections.Generic;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    public class RoleDataService: BaseService<RoleData, RoleDataOutputDto, string>, IRoleDataService
    {
		private readonly IRoleDataRepository _repository;
        private readonly ILogService _logService;
        public RoleDataService(IRoleDataRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }
        /// <summary>
        /// ݽɫȨʲ
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        public List<string> GetListDeptByRole(string roleIds)
        {
            return _repository.GetListDeptByRole(roleIds);
        }
    }
}