using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.Commons.Extend;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ItemsDetailService: BaseService<ItemsDetail, ItemsDetailOutputDto, string>, IItemsDetailService
    {
        private readonly IItemsDetailRepository _repository;
        private readonly ILogService _logService;
        private readonly IItemsService itemsService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logService"></param>
        public ItemsDetailService(IItemsDetailRepository repository, ILogService logService, IItemsService _itemsService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
            itemsService = _itemsService;
        }

        /// <summary>
        /// ֵȡ÷
        /// </summary>
        /// <param name="itemCode"></param>
        /// <returns></returns>
        public async Task<List<ItemsDetailOutputDto>> GetItemDetailsByItemCode(string itemCode)
        {
            string where = string.Empty;
            if (!string.IsNullOrEmpty(itemCode))
            {
                where = string.Format("EnCode='{0}'", itemCode);
            }
            Items items = await itemsService.GetByEnCodAsynce(itemCode);
            List<ItemsDetailOutputDto> resultList = new List<ItemsDetailOutputDto>();
            if (items != null)
            {
                where = string.Format("ItemId='{0}'", items.Id);
                IEnumerable<ItemsDetail> list = _repository.GetAllByIsNotDeleteAndEnabledMark(where);
                resultList = list.OrderBy(t => t.SortCode).MapTo<ItemsDetailOutputDto>();
            }
            return resultList;
        }


        /// <summary>
        /// 取得資料字典明細
        /// </summary>
        /// <param name="itemId">Id</param>
        /// <returns></returns>
        public async Task<List<ItemsDetailOutputDto>> GetAllItemsDetailTreeTable(string itemId)
        {
            string where = "1=1";
            List<ItemsDetailOutputDto> reslist = new List<ItemsDetailOutputDto>();
            where += "and EnabledMark='true' and ItemId='" + itemId + "'";
            IEnumerable<ItemsDetail> elist = await _repository.GetListWhereAsync(where);
            List<ItemsDetail> list = elist.OrderBy(t => t.SortCode).ToList();
            List<ItemsDetail> oneMenuList = list.FindAll(t => t.ParentId == "");
            foreach (ItemsDetail item in oneMenuList)
            {
                ItemsDetailOutputDto menuTreeTableOutputDto = new ItemsDetailOutputDto();
                menuTreeTableOutputDto = item.MapTo<ItemsDetailOutputDto>();
                menuTreeTableOutputDto.Children = GetSubMenus(list, item.Id).ToList<ItemsDetailOutputDto>();
                reslist.Add(menuTreeTableOutputDto);
            }
            return reslist;
        }

        public async Task<List<ItemsDetailFormOutputDto>> GetAllItemsDetailFormTreeTable(string itemId)
        {
            string where = "1=1";
            List<ItemsDetailFormOutputDto> reslist = new List<ItemsDetailFormOutputDto>();
            where += "and EnabledMark='true' and ItemId='" + itemId + "'";
            IEnumerable<ItemsDetail> elist = await _repository.GetListWhereAsync(where);
            List<ItemsDetail> list = elist.OrderBy(t => t.SortCode).ToList();
            List<ItemsDetail> oneFormList = list.FindAll(t => t.ParentId == "");
            foreach (ItemsDetail item in oneFormList)
            {
                ItemsDetailFormOutputDto formTreeTableOutputDto = new ItemsDetailFormOutputDto();
                formTreeTableOutputDto = item.MapTo<ItemsDetailFormOutputDto>();
                formTreeTableOutputDto.Children = GetSubForms(list, item.Id).ToList<ItemsDetailFormOutputDto>();
                reslist.Add(formTreeTableOutputDto);
            }
            return reslist;
        }
        /// <summary>
        /// 取得子選單
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId">Id</param>
        /// <returns></returns>
        private List<ItemsDetailOutputDto> GetSubMenus(List<ItemsDetail> data, string parentId)
        {
            List<ItemsDetailOutputDto> list = new List<ItemsDetailOutputDto>();
            ItemsDetailOutputDto menuTreeTableOutputDto = new ItemsDetailOutputDto();
            var ChilList = data.FindAll(t => t.ParentId == parentId);
            foreach (ItemsDetail entity in ChilList)
            {
                menuTreeTableOutputDto = entity.MapTo<ItemsDetailOutputDto>();
                menuTreeTableOutputDto.Children = GetSubMenus(data, entity.Id).OrderBy(t => t.SortCode).MapTo<ItemsDetailOutputDto>();
                list.Add(menuTreeTableOutputDto);
            }
            return list;
        }
        /// <summary>
        /// 取得子表單
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId">Id</param>
        /// <returns></returns>
        private List<ItemsDetailFormOutputDto> GetSubForms(List<ItemsDetail> data, string parentId)
        {
            List<ItemsDetailFormOutputDto> list = new List<ItemsDetailFormOutputDto>();
            ItemsDetailFormOutputDto formTreeTableOutputDto = new ItemsDetailFormOutputDto();
            var ChilList = data.FindAll(t => t.ParentId == parentId);
            foreach (ItemsDetail entity in ChilList)
            {
                formTreeTableOutputDto = entity.MapTo<ItemsDetailFormOutputDto>();
                formTreeTableOutputDto.Children = GetSubForms(data, entity.Id).OrderBy(t => t.SortCode).MapTo<ItemsDetailFormOutputDto>();
                list.Add(formTreeTableOutputDto);
            }
            return list;
        }
    }
}