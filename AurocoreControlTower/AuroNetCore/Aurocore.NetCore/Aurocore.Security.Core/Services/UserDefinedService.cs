using System;
using System.Collections.Generic;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    public class UserDefinedService: BaseService<UserDefined, UserDefinedOutputDto, string>, IUserDefinedService
    {
        private readonly  IUserDefinedRepository _userDefinedRepository;
        
        public UserDefinedService(IUserDefinedRepository repository) : base(repository)
        {
            _userDefinedRepository = repository;
           
        }

        /// <summary>
        /// 取得自定義
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
       public List<UserDefined> GetAllByUserId(string userId)
        {
           return _userDefinedRepository.GetAllByUserId(userId);
        }

        List<UserDefined> IUserDefinedService.GetCustoms(string userId, string dictId)
        {
            List<UserDefined> _rst =new List<UserDefined>();
            _rst = _userDefinedRepository.GetCustoms(userId, dictId);
            if (_rst == null)
            {
                _rst.Add(new UserDefined { CustomSetting = "", UserId = userId, DictId = dictId });
                return _rst;
            }

            return _rst;

        }
    }
}