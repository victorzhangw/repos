using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    public class ItemsService: BaseService<Items, ItemsOutputDto, string>, IItemsService
    {
        private readonly IItemsRepository _repository;
        private readonly ILogService _logService;
        public ItemsService(IItemsRepository repository, ILogService logService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
        }


        /// <summary>
        /// ȡܲ˵Vue б
        /// </summary>
        /// <returns></returns>
        public async Task<List<ItemsOutputDto>> GetAllItemsTreeTable()
        {
            string where = "1=1";
            List<ItemsOutputDto> reslist = new List<ItemsOutputDto>();
            IEnumerable<Items> elist = _repository.GetListWhere("1=1");
            List<Items> list = elist.OrderBy(t => t.SortCode).ToList();
            List<Items> oneMenuList = list.FindAll(t => t.ParentId == "");
            foreach (Items item in oneMenuList)
            {
                ItemsOutputDto menuTreeTableOutputDto = new ItemsOutputDto();
                menuTreeTableOutputDto = item.MapTo<ItemsOutputDto>();
                menuTreeTableOutputDto.Children = GetSubMenus(list, item.Id).ToList<ItemsOutputDto>();
                reslist.Add(menuTreeTableOutputDto);
            }
            return reslist;
        }

        /// <summary>
        /// ݱѯֵ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        public async Task<Items> GetByEnCodAsynce(string enCode)
        {
            return await _repository.GetByEnCodAsynce(enCode);
        }


        /// <summary>
        /// ȡӲ˵ݹ
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId">Id</param>
        /// <returns></returns>
        private List<ItemsOutputDto> GetSubMenus(List<Items> data, string parentId)
        {
            List<ItemsOutputDto> list = new List<ItemsOutputDto>();
            ItemsOutputDto menuTreeTableOutputDto = new ItemsOutputDto();
            var ChilList = data.FindAll(t => t.ParentId == parentId);
            foreach (Items entity in ChilList)
            {
                menuTreeTableOutputDto = entity.MapTo<ItemsOutputDto>();
                menuTreeTableOutputDto.Children = GetSubMenus(data, entity.Id).OrderBy(t => t.SortCode).MapTo<ItemsOutputDto>();
                list.Add(menuTreeTableOutputDto);
            }
            return list;
        }
    }
}