using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Json;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    /// <summary>
    /// Ϣ
    /// </summary>
    public class AreaService: BaseService<Area, AreaOutputDto, string>, IAreaService
    {
        private readonly IAreaRepository _repository;
        private readonly ILogService _logService;
        public AreaService(IAreaRepository repository, ILogService logService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
        }


        #region uniappѡ
        /// <summary>
        /// ȡпõĵuniappѡ
        /// </summary>
        /// <returns></returns>
        public List<AreaPickerOutputDto> GetAllByEnable()
        {
            List<AreaPickerOutputDto> list = new List<AreaPickerOutputDto>();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            list = JsonConvert.DeserializeObject<List<AreaPickerOutputDto>>(AurocoreCacheHelper.Get("Area_Enable_Uniapp").ToJson());
            if (list == null || list.Count <= 0)
            {
                List<Area> listFunction = _repository.GetAllByIsNotDeleteAndEnabledMark("Layers in (0,1,2)").OrderBy(t => t.SortCode).ToList();
                list = UniappViewJson(listFunction, "");
                AurocoreCacheHelper.Add("Area_Enable_Uniapp", list);
            }
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<AreaPickerOutputDto> GetProvinceToAreaByEnable()
        {
            List<AreaPickerOutputDto> list = new List<AreaPickerOutputDto>();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            list = JsonConvert.DeserializeObject<List<AreaPickerOutputDto>>(AurocoreCacheHelper.Get("Area_ProvinceToArea_Enable_Uniapp").ToJson());
            if (list == null || list.Count <= 0)
            {
                List<Area> listFunctionTemp = _repository.GetAllByIsNotDeleteAndEnabledMark("Layers in (1,2,3)").OrderBy(t => t.Id).ToList();
                List<Area> listFunction = new List<Area>();
                foreach (Area item in listFunctionTemp)
                {
                    if (item.Layers == 1) { item.ParentId = ""; }
                    listFunction.Add(item);
                }

                list = UniappViewJson(listFunction, "");
                AurocoreCacheHelper.Add("Area_ProvinceToArea_Enable_Uniapp", list);
            }
            return list;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<AreaPickerOutputDto> UniappViewJson(List<Area> data, string parentId)
        {
            List<AreaPickerOutputDto> list = new List<AreaPickerOutputDto>();
            var ChildNodeList = data.FindAll(t => t.ParentId == parentId).ToList();
            foreach (Area entity in ChildNodeList)
            {
                AreaPickerOutputDto treeViewModel = new AreaPickerOutputDto();
                treeViewModel.value = entity.Id;
                treeViewModel.label = entity.FullName;
                treeViewModel.children = ChildrenUniappViewList(data, entity.Id);
                list.Add(treeViewModel);
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public List<AreaPickerOutputDto> ChildrenUniappViewList(List<Area> data, string parentId)
        {
            List<AreaPickerOutputDto> listChildren = new List<AreaPickerOutputDto>();
            var ChildNodeList = data.FindAll(t => t.ParentId == parentId).ToList();
            foreach (Area entity in ChildNodeList)
            {
                AreaPickerOutputDto treeViewModel = new AreaPickerOutputDto();
                treeViewModel.value = entity.Id;
                treeViewModel.label = entity.FullName;
                treeViewModel.children = ChildrenUniappViewList(data, entity.Id);
                listChildren.Add(treeViewModel);
            }
            return listChildren;
        }
        #endregion
    }
}