using System;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    public class UserLogOnService: BaseService<UserLogOn, UserLogOnOutputDto, string>, IUserLogOnService
    {
        private readonly IUserLogOnRepository _userLogOnRepository;
        private readonly ILogService _logService;
        public UserLogOnService(IUserLogOnRepository repository, ILogService logService) : base(repository)
        {
            _userLogOnRepository = repository;
            _logService = logService;
        }

        /// <summary>
        /// ݻԱIDȡû¼Ϣʵ
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
       public UserLogOn GetByUserId(string userId)
        {
           return _userLogOnRepository.GetByUserId(userId);
        }

    }
}