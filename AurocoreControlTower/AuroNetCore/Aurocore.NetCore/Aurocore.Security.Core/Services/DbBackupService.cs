using System;
using Aurocore.Commons.Services;
using Aurocore.Security.Dtos;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Security.Services
{
    public class DbBackupService: BaseService<DbBackup, DbBackupOutputDto, string>, IDbBackupService
    {
        private readonly IDbBackupRepository _repository;
        private readonly ILogService _logService;
        public DbBackupService(IDbBackupRepository repository, ILogService logService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
        }
    }
}