
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Aurocore.Commons.Models;

namespace Aurocore.Security.Models
{
    /// <summary>
    /// 使用者表，資料實體物件
    /// </summary>
    [Table("Sys_UserDefined")]
    [Serializable]
    public class UserDefined : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    { 
        /// <summary>
        /// 預設建構函式（需要初始化屬性的在此處理）
        /// </summary>
	    public UserDefined()
        {
        }

        #region Property Members
        /// <summary>
        /// 帳戶
        /// </summary>
        public virtual string UserId { get; set; }

        /// <summary>
        /// 資料字典索引 Id
        /// </summary>
        public  string DictId { get; set; }
        /// <summary>
        /// 資料字典設定值
        /// 設定字串之間以“;”分開
        /// </summary>
        public virtual string CustomSetting { get; set; }
        /// <summary>
        /// 描述設定值
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// 刪除標誌
        /// </summary>
        public virtual bool? DeleteMark { get; set; }

        /// <summary>
        /// 有效標誌
        /// </summary>
        public virtual bool EnabledMark { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 建立使用者主鍵
        /// </summary>
        [MaxLength(50)]
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 公司id
        /// </summary>
        [MaxLength(50)]
        public virtual string CompanyId { get; set; }

        /// <summary>
        /// 部門id
        /// </summary>
        [MaxLength(50)]
        public virtual string DeptId { get; set; }

        /// <summary>
        /// 最後修改時間
        /// </summary>
        public virtual DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 最後修改使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string LastModifyUserId { get; set; }

        /// <summary>
        /// 刪除時間
        /// </summary>
        public virtual DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string DeleteUserId { get; set; }
        #endregion

    }
}