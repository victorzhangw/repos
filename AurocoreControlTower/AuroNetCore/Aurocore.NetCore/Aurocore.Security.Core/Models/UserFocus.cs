
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aurocore.Security.Models
{
    /// <summary>
    /// 使用者表，資料實體物件
    /// </summary>
    [Table("Sys_UserFocus")]
    [Serializable]
    public class UserFocus : BaseEntity<string>,ICreationAudited
    { 
        /// <summary>
        /// 預設建構函式（需要初始化屬性的在此處理）
        /// </summary>
	    public UserFocus()
        {
            this.Id = Guid.NewGuid().ToString();
        }

        #region Property Members

        /// <summary>
        /// 關注的使用者ID
        /// </summary>
        public virtual string FocusUserId { get; set; }

        /// <summary>
        /// 建立使用者ID
        /// </summary>
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 關注時間
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }
        #endregion

    }
}