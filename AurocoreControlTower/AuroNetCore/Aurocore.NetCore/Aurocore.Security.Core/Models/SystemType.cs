
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Models;

namespace Aurocore.Security.Models
{
    /// <summary>
    /// 子系統
    /// </summary>
    [Table("Sys_SystemType")]
    [Serializable]
    public class SystemType : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 預設建構函式（需要初始化屬性的在此處理）
        /// </summary>
        public SystemType()
        {
            this.Id = GuidUtils.CreateNo();

        }

        #region Property Members
        /// <summary>
        /// 系統名稱
        /// </summary>
        public virtual string FullName { get; set; }

        /// <summary>
        /// 編碼
        /// </summary>
        public virtual string EnCode { get; set; }

        /// <summary>
        /// 鏈接
        /// </summary>
        public virtual string Url { get; set; }


        /// <summary>
        /// 允許編輯
        /// </summary>
        public virtual bool? AllowEdit { get; set; }

        /// <summary>
        /// 允許刪除
        /// </summary>
        public virtual bool? AllowDelete { get; set; }

        /// <summary>
        /// 排序碼
        /// </summary>
        public virtual int? SortCode { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// 刪除標誌
        /// </summary>
        public virtual bool? DeleteMark { get; set; }

        /// <summary>
        /// 有效標誌
        /// </summary>
        public virtual bool EnabledMark { get; set; }

        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 建立使用者主鍵
        /// </summary>
        [MaxLength(50)]
        public virtual string CreatorUserId { get; set; }

        /// <summary>
        /// 最後修改時間
        /// </summary>
        public virtual DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 最後修改使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string LastModifyUserId { get; set; }

        /// <summary>
        /// 刪除時間
        /// </summary>
        public virtual DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 刪除使用者
        /// </summary>
        [MaxLength(50)]
        public virtual string DeleteUserId { get; set; }
        #endregion

    }
}
