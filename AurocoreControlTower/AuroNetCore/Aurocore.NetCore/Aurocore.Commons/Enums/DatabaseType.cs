using System;
using System.Collections.Generic;
using System.Text;

namespace Aurocore.Commons.Enums
{

    /// <summary>
    /// 資料庫型別列舉
    /// </summary>
    public enum DatabaseType
    {
        /// <summary>
        /// SqlServer資料庫
        /// </summary>
        SqlServer=0,
        /// <summary>
        /// Oracle資料庫
        /// </summary>
        Oracle,
        /// <summary>
        /// Access資料庫
        /// </summary>
        Access,
        /// <summary>
        /// MySql資料庫
        /// </summary>
        MySql,
        /// <summary>
        /// SQLite資料庫
        /// </summary>
        SQLite,
        /// <summary>
        /// PostgreSQL資料庫
        /// </summary>
        PostgreSQL
    }
}
