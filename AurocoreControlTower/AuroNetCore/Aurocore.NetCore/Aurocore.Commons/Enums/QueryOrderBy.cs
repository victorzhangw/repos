using System;
using System.Collections.Generic;
using System.Text;

namespace Aurocore.Commons.Enums
{
    public enum QueryOrderBy
    {
        Desc = 1,
        Asc = 2
    }
}
