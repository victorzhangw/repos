using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Aurocore.Commons.Extend
{
    /// <summary>
    /// 根據業務物件的型別進行反射操作輔助類
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Reflect<T> where T : class
    {
        private static Hashtable ObjCache = new Hashtable();
        private static object syncRoot = new Object();

        /// <summary>
        /// 根據參數建立物件實例
        /// </summary>
        /// <param name="sName">物件全域性名稱</param>
        /// <param name="sFilePath">檔案路徑</param>
        /// <returns></returns>
        public static T Create(string sName, string sFilePath)
        {
            return Create(sName, sFilePath, true);
        }

        /// <summary>
        /// 根據參數建立物件實例
        /// </summary>
        /// <param name="sName">物件全域性名稱</param>
        /// <param name="sFilePath">檔案路徑</param>
        /// <param name="bCache">快取集合</param>
        /// <returns></returns>
        public static T Create(string sName, string sFilePath, bool bCache)
        {
            string CacheKey = sName;
            T objType = null;
            if (bCache)
            {
                objType = (T)ObjCache[CacheKey];    //從快取讀取 
                if (!ObjCache.ContainsKey(CacheKey))
                {
                    lock (syncRoot)
                    {
                        objType = CreateInstance(CacheKey, sFilePath);
                        ObjCache.Add(CacheKey, objType);//快取資料職位問物件
                    }
                }
            }
            else
            {
                objType = CreateInstance(CacheKey, sFilePath);
            }

            return objType;
        }

        /// <summary>
        /// 根據全名和路徑構造物件
        /// </summary>
        /// <param name="sName">物件全名</param>
        /// <param name="sFilePath">程式集路徑</param>
        /// <returns></returns>
        private static T CreateInstance(string sName, string sFilePath)
        {
            Assembly assemblyObj = Assembly.Load(sFilePath);
            if (assemblyObj == null)
            {
                throw new ArgumentNullException("sFilePath", string.Format("無法載入sFilePath={0} 的程式集", sFilePath));
            }

            T obj = (T)assemblyObj.CreateInstance(sName); //反射建立 
            return obj;
        }
    }
}
