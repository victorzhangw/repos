using System;
using System.Collections.Generic;
using System.Text;

namespace Aurocore.Commons.Dtos
{
    /// <summary>
    /// 定義輸入DTO
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IInputDto<TKey>
    {
        /// <summary>
        /// 獲取或設定 主鍵，唯一標識
        /// </summary>
        TKey Id { get; set; }
    }
}
