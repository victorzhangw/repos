using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Aurocore.Commons.Pages;

namespace Aurocore.Commons.Dtos
{
    /// <summary>
    /// 查詢條件公共實體類
    /// </summary>
    [Serializable]
    [DataContract]
    public class SearchInputDto<T> : PagerInfo
    {
        /// <summary>
        /// 關鍵詞
        /// </summary>
        public string Keywords
        {
            get; set;
        }
        /// <summary>
        /// 編碼/程式碼
        /// </summary>
        public string EnCode
        {
            get; set;
        }
        /// <summary>
        /// 排序方式 預設asc
        /// </summary>
        public string Order
        {
            get; set;
        }
        /// <summary>
        /// 搜索起始日期
        /// </summary>
        public string StartDateTime
        {
            get; set;
        }
        /// <summary>
        /// 搜索結束日期
        /// </summary>
        public string EndDateTime
        {
            get; set;
        }
        /// <summary>
        /// 排序欄位 預設Id
        /// </summary>
        public string Sort
        {
            get; set;
        } = "Id";

        /// <summary>
        /// 查詢條件
        /// </summary>
        public T Filter { get; set; }
        
    }
}
