using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;

namespace Aurocore.Commons.Models
{
    /// <summary>
    /// 所有資料庫檢視對應實體類必須繼承此類
    /// </summary>
    [Serializable]
    public abstract class BaseViewModel : IEntity
    {

    }
}
