using System;

namespace Aurocore.Commons.Models
{
    /// <summary>
    /// 定義更新審計的資訊
    /// </summary>
    public interface IModificationAudited
    {
        /// <summary>
        /// 獲取或設定 最後修改使用者
        /// </summary>
        string LastModifyUserId { get; set; }
        /// <summary>
        /// 獲取或設定 最後修改時間
        /// </summary>
        DateTime? LastModifyTime { get; set; }
    }
}