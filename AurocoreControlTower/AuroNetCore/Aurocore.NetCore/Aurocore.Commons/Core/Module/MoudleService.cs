using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Aurocore.Commons.Repositories;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Linq;
using Aurocore.Commons.Log;

namespace Aurocore.Commons.Module
{
    /// <summary>
    /// 模組服務
    /// </summary>
    public class MoudleService
    {
        /// <summary>
        /// 實現應用模組程式集的註冊服務
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceProvider LoaderMoudleService(IServiceCollection services)
        {
           // services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            var apps = AppContext.BaseDirectory + "Apps";
            if (!Directory.Exists(apps))
            {
                Directory.CreateDirectory(apps);
            }
            // 把 Apps 下的動態庫拷貝一份來執行，
            // 使 Apps 中的動態庫不會在執行時被佔用（以便重新編譯）
            var targetPath = PrepareShadowCopies();
            // 從 Shadow Copy 目錄載入 Assembly 並註冊到 Mvc 中
            //LoadFromShadowCopies(targetPath);

            string PrepareShadowCopies()
            {
                // 準備 Shadow Copy 的目標目錄
                var target = Path.Combine(AppContext.BaseDirectory, "app_data", "apps-cache");

                if (!Directory.Exists(target))
                {
                    Directory.CreateDirectory(target);
                }
                // 找到外掛目錄下 bin 目錄中的 .dll，拷貝
                 Directory.EnumerateDirectories(apps)
                    .Select(path => Path.Combine(path, "bin"))
                    .Where(Directory.Exists)
                    .SelectMany(bin => Directory.EnumerateFiles(bin, "*.dll"))
                    .ForEach(dll => File.Copy(dll, Path.Combine(target, Path.GetFileName(dll)), true));

                return target;
            }

            DirectoryInfo folder = new DirectoryInfo(targetPath);
            List<Assembly> myAssembly = new List<Assembly>();
            myAssembly.Add(Assembly.Load("Aurocore.Security.Core"));
            if (File.Exists(AppContext.BaseDirectory+ "Aurocore.Messages.Core.dll"))
            {
                myAssembly.Add(Assembly.Load("Aurocore.Messages.Core"));
            }
            foreach (FileInfo finfo in folder.GetFiles("*.Core.dll"))
            {
                try
                {
                    myAssembly.Add(Assembly.LoadFrom(finfo.FullName));
                    string dllNamespaceStr = finfo.Name.Substring(0, finfo.Name.IndexOf(".Core"));
                    IoCContainer.RegisterFrom(finfo.FullName);
                    IoCContainer.RegisterLoadFrom(finfo.FullName, dllNamespaceStr);
                    Log4NetHelper.Info("注入應用模組" + finfo.Name + "成功");
                }
                catch (Exception ex)
                {
                    Log4NetHelper.Error("注入應用模組" + finfo.Name + "失敗\r\n" , ex);
                }
            }

            services.AddAutoMapper(myAssembly);
            services.AddScoped<IMapper, Mapper>();

            return IoCContainer.Build(services);
        }

    }
}
