using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Aurocore.Commons.Dapper;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.IRepositories;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Repositories;
namespace Aurocore.Commons.IRepositories
{
    /// <summary>
    /// 定義泛型介面,實體倉儲模型的資料標準操作
    /// </summary>
    /// <typeparam name="T">實體型別</typeparam>
    /// <typeparam name="TKey">主鍵型別</typeparam>
    public interface IRepository<T, TKey>:IDisposable where T : Entity
    {

        #region 單個實體
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="primaryKey">主鍵</param>
        /// <returns></returns>
        T Get(TKey primaryKey);

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="primaryKey">主鍵</param>
        /// <returns></returns>
        Task<T> GetAsync(TKey primaryKey);

        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        T GetWhere(string where);

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        Task<T> GetWhereAsync(string where);

        #endregion

        /// <summary>
        /// 同步查詢所有實體。
        /// </summary>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAll(IDbTransaction trans=null);

        /// <summary>
        /// 非同步查詢所有實體。
        /// </summary>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync(IDbTransaction trans=null);


        /// <summary>
        /// 根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetListWhere(string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 非同步根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetListWhereAsync(string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetListTopWhere(int top, string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetListTopWhereAsync(int top, string where = null, IDbTransaction trans = null);
        


        /// <summary>
        /// 同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        long Insert(T entity, IDbTransaction trans=null);

        /// <summary>
        /// 非同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<long> InsertAsync(T entity, IDbTransaction trans=null);

        /// <summary>
        /// 同步批量新增實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <returns></returns>
        void  Insert(List<T> entities);
        /// <summary>
        /// 同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Update(T entity, TKey primaryKey, IDbTransaction trans=null);

        /// <summary>
        /// 非同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity, TKey primaryKey, IDbTransaction trans=null);

        /// <summary>
        /// 同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Update(List<T> entities, IDbTransaction trans=null);

        /// <summary>
        /// 非同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(List<T> entities, IDbTransaction trans=null);

        /// <summary>
        /// 更新某一欄位值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        bool UpdateTableField(string strField, string fieldValue, string where, IDbTransaction trans = null);
        
        /// <summary>
        /// 非同步更新某一欄位值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<bool> UpdateTableFieldAsync(string strField, string fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 更新某一欄位值，欄位值為數字
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        bool UpdateTableField(string strField, int fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 更新某一欄位值，欄位值為數字
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        Task<bool> UpdateTableFieldAsync(string strField, int fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <returns></returns>
        bool Delete(T entity);

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T entity, IDbTransaction trans=null);

        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="primaryKey">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Delete(TKey primaryKey, IDbTransaction trans=null);

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="primaryKey">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(TKey primaryKey, IDbTransaction trans=null);

        /// <summary>
        /// 按主鍵批量刪除
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool DeleteBatch(IList<dynamic> ids, IDbTransaction trans = null);

        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool DeleteBatchWhere(string where, IDbTransaction trans = null);
        /// <summary>
        /// 非同步按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteBatchWhereAsync(string where, IDbTransaction trans = null);
        /// <summary>
        /// 同步軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool DeleteSoft(bool bl, TKey primaryKey,string userId=null, IDbTransaction trans=null);

        /// <summary>
        /// 非同步軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteSoftAsync(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans=null);

        /// <summary>
        /// 非同步批量軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null);
        /// <summary>
        /// 設定資料有效性，將EnabledMark設定為1-有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool SetEnabledMark(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans=null);

        /// <summary>
        /// 非同步設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> SetEnabledMarkAsync(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans=null);


        /// <summary>
        /// 非同步按條件設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null);
        Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null);
        Task<bool> SetEnabledMarkByWhereAsync(string setfield, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null);
        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsDeleteMark(string where = null, IDbTransaction trans=null);
       
        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsNotDeleteMark(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsEnabledMark(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsNotEnabledMark(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsNotDeleteAndEnabledMark(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsDeleteMarkAsync(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsNotDeleteMarkAsync(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsEnabledMarkAsync(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsNotEnabledMarkAsync(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsNotDeleteAndEnabledMarkAsync(string where = null, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc,  IDbTransaction trans = null);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        List<T> FindWithPager(string condition, PagerInfo info, IDbTransaction trans = null);


        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<List<T>> FindWithPagerSqlAsync(string condition, PagerInfo info, string fieldToSort, bool desc,  IDbTransaction trans = null);
        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        List<T> FindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        Task<List<object>> FindWithPagerRelationUserAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);
        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        List<object> FindWithPagerRelationUser(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        int GetCountByWhere(string condition);
        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        Task<int> GetCountByWhereAsync(string condition);

        /// <summary>
        /// 根據條件查詢獲取某個欄位的最大值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        Task<int> GetMaxValueByFieldAsync(string strField, string where, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件統計某個欄位之和,sum(欄位)
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        Task<int> GetSumValueByFieldAsync(string strField, string where, IDbTransaction trans = null);

        /// <summary>
        /// 多表操作--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        Task<Tuple<bool, string>> ExecuteTransactionAsync(List<Tuple<string, object>> trans, int? commandTimeout = null);
        /// <summary>
        /// 多表操作--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
       Tuple<bool, string> ExecuteTransaction(List<Tuple<string, object>> trans, int? commandTimeout = null);


        #region EF

        #region Insert
        /// <summary>
        /// 新增實體
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Add(T entity);
        /// <summary>
        /// 新增實體
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<int> AddAsync(T entity);
        /// <summary>
        /// 批量新增，數量量較多是推薦使用BulkInsert()
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int AddRange(ICollection<T> entities);
        /// <summary>
        /// 批量新增，數量量較多是推薦使用BulkInsert()
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task<int> AddRangeAsync(ICollection<T> entities);
        /// <summary>
        /// 批量新增SqlBulk方式，效率最高
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="destinationTableName"></param>
        void BulkInsert(IList<T> entities, string destinationTableName = null);
        /// <summary>
        /// 執行新增的sql語句
        /// </summary>
        /// <param name="sql">新增Sql語句</param>
        /// <returns></returns>
        int AddBySql(string sql);

        #endregion

        #region Delete
        /// <summary>
        /// 根據主鍵刪除資料
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        int Delete(TKey key);
        /// <summary>
        /// 執行刪除資料Sql語句
        /// </summary>
        /// <param name="sql">刪除的Sql語句</param>
        /// <returns></returns>
        int DeleteBySql(string sql);
        #endregion

        #region Update
        /// <summary>
        /// 更新一個實體資料
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Edit(T entity);
        /// <summary>
        /// 批量更新資料實體
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        int EditRange(ICollection<T> entities);
        /// <summary>
        /// 更新指定欄位的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">資料實體</param>
        /// <param name="updateColumns">指定欄位</param>
        /// <returns></returns>
        int Update(T model, params string[] updateColumns);
        /// <summary>
        /// 執行更新資料的Sql語句
        /// </summary>
        /// <param name="sql">更新資料的Sql語句</param>
        /// <returns></returns>
        int UpdateBySql(string sql);

        #endregion

        #region Query

        /// <summary>
        /// 根據條件統計數量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        int Count(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 根據條件統計數量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> CountAsync(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        bool Exist(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<bool> ExistAsync(Expression<Func<T, bool>> @where = null);
        /// <summary>
        ///  根據主鍵獲取實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        T GetSingle(TKey key);
        /// <summary>
        ///  根據主鍵獲取實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<T> GetSingleAsync(TKey key);
        /// <summary>
        /// 獲取單個實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        T GetSingleOrDefault(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 獲取單個實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<T> GetSingleOrDefaultAsync(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 獲取實體列表。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IList<T> Get(Expression<Func<T, bool>> @where = null);
        /// <summary>
        /// 獲取實體列表。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<List<T>> GetAsync(Expression<Func<T, bool>> @where = null);
        /// <summary>
        ///  分頁獲取實體列表。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="pagerInfo">分頁資訊</param>
        /// <param name="asc">排序方式</param>
        /// <param name="orderby">排序欄位</param>
        /// <returns></returns>
        IEnumerable<T> GetByPagination(Expression<Func<T, bool>> @where,PagerInfo pagerInfo, bool asc = true,
            params Expression<Func<T, object>>[] @orderby);
        /// <summary>
        /// sql語句查詢資料集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        List<T> GetBySql(string sql);
        /// <summary>
        /// sql語句查詢資料集，返回輸出Dto實體
        /// </summary>
        /// <typeparam name="TView">返回結果物件</typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        List<TView> GetViews<TView>(string sql);
        /// <summary>
        /// 查詢檢視
        /// </summary>
        /// <typeparam name="TView">返回結果物件</typeparam>
        /// <param name="viewName">檢視名稱</param>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        List<TView> GetViews<TView>(string viewName, Func<TView, bool> where);

        #endregion
        #endregion
    }
}
