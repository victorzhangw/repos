using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Aurocore.Commons.Core.DataManager;

namespace Aurocore.Commons.Core.Dapper
{
    /// <summary>
    /// 註冊的時候 InstancePerLifetimeScope
    /// 執行緒內唯一（也就是單個請求內唯一）
    /// </summary>
    public class DapperDbContext
    {

        private IDbConnection dbConnection { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IDbConnection Connection
        {
            get
            {
                if (dbConnection == null || dbConnection.State == ConnectionState.Closed)
                {
                    dbConnection = DBServerProvider.GetDBConnection();

                    if (MiniProfiler.Current != null)
                    {
                        dbConnection = new StackExchange.Profiling.Data.ProfiledDbConnection((DbConnection)dbConnection, MiniProfiler.Current);
                    }
                }
                return dbConnection;
            }
        }
        public IDbConnection GetConnection<T>() where T : class
        {
            if (dbConnection == null || dbConnection.State == ConnectionState.Closed)
            {
                dbConnection = DBServerProvider.GetDBConnection<T>();

                if (MiniProfiler.Current != null)
                {
                    dbConnection = new StackExchange.Profiling.Data.ProfiledDbConnection((DbConnection)dbConnection, MiniProfiler.Current);
                }
            }
            return dbConnection;
        }
        /// <summary>
        /// 事務
        /// </summary>
        public IDbTransaction DbTransaction { get; set; }

        /// <summary>
        /// 是否已被提交
        /// </summary>
        public bool Committed { get; private set; } = true;

        /// <summary>
        /// 開啟事務
        /// </summary>
        public void BeginTransaction()
        {
            Committed = false;
            bool isClosed = Connection.State == ConnectionState.Closed;
            if (isClosed) Connection.Open();
            DbTransaction = Connection?.BeginTransaction();
        }

        /// <summary>
        /// 事務提交
        /// </summary>
        public void CommitTransaction()
        {
            DbTransaction?.Commit();
            Committed = true;
            Dispose();
        }

        /// <summary>
        /// 事務回滾
        /// </summary>
        public void RollBackTransaction()
        {
            DbTransaction?.Rollback();
            Committed = true;
            Dispose();
        }


        #region Dispose實現
        private bool disposedValue = false; // 要檢測冗餘呼叫



        /// <summary>
        /// 釋放
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 釋放託管狀態(託管物件)。
                }

                // TODO: 釋放未託管的資源(未託管的物件)並在以下內容中替代終結器。
                // TODO: 將大型欄位設定為 null。

                disposedValue = true;
            }
            if (Connection != null)
            {
                DbTransaction.Dispose();
                Connection.Dispose();
            }
        }
        /// <summary>
        /// 僅當以上 Dispose(bool disposing) 擁有用於釋放未託管資源的程式碼時才替代終結器。
        /// </summary>
        public void Dispose()
        {
            // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
            Dispose(true);

            DbTransaction?.Dispose();
            if (Connection.State == ConnectionState.Open)
                Connection?.Close();
            // TODO: 如果在以上內容中替代了終結器，則取消註釋以下行。
            // GC.SuppressFinalize(this);
        }

        #endregion

    }
}
