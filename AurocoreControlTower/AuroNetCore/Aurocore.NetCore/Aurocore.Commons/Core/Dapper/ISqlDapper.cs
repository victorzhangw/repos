using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Models;

namespace Aurocore.Commons.Dapper
{
    /// <summary>
    /// Dapper定義介面
    /// </summary>
    public interface ISqlDapper
    {

        /// <summary>
        /// 開啟事務
        /// </summary>
        void BeginTransaction();
        /// <summary>
        /// 事務提交
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// 事務回滾
        /// </summary>
        void RollBackTransaction();

        T Get<T>(dynamic primaryKey) where T : class;
        Task<T> GetAsync<T>(dynamic primaryKey) where T : class;
    }
}
