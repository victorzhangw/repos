using Dapper;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aurocore.Commons.Core.DataManager;

namespace Aurocore.Commons.Dapper
{
    /// <summary>
    /// Dapper常用方法的實現
    /// 註冊的時候 InstancePerLifetimeScope
    /// 執行緒內唯一（也就是單個請求內唯一）
    /// </summary>
    public class SqlDapper : ISqlDapper
    {
        private IDbConnection dbConnection { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IDbConnection Connection
        {
            get
            {
                if (dbConnection == null || dbConnection.State == ConnectionState.Closed)
                {
                    dbConnection = DBServerProvider.GetDBConnection();
                }
                return dbConnection;
            }
        }

        public SqlDapper()
        {
            DBServerProvider.GetConnectionString();
        }
        /// <summary>
        /// 獲取資料庫連線
        /// </summary>
        /// <param name="connKeyName"></param>
        public SqlDapper(string connKeyName)
        {
           DBServerProvider.GetConnectionString(connKeyName);
        }

        /// <summary>
        /// 事務
        /// </summary>
        public IDbTransaction DbTransaction { get; set; }

        /// <summary>
        /// 是否已被提交
        /// </summary>
        public bool Committed { get; private set; } = true;

        /// <summary>
        /// 開啟事務
        /// </summary>
        public void BeginTransaction()
        {
            Committed = false;
            bool isClosed = Connection.State == ConnectionState.Closed;
            if (isClosed) Connection.Open();
            DbTransaction = Connection?.BeginTransaction();
        }

        /// <summary>
        /// 事務提交
        /// </summary>
        public void CommitTransaction()
        {
            DbTransaction?.Commit();
            Committed = true;
            Dispose();
        }

        /// <summary>
        /// 事務回滾
        /// </summary>
        public void RollBackTransaction()
        {
            DbTransaction?.Rollback();
            Committed = true;
            Dispose();
        }

        public T Get<T>(dynamic primaryKey) where T : class
        {
            var type = typeof(T);
            var key = GetSingleKey<T>(nameof(Get));
            var name = GetTableName(type);
            string sql = $"SELECT * FROM {name} WHERE {key.Name} = @keyName";

            var dynParms = new DynamicParameters();
            dynParms.Add("@keyName", primaryKey);
            return Connection.QueryFirstOrDefault<T>(sql);
        }

        public async Task<T> GetAsync<T>(dynamic primaryKey) where T : class
        {
            var type = typeof(T);
            var key = GetSingleKey<T>(nameof(Get));
            var name = GetTableName(type);
            string sql = $"SELECT * FROM {name} WHERE {key.Name} = @keyName";

            var dynParms = new DynamicParameters();
            dynParms.Add("@keyName", primaryKey);
            return await Connection.QueryFirstOrDefaultAsync<T>(sql);
        }


        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> KeyProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();
        private static string GetTableName(Type type)
        {
#if NETSTANDARD1_3
                var info = type.GetTypeInfo();
#else
                var info = type;
#endif
                //NOTE: This as dynamic trick falls back to handle both our own Table-attribute as well as the one in EntityFramework 
                var tableAttrName =
                    info.GetCustomAttribute<TableAttribute>(false)?.Name
                    ?? (info.GetCustomAttributes(false).FirstOrDefault(attr => attr.GetType().Name == "TableAttribute") as dynamic)?.Name;


            return tableAttrName;
        }


        private static PropertyInfo GetSingleKey<T>(string method)
        {
            var type = typeof(T);
            var keys = KeyPropertiesCache(type);
            //var explicitKeys = ExplicitKeyPropertiesCache(type);
            var keyCount = keys.Count;// + explicitKeys.Count;
            if (keyCount > 1)
                throw new DataException($"{method}<T> only supports an entity with a single [Key]  property. [Key] Count: {keys.Count}");
            if (keyCount == 0)
                throw new DataException($"{method}<T> only supports an entity with a [Key]  property");

            return keys[0];
        }

        /// <summary>
        /// 查詢主鍵,屬性加了Key標籤
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static List<PropertyInfo> KeyPropertiesCache(Type type)
        {
            if (KeyProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> pi))
            {
                return pi.ToList();
            }

            var allProperties = TypePropertiesCache(type);
            var keyProperties = allProperties.Where(p => p.GetCustomAttributes(true).Any(a => a is KeyAttribute)).ToList();

            if (keyProperties.Count == 0)
            {
                var idProp = allProperties.Find(p => string.Equals(p.Name, "Id", StringComparison.CurrentCultureIgnoreCase));
                if (idProp != null)
                {
                    keyProperties.Add(idProp);
                }
            }

            KeyProperties[type.TypeHandle] = keyProperties;
            return keyProperties;
        }

        private static List<PropertyInfo> TypePropertiesCache(Type type)
        {
            if (TypeProperties.TryGetValue(type.TypeHandle, out IEnumerable<PropertyInfo> pis))
            {
                return pis.ToList();
            }

            var properties = type.GetProperties().ToArray();
            TypeProperties[type.TypeHandle] = properties;
            return properties.ToList();
        }

        #region 輔助類方法


        /// <summary>
        /// 驗證是否存在注入程式碼(條件語句）
        /// </summary>
        /// <param name="inputData"></param>
        public virtual bool HasInjectionData(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
                return false;

            //裡面定義惡意字元集合
            //驗證inputData是否包含惡意集合
            if (Regex.IsMatch(inputData.ToLower(), GetRegexString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 獲取正規表示式
        /// </summary>
        /// <returns></returns>
        private string GetRegexString()
        {
            //構造SQL的注入關鍵字元
            string[] strBadChar =
            {
                "select\\s",
                "from\\s",
                "insert\\s",
                "delete\\s",
                "update\\s",
                "drop\\s",
                "truncate\\s",
                "exec\\s",
                "count\\(",
                "declare\\s",
                "asc\\(",
                "mid\\(",
                //"char\\(",
                "net user",
                "xp_cmdshell",
                "/add\\s",
                "exec master.dbo.xp_cmdshell",
                "net localgroup administrators"
            };

            //構造正規表示式
            string str_Regex = ".*(";
            for (int i = 0; i < strBadChar.Length - 1; i++)
            {
                str_Regex += strBadChar[i] + "|";
            }
            str_Regex += strBadChar[strBadChar.Length - 1] + ").*";

            return str_Regex;
        }


        #endregion


        #region Dispose實現
        private bool disposedValue = false; // 要檢測冗餘呼叫
        /// <summary>
        /// 釋放
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 釋放託管狀態(託管物件)。
                }

                // TODO: 釋放未託管的資源(未託管的物件)並在以下內容中替代終結器。
                // TODO: 將大型欄位設定為 null。

                disposedValue = true;
            }
            if (Connection != null)
            {
                DbTransaction.Dispose();
                Connection.Dispose();
            }
        }

        // TODO: 僅當以上 Dispose(bool disposing) 擁有用於釋放未託管資源的程式碼時才替代終結器。
        // ~BaseRepository() {
        //   // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
        //   Dispose(false);
        // }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
            Dispose(true);

            DbTransaction?.Dispose();
            if (Connection.State == ConnectionState.Open)
                Connection?.Close();
            // TODO: 如果在以上內容中替代了終結器，則取消註釋以下行。
            // GC.SuppressFinalize(this);
        }

        #endregion

    }
}
