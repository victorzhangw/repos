using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Options;

namespace Aurocore.Commons.DbContextCore
{
    /// <summary>
    /// 工廠類
    /// </summary>
    public class DbContextFactory
    {
        /// <summary>
        /// 
        /// </summary>
        public static DbContextFactory Instance => new DbContextFactory();
        /// <summary>
        /// 服務
        /// </summary>
        public IServiceCollection ServiceCollection { get; set; }
        /// <summary>
        /// 建構函式
        /// </summary>
        public DbContextFactory()
        {
        }
        public void AddDbContext<TContext>(DbContextOption option)
            where TContext : BaseDbContext, IDbContextCore
        {
            ServiceCollection.AddDbContext<IDbContextCore, TContext>(option);
        }

        public void AddDbContext<ITContext, TContext>(DbContextOption option)
            where ITContext : IDbContextCore
            where TContext : BaseDbContext, ITContext
        {
            ServiceCollection.AddDbContext<ITContext, TContext>(option);
        }
    }
}
