using System;
using System.Collections.Generic;
using System.Text;

namespace Aurocore.Commons.Core.DataManager
{
    /// <summary>
    /// 資料庫連線設定特性
    /// </summary>
    public class AppDBContextAttribute : Attribute
    {
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        public string DbConfigName { get; set; }

        public AppDBContextAttribute(string dbConfigName)
        {
            DbConfigName = dbConfigName;
        }
    }
}
