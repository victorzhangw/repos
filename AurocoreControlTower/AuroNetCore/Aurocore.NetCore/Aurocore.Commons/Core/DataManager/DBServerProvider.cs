using Microsoft.Data.Sqlite;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using Aurocore.Commons.Dapper;
using Aurocore.Commons.DbContextCore;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.IDbContext;

namespace Aurocore.Commons.Core.DataManager
{
    /// <summary>
    /// 資料庫服務提供者
    /// </summary>
    public class DBServerProvider
    {
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        private static string dbConfigName = "";
        /// <summary>
        /// 資料庫連線
        /// </summary>
        private static IDbConnection dbConnection;
        #region Dapper Context
        /// <summary>
        /// 獲取預設資料庫連線
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
          return  GetConnectionString(dbConfigName);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
          return  dbConfigName = key?? dbConfigName;
        }

        /// <summary>
        /// 獲取資料庫連線
        /// </summary>
        /// <returns></returns>
        public static IDbConnection GetDBConnection<TEntity>()
        {
            string conStringEncrypt = Configs.GetConfigurationValue("AppSetting", "ConStringEncrypt");
            bool isMultiTenant = Configs.GetConfigurationValue("AppSetting", "IsMultiTenant").ToBool();
            //獲取實體真實的資料庫連線池物件名，如果不存在則用預設資料連線池名
            dbConfigName = typeof(TEntity).GetCustomAttribute<AppDBContextAttribute>(false)?.DbConfigName ?? dbConfigName;
            if (string.IsNullOrEmpty(dbConfigName))
            {
                dbConfigName = Configs.GetConfigurationValue("AppSetting", "DefaultDataBase");
            }
            // 資料庫連線設定
            string defaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
            if (isMultiTenant)
            {
                defaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
            }
            if (conStringEncrypt == "true")
            {
                defaultSqlConnectionString = DEncrypt.Decrypt(defaultSqlConnectionString);
            }
            string dbType = dbConfigName.ToUpper();
            if (dbType.Contains("MSSQL"))
            {
                dbConnection=new SqlConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("MYSQL"))
            {
                dbConnection =new MySqlConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("ORACLE"))
            {
                dbConnection = new OracleConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("SQLITE"))
            {
                dbConnection = new SqliteConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("NPGSQL"))
            {
                dbConnection = new NpgsqlConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("MEMORY"))
            {
                throw new NotSupportedException("In Memory Dapper Database Provider is not yet available.");
            }
            else
            {
                throw new NotSupportedException("The database is not supported");
            }
            return dbConnection;
        }

        /// <summary>
        /// 獲取資料庫連線
        /// </summary>
        /// <returns></returns>
        public static IDbConnection GetDBConnection()
        {
            string conStringEncrypt = Configs.GetConfigurationValue("AppSetting", "ConStringEncrypt");
            bool isMultiTenant = Configs.GetConfigurationValue("AppSetting", "IsMultiTenant").ToBool();
            if (string.IsNullOrEmpty(dbConfigName))
            {
                dbConfigName = Configs.GetConfigurationValue("AppSetting", "DefaultDataBase");
            }
            // 資料庫連線設定
            string defaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
            if (isMultiTenant)
            {
                defaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
            }
            if (conStringEncrypt == "true")
            {
                defaultSqlConnectionString = DEncrypt.Decrypt(defaultSqlConnectionString);
            }
            string dbType = dbConfigName.ToUpper();
            if (dbType.Contains("MSSQL"))
            {
                dbConnection = new SqlConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("MYSQL"))
            {
                dbConnection = new MySqlConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("ORACLE"))
            {
                dbConnection = new OracleConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("SQLITE"))
            {
                dbConnection = new SqliteConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("NPGSQL"))
            {
                dbConnection = new NpgsqlConnection(defaultSqlConnectionString);
            }
            else if (dbType.Contains("MEMORY"))
            {
                throw new NotSupportedException("In Memory Dapper Database Provider is not yet available.");
            }
            else
            {
                throw new NotSupportedException("The database is not supported");
            }
            return dbConnection;
        }
        /// <summary>
        /// 預設資料庫連線
        /// </summary>
        public static ISqlDapper SqlDapper
        {
            get
            {
                return new SqlDapper(dbConfigName);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static ISqlDapper GetSqlDapper(string dbName = null)
        {
            return new SqlDapper(dbName ?? dbConfigName);
        }
        /// <summary>
        /// 指定資料庫連線
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public static ISqlDapper GetSqlDapper<TEntity>()
        {
            //獲取實體真實的資料庫連線池物件名，如果不存在則用預設資料連線池名
            dbConfigName = typeof(TEntity).GetCustomAttribute<AppDBContextAttribute>(false)?.DbConfigName ?? dbConfigName;
            return GetSqlDapper(dbConfigName);
        }
        #endregion

        #region EF Context
        /// <summary>
        /// 獲取實體的資料庫連線
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="defaultDbContext"></param>
        /// <returns></returns>
        public static void GetDbContextConnection<TEntity>(IDbContextCore defaultDbContext)
        {
            dbConfigName = typeof(TEntity).GetCustomAttribute<AppDBContextAttribute>(false)?.DbConfigName ?? dbConfigName;
        }
        #endregion
    }
}
