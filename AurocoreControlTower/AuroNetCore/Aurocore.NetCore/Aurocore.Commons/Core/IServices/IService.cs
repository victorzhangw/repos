using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;

namespace Aurocore.Commons.IServices
{
    /// <summary>
    /// 泛型應用服務介面
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TODto"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public interface IService<T,TODto, TKey> : IDisposable where T : Entity
        where TODto : class
    {
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        T Get(TKey id);
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        TODto GetOutDto(TKey id);

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        Task<TODto> GetOutDtoAsync(TKey id);
        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        Task<T> GetAsync(TKey id);
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        T GetWhere(string where, IDbTransaction trans = null);
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        TODto GetOutDtoWhere(string where, IDbTransaction trans = null);
        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<T> GetWhereAsync(string where, IDbTransaction trans = null);

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<TODto> GetOutDtoWhereAsync(string where, IDbTransaction trans = null);
        /// <summary>
        /// 同步查詢所有實體。
        /// </summary>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAll(IDbTransaction trans = null);

        /// <summary>
        /// 非同步查詢所有實體。
        /// </summary>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync(IDbTransaction trans = null);


        /// <summary>
        /// 根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetListWhere(string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 非同步根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetListWhereAsync(string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetListTopWhere(int top, string where = null, IDbTransaction trans = null);
        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetListTopWhereAsync(int top, string where = null, IDbTransaction trans = null);

        /// <summary>
        /// 同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        long Insert(T entity, IDbTransaction trans = null);

        /// <summary>
        /// 非同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<long> InsertAsync(T entity, IDbTransaction trans = null);

        /// <summary>
        /// 同步批量新增實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <returns></returns>
        void Insert(List<T> entities);


        /// <summary>
        /// 同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Update(T entity, TKey id, IDbTransaction trans = null);

        /// <summary>
        /// 非同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(T entity, TKey id, IDbTransaction trans = null);

        /// <summary>
        /// 同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Update(List<T> entities, IDbTransaction trans = null);

        /// <summary>
        /// 非同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> UpdateAsync(List<T> entities, IDbTransaction trans = null);

        /// <summary>
        /// 更新某一欄位值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        bool UpdateTableField(string strField, string fieldValue, string where, IDbTransaction trans = null);

        /// <summary>
        /// 非同步更新某一欄位值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<bool> UpdateTableFieldAsync(string strField, string fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 更新某一欄位值，欄位值為數字
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        bool UpdateTableField(string strField, int fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 更新某一欄位值，欄位值為數字
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        Task<bool> UpdateTableFieldAsync(string strField, int fieldValue, string where, IDbTransaction trans = null);
        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Delete(T entity, IDbTransaction trans = null);

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(T entity, IDbTransaction trans = null);

        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool Delete(TKey id, IDbTransaction trans = null);

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(TKey id, IDbTransaction trans = null);

        /// <summary>
        /// 按主鍵批量刪除
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool DeleteBatch(IList<dynamic> ids, IDbTransaction trans = null);

        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool DeleteBatchWhere(string where, IDbTransaction trans = null);
        /// <summary>
        /// 非同步按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteBatchWhereAsync(string where, IDbTransaction trans = null);
        /// <summary>
        /// 同步軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool DeleteSoft(bool bl, TKey id, string userId = null, IDbTransaction trans = null);

        /// <summary>
        /// 非同步軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteSoftAsync(bool bl, TKey id, string userId = null, IDbTransaction trans = null);

        /// <summary>
        /// 非同步批量軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> DeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null);
        /// <summary>
        /// 設定資料有效性，將EnabledMark設定為1-有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        bool SetEnabledMark(bool bl, TKey id, string userId = null, IDbTransaction trans = null);

        /// <summary>
        /// 非同步設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> SetEnabledMarkAsync(bool bl, TKey id, string userId = null, IDbTransaction trans = null);


        /// <summary>
        /// 非同步按條件設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null);

        Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null);
        Task<bool> SetEnabledMarkByWhereAsync(string setfield, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null);
        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsDeleteMark(string where = null, IDbTransaction trans = null);

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsNotDeleteMark(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsEnabledMark(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsNotEnabledMark(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        IEnumerable<T> GetAllByIsNotDeleteAndEnabledMark(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsDeleteMarkAsync(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsNotDeleteMarkAsync(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsEnabledMarkAsync(string where = null, IDbTransaction tran = null);

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="tran">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsNotEnabledMarkAsync(string where = null, IDbTransaction tran = null);
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllByIsNotDeleteAndEnabledMarkAsync(string where = null, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        List<T> FindWithPager(string condition, PagerInfo info, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定物件的集合</returns>
        PageResult<TODto> FindWithPager(SearchInputDto<T> search);
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定物件的集合</returns>
        Task<PageResult<TODto>> FindWithPagerAsync(SearchInputDto<T> search);
        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<List<T>> FindWithPagerSqlAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);
        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        List<T> FindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        Task<List<object>> FindWithPagerRelationUserAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);
        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        List<object> FindWithPagerRelationUser(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        int GetCountByWhere(string condition);
        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        Task<int> GetCountByWhereAsync(string condition);

        /// <summary>
        /// 根據條件查詢獲取某個欄位的最大值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        Task<int> GetMaxValueByFieldAsync(string strField, string where, IDbTransaction trans = null);

        /// <summary>
        /// 根據條件統計某個欄位之和,sum(欄位)
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        Task<int> GetSumValueByFieldAsync(string strField, string where, IDbTransaction trans = null);

        /// <summary>
        /// 多表操作--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        Task<Tuple<bool, string>> ExecuteTransactionAsync(List<Tuple<string, object>> trans, int? commandTimeout = null);
        /// <summary>
        /// 多表操作--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        Tuple<bool, string> ExecuteTransaction(List<Tuple<string, object>> trans, int? commandTimeout = null);
    }
}