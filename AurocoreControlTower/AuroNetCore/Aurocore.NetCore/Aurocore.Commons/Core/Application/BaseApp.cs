using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.Commons.IServices;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;

namespace Aurocore.Commons.Application
{

    /// <summary>
    /// 業務層基類，Service用於普通的資料庫操作
    /// </summary>
    /// <typeparam name="T">實體型別</typeparam>
    /// <typeparam name="TDto">實體型別</typeparam>
    /// <typeparam name="TService">Service型別</typeparam>
    /// <typeparam name="Tkey">主鍵型別</typeparam>
    public class BaseApp<T, TDto, TService, Tkey>
       where T : Entity
       where TDto : class
        where TService : IService<T, TDto, Tkey>
    {
        /// <summary>
        /// 用於普通的資料庫操作
        /// </summary>
        /// <value>The service.</value>
        protected IService<T,TDto, Tkey> service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_service"></param>
        public BaseApp(IService<T,TDto, Tkey> _service)
        {
            service = _service;
        }

        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <returns></returns>
        public virtual bool Delete(T entity)
        {
            return service.Delete(entity, null);
        }
        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public virtual bool Delete(Tkey id)
        {
            return service.Delete(id, null);
        }

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public virtual Task<bool> DeleteAsync(Tkey id)
        {
            return service.DeleteAsync(id, null);
        }


        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <returns></returns>
        public virtual Task<bool> DeleteAsync(T entity)
        {
            return service.DeleteAsync(entity, null);
        }

        /// <summary>
        /// 按主鍵批量刪除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public virtual bool DeleteBatch(IList<dynamic> ids)
        {
            return service.DeleteBatch(ids, null);
        }


        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <returns></returns>
        public virtual bool DeleteBatchWhere(string where)
        {
            return service.DeleteBatchWhere(where, null);
        }
        /// <summary>
        /// 軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <returns></returns>
        public virtual bool DeleteSoft(bool bl, Tkey id, string userId)
        {
            return service.DeleteSoft(bl, id, userId, null);
        }

        /// <summary>
        /// 非同步軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <returns></returns>
        public virtual Task<bool> DeleteSoftAsync(bool bl, Tkey id, string userId)
        {
            return service.DeleteSoftAsync(bl, id, userId, null);
        }
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public virtual T Get(Tkey id)
        {
            return service.Get(id);
        }
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual T GetWhere(string where)
        {
            return service.GetWhere(where, null);
        }

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<T> GetWhereAsync(string where)
        {
            return await service.GetWhereAsync(where, null);
        }

        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetListTopWhere(int top, string where = null)
        {
            return service.GetListTopWhere(top, where);
        }
        /// <summary>
        /// 同步查詢所有實體。
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll()
        {
            return service.GetAll(null);
        }

        /// <summary>
        /// 非同步步查詢所有實體。
        /// </summary>
        /// <returns></returns>
        public virtual Task<IEnumerable<T>> GetAllAsync()
        {
            return service.GetAllAsync(null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual Task<T> GetAsync(Tkey id)
        {
            return service.GetAsync(id);
        }
        ///<summary>
        /// 根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>

        public virtual IEnumerable<T> GetListWhere(string where = null)
        {
            return service.GetListWhere(where, null);
        }
        ///<summary>
        /// 非同步根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetListWhereAsync(string where = null)
        {
            return await service.GetListWhereAsync(where, null);
        }
        /// <summary>
        /// 同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <returns></returns>
        public virtual long Insert(T entity)
        {
            return service.Insert(entity, null);
        }

        /// <summary>
        /// 非同步步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <returns></returns>
        public virtual Task<long> InsertAsync(T entity)
        {
            return service.InsertAsync(entity, null);
        }
        /// <summary>
        /// 同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <returns></returns>
        public virtual bool Update(T entity, Tkey id)
        {
            return service.Update(entity, id, null);
        }

        /// <summary>
        /// 同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <returns></returns>
        public virtual bool Update(List<T> entities)
        {
            return service.Update(entities, null);
        }

        /// <summary>
        /// 非同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <returns></returns>
        public virtual Task<bool> UpdateAsync(T entity, Tkey id)
        {
            return service.UpdateAsync(entity, id, null);
        }

        /// <summary>
        /// 非同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <returns></returns>
        public virtual Task<bool> UpdateAsync(List<T> entities)
        {
            return service.UpdateAsync(entities, null);
        }


        /// <summary>
        /// 更新某一欄位值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <returns></returns>
        public virtual bool UpdateTableField(string strField, string fieldValue, string where)
        {

            return service.UpdateTableField(strField, fieldValue, where, null);
        }

        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsDeleteMark(string where = null)
        {
            return service.GetAllByIsDeleteMark(where, null);
        }

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotDeleteMark(string where = null)
        {
            return service.GetAllByIsNotDeleteMark(where, null);
        }

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsEnabledMark(string where = null)
        {
            return service.GetAllByIsEnabledMark(where, null);
        }

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotEnabledMark(string where = null)
        {
            return service.GetAllByIsNotEnabledMark(where, null);
        }
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotDeleteAndEnabledMark(string where = null)
        {
            return service.GetAllByIsNotDeleteAndEnabledMark(where, null);
        }

        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsDeleteMarkAsync(string where = null)
        {
            return await service.GetAllByIsDeleteMarkAsync(where, null);
        }

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotDeleteMarkAsync(string where = null)
        {
            return await service.GetAllByIsNotDeleteMarkAsync(where, null);
        }

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual  async Task<IEnumerable<T>> GetAllByIsEnabledMarkAsync(string where = null)
        {
            return await service.GetAllByIsEnabledMarkAsync(where, null);
        }

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotEnabledMarkAsync(string where = null)
        {
            return await service.GetAllByIsNotEnabledMarkAsync(where, null);
        }

        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotDeleteAndEnabledMarkAsync(string where = null)
        {
            return await service.GetAllByIsNotDeleteAndEnabledMarkAsync(where, null);
        }


        /// <summary>
        /// 設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <returns></returns>
        public virtual bool SetEnabledMark(bool bl, Tkey id, string userId = null)
        {
            return service.SetEnabledMark(bl, id, userId, null);
        }

        /// <summary>
        /// 非同步設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <returns></returns>
        public virtual async Task<bool> SetEnabledMarkAsync(bool bl, Tkey id, string userId = null)
        {
            return await service.SetEnabledMarkAsync(bl, id, userId, null);
        }


        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info)
        {
            return service.FindWithPager(condition, info, null);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort)
        {
            return service.FindWithPager(condition, info, fieldToSort, null);
        }
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc)
        {
            return service.FindWithPager(condition, info, fieldToSort, desc, null);
        }

        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <returns></returns>
        public virtual List<T> FindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc)
        {
            return service.FindWithPagerSql(condition, info, fieldToSort, desc, null);
        }

        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <returns></returns>
        public virtual List<object> FindWithPagerRelationUser(string condition, PagerInfo info, string fieldToSort, bool desc)
        {
            return service.FindWithPagerRelationUser(condition, info, fieldToSort, desc, null);
        }
        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        public virtual int GetCountByWhere(string condition)
        {
            return service.GetCountByWhere(condition);
        }
    }
}
