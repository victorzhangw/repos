using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.IRepositories;
using Aurocore.Commons.IServices;
using Aurocore.Commons.Json;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Zxw.Framework.NetCore.Extensions;

namespace Aurocore.Commons.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TODto"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public abstract class BaseService<T, TODto,TKey> : IService<T, TODto, TKey>
        where T: Entity
        where TODto : class
        where TKey : IEquatable<TKey>
    {
        private IHttpContextAccessor _accessor;
        /// <summary>
        /// 
        /// </summary>
        protected IRepository<T,TKey> repository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iRepository"></param>
        protected BaseService(IRepository<T, TKey> iRepository)
        {
            repository = iRepository ?? throw new ArgumentNullException(nameof(repository));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iRepository"></param>
        /// <param name="accessor"></param>
        protected BaseService(IRepository<T, TKey> iRepository, IHttpContextAccessor accessor)
        {
            _accessor = accessor;
            repository = iRepository ?? throw new ArgumentNullException(nameof(repository));

        }
        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual bool Delete(T entity,IDbTransaction trans=null)
        {
          return repository.Delete(entity);
        }
        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual bool Delete(TKey id, IDbTransaction trans = null)
        {
            return repository.Delete(id,trans);
        }

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual Task<bool> DeleteAsync(TKey id, IDbTransaction trans = null)
        {
            return repository.DeleteAsync(id, trans);
        }


        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual Task<bool> DeleteAsync(T entity, IDbTransaction trans = null)
        {
            return repository.DeleteAsync(entity, trans);
        }

        /// <summary>
        /// 按主鍵批量刪除
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual bool DeleteBatch(IList<dynamic> ids, IDbTransaction trans = null)
        {
            return repository.DeleteBatch(ids, trans);
        }


        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual bool DeleteBatchWhere(string where, IDbTransaction trans = null)
        {
            return repository.DeleteBatchWhere(where, trans);
        }

        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteBatchWhereAsync(string where, IDbTransaction trans = null)
        {
            return await repository.DeleteBatchWhereAsync(where, trans);
        }
        /// <summary>
        /// 軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual bool DeleteSoft(bool bl, TKey id,string userId, IDbTransaction trans = null)
        {
            return repository.DeleteSoft(bl,id,userId, trans);
        }

        /// <summary>
        /// 非同步軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteSoftAsync(bool bl, TKey id, string userId, IDbTransaction trans = null)
        {
            return await repository.DeleteSoftAsync(bl,id,userId, trans);
        }
        /// <summary>
        /// 非同步批量軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            return await repository.DeleteSoftBatchAsync(bl, where, userId, trans);
        }
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public virtual T Get(TKey id)
        {
            return repository.Get(id);
        }

        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <returns></returns>
        public virtual TODto GetOutDto(TKey id)
        {
            return repository.Get(id).MapTo<TODto>();
        }

        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual T GetWhere(string where, IDbTransaction trans = null)
        {
            return repository.GetWhere(where);
        }
        /// <summary>
        /// 同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual TODto GetOutDtoWhere(string where, IDbTransaction trans = null)
        {
            return repository.GetWhere(where).MapTo<TODto>();
        }

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<T> GetWhereAsync(string where, IDbTransaction trans = null)
        {
            return await repository.GetWhereAsync(where);
        }

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<TODto> GetOutDtoWhereAsync(string where, IDbTransaction trans = null)
        {
            T info = await repository.GetWhereAsync(where);
            return info.MapTo<TODto>();
        }
        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetListTopWhere(int top, string where = null, IDbTransaction trans = null)
        {
            return repository.GetListTopWhere(top, where);
        }

        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetListTopWhereAsync(int top, string where = null, IDbTransaction trans = null)
        {
            return await repository.GetListTopWhereAsync(top, where);
        }
        /// <summary>
        /// 同步查詢所有實體。
        /// </summary>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll(IDbTransaction trans = null)
        {
            return repository.GetAll(trans);
        }

        /// <summary>
        /// 非同步步查詢所有實體。
        /// </summary>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual Task<IEnumerable<T>> GetAllAsync(IDbTransaction trans = null)
        {
            return  repository.GetAllAsync(trans);
        }

        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<T> GetAsync(TKey id)
        {
            return await repository.GetAsync(id);
        }
        /// <summary>
        /// 非同步查詢單個實體。
        /// </summary>
        /// <param name="id">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<TODto> GetOutDtoAsync(TKey id)
        {
            T info=await repository.GetAsync(id);
            return info.MapTo<TODto>();
        }
        ///<summary>
        /// 根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>

        public virtual IEnumerable<T> GetListWhere(string where = null, IDbTransaction trans = null)
        {
            return repository.GetListWhere(where, trans);
        }
        ///<summary>
        /// 非同步根據查詢條件查詢資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetListWhereAsync(string where = null, IDbTransaction trans = null)
        {
            return await repository.GetListWhereAsync(where, trans);
        }
        /// <summary>
        /// 同步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual long Insert(T entity, IDbTransaction trans = null)
        {
            return repository.Insert(entity, trans);
        }

        /// <summary>
        /// 非同步步新增實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual Task<long> InsertAsync(T entity, IDbTransaction trans = null)
        {
            return repository.InsertAsync(entity, trans);
        }
        /// <summary>
        /// 同步批量新增實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <returns></returns>
        public virtual void Insert(List<T> entities)
        {
            repository.Insert(entities);
        }
        /// <summary>
        /// 同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual bool Update(T entity, TKey id, IDbTransaction trans = null)
        {
            return repository.Update(entity,id,trans);
        }

        /// <summary>
        /// 同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual bool Update(List<T> entities, IDbTransaction trans = null)
        {
            return repository.Update(entities, trans);
        }

        /// <summary>
        /// 非同步更新實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual Task<bool> UpdateAsync(T entity, TKey id, IDbTransaction trans = null)
        {
            return repository.UpdateAsync(entity,id, trans);
        }

        /// <summary>
        /// 非同步批量更新實體。
        /// </summary>
        /// <param name="entities">實體集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual Task<bool> UpdateAsync(List<T> entities, IDbTransaction trans = null)
        {
            return repository.UpdateAsync(entities, trans);
        }

        /// <summary>
        /// 更新某一欄位值,欄位值字元型別
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值字元型別</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool UpdateTableField(string strField, string fieldValue, string where, IDbTransaction trans = null)
        {

            return repository.UpdateTableField(strField, fieldValue, where, trans);
        }
        /// <summary>
        /// 更新某一欄位值,欄位值字元型別
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值字元型別</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> UpdateTableFieldAsync(string strField, string fieldValue, string where, IDbTransaction trans = null)
        {
            return await repository.UpdateTableFieldAsync(strField, fieldValue, where, trans);
        }
        /// <summary>
        /// 更新某一欄位值,欄位值數字型別
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool UpdateTableField(string strField, int fieldValue, string where, IDbTransaction trans = null)
        {
            return repository.UpdateTableField(strField, fieldValue, where, trans);
        }

        /// <summary>
        /// 更新某一欄位值,欄位值數字型別
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> UpdateTableFieldAsync(string strField, int fieldValue, string where, IDbTransaction trans = null)
        {
            return await repository.UpdateTableFieldAsync(strField, fieldValue, where, trans);
        }
        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsDeleteMark(string where = null, IDbTransaction trans = null)
        {
            return repository.GetAllByIsDeleteMark(where, trans);
        }

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotDeleteMark(string where = null, IDbTransaction trans = null)
        {
            return repository.GetAllByIsNotDeleteMark(where, trans);
        }

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsEnabledMark(string where = null, IDbTransaction trans = null)
        {
            return repository.GetAllByIsEnabledMark(where, trans);
        }

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotEnabledMark(string where = null, IDbTransaction trans = null)
        {
            return repository.GetAllByIsNotEnabledMark(where, trans);
        }
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotDeleteAndEnabledMark(string where = null, IDbTransaction trans = null)
        {
            return repository.GetAllByIsNotDeleteAndEnabledMark(where, trans);
        }

        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsDeleteMarkAsync(string where = null, IDbTransaction trans = null)
        {
            return await repository.GetAllByIsDeleteMarkAsync(where, trans);
        }

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotDeleteMarkAsync(string where = null, IDbTransaction trans = null)
        {
            return await repository.GetAllByIsNotDeleteMarkAsync(where, trans);
        }

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsEnabledMarkAsync(string where = null, IDbTransaction trans = null)
        {
            return await repository.GetAllByIsEnabledMarkAsync(where, trans);
        }

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotEnabledMarkAsync(string where = null, IDbTransaction trans = null)
        {
            return await repository.GetAllByIsNotEnabledMarkAsync(where, trans);
        }

        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotDeleteAndEnabledMarkAsync(string where = null, IDbTransaction trans = null)
        {
            return await repository.GetAllByIsNotDeleteAndEnabledMarkAsync(where, trans);
        }


        /// <summary>
        /// 設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual bool SetEnabledMark(bool bl, TKey id,string userId=null, IDbTransaction trans = null)
        {
            return repository.SetEnabledMark(bl, id, userId, trans);
        }

        /// <summary>
        /// 非同步設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="id">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<bool> SetEnabledMarkAsync(bool bl, TKey id, string userId = null, IDbTransaction trans = null)
        {
            return await repository.SetEnabledMarkAsync(bl, id,userId, trans);
        }


        /// <summary>
        /// 非同步按條件設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            return await this.repository.SetEnabledMarkByWhereAsync(bl, where, userId, trans);
        }
        public virtual async Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null)
        {
            return await this.repository.SetEnabledMarkByWhereAsync(bl, where, paramparameters, userId, trans);
        }
        public virtual async Task<bool> SetEnabledMarkByWhereAsync(string setfield, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null)
        {
            return await this.repository.SetEnabledMarkByWhereAsync(setfield, where, paramparameters, userId, trans);
        }
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, IDbTransaction trans = null)
        {
            return repository.FindWithPager(condition, info, trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null)
        {
            return repository.FindWithPager(condition, info, fieldToSort, trans);
        }
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">是否降序</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            return repository.FindWithPager(condition, info, fieldToSort, desc, trans);
        }

        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual List<T> FindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            return repository.FindWithPagerSql(condition, info, fieldToSort, desc,  trans);
        }


        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            return await repository.FindWithPagerAsync(condition, info, fieldToSort, desc, trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// 查詢條件變換時請重寫該方法。
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定物件的集合</returns>
        public virtual PageResult<TODto> FindWithPager(SearchInputDto<T> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege();
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<T> list =repository.FindWithPager(where, pagerInfo, search.Sort, order);
            PageResult<TODto> pageResult = new PageResult<TODto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<TODto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }


        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// 查詢條件變換時請重寫該方法。
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<PageResult<TODto>> FindWithPagerAsync(SearchInputDto<T> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege();
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<T> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
            PageResult<TODto> pageResult = new PageResult<TODto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<TODto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null)
        {
            return await repository.FindWithPagerAsync(condition, info, fieldToSort,trans);
        }


        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, IDbTransaction trans = null)
        {
            return await repository.FindWithPagerAsync(condition, info, trans);
        }

        /// <summary>
        /// 分頁查詢，自行封裝sql語句
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> FindWithPagerSqlAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            return await repository.FindWithPagerAsync(condition, info,fieldToSort,desc, trans);
        }

        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        public virtual async Task<List<object>> FindWithPagerRelationUserAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            return await repository.FindWithPagerRelationUserAsync(condition, info, fieldToSort, desc, trans);
        }

        /// <summary>
        /// 分頁查詢包含使用者資訊
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表欄位
        /// 使用者資訊主要有使用者帳號：Account、別名：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        public virtual  List<object> FindWithPagerRelationUser(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            return repository.FindWithPagerRelationUser(condition, info, fieldToSort, desc, trans);
        }
        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        public virtual int GetCountByWhere(string condition)
        {
            return repository.GetCountByWhere(condition);
        }

        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<int> GetCountByWhereAsync(string condition)
        {
            return await repository.GetCountByWhereAsync(condition);
        }

        /// <summary>
        /// 根據條件查詢獲取某個欄位的最大值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        public virtual async Task<int> GetMaxValueByFieldAsync(string strField, string where, IDbTransaction trans = null)
        {
            return await repository.GetMaxValueByFieldAsync(strField,where);
        }

        /// <summary>
        /// 根據條件統計某個欄位之和,sum(欄位)
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        public virtual async Task<int> GetSumValueByFieldAsync(string strField, string where, IDbTransaction trans = null)
        {
            return await repository.GetSumValueByFieldAsync(strField, where);
        }

        /// <summary>
        /// 多表操作--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        public virtual async Task<Tuple<bool, string>> ExecuteTransactionAsync(List<Tuple<string, object>> trans, int? commandTimeout = null)
        {
            return await repository.ExecuteTransactionAsync(trans, commandTimeout);
        }
        /// <summary>
        /// 多表操作--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        public virtual Tuple<bool, string> ExecuteTransaction(List<Tuple<string, object>> trans, int? commandTimeout = null)
        {
            return  repository.ExecuteTransaction(trans, commandTimeout);
        }
        /// <summary>
        /// 獲取目前登錄使用者的資料職位問許可權
        /// </summary>
        /// <param name="blDeptCondition">是否開啟，預設開啟</param>
        /// <returns></returns>
        protected virtual string GetDataPrivilege(bool blDeptCondition=true)
        {
            string where = "1=1";
            //開許可權資料過濾
            if (blDeptCondition)
            {
                var identities =HttpContextHelper.HttpContext.User.Identities;
                var claimsIdentity = identities.First<ClaimsIdentity>();
                List<Claim> claimlist = claimsIdentity.Claims as List<Claim>;
                AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                if (claimlist[1].Value != "admin")
                {
                    //如果公司過濾條件不為空，那麼需要進行過濾
                    List<String> list = JsonSerializer.Deserialize<List<String>>(AurocoreCacheHelper.Get("User_RoleData_" + claimlist[0].Value).ToJson());
                    string DataFilterCondition = String.Join(",", list.ToArray());
                    if (!string.IsNullOrEmpty(DataFilterCondition))
                    {
                        where += string.Format(" and (DeptId in ('{0}') or CreatorUserId='{1}')", DataFilterCondition.Replace(",", "','"), claimlist[0].Value);
                    }
                    bool isMultiTenant = Configs.GetConfigurationValue("AppSetting", "IsMultiTenant").ToBool();
                    if (isMultiTenant)
                    {
                        where += string.Format(" and TenantId='{0}'", claimlist[3].Value);
                    }
                }
            }
            return where;
        }
        #region IDisposable Support
        private bool disposedValue = false; // 要檢測冗餘呼叫
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 釋放託管狀態(託管物件)。
                }

                // TODO: 釋放未託管的資源(未託管的物件)並在以下內容中替代終結器。
                // TODO: 將大型欄位設定為 null。

                disposedValue = true;
            }
        }

        // TODO: 僅當以上 Dispose(bool disposing) 擁有用於釋放未託管資源的程式碼時才替代終結器。
        // ~BaseService() {
        //   // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
        //   Dispose(false);
        // }

        /// <summary>
        /// 新增此程式碼以正確實現可處置模式
        /// </summary>
        public void Dispose()
        {
            // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
            Dispose(true);
            // TODO: 如果在以上內容中替代了終結器，則取消註釋以下行。
            // GC.SuppressFinalize(this);
        }




        #endregion
    }
}
