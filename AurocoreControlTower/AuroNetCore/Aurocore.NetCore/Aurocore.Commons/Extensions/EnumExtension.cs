﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Aurocore.Commons.Extensions
{
    /// <summary>
    /// Enum 擴充
    /// </summary>
    public static class EnumExtension
    {
       /// <summary>
       /// 取得 Enum 顯示文字
       /// </summary>
       /// <param name="enumValue"></param>
       /// <returns></returns>
        public static string GetDisplayName(this Enum enumValue)
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<DisplayAttribute>()
                            .GetName();
        }
    }
    
}
