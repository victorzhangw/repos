﻿using System;
using Aurocore.Commons.IRepositories;
using {ModelsNamespace};

namespace {IRepositoriesNamespace}
{
    /// <summary>
    /// 定義{TableNameDesc}倉儲接口
    /// </summary>
    public interface I{ModelTypeName}Repository:IRepository<{ModelTypeName}, {KeyTypeName}>
    {
    }
}