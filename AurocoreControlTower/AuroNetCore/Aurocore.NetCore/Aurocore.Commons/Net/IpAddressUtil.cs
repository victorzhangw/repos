using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;


namespace Aurocore.Commons.Net
{
    /// <summary>
    /// IP地址
    /// </summary>
    public class IpAddressUtil
    {
        /// <summary>
        /// Ip地址段是否包含另外一個IP地址
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        public static bool ContainsIp(string rule, string clientIp)
        {
            var ip = ParseIp(clientIp);

            var range = new IpAddressRange(rule);
            if (range.Contains(ip))
            {
                return true;
            }

            return false;
        }
        /// <summary>
        /// Ip地址集合是否包含另外一個IP地址
        /// </summary>
        /// <param name="ipRules">Ip地址集合List</param>
        /// <param name="clientIp"></param>
        /// <returns></returns>
        public static bool ContainsIp(List<string> ipRules, string clientIp)
        {
            var ip = ParseIp(clientIp);
            if (ipRules != null && ipRules.Any())
            {
                foreach (var rule in ipRules)
                {
                    var range = new IpAddressRange(rule);
                    if (range.Contains(ip))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        /// <summary>
        /// Ip地址集合是否包含另外一個IP地址
        /// </summary>
        /// <param name="ipRules"></param>
        /// <param name="clientIp"></param>
        /// <param name="rule"></param>
        /// <returns></returns>
        public static bool ContainsIp(List<string> ipRules, string clientIp, out string rule)
        {
            rule = null;
            var ip = ParseIp(clientIp);
            if (ipRules != null && ipRules.Any())
            {
                foreach (var r in ipRules)
                {
                    var range = new IpAddressRange(r);
                    if (range.Contains(ip))
                    {
                        rule = r;
                        return true;
                    }
                }
            }

            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static IPAddress ParseIp(string ipAddress)
        {
            return IPAddress.Parse(ipAddress);
        }

        /// <summary>
        /// 是否為ip
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
        
    }
}
