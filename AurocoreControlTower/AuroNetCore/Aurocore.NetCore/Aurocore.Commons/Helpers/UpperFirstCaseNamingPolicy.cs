using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Aurocore.Commons.Extend;

namespace Aurocore.Commons.Helpers
{
    /// <summary>
    /// 首字母大寫
    /// </summary>
    public class UpperFirstCaseNamingPolicy:JsonNamingPolicy
    {
        public override string ConvertName(string name) =>
            name.UpperFirst();
    }
}
