﻿using Aurocore.Commons.Encrypt;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Dapper;
using Microsoft.Data.Sqlite;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using System.Data.SqlClient;
using System.Data;

namespace Aurocore.Commons.Helpers
{
    /// <summary>
    /// 共享連線
    /// </summary>
   public sealed class OpenSharedConnectionHelper
    {
        #region 初始化
        /// <summary>
        /// 連線字串
        /// </summary>
        internal static string DefaultSqlConnectionString { get; set; }

        private static DbConnection dbConnection;
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        protected static string dbConfigName = "";
        /// <summary>
        /// 實例化
        /// </summary>
        public OpenSharedConnectionHelper()
        {

        }
        #endregion
        /// <summary>
        /// 開啟資料庫連線（含外部連線）
        /// </summary>
        /// <param name="DBConfing"> Appsetting 資料庫連線字段</param>
        /// <param name="DBType"> Appsetting 資料庫類別</param>
        public static DbConnection OpenConnection(string DBConfing="",string DBType="")
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            object connCode = AurocoreCacheHelper.Get(DBConfing);
            string dbType = "";
            if (connCode != null)
            {
                DefaultSqlConnectionString = connCode.ToString();
                dbType = AurocoreCacheHelper.Get(DBType).ToString().ToUpper();
            }
            else
            {
                string conStringEncrypt = Configs.GetConfigurationValue("AppSetting", "ConStringEncrypt");
                if (string.IsNullOrEmpty(dbConfigName))
                {
                    dbConfigName = Configs.GetConfigurationValue("AppSetting", "DefaultDataBase");
                }
                DefaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
                if (conStringEncrypt == "true")
                {
                    DefaultSqlConnectionString = DEncrypt.Decrypt(DefaultSqlConnectionString);
                }
                dbType = dbConfigName.ToUpper();
                TimeSpan expiresSliding = DateTime.Now.AddMinutes(30) - DateTime.Now;
                AurocoreCacheHelper.Add("DBConfing", DefaultSqlConnectionString, expiresSliding, false);
                AurocoreCacheHelper.Add("DBType", dbType, expiresSliding, false);
            }
            dbType = dbConfigName.ToUpper();
            if (dbType.Contains("SQLSERVER"))
            {
                dbConnection = new SqlConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("MYSQL"))
            {
                dbConnection = new MySqlConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("ORACLE"))
            {
                dbConnection = new OracleConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("SQLITE"))
            {
                dbConnection = new SqliteConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("MEMORY"))
            {
                throw new NotSupportedException("In Memory Dapper Database Provider is not yet available.");
            }
            else if (dbType.Contains("NPGSQL"))
            {
                dbConnection = new NpgsqlConnection(DefaultSqlConnectionString);
            }
            else
            {
                throw new NotSupportedException("The database is not supported");
            }
            if (dbConnection.State != ConnectionState.Open)
            {
                dbConnection.Open();
            }
            return dbConnection;
        }
    }
}
