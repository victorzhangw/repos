using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Json;

namespace Aurocore.Commons.Helpers
{

    /// <summary>
    /// http請求類
    /// </summary>
    public class HttpHelper
    {
        private readonly HttpClient _httpClient;
        private  string _baseIPAddress;

        /// <param name="ipaddress">請求的基礎IP，例如：http://192.168.1.22:8081/ </param>
        public HttpHelper(string ipaddress = "")
        {
            this._baseIPAddress = ipaddress;
            _httpClient = new HttpClient { BaseAddress = new Uri(_baseIPAddress) };
        }

        /// <summary>
        /// 建立帶使用者資訊的請求客戶端
        /// </summary>
        /// <param name="userName">使用者帳號</param>
        /// <param name="pwd">使用者密碼，當WebApi端不要求密碼驗證時，可傳空串</param>
        /// <param name="uriString">The URI string.</param>
        public HttpHelper(string userName, string pwd = "", string uriString = "")
            : this(uriString)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                _httpClient.DefaultRequestHeaders.Authorization = CreateBasicCredentials(userName, pwd);
            }
        }

        /// <summary>
        /// Get請求資料
        /// <para>最終以url參數的方式提交</para>
        /// <para>yubaolee 2016-3-3 重構與post同樣非同步呼叫</para>
        /// </summary>
        /// <param name="parameters">參數字典,可為空</param>
        /// <param name="requestUri">例如/api/Files/UploadFile</param>
        /// <returns></returns>
        public string Get(Dictionary<string, string> parameters, string requestUri)
        {
            if (parameters != null)
            {
                var strParam = string.Join("&", parameters.Select(o => o.Key + "=" + o.Value));
                requestUri = string.Concat(ConcatURL(requestUri), '?', strParam);
            }
            else
            {
                requestUri = ConcatURL(requestUri);
            }

            var result = _httpClient.GetStringAsync(requestUri);
            return result.Result;
        }

        /// <summary>
        /// Get請求資料
        /// <para>最終以url參數的方式提交</para>
        /// </summary>
        /// <param name="parameters">參數字典</param>
        /// <param name="requestUri">例如/api/Files/UploadFile</param>
        /// <returns>實體物件</returns>
        public T Get<T>(Dictionary<string, string> parameters, string requestUri) where T : class
        {
            string jsonString = Get(parameters, requestUri);
            if (string.IsNullOrEmpty(jsonString))
                return null;

            return JsonHelper.ToObject<T>(jsonString);
        }

        /// <summary>
        /// 同步GET請求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">請求響應超時時間，單位/s(預設100秒)</param>
        /// <returns></returns>
        public  string HttpGet(string url, Dictionary<string, string> headers = null, int timeout = 0)
        {
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    _httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            if (timeout > 0)
            {
                _httpClient.Timeout = new TimeSpan(0, 0, timeout);
            }
            var result = _httpClient.GetAsync(ConcatURL(url));
            Byte[] resultBytes = result.Result.Content.ReadAsByteArrayAsync().Result;
            return Encoding.UTF8.GetString(resultBytes);
        }

        /// <summary>
        /// 非同步GET請求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <param name="timeout">請求響應超時時間，單位/s(預設100秒)</param>
        /// <returns></returns>
        public  async Task<string> HttpGetAsync(string url, Dictionary<string, string> headers = null, int timeout = 0)
        {
            if (headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    _httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            if (timeout > 0)
            {
                _httpClient.Timeout = new TimeSpan(0, 0, timeout);
            }
            //_httpClient.DefaultRequestHeaders.Add("content-type", "application/json");
            var result = await _httpClient.GetAsync(ConcatURL(url));
            Byte[] resultBytes = result.Content.ReadAsByteArrayAsync().Result;
            return Encoding.UTF8.GetString(resultBytes);
        }
        /// <summary>
        /// 以json的方式Post資料 返回string型別
        /// <para>最終以json的方式放置在http體中</para>
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="requestUri">例如/api/Files/UploadFile</param>
        /// <param name="headers"></param>
        /// <param name="contentType"></param>
        /// <param name="timeout">請求響應超時時間，單位/s(預設100秒)</param>
        /// <param name="encoding">預設UTF8</param>
        /// <returns></returns>
        public string Post(object entity, string requestUri, Dictionary<string, string> headers = null, string contentType = "application/json", int timeout = 0, Encoding encoding = null)
        {
            string request = string.Empty;
            if (entity != null)
                request = JsonConvert.SerializeObject(entity);
            if(headers != null)
            {
                foreach (KeyValuePair<string, string> header in headers)
                {
                    _httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            if (timeout > 0)
            {
                _httpClient.Timeout = new TimeSpan(0, 0, timeout);
            }
            HttpContent httpContent = new StringContent(request ?? "", encoding ?? Encoding.UTF8);
            if (contentType != null)
            {
                httpContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(contentType);
            }
            return Post(requestUri, httpContent);
        }


        /// <summary>
        /// Post Dic資料
        /// <para>最終以formurlencode的方式放置在http體中</para>
        /// </summary>
        /// <returns>System.String.</returns>
        public string PostDic(Dictionary<string, string> temp, string requestUri)
        {
            HttpContent httpContent = new FormUrlEncodedContent(temp);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            return Post(requestUri, httpContent);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        public string PostByte(byte[] bytes, string requestUrl)
        {
            HttpContent content = new ByteArrayContent(bytes);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return Post(requestUrl, content);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        private string Post(string requestUrl, HttpContent content)
        {
            var result = _httpClient.PostAsync(ConcatURL(requestUrl),content);
            Byte[] resultBytes = result.Result.Content.ReadAsByteArrayAsync().Result;
            return Encoding.UTF8.GetString(resultBytes);
            //return result.Result.Content.ReadAsStringAsync().Result;
        }

        /// <summary>
        /// 把請求的URL相對路徑組合成絕對路徑
        /// </summary>
        private string ConcatURL(string requestUrl)
        {
            return new Uri(_httpClient.BaseAddress, requestUrl).OriginalString;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private AuthenticationHeaderValue CreateBasicCredentials(string userName, string password)
        {
            string toEncode = userName + ":" + password;
            Encoding encoding = Encoding.GetEncoding("utf-8");
            byte[] toBase64 = encoding.GetBytes(toEncode);
            string parameter = Convert.ToBase64String(toBase64);

            return new AuthenticationHeaderValue("Basic", parameter);
        }
    }
}
