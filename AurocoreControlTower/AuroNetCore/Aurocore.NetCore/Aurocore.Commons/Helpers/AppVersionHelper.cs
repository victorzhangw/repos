using System;
using System.Collections.Generic;
using System.Text;

namespace Aurocore.Commons.Helpers
{
    /// <summary>
    /// 應用版本號、軟體廠商等資訊
    /// </summary>
   public class AppVersionHelper
    {
        /// <summary>
        /// 版本號
        /// </summary>
        public const string Version = "3.0";
        /// <summary>
        /// 軟體廠商
        /// </summary>
        public const string Manufacturer = "特聿科技行銷股份有限公司";
        /// <summary>
        /// 網站地址
        /// </summary>
        public const string WebSite = "https://www.aurocore.net";
        /// <summary>
        /// 更新地址
        /// </summary>
        public const string UpdateUrl = "https://www.aurocore.net";
    }
}
