﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Aurocore.Commons.Helpers
{
    /// <summary>
    /// 物件匯出
    /// </summary>
    public class ObjectDump
    {
        private static string Dump(object o)
        {
            string json = JsonConvert.SerializeObject(o, Formatting.Indented);
            return json;
        }
    }
}
