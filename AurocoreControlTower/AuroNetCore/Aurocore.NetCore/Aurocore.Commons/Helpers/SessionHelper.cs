using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Aurocore.Commons.Extensions;

namespace Aurocore.Commons.Helpers
{
    /// <summary>
    /// Session幫助類，可在非controler中讀取或儲存session
    /// </summary>
    public static class SessionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static HttpContext HttpHelper => HttpContextHelper.HttpContext;
        /// <summary>
        /// 設定 Session
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetSession(string key, object value)
        {
            HttpHelper.Session.Set(key, value);
        }

        /// <summary>
        /// 獲取 Session
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetSession<T>(string key)
        {
            return HttpHelper.Session.Get<T>(key);
        }

        /// <summary>
        /// 獲取 Session
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetString(string key)
        {
            return HttpHelper.Session.GetString(key);
        }
    }
}
