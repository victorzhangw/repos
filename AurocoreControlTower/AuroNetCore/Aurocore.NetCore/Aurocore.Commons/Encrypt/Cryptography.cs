using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.Commons
{
    /// <summary>
    /// 用於 webapi 產生 ticket 使用，公開
    /// </summary>
    public sealed class Cryptography
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tmpStr"></param>
        /// <param name="encodingAESKey"></param>
        /// <returns></returns>
        public static string AES_encrypt(string tmpStr, string encodingAESKey)
        {
            string str = tmpStr + GetMD5_32(encodingAESKey);
            return GetMD5_32(SHA256(str)) + GetMD5_32(tmpStr);
        }
        /// <summary>
        /// SHA256函式
        /// </summary>
        /// <param name="str">原始字串</param>
        /// <returns>SHA256結果(返回長度為44位元組的字串)</returns>
        public static string SHA256(string str)
        {
            byte[] SHA256Data = Encoding.UTF8.GetBytes(str);
            SHA256Managed Sha256 = new SHA256Managed();
            byte[] Result = Sha256.ComputeHash(SHA256Data);
            return Convert.ToBase64String(Result);  //返回長度為44位元組的字串
        }



        /// <summary>
        /// 獲得32位的MD5加密
        /// </summary>
        public static string GetMD5_32(string input)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }

            return sb.ToString();
        }
        /// <summary>
        /// 驗證key和iv都必須為128bits或192bits或256bits
        /// </summary>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        private static void Validate_KeyIV_Length(string key, string iv)
        {

            List<int> LegalSizes = new List<int>() { 128, 192, 256 };
            int keyBitSize = Encoding.UTF8.GetBytes(key).Length * 8;
            int ivBitSize = Encoding.UTF8.GetBytes(iv).Length * 8;
            if (!LegalSizes.Contains(keyBitSize) || !LegalSizes.Contains(ivBitSize))
            {
                throw new Exception($@"key或iv的長度不在128bits、192bits、256bits其中一個，輸入的key bits:{keyBitSize},iv bits:{ivBitSize}");
            }
        }
        /// <summary>
        /// 加密後回傳base64String，
        /// 如果key和iv忘記遺失的話，資料就解密不回來
        /// base64String若使用在Url的話，Web端記得做UrlEncode
        /// </summary>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <param name="plain_text"></param>
        /// <returns></returns>
        public static string AesCBCEncrypt(string key, string iv, string plain_text)
        {


            Validate_KeyIV_Length(key, iv);
                
            byte[] keyArray = Convert.FromBase64String(key);
            byte[] ivArray = Convert.FromBase64String(iv);
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(plain_text);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.IV = ivArray;
            rDel.Mode = CipherMode.CBC;

            rDel.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        /// <summary>
        /// 解密後，回傳明碼文字
        /// </summary>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static string AesCBCDecrypt(string key, string iv, string base64String)
        {


            Validate_KeyIV_Length(key, iv);
            /*
            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;//非必須，但加了較安全
            aes.Padding = PaddingMode.PKCS7;//非必須，但加了較安全
            var _bytekey = Convert.FromBase64String(key);
            var _byteiv = Convert.FromBase64String(iv);
           // ICryptoTransform transform = aes.CreateDecryptor(Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(iv));
            ICryptoTransform transform = aes.CreateDecryptor(_bytekey,_byteiv);
            byte[] bEnBase64String = null;
            byte[] outputData = null;
            try
            {
                bEnBase64String = Convert.FromBase64String(base64String);//有可能base64String格式錯誤

                outputData = transform.TransformFinalBlock(bEnBase64String, 0, bEnBase64String.Length);//有可能解密出錯
            }
            catch (Exception ex)
            {
                //todo 寫Log
                throw new Exception($@"解密出錯:{ex.Message}");
            }

            //解密成功
            return Encoding.UTF8.GetString(outputData);*/
            var rgbKey = Convert.FromBase64String(key);
            var rgbIV = Convert.FromBase64String(iv);
            var buff = Convert.FromBase64String(base64String);

            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = rgbKey;
            rDel.IV = rgbIV;
            rDel.Mode = CipherMode.CBC;
            rDel.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = rDel.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(buff, 0, buff.Length);
            return UTF8Encoding.UTF8.GetString(resultArray);


        }
            

        }


    }

