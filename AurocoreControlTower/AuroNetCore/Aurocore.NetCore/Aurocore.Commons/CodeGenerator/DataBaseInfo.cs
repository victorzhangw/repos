using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.Commons.CodeGenerator
{
    /// <summary>
    /// 資料庫模型
    /// </summary>
    public class DataBaseInfo
    {

        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DbName { get; set; }
    }
}
