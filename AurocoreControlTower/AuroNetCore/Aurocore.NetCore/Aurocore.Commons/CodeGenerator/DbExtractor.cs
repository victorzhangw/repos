using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Pages;

namespace Aurocore.Commons.CodeGenerator
{
    public class DbExtractor:IDbExtractor
    {
        public string dbType="";
        public string dbName = "";

        public DbExtractor()
        {
            MssqlExtractor mssqlExtractor = new MssqlExtractor();
            mssqlExtractor.OpenSharedConnection();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            object odbty = AurocoreCacheHelper.Get("CodeGeneratorDbType");
            if (odbty != null)
                dbType = odbty.ToString().ToUpper();
            object odbn = AurocoreCacheHelper.Get("CodeGeneratorDbName");
            if (odbn != null)
                dbName = odbn.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<DataBaseInfo> GetAllDataBases()
        {
            List<DataBaseInfo> list = new List<DataBaseInfo>();
            if (dbType.Contains("SQLSERVER"))
            {
                MssqlExtractor mssqlExtractor = new MssqlExtractor();
                list= mssqlExtractor.GetAllDataBases();
            }
            else if (dbType.Contains("MYSQL"))
            {
                MySqlExtractor mssqlExtractor = new MySqlExtractor();
                list= mssqlExtractor.GetAllDataBases();
            }
            return list;
        }
        /// <summary>
        /// 獲取資料所有表資訊
        /// </summary>
        /// <param name="tablelist">資料庫表名稱</param>
        /// <returns></returns>
        public List<DbTableInfo> GetWhereTables(string tablelist = null)
        {
            List<DbTableInfo> list = new List<DbTableInfo>();
            if (dbType.Contains("SQLSERVER"))
            {
                MssqlExtractor mssqlExtractor = new MssqlExtractor();
                list = mssqlExtractor.GetAllTables(tablelist);
            }
            else if (dbType.Contains("MYSQL"))
            {
                MySqlExtractor mssqlExtractor = new MySqlExtractor();
                list = mssqlExtractor.GetAllTables(this.dbName,tablelist);
            }
            return list;
        }

        public List<DbTableInfo> GetTablesWithPage(string strwhere, string fieldNameToSort, bool isDescending, PagerInfo info)
        {
            List<DbTableInfo> list = new List<DbTableInfo>();
            if (dbType.Contains("SQLSERVER"))
            {
                MssqlExtractor mssqlExtractor = new MssqlExtractor();
                list = mssqlExtractor.GetAllTables(strwhere, fieldNameToSort, isDescending, info);
            }
            else if (dbType.Contains("MYSQL"))
            {
                MySqlExtractor mysqlExtractor = new MySqlExtractor();
                list = mysqlExtractor.GetAllTables(this.dbName, strwhere, fieldNameToSort, isDescending, info);
            }
            return list;
        }

        /// <summary>
        /// 獲取表的所有欄位名及欄位型別
        /// </summary>
        /// <param name="tableName">資料表的名稱</param>
        /// <returns></returns>
        public List<DbFieldInfo> GetAllColumns(string tableName)
        {
            List<DbFieldInfo> list = new List<DbFieldInfo>();
            if (dbType.Contains("SQLSERVER"))
            {
                MssqlExtractor mssqlExtractor = new MssqlExtractor();
                list = mssqlExtractor.GetAllColumns(tableName);
            }
            else if (dbType.Contains("MYSQL"))
            {
                MySqlExtractor mysqlExtractor = new MySqlExtractor();
                list = mysqlExtractor.GetAllColumns(this.dbName, tableName);
            }
            return list;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
