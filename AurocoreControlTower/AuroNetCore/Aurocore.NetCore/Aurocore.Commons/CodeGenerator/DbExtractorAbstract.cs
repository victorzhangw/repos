using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Microsoft.Data.Sqlite;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Pages;

namespace Aurocore.Commons.CodeGenerator
{
    /// <summary>
    /// 實現資料庫抽取器基類
    /// </summary>
    public abstract class DbExtractorAbstract
    {

        #region 初始化
        /// <summary>
        /// 連線字串
        /// </summary>
        internal string DefaultSqlConnectionString { get; set; }

        private DbConnection dbConnection;
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        protected string dbConfigName = "";
        /// <summary>
        /// 實例化
        /// </summary>
        public DbExtractorAbstract()
        {

        }
        #endregion

        /// <summary>
        /// 資料庫連線,根據資料庫型別自動識別，型別區分用設定名稱是否包含主要關鍵字
        /// MSSQL、MYSQL、ORACLE、SQLITE、MEMORY、NPGSQL
        /// </summary>
        /// <returns></returns>
        public DbConnection OpenSharedConnection()
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            object connCode = AurocoreCacheHelper.Get("CodeGeneratorDbConn");
            string dbType = "";
            if (connCode!=null)
            {
                DefaultSqlConnectionString = connCode.ToString();
                dbType=AurocoreCacheHelper.Get("CodeGeneratorDbType").ToString().ToUpper();
            }
            else
            {
                string conStringEncrypt = Configs.GetConfigurationValue("AppSetting", "ConStringEncrypt");
                if (string.IsNullOrEmpty(dbConfigName))
                {
                    dbConfigName = Configs.GetConfigurationValue("AppSetting", "DefaultDataBase");
                }
                DefaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
                if (conStringEncrypt == "true")
                {
                    
                    DefaultSqlConnectionString = DEncrypt.Decrypt(DefaultSqlConnectionString);
                }
                dbType = dbConfigName.ToUpper();
                TimeSpan expiresSliding = DateTime.Now.AddMinutes(30) - DateTime.Now;
                AurocoreCacheHelper.Add("CodeGeneratorDbConn", DefaultSqlConnectionString, expiresSliding, false);
                AurocoreCacheHelper.Add("CodeGeneratorDbType", dbType, expiresSliding, false);
            }
            if (dbType.Contains("SQLSERVER"))
            {
                dbConnection = new SqlConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("MYSQL"))
            {
                dbConnection = new MySqlConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("ORACLE"))
            {
                dbConnection = new OracleConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("SQLITE"))
            {
                dbConnection = new SqliteConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("MEMORY"))
            {
                throw new NotSupportedException("In Memory Dapper Database Provider is not yet available.");
            }
            else if (dbType.Contains("NPGSQL"))
            {
                dbConnection = new NpgsqlConnection(DefaultSqlConnectionString);
            }
            else
            {
                throw new NotSupportedException("The database is not supported");
            }
            if (dbConnection.State != ConnectionState.Open)
            {
                dbConnection.Open();
            }
            return dbConnection;
        }



        #region 資訊抽取

        /// <summary>
        /// 獲取資料庫的所有表的資訊，
        /// 請定義TABLE_NAME 和 COMMENTS 欄位的指令碼
        /// </summary>
        /// <param name="sql">具體的指令碼</param>
        /// <returns></returns>
        protected List<DbTableInfo> GetAllTablesInternal(string sql)
        {
            var list = new List<DbTableInfo>();
            using (DbConnection conn = OpenSharedConnection())
            {
                list = conn.Query<DbTableInfo>(sql).ToList();
            }
            return list;
        }
        /// <summary>
        /// 獲取所有資料庫資訊，
        /// </summary>
        /// <param name="sql">具體的指令碼</param>
        /// <returns></returns>
        protected List<DataBaseInfo> GetAllDataBaseInternal(string sql)
        {
            var list = new List<DataBaseInfo>();
            using (DbConnection conn = OpenSharedConnection())
            {
                list = conn.Query<DataBaseInfo>(sql).ToList();
            }
            return list;
        }


        /// <summary>
        /// 獲取資料庫的所有表的資訊，
        /// 請定義TABLE_NAME 和 COMMENTS 欄位的指令碼
        /// </summary>
        /// <param name="sql">具體的指令碼</param>
        /// <param name="info">分頁資訊</param>
        /// <returns></returns>
        protected List<DbTableInfo> GetAllTablesInternal(string sql, PagerInfo info)
        {
            var list = new List<DbTableInfo>();
            using (DbConnection conn = OpenSharedConnection())
            {
                var reader = conn.QueryMultiple(sql);
                info.RecordCount = reader.ReadFirst<int>();
                list = reader.Read<DbTableInfo>().AsList();
            }
            return list;
        }
        /// <summary>
        /// 獲取表的所有欄位名及欄位型別
        /// </summary>
        /// <returns></returns>
        protected List<DbFieldInfo> GetAllColumnsInternal(string sql)
        {
            List<DbFieldInfo> list = new List<DbFieldInfo>();
            using (DbConnection conn = OpenSharedConnection())
            {
               IEnumerable<dynamic> dlist = conn.Query(sql);
                foreach(var item in dlist)
                {
                    DbFieldInfo dbFieldInfo = new DbFieldInfo
                    {
                        FieldName = item.FieldName,
                        //Increment = item.Increment == "1" ? true : false,
                        IsIdentity = item.IsIdentity == "1" ? true : false,
                        FieldType = item.FieldType.ToString(),
                        DataType = item.FieldType.ToString(),
                        FieldMaxLength = item.FieldMaxLength,
                        FieldPrecision = item.FieldPrecision,
                        FieldScale = item.FieldScale,
                        IsNullable = item.IsNullable == "1" ? true : false,
                        FieldDefaultValue = item.FieldDefaultValue,
                        Description = item.Description
                    };
                    list.Add(dbFieldInfo);
                }
            }
            return list;
        }

        #endregion
    }
}
