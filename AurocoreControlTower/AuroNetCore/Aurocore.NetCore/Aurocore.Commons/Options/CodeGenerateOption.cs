namespace Aurocore.Commons.Options
{
    /// <summary>
    /// 程式碼產生器設定
    /// </summary>
    public class CodeGenerateOption
    {

        /// <summary>
        /// 專案名稱空間
        /// </summary>
        public string BaseNamespace { get; set; }
        /// <summary>
        /// 資料實體名稱空間
        /// </summary>
        public string ModelsNamespace { get; set; }
        /// <summary>
        /// 輸入輸出資料實體名稱空間
        /// </summary>
        public string DtosNamespace { get; set; }
        /// <summary>
        /// 倉儲介面名稱空間
        /// </summary>
        public string IRepositoriesNamespace { get; set; }
        /// <summary>
        /// 倉儲實現名稱空間
        /// </summary>
        public string RepositoriesNamespace { get; set; }
        /// <summary>
        /// 服務介面名稱空間
        /// </summary>
        public string IServicsNamespace { get; set; }
        /// <summary>
        /// 服務介面實現名稱空間
        /// </summary>
        public string ServicesNamespace { get; set; }

        /// <summary>
        /// Api控制器名稱空間
        /// </summary>
        public string ApiControllerNamespace { get; set; }

        /// <summary>
        /// 去掉的表頭字元
        /// </summary>
        public string ReplaceTableNameStr { get; set; }
        /// <summary>
        /// 要生資料的表，用「，」分割
        /// </summary>
        public string TableList { get; set; }
    }
}
