using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Aurocore.Commons.Core.Dapper;

namespace Aurocore.Commons.Attributes
{
    /// <summary>
    /// 工作單元事務提交攔截器
    /// </summary>
    public class UnitOfWorkIInterceptor : IInterceptor
    {
        private DapperDbContext dBContext;
        public UnitOfWorkIInterceptor(DapperDbContext dBContext)
        {
            this.dBContext = dBContext;
        }
        public void Intercept(IInvocation invocation)
        {
            MethodInfo methodInfo = invocation.MethodInvocationTarget;
            if (methodInfo == null)
                methodInfo = invocation.Method;

            UnitOfWorkAttribute transaction = methodInfo.GetCustomAttributes<UnitOfWorkAttribute>(true).FirstOrDefault();
            //如果標記了 [UnitOfWork]，並且不在事務巢狀中。
            if (transaction != null && dBContext.Committed)
            {
                //開啟事務
                dBContext.BeginTransaction();
                try
                {
                    //事務包裹 查詢語句 
                    //https://github.com/mysql-net/MySqlConnector/issues/405
                    invocation.Proceed();
                    //提交事務
                    dBContext.CommitTransaction();
                }
                catch (Exception ex)
                {
                    //回滾
                    dBContext.RollBackTransaction();
                    throw;
                }
            }
            else
            {
                //如果沒有標記[UnitOfWork]，直接執行方法
                invocation.Proceed();
            }
        }
    }
}
