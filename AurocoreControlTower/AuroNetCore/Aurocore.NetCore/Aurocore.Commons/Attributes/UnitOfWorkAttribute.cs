using System;
using System.Collections.Generic;
using System.Text;

namespace Aurocore.Commons.Attributes
{
    /// <summary>
    /// 工作單元
    /// 僅用來做特性標記 
    /// </summary>
    public class UnitOfWorkAttribute : Attribute
    {

    }
}
