using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace Aurocore.Commons.IoC
{
    /// <summary>
    /// 服務定位器。
    /// </summary>
    public static class ServiceLocator
    {
        #region Others

        /// <summary>
        /// .NET CORE 內建服務提供者引用。
        /// </summary>
        public static IServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// 手動獲取.NET CORE已註冊服務實例。
        /// 使用之前請確保在啟動類（Startup.cs）設定（Configure）方法中已經保持【app.ApplicationServices】引用。
        /// </summary>
        /// <typeparam name="T">型別</typeparam>
        /// <returns></returns>
        public static T GetService<T>()
        {
            if (ServiceProvider == null)
            {
                throw new ArgumentNullException(nameof(ServiceProvider));
            }
            return (T)ServiceProvider.GetService(typeof(T));
        }

        #endregion

        #region Fields

        /// <summary>
        /// 型別與實例型別的對映集合。
        /// </summary>
        private static readonly Dictionary<Type, Type> _mapping = new Dictionary<Type, Type>();

        /// <summary>
        /// 型別與已實例化物件對映集合。
        /// </summary>
        private static readonly Dictionary<Type, object> _resources = new Dictionary<Type, object>();

        /// <summary>
        /// 操作鎖。（待優化）
        /// 存在問題：lock機制會把其它執行緒鎖在外面，無論是讀還是寫，都會被鎖，效能比較低。
        /// 解決方案：執行緒安全可嘗試用ConcurrentDictionary代替。
        /// </summary>
        private static object _lock = new object();

        #endregion

        #region Add

        /// <summary>
        /// 新增註冊資源。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        /// <param name="instance">資源實例</param>
        public static void Add<T>(object instance) where T : class
        {
            Add(typeof(T), instance);
        }

        /// <summary>
        /// 新增註冊資源。
        /// </summary>
        /// <param name="type">資源型別</param>
        /// <param name="instance">資源實例</param>
        public static void Add(Type type, object instance)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            if (!type.IsInstanceOfType(instance))
            {
                throw new InvalidCastException(
                    string.Format(CultureInfo.InvariantCulture, "Resource does not implement supplied interface: {0}", type.FullName));
            }

            lock (_lock)
            {
                if (_resources.ContainsKey(type))
                {
                    throw new ArgumentException(
                        string.Format(CultureInfo.InvariantCulture, "Resource is already existing : {0}", type.FullName));
                }
                _resources[type] = instance;
            }
        }

        #endregion

        #region Get

        /// <summary>
        /// 查詢指定型別的資源實例。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        /// <returns>資源實例</returns>
        public static T Get<T>() where T : class
        {
            return Get(typeof(T)) as T;
        }

        /// <summary>
        /// 查詢指定型別的資源實例。
        /// </summary>
        /// <param name="type">The type of instance.</param>
        /// <returns>資源實例</returns>
        public static object Get(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            object resource;

            lock (_lock)
            {
                if (!_resources.TryGetValue(type, out resource))
                {
                    throw new KeyNotFoundException(type.FullName);
                }
            }

            if (resource == null)
            {
                throw new NullReferenceException(type.FullName);
            }

            return resource;
        }

        /// <summary>
        /// 嘗試查詢指定型別的資源實例。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        /// <param name="resource">資源實例</param>
        /// <returns>是否存在</returns>
        public static bool TryGet<T>(out T resource) where T : class
        {
            bool isFound = false;
            object target;
            resource = null;

            lock (_lock)
            {
                if (_resources.TryGetValue(typeof(T), out target))
                {
                    resource = target as T;
                    isFound = true;
                }
            }

            return isFound;
        }

        #endregion

        #region Register

        /// <summary>
        /// 註冊型別。
        /// </summary>
        /// <typeparam name="T">實體型別，型別限制為有公共無參建構函式</typeparam>
        public static void RegisterType<T>() where T : class, new()
        {
            lock (_lock)
            {
                _mapping[typeof(T)] = typeof(T);
            }
        }

        /// <summary>
        /// 註冊型別。
        /// </summary>
        /// <typeparam name="TFrom">資源型別</typeparam>
        /// <typeparam name="TTo">實體型別，型別限制為有公共無參建構函式</typeparam>
        public static void RegisterType<TFrom, TTo>()
            where TFrom : class
            where TTo : TFrom, new()
        {
            lock (_lock)
            {
                _mapping[typeof(TFrom)] = typeof(TTo);
                _mapping[typeof(TTo)] = typeof(TTo);
            }
        }

        /// <summary>
        /// 是否已註冊此型別。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        /// <returns>是否已註冊此型別</returns>
        public static bool IsRegistered<T>()
        {
            lock (_lock)
            {
                return _mapping.ContainsKey(typeof(T));
            }
        }

        #endregion

        #region Resolve

        /// <summary>
        /// 獲取型別實例。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        /// <returns>型別實例</returns>
        public static T Resolve<T>()
          where T : class
        {
            T resource = default(T);

            bool existing = TryGet<T>(out resource);
            if (!existing)
            {
                ConstructorInfo constructor = null;

                lock (_lock)
                {
                    if (!_mapping.ContainsKey(typeof(T)))
                    {
                        throw new KeyNotFoundException(
                          string.Format(CultureInfo.InvariantCulture, "Cannot find the target type : {0}", typeof(T).FullName));
                    }

                    Type concrete = _mapping[typeof(T)];
                    constructor = concrete.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, new Type[0], null);

                    if (constructor == null)
                    {
                        throw new NullReferenceException(
                          string.Format(CultureInfo.InvariantCulture, "Public constructor is missing for type : {0}", typeof(T).FullName));
                    }
                }

                Add<T>((T)constructor.Invoke(null));
            }

            return Get<T>();
        }

        #endregion

        #region Remove

        /// <summary>
        /// 移除指定型別的資源實例。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        public static void Remove<T>()
        {
            Teardown(typeof(T));
        }

        /// <summary>
        /// 移除指定型別的資源實例。
        /// </summary>
        /// <param name="type">資源型別</param>
        public static void Remove(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            lock (_lock)
            {
                _resources.Remove(type);
            }
        }

        #endregion

        #region Teardown

        /// <summary>
        /// 拆除指定型別的資源實例及註冊對映型別。
        /// </summary>
        /// <typeparam name="T">資源型別</typeparam>
        public static void Teardown<T>()
        {
            Teardown(typeof(T));
        }

        /// <summary>
        /// 拆除指定型別的資源實例及註冊對映型別。
        /// </summary>
        /// <param name="type">資源型別</param>
        public static void Teardown(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            lock (_lock)
            {
                _resources.Remove(type);
                _mapping.Remove(type);
            }
        }

        #endregion

        #region Clear

        /// <summary>
        /// 移除所有資源。
        /// </summary>
        public static void Clear()
        {
            lock (_lock)
            {
                _resources.Clear();
                _mapping.Clear();
            }
        }

        #endregion
    }
}
