using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;

namespace Aurocore.Commons.Pages
{
    /// <summary>
    /// 儲存分頁請求的結果。
    /// </summary>
    /// <typeparam name="T">返回結果集中的POCO型別</typeparam>
    public class PageResult<T>:CommonResult
    {
        public PageResult()
        {
        }
        public PageResult(bool success, string msg, object rows)
        {
            this.Success = success;
            this.ErrMsg = msg;
            this.ResData =rows ;
        }
        public PageResult(long currentPage, long totalItems, long itemsPerPage)
        {
            CurrentPage = currentPage;
            TotalItems = totalItems;
            ItemsPerPage = itemsPerPage;
        }

        public PageResult(long currentPage, long totalPages, long totalItems, long itemsPerPage, List<T> items, object context) : this(currentPage, totalPages, totalItems)
        {
            ItemsPerPage = itemsPerPage;
            Items = items;
            Context = context;
        }

        /// <summary>
        /// 目前頁碼。
        /// </summary>
        public long CurrentPage { get; set; }

        /// <summary>
        /// 總頁碼數。
        /// </summary>
        public long TotalPages { get; set; }

        /// <summary>
        /// 記錄總數。
        /// </summary>
        public long TotalItems { get; set; }

        /// <summary>
        /// 每頁數量。
        /// </summary>
        public long ItemsPerPage { get; set; }

        /// <summary>
        /// 目前結果集。
        /// </summary>
        public List<T> Items { get; set; }

        /// <summary>
        /// 自定義使用者屬性。
        /// </summary>
        public object Context { get; set; }
    }
}