using System;
using Aurocore.Commons.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.IRepositories
{
    /// <summary>
    /// 定義倉儲接口
    /// </summary>
    public interface ICustomer_ActivityRepository:IRepository<Customer_Activity, string>
    {
    }
}