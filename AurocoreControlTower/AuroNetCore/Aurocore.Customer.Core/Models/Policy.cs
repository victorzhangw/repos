using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.Customers.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("CRM_Policy")]
    [Serializable]
    public class Policy:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        ///  保單號碼
        /// </summary>
        public string CpolicyNo { get; set; }

        /// <summary>
        ///  要保序號
        /// </summary>
        public string PerserialNo { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Period { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Period1 { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Mailingtype { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredid { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredname { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredbirth { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredadress { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredemail { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredmobile { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string InsureddaytimepNo { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredcontactname { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredcontactmobile { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredcontactemail { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insuredrelationship { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantid { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantname { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantbirth { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantadress { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantemail { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantmobile { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string ApplicantdaytimepNo { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantrel { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantcontactname { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantcontactmobile { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantcontactemail { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Applicantrelationship { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Insure_type { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string InsuranceType { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string InsuranceName { get; set; }

        /// <summary>
        ///  車號
        /// </summary>
        public string VehlicenseNo { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Grosspremium { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Projecttype { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Cosort { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string Policyamount { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string DeleteUserId { get; set; }


    }
}
