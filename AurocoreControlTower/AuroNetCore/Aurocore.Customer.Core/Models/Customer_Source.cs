using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.Customers.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("BTC_Customer_Source")]
    [Serializable]
    public class Customer_Source:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 設定或獲取來源識別碼
        /// </summary>
        public int? SourceID { get; set; }

        /// <summary>
        /// 設定或獲取來源名稱
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeleteUserId { get; set; }


    }
}
