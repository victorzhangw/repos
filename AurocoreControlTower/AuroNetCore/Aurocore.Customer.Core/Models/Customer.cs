using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.Customers.Models
{
    /// <summary>
    /// 客戶資料
    /// </summary>
    [Table("BTC_Customer")]
    [Serializable]
    public class Customer:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        
        
        
        /// <summary>
        /// 設定或獲取客戶ID(身分證)
        /// </summary>
        public string Pid { get; set; }
        /// <summary>
        /// 客戶 UUID
        /// </summary>
        public string CustomerID { get; set; }

        /// <summary>
        /// 設定或獲取客戶姓名
        /// </summary>
        public string Name { get; set; }

            /// <summary>
            /// 設定或獲取性別
            /// </summary>
            public string Sex { get; set; }

            /// <summary>
            /// 設定或獲取出生年日月
            /// </summary>
            public DateTime? Birthday { get; set; }

            /// <summary>
            /// 設定或獲取電話號碼
            /// </summary>
            public string Tel { get; set; }

            /// <summary>
            /// 設定或獲取電子郵件
            /// </summary>
            public string Email { get; set; }

            /// <summary>
            /// 設定或獲取手機號碼
            /// </summary>
            public string MobilePhone { get; set; }

            /// <summary>
            /// 設定或獲取電子保單Email
            /// </summary>
            public string EplyEmail { get; set; }

            /// <summary>
            /// 設定或獲取保單型式
            /// </summary>
            public string PolicyType { get; set; }

            /// <summary>
            /// 設定或獲取保單寄送類型
            /// </summary>
            public string PolicPostWay { get; set; }

            /// <summary>
            /// 設定或獲取保單郵寄地址
            /// </summary>
            public string PolicPostAddress { get; set; }

            /// <summary>
            /// 設定或獲取國籍別
            /// </summary>
            public string Country { get; set; }

            /// <summary>
            /// 設定或獲取職業等級
            /// </summary>
            public string OccupLevel { get; set; }

            /// <summary>
            /// 設定或獲取職業代號
            /// </summary>
            public string Occup { get; set; }

            /// <summary>
            /// 設定或獲取HCC等級
            /// </summary>
            public int? HCCLevel { get; set; }

            /// <summary>
            /// 設定或獲取建立的系統
            /// </summary>
            public int? CreatedSystemID { get; set; }

            /// <summary>
            /// 設定或獲取最新資料來源
            /// </summary>
            public string DateSource { get; set; }

            /// <summary>
            /// 設定或獲取是否為會員
            /// </summary>
            public string IsMember { get; set; }

            /// <summary>
            /// 設定或獲取是否有投保過
            /// </summary>
            public string IsInsured { get; set; }

            /// <summary>
            /// 設定或獲取是否有參加活動
            /// </summary>
            public string IsActivity { get; set; }

            /// <summary>
            /// 設定或獲取最後投保時間
            /// </summary>
            public DateTime? LastInusredTime { get; set; }

            /// <summary>
            /// 設定或獲取婚姻狀態
            /// </summary>
            public string MaritalStatus { get; set; }

            /// <summary>
            /// 設定或獲取客戶類別
            /// </summary>
            public int? CustomerType { get; set; }

            /// <summary>
            /// 設定或獲取是否為營發名單
            /// </summary>
            public int? IsOnList { get; set; }

            /// <summary>
            /// 設定或獲取是否願意被行銷
            /// </summary>
            public int? IsSaledAble { get; set; }

            /// <summary>
            /// 設定或獲取通路來源
            /// </summary>
            public int? Path { get; set; }

            /// <summary>
            /// 設定或獲取備註
            /// </summary>
            public string Memo { get; set; }

            /// <summary>
            /// 設定或獲取郵遞區號
            /// </summary>
            public string Zipcode { get; set; }

            /// <summary>
            /// 設定或獲取縣市
            /// </summary>
            public string City { get; set; }

            /// <summary>
            /// 設定或獲取行政區
            /// </summary>
            public string Area { get; set; }

            /// <summary>
            /// 設定或獲取地址
            /// </summary>
            public string Address { get; set; }

            /// <summary>
            /// 設定或獲取客戶狀態
            /// </summary>
            public int? Status { get; set; }

            /// <summary>
            /// 設定或獲取停用時間
            /// </summary>
            public DateTime? DisableTime { get; set; }

            /// <summary>
            /// 設定或獲取客戶來源
            /// </summary>
            public int? Source { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public bool? EnabledMark { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public bool? DeleteMark { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public DateTime? CreatorTime { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public string CreatorUserId { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public string CompanyId { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public string DeptId { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public DateTime? LastModifyTime { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public string LastModifyUserId { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public DateTime? DeleteTime { get; set; }

            /// <summary>
            /// 設定或獲取 
            /// </summary>
            public string DeleteUserId { get; set; }

        }
}
