﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.Customers.Models
{
    public static  class FilterStatus
    {
       
        public const string TypeStatus = "2206148226820";
        public const string TypeDealStatus = "2206148226821";
        public const string TypePayStatus = "2206150000001";
        /// <summary>
        /// 保單狀態
        /// </summary>
        public static readonly Dictionary<string, string> Status = new Dictionary<string, string>

        {
            { "status1", "1" },
            { "status2", "2" },
            { "status3", "3" },
            { "status4", "4" },
            { "status5", "5" },
            { "status6", "6" },
            { "status7", "7" },
            { "status8", "8" },
            { "status9", "9" }
        };
        /// <summary>
        /// 處理狀態
        /// </summary>
        public static readonly Dictionary<string, string> DealStatus = new Dictionary<string, string>

        {
            { "process1", "1" },
            { "process2", "3" },
            { "process3", "4" },
            { "process4", "5" },
            { "process5", "5" },
            { "process6", "2" },
            { "process7", "7" },
            { "process8", "8" },
            { "process9", "9" },
            { "process10", "10" },
            { "process11", "11" }
        };
        /// <summary>
        /// 交易狀態
        /// </summary>
        public static readonly Dictionary<string, string>  PayStatus = new Dictionary<string, string>

        {
            { "tradestatus1", "0" },
            { "tradestatus2", "1" },
            { "tradestatus3", "2" }
            
        };
    }
}
