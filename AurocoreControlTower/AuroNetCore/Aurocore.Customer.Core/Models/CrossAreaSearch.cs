﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.Customers.Models
{
    public class CrossAreaSearch
    {
        /// <summary>
        /// 關鍵字全部搜索
        /// </summary>
        public string Keyword { get; set; }
        /// <summary>
        /// 要保人身份證號/居留證號
        /// </summary>
        public string Pid { get; set; }
        /// <summary>
        /// 被保人身份證號/居留證號
        /// </summary>
        public string InsuredPid { get; set; }
        /// <summary>
        ///  車牌號碼
        /// </summary>
        public string PlateNo { get; set; }
        /// <summary>
        /// 要保序號
        /// </summary>
        public string EstimateId { get; set; }
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string PolicyNo { get; set; }
        /// <summary>
        /// 電話號碼
        /// </summary>
        public string PhoneNo { get; set; }
        /// <summary>
        /// 公司統一編號
        /// </summary>
        public string UniformTaxNo { get; set; }

        /// <summary>
        /// 駕照號碼
        /// </summary>
        public string VehlicenseNo { get; set; }
        /// <summary>
        /// 要保書號碼
        /// </summary>
        public string CpolicyNo { get; set; }
    }
}
