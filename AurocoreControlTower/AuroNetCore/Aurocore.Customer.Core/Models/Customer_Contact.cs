using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.Customers.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("BTC_Customer_Contact")]
    [Serializable]
    public class Customer_Contact:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 客戶 UUID
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人流水號
        /// </summary>
        public int? ContactPersonID { get; set; }

        /// <summary>
        /// 設定或獲取聯絡人姓名
        /// </summary>
        public string ContactPersonName { get; set; }

        /// <summary>
        /// 設定或獲取聯絡人電話
        /// </summary>
        public string ContactPersonPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ContactPersonEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeleteUserId { get; set; }


    }
}
