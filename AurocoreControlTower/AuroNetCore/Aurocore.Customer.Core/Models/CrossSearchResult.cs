﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Customers.Models;
using Aurocore.WorkOrders.Models;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using Aurocore.CMS.Models;
namespace Aurocore.Customers.Models
{
    [Serializable]
    
    public class CrossSearchResult
    {
        public CrossSearchResult()
        {
            Policies = new List<Policy>();
            WorkOrders = new List<WorkOrder>();
            Estimates = new List<Vehicle_Estimate>();

        }
        public string SearchTerm { get; set; }
        public List<Policy> Policies { get; set; }
        public List<WorkOrder> WorkOrders { get; set; }
        public List<Vehicle_Estimate> Estimates { get; set; }
    }

}
