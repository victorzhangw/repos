using System;
using Aurocore.Commons.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface ICustomer_SystemService:IService<Customer_System,Customer_SystemOutputDto, string>
    {
    }
}
