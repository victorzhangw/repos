using System;
using Aurocore.Commons.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.CMS.Dtos;

namespace Aurocore.Customers.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface ICustomerService:IService<Customer,CustomerOutputDto, string>
    {
        Task<PageResult<CustomerOutputDto>> FindCustomerWithPagerAsync(SearchInputDto<CrossAreaSearch> search);
        Task<PageResult<EstimateOutputDto>> FindCustomerEstimateWithPagerAsync(SearchInputDto<CrossAreaSearch> search);
    }
}
