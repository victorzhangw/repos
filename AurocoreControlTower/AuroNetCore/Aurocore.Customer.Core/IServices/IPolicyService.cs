using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.IServices;
using Aurocore.Commons.Pages;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;


namespace Aurocore.Customers.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface IPolicyService:IService<Policy,PolicyOutputDto, string>
    {
         Task<List<PolicySearchOutputDto>> FindPolicyIncludeedPerson(SearchInputDto<Policy> search);
        Task<Policy> FindCorrespondPerson(SearchInputDto<Policy> search);
    }
}
