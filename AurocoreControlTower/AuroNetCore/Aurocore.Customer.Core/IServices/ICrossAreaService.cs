﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
namespace Aurocore.Customers.IServices
{
    public interface ICrossAreaService 
    {
        Task<CrossSearchResult> GetWorkOrdersList(SearchInputDto<CrossAreaSearch> search);
        Task<CrossSearchResult> GetCrossCustomerDatas(SearchInputDto<CrossAreaSearch> search);
        Task<CrossSearchResult> GetEstimateList(SearchInputDto<CrossAreaSearch> search);
    }
}
