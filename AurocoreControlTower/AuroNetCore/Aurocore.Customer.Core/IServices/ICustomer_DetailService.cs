using System;
using Aurocore.Commons.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface ICustomer_DetailService:IService<Customer_Detail,Customer_DetailOutputDto, string>
    {
    }
}
