using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Customer_DetailRepository : BaseRepository<Customer_Detail, string>, ICustomer_DetailRepository
    {
		public Customer_DetailRepository()
        {
        }

        public Customer_DetailRepository(IDbContextCore context) : base(context)
        {
        }
    }
}