using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Customer_ActivityRepository : BaseRepository<Customer_Activity, string>, ICustomer_ActivityRepository
    {
		public Customer_ActivityRepository()
        {
        }

        public Customer_ActivityRepository(IDbContextCore context) : base(context)
        {
        }
    }
}