using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Customer_SourceRepository : BaseRepository<Customer_Source, string>, ICustomer_SourceRepository
    {
		public Customer_SourceRepository()
        {
        }

        public Customer_SourceRepository(IDbContextCore context) : base(context)
        {
        }
    }
}