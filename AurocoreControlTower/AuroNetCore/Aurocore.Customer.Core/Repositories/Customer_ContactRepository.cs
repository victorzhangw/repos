using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Customer_ContactRepository : BaseRepository<Customer_Contact, string>, ICustomer_ContactRepository
    {
		public Customer_ContactRepository()
        {
        }

        public Customer_ContactRepository(IDbContextCore context) : base(context)
        {
        }
    }
}