using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Repositories
{
	/// <summary>
	/// 倉儲接口的實現
	/// </summary>
	public class CustomerRepository : BaseRepository<Customer, string>, ICustomerRepository
	{
		public CustomerRepository()
		{
		}

		public CustomerRepository(IDbContextCore context) : base(context)
		{
		}
	}
}