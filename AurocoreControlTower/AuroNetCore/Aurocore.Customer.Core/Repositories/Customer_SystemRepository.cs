using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Customer_SystemRepository : BaseRepository<Customer_System, string>, ICustomer_SystemRepository
    {
		public Customer_SystemRepository()
        {
        }

        public Customer_SystemRepository(IDbContextCore context) : base(context)
        {
        }
    }
}