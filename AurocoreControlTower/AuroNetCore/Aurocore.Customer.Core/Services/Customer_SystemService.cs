using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Customer_SystemService: BaseService<Customer_System,Customer_SystemOutputDto, string>, ICustomer_SystemService
    {
		private readonly ICustomer_SystemRepository _repository;
        public Customer_SystemService(ICustomer_SystemRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}