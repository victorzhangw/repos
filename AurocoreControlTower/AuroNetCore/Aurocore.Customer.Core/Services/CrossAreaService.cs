﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
using Aurocore.WorkOrders.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.CMS.IServices;
using Aurocore.CMS.IRepositories;
using Aurocore.WorkOrders.Enums;

namespace Aurocore.Customers.Services
{
	/// <summary>
	/// 服務接口實現
	/// </summary>
	public class CrossAreaService :  ICrossAreaService
	{
		private readonly ICrossAreaRepository _repository;
		private readonly IWorkOrderRepository _workOrderRepository;
		private readonly IWorkOrderDetailRepository _workOrderDetailRepository;
		private readonly IPolicyRepository _policyRepository;
		private readonly IEstimateRepository _estimateRepository;
		public CrossAreaService(ICrossAreaRepository repository,
            IWorkOrderRepository workOrderRepository,
            IPolicyRepository policyRepository,
            IEstimateRepository estimateRepository,
            IWorkOrderDetailRepository workOrderDetailRepository)
        {
            _repository = repository;
            _workOrderRepository = workOrderRepository;
            _policyRepository = policyRepository;
            _estimateRepository = estimateRepository;
            _workOrderDetailRepository = workOrderDetailRepository;
        }
        /// <summary>
        /// 取得客戶/會員資料
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public async Task<CrossSearchResult> GetCrossCustomerDatas(SearchInputDto<CrossAreaSearch> search)
		{
			CrossSearchResult crossSearchResult = new CrossSearchResult();
			bool order = search.Order == "asc" ? false : true;
			string woWhere =  "1=1";
			string esWhere =  "1=1"; 
			//string policyWhere = "1=1";
			
			if (search.Filter != null)
			{
				if (!string.IsNullOrEmpty(search.Filter.Pid))
				{
					crossSearchResult.SearchTerm = search.Filter.Pid;
					woWhere += $" AND ApplicantId='{search.Filter.Pid}' ";
					crossSearchResult.SearchTerm = search.Filter.Pid;
				}
				if (!string.IsNullOrEmpty(search.Filter.InsuredPid))
				{
					crossSearchResult.SearchTerm = search.Filter.InsuredPid;
					woWhere += $" AND ApplicantId='{search.Filter.InsuredPid}' ";
					crossSearchResult.SearchTerm = search.Filter.InsuredPid;
				}
				if (!string.IsNullOrEmpty(search.Filter.UniformTaxNo))
				{
					crossSearchResult.SearchTerm = search.Filter.InsuredPid;
					woWhere += $" AND ApplicantId='{search.Filter.UniformTaxNo}' ";
					crossSearchResult.SearchTerm = search.Filter.UniformTaxNo;
				}
				if (!string.IsNullOrEmpty(search.Filter.EstimateId))
				{
					crossSearchResult.SearchTerm = search.Filter.EstimateId;
					woWhere += $" AND SourceId='{search.Filter.EstimateId}' ";
					crossSearchResult.SearchTerm = search.Filter.EstimateId;
				}

				
			}

			if (!string.IsNullOrEmpty(search.Keywords))
			{
				 woWhere = "1=1";
				crossSearchResult.SearchTerm = search.Filter.EstimateId;
				woWhere += $" AND SourceId='{search.Keywords}' ";
				woWhere += $" OR ApplicantId='{search.Keywords}'";
				crossSearchResult.SearchTerm = search.Keywords;
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				esWhere = "1=1";
				crossSearchResult.SearchTerm = search.Filter.EstimateId;
				esWhere += $" AND EstimateId='{search.Keywords}' ";
				esWhere += $" OR ApplicantID='{search.Keywords}' ";
				crossSearchResult.SearchTerm = search.Keywords;
			}
			var woLists = await	_workOrderRepository.GetListWhereAsync(woWhere);
			
			var EstimateLists = await	_estimateRepository.GetListWhereAsync(esWhere);
		
			if (woLists.Count() > 0)
			{
				var wo = woLists.ToList();
				
                if (!string.IsNullOrEmpty(search.Filter.EstimateId))
                {
					for(int i = 0; i < wo.Count; i++)
                    {
						string _detailWhere = @$"1=1 AND WorkOrderId='{wo[i].Id}' 
								AND(ResponseTypeName = '{nameof(ResponseType.OutBound話務記錄)}' 
								OR ResponseTypeName = '{nameof(ResponseType.Inbound話務記錄)}')";
						var _details = await _workOrderDetailRepository.GetListWhereAsync(_detailWhere);
						wo.RemoveAt(i);
						if (_details.Count() > 0)
						{
							var wod = _details.ToList();
							for (int j = 0; j < wod.Count(); j++)
							{
                                wo.Add(new WorkOrder
								{
									FormTypeName = wod[j].ResponseTypeName,
									StatusName = wod[j].StatusName,
									CreatorTime = wod[j].CreatorTime,
									Attachment = wod[j].Attachment,
									Header = wod[j].Header,
									OwnerName = wod[j].CreatorUserName,
									IsRead=wod[j].IsRead
									
								});
							}


						}
					}
					
                }
				crossSearchResult.WorkOrders = wo.OrderByDescending(c => c.CreatorTime).ToList();
			}
			if (EstimateLists.Count() > 0)
			{
				//env = env.OrderByDescending(c => c.ReportDate).ToList();
				//crossSearchResult.Estimates = EstimateLists.ToList();
				crossSearchResult.Estimates = EstimateLists.ToList();
			}

			return crossSearchResult;
		}

       

        public async Task<CrossSearchResult> GetEstimateList(SearchInputDto<CrossAreaSearch> search)
        {
			CrossSearchResult crossSearchResult = new CrossSearchResult();
			bool order = search.Order == "asc" ? false : true;
			string woWhere = "1=1";
			//string policyWhere = "1=1";

			if (search.Filter != null)
			{
				if (!string.IsNullOrEmpty(search.Filter.Pid))
				{
					crossSearchResult.SearchTerm = search.Filter.Pid;
					woWhere += $" AND ApplicantId='{search.Filter.Pid}' ";

				}
				if (!string.IsNullOrEmpty(search.Filter.InsuredPid))
				{
					crossSearchResult.SearchTerm = search.Filter.InsuredPid;
					woWhere += $" AND ApplicantId='{search.Filter.InsuredPid}' ";

				}
				

			}

			var elist = await _estimateRepository.GetListWhereAsync(woWhere);

			if (elist.Count() > 0)
			{
				crossSearchResult.Estimates = elist.ToList();
			}



			return crossSearchResult;
		}

        public async Task<CrossSearchResult> GetWorkOrdersList(SearchInputDto<CrossAreaSearch> search)
        {
			CrossSearchResult crossSearchResult = new CrossSearchResult();
			bool order = search.Order == "asc" ? false : true;
			string woWhere = "1=1";
			//string policyWhere = "1=1";

			if (search.Filter != null)
			{
				if (!string.IsNullOrEmpty(search.Filter.Pid))
				{
					crossSearchResult.SearchTerm = search.Filter.Pid;
					woWhere += $" AND ApplicantId='{search.Filter.Pid}' ";

				}
				if (!string.IsNullOrEmpty(search.Filter.InsuredPid))
				{
					crossSearchResult.SearchTerm = search.Filter.InsuredPid;
					woWhere += $" AND ApplicantId='{search.Filter.InsuredPid}' ";

				}
				if (!string.IsNullOrEmpty(search.Filter.UniformTaxNo))
				{
					crossSearchResult.SearchTerm = search.Filter.InsuredPid;
					woWhere += $" AND ApplicantId='{search.Filter.UniformTaxNo}' ";

				}

			}

			var woLists = await _workOrderRepository.GetListWhereAsync(woWhere);

			if (woLists.Count() > 0)
			{
				crossSearchResult.WorkOrders = woLists.ToList();
			}



			return crossSearchResult;
		}
    }
}
