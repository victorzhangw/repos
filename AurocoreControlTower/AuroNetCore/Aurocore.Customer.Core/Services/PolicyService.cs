using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
using System.Linq;
namespace Aurocore.Customers.Services
{
	/// <summary>
	/// 服務接口實現
	/// </summary>
	public class PolicyService: BaseService<Policy,PolicyOutputDto, string>, IPolicyService
	{
		private readonly IPolicyRepository _repository;
		public PolicyService(IPolicyRepository repository) : base(repository)
		{
			_repository=repository;
		}

		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public override async Task<PageResult<PolicyOutputDto>> FindWithPagerAsync(SearchInputDto<Policy> search)
		{
			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += "  AND (";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += "  CpolicyNo = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or PerserialNo = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Period = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Insuredid = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Insuredname = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Insuredmobile = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or InsureddaytimepNo = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Applicantid = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Applicantname = '" + search.Keywords + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Applicantmobile = '" + search.Keywords  + "'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or ApplicantdaytimepNo = '" + search.Keywords + "'";
			}


			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += "  )";
			}

			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<Policy> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort = "Id", order);
			PageResult<PolicyOutputDto> pageResult = new PageResult<PolicyOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<PolicyOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}

		/// <summary>
		/// 搜索保單相關人資料
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public  async Task<List<PolicySearchOutputDto>> FindPolicyIncludeedPerson(SearchInputDto<Policy> search)
		{
			Policy policy = new Policy();
			
			string where = GetDataPrivilege(false);
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += "  AND (";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += "  CpolicyNo like '%" + search.Keywords + "%'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or PerserialNo like '%" + search.Keywords + "%'";
			}
			
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or VehlicenseNo like '%" + search.Keywords + "%'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Insuredid like '%" + search.Keywords + "%'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Insuredname like '%" + search.Keywords + "%'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Applicantid like '%" + search.Keywords + "%'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Applicantmobile like '%" + search.Keywords + "%'";
			}
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " or Applicantname like '%" + search.Keywords + "%'";
			}
			// SQL where 結尾
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += "  )";
			}

			// 特定篩選條件
			if (!string.IsNullOrEmpty(search.Filter.CpolicyNo))
			{
				where += "  AND ( CpolicyNo = '" + search.Filter.CpolicyNo + "' OR PerserialNo='"+ search.Filter.CpolicyNo + "')";
			}
			if (!string.IsNullOrEmpty(search.Filter.VehlicenseNo))
			{
				where += "  AND ( VehlicenseNo = '" + search.Filter.VehlicenseNo+"')";
			}
			



			var  _policies= await repository.GetListWhereAsync(where);
			List<PolicySearchOutputDto> policySearchList = new();
			if (_policies != null)
			{
				var policyGroup = from _policy in _policies
								  group _policy by new {_policy.CpolicyNo,_policy.PerserialNo } into newGroup
								  select newGroup;
				foreach(var pg in policyGroup)
				{
					PolicySearchOutputDto policySearch = new();
					
					foreach (var item in pg)
					{
						
						if (!string.IsNullOrEmpty(item.CpolicyNo))
						{
							policySearch.PolicyNo = item.CpolicyNo;

						}
						else
						{
							policySearch.PolicyNo = item.PerserialNo;
						}
						policySearch.VehlicenseNo = item.VehlicenseNo;
						if (!string.IsNullOrEmpty(item.Applicantname))
						{
							PersonalData personalData = new();
							personalData.Name = item.Applicantname;
							personalData.Pid = item.Applicantid;
							personalData.Phone = item.Applicantmobile;
							personalData.Relation = "要保人";
							policySearch.Persons.Add(personalData);

						}
						if (!string.IsNullOrEmpty(item.Insuredname))
						{
							PersonalData personalData = new();
							personalData.Name = item.Insuredname;
							personalData.Pid = item.Insuredid;
							personalData.Phone = item.Insuredmobile;
							personalData.Relation = "被保人";
							policySearch.Persons.Add(personalData);
						}
						if (!string.IsNullOrEmpty(item.Applicantcontactname))
						{
							PersonalData personalData = new();
							personalData.Name = item.Applicantcontactname;
							personalData.Phone = item.Applicantcontactmobile;
							personalData.Relation = "要保聯絡人";
							policySearch.Persons.Add(personalData);
						}
						if (!string.IsNullOrEmpty(item.Insuredcontactname))
						{
							PersonalData personalData = new();
							personalData.Name = item.Insuredcontactname;
							personalData.Phone = item.Insuredcontactmobile;
							personalData.Relation = "被保聯絡人";
							policySearch.Persons.Add(personalData);
						}
					}
				
					policySearchList.Add(policySearch);
				}
			}
			
			return policySearchList;
		}
		/// <summary>
		/// 搜索保單相關人資料
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public async Task<Policy> FindCorrespondPerson(SearchInputDto<Policy> search)
		{
			PolicySearchOutputDto policySearchOutputDto = new();
			string where = GetDataPrivilege(false);
			Policy _policy = new Policy();
			if (!string.IsNullOrEmpty(search.Filter.CpolicyNo))
			{
				where += "  AND ( CpolicyNo = '" + search.Filter.CpolicyNo + "' OR PerserialNo='" + search.Filter.CpolicyNo + "')";
				_policy = await repository.GetWhereAsync(where);
			}
			
			
			//policySearchOutputDto = _policy.MapTo<PolicySearchOutputDto>();


			return _policy;
		}
	}
}