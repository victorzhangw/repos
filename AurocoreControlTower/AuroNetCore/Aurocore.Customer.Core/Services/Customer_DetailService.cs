using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Customer_DetailService: BaseService<Customer_Detail,Customer_DetailOutputDto, string>, ICustomer_DetailService
    {
		private readonly ICustomer_DetailRepository _repository;
        public Customer_DetailService(ICustomer_DetailRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}