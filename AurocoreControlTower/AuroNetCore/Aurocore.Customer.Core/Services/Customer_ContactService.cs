using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Customer_ContactService: BaseService<Customer_Contact,Customer_ContactOutputDto, string>, ICustomer_ContactService
    {
		private readonly ICustomer_ContactRepository _repository;
        public Customer_ContactService(ICustomer_ContactRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}