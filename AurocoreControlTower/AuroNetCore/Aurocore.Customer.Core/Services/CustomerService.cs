using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
using Aurocore.CMS.Models;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.IRepositories;

namespace Aurocore.Customers.Services
{
	/// <summary>
	/// 服務接口實現
	/// </summary>
	public class CustomerService: BaseService<Customer,CustomerOutputDto, string>, ICustomerService
	{
		private readonly ICustomerRepository _repository;
		private readonly IEstimateRepository _EstimateRepositoryrepository;
		public CustomerService(ICustomerRepository repository,IEstimateRepository estimateRepository) : base(repository)
		{
			_repository=repository;
			_EstimateRepositoryrepository=estimateRepository;
		}
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		
		public override async Task<PageResult<CustomerOutputDto>> FindWithPagerAsync(SearchInputDto<Customer> search)
		{

			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);

			if (search.Filter != null)
			{
				if (!string.IsNullOrEmpty(search.Filter.Pid))
				{

					where += $" AND PId='{search.Filter.Pid}' ";
					
				}

			}
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<Customer> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort="Id", order);
			PageResult<CustomerOutputDto> pageResult = new PageResult<CustomerOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<CustomerOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}
		public  async Task<PageResult<CustomerOutputDto>> FindCustomerWithPagerAsync(SearchInputDto<CrossAreaSearch> search)
		{

			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);

			if (search.Filter != null)
			{
				if (!string.IsNullOrEmpty(search.Filter.Pid))
				{

					where += $" AND PId='{search.Filter.Pid}' ";

				}
				if (!string.IsNullOrEmpty(search.Filter.InsuredPid))
				{

					where += $" AND PId='{search.Filter.Pid}' ";

				}
				if (!string.IsNullOrEmpty(search.Filter.UniformTaxNo))
				{

					where += $" AND PId='{search.Filter.Pid}' ";

				}

			}
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<Customer> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort = "LastModifyTime", order);
			PageResult<CustomerOutputDto> pageResult = new PageResult<CustomerOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<CustomerOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}
		public async Task<PageResult<EstimateOutputDto>> FindCustomerEstimateWithPagerAsync(SearchInputDto<CrossAreaSearch> search)
		{

			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);

			if (search.Filter != null)
			{
				if (!string.IsNullOrEmpty(search.Filter.CpolicyNo))
				{

					where += $" AND EstimateId='{search.Filter.CpolicyNo}' ";

				}
				if (!string.IsNullOrEmpty(search.Filter.VehlicenseNo))
				{

					where += $" AND TagId='{search.Filter.VehlicenseNo}' ";

				}
				

			}
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<Vehicle_Estimate> list = await _EstimateRepositoryrepository.FindWithPagerAsync(where, pagerInfo, search.Sort = "EstimateId", order);
			PageResult<EstimateOutputDto> pageResult = new PageResult<EstimateOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<EstimateOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}


	}
}