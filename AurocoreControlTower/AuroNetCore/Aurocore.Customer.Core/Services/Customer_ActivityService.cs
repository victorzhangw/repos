using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.Customers.IRepositories;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Customer_ActivityService: BaseService<Customer_Activity,Customer_ActivityOutputDto, string>, ICustomer_ActivityService
    {
		private readonly ICustomer_ActivityRepository _repository;
        public Customer_ActivityService(ICustomer_ActivityRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}