using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Customer_ContactOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取客戶識別碼
        /// </summary>
        [MaxLength(32)]
        public string CustomerID { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人流水號
        /// </summary>
        public int? ContactPersonID { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人姓名
        /// </summary>
        [MaxLength(50)]
        public string ContactPersonName { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人電話
        /// </summary>
        [MaxLength(20)]
        public string ContactPersonPhone { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(50)]
        public string ContactPersonEmail { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeleteUserId { get; set; }

    }
}
