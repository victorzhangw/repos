using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Policy))]
    [Serializable]
    public class PolicyInputDto: IInputDto<string>
    {

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CpolicyNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PerserialNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Period { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Period1 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Mailingtype { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredid { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredbirth { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredadress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsureddaytimepNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredcontactname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredcontactmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredcontactemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insuredrelationship { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantid { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantbirth { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantadress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantdaytimepNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantrel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantcontactname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantcontactmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantcontactemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicantrelationship { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Insure_type { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsuranceType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsuranceName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string VehlicenseNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Grosspremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Projecttype { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Cosort { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Policyamount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

    }
}
