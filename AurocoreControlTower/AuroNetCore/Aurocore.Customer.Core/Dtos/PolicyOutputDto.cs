using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class PolicyOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CpolicyNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string PerserialNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Period { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Period1 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Mailingtype { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Email { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredid { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredbirth { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredadress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string InsureddaytimepNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredcontactname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredcontactmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredcontactemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insuredrelationship { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantid { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantbirth { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantadress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string ApplicantdaytimepNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantrel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantcontactname { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantcontactmobile { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantcontactemail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Applicantrelationship { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Insure_type { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string InsuranceType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string InsuranceName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string VehlicenseNo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Grosspremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Projecttype { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Cosort { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Policyamount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeleteUserId { get; set; }


    }
}
