using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Customer_Activity))]
    [Serializable]
    public class Customer_ActivityInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取客戶識別碼
        /// </summary>
        public string CustomerID { get; set; }

        /// <summary>
        /// 設定或獲取活動名稱
        /// </summary>
        public string ActivityName { get; set; }

        /// <summary>
        /// 設定或獲取參加時間
        /// </summary>
        public DateTime? JoinDate { get; set; }

        /// <summary>
        /// 設定或獲取是否得獎
        /// </summary>
        public int? IsReward { get; set; }

        /// <summary>
        /// 設定或獲取獎項名稱
        /// </summary>
        public string RewardName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }


    }
}
