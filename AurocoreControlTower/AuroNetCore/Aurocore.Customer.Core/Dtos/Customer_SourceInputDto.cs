using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Customer_Source))]
    [Serializable]
    public class Customer_SourceInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取來源識別碼
        /// </summary>
        public int? SourceID { get; set; }
        /// <summary>
        /// 設定或獲取來源名稱
        /// </summary>
        public string SourceName { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

    }
}
