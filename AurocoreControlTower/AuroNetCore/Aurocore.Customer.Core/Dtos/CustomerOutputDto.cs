using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class CustomerOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取客戶識別碼
        /// </summary>
        [MaxLength(32)]
        public string CustomerID { get; set; }

        /// <summary>
        /// 設定或獲取客戶ID(身分證)
        /// </summary>
        [MaxLength(12)]
        public string Pid { get; set; }

        /// <summary>
        /// 設定或獲取客戶姓名
        /// </summary>
        [MaxLength(240)]
        public string Name { get; set; }

        /// <summary>
        /// 設定或獲取性別
        /// </summary>
        [MaxLength(1)]
        public string Sex { get; set; }

        /// <summary>
        /// 設定或獲取出生年日月
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 設定或獲取電話號碼
        /// </summary>
        [MaxLength(20)]
        public string Tel { get; set; }

        /// <summary>
        /// 設定或獲取電子郵件
        /// </summary>
        [MaxLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// 設定或獲取手機號碼
        /// </summary>
        [MaxLength(20)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 設定或獲取電子保單Email
        /// </summary>
        [MaxLength(50)]
        public string EplyEmail { get; set; }

        /// <summary>
        /// 設定或獲取保單型式
        /// </summary>
        [MaxLength(1)]
        public string PolicyType { get; set; }

        /// <summary>
        /// 設定或獲取保單寄送類型
        /// </summary>
        [MaxLength(1)]
        public string PolicPostWay { get; set; }

        /// <summary>
        /// 設定或獲取保單郵寄地址
        /// </summary>
        [MaxLength(180)]
        public string PolicPostAddress { get; set; }

        /// <summary>
        /// 設定或獲取國籍別
        /// </summary>
        [MaxLength(3)]
        public string Country { get; set; }

        /// <summary>
        /// 設定或獲取職業等級
        /// </summary>
        [MaxLength(1)]
        public string OccupLevel { get; set; }

        /// <summary>
        /// 設定或獲取職業代號
        /// </summary>
        [MaxLength(10)]
        public string Occup { get; set; }

        /// <summary>
        /// 設定或獲取HCC等級
        /// </summary>
        public int? HCCLevel { get; set; }

        /// <summary>
        /// 設定或獲取建立的系統
        /// </summary>
        public int? CreatedSystemID { get; set; }

        /// <summary>
        /// 設定或獲取最新資料來源
        /// </summary>
        [MaxLength(50)]
        public string DateSource { get; set; }

        /// <summary>
        /// 設定或獲取是否為會員
        /// </summary>
        [MaxLength(1)]
        public string IsMember { get; set; }

        /// <summary>
        /// 設定或獲取是否有投保過
        /// </summary>
        [MaxLength(1)]
        public string IsInsured { get; set; }

        /// <summary>
        /// 設定或獲取是否有參加活動
        /// </summary>
        [MaxLength(1)]
        public string IsActivity { get; set; }

        /// <summary>
        /// 設定或獲取最後投保時間
        /// </summary>
        public DateTime? LastInusredTime { get; set; }

        /// <summary>
        /// 設定或獲取婚姻狀態
        /// </summary>
        [MaxLength(1)]
        public string MaritalStatus { get; set; }

        /// <summary>
        /// 設定或獲取客戶類別
        /// </summary>
        public int? CustomerType { get; set; }

        /// <summary>
        /// 設定或獲取是否為營發名單
        /// </summary>
        public int? IsOnList { get; set; }

        /// <summary>
        /// 設定或獲取是否願意被行銷
        /// </summary>
        public int? IsSaledAble { get; set; }

        /// <summary>
        /// 設定或獲取通路來源
        /// </summary>
        public int? Path { get; set; }

        /// <summary>
        /// 設定或獲取備註
        /// </summary>
        [MaxLength(200)]
        public string Memo { get; set; }

        /// <summary>
        /// 設定或獲取郵遞區號
        /// </summary>
        [MaxLength(20)]
        public string Zipcode { get; set; }

        /// <summary>
        /// 設定或獲取縣市
        /// </summary>
        [MaxLength(20)]
        public string City { get; set; }

        /// <summary>
        /// 設定或獲取行政區
        /// </summary>
        [MaxLength(20)]
        public string Area { get; set; }

        /// <summary>
        /// 設定或獲取地址
        /// </summary>
        [MaxLength(50)]
        public string Address { get; set; }

        /// <summary>
        /// 設定或獲取客戶狀態
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 設定或獲取停用時間
        /// </summary>
        public DateTime? DisableTime { get; set; }

        /// <summary>
        /// 設定或獲取客戶來源
        /// </summary>
        public int? Source { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeleteUserId { get; set; }



    }
}
