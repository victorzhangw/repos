using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Customer_System))]
    [Serializable]
    public class Customer_SystemInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取平台識別碼
        /// </summary>
        public int SystemID { get; set; }
        /// <summary>
        /// 設定或獲取平台名稱
        /// </summary>
        public string SystemName { get; set; }
        /// <summary>
        /// 設定或獲取Id
        /// </summary>
        public bool? EnabledMark { get; set; }

    }
}
