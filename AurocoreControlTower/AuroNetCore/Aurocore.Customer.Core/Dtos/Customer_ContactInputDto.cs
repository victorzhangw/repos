using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Customer_Contact))]
    [Serializable]
    public class Customer_ContactInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取客戶識別碼
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人流水號
        /// </summary>
        public int? ContactPersonID { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人姓名
        /// </summary>
        public string ContactPersonName { get; set; }
        /// <summary>
        /// 設定或獲取聯絡人電話
        /// </summary>
        public string ContactPersonPhone { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ContactPersonEmail { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

    }
}
