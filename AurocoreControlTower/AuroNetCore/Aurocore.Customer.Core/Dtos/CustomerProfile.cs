using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
           CreateMap<Customer, CustomerOutputDto>();
           CreateMap<CustomerInputDto, Customer>();
           CreateMap<Customer_Activity, Customer_ActivityOutputDto>();
           CreateMap<Customer_ActivityInputDto, Customer_Activity>();
           CreateMap<Customer_Contact, Customer_ContactOutputDto>();
           CreateMap<Customer_ContactInputDto, Customer_Contact>();
           CreateMap<Customer_Detail, Customer_DetailOutputDto>();
           CreateMap<Customer_DetailInputDto, Customer_Detail>();
           CreateMap<Customer_Source, Customer_SourceOutputDto>();
           CreateMap<Customer_SourceInputDto, Customer_Source>();
           CreateMap<Customer_System, Customer_SystemOutputDto>();
           CreateMap<Customer_SystemInputDto, Customer_System>();

        }
    }
}
