using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Customer_Detail))]
    [Serializable]
    public class Customer_DetailInputDto: IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

    }
}
