using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Customers.Models;

namespace Aurocore.Customers.Dtos
{
    public class PolicyProfile : Profile
    {
        public PolicyProfile()
        {
           CreateMap<Policy, PolicyOutputDto>();
           CreateMap<PolicyInputDto, Policy>();

        }
    }
}
