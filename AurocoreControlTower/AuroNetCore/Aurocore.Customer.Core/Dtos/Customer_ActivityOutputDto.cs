using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Customer_ActivityOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取客戶識別碼
        /// </summary>
        [MaxLength(32)]
        public string CustomerID { get; set; }
        /// <summary>
        /// 設定或獲取活動名稱
        /// </summary>
        [MaxLength(50)]
        public string ActivityName { get; set; }
        /// <summary>
        /// 設定或獲取參加時間
        /// </summary>
        public DateTime? JoinDate { get; set; }
        /// <summary>
        /// 設定或獲取是否得獎
        /// </summary>
        public int? IsReward { get; set; }
        /// <summary>
        /// 設定或獲取獎項名稱
        /// </summary>
        [MaxLength(50)]
        public string RewardName { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeleteUserId { get; set; }

    }
}
