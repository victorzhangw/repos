﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Customers.Models;
using Aurocore.WorkOrders.Models;
using System.ComponentModel.DataAnnotations;
namespace Aurocore.Customers.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class CrossSearchResultOutputDto
    {
        /// <summary>
        /// 搜索字
        /// </summary>
        public string SearchTerm { get; set; }
        /// <summary>
        /// 保單
        /// </summary>
        public Policy policy { get; set; }
        /// <summary>
        /// 案件
        /// </summary>
        public WorkOrder workOrder { get; set; }
    }
}
