﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.Customers.Dtos
{
    public class PolicySearchOutputDto
    {
        public  PolicySearchOutputDto()
        {
            this.Persons = new();
        }
        /// <summary>
        /// 保險 / 要保 單號
        /// </summary>
        public string PolicyNo { get; set; }
        /// <summary>
        /// 車號
        /// </summary>
        public string VehlicenseNo { get; set; }
        public List<PersonalData> Persons { get; set; }
    }
    public class PersonalData
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 保單記載之關係
        /// </summary>
        public string Relation { get; set; }
        /// <summary>
        /// 聯繫電話
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 個人 ID
        /// </summary>
        public string Pid { get; set; }
    } 
}
