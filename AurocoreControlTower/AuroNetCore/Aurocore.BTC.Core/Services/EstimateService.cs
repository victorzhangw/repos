using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class EstimateService: BaseService<Estimate,EstimateOutputDto, string>, IEstimateService
    {
		private readonly IEstimateRepository _repository;
        public EstimateService(IEstimateRepository repository) : base(repository)
        {
			_repository=repository;
        }

        Estimate IEstimateService.GetVehicleEstimate(string EstimateId)
        {
            return _repository.GetVehicleEstimate(EstimateId);
        }

        PageResult<EstimateOutputDto> IEstimateService.GetVehicleEstimates(SearchInputDto<Estimate> search)
        {
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            return _repository.GetVehicleEstimates(search);
        }
    }
}