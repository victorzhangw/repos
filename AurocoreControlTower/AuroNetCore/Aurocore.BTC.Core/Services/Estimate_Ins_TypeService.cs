using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Estimate_Ins_TypeService: BaseService<Estimate_Ins_Type,Estimate_Ins_TypeOutputDto, string>, IEstimate_Ins_TypeService
    {
		private readonly IEstimate_Ins_TypeRepository _repository;
        public Estimate_Ins_TypeService(IEstimate_Ins_TypeRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}