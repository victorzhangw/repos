using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Estimate_HccService: BaseService<Estimate_Hcc,Estimate_HccOutputDto, string>, IEstimate_HccService
    {
		private readonly IEstimate_HccRepository _repository;
        public Estimate_HccService(IEstimate_HccRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}