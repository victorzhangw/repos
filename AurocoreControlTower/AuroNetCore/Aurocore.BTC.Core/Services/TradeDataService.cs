using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class TradeDataService: BaseService<TradeData,TradeDataOutputDto, string>, ITradeDataService
    {
		private readonly ITradeDataRepository _repository;
        public TradeDataService(ITradeDataRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}