using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Ca_Ea_PstService: BaseService<Ca_Ea_Pst,Ca_Ea_PstOutputDto, string>, ICa_Ea_PstService
    {
		private readonly ICa_Ea_PstRepository _repository;
        public Ca_Ea_PstService(ICa_Ea_PstRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}