using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;
using Dapper;
using System.Linq;
using System.Collections.Generic;
using Aurocore.BTC.Core.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Dtos;

namespace Aurocore.BTC.Core.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class EstimateRepository : BaseRepository<Estimate, string>, IEstimateRepository
    {
		public EstimateRepository()
        {
        }

        public EstimateRepository(IDbContextCore context) : base(context)
        {
        }

        public Estimate GetVehicleEstimate(string EstimateId)
        {
            string sql = string.Format("SELECT * FROM BTC_Estimate WHERE EstimateId={0} ", EstimateId);
            Estimate estimate = DapperConn.QueryFirst<Estimate>(sql);
            return estimate;
        }

        public PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Estimate> search)
        {
            
            
            string sql = string.Format("SELECT TOP {0} * FROM BTC_Estimate");
            List<Estimate> result = DapperConn.Query<Estimate>(sql).ToList();
            PageResult<EstimateOutputDto> pageResult = new PageResult<EstimateOutputDto>
            
            {
                CurrentPage = 1,
                Items = result.MapTo<EstimateOutputDto>(),
                ItemsPerPage = 1,
                
                TotalItems = result.Count
            };
            return pageResult;
        }
    }
}