using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;

namespace Aurocore.BTC.Core.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Estimate_HccRepository : BaseRepository<Estimate_Hcc, string>, IEstimate_HccRepository
    {
		public Estimate_HccRepository()
        {
        }

        public Estimate_HccRepository(IDbContextCore context) : base(context)
        {
        }

        Estimate_Hcc IEstimate_HccRepository.GetEstimateHcc(string EstimateId)
        {
            throw new NotImplementedException();
        }

        PageResult<Estimate_Hcc> IEstimate_HccRepository.GetEstimateHccs(string top)
        {
            throw new NotImplementedException();
        }
    }
}