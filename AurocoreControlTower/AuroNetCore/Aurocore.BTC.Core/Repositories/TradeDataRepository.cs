using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;

namespace Aurocore.BTC.Core.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class TradeDataRepository : BaseRepository<TradeData, string>, ITradeDataRepository
    {
		public TradeDataRepository()
        {
        }

        public TradeDataRepository(IDbContextCore context) : base(context)
        {
        }

        TradeData ITradeDataRepository.GetTradeData(string EstimateId)
        {
            throw new NotImplementedException();
        }

        PageResult<TradeData> ITradeDataRepository.GetTradeDatas(string top)
        {
            throw new NotImplementedException();
        }
    }
}