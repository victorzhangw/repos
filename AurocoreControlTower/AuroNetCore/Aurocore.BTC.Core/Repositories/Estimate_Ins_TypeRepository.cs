using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;

namespace Aurocore.BTC.Core.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Estimate_Ins_TypeRepository : BaseRepository<Estimate_Ins_Type, string>, IEstimate_Ins_TypeRepository
    {
		public Estimate_Ins_TypeRepository()
        {
        }

        public Estimate_Ins_TypeRepository(IDbContextCore context) : base(context)
        {
        }

        Estimate_Ins_Type IEstimate_Ins_TypeRepository.GetEstimateType(string EstimateId)
        {
            throw new NotImplementedException();
        }

        PageResult<Estimate_Ins_Type> IEstimate_Ins_TypeRepository.GetEstimateTypes(string top)
        {
            throw new NotImplementedException();
        }
    }
}