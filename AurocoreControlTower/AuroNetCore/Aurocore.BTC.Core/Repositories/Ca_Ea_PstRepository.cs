using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.BTC.Core.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;

namespace Aurocore.BTC.Core.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Ca_Ea_PstRepository : BaseRepository<Ca_Ea_Pst, string>, ICa_Ea_PstRepository
    {
		public Ca_Ea_PstRepository()
        {
        }

        public Ca_Ea_PstRepository(IDbContextCore context) : base(context)
        {
        }

        Ca_Ea_Pst ICa_Ea_PstRepository.GetEstimate(string EstimateId)
        {
            throw new NotImplementedException();
        }

        PageResult<Ca_Ea_Pst> ICa_Ea_PstRepository.GetEstimates(string top)
        {
            throw new NotImplementedException();
        }
    }
}