using System;
using Aurocore.Commons.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Dtos;

namespace Aurocore.BTC.Core.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface IEstimateService:IService<Estimate,EstimateOutputDto, string>
       
    {
        /// <summary>
        /// 取得要保書資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Estimate> search);
        /// <summary>
        /// 取得要保書資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Estimate GetVehicleEstimate(string EstimateId);
    }
}
