using System;
using Aurocore.Commons.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface ICa_Ea_PstService:IService<Ca_Ea_Pst,Ca_Ea_PstOutputDto, string>
    {
    }
}
