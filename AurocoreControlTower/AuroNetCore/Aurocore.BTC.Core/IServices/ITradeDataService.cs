using System;
using Aurocore.Commons.IServices;
using Aurocore.BTC.Core.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface ITradeDataService:IService<TradeData,TradeDataOutputDto, string>
    {
    }
}
