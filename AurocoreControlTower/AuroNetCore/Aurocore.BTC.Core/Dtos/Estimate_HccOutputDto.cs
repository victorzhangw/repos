using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.BTC.Core.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Estimate_HccOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RouteMain { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RateDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SexAndMarriage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SexAndMarriageFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Age { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AgeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DamageAccSum5Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DamageAccSum5YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaAccSum5Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaAccSum5YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DutyAccSum5Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DutyAccSum5YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarTypeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarAge { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarAgeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string HasDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DamageFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string HasLia { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string HasTheft { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TheftFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ExtInsSum { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ExtInsSumFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ZipCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ZipCodeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarTypeYear { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarTypeYearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Score { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Level { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CreateID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CreateTime { get; set; }


    }
}
