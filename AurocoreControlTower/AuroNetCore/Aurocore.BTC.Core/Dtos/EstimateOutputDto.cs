using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.BTC.Core.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class EstimateOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string FK { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string OtherCompanyCompelCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelDatePeriod { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsIns24IncludeCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelPolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewCompelPolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewCompelCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelCheck { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChoiceDatePeriod { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChoiceDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChoiceDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChoicePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewChoicePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewChoiceCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ProvisionD { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredIdExt { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredBirthDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredSex { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredMarriage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredTel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredTelNight { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredMobilPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredFax { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredDeputy { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredAssets { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredCntryRiskLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Assured_cntry { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredRiskLevelOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AssuredLicense { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string UserNames { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantBirthDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantSex { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantMobilPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantTelDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantTelNight { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantFax { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantDeputy { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantAssets { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantCntryRiskLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Applicant_cntry { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantRiskLevelOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TagId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BrandId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BrandName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BrandGroupId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarStyle { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IssueDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ProductYear { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ProductMonth { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarTypeName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Cylinder { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EngineId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BodyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarPrice { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TransportAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TransportUnitType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string VehicleModelName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsNew { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PowerType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string OfficerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RouteId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RateType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RouteComm { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessFromType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SubCompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LeaderId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string QuotaId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CommissionTargetId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Resource { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BrokerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BranchId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsContact { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ContactDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ContactUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ContactReason { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Status { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DealStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantPostStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ExamineDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EntryDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string UpdateDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ReceiveFaxDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryResultCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumCompelPolicyInfo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryResultCompelPolicyInfo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SexAgeFactorCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LastCompelLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string OtherCompanyCompelPolicyDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryLastCompelDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastCompelPolicyDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DrunkDrivingTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastCompelCarType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompelFirstDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastCompelCompany { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastChoicePolicyNum { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastChoicePolicyDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastChoicePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChoiceFirstDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsExistPolicyDetail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarDamageProductYearRateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarDamageProductYearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryResultDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SexAgeFactorDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarDamageAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumOnceUnknownDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarOnceUnknownDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarOnceUnknownDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarOnceUnknownDamageAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumNoneUnknownDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarNoneUnknownDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarNoneUnknownDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CarNoneUnknownDamageAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string UnknownDamageFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumLastDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryDamageDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryDamagePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryDamageInsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewTradevanQueryNumDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewCarDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RenewCarDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DamageAccidentCloseTimesFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DamageDutyAndCountFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Damage_Acc_1 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Damage_Acc_2 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Damage_Acc_3 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BurglaryProductYearRateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BurglaryProductYearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumTheft { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryTheftDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanLastTheftPolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SexAgeFactorLiability { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryResultLia { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradevanQueryNumLia { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaAccidentTimesWithinYear { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaAccidentTimesFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LiaDutyAndCountFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PremiumChoice { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PremiumCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ReadyMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string StableMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CompensationMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ResearchMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InvestMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessRuleMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AdjustPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CapitalMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string MaintainMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsDiscount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Nequipment { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CalculateTimeUnit { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string MisEntryDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplyDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyOutTrans { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PaymentAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PaymentZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyAreaCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessCompany { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessRoute { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessSales { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsPolicyPostOutsourcing { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyPostWay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyPostZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyPostAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyPostRecipient { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EPolicyEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TermType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CancelEstimateUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CancelEstimateDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LastEstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsCheck { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CheckDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CheckUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CheckReason { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CustomerCheckReason { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CheckNote { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CheckType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SentMailDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SentMailReceiver { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SentMailReceiverName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SentMailReceiverPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChOfficerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChQuotaId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChLeader { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ChRouter { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Corp { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CorpName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string MvdisStatusCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string MvdisJson { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string MvdisImgPath { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsCategory { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ProjectID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CustomerID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PromotionID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ActivityCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CustomerNote { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ManagerNote { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ProjectAttribute { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string SourceIP { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PackageId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PromoPackageId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Device { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PersonalNoteDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsuranceNoteDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ServiceNoteDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PolicyEntry { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicationReminder { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TransferDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AreaId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CashierId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PublicWelfare { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ActivityReminder { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Lottery { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IsOtherRoute { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Depreciate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ACCUSerialNumber { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string VirtualHCCLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string VirtualHCCScore { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string KYCDate { get; set; }


    }
}
