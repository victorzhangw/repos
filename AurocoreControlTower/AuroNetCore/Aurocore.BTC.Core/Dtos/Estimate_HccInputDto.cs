using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Estimate_Hcc))]
    [Serializable]
    public class Estimate_HccInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RouteMain { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RateDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SexAndMarriage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SexAndMarriageFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Age { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DamageAccSum5Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DamageAccSum5YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaAccSum5Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaAccSum5YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DutyAccSum5Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DutyAccSum5YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string YearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarTypeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarAge { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarAgeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string HasDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DamageFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string HasLia { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string HasTheft { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TheftFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ExtInsSum { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ExtInsSumFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ZipCodeFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarTypeYear { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarTypeYearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CreateID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CreateTime { get; set; }


    }
}
