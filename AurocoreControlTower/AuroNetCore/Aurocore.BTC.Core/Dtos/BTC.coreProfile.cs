using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Dtos
{
    public class BTCcoreProfile : Profile
    {
        public BTCcoreProfile()
        {
           CreateMap<Ca_Ea_Pst, Ca_Ea_PstOutputDto>();
           CreateMap<Ca_Ea_PstInputDto, Ca_Ea_Pst>();
           CreateMap<Estimate, EstimateOutputDto>();
           CreateMap<EstimateInputDto, Estimate>();
           CreateMap<Estimate_Hcc, Estimate_HccOutputDto>();
           CreateMap<Estimate_HccInputDto, Estimate_Hcc>();
           CreateMap<Estimate_Ins_Type, Estimate_Ins_TypeOutputDto>();
           CreateMap<Estimate_Ins_TypeInputDto, Estimate_Ins_Type>();
           CreateMap<TradeData, TradeDataOutputDto>();
           CreateMap<TradeDataInputDto, TradeData>();

        }
    }
}
