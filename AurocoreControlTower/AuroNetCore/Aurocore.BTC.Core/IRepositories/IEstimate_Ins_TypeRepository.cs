using System;
using Aurocore.Commons.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;
namespace Aurocore.BTC.Core.IRepositories
{
    /// <summary>
    /// 定義險種明細倉儲接口
    /// </summary>
    public interface IEstimate_Ins_TypeRepository:IRepository<Estimate_Ins_Type, string>
    {
        /// <summary>
        /// 取得要保書險種資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<Estimate_Ins_Type> GetEstimateTypes(string top);
        /// <summary>
        /// 取得要保書險種資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Estimate_Ins_Type GetEstimateType(string EstimateId);
    }
}