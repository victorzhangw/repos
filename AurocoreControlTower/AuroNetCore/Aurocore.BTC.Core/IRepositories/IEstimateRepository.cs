using System;
using Aurocore.Commons.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;
using Aurocore.BTC.Core.Dtos;
using Aurocore.Commons.Dtos;

namespace Aurocore.BTC.Core.IRepositories
{
    /// <summary>
    /// 定義車險主檔倉儲接口
    /// </summary>
    public interface IEstimateRepository:IRepository<Estimate, string>
    {
        /// <summary>
        /// 取得要保書資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Estimate> search);
        /// <summary>
        /// 取得要保書資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Estimate GetVehicleEstimate(string EstimateId);
    }
}