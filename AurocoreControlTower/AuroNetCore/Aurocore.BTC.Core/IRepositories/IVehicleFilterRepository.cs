﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aurocore.Commons.IRepositories;
using Aurocore.BTC.Core.Models;
using Aurocore.Commons.Pages;
using System.Threading.Tasks;


namespace Aurocore.BTC.Core.IRepositories
{
    public interface IVehicleTypeRepository 
    {

        /// <summary>
        /// 取得車險要保書資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<Ca_Ea_Pst> GetEstimates(string top);
        /// <summary>
        /// 取得車險要保書實體
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Ca_Ea_Pst GetEstimate(string EstimateId);
    }
}
