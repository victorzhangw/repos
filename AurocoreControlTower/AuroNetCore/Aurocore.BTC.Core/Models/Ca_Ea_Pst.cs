using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.BTC.Core.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("BTC_Ca_Ea_Pst")]
    [Serializable]
    public class Ca_Ea_Pst:BaseEntity<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantBirthday { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsScope { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantCareerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsPremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgentRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgentMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DiscountRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyDailyPay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyInsPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryTel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryZip { get; set; }


    }
}
