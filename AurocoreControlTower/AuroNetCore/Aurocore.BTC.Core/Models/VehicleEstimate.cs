﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using Aurocore.BTC.Core.Models;

namespace Aurocore.BTC.Core.Models
{
    public class VehicleEstimate
    {
        public VehicleEstimate()
        {
            Estimate_Ins_Types = new List<Estimate_Ins_Type>();
            Estimate_Hcc = new List<Estimate_Hcc>();
        }
        public Estimate Estimate { get; set; }
        public List<Estimate_Ins_Type> Estimate_Ins_Types { get; set; }
        public List<Estimate_Hcc> Estimate_Hcc { get; set; }
        public TradeData TradeData { get; set; }
    }
}
