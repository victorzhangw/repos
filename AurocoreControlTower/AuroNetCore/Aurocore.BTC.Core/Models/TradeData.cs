using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.BTC.Core.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("BTC_TradeData")]
    [Serializable]
    public class TradeData:BaseEntity<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsCategory { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MerchOID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LogDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayWay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradeSuccess { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PaidUpAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PaidUpSerial { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Properties { get; set; }

        /// <summary>
        /// 允許的持卡人清單
        /// Dictionary<string, string>
        /// </summary>
        public string AllowHolders { get; set; }


    }
}
