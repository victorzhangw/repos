using System;
using Aurocore.Commons.IRepositories;
using Aurocore.WebAnalytics.Tealeaf.Models;

namespace Aurocore.WebAnalytics.Tealeaf.IRepositories
{
    /// <summary>
    /// 定義Tealeaf Events倉儲接口
    /// </summary>
    public interface ITealeafRepository:IRepository<TealeafModel, string>
    {
    }
}