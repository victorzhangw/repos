using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.WebAnalytics.Tealeaf.IRepositories;
using Aurocore.WebAnalytics.Tealeaf.Models;

namespace Aurocore.Tealeaf.Repositories
{
	/// <summary>
	/// Tealeaf Events倉儲接口的實現
	/// </summary>
	public class TealeafRepository : BaseRepository<TealeafModel, string>, ITealeafRepository
	{
		public TealeafRepository()
		{
		}

		public TealeafRepository(IDbContextCore context) : base(context)
		{
		}
	}
}