using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.WebAnalytics.Tealeaf.Models
{
    /// <summary>
    /// Tealeaf Events，資料實體物件
    /// </summary>
    [Table("CRM_Tealeaf")]
    [Serializable]
    public class TealeafModel:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// 鑰Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 鑰事件形態
        /// </summary>
        public string EventTyoe { get; set; }
        /// <summary>
        /// 鑰事件
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 鑰日期
        /// </summary>
        public string EventDate { get; set; }
        /// <summary>
        /// 鑰事件資料
        /// </summary>
        public string Events { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? DeleteMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreatorTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CreatorUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CompanyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DeptId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DeleteUserId { get; set; }

    }
}
