using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.WebAnalytics.Tealeaf.IRepositories;
using Aurocore.WebAnalytics.Tealeaf.IServices;
using Aurocore.WebAnalytics.Tealeaf.Dtos;
using Aurocore.WebAnalytics.Tealeaf.Models;

namespace Aurocore.Tealeaf.Services
{
    /// <summary>
    /// Tealeaf Events服務接口實現
    /// </summary>
    public class TealeafService: BaseService<TealeafModel,TealeafOutputDto, string>, ITealeafService
    {
		private readonly ITealeafRepository _repository;
        public TealeafService(ITealeafRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}