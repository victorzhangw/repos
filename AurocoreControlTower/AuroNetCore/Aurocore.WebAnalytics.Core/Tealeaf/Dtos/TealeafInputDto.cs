using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.WebAnalytics.Tealeaf.Models;

namespace Aurocore.WebAnalytics.Tealeaf.Dtos
{
    /// <summary>
    /// Tealeaf Events輸入物件模型
    /// </summary>
    [AutoMap(typeof(TealeafModel))]
    [Serializable]
    public class TealeafInputDto: IInputDto<string>
    {
        /// <summary>
        /// 鑰Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 鑰事件形態
        /// </summary>
        public string EventTyoe { get; set; }

        /// <summary>
        /// 鑰事件
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// 鑰日期
        /// </summary>
        public string EventDate { get; set; }

        /// <summary>
        /// 鑰事件資料
        /// </summary>
        public string Events { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

    }
}
