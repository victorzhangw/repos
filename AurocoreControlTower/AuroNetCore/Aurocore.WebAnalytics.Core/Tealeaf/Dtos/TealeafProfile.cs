using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.WebAnalytics.Tealeaf.Models;

namespace Aurocore.WebAnalytics.Tealeaf.Dtos
{
    public class TealeafProfile : Profile
    {
        public TealeafProfile()
        {
           CreateMap<TealeafModel, TealeafOutputDto>();
           CreateMap<TealeafInputDto, TealeafModel>();

        }
    }
}
