using System;
using Aurocore.Commons.IServices;
using Aurocore.WebAnalytics.Tealeaf.Dtos;
using Aurocore.WebAnalytics.Tealeaf.Models;

namespace Aurocore.WebAnalytics.Tealeaf.IServices
{
    /// <summary>
    /// 定義Tealeaf Events服務接口
    /// </summary>
    public interface ITealeafService:IService<TealeafModel,TealeafOutputDto, string>
    {
    }
}
