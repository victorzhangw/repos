﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WebAnalytics.Enums
{
    public enum ReportName
    {
        GADailyReport,
        TealeafDailyReport
    }
}
