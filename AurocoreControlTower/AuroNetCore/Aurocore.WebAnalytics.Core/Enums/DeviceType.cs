﻿using System;


namespace Aurocore.WebAnalytics.Enums
{
    public enum DeviceType
    {
        手機 =0,
        平板=1,
        筆電或桌機=2

    }
    public class AnalyticsEnums
    {
        public static DeviceType AliasTodeviceType(string deviceType)
        {
            return deviceType switch
            {
                "tablet" => DeviceType.平板,
                "mobile" => DeviceType.手機,
                "desktop" => DeviceType.筆電或桌機,
                _ => throw new ArgumentException(deviceType),
            };
        }
    }
    
}
