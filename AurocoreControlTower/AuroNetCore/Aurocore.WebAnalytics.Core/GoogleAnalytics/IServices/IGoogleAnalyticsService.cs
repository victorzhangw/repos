using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.IServices;
using Aurocore.WebAnalytics.GoogleAnalytics.Dtos;
using Aurocore.WebAnalytics.GoogleAnalytics.Models;

namespace Aurocore.WebAnalytics.GoogleAnalytics.IServices
{
    /// <summary>
    /// 定義Google Analytics服務接口
    /// </summary>
    public interface IGoogleAnalyticsService:IService<GAModel,GoogleAnalyticsOutputDto, string>
    {
         Task<int> StoreGoogleAnalyticsResult(string OpenIdType, string OpenName, GoogleAnalyticsInputDto inputDto);
         Task <ResponseReport> GetDateRangeGAData(string OpenIdType, string OpenName, GoogleAnalyticsInputDto inputDto);

    }
}
