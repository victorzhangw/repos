using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.IRepositories;
using Aurocore.WebAnalytics.GoogleAnalytics.Models;
using Google.Apis.AnalyticsReporting.v4;

namespace Aurocore.WebAnalytics.GoogleAnalytics.IRepositories
{
    /// <summary>
    /// 定義Google Analytics倉儲接口
    /// </summary>
    public interface IGoogleAnalyticsRepository:IRepository<GAModel, string>
    {
         Task<ResponseReport> AnalyticsReport(string OpenIdType, string OpenName,  List<ParameterDefines> parameterDefine, string startDate, string endDate);
    }
}