using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.WebAnalytics.GoogleAnalytics.Models;

namespace Aurocore.WebAnalytics.GoogleAnalytics.Dtos
{
    public class GoogleAnalyticsProfile : Profile
    {
        public GoogleAnalyticsProfile()
        {
           CreateMap<GAModel, GoogleAnalyticsOutputDto>();
           CreateMap<GoogleAnalyticsInputDto, GAModel>();

        }
    }
}
