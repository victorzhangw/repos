﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using Microsoft.Data.Sqlite;
using MySql.Data.MySqlClient;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Pages;
using Aurocore.Commons;

namespace Aurocore.WebAnalytics.Helpers
{
    public  class TwISOCodeHelpers

    {
        #region 初始化
        /// <summary>
        /// 連線字串
        /// </summary>
        internal static string DefaultSqlConnectionString { get; set; }

        private static DbConnection dbConnection;
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        protected static string dbConfigName = "";
        /// <summary>
        /// 實例化
        /// </summary>
        public TwISOCodeHelpers()
        {

        }
        #endregion

        public static RegionIso IsoCodeConverter(string value)
        {
            RegionIso regionIso = new();
          
            string isoSql = @"SELECT isocode,shortname,fullname,areaname FROM Isocode 
            as a INNER JOIN area as b ON a.area_id=b.id WHERE isocode=@isocode";
            try
            {
                using (DbConnection conn = OpenSharedConnection())
                {
                    regionIso = conn.QueryFirst<RegionIso>(isoSql, new { isocode = value });
                    
                }

              

            }
            catch (Exception e)
            {

                Console.WriteLine(e);

            }
            return regionIso;
        }
        /// <summary>
        /// 資料庫連線,根據資料庫型別自動識別，型別區分用設定名稱是否包含主要關鍵字
        /// MSSQL、MYSQL、ORACLE、SQLITE、MEMORY、NPGSQL
        /// </summary>
        /// <returns></returns>
        public static DbConnection OpenSharedConnection()
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            object connCode = AurocoreCacheHelper.Get("CodeGeneratorDbConn");
            string dbType = "";
            if (connCode != null)
            {
                DefaultSqlConnectionString = connCode.ToString();
                dbType = AurocoreCacheHelper.Get("CodeGeneratorDbType").ToString().ToUpper();
            }
            else
            {
                string conStringEncrypt = Configs.GetConfigurationValue("AppSetting", "ConStringEncrypt");
                if (string.IsNullOrEmpty(dbConfigName))
                {
                    dbConfigName = Configs.GetConfigurationValue("AppSetting", "DefaultDataBase");
                }
                DefaultSqlConnectionString = Configs.GetConnectionString(dbConfigName);
                if (conStringEncrypt == "true")
                {
                    DefaultSqlConnectionString = DEncrypt.Decrypt(DefaultSqlConnectionString);
                }
                dbType = dbConfigName.ToUpper();
                TimeSpan expiresSliding = DateTime.Now.AddMinutes(30) - DateTime.Now;
                AurocoreCacheHelper.Add("CodeGeneratorDbConn", DefaultSqlConnectionString, expiresSliding, false);
                AurocoreCacheHelper.Add("CodeGeneratorDbType", dbType, expiresSliding, false);
            }
            if (dbType.Contains("SQLSERVER"))
            {
                dbConnection = new SqlConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("MYSQL"))
            {
                dbConnection = new MySqlConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("ORACLE"))
            {
                dbConnection = new OracleConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("SQLITE"))
            {
                dbConnection = new SqliteConnection(DefaultSqlConnectionString);
            }
            else if (dbType.Contains("MEMORY"))
            {
                throw new NotSupportedException("In Memory Dapper Database Provider is not yet available.");
            }
            else if (dbType.Contains("NPGSQL"))
            {
                dbConnection = new NpgsqlConnection(DefaultSqlConnectionString);
            }
            else
            {
                throw new NotSupportedException("The database is not supported");
            }
            if (dbConnection.State != ConnectionState.Open)
            {
                dbConnection.Open();
            }
            return dbConnection;
        }
    }
    public class RegionIso
    {
        public string IsoCode { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string AreaName { get; set; }
    }

    
}
