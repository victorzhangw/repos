﻿using Google.Apis.AnalyticsReporting.v4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.WebAnalytics.Helpers

{
    /// <summary>
    ///  Google Analytics 定義
    /// </summary>
    class GaDefineHelper
    {
        /// <summary>
        ///  Define Dimensions
        /// </summary>
        /// <returns> Dimension</returns>
        public static Dictionary<string, Dimension> SetDimensions()
        {
            Dictionary<string, Dimension> dictDimension = new Dictionary<string, Dimension>();
            dictDimension.Add("date", new Dimension { Name = "ga:date" });
            dictDimension.Add("source", new Dimension { Name = "ga:source" });
            dictDimension.Add("country", new Dimension { Name = "ga:country" });
            dictDimension.Add("campaign", new Dimension { Name = "ga:campaign" });
            dictDimension.Add("adGroup", new Dimension { Name = "ga:adGroup" });
            dictDimension.Add("keyword", new Dimension { Name = "ga:keyword" });
            dictDimension.Add("exitPagePath", new Dimension { Name = "ga:exitPagePath" });
            dictDimension.Add("sourceMedium", new Dimension { Name = "ga:sourceMedium" });
            dictDimension.Add("medium", new Dimension { Name = "ga:medium" });
            dictDimension.Add("deviceCategory", new Dimension { Name = "ga:deviceCategory" });
            dictDimension.Add("fullReferrer", new Dimension { Name = "ga:fullReferrer" });
            dictDimension.Add("userGender", new Dimension { Name = "ga:userGender" });
            dictDimension.Add("userAgeBracket", new Dimension { Name = "ga:userAgeBracket" });
            dictDimension.Add("dayOfWeekName", new Dimension { Name = "ga:dayOfWeekName" });
            dictDimension.Add("pagePath", new Dimension { Name = "ga:pagePath" });
            dictDimension.Add("pagePath1", new Dimension { Name = "ga:pagePathLevel1" });
            dictDimension.Add("region", new Dimension { Name = "ga:region" });
            dictDimension.Add("regionId", new Dimension { Name = "ga:regionId" });
            dictDimension.Add("regionIsoCode", new Dimension { Name = "ga:regionIsoCode" });
            dictDimension.Add("userType", new Dimension { Name = "ga:userType" });
            dictDimension.Add("userBucket", new Dimension { Name = "ga:userBucket" });
            dictDimension.Add("productSku", new Dimension { Name = "ga:productSku" });
            dictDimension.Add("productName", new Dimension { Name = "ga:productName" });
            dictDimension.Add("pageTitle", new Dimension { Name = "ga:pageTitle" });
            dictDimension.Add("channelGrouping", new Dimension { Name = "ga:channelGrouping" });

            
            return dictDimension;
        }
        /// <summary>
        /// Define Metrics 
        /// </summary>
        /// <returns> Metrics </returns>
        public static Dictionary<string, Metric> SetMetrics()
        {
            Dictionary<string, Metric> dictMetric = new Dictionary<string, Metric>();
            dictMetric.Add("pageviews", new Metric { Expression = "ga:pageviews", Alias = "瀏覽量" });
            dictMetric.Add("uniquePageviews", new Metric { Expression = "ga:uniquePageviews", Alias = "不重複瀏覽量" });
            dictMetric.Add("users", new Metric { Expression = "ga:users", Alias = "訪客數" });
            dictMetric.Add("newUsers", new Metric { Expression = "ga:newUsers", Alias = "新訪客數" });
            dictMetric.Add("bounces", new Metric { Expression = "ga:bounces", Alias = "跳出數" });
            dictMetric.Add("bounceRate", new Metric { Expression = "ga:bounceRate", Alias = "跳出率" });
            dictMetric.Add("sessionDuration", new Metric { Expression = "ga:sessionDuration", Alias = "工作階段時間長度" });
            dictMetric.Add("avgSessionDuration", new Metric { Expression = "ga:avgSessionDuration", Alias = "平均工作階段時間長度" });
            dictMetric.Add("exitRate", new Metric { Expression = "ga:exitRate", Alias = "離開率" });
            dictMetric.Add("exits", new Metric { Expression = "ga:exits", Alias = "離開數" });
            dictMetric.Add("hits", new Metric { Expression = "ga:hits", Alias = "匹配" });
            dictMetric.Add("avgTimeOnPage", new Metric { Expression = "ga:avgTimeOnPage", Alias = "平均頁面瀏覽時長" });
            dictMetric.Add("sessions", new Metric { Expression = "ga:sessions", Alias = "工作階段" });
            dictMetric.Add("avgPageLoadTime", new Metric { Expression = "ga:avgPageLoadTime", Alias = "平均頁面載入時間" });
            dictMetric.Add("avgServerResponseTime", new Metric { Expression = "ga:avgServerResponseTime", Alias = "avgServerResponseTime" });
            dictMetric.Add("pageviewsPerSession", new Metric { Expression = "ga:pageviewsPerSession", Alias = "平均工作階段頁面瀏覽數" });
            dictMetric.Add("entranceRate", new Metric { Expression = "ga:entranceRate", Alias = "entranceRate" });
            dictMetric.Add("_1dayUsers", new Metric { Expression = "ga:1dayUsers", Alias = "1dayUsers" });
            dictMetric.Add("_7dayUsers", new Metric { Expression = "ga:7dayUsers", Alias = "7dayUsers" });
            dictMetric.Add("_14dayUsers", new Metric { Expression = "ga:14dayUsers", Alias = "14dayUsers" });
            dictMetric.Add("_28dayUsers", new Metric { Expression = "ga:28dayUsers", Alias = "28dayUsers" });
            dictMetric.Add("_30dayUsers", new Metric { Expression = "ga:30dayUsers", Alias = "30dayUsers" });
            dictMetric.Add("impressions", new Metric { Expression = "ga:impressions", Alias = "曝光" });
            dictMetric.Add("adClicks", new Metric { Expression = "ga:adClicks", Alias = "廣告點擊" });
            dictMetric.Add("adCost", new Metric { Expression = "ga:adCost", Alias = "廣告成本" });
            dictMetric.Add("CPM", new Metric { Expression = "ga:CPM", Alias = "CPM" });
            dictMetric.Add("CPC", new Metric { Expression = "ga:CPC", Alias = "CPC" });
            dictMetric.Add("CTR", new Metric { Expression = "ga:CTR", Alias = "CTR" });
            dictMetric.Add("costPerGoalConversion", new Metric { Expression = "ga:costPerGoalConversion", Alias = "costPerGoalConversion" });
            dictMetric.Add("costPerTransaction", new Metric { Expression = "ga:costPerTransaction", Alias = "costPerTransaction" });
            dictMetric.Add("RPC", new Metric { Expression = "ga:RPC", Alias = "RPC" });
            dictMetric.Add("ROAS", new Metric { Expression = "ga:ROAS", Alias = "ROAS" });
            dictMetric.Add("transactions", new Metric { Expression = "ga:transactions", Alias = "交易次數" });
            dictMetric.Add("totalValue", new Metric { Expression = "ga:totalValue", Alias = "總價值" });
            dictMetric.Add("transactionRevenue", new Metric { Expression = "ga:transactionRevenue", Alias = "收益" });
            dictMetric.Add("transactionsPerSession", new Metric { Expression = "ga:transactionsPerSession", Alias = "電商轉換率" });
            dictMetric.Add("revenuePerTransaction", new Metric { Expression = "ga:revenuePerTransaction", Alias = "平均訂單價值" });
            dictMetric.Add("goal1Completions", new Metric { Expression = "ga:goal1Completions", Alias = "加入會員" });
            dictMetric.Add("goal2Completions", new Metric { Expression = "ga:goal2Completions", Alias = "購物車檢視" });
            dictMetric.Add("goal3Completions", new Metric { Expression = "ga:goal3Completions", Alias = "交易完成" });
            dictMetric.Add("goal4Completions", new Metric { Expression = "ga:goal4Completions", Alias = "交易完成(海外)" });
            dictMetric.Add("itemQuantity", new Metric { Expression = "ga:itemQuantity", Alias = "購買數量" });
            dictMetric.Add("itemsPerPurchase", new Metric { Expression = "ga:itemsPerPurchase", Alias = "平均購買數量" });
            dictMetric.Add("avgEventValue", new Metric { Expression = "ga:avgEventValue", Alias = "平均訂單價值" });
            dictMetric.Add("revenuePerItem", new Metric { Expression = "ga:revenuePerItem", Alias = "平均結賬單價" });
            dictMetric.Add("itemRevenue", new Metric { Expression = "ga:itemRevenue", Alias = "產品收益" });
            dictMetric.Add("productListClicks", new Metric { Expression = "ga:productListClicks", Alias = "產品點擊次數" });
            dictMetric.Add("productCheckouts", new Metric { Expression = "ga:productCheckouts", Alias = "產品結賬次數" });
            dictMetric.Add("productAddsToCart", new Metric { Expression = "ga:productAddsToCart", Alias = "產品加入購物車次數" });
            dictMetric.Add("organicSearches", new Metric { Expression = "ga:organicSearches", Alias = "自然搜索" });
            dictMetric.Add("timeOnPage", new Metric { Expression = "ga:timeOnPage", Alias = "頁面停留時間" });
            dictMetric.Add("pageLoadTime", new Metric { Expression = "ga:pageLoadTime", Alias = "頁面載入時間" });
            


            return dictMetric;
        }
    }
}
