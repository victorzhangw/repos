using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Enums;
using Aurocore.Commons.Extend;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Options;
using Aurocore.Email.Core;
using Aurocore.Security.IRepositories;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;
using Microsoft.AspNetCore.Mvc;
using Aurocore.Commons.Models;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Models;
using Microsoft.AspNetCore.Hosting;

namespace Aurocore.Quartz.Jobs
{
    /// <summary>
    /// 測試demo
    /// 必須實現IJob介面中的Execute()方法
    /// </summary>
    public class WorkOrderMail : IJob
    {
        ITaskManagerService iService = IoCContainer.Resolve<ITaskManagerService>();
        ILogRepository iLogService = IoCContainer.Resolve<ILogRepository>();
        IWorkOrderService iworkOrderService = IoCContainer.Resolve<IWorkOrderService>();
         IWebHostEnvironment _hostingEnvironment = IoCContainer.Resolve<IWebHostEnvironment>();
        //  private readonly IWorkOrderService workOrderService;

        public Task Execute(IJobExecutionContext context)
        { 
            DateTime dateTime = DateTime.Now;
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            AbstractTrigger trigger = (context as JobExecutionContextImpl).Trigger as AbstractTrigger;
            string sqlWhere = string.Format("Id='{0}' and GroupName='{1}'", trigger.Name, trigger.Group);
            TaskManager taskManager = iService.GetWhere(sqlWhere);
            if (taskManager == null)
            {
                FileQuartz.WriteErrorLog($"任務不存在");
                return Task.Delay(1);
            }
            try
            {
                string msg = $"開始時間:{dateTime.ToString("yyyy-MM-dd HH:mm:ss ffff")}";
                //記錄任務執行記錄
                iService.RecordRun(taskManager.Id, JobAction.開始,true, msg);
                //初始化任務日誌
                FileQuartz.InitTaskJobLogPath(taskManager.Id);
                var jobId = context.MergedJobDataMap.GetString("OpenJob");
                var rtnMsg = RetrieveEmail("EzGo");
                Log4NetHelper.Info(DateTime.Now.ToString() + "執行任務");


                stopwatch.Stop();
                string content = $"結束時間:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff")} 共耗時{stopwatch.ElapsedMilliseconds} 毫秒\r\n";
                iService.RecordRun(taskManager.Id, JobAction.結束, true, content);
                if ((MsgType)taskManager.SendMail == MsgType.All)
                {
                    string emailAddress = sysSetting.Email;
                    if (!string.IsNullOrEmpty(taskManager.EmailAddress))
                    {
                        emailAddress = taskManager.EmailAddress;
                    }

                    List<string> recipients = new List<string>();
                    recipients = emailAddress.Split(",").ToList();
                    var mailBodyEntity = new MailBodyEntity()
                    {
                        Body = msg + content + rtnMsg + "<br>" + ",請勿回覆本郵件",
                        Recipients = recipients,
                        Subject = taskManager.TaskName
                    };
                    EWSMailHelper.SendEmail(mailBodyEntity);

                }
            }
            catch (Exception ex)
            {
                stopwatch.Stop();
                string content = $"結束時間:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff")} 共耗時{stopwatch.ElapsedMilliseconds} 毫秒\r\n";
                iService.RecordRun(taskManager.Id, JobAction.結束, false, content + ex.Message);
                FileQuartz.WriteErrorLog(ex.Message);
                if ((MsgType)taskManager.SendMail == MsgType.Error || (MsgType)taskManager.SendMail == MsgType.All)
                {
                    string emailAddress = sysSetting.Email;
                    if (!string.IsNullOrEmpty(taskManager.EmailAddress))
                    {
                        emailAddress = taskManager.EmailAddress;
                    }

                    List<string> recipients = new List<string>();
                    recipients = emailAddress.Split(",").ToList();
                    var mailBodyEntity = new MailBodyEntity()
                    {
                        Body = "處理失敗," + ex.Message + ",請勿回覆本郵件",
                        Recipients = recipients,
                        Subject = taskManager.TaskName
                    };
                    EWSMailHelper.SendEmail(mailBodyEntity);
                }
            }

            return Task.Delay(1);
        }
        private async Task<string> RetrieveEmail(string itemCode)
        {
            CommonResult result = new CommonResult();
            
            //AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            //SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            string localpath = _hostingEnvironment.WebRootPath;
            var rtnList = await iworkOrderService.RetrieveEmail(itemCode, _hostingEnvironment).ConfigureAwait(false);
            if (rtnList.Count != 0)
            {
                result.ErrCode = "0";
                result.ResData = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + " : " + "已匯入 MEMO 資料筆數:" + rtnList.Count;
                result.ErrMsg = "SUCCESS";
            }
            else
            {
                result.ErrMsg = "FAIL";
                result.ErrCode = "43002";
            }
            return result.ToJson(); 
        }
    }
}
