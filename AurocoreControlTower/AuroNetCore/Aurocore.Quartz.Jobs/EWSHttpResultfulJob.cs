using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Enums;
using Aurocore.Commons.Extend;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Json;
using Aurocore.Email.Core;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.Quartz.Jobs
{
    /// <summary>
    /// 處理遠端介面url的定時任務
    /// </summary>
    public class EWSHttpResultfulJob : IJob
    {
        ITaskManagerService iService = IoCContainer.Resolve<ITaskManagerService>();

        /// <summary>
        /// 執行遠端介面url的定時任務
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            AbstractTrigger trigger = (context as JobExecutionContextImpl).Trigger as AbstractTrigger;
            string sqlWhere = string.Format("Id='{0}' and GroupName='{1}'", trigger.Name, trigger.Group);
            TaskManager taskManager = iService.GetWhere(sqlWhere);
            string httpMessage = "";
            if (taskManager == null)
            {
                FileQuartz.WriteErrorLog($"任務不存在");
                return Task.Delay(1);
            }
            FileQuartz.InitTaskJobLogPath(taskManager.Id);
            string msg = $"開始時間:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff")}";
            //記錄任務執行記錄
            iService.RecordRun(taskManager.Id, JobAction.開始, true, msg);
            if (string.IsNullOrEmpty(taskManager.JobCallAddress) || taskManager.JobCallAddress == "/")
            {
                FileQuartz.WriteErrorLog($"{ DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff")}未設定任務地址,");
                iService.RecordRun(taskManager.Id, JobAction.結束, false, "未設定任務地址");
                return Task.Delay(1);
            }
            try
            {
                Dictionary<string, string> header = new Dictionary<string, string>();
                //if (!string.IsNullOrEmpty(taskOptions.AuthKey)
                //    && !string.IsNullOrEmpty(taskOptions.AuthValue))
                //{
                //    header.Add(taskOptions.AuthKey.Trim(), taskOptions.AuthValue.Trim());
                //}
                if (!string.IsNullOrEmpty(taskManager.JobCallParams))
                {
                    httpMessage = HttpRequestHelper.HttpPost(taskManager.JobCallAddress, taskManager.JobCallParams, null, header);
                }
                else
                {
                    httpMessage = HttpRequestHelper.HttpGet(taskManager.JobCallAddress);
                }
                stopwatch.Stop();
                string content = $"結束時間:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff")} 共耗時{stopwatch.ElapsedMilliseconds} 毫秒,訊息:{httpMessage ?? "OK"}\r\n";
                iService.RecordRun(taskManager.Id, JobAction.結束, true, content);
                if ((MsgType)taskManager.SendMail == MsgType.All)
                {
                    string emailAddress = sysSetting.Email;
                    if (!string.IsNullOrEmpty(taskManager.EmailAddress))
                    {
                        emailAddress = taskManager.EmailAddress;
                    }

                    List<string> recipients = new List<string>();
                    recipients = taskManager.EmailAddress.Split(",").ToList();
                    //recipients.Add(taskManager.EmailAddress);
                    var mailBodyEntity = new MailBodyEntity()
                    {
                        Body = content + "\n\r請勿直接回複本郵件！",
                        Recipients = recipients,
                        Subject = taskManager.TaskName,
                    };
                    EWSMailHelper.SendEmail(mailBodyEntity);
                }
            }
            catch (Exception ex)
            {
                stopwatch.Stop();
                string content = $"結束時間:{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff")} 共耗時{stopwatch.ElapsedMilliseconds} 毫秒\r\n";
                iService.RecordRun(taskManager.Id, JobAction.結束, false, content + ex.Message);
                FileQuartz.WriteErrorLog(ex.Message);
                if ((MsgType)taskManager.SendMail == MsgType.Error || (MsgType)taskManager.SendMail == MsgType.All)
                {
                    string emailAddress = sysSetting.Email;
                    if (!string.IsNullOrEmpty(taskManager.EmailAddress))
                    {
                        emailAddress = taskManager.EmailAddress;
                    }
                    List<string> recipients = new List<string>();
                    recipients = emailAddress.Split(",").ToList();
                    var mailBodyEntity = new MailBodyEntity()
                    {
                        Body = ex.Message + "\n\r請勿直接回複本郵件!",
                        Recipients = recipients,
                        Subject = taskManager.TaskName,
                    };
                    EWSMailHelper.SendEmail(mailBodyEntity);
                }
            }

            return Task.Delay(1);
        }
    }
}
