﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace Aurocore.WebApi.Areas.TMNEWASys.Controllers
{
    /// <summary>
    ///  取得 PDF
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]

    public class ReadPDFController : AreaApiController<APP, AppOutputDto, APPInputDto, IAPPService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public ReadPDFController(IAPPService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "APP/List";
            AuthorizeKey.InsertKey = "APP/Add";
            AuthorizeKey.UpdateKey = "APP/Edit";
            AuthorizeKey.UpdateEnableKey = "APP/Enable";
            AuthorizeKey.DeleteKey = "APP/Delete";
            AuthorizeKey.DeleteSoftKey = "APP/DeleteSoft";
            AuthorizeKey.ViewKey = "APP/View";
        }
        /// <summary>
        /// 取得 要保書/訂單 PDF 檔
        /// </summary>
        /// <param name="Id">要保書/保單//訂單號碼</param>
        /// <returns></returns>
        [HttpGet("GetInsurance")]
        [AllowAnonymous]
        public IActionResult GetInsurance(string Id)
        {
            CommonResult result = new CommonResult();
            result.Success = true;
            string pdfUri = $"http://devzoneapi2.aurocore.com/InsurancePdf/{Id}.pdf";
            try
            {
                var request = WebRequest.Create(pdfUri);
                using (var response = request.GetResponse())
                using (var responseStream = response.GetResponseStream())
                {
                    result.ErrCode = ErrCode.err0;
                    result.ResData = pdfUri;
                }
            }
            catch (WebException ex) when ((ex.Response as HttpWebResponse)?.StatusCode == HttpStatusCode.NotFound)
            {
                result.Success = false;
                result.ErrMsg = "無此保單";
            }
            catch (WebException ex)
            {
                result.Success = false;
                result.ErrMsg = ex.ToString();
            }


            return ToJsonContent(result);
        }
    }
}
