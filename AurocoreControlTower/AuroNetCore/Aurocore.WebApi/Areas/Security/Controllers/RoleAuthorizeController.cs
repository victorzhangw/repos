using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 角色許可權介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class RoleAuthorizeController : AreaApiController<RoleAuthorize, RoleAuthorizeOutputDto, RoleAuthorizeInputDto, IRoleAuthorizeService,string>
    {
        private readonly IMenuService menuService;
        private readonly IRoleDataService roleDataService;
        private readonly IOrganizeService organizeService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_menuService"></param>
        /// <param name="_roleDataService"></param>
        /// <param name="_organizeService"></param>
        public RoleAuthorizeController(IRoleAuthorizeService _iService, IMenuService _menuService, IRoleDataService _roleDataService, IOrganizeService _organizeService) : base(_iService)
        {
            iService = _iService;
            menuService = _menuService;
            roleDataService = _roleDataService;
            organizeService = _organizeService;
            AuthorizeKey.ListKey = "RoleAuthorize/List";
            AuthorizeKey.InsertKey = "RoleAuthorize/Add";
            AuthorizeKey.UpdateKey = "RoleAuthorize/Edit";
            AuthorizeKey.UpdateEnableKey = "RoleAuthorize/Enable";
            AuthorizeKey.DeleteKey = "RoleAuthorize/Delete";
            AuthorizeKey.DeleteSoftKey = "RoleAuthorize/DeleteSoft";
            AuthorizeKey.ViewKey = "RoleAuthorize/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(RoleAuthorize info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(RoleAuthorize info)
        {
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(RoleAuthorize info)
        {
        }

        /// <summary>
        /// 角色分配許可權樹
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        [HttpGet("GetRoleAuthorizeFunction")]
        [AurocoreAuthorize("List")]
        public IActionResult GetRoleAuthorizeFunction(string roleId,int itemType)
        {
            CommonResult result = new CommonResult();
            roleId = "'" + roleId + "'";
            List<string> resultlist = new List<string>();
            IEnumerable<RoleAuthorize> list= iService.GetListRoleAuthorizeByRoleId(roleId, itemType);
            foreach(RoleAuthorize info in list)
            {
                resultlist.Add(info.ItemId);
            }
            result.ResData = resultlist;
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }


        /// <summary>
        /// 儲存角色許可權
        /// </summary>
        /// <param name="roleinfo">功能許可權</param>
        /// <returns></returns>
        [HttpPost("SaveRoleAuthorize")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> SaveRoleAuthorize(RoleAuthorizeDataInputDto roleinfo)
        {
            CommonResult result = new CommonResult();
            try
            {                
                List<RoleAuthorize> inList = new List<RoleAuthorize>();
                List<ModuleFunctionOutputDto> list = roleinfo.RoleFunctios.ToList<ModuleFunctionOutputDto>();
                foreach (ModuleFunctionOutputDto item in list)
                {
                    RoleAuthorize info = new RoleAuthorize();
                    info.ObjectId = roleinfo.RoleId;
                    info.ItemType = item.FunctionTag;
                    info.ObjectType = 1;
                    info.ItemId = item.Id.ToString();
                    OnBeforeInsert(info);
                    inList.Add(info);
                }

                List<RoleData> roleDataList = new List<RoleData>();
                List<OrganizeOutputDto> listRoleData = roleinfo.RoleData.ToList<OrganizeOutputDto>();
                foreach (OrganizeOutputDto item in listRoleData)
                {
                    RoleData info = new RoleData();
                    info.RoleId = roleinfo.RoleId;
                    info.AuthorizeData = item.Id;
                    info.DType = "dept";
                    roleDataList.Add(info);
                }
                List<SystemTypeOutputDto> listRoleSystem = roleinfo.RoleSystem.ToList<SystemTypeOutputDto>();
                foreach (SystemTypeOutputDto item in listRoleSystem)
                {
                    RoleAuthorize info = new RoleAuthorize();
                    info.ObjectId = roleinfo.RoleId;
                    info.ItemType = 0;
                    info.ObjectType = 1;
                    info.ItemId = item.Id.ToString();
                    OnBeforeInsert(info);
                    inList.Add(info);
                }
                result.Success = await iService.SaveRoleAuthorize(roleinfo.RoleId,inList, roleDataList);
                if (result.Success)
                {
                    result.ErrCode = ErrCode.successCode;
                }
            }
            catch (Exception ex)
            {
                result.ErrMsg = ex.Message;
            }
            return ToJsonContent(result);
        }

        private List<RoleAuthorize> SubFunction(List<ModuleFunctionOutputDto> list, string roleId)
        {
            List<RoleAuthorize> inList = new List<RoleAuthorize>();
            foreach (ModuleFunctionOutputDto item in list)
            {
                RoleAuthorize info = new RoleAuthorize();
                info.ObjectId = roleId;
                info.ItemType = 1;
                info.ObjectType = 1;
                info.ItemId = item.Id.ToString();
                OnBeforeInsert(info);
                inList.Add(info);
                inList.Concat(SubFunction(item.Children, roleId));
            }
            return inList;
        }

        /// <summary>
        /// 獲取功能選單適用於Vue Tree樹形
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllFunctionTree")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetAllFunctionTree()
        {
            CommonResult result = new CommonResult();
            try
            {
                List<ModuleFunctionOutputDto> list = await iService.GetAllFunctionTree();
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = list;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取選單異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }
        /*
        /// <summary>
        /// 獲取功能選單適用於Vue 樹形列表
        /// </summary>
        /// <param name="systemTypeId">子系統Id</param>
        /// <returns></returns>
        //[HttpGet("GetAllFunctionTreeTable")]
        //[AurocoreAuthorize("List")]
        //public async Task<IActionResult> GetAllFunctionTreeTable(string systemTypeId)
        //{
        //    CommonResult result = new CommonResult();
        //    try
        //    {
        //        List<FunctionTreeTableOutputDto> list = await menuService.GetAllFunctionTreeTable(systemTypeId);
        //        result.Success = true;
        //        result.ErrCode = ErrCode.successCode;
        //        result.ResData = list;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log4NetHelper.Error("獲取選單異常", ex);
        //        result.ErrMsg = ErrCode.err40110;
        //        result.ErrCode = "40110";
        //    }
        //    return ToJsonContent(result);
        //}*/
    }
}