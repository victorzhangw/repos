using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;
using Aurocore.Security.IServices;
using System.Linq;
using Aurocore.Commons.Tree;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.Mvc.Filter;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 資料字典介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class ItemsController : AreaApiController<Items, ItemsOutputDto, ItemsInputDto, IItemsService,string>
    {

        private readonly IItemsDetailService itemsDetailService;
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_itemsDetailService"></param>
        public ItemsController(IItemsService _iService, IItemsDetailService _itemsDetailService) : base(_iService)
        {
            iService = _iService;
            itemsDetailService=_itemsDetailService;
            AuthorizeKey.ListKey = "Items/List";
            AuthorizeKey.InsertKey = "Items/Add";
            AuthorizeKey.UpdateKey = "Items/Edit";
            AuthorizeKey.UpdateEnableKey = "Items/Enable";
            AuthorizeKey.DeleteKey = "Items/Delete";
            AuthorizeKey.DeleteSoftKey = "Items/DeleteSoft";
            AuthorizeKey.ViewKey = "Items/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Items info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Items info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Items info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 按字典分類編碼查詢樹形展開，新增、修改需要
        /// </summary>
        /// <param name="itemCode">資料字典分類編碼</param>
        /// <returns></returns>
        [HttpGet("FindTreeSelectJson")]
        [NoPermissionRequired]
        public async Task<IActionResult> FindTreeSelectJson(string itemCode)
        {
            CommonResult result = new CommonResult();
            List<ItemsDetailOutputDto> list = await itemsDetailService.GetItemDetailsByItemCode(itemCode);
            var treeList = new List<TreeSelectModel>();
            foreach (ItemsDetailOutputDto item in list)
            {
                TreeSelectModel treeModel = new TreeSelectModel();
                treeModel.id = item.ItemCode;
                treeModel.text = item.ItemName;
                treeModel.parentId = item.ParentId;
                treeList.Add(treeModel);
            }
            result.ErrCode = ErrCode.err0;
            result.ResData = treeList.TreeSelectJson();
            return ToJsonContent(result);
        }

        /// <summary>
        /// 按字典分類編碼查詢,Vue element select新增、修改需要
        /// </summary>
        /// <param name="itemCode">資料字典分類編碼</param>
        /// <returns></returns>
        [HttpGet("GetListByItemCode")] 
        [NoPermissionRequired]
        public async Task<IActionResult> GetListByItemCode(string itemCode)
        {
            CommonResult result = new CommonResult();
            IEnumerable<ItemsDetailOutputDto> list = await itemsDetailService.GetItemDetailsByItemCode(itemCode);
            result.ErrCode = ErrCode.successCode;
            result.ResData = list;
            return ToJsonContent(result);
        }


        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(ItemsInputDto tinfo, string id)
        {
            CommonResult result = new CommonResult();

            Items info = iService.Get(id);
            info.FullName = tinfo.FullName;
            info.EnCode = tinfo.EnCode;
            info.ParentId = tinfo.ParentId;
            info.EnabledMark = tinfo.EnabledMark;
            info.IsTree = tinfo.IsTree;
            info.SortCode = tinfo.SortCode;
            info.Description = tinfo.Description;


            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 獲取功能選單適用於Vue 樹形列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllItemsTreeTable")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetAllItemsTreeTable()
        {
            CommonResult result = new CommonResult();
            try
            {
                List<ItemsOutputDto> list = await iService.GetAllItemsTreeTable();
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = list;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取選單異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }
    }
}