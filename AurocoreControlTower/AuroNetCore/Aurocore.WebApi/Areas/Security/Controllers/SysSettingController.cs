using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Extend;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Security.Application;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;
using Aurocore.WebApi.Areas.Security.Models;

namespace Aurocore.WebApi.Areas.Security
{
    /// <summary>
    /// 系統基本資訊
    /// </summary>
    [Route("api/Security/[controller]")]
    [ApiController]
    public class SysSettingController : ApiController
    {

        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IUserService userService;
        private readonly IMenuService menuService;
        private readonly IRoleService roleService;
        private readonly ILogService logService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostingEnvironment"></param>
        /// <param name="_userService"></param>
        /// <param name="_menuService"></param>
        /// <param name="_roleService"></param>
        /// <param name="_logService"></param>
        public SysSettingController(IWebHostEnvironment hostingEnvironment, IUserService _userService, IMenuService _menuService, IRoleService _roleService, ILogService _logService)
        {
            _hostingEnvironment = hostingEnvironment;
            userService = _userService;
            menuService = _menuService;
            roleService = _roleService;
            logService = _logService;
        }

        /// <summary>
        /// 獲取系統資訊
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetSysInfo")]
        [AurocoreAuthorize("GetSysInfo")]
        public async Task<IActionResult> GetSysInfo()
        {
            CommonResult result = new CommonResult();
            try
            {
                SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
                AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                AurocoreCacheHelper.Add("SysSetting", sysSetting);
                DashboardOutModel dashboardOutModel = new DashboardOutModel();
                dashboardOutModel.CertificatedCompany = sysSetting.CompanyName;
                dashboardOutModel.WebUrl = sysSetting.WebUrl;
                dashboardOutModel.Title = sysSetting.SoftName;
                dashboardOutModel.MachineName = Environment.MachineName;
                dashboardOutModel.ProcessorCount= Environment.ProcessorCount;
                dashboardOutModel.SystemPageSize = Environment.SystemPageSize;
                dashboardOutModel.WorkingSet = Environment.WorkingSet;
                dashboardOutModel.TickCount = Environment.TickCount;
                dashboardOutModel.RunTimeLength = (Environment.TickCount/1000).ToBrowseTime();
                dashboardOutModel.FrameworkDescription = RuntimeInformation.FrameworkDescription;
                dashboardOutModel.OSName = RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? "Linux" : RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "OSX" : "Windows";
                dashboardOutModel.OSDescription = RuntimeInformation.OSDescription + " " + RuntimeInformation.OSArchitecture;
                dashboardOutModel.OSArchitecture = RuntimeInformation.OSArchitecture.ToString();
                dashboardOutModel.ProcessArchitecture = RuntimeInformation.ProcessArchitecture.ToString();
                
                dashboardOutModel.Directory = AppContext.BaseDirectory;
                Version version = Environment.Version;
                dashboardOutModel.SystemVersion = version.Major+"."+version.Minor+"."+version.Build;
                dashboardOutModel.Version = AppVersionHelper.Version;
                dashboardOutModel.Manufacturer = AppVersionHelper.Manufacturer;
                dashboardOutModel.WebSite = AppVersionHelper.WebSite;
                dashboardOutModel.UpdateUrl = AppVersionHelper.UpdateUrl;
                dashboardOutModel.IPAdress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                dashboardOutModel.Port = Request.HttpContext.Connection.LocalPort.ToString();
                dashboardOutModel.TotalUser = await userService.GetCountByWhereAsync("1=1");
                dashboardOutModel.TotalModule = await menuService.GetCountByWhereAsync("1=1");
                dashboardOutModel.TotalRole = await roleService.GetCountByWhereAsync("1=1");
                dashboardOutModel.TotalLog = 0;// await logService.GetCountByWhereAsync("1=1");
                result.ResData = dashboardOutModel;
                result.ErrCode = ErrCode.successCode;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取系統資訊異常", ex);
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 獲取系統基本資訊不完整資訊
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetInfo")]
        [NoPermissionRequired]
        public IActionResult GetInfo()
        {
            CommonResult result = new CommonResult();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            SysSettingOutputDto sysSettingOutputDto = new SysSettingOutputDto();
            if (sysSetting == null)
            {
                sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            }
            sysSetting.Email = "";
            sysSetting.Emailsmtp = "";
            sysSetting.Emailpassword = "";
            sysSetting.Smspassword = "";
            sysSetting.SmsSignName = "";
            sysSetting.Smsusername = "";
            sysSettingOutputDto = sysSetting.MapTo<SysSettingOutputDto>();
            if (sysSettingOutputDto != null)
            {
                sysSettingOutputDto.CopyRight= UIConstants.CopyRight;
                result.ResData = sysSettingOutputDto;
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 獲取系統基本資訊
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllInfo")]
        [AurocoreAuthorize("GetSysInfo")]
        public IActionResult GetAllInfo()
        {
            CommonResult result = new CommonResult();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            SysSettingOutputDto sysSettingOutputDto = new SysSettingOutputDto();
            if (sysSetting == null)
            {
                sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            }

            //對關鍵資訊解密
            /*
            if (!string.IsNullOrEmpty(sysSetting.Email))
                sysSetting.Email = DEncrypt.Decrypt(sysSetting.Email);
            if (!string.IsNullOrEmpty(sysSetting.Emailsmtp))
                sysSetting.Emailsmtp = DEncrypt.Decrypt(sysSetting.Emailsmtp);
            if (!string.IsNullOrEmpty(sysSetting.Emailpassword))
                sysSetting.Emailpassword = DEncrypt.Decrypt(sysSetting.Emailpassword);
            if (!string.IsNullOrEmpty(sysSetting.Smspassword))
                sysSetting.Smspassword = DEncrypt.Decrypt(sysSetting.Smspassword);
            if (!string.IsNullOrEmpty(sysSetting.Smsusername))
                sysSetting.Smsusername = DEncrypt.Decrypt(sysSetting.Smsusername);
            sysSettingOutputDto = sysSetting.MapTo<SysSettingOutputDto>();*/
            if (!string.IsNullOrEmpty(sysSetting.Email))
                sysSetting.Email = (sysSetting.Email);
            if (!string.IsNullOrEmpty(sysSetting.Emailsmtp))
                sysSetting.Emailsmtp = (sysSetting.Emailsmtp);
            if (!string.IsNullOrEmpty(sysSetting.Emailpassword))
                sysSetting.Emailpassword = (sysSetting.Emailpassword);
            if (!string.IsNullOrEmpty(sysSetting.Smspassword))
                sysSetting.Smspassword = (sysSetting.Smspassword);
            if (!string.IsNullOrEmpty(sysSetting.Smsusername))
                sysSetting.Smsusername = (sysSetting.Smsusername);
            if (!string.IsNullOrEmpty(sysSetting.ReceiveEmailimap))
                sysSetting.Smsusername = (sysSetting.ReceiveEmailimap);
            if (!string.IsNullOrEmpty(sysSetting.ReceiveEmailpassword))
                sysSetting.Smsusername = (sysSetting.ReceiveEmailpassword);
            if (!string.IsNullOrEmpty(sysSetting.ReceiveEmailport))
                sysSetting.Smsusername = (sysSetting.ReceiveEmailport);
            if (!string.IsNullOrEmpty(sysSetting.ReceiveEmailssl))
                sysSetting.Smsusername = (sysSetting.ReceiveEmailssl);
            if (!string.IsNullOrEmpty(sysSetting.ReceiveEmailusername))
                sysSetting.Smsusername = (sysSetting.ReceiveEmailusername);
            sysSettingOutputDto = sysSetting.MapTo<SysSettingOutputDto>();
            if (sysSettingOutputDto != null)
            {
                sysSettingOutputDto.CopyRight = UIConstants.CopyRight;
                result.ResData = sysSettingOutputDto;
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 儲存系統設定資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost("Save")]
        [AurocoreAuthorize("Edit")]
        public IActionResult Save(SysSetting info)
        {
            CommonResult result = new CommonResult();
            info.LocalPath = _hostingEnvironment.WebRootPath;
            SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            sysSetting = info;
            //對關鍵資訊加密
            /*
            if(!string.IsNullOrEmpty(info.Email))
            sysSetting.Email = DEncrypt.Encrypt(info.Email);
            if (!string.IsNullOrEmpty(info.Emailsmtp))
                sysSetting.Emailsmtp = DEncrypt.Encrypt(info.Emailsmtp);
            if (!string.IsNullOrEmpty(info.Emailpassword))
                sysSetting.Emailpassword = DEncrypt.Encrypt(info.Emailpassword);
            if (!string.IsNullOrEmpty(info.Smspassword))
                sysSetting.Smspassword = DEncrypt.Encrypt(info.Smspassword);
            if (!string.IsNullOrEmpty(info.Smsusername))
                sysSetting.Smsusername = DEncrypt.Encrypt(info.Smsusername);*/
            if (!string.IsNullOrEmpty(info.Email))
                sysSetting.Email = (info.Email);
            if (!string.IsNullOrEmpty(info.Emailsmtp))
                sysSetting.Emailsmtp = (info.Emailsmtp);
            if (!string.IsNullOrEmpty(info.Emailpassword))
                sysSetting.Emailpassword = (info.Emailpassword);
            if (!string.IsNullOrEmpty(info.Smspassword))
                sysSetting.Smspassword = (info.Smspassword);
            if (!string.IsNullOrEmpty(info.Smsusername))
                sysSetting.Smsusername = (info.Smsusername);
            string uploadPath = _hostingEnvironment.WebRootPath + "/" + sysSetting.Filepath;
            if (!Directory.Exists(uploadPath))
            {
                Directory.CreateDirectory(uploadPath);
            }
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            if (AurocoreCacheHelper.Exists("SysSetting"))
            {
                AurocoreCacheHelper.Replace("SysSetting", sysSetting);
            }
            else
            {
                //寫入快取
                AurocoreCacheHelper.Add("SysSetting", sysSetting);
            }
            XmlConverter.Serialize<SysSetting>(sysSetting, "xmlconfig/sys.config");
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }
    }
}