using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;
using Aurocore.Security.IServices;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.AspNetCore.ViewModel;
using System.Linq;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Core.Dtos;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 功能選單介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class MenuController : AreaApiController<Menu, MenuOutputDto, MenuInputDto, IMenuService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public MenuController(IMenuService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Menu/List";
            AuthorizeKey.InsertKey = "Menu/Add";
            AuthorizeKey.UpdateKey = "Menu/Edit";
            AuthorizeKey.UpdateEnableKey = "Menu/Enable";
            AuthorizeKey.DeleteKey = "Menu/Delete";
            AuthorizeKey.DeleteSoftKey = "Menu/DeleteSoft";
            AuthorizeKey.ViewKey = "Menu/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Menu info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
            if (string.IsNullOrEmpty(info.ParentId))
            {
                info.Layers = 1;
                info.ParentId = "";
            }
            else
            {
                info.Layers = iService.Get(info.ParentId).Layers + 1;
            }

            if (info.MenuType == "F")
            {
                info.IsFrame = false;
                info.Component = "";
                info.UrlAddress = "";
            }

        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost("Insert")]
        [AurocoreAuthorize("Add")]
        public override async Task<IActionResult> InsertAsync(MenuInputDto info)
        {
            CommonResult result = new CommonResult();
            Menu menu = info.MapTo<Menu>();
            long ln = 0;
            if (info.IsBatch)
            {
                string strEnCode = info.EnCode;
                Menu listInfo = new Menu();
                listInfo = menu;
                listInfo.FullName ="列表";
                listInfo.EnCode = strEnCode + "/List";
                listInfo.Icon = "";
                OnBeforeInsert(listInfo);
                string listId = info.ParentId;
                ln = iService.Insert(listInfo);

                Menu addInfo = new Menu();
                addInfo = menu;
                addInfo.Id = GuidUtils.CreateNo();
                addInfo.FullName = "新增";
                addInfo.EnCode = strEnCode + "/Add";
                addInfo.ParentId = listId;
                addInfo.Icon = "el-icon-plus";
                addInfo.SortCode = 1;
                OnBeforeInsert(addInfo);
                ln = iService.Insert(addInfo);

                Menu editnfo = new Menu();
                editnfo = menu;
                editnfo.Id = GuidUtils.CreateNo();
                editnfo.FullName = "修改";
                editnfo.EnCode = strEnCode + "/Edit";
                editnfo.ParentId = listId;
                editnfo.Icon = "el-icon-edit";
                editnfo.SortCode = 2;
                OnBeforeInsert(editnfo);
                ln = iService.Insert(editnfo);


                Menu enableInfo = new Menu();
                enableInfo = menu;
                enableInfo.Id = GuidUtils.CreateNo();
                enableInfo.FullName = "禁用";
                enableInfo.EnCode = strEnCode + "/Enable";
                enableInfo.ParentId = listId;
                enableInfo.Icon = "el-icon-video-pause";
                enableInfo.SortCode = 3;
                OnBeforeInsert(enableInfo);
                ln = iService.Insert(enableInfo);


                Menu enableInfo1 = new Menu();
                enableInfo1 = menu;
                enableInfo1.Id = GuidUtils.CreateNo();
                enableInfo1.FullName = "啟用";
                enableInfo1.EnCode = strEnCode + "/Enable";
                enableInfo1.ParentId = listId;
                enableInfo1.Icon = "el-icon-video-play";
                enableInfo1.SortCode = 4;
                OnBeforeInsert(enableInfo1);
                ln = iService.Insert(enableInfo1);


                Menu deleteSoftInfo = new Menu();
                deleteSoftInfo = menu;
                deleteSoftInfo.Id = GuidUtils.CreateNo();
                deleteSoftInfo.FullName = "軟刪除";
                deleteSoftInfo.EnCode = strEnCode + "/DeleteSoft";
                deleteSoftInfo.ParentId = listId;
                deleteSoftInfo.Icon = "el-icon-delete";
                deleteSoftInfo.SortCode = 5;
                OnBeforeInsert(deleteSoftInfo);
                ln = iService.Insert(deleteSoftInfo);


                Menu deleteInfo = new Menu();
                deleteInfo = menu;
                deleteInfo.Id = GuidUtils.CreateNo();
                deleteInfo.FullName = "刪除";
                deleteInfo.EnCode = strEnCode + "/Delete";
                deleteInfo.ParentId = listId;
                deleteInfo.Icon = "el-icon-delete";
                deleteInfo.SortCode = 6;
                OnBeforeInsert(deleteInfo);
                ln = iService.Insert(deleteInfo);
            }
            else
            {
                OnBeforeInsert(menu);
                ln = await iService.InsertAsync(menu);
            }

            if (ln >= 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Menu info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;

            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
            if (string.IsNullOrEmpty(info.ParentId))
            {
                info.Layers = 1;
                info.ParentId = "";
            }
            else
            {
                info.Layers = iService.Get(info.ParentId).Layers + 1;
            }
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Menu info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(MenuInputDto tinfo, string id)
        {
            CommonResult result = new CommonResult();

            Menu info = iService.Get(id);
            info.FullName = tinfo.FullName;
            info.EnCode = tinfo.EnCode;
            info.SystemTypeId = tinfo.SystemTypeId;
            info.ParentId= tinfo.ParentId;
            info.Icon = tinfo.Icon;
            info.EnabledMark = tinfo.EnabledMark;
            info.SortCode = tinfo.SortCode;
            info.Description = tinfo.Description;
            info.MenuType = tinfo.MenuType;
            if (info.MenuType == "F")
            {
                info.IsFrame = false;
                info.Component = "";
                info.UrlAddress = "";
            }
            else
            {
                info.Component = tinfo.Component;
                info.IsFrame = tinfo.IsFrame;
                info.UrlAddress = tinfo.UrlAddress;
            }
            info.IsShow = tinfo.IsShow;


            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 獲取功能選單適用於Vue 樹形列表
        /// </summary>
        /// <param name="systemTypeId">子系統Id</param>
        /// <returns></returns>
        [HttpGet("GetAllMenuTreeTable")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetAllMenuTreeTable(string systemTypeId)
        {
            CommonResult result = new CommonResult();
            try
            {
                List<MenuTreeTableOutputDto> list = await iService.GetAllMenuTreeTable(systemTypeId);
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = list;
            }catch(Exception ex)
            {
                Log4NetHelper.Error("獲取選單異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 非同步批量物理刪除
        /// </summary>
        /// <param name="info"></param>
        [HttpDelete("DeleteBatchAsync")]
        [AurocoreAuthorize("Delete")]
        public override async Task<IActionResult> DeleteBatchAsync(DeletesInputDto info)
        {
            CommonResult result = new CommonResult();
             
            if (info.Ids.Length>0)
            {
                result = await iService.DeleteBatchWhereAsync(info).ConfigureAwait(false);
                if (result.Success)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                }
                else
                {
                    result.ErrCode = "43003";
                }
            }
            return ToJsonContent(result);
        }

    }
}