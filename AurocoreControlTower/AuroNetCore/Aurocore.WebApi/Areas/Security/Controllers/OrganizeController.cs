using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Core.Dtos;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 組織機構介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class OrganizeController : AreaApiController<Organize, OrganizeOutputDto, OrganizeInputDto, IOrganizeService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public OrganizeController(IOrganizeService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Organize/List";
            AuthorizeKey.InsertKey = "Organize/Add";
            AuthorizeKey.UpdateKey = "Organize/Edit";
            AuthorizeKey.UpdateEnableKey = "Organize/Enable";
            AuthorizeKey.DeleteKey = "Organize/Delete";
            AuthorizeKey.DeleteSoftKey = "Organize/DeleteSoft";
            AuthorizeKey.ViewKey = "Organize/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Organize info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
            if (string.IsNullOrEmpty(info.ParentId))
            {
                info.Layers = 1;
                info.ParentId = "";
            }
            else
            {
                info.Layers = iService.Get(info.ParentId).Layers + 1;
            }

        }

        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Organize info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
            if (string.IsNullOrEmpty(info.ParentId))
            {
                info.Layers = 1;
                info.ParentId = "";
            }
            else
            {
                info.Layers = iService.Get(info.ParentId).Layers + 1;
            }
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Organize info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(OrganizeInputDto tinfo, string id)
        {
            CommonResult result = new CommonResult();

            Organize info = iService.Get(id);
            info.ParentId = tinfo.ParentId;
            info.FullName = tinfo.FullName;
            info.EnCode = tinfo.EnCode;
            info.ShortName = tinfo.ShortName;
            info.CategoryId = tinfo.CategoryId;
            info.ManagerId = tinfo.ManagerId;
            info.TelePhone = tinfo.TelePhone;
            info.MobilePhone = tinfo.MobilePhone;
            
            info.Fax = tinfo.Fax;
            info.Email = tinfo.Email;
            info.Address = tinfo.Address;
            info.AllowEdit = tinfo.AllowEdit;
            info.AllowDelete = tinfo.AllowDelete;
            info.ManagerId = tinfo.ManagerId;
            info.EnabledMark = tinfo.EnabledMark;
            info.DeleteMark = tinfo.DeleteMark;
            info.SortCode = tinfo.SortCode;
            info.Description = tinfo.Description;

            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 獲取組織機構適用於Vue 樹形列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllOrganizeTreeTable")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetAllOrganizeTreeTable()
        {
            CommonResult result = new CommonResult();
            try
            {
                List<OrganizeOutputDto> list = await iService.GetAllOrganizeTreeTable();
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = list;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取組織結構異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 獲取組織機構適用於Vue Tree樹形
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAllOrganizeTree")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetAllOrganizeTree()
        {
            CommonResult result = new CommonResult();
            try
            {
                List<OrganizeOutputDto> list = await iService.GetAllOrganizeTreeTable();
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = list;
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲取組織結構異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 非同步批量物理刪除
        /// </summary>
        /// <param name="info"></param>
        [HttpDelete("DeleteBatchAsync")]
        [AurocoreAuthorize("Delete")]
        public override async Task<IActionResult> DeleteBatchAsync(DeletesInputDto info)
        {
            CommonResult result = new CommonResult();

            if (info.Ids.Length > 0)
            {
                result = await iService.DeleteBatchWhereAsync(info).ConfigureAwait(false);
                if (result.Success)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                }
                else
                {
                    result.ErrCode = "43003";
                }
            }
            return ToJsonContent(result);
        }
    }
}