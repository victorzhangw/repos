using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;
using Aurocore.WebApi.Areas.Security.Models;
using iText.Layout.Element;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Exchange.WebServices.Data;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 使用者介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class UserDefinedController : AreaApiController<UserDefined, UserDefinedOutputDto, UserDefinedInputDto, IUserDefinedService,string>
    {
        
        private IUserDefinedService userDefinedService;
        private IUserService userService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_userService"></param>

        public UserDefinedController(IUserDefinedService _iService, IUserService _userService) : base(_iService)
        {
            iService = _iService;
            userService = _userService;
            AuthorizeKey.ListKey = "UserDefined/List";
            AuthorizeKey.InsertKey = "UserDefined/Add";
            AuthorizeKey.UpdateKey = "UserDefined/Edit";
            AuthorizeKey.UpdateEnableKey = "UserDefined/Enable";
            AuthorizeKey.DeleteKey = "UserDefined/Delete";
            AuthorizeKey.DeleteSoftKey = "UserDefined/DeleteSoft";
            AuthorizeKey.ViewKey = "UserDefined/View";
            
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(UserDefined info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            
            info.DeleteMark = false;
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(UserDefined info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
            
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(UserDefined info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }



        /// <summary>
        /// 非同步新增資料
        /// 新增自定義字串，若為多組Id 組合 ，用 “;” 分開
        /// </summary>
        /// <param name="tinfo"></param>
        /// <remarks name="UserId">登入者 Id</remarks>
        /// <remarks name="DictId"> 險種 Id</remarks>
        /// <remarks name="CustomSetting"> 各設定值 Id（以逗號分開）</remarks>
        /// <returns></returns>
        [HttpPost("Insert")]
        [AurocoreAuthorize("Add")]
        public override async Task<IActionResult> InsertAsync(UserDefinedInputDto tinfo)
        {
            CommonResult result = new CommonResult();

            if (!string.IsNullOrEmpty(tinfo.UserId)&& !string.IsNullOrEmpty(tinfo.DictId))
            {
                string where = string.Format("Id='{0}'", tinfo.UserId);
                User user = userService.GetWhere(where);
                if (user == null)
                {
                    result.ErrMsg = "使用者 Id 不存在";
                    return ToJsonContent(result);
                }
            }
            else
            {
                result.ErrMsg = "使用者 Id/ 字典 Id不能為空值";
                return ToJsonContent(result);
            }
            if (!string.IsNullOrEmpty(tinfo.CustomSetting))
            {
                
                var filterAry = tinfo.CustomSetting.Split(",");
                List<string> csList =filterAry.ToList();
                for(int i = 0; i < csList.Count; i++)
                {
                    var dictAry = csList[i].Split("|");
                    if (string.IsNullOrEmpty(dictAry[1]) || dictAry[1] == "undefined")
                    {
                        csList.Remove(csList[i]);
                    }
                } 
                
               tinfo.CustomSetting= String.Join(",", csList);
            }
            
            UserDefined info = tinfo.MapTo<UserDefined>();
            OnBeforeInsert(info);
            
            long ln = await iService.InsertAsync(info).ConfigureAwait(true);
            
            
            if (ln>0)
            {
               
                result.Success=true;
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.Success = false;
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 取得所有自定義資料
        /// </summary>
        /// <param name="tinfo">登入者 Id</param>
        /// <returns></returns>
        [HttpPost("GetAllByUserId")]
        [AurocoreAuthorize("List")]
        public  async Task<IActionResult> GetAllByUserId(UserDefinedInputDto tinfo)
        {
            CommonResult result = new CommonResult();
            var lst =iService.GetAllByUserId(tinfo.UserId);
            if (lst.Count > 0)
            {
                result.Success = true;
                result.ErrCode= ErrCode.successCode;
                result.ResData = lst;
            }
            else
            {
                result.Success = false;
                result.ErrCode = ErrCode.err1;
                result.ErrCode = ErrCode.err60001;
            }
            return ToJsonContent(result, true);
        }
        /// <summary>
        /// 取得特定資料字典分類下全部自定義
        /// </summary>
        /// <param name="tinfo">使用者自定義</param>
        /// <remarks name="UserId">登入者 Id</remarks>

        /// <returns></returns>
        [HttpPost("GetCustoms")]
        [AurocoreAuthorize("Add")]
        public  async Task<IActionResult> GetCustoms(UserDefinedInputDto tinfo )
        {
            CommonResult result = new CommonResult();
            string userId = CurrentUser.UserId;
            var lst = iService.GetCustoms(userId, tinfo.DictId);
            if (lst.Count > 0)
            {
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = lst;
            }
            else
            {
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
                result.ResData = "";
            }
            
            return ToJsonContent(result, true);
        }










    }
}