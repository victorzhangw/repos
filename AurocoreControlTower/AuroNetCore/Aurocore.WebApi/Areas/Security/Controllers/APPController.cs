using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 應用管理介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
   
    public class APPController : AreaApiController<APP, AppOutputDto, APPInputDto, IAPPService,string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public APPController(IAPPService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "APP/List";
            AuthorizeKey.InsertKey = "APP/Add";
            AuthorizeKey.UpdateKey = "APP/Edit";
            AuthorizeKey.UpdateEnableKey = "APP/Enable";
            AuthorizeKey.DeleteKey = "APP/Delete";
            AuthorizeKey.DeleteSoftKey = "APP/DeleteSoft";
            AuthorizeKey.ViewKey = "APP/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(APP info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.AppSecret = MD5Util.GetMD5_32(GuidUtils.NewGuidFormatN()).ToUpper();
            if (info.IsOpenAEKey)
            {
                info.EncodingAESKey = MD5Util.GetMD5_32(GuidUtils.NewGuidFormatN()).ToUpper();
            }
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.CompanyId = CurrentUser.OrganizeId;
            info.DeptId = CurrentUser.DeptId;
            info.DeleteMark = false;
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(APP info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(APP info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(APPInputDto tinfo, string id)
        {
            CommonResult result = new CommonResult();

            APP info = iService.Get(id);
            info.AppId = tinfo.AppId;
            info.RequestUrl = tinfo.RequestUrl;
            info.Token = tinfo.Token;
            info.EnabledMark = tinfo.EnabledMark;
            info.Description = tinfo.Description;

            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(true);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }



        /// <summary>
        /// 重置AppSecret
        /// </summary>
        /// <returns></returns>
        [HttpGet("ResetAppSecret")]
        [AurocoreAuthorize("ResetAppSecret")]
        public async Task<IActionResult> ResetAppSecret(string id)
        {
            CommonResult result = new CommonResult();
            APP aPP = iService.Get(id);
            aPP.AppSecret = MD5Util.GetMD5_32(GuidUtils.NewGuidFormatN()).ToUpper();
            bool bl = await iService.UpdateAsync(aPP, id);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = aPP.AppSecret;
                result.Success = true;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 重置訊息加密金鑰EncodingAESKey
        /// </summary>
        /// <returns></returns>
        [HttpGet("ResetEncodingAESKey")]
        [AurocoreAuthorize("ResetEncodingAESKey")]
        public async Task<IActionResult> ResetEncodingAESKey(string id)
        {
            CommonResult result = new CommonResult();
            APP aPP = iService.Get(id);
            aPP.EncodingAESKey = MD5Util.GetMD5_32(GuidUtils.NewGuidFormatN()).ToUpper();
            bool bl = await iService.UpdateAsync(aPP, id);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = aPP.EncodingAESKey;
                result.Success = true;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
    }
}