using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.ViewModel;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 定時任務執行日誌介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class TaskJobsLogController : AreaApiController<TaskJobsLog, TaskJobsLogOutputDto,TaskJobsLogInputDto,ITaskJobsLogService,string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public TaskJobsLogController(ITaskJobsLogService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "TaskJobsLog/List";
            AuthorizeKey.InsertKey = "TaskJobsLog/Add";
            AuthorizeKey.UpdateKey = "TaskJobsLog/Edit";
            AuthorizeKey.UpdateEnableKey = "TaskJobsLog/Enable";
            AuthorizeKey.DeleteKey = "TaskJobsLog/Delete";
            AuthorizeKey.DeleteSoftKey = "TaskJobsLog/DeleteSoft";
            AuthorizeKey.ViewKey = "TaskJobsLog/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(TaskJobsLog info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(TaskJobsLog info)
        {
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(TaskJobsLog info)
        {
        }


        /// <summary>
        /// 根據任務Id查詢最新的30條日誌
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet("FindWithByTaskIdAsync")]
        [AurocoreAuthorize("List")]
        public  async Task<IActionResult> FindWithByTaskIdAsync([FromQuery] SearchModel search)
        {
            CommonResult result = new CommonResult();
            string where = "";
            if (!string.IsNullOrEmpty(search.Keywords))
            {
                where += string.Format(" TaskId ='{0}' ", search.Keywords);
            }
            where += " order by CreatorTime desc";
            IEnumerable<TaskJobsLog> list = await iService.GetListTopWhereAsync(40,where);
            List<TaskJobsLogVueTimelineOutputDto> resultList = list.MapTo<TaskJobsLogVueTimelineOutputDto>();
            result.ResData = resultList;
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }
    }
}