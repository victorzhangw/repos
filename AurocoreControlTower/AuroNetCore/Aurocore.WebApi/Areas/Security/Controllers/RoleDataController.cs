using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;
using Aurocore.Security.IServices;
using Aurocore.AspNetCore.Mvc;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 角色資料許可權介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class RoleDataController : AreaApiController<RoleData, RoleDataOutputDto, RoleDataInputDto, IRoleDataService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public RoleDataController(IRoleDataService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "RoleData/List";
            AuthorizeKey.InsertKey = "RoleData/Add";
            AuthorizeKey.UpdateKey = "RoleData/Edit";
            AuthorizeKey.UpdateEnableKey = "RoleData/Enable";
            AuthorizeKey.DeleteKey = "RoleData/Delete";
            AuthorizeKey.DeleteSoftKey = "RoleData/DeleteSoft";
            AuthorizeKey.ViewKey = "RoleData/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(RoleData info)
        {
            info.Id = GuidUtils.CreateNo();
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(RoleData info)
        {
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(RoleData info)
        {
        }


        /// <summary>
        /// 角色可以職位問資料
        /// </summary>
        /// <param name="roleId">角色Id</param>
        /// <returns></returns>
        [HttpGet("GetAllRoleDataByRoleId")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetAllRoleDataByRoleId(string roleId)
        {
            CommonResult result = new CommonResult();
            string where = string.Format("RoleId='{0}'", roleId); 
            List<string> resultlist = new List<string>();
            IEnumerable<RoleData> list =await iService.GetListWhereAsync(where);
            foreach (RoleData info in list)
            {
                resultlist.Add(info.AuthorizeData);
            }
            result.ResData = resultlist;
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }
    }
}