using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Encrypt;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 子系統介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class SystemTypeController : AreaApiController<SystemType, SystemTypeOutputDto, SystemTypeInputDto, ISystemTypeService,string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public SystemTypeController(ISystemTypeService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "SystemType/List";
            AuthorizeKey.InsertKey = "SystemType/Add";
            AuthorizeKey.UpdateKey = "SystemType/Edit";
            AuthorizeKey.UpdateEnableKey = "SystemType/Enable";
            AuthorizeKey.DeleteKey = "SystemType/Delete";
            AuthorizeKey.DeleteSoftKey = "SystemType/DeleteSoft";
            AuthorizeKey.ViewKey = "SystemType/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(SystemType info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(SystemType info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(SystemType info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }

        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(SystemTypeInputDto tinfo, string id)
        {
            CommonResult result = new CommonResult();

            SystemType info = iService.Get(id);
            info.FullName = tinfo.FullName;
            info.EnCode = tinfo.EnCode;
            info.Url = tinfo.Url;
            info.AllowEdit = tinfo.AllowEdit;
            info.AllowDelete = tinfo.AllowDelete;
            info.SortCode = tinfo.SortCode;
            info.EnabledMark = tinfo.EnabledMark;
            info.Description = tinfo.Description;

            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(true);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 獲取所有子系統
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetSubSystemList")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetSubSystemList()
        {
            CommonResult result = new CommonResult();
            try
            {
                IEnumerable<SystemType> list = await iService.GetAllAsync();
                result.ErrCode = ErrCode.successCode;
                result.ResData = list.MapTo<SystemTypeOutputDto>();
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("獲子系統異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 系統切換時獲取憑據
        /// 適用於不同子系統分別獨立部署站點場景
        /// </summary>
        /// <param name="systype"></param>
        /// <returns></returns>
        [HttpGet("AurocoreConnecSys")]
        [AurocoreAuthorize("")]
        public  IActionResult AurocoreConnecSys(string systype)
        {
            CommonResult result = new CommonResult();
            try
            {
                if (!string.IsNullOrEmpty(systype))
                {
                    SystemType systemType = iService.GetByCode(systype);
                    string openmf = MD5Util.GetMD5_32(DEncrypt.Encrypt(CurrentUser.UserId + systemType.Id, GuidUtils.NewGuidFormatN())).ToLower();
                    AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                    TimeSpan expiresSliding = DateTime.Now.AddSeconds(10) - DateTime.Now;
                    AurocoreCacheHelper.Add("openmf" + openmf, CurrentUser.UserId,expiresSliding, false);
                    result.ErrCode = ErrCode.successCode;
                    result.ResData = systemType.Url + "?openmf=" + openmf;
                }
                else
                {
                    result.ErrCode = ErrCode.failCode;
                    result.ErrMsg = "切換子系統參數錯誤";
                }
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("切換子系統異常", ex);
                result.ErrMsg = ErrCode.err40110;
                result.ErrCode = "40110";
            }
            return ToJsonContent(result);
        }
    }
}