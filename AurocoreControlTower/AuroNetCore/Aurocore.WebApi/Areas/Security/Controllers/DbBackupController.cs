using Microsoft.AspNetCore.Mvc;
using System;
using Aurocore.AspNetCore.Controllers;
using Aurocore.Commons.Helpers;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 資料庫備份介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class DbBackupController : AreaApiController<DbBackup, DbBackupOutputDto, DbBackupInputDto, IDbBackupService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public DbBackupController(IDbBackupService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "DbBackup/List";
            AuthorizeKey.InsertKey = "DbBackup/Add";
            AuthorizeKey.UpdateKey = "DbBackup/Edit";
            AuthorizeKey.UpdateEnableKey = "DbBackup/Enable";
            AuthorizeKey.DeleteKey = "DbBackup/Delete";
            AuthorizeKey.DeleteSoftKey = "DbBackup/DeleteSoft";
            AuthorizeKey.ViewKey = "DbBackup/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(DbBackup info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(DbBackup info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(DbBackup info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
    }
}