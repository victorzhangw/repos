using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 日誌介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class LogController : AreaApiController<Log, LogOutputDto, LogInputDto, ILogService, string>
    {

        private IOrganizeService organizeService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_organizeService"></param>
        public LogController(ILogService _iService, IOrganizeService _organizeService) : base(_iService)
        {
            iService = _iService;
            organizeService = _organizeService;

            AuthorizeKey.ListKey = "Log/List";
            AuthorizeKey.InsertKey = "Log/Add";
            AuthorizeKey.UpdateKey = "Log/Edit";
            AuthorizeKey.UpdateEnableKey = "Log/Enable";
            AuthorizeKey.DeleteKey = "Log/Delete";
            AuthorizeKey.DeleteSoftKey = "Log/DeleteSoft";
            AuthorizeKey.ViewKey = "Log/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Log info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Log info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Log info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 非同步分頁查詢
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("FindWithPagerSearchAsync")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> FindWithPagerSearchAsync(SearchLogModel search)
        {
            CommonResult<PageResult<LogOutputDto>> result = new CommonResult<PageResult<LogOutputDto>>();
            result.ResData = await iService.FindWithPagerSearchAsync(search);
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
        }
    }
}