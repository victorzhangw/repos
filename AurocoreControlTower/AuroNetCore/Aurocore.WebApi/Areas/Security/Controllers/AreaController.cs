using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;
using Aurocore.Security.IServices;

namespace Aurocore.WebApi.Areas.Security.Controllers
{
    /// <summary>
    /// 地區介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AreaController : AreaApiController<Area, AreaOutputDto, AreaInputDto, IAreaService, string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public AreaController(IAreaService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Area/List";
            AuthorizeKey.InsertKey = "Area/Add";
            AuthorizeKey.UpdateKey = "Area/Edit";
            AuthorizeKey.UpdateEnableKey = "Area/Enable";
            AuthorizeKey.DeleteKey = "Area/Delete";
            AuthorizeKey.DeleteSoftKey = "Area/DeleteSoft";
            AuthorizeKey.ViewKey = "Area/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Area info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Area info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Area info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
    }
}