using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.BulletinBoard.Dtos;
using Aurocore.BulletinBoard.Models;
using Aurocore.BulletinBoard.IServices;
using Aurocore.AspNetCore.Mvc.Filter;
using Microsoft.AspNetCore.Authorization;
using Aurocore.AspNetCore.Mvc;
using Aurocore.BulletinBoard.Enums;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using System.Dynamic;
using Newtonsoft.Json;
using Aurocore.Commons.Cache;
using Aurocore.Security.Models;
using Aurocore.Commons.Json;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Enums;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Models;
using Aurocore.WebApi.Hubs;
using Microsoft.AspNetCore.SignalR;
namespace Aurocore.WebApi.Areas.BulletinBoard.Controllers
{
   
    /// <summary>
    /// 系統公告接口
    /// </summary>
    [ApiController]
    [Route("api/BulletinBoard/[controller]/")]
   
    public class BulletinController : AreaApiController<Sys_Bulletin, Sys_BulletinOutputDto,Sys_BulletinInputDto,ISys_BulletinService,string>
    {
        private readonly IWorkOrderDetailService workOrderDetailService;
        private readonly IWorkOrderService workOrderService;
        private readonly IHubContext<BroadcastHub> hubContext;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_WorkOrderDetailService"></param>
        /// <param name="_WorkOrderService"></param>
        /// <param name="_hubContext"></param>
        public BulletinController(ISys_BulletinService _iService, IWorkOrderDetailService _WorkOrderDetailService,
            IWorkOrderService _WorkOrderService, IHubContext<BroadcastHub> _hubContext) : base(_iService)
        {
            iService = _iService;
            workOrderDetailService = _WorkOrderDetailService;
            workOrderService = _WorkOrderService;
            hubContext = _hubContext;
            AuthorizeKey.ListKey = "Bulletin/List";
            AuthorizeKey.InsertKey = "Bulletin/Add";
            AuthorizeKey.UpdateKey = "Bulletin/Edit";
            AuthorizeKey.UpdateEnableKey = "Bulletin/Enable";
            AuthorizeKey.DeleteKey = "Bulletin/Delete";
            AuthorizeKey.DeleteSoftKey = "Bulletin/DeleteSoft";
            AuthorizeKey.ViewKey = "Bulletin/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Sys_Bulletin info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Sys_Bulletin info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Sys_Bulletin info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 獲取公告欄固定筆數資料
        /// </summary>
        /// <param name="limits"></param>
        /// <returns></returns>
        [HttpGet("GetBulletinbyLimit")]
        [AllowAnonymous]
        public IActionResult GetBulletinbyLimit(string limits)
        {
            CommonResult result = new CommonResult();
            result.ResData =  iService.GetBulletin(limits);
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                
            }
            else
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            return ToJsonContent(result);
        }
            
        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="binfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(Sys_BulletinInputDto binfo, string id)
        {
            CommonResult result = new CommonResult();
            
            Sys_Bulletin info = iService.Get(id);
            info.Title = binfo.Title;
            info.Context = binfo.Context;
            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 設定通知不再顯示
        /// </summary>
        /// <param name="userNotification"></param>
        /// <returns></returns>
        [HttpPost("SetNotificationStatus")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> NotificationStatus(UserNotification userNotification)
        {
            CommonResult result = new CommonResult();

            // Sys_Bulletin info = iService.Get(userNotification);
            WorkOrder workOrder = new();
            WorkOrderDetail workOrderDetail = new();
            workOrder.Id = string.Empty;
            workOrderDetail.Id = string.Empty;
            switch (userNotification.ParentFormTypeName)
            {
                case nameof(WorkOrderType.Memo):
                    workOrder = workOrderService.Get(userNotification.Id);
                    break;
                case nameof(WorkOrderType.EzGo):
                    workOrder = workOrderService.Get(userNotification.Id);
                    break;
                case nameof(WorkOrderType.EFax):
                    workOrder = workOrderService.Get(userNotification.Id);
                    break;
                case nameof(WorkOrderType.OutBound):
                    workOrder = workOrderService.Get(userNotification.Id);
                    break;
                case nameof(WorkOrderType.InBound):
                    workOrder = workOrderService.Get(userNotification.Id);
                    break;
                case nameof(WorkOrderType.汽車險案件):
                    workOrder = workOrderService.Get(userNotification.Id);
                    break;
                case nameof(ResponseType.一般問題單):
                    workOrderDetail = workOrderDetailService.Get(userNotification.Id);
                    break;
                case nameof(ResponseType.批改申請單):
                    workOrderDetail = workOrderDetailService.Get(userNotification.Id);
                    break;
                case nameof(ResponseType.補發申請單):
                    workOrderDetail = workOrderDetailService.Get(userNotification.Id);
                    break;
                case nameof(ResponseType.待辦單據):
                    workOrderDetail = workOrderDetailService.Get(userNotification.Id);
                    break;
            }
            if(string.IsNullOrEmpty(workOrder.Id)  && string.IsNullOrEmpty(workOrderDetail.Id))
            {
                result.ErrMsg = "案件或單據傳入 Id 有誤，導致無法更新";
                result.ErrCode =ErrCode.err1;
                return ToJsonContent(result);
            }
            if(!string.IsNullOrEmpty(workOrder.Id))
            {
                bool bl = await iService.NotificationStatus(workOrder);
                if (bl)
                {
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    return ToJsonContent(result);
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }
            if (!string.IsNullOrEmpty(workOrderDetail.Id))
            {
                bool bl = await iService.NotificationStatus(workOrderDetail);
                if (bl)
                {
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    return ToJsonContent(result);
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }

            return ToJsonContent(result);
        }

        /// <summary>
        /// 產生 PDF
        /// </summary>
        /// <param name="baseName"></param>
        /// <param name="formType"></param>
        /// <param name="applicationForm"></param>
        /// <returns></returns>
        [HttpPost("GeneratePDF")]
        [AllowAnonymous]
        public  IActionResult  GeneratePDFbyTempalte (string baseName, [FromQuery]DocumentType formType, [FromBody] dynamic applicationForm)
        {
            var converter = new ExpandoObjectConverter();
            var data = JsonConvert.DeserializeObject<ExpandoObject>(applicationForm.ToString(), converter) as dynamic;

            //dynamic data = JsonConvert.DeserializeObject<dynamic>(applicationForm.ToString());
            CommonResult result = new CommonResult();
            try
            {
                if (string.IsNullOrEmpty(baseName))
                {
                    result.ErrMsg = "專案名稱空間不能為空";
                    result.ErrCode = ErrCode.failCode;
                }
                else
                {
                    iService.GeneratePDFFiles(baseName, formType, data);

                    var path = AppDomain.CurrentDomain.BaseDirectory;
                    var parentPath = path.Substring(0, path.LastIndexOf("\\"));
                    var servicesPath = parentPath + "\\" + formType;
                    //產生壓縮包

                    AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                    SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
                    string zipReturnFileName =  DateTime.Now.ToString("yyyyMMddHHmmss") + ".zip";
                    string zipFileBasePath = "GeneratePDF";
                    string zipFilesPath = sysSetting.LocalPath + "\\" + zipFileBasePath;
                    if (!System.IO.Directory.Exists(zipFilesPath))
                    {
                        System.IO.Directory.CreateDirectory(zipFilesPath);
                    }
                    string zipFileName = zipFilesPath + "\\" + zipReturnFileName;
                    if (System.IO.File.Exists(zipFileName))
                    {
                        System.IO.File.Delete(zipFileName);
                    }
                    FileHelper.ZipFileDirectory(servicesPath, zipFileName, 7, "", "", "*.*");
                    FileHelper.DeleteDirectory(servicesPath);
                    result.ErrCode = ErrCode.successCode;
                    result.Success = true;
                    result.ResData = new string[2] { zipFileBasePath + "/" + zipReturnFileName, zipReturnFileName };
                    
                }
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("程式碼產生異常", ex);
                result.ErrMsg = "程式碼產生異常:" + ex.Message;
                result.ErrCode = ErrCode.failCode;
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 刪除 PDF 檔 
        /// </summary>
        /// <param name="fileName">輸入 ALLPDFFiles 則為全部刪除,其餘為檔名</param>
        /// <returns></returns>
        [HttpDelete("DeletePDF")]
        [AllowAnonymous]
        public IActionResult DeletePDFs(string fileName )
        {
            CommonResult result = new CommonResult();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            string FileBasePath = "GeneratePDF";
            string FilePath = sysSetting.LocalPath + "\\" + FileBasePath;
            try
            {
                if(fileName == "ALLPDFFiles")
                {
                    FileHelper.DeleteDirectory(FilePath);
                }
                else
                {
                    if (System.IO.File.Exists(FilePath+"\\"+fileName))
                    {
                        System.IO.File.Delete(FilePath + "\\" + fileName);
                    }
                }
                result.ErrCode = ErrCode.successCode;
                result.Success = true;
                result.ResData ="Deleted";
            }
            catch(Exception ex)
            {
                Log4NetHelper.Error("程式碼產生異常", ex);
                result.ErrMsg = "程式碼產生異常:" + ex.Message;
                result.ErrCode = ErrCode.failCode;
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 獲取個人通知事項
        /// </summary>
     
        /// <returns></returns>
        [HttpPost("GetUserNotification")]
        [AurocoreAuthorize("List")]
        public  async Task<IActionResult> GetUserNotification()
        {
            CommonResult result = new CommonResult();
            SeachWorkOrder search = new();
            search.CurrentUserId = CurrentUser.UserId;
            var returnList = await iService.FindNotificationAsync(search).ConfigureAwait(false);

            if (returnList.Count > 0)
            {
                result.Success = true;

            }
            
            if (result.Success)
            {
               
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = returnList;
                result.RelationData = returnList.Count > 0 ? returnList.Count : 0;



            }
            else
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.RelationData = 0;
            }
            return ToJsonContent(result);
        }



    }
}