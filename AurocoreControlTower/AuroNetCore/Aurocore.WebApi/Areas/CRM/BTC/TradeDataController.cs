using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.CMS.IServices;

namespace Aurocore.CMSApi.Areas.CMS.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/CMS/[controller]")]
    public class TradeDataController : AreaApiController<TradeData, TradeDataOutputDto,TradeDataInputDto,ITradeDataService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public TradeDataController(ITradeDataService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "TradeData/List";
            AuthorizeKey.InsertKey = "TradeData/Add";
            AuthorizeKey.UpdateKey = "TradeData/Edit";
            AuthorizeKey.UpdateEnableKey = "TradeData/Enable";
            AuthorizeKey.DeleteKey = "TradeData/Delete";
            AuthorizeKey.DeleteSoftKey = "TradeData/DeleteSoft";
            AuthorizeKey.ViewKey = "TradeData/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(TradeData info)
        {
            info.Id = GuidUtils.CreateNo();
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(TradeData info)
        {
           
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(TradeData info)
        {
            
        }
    }
}