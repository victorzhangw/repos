using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.CMS.IServices;

namespace Aurocore.CMSApi.Areas.CMS.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/CMS/[controller]")]
    public class Ca_Ea_PstController : AreaApiController<Ca_Ea_Pst, Ca_Ea_PstOutputDto,Ca_Ea_PstInputDto,ICa_Ea_PstService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public Ca_Ea_PstController(ICa_Ea_PstService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Ca_Ea_Pst/List";
            AuthorizeKey.InsertKey = "Ca_Ea_Pst/Add";
            AuthorizeKey.UpdateKey = "Ca_Ea_Pst/Edit";
            AuthorizeKey.UpdateEnableKey = "Ca_Ea_Pst/Enable";
            AuthorizeKey.DeleteKey = "Ca_Ea_Pst/Delete";
            AuthorizeKey.DeleteSoftKey = "Ca_Ea_Pst/DeleteSoft";
            AuthorizeKey.ViewKey = "Ca_Ea_Pst/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Ca_Ea_Pst info)
        {
            info.Id = GuidUtils.CreateNo();
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Ca_Ea_Pst info)
        {
         
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Ca_Ea_Pst info)
        {
           
        }
    }
}