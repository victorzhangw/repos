using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.CMS.IServices;

namespace Aurocore.CMSApi.Areas.CMS.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/CMS/[controller]")]
    public class Estimate_Ins_TypeController : AreaApiController<Estimate_Ins_Type, Estimate_Ins_TypeOutputDto,Estimate_Ins_TypeInputDto,IEstimate_Ins_TypeService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public Estimate_Ins_TypeController(IEstimate_Ins_TypeService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Estimate_Ins_Type/List";
            AuthorizeKey.InsertKey = "Estimate_Ins_Type/Add";
            AuthorizeKey.UpdateKey = "Estimate_Ins_Type/Edit";
            AuthorizeKey.UpdateEnableKey = "Estimate_Ins_Type/Enable";
            AuthorizeKey.DeleteKey = "Estimate_Ins_Type/Delete";
            AuthorizeKey.DeleteSoftKey = "Estimate_Ins_Type/DeleteSoft";
            AuthorizeKey.ViewKey = "Estimate_Ins_Type/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Estimate_Ins_Type info)
        {
            info.Id = GuidUtils.CreateNo();
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Estimate_Ins_Type info)
        {
           
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Estimate_Ins_Type info)
        {
            
        }
    }
}