using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.CMS.IServices;

namespace Aurocore.CMSApi.Areas.CMS.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/CMS/[controller]")]
    public class Estimate_HccController : AreaApiController<Estimate_Hcc, Estimate_HccOutputDto,Estimate_HccInputDto,IEstimate_HccService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public Estimate_HccController(IEstimate_HccService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Estimate_Hcc/List";
            AuthorizeKey.InsertKey = "Estimate_Hcc/Add";
            AuthorizeKey.UpdateKey = "Estimate_Hcc/Edit";
            AuthorizeKey.UpdateEnableKey = "Estimate_Hcc/Enable";
            AuthorizeKey.DeleteKey = "Estimate_Hcc/Delete";
            AuthorizeKey.DeleteSoftKey = "Estimate_Hcc/DeleteSoft";
            AuthorizeKey.ViewKey = "Estimate_Hcc/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Estimate_Hcc info)
        {
            info.Id = GuidUtils.CreateNo();
            
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Estimate_Hcc info)
        {
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Estimate_Hcc info)
        {
            
        }
    }
}