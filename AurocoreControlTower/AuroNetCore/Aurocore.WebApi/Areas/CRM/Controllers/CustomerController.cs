using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
using Aurocore.Customers.IServices;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Dtos;
using Aurocore.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Aurocore.WorkOrders.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;

namespace Aurocore.CustomerApi.Areas.Customers.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : AreaApiController<Customer, CustomerOutputDto,CustomerInputDto,ICustomerService,string>
    {
        private readonly IPolicyService  policyService;
        private readonly ICrossAreaService crossAreaService;
        private readonly IWorkOrderService workOrderService;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_policyService"></param>
        /// <param name="_crossAreaService"></param>
        /// <param name="_workOrderService"></param>
        public CustomerController(ICustomerService _iService,IPolicyService _policyService,
            ICrossAreaService _crossAreaService,IWorkOrderService _workOrderService) : base(_iService)
        {
            iService = _iService;
            policyService = _policyService;
            crossAreaService = _crossAreaService;
            workOrderService = _workOrderService;
            AuthorizeKey.ListKey = "Customer/List";
            AuthorizeKey.InsertKey = "Customer/Add";
            AuthorizeKey.UpdateKey = "Customer/Edit";
            AuthorizeKey.UpdateEnableKey = "Customer/Enable";
            AuthorizeKey.DeleteKey = "Customer/Delete";
            AuthorizeKey.DeleteSoftKey = "Customer/DeleteSoft";
            AuthorizeKey.ViewKey = "Customer/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Customer info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Customer info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Customer info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 取得客戶清單
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("GetCustomers")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public  async Task<IActionResult> GetCustomers(SearchInputDto<CrossAreaSearch> search)
        {
            CommonResult result = new CommonResult();

            var _data = await iService.FindCustomerEstimateWithPagerAsync(search);

            if (_data.TotalItems>0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg= ErrCode.err0;
                result.ResData = _data;
            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "取得資料失敗";
            }


            return ToJsonContent(result);
            /*
            CommonResult result = new CommonResult();
            result.ResData = await policyService.FindPolicyIncludeedPerson(search);
            
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);*/
        }
        /// <summary>
        /// 透過客戶取得案件清單
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("WorkOrderListbyCustomer")]
        //[AurocoreAuthorize("List")]
        [NoPermissionRequired]
        [AllowAnonymous]
        public async Task<IActionResult> WorkOrderListbyCustomer(SearchInputDto<CrossAreaSearch> search)
        {

            CommonResult result = new CommonResult();

            var _data = await crossAreaService.GetCrossCustomerDatas(search);

            if (!string.IsNullOrEmpty(_data.SearchTerm))
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = _data;
            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "取得資料失敗";
            }


            return ToJsonContent(result);
        }
        /// <summary>
        /// 透過客戶取得案件清單
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("EstimateListbyCustomer")]
        [AurocoreAuthorize("List")]
        //[NoPermissionRequired]
        [AllowAnonymous]
        public async Task<IActionResult> EstimateListbyCustomer(SearchInputDto<CrossAreaSearch> search)
        {

            CommonResult result = new CommonResult();

            var _data = await crossAreaService.GetEstimateList(search);

            if (!string.IsNullOrEmpty(_data.SearchTerm))
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = _data;
            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "取得資料失敗";
            }


            return ToJsonContent(result);
        }
        /// <summary>
        /// 透過保單取得客戶清單
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("GetCustomersbyPolicy")]
        //[AurocoreAuthorize("List")]
        [NoPermissionRequired]
        [AllowAnonymous]
        public virtual async Task<CommonResult<PageResult<PolicyOutputDto>>> GetCustomersbyPolicy(SearchInputDto<Policy> search)
        {
            CommonResult<PageResult<PolicyOutputDto>> result = new CommonResult<PageResult<PolicyOutputDto>>();
            result.ResData = await policyService.FindWithPagerAsync(search);
            /*
            CustomerOutputDto customer = new()
            {
                CName = "王大明",
                CAddress="台北市內湖區洲子街111號",
                CMobilePhone="0912111456"

            };
            result.ResData = customer;*/
            result.ErrCode = ErrCode.successCode;
            return result;
        }
        /// <summary>
        /// 透過保單取得客戶保單清單
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("GetCrossAreaCustomerDatas")]
        //[AurocoreAuthorize("List")]
        [NoPermissionRequired]
        [AllowAnonymous]
        public async Task<IActionResult> CrossAreaCustomerData(SearchInputDto<CrossAreaSearch> search)
        {

            CommonResult result = new CommonResult();

            var _data = await crossAreaService.GetCrossCustomerDatas(search);

            if (!string.IsNullOrEmpty(_data.SearchTerm))
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = _data;
            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "取得資料失敗";
            }


            return ToJsonContent(result);
        }
        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="Pid">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(CustomerInputDto tinfo, string Pid)
        {
            CommonResult result = new CommonResult();
            string _where = string.Format(" Pid='{0}'",Pid);
            Customer info = iService.GetWhere(_where);
            info.Email = tinfo.Email;
            info.MobilePhone= tinfo.MobilePhone;

            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, info.Id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
    }
}