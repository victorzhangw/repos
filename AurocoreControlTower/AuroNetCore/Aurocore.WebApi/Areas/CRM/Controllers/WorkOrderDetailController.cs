using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Cache;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.IServices;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Email.Core;
using Aurocore.WorkOrders.Enums;
using Aurocore.WorkOrders.Services;
using Aurocore.Commons.Extend;
using Aurocore.SMS.Mitake;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Aurocore.Security.Models;
using Aurocore.Security.IServices;
using Aurocore.Commons;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Json;
using Aurocore.Commons.Options;
using RestSharp;
using System.Xml;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Dtos;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Exchange.WebServices.Data;
using System.Net;
using Aurocore.WebApi.Hubs;
using Microsoft.AspNetCore.SignalR;
using Aurocore.AspNetCore.ViewModel;
using Aurocore.Commons.Extensions;
using Aurocore.WorkOrders.Helpers;
using Castle.Core.Internal;
using Aurocore.CMS.Core.Models;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace Aurocore.WebApi.Areas.CRM.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/WorkOrder/[controller]")]
    public class WorkOrderDetailController : AreaApiController<WorkOrderDetail, WorkOrderDetailOutputDto,WorkOrderDetailInputDto,IWorkOrderDetailService,string>
    {
        private readonly IWorkOrderService workOrderService;
        private readonly IWorkOrderLogService workOrderLogService;
        private readonly IUserService userService;
        private readonly IOrganizeService organizeService;
        private readonly IItemsDetailService itemsDetailService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IHubContext<BroadcastHub> hubContext;
        private readonly IUploadFileService uploadFileService;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_workorderservice"></param>
        /// <param name="_iUserService">使用者</param>
        /// <param name="_itemsDetailService"></param>
        /// <param name="_workOrderLogService"></param>
        /// <param name="hostingEnvironment"></param>
        /// <param name="_organizeService"></param>
        /// <param name="_hubContext"></param>
        /// <param name="_uploadFileService"></param>
        public WorkOrderDetailController(IWorkOrderDetailService _iService,IWorkOrderService _workorderservice
            , IUserService _iUserService, IItemsDetailService _itemsDetailService,
            IWorkOrderLogService _workOrderLogService ,IWebHostEnvironment hostingEnvironment,
            IOrganizeService _organizeService, IHubContext<BroadcastHub> _hubContext, IUploadFileService _uploadFileService) : base(_iService)
        {
            iService = _iService;
            this.workOrderService = _workorderservice;
            this.userService = _iUserService;
            this.itemsDetailService = _itemsDetailService;
            this.workOrderLogService = _workOrderLogService;
            this.organizeService = _organizeService;
            _hostingEnvironment = hostingEnvironment;
            hubContext = _hubContext;
            uploadFileService = _uploadFileService;
        }

        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(WorkOrderDetail info)
        {
            // 轉換 Base64 字串
            /*
            if (info.ResponseTypeName == nameof(ResponseType.ExternalEMail))
            {
                try
                {
                    info.Body = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));
                }
                catch
                {

                    info.Body = info.Body;


                }
            }*/
                
            
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.CreatorUserName = CurrentUser.Name;
            info.DeleteMark = false;
            info.EnabledMark = true;
           
        }

        
        
        
        /// <summary>
        /// 取得工作詳情（單據）列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("GetWorkOrderDetails")]
        [AurocoreAuthorize("List")]

        public virtual async Task<CommonResult<PageResult<WorkOrderDetailOutputDto>>> GetWorkOrderDetails(SearchWorkOrderDetail search)
        {
            CommonResult<PageResult<WorkOrderDetailOutputDto>> result = new CommonResult<PageResult<WorkOrderDetailOutputDto>>();
            // change frontend return daterange 
            if (!string.IsNullOrEmpty(search.Filter.CaseOpenTime))
            {
                search.StartDateTime = search.Filter.CaseOpenTime;
                if (!string.IsNullOrEmpty(search.Filter.CaseCloseTime))
                {
                    search.EndDateTime = search.Filter.CaseCloseTime;
                }
                else
                {
                    search.EndDateTime = search.Filter.CaseOpenTime;
                }
            }
            if (!string.IsNullOrEmpty(search.Filter.OwnerId))
            {
                search.Filter.CurrentEditorId = search.Filter.OwnerId;
                //search.EnCode = string.IsNullOrEmpty(search.Filter.OwnerId) ? ((int)SearchType.allcase).ToString() : ((int)SearchType.personalcase).ToString(),
            }

            SeachWorkOrder seachWorkOrder = new SeachWorkOrder() {
                DepartmentId = CurrentUser.DeptId,
                EnCode = string.IsNullOrEmpty(search.Filter.OwnerId) ? ((int)SearchType.allcase).ToString() : ((int)SearchType.personalcase).ToString(),
                Filter = new()
                {
                    StatusId = search.Filter.StatusId,
                    StatusName = search.Filter.StatusName,
                    OwnerId = CurrentUser.UserId,

                }

            };
            
           // result.ResData = await iService.FindWorkOrderDerailsWithPagerAsync(search);
            result.ResData = await iService.FindWithPagerSearchAsync(search, seachWorkOrder);
            result.ErrCode = ErrCode.successCode;
            return result;
        }
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(WorkOrderDetail info)
        {
            /* 轉換 base64 字串
            if (info.ResponseTypeName == nameof(ResponseType.ExternalEMail))
            {
                try
                {
                    info.Body = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));

                }
                catch
                {

                    info.Body = info.Body;
                }
            }*/
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(WorkOrderDetail info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 儲存後回覆郵件
        /// </summary>
        /// <param name="info"></param>
        protected  async void OnAfterReplyMail(WorkOrderDetailInputDto info)
        {
            string content = info.Body;
            string header = info.Header;
            List<string> recipients = new();
            List<string> attachemnts = new();
            recipients = info.ContactEmail.Split(",").ToList();
            attachemnts = info.Attachment.Split(",").ToList();
            List<MailFile> lstmailFile = new();

            foreach (var attachment in attachemnts)
            {
                string mime = MimeKit.MimeTypes.GetMimeType(attachment);
                string ext = System.IO.Path.GetExtension(attachment).ToLower();
                lstmailFile.Add(new MailFile { MailFilePath = attachment, MailFileType = mime, MailFileSubType = ext });
            }
            var mailBodyEntity = new MailBodyEntity()
            {
                Sender = "新安東京海上產物保險",
                BoxType = "EzGo",
                Body = content + "</br>\n\r\n\r請勿直接回複本郵件！",
                Recipients = recipients,
                Subject = header,
                MailFiles = lstmailFile,
                EWSItemId = info.SourceId
            };
            //string content = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));
            if (!string.IsNullOrEmpty(info.SourceId)){
                await EWSMailHelper.ReplyEmail(mailBodyEntity);
            }
            else
            {
                mailBodyEntity.Recipients.Add( info.ContactEmail);
                EWSMailHelper.SendEmail(mailBodyEntity);
            }
           
            
        }
        /// <summary>
        /// 儲存後寄發簡訊
        /// </summary>
        /// <param name="info"></param>
        protected string OnAfterSavetoSMS(WorkOrderDetail info)
        {
            MitakeSMS mitakeSMS = new MitakeSMS();
            
            mitakeSMS.Send(info.ContactPhone,"",info.Body,info.ContactId,out string returnMsg);
            return returnMsg;
        }
        /// <summary>
        /// 寄發通知
        /// </summary>
        /// <param name="notifyHeader"></param>
        /// <param name="notifyBody"></param>
        /// <param name="notifyAttachments"></param>
        /// <param name="user"></param>
        protected async void SendNotification(User user,string notifyHeader, string notifyBody,string notifyAttachments="")
        {

            //string content = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));
            string content = notifyBody;
            string header = notifyHeader;

            List<string> attachemnts = new();
            List<string> recipients = new();
            recipients.Add(user.Email);
            if (!string.IsNullOrEmpty(notifyAttachments))
            {
                attachemnts = notifyAttachments.Split(",").ToList();
            }
            
            List<MailFile> lstmailFile = new();

            foreach (var attachment in attachemnts)
            {
                string mime = MimeKit.MimeTypes.GetMimeType(attachment);
                string ext = System.IO.Path.GetExtension(attachment).ToLower();
                lstmailFile.Add(new MailFile { MailFilePath = attachment, MailFileType = mime, MailFileSubType = ext });
            }
            var mailBodyEntity = new MailBodyEntity()
            {
                Sender = "新安東京海上產物保險",
                BoxType = "EzGo",
                Body = content + "</br>\n\r\n\r請勿直接回複本郵件！",
                Recipients= recipients,
                Subject = header,
                MailFiles = lstmailFile,
                
            };
            await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
            await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
            EWSMailHelper.SendEmail(mailBodyEntity);
        }
        /// <summary>
        /// 寄發通知
        /// </summary>
        /// <param name="notifyHeader"></param>
        /// <param name="notifyBody"></param>
        /// <param name="notifyAttachments"></param>
        /// <param name="organize"></param>
        protected async void SendNotification(Organize organize, string notifyHeader, string notifyBody, string notifyAttachments = "")
        {

            //string content = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));
            string content = notifyBody;
            string header = notifyHeader;

            List<string> attachemnts = new();
            List<string> recipients = new();
            recipients.Add(organize.Email);
            attachemnts = notifyAttachments.Split(",").ToList();
            List<MailFile> lstmailFile = new();

            foreach (var attachment in attachemnts)
            {
                string mime = MimeKit.MimeTypes.GetMimeType(attachment);
                string ext = System.IO.Path.GetExtension(attachment).ToLower();
                lstmailFile.Add(new MailFile { MailFilePath = attachment, MailFileType = mime, MailFileSubType = ext });
            }
            var mailBodyEntity = new MailBodyEntity()
            {
                Sender = "新安東京海上產物保險",
                BoxType = "EzGo",
                Body = content + "</br>\n\r\n\r請勿直接回複本郵件！",
                Recipients = recipients,
                Subject = header,
                MailFiles = lstmailFile,

            };
            await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
            await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
            EWSMailHelper.SendEmail(mailBodyEntity);
        }
        /// <summary>
        /// 在更新資料前對WorkOrderDetail的狀態操作
        /// </summary>
        /// <param name="dinfo"></param>
        /// <param name="workflowstatus">狀態</param>
        /// <returns></returns>
        private async Task<WorkOrderDetail> ChangeWorkOrderDetailStatus(WorkOrderDetail dinfo, string workflowstatus)
        {
            string where = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", workflowstatus);
            var itemDetail =  await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
            dinfo.StatusId = itemDetail.Id;
            dinfo.StatusName = itemDetail.ItemCode;
            

           
            return dinfo;
        }
        
        /// <summary>
        /// 在更新對WorkOrder的狀態操作
        /// </summary>
        /// <param name="info"></param>
        /// <param name="workflowstatus">狀態</param>
        /// <returns></returns>
        private async Task<WorkOrder> ChangeWorkOrderStatus(WorkOrder info, string workflowstatus)
        {
            string where = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", workflowstatus);
            var itemDetail = await itemsDetailService.GetWhereAsync(where);
            info.StatusId = itemDetail.Id;
            info.StatusName = itemDetail.ItemCode;
            if(workflowstatus == nameof(WorkflowStatus.已結案))
            {
                info.CaseCloseTime = DateTime.Now;
            }
            
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
            return info;
        }
        /// <summary>
        /// 新增表單記錄
        /// </summary>
        /// <param name="dinfo">工作單明細/單據</param>
        /// <param name="message">日誌訊息</param>
        /// <param name="logType">日誌分類</param>
        /// <param name="logMessageType">區別內外聯繫</param>
        /// <returns></returns>
        private async Task<bool> InsertWorkOrderLog(WorkOrderDetail dinfo,string message,LogType logType, LogMessageType logMessageType)
        {
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.InternalEMail));
            var itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
            User userinfo = userService.Get(dinfo.CurrentEditorId);
            ReplyMessage replyMessage = new();
            if (userinfo == null)
            {
                userinfo = userService.Get(CurrentUser.UserId);
                if (userinfo == null)
                {
                    
                    Log4NetHelper.Error("工作階段-沒有操作者資料");
                    throw new Exception("工作階段-沒有操作者資料");
                }
                Organize organize = organizeService.Get(userinfo.DepartmentId);
                replyMessage.MessageType = logMessageType.ToString();
                replyMessage.ContactCategory = organize.Id;
                replyMessage.ContactId = organize.Id;
                replyMessage.ContactWay = organize.Email;
                replyMessage.ContactName = organize.FullName;
                replyMessage.Header = message;
                
                message = JsonHelper.ToJson(replyMessage);
            }
            else
            {
                replyMessage.MessageType = logMessageType.ToString();
                replyMessage.ContactCategory = itemDetail.Id;
                replyMessage.ContactId = userinfo.Id;
                replyMessage.ContactWay = userinfo.Email;
                replyMessage.ContactName = userinfo.RealName;
                replyMessage.Header = message;
                
                message = JsonHelper.ToJson(replyMessage);
            }
            

            WorkOrderLog orderLog = new() {
                
                WorkOrderId = dinfo.WorkOrderId,
                WorkOrderDetailId = dinfo.Id,
                ReplyMessage = message,
                ToUserId = dinfo.CurrentEditorId,
                CreatorTime = DateTime.Now,
                CreatorUserId = CurrentUser.UserId,
                DeleteMark = false,
                LogTypeName = logType.ToString(),
                Id = GuidUtils.CreateShortNo(),
                CurrentFormLog = JsonHelper.ToJson(dinfo)

        };

            var log = await workOrderLogService.InsertAsync(orderLog);
            if(log> 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="winfo">WorkOrder 案件</param>
        /// <param name="workorderdetailId">單據 Id</param>
        /// <param name="message">訊息</param>
        /// <param name="logType">記錄形態</param>
        /// <param name="logMessageType">區別內外聯繫</param>
        /// <returns></returns>
        private async Task<bool> InsertWorkOrderLog(WorkOrder winfo,string workorderdetailId ,string message, LogType logType, LogMessageType logMessageType)
        {
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.InternalEMail));
            var itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
            
            ReplyMessage replyMessage = new()
            {
                MessageType = logMessageType.ToString(),
                ContactCategory = itemDetail.Id,
                Header = message
            };
            message = JsonHelper.ToJson(replyMessage);

            WorkOrderLog orderLog = new()
            {
                WorkOrderId = winfo.Id,   
                WorkOrderDetailId = workorderdetailId,
                ReplyMessage = message,
                CreatorTime = DateTime.Now,
                CreatorUserId = CurrentUser.UserId,
                DeleteMark = false,
                LogTypeName = logType.ToString(),
                Id = GuidUtils.CreateShortNo()
                

            };

            var log = await workOrderLogService.InsertAsync(orderLog);
            if (log > 0)
            {
                return true;
            }
            return false;
        }
        #region 要保書備註
        /// <summary>
        /// 新增要保表單記錄（審核備註,管理者備註）
        /// </summary>
        /// <param name="dinfo"></param>
        /// <param name="messageheader"></param>
        /// <param name="messagebody"></param>
        /// <param name="logType"></param>
        /// <param name="logMessageType"></param>
        /// <param name="replyStatus">管理者備註 預設為1 ,審核則取 ReplyStatus </param> 
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private async Task<bool> InsertEstimateAdminLog(WorkOrderDetail dinfo, string messageheader, string messagebody, LogType logType, LogMessageType logMessageType, bool? replyStatus)
        {
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.InternalEMail));
            var itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
            User userinfo = userService.Get(dinfo.CurrentEditorId);
            ReplyMessage replyMessage = new();
            string replyJsonMessage = string.Empty;
            if (userinfo == null)
            {
                userinfo = userService.Get(CurrentUser.UserId);
                if (userinfo == null)
                {

                    Log4NetHelper.Error("工作階段-沒有操作者資料");
                    throw new Exception("工作階段-沒有操作者資料");
                }
                Organize organize = organizeService.Get(userinfo.DepartmentId);
                replyMessage.MessageType = logMessageType.ToString();
                replyMessage.ContactCategory = organize.Id;
                replyMessage.ContactId = organize.Id;
                replyMessage.ContactWay = organize.Email;
                replyMessage.ContactName = organize.FullName;
                replyMessage.Header = messageheader;
                replyMessage.Body = messagebody;
                replyJsonMessage = JsonHelper.ToJson(replyMessage);
            }
            else
            {
                replyMessage.MessageType = logMessageType.ToString();
                replyMessage.ContactCategory = itemDetail.Id;
                replyMessage.ContactId = userinfo.Id;
                replyMessage.ContactWay = userinfo.Email;
                replyMessage.ContactName = userinfo.RealName;
                replyMessage.Header = messageheader;
                replyMessage.Body = messagebody;
                replyJsonMessage = JsonHelper.ToJson(replyMessage);
            }


            WorkOrderLog orderLog = new()
            {

                WorkOrderId = dinfo.WorkOrderId,
                WorkOrderDetailId = dinfo.Id,
                ReplyMessage = replyJsonMessage,
                ToUserId = dinfo.CurrentEditorId,
                CreatorTime = DateTime.Now,
                CreatorUserId = CurrentUser.UserId,
                DeleteMark = false,
                LogTypeName = logType.ToString(),
                Id = GuidUtils.CreateShortNo(),
                CurrentFormLog = JsonHelper.ToJson(dinfo),
                ReplyStatus = replyStatus

            };
            switch (dinfo.Header)
            {


                case AdminNoteHedaerType.Header1://管理者備註 ：預設後送核保
                    Organize organize = organizeService.Get(dinfo.CoDeptId);
                    User couserinfo = userService.Get(dinfo.CoOwnerId);
                    string _itemwhere1 = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.核保審批中));
                    var _item1 = await itemsDetailService.GetWhereAsync(_itemwhere1).ConfigureAwait(false);
                    var _wo1 = workOrderService.Get(dinfo.WorkOrderId);
                    if (_wo1.StatusName == nameof(WorkflowStatus.等待分配))
                    {
                       
                        _wo1.OwnerId = couserinfo.Id;
                        _wo1.OwnerName = couserinfo.RealName;
                        _wo1.CoOwnerId = CurrentUser.UserId;
                        _wo1.CoOwnerName = CurrentUser.Name;
                        _wo1.CaseOpenTime = DateTime.Now;
                        _wo1.StatusId = _item1.Id;
                        _wo1.StatusName = _item1.ItemCode;
                        _wo1.LastModifyTime = DateTime.Now;
                        _wo1.LastModifyUserId = CurrentUser.UserId;
                        var _updated2 = await workOrderService.UpdateAsync(_wo1, _wo1.Id).ConfigureAwait(false);
                        if (!_updated2)
                        {
                            Log4NetHelper.Error(_wo1.Id + ":案件指派-指派失敗");
                            throw new Exception(_wo1.Id + ":案件指派-指派失敗");
                        }
                    }
                    if (_wo1.StatusName == nameof(WorkflowStatus.處理中)
                        ||_wo1.StatusName == nameof(WorkflowStatus.核保審批中) 
                        || _wo1.StatusName == nameof(WorkflowStatus.協辦中))
                    {
                        /* 本版本為 案件留在 客服，協辦為 核保（協辦計算 案件數量有問題）
                       _wo1.OwnerId = CurrentUser.UserId;
                       _wo1.OwnerName = CurrentUser.Name;
                       _wo1.CoOwnerId = dinfo.CoOwnerId;
                       _wo1.CoOwnerName = dinfo.CoOwnerName;
                        */
                        _wo1.OwnerId = couserinfo.Id;
                        _wo1.OwnerName = couserinfo.RealName;
                        _wo1.CoOwnerId = CurrentUser.UserId;
                        _wo1.CoOwnerName = CurrentUser.Name;
                        _wo1.StatusId = _item1.Id;
                        _wo1.StatusName = _item1.ItemCode;
                        _wo1.LastModifyTime = DateTime.Now;
                        _wo1.LastModifyUserId = CurrentUser.UserId;
                        var _updated2 = await workOrderService.UpdateAsync(_wo1, _wo1.Id).ConfigureAwait(false);
                        if (!_updated2)
                        {
                            Log4NetHelper.Error(_wo1.Id + ":案件指派-指派失敗");
                            throw new Exception(_wo1.Id + ":案件指派-指派失敗");
                        }
                   }
                   string mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{dinfo.ResponseTypeName} )";
                   string mailBodyTmp = $@"(<h3>案件編號：{_wo1.Id}</h3><br><br><h3>要保書編號：{_wo1.SourceId}
                   </h3><br><br><h4>狀態：{dinfo.Header}</h4><br><br><h4>備註{dinfo.Body}</h4><br><br><h3>{organize.FullName}：{couserinfo.RealName}</h3>)";
                   SendNotification(couserinfo, mailHeaderTmp, mailBodyTmp, dinfo.Attachment);
                   await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                   await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                   await hubContext.Clients.All.SendAsync("SendMessage", "GetEstimatewithRealtions");

                   break;
               case AdminNoteHedaerType.Header2:
                   string where2 = string.Format("ItemCode='{0}'", nameof(ResponseType.OutBound話務記錄));
                   var itemDetail2 = await itemsDetailService.GetWhereAsync(where2).ConfigureAwait(false);
                   string swhere2 = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.已結案));
                   var sitemDetail2 = await itemsDetailService.GetWhereAsync(swhere2).ConfigureAwait(false);
                   var iwhere2 = string.Format("Id='{0}' ", dinfo.InsuranceTypeId);
                   var iitemDetail2 = await itemsDetailService.GetWhereAsync(iwhere2).ConfigureAwait(false);
                   // User userinfo2 = userService.Get(dinfo.CurrentEditorId);
                   WorkOrderDetail _wod2 = new WorkOrderDetail();
                   _wod2.ResponseTypeId = itemDetail2.Id;
                   _wod2.ResponseTypeName = itemDetail2.ItemCode;
                   _wod2.StatusId = sitemDetail2.Id;
                   _wod2.StatusName = sitemDetail2.ItemCode;
                   _wod2.Header = dinfo.Header;
                   _wod2.Body = dinfo.Body;
                   _wod2.InsuranceTypeId = dinfo.InsuranceTypeId;
                   _wod2.InsuranceTypeName = iitemDetail2.ItemCode;
                   _wod2.CaseCategoryLayer1Id = dinfo.CaseCategoryLayer1Id;
                   _wod2.CaseCategoryLayer1Name = dinfo.CaseCategoryLayer1Name;
                   _wod2.CaseCategoryLayer2Id = dinfo.CaseCategoryLayer2Id;
                   _wod2.CaseCategoryLayer2Name = dinfo.CaseCategoryLayer2Name;
                   _wod2.WorkOrderId = dinfo.WorkOrderId;
                   OnBeforeInsert(_wod2);
                   var _inserted2 = await iService.CreateSingleWorkOrderDetail(_wod2).ConfigureAwait(false);
                   if (_inserted2 <= 0)
                   {
                       Log4NetHelper.Error(dinfo.WorkOrderId + ":案件-建立 OutBound 電話記錄失敗");
                       throw new Exception(dinfo.WorkOrderId + ":案件-建立 OutBound 電話記錄失敗");
                   }
                   break;
               case AdminNoteHedaerType.Header3:
                   string where3 = string.Format("ItemCode='{0}'", nameof(ResponseType.Inbound話務記錄));
                   var itemDetail3 = await itemsDetailService.GetWhereAsync(where3).ConfigureAwait(false);
                   string swhere3 = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.已結案));
                   var sitemDetail3 = await itemsDetailService.GetWhereAsync(swhere3).ConfigureAwait(false);
                   var iwhere3 = string.Format("Id='{0}' ", dinfo.InsuranceTypeId);
                   var iitemDetail3 = await itemsDetailService.GetWhereAsync(iwhere3).ConfigureAwait(false);
                   // User userinfo2 = userService.Get(dinfo.CurrentEditorId);
                   WorkOrderDetail _wod3 = new WorkOrderDetail();
                   _wod3.ResponseTypeId = itemDetail3.Id;
                   _wod3.ResponseTypeName = itemDetail3.ItemCode;
                   _wod3.StatusId = sitemDetail3.Id;
                   _wod3.StatusName = sitemDetail3.ItemCode;
                   _wod3.Header = dinfo.Header;
                   _wod3.Body = dinfo.Body;
                   _wod3.InsuranceTypeId = dinfo.InsuranceTypeId;
                   _wod3.InsuranceTypeName = iitemDetail3.ItemCode;
                   _wod3.CaseCategoryLayer1Id = dinfo.CaseCategoryLayer1Id;
                   _wod3.CaseCategoryLayer1Name = dinfo.CaseCategoryLayer1Name;
                   _wod3.CaseCategoryLayer2Id = dinfo.CaseCategoryLayer2Id;
                   _wod3.CaseCategoryLayer2Name = dinfo.CaseCategoryLayer2Name;

                   _wod3.WorkOrderId = dinfo.WorkOrderId;
                   OnBeforeInsert(_wod3);
                   var _inserted3 = await iService.CreateSingleWorkOrderDetail(_wod3).ConfigureAwait(false);
                   if (_inserted3 <= 0)
                   {
                       Log4NetHelper.Error(dinfo.WorkOrderId + ":案件-建立 InBound 電話記錄失敗");
                       throw new Exception(dinfo.WorkOrderId + ":案件-建立 InBound 電話記錄失敗");
                   }
                   break;

               case AdminNoteHedaerType.Header4://直接結案
                   string _where4 = $"Id={dinfo.WorkOrderId}";
                   string _itemwhere4 = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.已結案));
                   var _item4 = await itemsDetailService.GetWhereAsync(_itemwhere4).ConfigureAwait(false);
                   var _wo4 = workOrderService.Get(dinfo.WorkOrderId);
                   _wo4.CaseCloseTime = DateTime.Now;
                   _wo4.StatusId = _item4.Id;
                   _wo4.StatusName = _item4.ItemCode;
                   _wo4.LastModifyTime = DateTime.Now;
                   _wo4.LastModifyUserId = CurrentUser.UserId;
                   var _updated = await workOrderService.UpdateAsync(_wo4, _wo4.Id).ConfigureAwait(false);
                   /*後續送資料往 b2c (寫 static function)*/
                        if (!_updated)
                    {
                        Log4NetHelper.Error(_wo4.Id + ":案件結案-更新失敗");
                        throw new Exception(_wo4.Id + ":案件結案-更新失敗");
                    }
                    break;
            }
            var log = await workOrderLogService.InsertAsync(orderLog);
            if (log > 0)
            {
                return true;
            }
            return false;
        }
        private async Task<bool> InsertEstimateCheckLog(WorkOrderDetail dinfo, string messageheader, string messagebody, LogType logType, LogMessageType logMessageType, bool? replyStatus)
        {
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.InternalEMail));
            var itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
            User userinfo = userService.Get(dinfo.CurrentEditorId);
            ReplyMessage replyMessage = new();
            string replyJsonMessage = string.Empty;
            if (userinfo == null)
            {
                userinfo = userService.Get(CurrentUser.UserId);
                if (userinfo == null)
                {

                    Log4NetHelper.Error("工作階段-沒有操作者資料");
                    throw new Exception("工作階段-沒有操作者資料");
                }
                Organize organize = organizeService.Get(userinfo.DepartmentId);
                replyMessage.MessageType = logMessageType.ToString();
                replyMessage.ContactCategory = organize.Id;
                replyMessage.ContactId = organize.Id;
                replyMessage.ContactWay = organize.Email;
                replyMessage.ContactName = organize.FullName;
                replyMessage.Header = messageheader;
                replyMessage.Body = messagebody;
                replyJsonMessage = JsonHelper.ToJson(replyMessage);
            }
            else
            {
                replyMessage.MessageType = logMessageType.ToString();
                replyMessage.ContactCategory = itemDetail.Id;
                replyMessage.ContactId = userinfo.Id;
                replyMessage.ContactWay = userinfo.Email;
                replyMessage.ContactName = userinfo.RealName;
                replyMessage.Header = messageheader;
                replyMessage.Body = messagebody;
                replyJsonMessage = JsonHelper.ToJson(replyMessage);
            }


            WorkOrderLog orderLog = new()
            {

                WorkOrderId = dinfo.WorkOrderId,
                WorkOrderDetailId = dinfo.Id,
                ReplyMessage = replyJsonMessage,
                ToUserId = dinfo.CurrentEditorId,
                CreatorTime = DateTime.Now,
                CreatorUserId = CurrentUser.UserId,
                DeleteMark = false,
                LogTypeName = logType.ToString(),
                Id = GuidUtils.CreateShortNo(),
                CurrentFormLog = JsonHelper.ToJson(dinfo),
                ReplyStatus = replyStatus

            };
            switch (dinfo.Header)
            {

                case CheckNoteHedaerType.Header1://直接結案
                    string _where = $"Id={dinfo.WorkOrderId}";
                    string _itemwhere = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.已結案));
                    var _item = await itemsDetailService.GetWhereAsync(_itemwhere).ConfigureAwait(false);
                    var _wo1 = workOrderService.Get(dinfo.WorkOrderId);
                    _wo1.CaseCloseTime = DateTime.Now;
                    _wo1.StatusId = _item.Id;
                    _wo1.StatusName = _item.ItemCode;
                    _wo1.LastModifyTime = DateTime.Now;
                    _wo1.LastModifyUserId = CurrentUser.UserId;
                    var _updated = await workOrderService.UpdateAsync(_wo1, _wo1.Id).ConfigureAwait(false);
                    /*後續送資料往 b2c (寫 static function)*/
                    if (!_updated)
                    {
                        Log4NetHelper.Error(_wo1.Id + ":案件結案-更新失敗");
                        throw new Exception(_wo1.Id + ":案件結案-更新失敗");
                    }
                    break;
                default://核保備註 ：預設後送客服
                    Organize organize = organizeService.Get(dinfo.CoDeptId);
                    User couserinfo = userService.Get(dinfo.CoOwnerId);
                    string _itemwhere2 = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.處理中));
                    var _item2 = await itemsDetailService.GetWhereAsync(_itemwhere2).ConfigureAwait(false);
                    var _wo2 = workOrderService.Get(dinfo.WorkOrderId);
                    if (_wo2.StatusName == nameof(WorkflowStatus.等待分配))
                    {
                        _wo2.CaseOpenTime = DateTime.Now;
                        _wo2.StatusId = _item2.Id;
                        _wo2.StatusName = _item2.ItemCode;
                        _wo2.LastModifyTime = DateTime.Now;
                        _wo2.LastModifyUserId = CurrentUser.UserId;
                        _wo2.CoOwnerId= CurrentUser.UserId;
                        _wo2.CoOwnerName = CurrentUser.Name;
                        var _updated2 = await workOrderService.UpdateAsync(_wo2, _wo2.Id).ConfigureAwait(false);
                        if (!_updated2)
                        {
                            Log4NetHelper.Error(_wo2.Id + ":案件指派-指派失敗");
                            throw new Exception(_wo2.Id + ":案件指派-指派失敗");
                        }
                    }
                    if (_wo2.StatusName == nameof(WorkflowStatus.核保審批中)
                        ||_wo2.StatusName == nameof(WorkflowStatus.處理中))
                    {
                       
                        _wo2.StatusId = _item2.Id;
                        _wo2.StatusName = _item2.ItemCode;
                        _wo2.LastModifyTime = DateTime.Now;
                        _wo2.LastModifyUserId = CurrentUser.UserId;
                        _wo2.OwnerId = dinfo.CoOwnerId;
                        _wo2.OwnerName = dinfo.CoOwnerName;
                        _wo2.CoOwnerId = CurrentUser.UserId;
                        _wo2.CoOwnerName = CurrentUser.Name;
                        _wo2.StatusId = _item2.Id;
                        _wo2.StatusName = _item2.ItemCode;
                        var _updated2 = await workOrderService.UpdateAsync(_wo2, _wo2.Id).ConfigureAwait(false);
                        if (!_updated2)
                        {
                            Log4NetHelper.Error(_wo2.Id + ":案件指派-指派失敗");
                            throw new Exception(_wo2.Id + ":案件指派-指派失敗");
                        }
                    }
                    string mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{dinfo.ResponseTypeName} )";
                    string mailBodyTmp = $@"(<h3>案件編號：{_wo2.Id}</h3><br><br><h3>要保書編號：{_wo2.SourceId}
                    </h3><br><br><h4>狀態：{dinfo.Header}</h4><br><br><h4>備註{dinfo.Body}</h4><br><br><h3>{organize.FullName}：{couserinfo.RealName}</h3>)";
                    SendNotification(couserinfo, mailHeaderTmp, mailBodyTmp, dinfo.Attachment);
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetEstimatewithRealtions");

                    break;
            }
            var log = await workOrderLogService.InsertAsync(orderLog);
            if (log > 0)
            {



                return true;
            }
            return false;
        }
        #endregion

        /// <summary>
        /// 非同步更新工作單明細（不傳送通知）
        /// </summary>
        /// <param name="jinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(WorkOrderDetailInputDto jinfo, string id)
        {
            CommonResult result = new CommonResult();
            WorkOrderDetail oldinfo = iService.Get(id);
            WorkOrderDetail info = jinfo.MapTo<WorkOrderDetail>();
            if (oldinfo.StatusName == nameof(WorkflowStatus.已結案))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "單據已結案,無法修改";
                
                return ToJsonContent(result);
            }
            if (oldinfo.StatusName==nameof(WorkflowStatus.協辦中) && !string.IsNullOrEmpty(oldinfo.CoOwnerId) && (CurrentUser.UserId==oldinfo.CreatorUserId))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "單據由其他人負責，暫時無法修改";
                return ToJsonContent(result);
            }
            else if(oldinfo.StatusName == nameof(WorkflowStatus.協辦中) && string.IsNullOrEmpty(oldinfo.CoOwnerId)&&(!string.IsNullOrEmpty(jinfo.CoOwnerId)))
            {
                info.CoOwnerId = jinfo.CoOwnerId;
                info.CoOwnerName = jinfo.CoOwnerName;
                info.CurrentEditorId = jinfo.CoOwnerId;
                if (string.IsNullOrEmpty(jinfo.WorkOrderId))
                {
                    info.WorkOrderId = oldinfo.WorkOrderId;
                }
                info.Id = oldinfo.Id;
                info.StatusId = oldinfo.StatusId;
                info.StatusName = oldinfo.StatusName;
                info.CoDeptId = oldinfo.CoDeptId;
                info.DeleteMark = oldinfo.DeleteMark;
                info.CreatorTime = oldinfo.CreatorTime;
                info.CreatorUserId = oldinfo.CreatorUserId;
                OnBeforeUpdate(info);
                bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
                if (bl)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    User receiverinfo = userService.Get(info.CoOwnerId);
                    string mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{ info.ResponseTypeName} )";
                    string mailBodyTmp = $"(<h3>單據編號：{info.Id}</h3><br><br><h3>單據負責人：{receiverinfo.RealName}</h3>)";
                    
                    SendNotification(receiverinfo, mailHeaderTmp, mailBodyTmp, info.Attachment);
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }
            if (oldinfo.StatusName == nameof(WorkflowStatus.處理中))
            {


                //** 預防誤改原單據 
                if (string.IsNullOrEmpty(jinfo.WorkOrderId))
                {
                    info.WorkOrderId = oldinfo.WorkOrderId;
                }
                info.Id = oldinfo.Id;
                info.StatusId = oldinfo.StatusId;
                info.StatusName = oldinfo.StatusName;
                info.DeleteMark = oldinfo.DeleteMark;
                info.CreatorTime = oldinfo.CreatorTime;
                info.CreatorUserId = oldinfo.CreatorUserId;
                info.CurrentEditorId = info.CreatorUserId;
                OnBeforeUpdate(info);
                
                bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
                if (bl)
                {
                    if(info.ResponseTypeName==nameof(ResponseType.ExternalEMail)|| info.ResponseTypeName == nameof(ResponseType.ExternalEMail))
                    {
                        await InsertWorkOrderLog(oldinfo, oldinfo.Header, LogType.修改單據, LogMessageType.外部聯繫);
                    }
                    else
                    {
                        await InsertWorkOrderLog(oldinfo, oldinfo.Header, LogType.修改單據, LogMessageType.內部聯繫);
                    }
                    
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    /*
                    switch (info.ResponseTypeName)
                    {
                        case nameof(ResponseType.EMail):
                            OnAfterReplyMail(jinfo);
                            break;
                        case nameof(ResponseType.SMS):
                            OnAfterSavetoSMS(info);
                            break;
                    }*/
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            
            return ToJsonContent(result);
        }
        /// <summary>
        /// 非同步更新工作單明細（傳送通知）
        /// </summary>
        /// <param name="jinfo">單據資料</param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("UpdatewithSendNotify")]
        [AurocoreAuthorize("Edit")]
        public  async Task<IActionResult> UpdatewithSendNotify(WorkOrderDetailInputDto jinfo, string id)
        {
            CommonResult result = new();
            if (jinfo.StatusName == nameof(WorkflowStatus.協辦中) & string.IsNullOrEmpty(jinfo.CoDeptId))
            {
                Log4NetHelper.Error("協辦資料有誤");
                throw new ArgumentNullException("協辦資料有誤");
              
            }
            WorkOrderDetail oldinfo = iService.Get(id);
            WorkOrderDetail info = jinfo.MapTo<WorkOrderDetail>();
            if (!string.IsNullOrEmpty(jinfo.CoOwnerId))
            {
                info.CurrentEditorId = info.CoOwnerId;
            }
            else
            {
                info.CurrentEditorId = info.CoDeptId;
            }
            if (jinfo.StatusName == nameof(WorkflowStatus.協辦中))
            {
                
                //** 預防誤改原單據 
                info.WorkOrderId = oldinfo.WorkOrderId;
                info.Id = oldinfo.Id;
                info.DeleteMark = oldinfo.DeleteMark;
                info.CreatorTime = oldinfo.CreatorTime;
                info.CreatorUserId = oldinfo.CreatorUserId;   
                OnBeforeUpdate(info);
                info =  await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.協辦中)).ConfigureAwait(false);
                bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
                string mailHeaderTmp = string.Empty;
                string mailBodyTmp = string.Empty;
                if (bl)
                {
                    if (info.ResponseTypeName == nameof(ResponseType.ExternalEMail) || info.ResponseTypeName == nameof(ResponseType.ExternalEMail))
                    {
                        await InsertWorkOrderLog(oldinfo, oldinfo.Header, LogType.修改單據, LogMessageType.外部聯繫);
                    }
                    else
                    {
                        await InsertWorkOrderLog(oldinfo, oldinfo.Header, LogType.修改單據, LogMessageType.內部聯繫);
                    }
                   
                   
                    User userinfo = userService.Get(info.CreatorUserId);
                    if (string.IsNullOrEmpty(info.CoOwnerId) && !(string.IsNullOrEmpty(info.CoDeptId)))
                    {
                        mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{ info.ResponseTypeName} )";
                        mailBodyTmp = $"(<h3>單據編號：{info.Id}</h3><br><br><h3>此單據尚未分配執行者</h3><br><br><h3>擔當者：{userinfo.RealName}</h3>)";
                        Organize receiverinfo = organizeService.Get(info.CoDeptId);
                        SendNotification(receiverinfo, mailHeaderTmp, mailBodyTmp, info.Attachment);
                    }
                    else
                    {
                        mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{ info.ResponseTypeName} )";
                        mailBodyTmp = $"(<h3>單據編號：{info.Id}</h3><br><br><h3>擔當者：{userinfo.RealName}</h3>)";
                        User receiverinfo = userService.Get(info.CoOwnerId);
                        SendNotification(receiverinfo, mailHeaderTmp, mailBodyTmp, info.Attachment);
                    }
                    
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }

            return ToJsonContent(result);
        }
        /// <summary>
        /// 新增各式單據
        /// </summary>
        /// <param name="jinfo">單據資料</param>
        /// <returns></returns>
        [HttpPost("Insert")]
        [AurocoreAuthorize("Add")]
        public override async Task<IActionResult> InsertAsync(WorkOrderDetailInputDto jinfo)
        {
           
            CommonResult result = new CommonResult();
            bool isClosedCase =false;// 判斷案件是否直接結案
            WorkOrder workOrder = new();
            if (!string.IsNullOrEmpty(jinfo.WorkOrderId))
            {
                workOrder = workOrderService.Get(jinfo.WorkOrderId);
                if (workOrder == null)
                {
                    throw new Exception("無此案件編號");
                }
            }
            
            if (string.IsNullOrEmpty(jinfo.WorkOrderId) && !string.IsNullOrEmpty(jinfo.EstimateId))
            {
                if(jinfo.ResponseTypeName== nameof(ResponseType.Inbound話務記錄))
                {
                    jinfo.Header = AdminNoteHedaerType.Header3;
                    var rst = await InsertAdminNoteAsync(jinfo).ConfigureAwait(false);
                    
                    return rst;
                }
                if(jinfo.ResponseTypeName == nameof(ResponseType.OutBound話務記錄))
                {
                    jinfo.Header = AdminNoteHedaerType.Header2;
                    var rst =  await InsertAdminNoteAsync(jinfo).ConfigureAwait(false);
                    return rst;
                }
            }
            
            if (!string.IsNullOrEmpty(jinfo.WorkOrderId) 
                && !string.IsNullOrEmpty(jinfo.EstimateId)
                && (workOrder.StatusName == nameof(WorkflowStatus.處理中) || workOrder.StatusName == nameof(WorkflowStatus.核保審批中)))
            {
                if (string.IsNullOrEmpty(jinfo.EstimateId))
                {
                    jinfo.EstimateId = jinfo.PolicyNo;
                }
                if (jinfo.ResponseTypeName == nameof(ResponseType.Inbound話務記錄))
                {
                    jinfo.Header = AdminNoteHedaerType.Header3;
                    var rst = await InsertAdminNoteAsync(jinfo).ConfigureAwait(false);

                    return rst;
                }
                if (jinfo.ResponseTypeName == nameof(ResponseType.OutBound話務記錄))
                {
                    jinfo.Header = AdminNoteHedaerType.Header2;
                    var rst = await InsertAdminNoteAsync(jinfo).ConfigureAwait(false);
                    return rst;
                }
            }
            if (!string.IsNullOrEmpty(jinfo.WorkOrderId)
                && !string.IsNullOrEmpty(jinfo.PolicyNo)
                && (workOrder.StatusName == nameof(WorkflowStatus.處理中) || workOrder.StatusName == nameof(WorkflowStatus.核保審批中)))
            {
                if (jinfo.ResponseTypeName == nameof(ResponseType.Inbound話務記錄))
                {
                    jinfo.Header = AdminNoteHedaerType.Header3;
                    var rst = await InsertAdminNoteAsync(jinfo).ConfigureAwait(false);

                    return rst;
                }
                if (jinfo.ResponseTypeName == nameof(ResponseType.OutBound話務記錄))
                {
                    jinfo.Header = AdminNoteHedaerType.Header2;
                    var rst = await InsertAdminNoteAsync(jinfo).ConfigureAwait(false);
                    return rst;
                }
            }
            if (!string.IsNullOrEmpty(jinfo.WorkOrderId) && jinfo.EnabledMark == true)
            {
                isClosedCase = true;
            }
            else if(jinfo.ResponseTypeName==nameof(ResponseType.話務記錄)&& string.IsNullOrEmpty(jinfo.WorkOrderId)&& jinfo.EnabledMark == true)
            {
                isClosedCase = true;
            }
            else
            {
                isClosedCase = false;
            }
            if (!string.IsNullOrEmpty(jinfo.WorkOrderId) && jinfo.EnabledMark == true)
            {
                isClosedCase = true;
            }
            else if (jinfo.ResponseTypeName == nameof(ResponseType.Inbound話務記錄) && string.IsNullOrEmpty(jinfo.WorkOrderId) && jinfo.EnabledMark == true)
            {
                isClosedCase = true;
            }
            else
            {
                isClosedCase = false;
            }
            if (!string.IsNullOrEmpty(jinfo.StatusId))
            {
                var statusItem = await itemsDetailService.GetWhereAsync("Id='"+ jinfo.StatusId+ "'");
                if(statusItem.ItemName==nameof(WorkflowStatus.協辦中)|| statusItem.ItemName == nameof(WorkflowStatus.處理中))
                {
                    isClosedCase = false;
                }
            }
            if (string.IsNullOrEmpty(jinfo.WorkOrderId))
            {

                if (jinfo.ResponseTypeName == nameof(ResponseType.Inbound話務記錄) ||
                    jinfo.ResponseTypeName == nameof(ResponseType.OutBound話務記錄) ||
                        jinfo.ResponseTypeName == nameof(ResponseType.話務記錄))
                {

                }   
                else
                {
                    result.ErrMsg = "案件編號不存在";
                    result.ErrCode = "43001";
                    return ToJsonContent(result);
                }
                
            }
            
            if (string.IsNullOrEmpty(jinfo.EnabledMark.ToString()) )
            {
                result.ErrMsg = "未傳入狀態碼";
                result.ErrCode = "43001";
                return ToJsonContent(result);
            }
            if (!string.IsNullOrEmpty(jinfo.WorkOrderId))
            {
                
                if(workOrder == null)
                {
                    result.ErrMsg = "案件號碼不存在，請檢查是否誤傳單據";
                    result.ErrCode = "43001";
                    return ToJsonContent(result);
                }
                if (workOrder.StatusName == nameof(WorkflowStatus.已結案))
                {
                    result.ErrMsg = "案件已結案不可再發送";
                    result.ErrCode = "43001";
                    return ToJsonContent(result);
                }
                if(workOrder.StatusName == nameof(WorkflowStatus.等待分配))
                {
                    result.ErrMsg = "案件未分配，無法撰寫單據";
                    result.ErrCode = "43001";
                    return ToJsonContent(result);
                }
                if (string.IsNullOrEmpty(workOrder.SetAccountRelation))
                {
                    result.ErrMsg = "案件未填入歸戶資料，無法開立單據";
                    result.ErrCode = "43001";
                    return ToJsonContent(result);
                }

            }

            if (!string.IsNullOrEmpty(jinfo.Attachment))
            {
                var fileInfo = uploadFileService.Get(jinfo.Attachment);
                if(fileInfo != null)
                {
                    jinfo.Attachment = fileInfo.FilePath;
                }

            }

            WorkOrderDetail info = jinfo.MapTo<WorkOrderDetail>();
            if (!(string.IsNullOrEmpty(jinfo.CoOwnerId))&&(string.IsNullOrEmpty(jinfo.CoDeptId)))
            {
                User user = userService.Get(jinfo.CoOwnerId);
                info.CoDeptId = user.DepartmentId;
            }
           
           
            OnBeforeInsert(info);
            switch (info.ResponseTypeName)
            {
                case nameof(ResponseType.ExternalEMail):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                    info.IsNotified = true;
                    info.ContactStatus = nameof(ContactStatusType.已回覆郵件);
                    info.CurrentEditorId = CurrentUser.UserId;
                    
                    break;
                case nameof(ResponseType.ExternalSMS):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                    info.IsNotified = true;
                    info.ContactStatus = nameof(ContactStatusType.已回覆簡訊);
                    info.CurrentEditorId = CurrentUser.UserId;
                    break;
                case nameof(ResponseType.一般問題單):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                    info.IsNotified = false;
                    info.CurrentEditorId = CurrentUser.UserId;
                    break;
                case nameof(ResponseType.批改申請單):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                    info.IsNotified = false;
                    info.CurrentEditorId = CurrentUser.UserId;
                    break;
                case nameof(ResponseType.補發申請單):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                    info.IsNotified = false;
                    info.CurrentEditorId = CurrentUser.UserId;
                    break;
                case nameof(ResponseType.話務記錄):
                    if (string.IsNullOrEmpty(info.WorkOrderId))
                    {
                        ItemsDetail itemDetail = new();
                        bool isNotified = true;
                        if (isClosedCase == false)
                        {
                            string where = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.處理中));
                            itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
                            isNotified = false;
                            jinfo.EnabledMark = true;
                        }
                        else{
                            string where = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.已結案));
                            itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
                            jinfo.EnabledMark = true;
                        }
                        
                        string workorderId = GuidUtils.CreateShortNo();
                       
                        var woType = await itemsDetailService.GetWhereAsync("ItemName='InBound' AND ItemCode='案件分類'");
                        int _hour = woType.ItemProperty1.ToInt();
                        workOrder = new()
                        {

                            Id = workorderId,
                            ApplicantName = info.ContactName,
                            ApplicantPolicyNO = info.PolicyNo,
                            ApplicantId = info.ContactId,
                            SetAccountRelation = info.ContactRelation,
                            Itag = info.PlateNumber,
                            InsuranceTypeName = info.InsuranceTypeName,
                            ApplicantCTIPhone = info.ContactPhone,
                            ApplicantMobilePhone = info.ContactPhone,
                            FormTypeName = woType.ItemName,
                            FormTypeId = woType.Id,
                            ParentFormTypeId = woType.Id,
                            ParentFormTypeName = woType.ItemName,
                            StatusName = itemDetail.ItemName,
                            StatusId = itemDetail.Id,
                            Header = info.Header,
                            OwnerId = CurrentUser.UserId,
                            OwnerName = CurrentUser.Name,
                            CaseCategoryLayer1Id = info.CaseCategoryLayer1Id,
                            CaseCategoryLayer2Id = info.CaseCategoryLayer2Id,
                            CaseCategoryLayer1Name = info.CaseCategoryLayer1Name,
                            CaseCategoryLayer2Name = info.CaseCategoryLayer2Name,
                            CreatorTime = DateTime.Now,
                            CreatorUserId = CurrentUser.UserId,
                            IsNotified = isNotified,
                            LastModifyTime = DateTime.Now,
                            LastModifyUserId= CurrentUser.UserId,
                            CaseOpenTime = DateTime.Now,
                            CaseCloseTime = DateTime.Now.AddHours(_hour),
                            Supplement =JsonHelper.ToJson(new Memo()),
                            DeleteMark = false

                        };
                        if(string.IsNullOrEmpty(workOrder.ApplicantPolicyNO) || string.IsNullOrEmpty(workOrder.ApplicantName) || string.IsNullOrEmpty(workOrder.SetAccountRelation))
                        {
                            workOrder.SetAccountStatus = false;
                            workOrder.SetAccountStatusDesc = ErrCode.err70003;
                        }
                        else
                        {
                            workOrder.SetAccountStatus = true;
                        }
                        long wk = await workOrderService.InsertAsync(workOrder).ConfigureAwait(false);

                        if (wk <= 0)
                        {
                            result.ErrCode = ErrCode.err0;
                            result.ErrMsg = "建立案件失敗，請再嘗試一次";
                        }
                        else
                        {
                            await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                            await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                            result.ErrCode = ErrCode.successCode;
                            result.ErrMsg = ErrCode.err0;
                            result.ResData = info;
                        }

                        info.WorkOrderId = workorderId;

                        //return ToJsonContent(result);
                    }
                    info.CurrentEditorId = CurrentUser.UserId;
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);


                    break;
                case nameof(ResponseType.核保審批):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                    info.IsNotified = false;
                    info.CurrentEditorId = CurrentUser.UserId;
                    break;
                case nameof(ResponseType.Inbound話務記錄):
                    /* 客戶身份」、「客戶姓名」、「聯絡電話」、「險種」、「問題分類」、「子類型」
                     1.ContactRelation
                     2.ContactName
                     3.ContactPhone
                     4.InsuranceTypeId
                     5.CaseCategoryLayer1Id
                     6.CaseCategoryLayer2Id

                    */
                    if (string.IsNullOrEmpty(info.ContactRelation) || string.IsNullOrEmpty(info.ContactName) ||
                        string.IsNullOrEmpty(info.ContactPhone) || string.IsNullOrEmpty(info.InsuranceTypeId) ||
                        string.IsNullOrEmpty(info.CaseCategoryLayer1Id) || string.IsNullOrEmpty(info.CaseCategoryLayer2Id)
                        )
                    {

                        PropertyInfo[] properties = typeof(WorkOrderDetail).GetProperties();
                        foreach (PropertyInfo property in info.GetType().GetProperties())
                        {
                            if (property.Name== "ContactRelation" || property.Name == "ContactName"||
                                property.Name == "ContactPhone" || property.Name == "InsuranceTypeId" ||
                                property.Name == "CaseCategoryLayer1Id" || property.Name == "CaseCategoryLayer2Id"
                                )
                            {
                                if (string.IsNullOrEmpty(property.GetValue(info).ToString()))
                                {
                                    result.ErrCode = ErrCode.err43001;
                                    result.ErrMsg = String.Format("{0} ,欄位空白", property.GetCustomAttributes<DisplayAttribute>().First().Name);
                                    return ToJsonContent(result);
                                }
                            }
                            
                        }

                    }
                    if (string.IsNullOrEmpty(info.WorkOrderId))
                    {
                        ItemsDetail itemDetail = new();
                        bool isNotified = true;
                        if (isClosedCase == false)
                        {
                            string where = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.處理中));
                            itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
                            isNotified = false;
                            jinfo.EnabledMark = true;
                        }
                        else
                        {
                            string where = string.Format("ItemCode='{0}' AND ItemId='2021021618200145722953'", nameof(WorkflowStatus.已結案));
                            itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
                            jinfo.EnabledMark = true;
                        }

                        string workorderId = GuidUtils.CreateShortNo();

                        var woType = await itemsDetailService.GetWhereAsync("ItemName='InBound' AND ItemCode='案件分類'");
                        int _hour = woType.ItemProperty1.ToInt();
                        workOrder = new()
                        {

                            Id = workorderId,
                            ApplicantName = info.ContactName,
                            ApplicantPolicyNO = info.PolicyNo,
                            ApplicantId = info.ContactId,
                            SetAccountRelation = info.ContactRelation,
                            Itag = info.PlateNumber,
                            InsuranceTypeName = info.InsuranceTypeName,
                            ApplicantCTIPhone = info.ContactPhone,
                            ApplicantMobilePhone = info.ContactPhone,
                            FormTypeName = woType.ItemName,
                            FormTypeId = woType.Id,
                            ParentFormTypeId = woType.Id,
                            ParentFormTypeName = woType.ItemName,
                            StatusName = itemDetail.ItemName,
                            StatusId = itemDetail.Id,
                            Header = info.Header,
                            OwnerId = CurrentUser.UserId,
                            OwnerName = CurrentUser.Name,
                            CaseCategoryLayer1Id = info.CaseCategoryLayer1Id,
                            CaseCategoryLayer2Id = info.CaseCategoryLayer2Id,
                            CaseCategoryLayer1Name = info.CaseCategoryLayer1Name,
                            CaseCategoryLayer2Name = info.CaseCategoryLayer2Name,
                            CreatorTime = DateTime.Now,
                            CreatorUserId = CurrentUser.UserId,
                            IsNotified = isNotified,
                            LastModifyTime = DateTime.Now,
                            LastModifyUserId = CurrentUser.UserId,
                            CaseOpenTime = DateTime.Now,
                            CaseCloseTime = DateTime.Now.AddHours(_hour),
                            Supplement = JsonHelper.ToJson(new Memo()),
                            DeleteMark = false

                        };
                        if (string.IsNullOrEmpty(workOrder.ApplicantPolicyNO) || string.IsNullOrEmpty(workOrder.ApplicantName) || string.IsNullOrEmpty(workOrder.SetAccountRelation))
                        {
                            workOrder.SetAccountStatus = false;
                            workOrder.SetAccountStatusDesc = ErrCode.err70003;
                        }
                        else
                        {
                            workOrder.SetAccountStatus = true;
                        }
                        long wk = await workOrderService.InsertAsync(workOrder).ConfigureAwait(false);

                        if (wk <= 0)
                        {
                            result.ErrCode = ErrCode.err0;
                            result.ErrMsg = "建立案件失敗，請再嘗試一次";
                        }
                        else
                        {
                            await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                            await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                            result.ErrCode = ErrCode.successCode;
                            result.ErrMsg = ErrCode.err0;
                            result.ResData = info;
                        }

                        info.WorkOrderId = workorderId;

                        //return ToJsonContent(result);
                    }
                    
                    info.CurrentEditorId = CurrentUser.UserId;
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                    break;
                case nameof(ResponseType.OutBound話務記錄):
                    info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                    info.IsNotified = false;
                    info.CurrentEditorId = CurrentUser.UserId;
                    break;

            }

            long ln = await iService.CreateSingleWorkOrderDetail(info).ConfigureAwait(false);


            result.Success = ln > 0;
            result.Success = !String.IsNullOrEmpty(info.WorkOrderId);
            
            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                
                switch (info.ResponseTypeName)
                {
                    case nameof(ResponseType.ExternalEMail):

                        OnAfterReplyMail(jinfo);
                        result.ResData ="工作單號："+ info.Id;
                        break;
                    case nameof(ResponseType.ExternalSMS):
                        string _msg = OnAfterSavetoSMS(info);
                        result.ResData = "工作單號：" + info.Id+"| 帳戶狀態："+_msg;
                        break;
                    default:
                        /*

                        if (info.StatusName == nameof(WorkflowStatus.處理中) )
                        {
                            await InsertWorkOrderLog(info, info.Header, LogType.建立單據);
                            User coUserInfo = userService.Get(info.CoOwnerId);
                            User userinfo = userService.Get(info.CreatorUserId);

                            string mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{ info.ResponseTypeName} )";
                            string mailBodyTmp = $"(<h3>單據編號：{info.Id}<h3><br><br><h3>擔當者：{userinfo.RealName}</h3>)";
                            SendNotification(coUserInfo, mailHeaderTmp, mailBodyTmp, info.Attachment);
                        } else if (info.StatusName == nameof(WorkflowStatus.處理中))
                        {
                            
                        }*/
                        if (info.ResponseTypeName == nameof(ResponseType.ExternalEMail) || info.ResponseTypeName == nameof(ResponseType.ExternalEMail))
                        {
                            
                            await InsertWorkOrderLog(info, info.Header, LogType.建立單據, LogMessageType.外部聯繫);
                        }
                        else
                        {
                            await InsertWorkOrderLog(info, info.Header, LogType.建立單據, LogMessageType.內部聯繫);
                        }
                        
                        result.ResData = info;
                        break;
                       
                }
                if (isClosedCase)
                {
                    var updatedWo = await ChangeWorkOrderStatus(workOrder, nameof(WorkflowStatus.已結案));
                    workOrderService.Update(updatedWo, updatedWo.Id);
                }
                await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");


                //await hubContext.Clients.All.SendAsync("SendMessage", "WorkOrderTreeBriefData");

            }
            else
            {
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }
        #region 核保單據處理
        /// <summary>
        /// 新增審核備註
        /// </summary>
        /// <param name="jinfo">EstimateNoteInputDto</param>
        /// <returns></returns>
        [HttpPost("AddCheckNote")]
        [AurocoreAuthorize("Add")]
        public async Task<IActionResult> InsertCheckNoteAsync(WorkOrderDetailInputDto jinfo)
        {
            CommonResult result = new CommonResult();
            WorkOrder workOrder = new WorkOrder();
            WorkOrderDetail orderDtailinfo = new WorkOrderDetail();
            Organize organize = new Organize(); 
            if(!string.IsNullOrEmpty(CurrentUser.UserId))
            {
                organize = organizeService.Get(CurrentUser.DeptId);
            }
            User userinfo = new User();
            if (string.IsNullOrEmpty(jinfo.EstimateId) && string.IsNullOrEmpty(jinfo.WorkOrderId))
            {
                result.ErrMsg = "未填入案件或要保書 Id";
                result.ErrCode = ErrCode.err1;
                return ToJsonContent(result);
                
            }

            if (string.IsNullOrEmpty(jinfo.Header))
            {
                result.ErrMsg = "未填入核保備註-處理標籤";
                result.ErrCode = ErrCode.err1;
                return ToJsonContent(result);
               
            }
            if (string.IsNullOrEmpty(jinfo.WorkOrderId))
            {
                string where = string.Format("SourceId='{0}' ", jinfo.EstimateId);
                workOrder = workOrderService.GetWhere(where);
                jinfo.WorkOrderId = workOrder.Id;
            }
            else
            {
                workOrder = workOrderService.Get(jinfo.WorkOrderId);
            }
            if (workOrder == null)
            {
                result.ErrMsg = "要保書尚未轉案件";
                result.ErrCode = ErrCode.err1;
                return ToJsonContent(result);
                
                
            }
            else
            {
                if (workOrder.StatusName == nameof(WorkflowStatus.等待分配))
                {
                    result.ErrMsg = "案件尚未分配";
                    result.ErrCode = ErrCode.err1;
                    return ToJsonContent(result);
                }
                if (organize.FullName !=nameof(Department.核保))
                {
                    result.ErrMsg = "非核保部門無法進行批註";
                    result.ErrCode = ErrCode.err1;
                    return ToJsonContent(result);
                }
                if (workOrder.OwnerId != CurrentUser.UserId)
                {
                    result.ErrMsg = "案件正由他人處理，無法核保";
                    result.ErrCode = ErrCode.err1;
                    return ToJsonContent(result);
                }
                string where = string.Format("WorkOrderId='{0}' AND ResponseTypeName='{1}'", workOrder.Id,nameof(ResponseType.核保審批));
                orderDtailinfo = iService.GetWhere(where);
            }
            if (string.IsNullOrEmpty(jinfo.CoDeptId) && jinfo.Header!= CheckNoteHedaerType.Header1)
            {
                throw new Exception("未輸入後送單位");
            }
            if (!string.IsNullOrEmpty(jinfo.CoOwnerId))
            {
                userinfo = userService.Get(jinfo.CoOwnerId);
                if (userinfo == null)
                {
                    jinfo.CoOwnerId = "";
                    jinfo.CoOwnerName = "";
                }
                else
                {
                    jinfo.CoOwnerId = userinfo.Id;
                    jinfo.CoOwnerName = userinfo.RealName;
                }
            }
            if (orderDtailinfo == null)
            {
                WorkOrderDetail detail = new WorkOrderDetail()
                {
                    WorkOrderId = jinfo.WorkOrderId,
                    ResponseTypeId = "2206163508106",
                    ResponseTypeName = nameof(ResponseType.核保審批),
                    CaseCategoryLayer1Id = jinfo.CaseCategoryLayer1Id,
                    CaseCategoryLayer1Name = jinfo.CaseCategoryLayer1Name,
                    CaseCategoryLayer2Id = jinfo.CaseCategoryLayer2Id,
                    CaseCategoryLayer2Name = jinfo.CaseCategoryLayer2Name,
                    Header = jinfo.Header,
                    Body = jinfo.Body,
                    CoDeptId = jinfo.CoDeptId,
                    CoOwnerId = jinfo.CoOwnerId,
                    CoOwnerName = userinfo.RealName,
                    Timeliness = jinfo.Timeliness,
                    CurrentEditorId = CurrentUser.UserId,
                    
                    

                };
                orderDtailinfo = detail.MapTo<WorkOrderDetail>();

                OnBeforeInsert(orderDtailinfo);
                if (orderDtailinfo.Header == CheckNoteHedaerType.Header1)
                {
                    orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                    orderDtailinfo.IsNotified = false;
                }
                else{
                    orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.核保審批中)).ConfigureAwait(false);
                    orderDtailinfo.CurrentEditorId = jinfo.CoOwnerId;
                    orderDtailinfo.IsNotified = false;
                }
                
                
                long ln = await iService.CreateSingleWorkOrderDetail(orderDtailinfo).ConfigureAwait(false);
                result.Success = ln > 0;
                result.Success = !String.IsNullOrEmpty(orderDtailinfo.WorkOrderId);

                if (ln > 0)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;

                   
                    result.ResData = "審批單號：" + orderDtailinfo.Id;

                    if (string.IsNullOrEmpty(orderDtailinfo.Body))
                    {
                        orderDtailinfo.Body = "";
                    }
                    await InsertEstimateCheckLog(orderDtailinfo, orderDtailinfo.Header, orderDtailinfo.Body, LogType.審核備註, LogMessageType.內部聯繫, jinfo.ReplyStatus);
                   
                }
                else
                {
                    result.ErrMsg = ErrCode.err43001;
                    result.ErrCode = "43001";


                }
            }
            else
            {

                
                WorkOrderDetail newinfo = orderDtailinfo.MapTo<WorkOrderDetail>();
                OnBeforeUpdate(newinfo);
                if (newinfo.Header != jinfo.Header && !string.IsNullOrEmpty(jinfo.Header))
                {
                    newinfo.Header = jinfo.Header;
                }
                if (newinfo.Body != jinfo.Body && !string.IsNullOrEmpty(jinfo.Body))
                {
                    newinfo.Body = jinfo.Body;
                }
                if (newinfo.Timeliness != jinfo.Timeliness && !string.IsNullOrEmpty(jinfo.Timeliness.ToString()))
                {
                    newinfo.Timeliness = jinfo.Timeliness;
                }
                if (newinfo.CoOwnerId != jinfo.CoOwnerId && !string.IsNullOrEmpty(jinfo.CoOwnerId))
                {
                    newinfo.CoOwnerId = jinfo.CoOwnerId;
                }
                if (newinfo.CoDeptId != jinfo.CoDeptId && !string.IsNullOrEmpty(jinfo.CoDeptId))
                {
                    newinfo.CoDeptId = jinfo.CoDeptId;
                }
                if (!string.IsNullOrEmpty(jinfo.CoDeptId) && !string.IsNullOrEmpty(jinfo.CoOwnerId))
                {

                    /*
                    if(workOrder.OwnerId == jinfo.CoOwnerId && workOrder.StatusName == nameof(WorkflowStatus.核保審批中))
                    {
                        newinfo.CurrentEditorId = jinfo.CoOwnerId;
                    }
                    */
                    if ( workOrder.StatusName == nameof(WorkflowStatus.核保審批中))
                    {
                        newinfo.CurrentEditorId = jinfo.CoOwnerId;
                    }

                }
                bool bl = await iService.UpdateAsync(newinfo, newinfo.Id).ConfigureAwait(false);
                if (bl)
                {
                    if (string.IsNullOrEmpty(newinfo.Body))
                    {
                        newinfo.Body = "";
                    }
                    await InsertEstimateCheckLog(newinfo, newinfo.Header, newinfo.Body,LogType.審核備註, LogMessageType.內部聯繫, jinfo.ReplyStatus);


                   
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;

                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }

            return ToJsonContent(result);
        }
        /// <summary>
        /// 新增管理者備註
        /// </summary>
        /// <param name="jinfo">WorkOrderLogInputDto</param>
        /// <returns></returns>
        [HttpPost("AddManagerNote")]
        [AurocoreAuthorize("Add")]
        public async Task<IActionResult> InsertAdminNoteAsync(WorkOrderDetailInputDto jinfo)
        {
            /*
             缺少判斷險種，填入欄位 InsuranceType
             */
            CommonResult result = new CommonResult();
            WorkOrder workOrder = new WorkOrder();
            WorkOrderDetail orderDtailinfo = new WorkOrderDetail();
            User userinfo = new User();
            
            if (string.IsNullOrEmpty(jinfo.EstimateId) && string.IsNullOrEmpty(jinfo.WorkOrderId))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "未填入案件或要保書 Id";
                return ToJsonContent(result);
            
            }

            if(string.IsNullOrEmpty(jinfo.Header))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "未填入管理者備註-處理標籤";
                return ToJsonContent(result);
              
            }
            if (string.IsNullOrEmpty(jinfo.WorkOrderId))
            {
                string where = string.Format("SourceId='{0}' ", jinfo.EstimateId);
                workOrder = workOrderService.GetWhere(where);
                
            }
            else
            {
                workOrder = workOrderService.Get(jinfo.WorkOrderId);
                
            }
            
            if (workOrder == null)
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "要保書尚未轉案件";
                return ToJsonContent(result);
            
            }
            else
            {
                if (workOrder.StatusName == nameof(WorkflowStatus.等待分配))
                {
                    result.ErrCode = ErrCode.err43001;
                    result.ErrMsg = "案件尚未分配";
                    return ToJsonContent(result);
                   
                }
                jinfo.WorkOrderId = workOrder.Id;
                string where = string.Format("WorkOrderId='{0}' And ResponseTypeName='{1}' ", workOrder.Id, ResponseType.核保審批);
                orderDtailinfo = iService.GetWhere(where);
            }
            if (string.IsNullOrEmpty(jinfo.CoDeptId) && jinfo.Header==AdminNoteHedaerType.Header1)
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "未輸入後送單位";
                return ToJsonContent(result);
               
            }
            if (!string.IsNullOrEmpty(jinfo.CoOwnerId))
            {
                userinfo = userService.Get(jinfo.CoOwnerId);
                if (userinfo == null)
                {
                    jinfo.CoOwnerId = "";
                    jinfo.CoOwnerName = "";
                }
                else
                {
                    jinfo.CoOwnerId = userinfo.Id;
                    jinfo.CoOwnerName = userinfo.RealName;
                }
            }
            if (orderDtailinfo == null)
            {
                var _where = string.Empty;
                var _item = new ItemsDetail();
                _item.ItemCode = "";
                if (!string.IsNullOrEmpty(jinfo.InsuranceTypeId))
                {
                    _where = string.Format("Id='{0}'", jinfo.InsuranceTypeId);
                    _item= itemsDetailService.GetWhere(_where);
                }
                string _coownerID = string.Empty;
                string _coownerRealName = string.Empty;

                if (!string.IsNullOrEmpty(jinfo.CoOwnerId) && !string.IsNullOrEmpty(jinfo.CoOwnerName))
                {
                    _coownerID= jinfo.CoOwnerId;
                    _coownerRealName= jinfo.CoOwnerName;
                }
                WorkOrderDetail detail = new WorkOrderDetail()
                {
                    WorkOrderId = jinfo.WorkOrderId,
                    ResponseTypeId = "2206163508106",
                    ResponseTypeName = nameof(ResponseType.核保審批),
                    InsuranceTypeId = jinfo.InsuranceTypeId,
                    InsuranceTypeName = _item.ItemCode,
                    CaseCategoryLayer1Id = jinfo.CaseCategoryLayer1Id,
                    CaseCategoryLayer1Name = jinfo.CaseCategoryLayer1Name,
                    CaseCategoryLayer2Id = jinfo.CaseCategoryLayer2Id,
                    CaseCategoryLayer2Name = jinfo.CaseCategoryLayer2Name,
                    Header = jinfo.Header,
                    Body = jinfo.Body,
                    CoDeptId = jinfo.CoDeptId,
                    CoOwnerId = _coownerID,
                    CoOwnerName = _coownerRealName,
                    Timeliness = jinfo.Timeliness,
                    CurrentEditorId = CurrentUser.UserId,
                    

                };
                orderDtailinfo = detail.MapTo<WorkOrderDetail>();
                OnBeforeInsert(orderDtailinfo);
                if (orderDtailinfo.Header == AdminNoteHedaerType.Header1)
                {
                    if (string.IsNullOrEmpty(jinfo.CoOwnerId)&& string.IsNullOrEmpty(jinfo.CoDeptId))
                    {
                        result.ErrCode = ErrCode.err43001;
                        result.ErrMsg = "核保審批需指定後送單位";
                        return ToJsonContent(result);
                        
                    }
                    var _couser = userService.Get(jinfo.CoOwnerId);
                    orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.核保審批中)).ConfigureAwait(false);
                    orderDtailinfo.IsNotified = false;
                    orderDtailinfo.CoOwnerName= _couser.RealName;
                    orderDtailinfo.CurrentEditorId = jinfo.CoOwnerId;

                   
                }
                else
                {
                    orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                    orderDtailinfo.IsNotified = false;
                }
               

                long ln = await iService.CreateSingleWorkOrderDetail(orderDtailinfo).ConfigureAwait(false);


                if (ln > 0)
                {
                    result.Success = ln > 0;
                    result.Success = !String.IsNullOrEmpty(orderDtailinfo.WorkOrderId);
                    result.RelationData = "案件編號：" + orderDtailinfo.WorkOrderId;
                    result.ResData = "審批單號：" + orderDtailinfo.Id;
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    jinfo.ReplyStatus = true;
                    if (string.IsNullOrEmpty(orderDtailinfo.Body))
                    {
                        orderDtailinfo.Body = "";
                    }

                    await InsertEstimateAdminLog(orderDtailinfo, orderDtailinfo.Header, orderDtailinfo.Body, LogType.管理者備註, LogMessageType.內部聯繫, jinfo.ReplyStatus);
                }

            }
            else
            {

                if (orderDtailinfo.StatusName == nameof(WorkflowStatus.協辦中) && !string.IsNullOrEmpty(orderDtailinfo.CoOwnerId) && (CurrentUser.UserId == orderDtailinfo.CreatorUserId))
                {
                    result.ErrCode = ErrCode.err43001;
                    result.ErrMsg = "單據由其他人負責，暫時無法修改";
                    return ToJsonContent(result);
                }
                WorkOrderDetail newinfo = orderDtailinfo.MapTo<WorkOrderDetail>();
                if (newinfo.Header != jinfo.Header && !string.IsNullOrEmpty(jinfo.Header))
                {
                    newinfo.Header = jinfo.Header;
                }
                if (newinfo.Body != jinfo.Body && !string.IsNullOrEmpty(jinfo.Body))
                {
                    newinfo.Body = jinfo.Body;
                }
                if (newinfo.Timeliness != jinfo.Timeliness && !string.IsNullOrEmpty(jinfo.Timeliness.ToString()))
                {
                    newinfo.Timeliness = jinfo.Timeliness;
                }
                if (newinfo.CoOwnerId != jinfo.CoOwnerId && !string.IsNullOrEmpty(jinfo.CoOwnerId))
                {
                    newinfo.CoOwnerId = jinfo.CoOwnerId;
                    newinfo.CurrentEditorId= jinfo.CoOwnerId;
                }
                if (newinfo.CoDeptId != jinfo.CoDeptId && !string.IsNullOrEmpty(jinfo.CoDeptId))
                {
                    newinfo.CoDeptId = jinfo.CoDeptId;
                   

                }
                if (newinfo.CaseCategoryLayer1Id != jinfo.CaseCategoryLayer1Id && !string.IsNullOrEmpty(jinfo.CaseCategoryLayer1Id))
                {
                    newinfo.CaseCategoryLayer1Id = jinfo.CaseCategoryLayer1Id;
                }
                if (newinfo.CaseCategoryLayer2Id != jinfo.CaseCategoryLayer2Id && !string.IsNullOrEmpty(jinfo.CaseCategoryLayer2Id))
                {
                    newinfo.CaseCategoryLayer2Id = jinfo.CaseCategoryLayer2Id;
                }
                if (newinfo.CaseCategoryLayer1Name != jinfo.CaseCategoryLayer1Name && !string.IsNullOrEmpty(jinfo.CaseCategoryLayer1Name))
                {
                    newinfo.CaseCategoryLayer1Name = jinfo.CaseCategoryLayer1Name;
                }
                if (newinfo.CaseCategoryLayer2Name != jinfo.CaseCategoryLayer2Name && !string.IsNullOrEmpty(jinfo.CaseCategoryLayer2Name))
                {
                    newinfo.CaseCategoryLayer2Name = jinfo.CaseCategoryLayer2Name;
                }
                if (newinfo.InsuranceTypeId != jinfo.InsuranceTypeId
                    && string.IsNullOrEmpty(newinfo.InsuranceTypeId))
                {
                    newinfo.InsuranceTypeId = jinfo.InsuranceTypeId;
                }
                if (newinfo.InsuranceTypeId != jinfo.InsuranceTypeId
                    && !string.IsNullOrEmpty(jinfo.InsuranceTypeId))
                {
                    jinfo.InsuranceTypeId=newinfo.InsuranceTypeId  ;
                }
                OnBeforeUpdate(newinfo);

                bool bl = await iService.UpdateAsync(newinfo, newinfo.Id).ConfigureAwait(false);
                if (bl)
                {
                    jinfo.ReplyStatus = true;
                    if (string.IsNullOrEmpty(newinfo.Body))
                    {
                        newinfo.Body = "";
                    }
                    await InsertEstimateAdminLog(newinfo, newinfo.Header, newinfo.Body, LogType.管理者備註, LogMessageType.內部聯繫, jinfo.ReplyStatus);


                   
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;

                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }
            return ToJsonContent(result);
        }
        #endregion


        /// <summary>
        /// <summary>
        /// 回覆 0800 memo 單
        /// </summary>
        /// <param name="workorderId">案件編號</param>
        /// <param name="caseInfo"></param>
        /// <param name="isSend">是否送出</param>
        /// <returns></returns>
        [HttpPost("ResponsetoMemoUnit")]
        [AurocoreAuthorize("Add")]
        public async Task<IActionResult> ResponseMemoUnit(string workorderId,ResponseCaseInfo caseInfo, string isSend="Y")
        {
            CommonResult result = new CommonResult();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            AppSetting sysSetting = System.Text.Json.JsonSerializer.Deserialize<AppSetting>(AurocoreCacheHelper.Get("SysSetting").ToJson());
            string key = sysSetting.MemoKey;
            string iv = sysSetting.MemoIV;
            string localDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            
            var _workorder = workOrderService.Get(workorderId);
            if (_workorder == null)
            {
               
                throw new Exception("無此 ID");
            }
            
            try
            {

                var caseJson = JsonHelper.ToObject<CaseInfo>(_workorder.Supplement);
                List<ResponseCaseInfo> responseCaseInfos = new List<ResponseCaseInfo>();
                var responseJson = new ResponseMemo();
                responseCaseInfos.Add(new ResponseCaseInfo { 
                    inumber = caseJson.inumber ,
                    dconnect = caseInfo.dconnect,
                    answer_user= caseInfo.answer_user,
                    nresult_other = caseInfo.nresult_other,
                    ncustomer=caseInfo.ncustomer,
                    nconnecter=caseInfo.nconnecter,
                    nprocess = caseInfo.nprocess,
                    ireply = caseInfo.ireply
                });
                responseJson.case_info = responseCaseInfos;
                string _tmp = $"{localDate}_{caseJson.inumber}";
                string encryptStr = Cryptography.AesCBCEncrypt(key, iv, _tmp);
                //string decryptStr = Cryptography.AesCBCDecrypt(key, iv, encryptStr);
                responseJson.Token = encryptStr;
                if (isSend == "Y")
                {
                    var client = new RestClient(sysSetting.MemoUrl);
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("SOAPAction", sysSetting.MemoSoapAction);
                    request.AddHeader("Content-Type", "text/xml; charset=utf-8");
                    var body = @$"<s:Envelope xmlns:s=""http://schemas.xmlsoap.org/soap/envelope/"">
                       <s:Body><GetECommerceReport080Memo xmlns = ""http://tempuri.org/"" >
                 <sJson>{JsonHelper.ToJson(responseJson)}</sJson></GetECommerceReport080Memo></s:Body ></s:Envelope > ";
                    request.AddParameter("text/xml; charset=utf-8", body, ParameterType.RequestBody);

                    IRestResponse response = client.Execute(request);
                    List<ResponseMemoStatus> _rtnList = new List<ResponseMemoStatus>();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(response.Content);

                    //Get all nodes by Tags.
                    XmlNodeList elemList = doc.GetElementsByTagName("GetECommerceReport080MemoResult");
                    for (int i = 0; i < elemList.Count; i++)
                    {
                        var _resStatus = JsonHelper.ToObject<ResponseMemoStatus>(elemList[i].InnerXml);
                        _rtnList.Add(new ResponseMemoStatus { msg = _resStatus.msg, status = _resStatus.status });
                        Log4NetHelper.Info(elemList[i].InnerXml);
                    }
                }
                string where = string.Format("ItemCode='{0}'", nameof(ResponseType.ReplyMemotoSender));
                var itemDetail = await itemsDetailService.GetWhereAsync(where).ConfigureAwait(false);
                WorkOrderDetail info = new();
                info.WorkOrderId = workorderId;
                info = await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                info.ResponseTypeId = itemDetail.Id;
                info.ResponseTypeName = itemDetail.ItemName;
                info.CurrentEditorId = CurrentUser.UserId;
                OnBeforeInsert(info);
                long ln = await iService.CreateSingleWorkOrderDetail(info).ConfigureAwait(false);
                
                if (ln > 0)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                    //result.ResData = $"Decrypt :{ decryptStr} | Encrypt:{encryptStr} ";
                    result.ResData = info;
                    await InsertWorkOrderLog(_workorder, info.Id, JsonHelper.ToJson(responseCaseInfos),LogType.回覆MEMO,LogMessageType.內部聯繫);
                }
               
                

            }
            catch (Exception ex)
            {
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode =ErrCode.err1;
                Log4NetHelper.Error(ex.ToString());
            }
            
           
            return ToJsonContent(result);

        }
        /// <summary>
        /// 發送訊息通知處理單位
        /// </summary>
        /// <param name="id">單據 Id</param>
        /// <returns></returns>
        [HttpPost("SendNotifytoNext")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> SendNotifywithUpdateFormStatus(string id)
        {
            CommonResult result = new CommonResult();
            string localDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            var info = iService.Get(id);
            if (info == null)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "無此 ID";
                return ToJsonContent(result);
                
            }
            Organize organize = organizeService.Get(info.CoDeptId);
            if (organize == null)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "部門不存在";
                return ToJsonContent(result);

            }else if(string.IsNullOrEmpty(organize.Email)){
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "部門共用電子郵件未設定，請查明後再發送";
                return ToJsonContent(result);

            }
            if (info.StatusName == nameof(WorkflowStatus.已結案))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "已結案，無法發送訊息";
                return ToJsonContent(result);
                
            }
            if (string.IsNullOrEmpty(info.CoOwnerId)&&string.IsNullOrEmpty(info.CoDeptId))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "未指定協辦對象,請修正後重新發送";
                return ToJsonContent(result);
                
            }
            if (string.IsNullOrEmpty(info.CoOwnerId))
            {
                info.CurrentEditorId = "";
            }
            else
            {
                info.CurrentEditorId = info.CoOwnerId;
            }
            if(string.IsNullOrEmpty(info.CoOwnerId) && !string.IsNullOrEmpty(info.CoDeptId))
            {
                await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.等待分配)).ConfigureAwait(false);
            }
            else
            {
                await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.協辦中)).ConfigureAwait(false);
            }
            
            OnBeforeUpdate(info);
            User couserinfo = userService.Get(info.CoOwnerId);
            User userinfo = userService.Get(info.CreatorUserId);
            
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                await InsertWorkOrderLog(info, info.Header, LogType.發送訊息,LogMessageType.內部聯繫);
                
                
                if (string.IsNullOrEmpty(info.CurrentEditorId))
                {
                    string mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{ info.ResponseTypeName} )";
                    string mailBodyTmp = $"(<h3>單據編號：{info.Id}</h3><br><br><h3>此單據尚未分配執行者</h3><br><br><h3>擔當者：{userinfo.RealName}</h3>)";
                    SendNotification(organize, mailHeaderTmp, mailBodyTmp, info.Attachment);
                }
                else
                {
                    string mailHeaderTmp = $"({DateTime.Now.ToString("yyyy/MM/dd-HH:mm")}新增+{ info.ResponseTypeName} )";
                    string mailBodyTmp = $"(<h3>單據編號：{info.Id}</h3><br><br><h4>{info.Header}</h4><br><br><h3>擔當者：{userinfo.RealName}</h3>)";
                    SendNotification(couserinfo, mailHeaderTmp, mailBodyTmp, info.Attachment);
                }
                
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                /*
                switch (info.ResponseTypeName)
                {
                    case nameof(ResponseType.EMail):
                        OnAfterReplyMail(jinfo);
                        break;
                    case nameof(ResponseType.SMS):
                        OnAfterSavetoSMS(info);
                        break;
                }*/
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);

        }
        /// <summary>
        /// 單據結案
        /// </summary>
        /// <param name="id">單據 Id</param>
        /// <returns></returns>
        [HttpPost("SetCloseStatus")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> SetFormCloseStatus(string id)
        {
            CommonResult result = new CommonResult();
            
            var info = iService.Get(id);
            if (info == null)
            {

                throw new Exception("無此 ID");
            }
            if (info.StatusName == nameof(WorkflowStatus.協辦中))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = "單據由其他人負責，暫時無法修改";
                return ToJsonContent(result);
            }
            await ChangeWorkOrderDetailStatus(info, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                WorkOrderDetail workOrderDetail = new();
          
                await InsertWorkOrderLog(workOrderDetail, workOrderDetail.Header, LogType.結案單據, LogMessageType.不分);
                

                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                /*
                switch (info.ResponseTypeName)
                {
                    case nameof(ResponseType.EMail):
                        OnAfterReplyMail(jinfo);
                        break;
                    case nameof(ResponseType.SMS):
                        OnAfterSavetoSMS(info);
                        break;
                }*/
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);

        }
        /// <summary>
        /// SendMail
        /// </summary>
        [HttpPost("sendmail")]
        [AllowAnonymous]
        
        public IActionResult  sendmail(MailBodyEntity mailBodyEntity)
        {
            CommonResult result = new CommonResult();
            List<string> recipients = new();
            recipients.Add("victor.cheng@aurocore.com");
            /*
            var mailBodyEntity = new MailBodyEntity()
            {
                Sender= "EZGO",
                Body = "test" + "\n\r請勿直接回複本郵件！",
                Recipients = recipients,
                Subject = "新安測試郵件"
               
            };
            */
            result.Success =EWSMailHelper.SendEmail(mailBodyEntity);
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// SendMail
        /// </summary>
        [HttpPost("staticsendmail")]
        [AllowAnonymous]

        public IActionResult staticsendmail(MailBodyEntity mailBodyEntity)
        {
            CommonResult result = new CommonResult();
            var ews = new ExchangeService(ExchangeVersion.Exchange2010_SP2);

            //公司內部Exchange比較簡單，可慱入帳密登入
            ews.Credentials = new NetworkCredential("050490", "Bb532532", "tmnewa");
            
          

            ews.Url = new Uri("https://webmail.tmnewa.com.tw/ews/exchange.asmx");

            var mail = new EmailMessage(ews);
            mail.ToRecipients.Add("victor.cheng@aurocore.com");
            mail.Subject = $"發信測試: {DateTime.Now:HHmmss}";
            mail.Body = new MessageBody(BodyType.Text, "Sent from Office E");
            try
            {
                mail.Send(); //或是 mail.SendAndSaveCopy();
            }
            catch(Exception ex)
            {
                Log4NetHelper.Error(ex.Message);
            }
            


            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 合併
        /// </summary>
        [HttpPost("MergeEndorsementPdf")]
        [AllowAnonymous]

        public IActionResult MergeEndorsementForm(EndorsementForm endorsementForm)
        {
            
            CommonResult result = new CommonResult();
            string filePath = string.Empty;
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            filePath = (_hostingEnvironment.WebRootPath + "/" + endorsementForm.FilePath).ToFilePath();
            result.Success = iService.MergeEndorsement(endorsementForm, filePath);
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = sysSetting.WebUrl + "/" + endorsementForm.FilePath;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 合併
        /// </summary>
        [HttpPost("MergeReissuePdf")]
        [AllowAnonymous]

        public IActionResult MergeReissueForm(ReissueForm reissueForm)
        {

            CommonResult result = new CommonResult();
            string filePath = string.Empty;
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            reissueForm.FilePath = (_hostingEnvironment.WebRootPath + "/" + "Template/").ToFilePath();
            filePath = (_hostingEnvironment.WebRootPath + "/"+sysSetting.Filepath+"/").ToFilePath();
            string newPath = iService.MergeReissue(reissueForm, filePath);
            if (!string.IsNullOrEmpty(newPath))
            {
                result.Success = true;
            }
            
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = sysSetting.WebUrl +"/"+  newPath;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 取得單一單據處理資料
        /// </summary>
        /// <param name="Id">單據編號</param>
        /// <returns></returns>
        [HttpPost("WorkOrderDetailBriefbyType")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> WorkOrderDetailTypeTree(string Id)
        {
            CommonResult result = new CommonResult();
            WorkOrderRealateList treeOutputDtos = await iService.WorkOrderDetailTypeTree(Id);
            if (treeOutputDtos != null)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = treeOutputDtos;
                
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }

            return ToJsonContent(result);
        }

        /// <summary>
        /// 非同步批量更新工作單據狀態
        /// </summary>
        /// <param name="info"></param>
        [HttpPost("SetOwnerStatusBatchAsync")]
        [AurocoreAuthorize("Enable")]
        public virtual async Task<IActionResult> SetEnabledMarktBatchAsync(UpdateStatusViewModel info)
        {
            CommonResult result = new CommonResult();
            int isHot = Convert.ToInt32(info.Ishot);
            string assistwhere = string.Format("ItemName='{0}'", nameof(WorkflowStatus.協辦中));
            var assistwhereItem = await itemsDetailService.GetWhereAsync(assistwhere);
            string pendingWhere = $"ItemName='{nameof(WorkflowStatus.等待分配)}' ";
            var pendingItem = await itemsDetailService.GetWhereAsync(pendingWhere).ConfigureAwait(false);
            var user = userService.Get(info.OwnerID);
            string where = string.Empty;
            string setField = string.Empty;
            object parameter = null;
            List<string> processWhere = new ();
            List<string> coprocessWhere = new();


            
            //取得字典單據狀態
            
            if (string.IsNullOrEmpty(info.StatusId))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "沒有選擇單據狀態";
                return ToJsonContent(result);
            }
            else
            {
                string itemwhere = string.Format("Id='{0}'", info.StatusId);
                var itemsDetail = await itemsDetailService.GetWhereAsync(itemwhere);
                info.StatusName = itemsDetail.ItemName;
            }
            // 未傳入編號時 判斷全選
            if (info.Ids.Length == 0)
            {
                if (info.All == false)
                {
                    result.ErrCode = ErrCode.err43002;
                    result.ErrMsg = "未傳入單據編號";
                    return ToJsonContent(result);
                }
                // 判斷全選
                if (info.All == true && CurrentUser != null)
                {
                    string caseWhere = $" StatusId='{pendingItem.Id}' ";
                    var _wods = await iService.GetListWhereAsync(caseWhere);
                    //判斷是否有未分配單據
                    if (_wods != null)
                    {
                        List<WorkOrderDetail> orderDetails = _wods.ToList();
                        if (orderDetails.Count == 0)
                        {
                            result.ErrCode = ErrCode.err43002;
                            result.ErrMsg = "沒有未分配單據";
                            return ToJsonContent(result);
                        }
                        else
                        {
                            foreach (var item in orderDetails)
                            {
                                IEnumerable<object> query = from t in orderDetails select t.Id;
                                object[] ids = query.ToArray();
                                info.Ids = ids;
                                if (info.Ids.Length == 0)
                                {
                                    result.ErrMsg = ErrCode.err43002;
                                    result.ErrCode = "單據編號有誤，請重新嘗試";
                                    return ToJsonContent(result);
                                }
                            }
                        }
                    }
                }

            }
            where = "id in ('" + info.Ids.Join(",").Replace(",", "','") + "')";
            var wods = await iService.GetListWhereAsync(where);
            if ((wods.ToList()).Count == 0)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "單據號碼有誤，請通知技術人員";
                return ToJsonContent(result);
            }
            switch (info.StatusName)
            {
                case nameof(WorkflowStatus.等待分配):
                    foreach (var wod in wods)
                    {
                        if(wod.StatusName == nameof(WorkflowStatus.已結案))
                        {
                            result.ErrMsg = ErrCode.err1;
                            result.ErrCode = wod.Id +" :此單據已結案，無法再分配";
                            return ToJsonContent(result);
                        }

                    }
                    setField = $"StatusId='{info.StatusId}',CurrentEditorId='',StatusName='{info.StatusName}',Ishot='{isHot}";
                    break;
                default:
                    foreach (var wod in wods)
                    {
                        if(wod.CreatorUserId == info.OwnerID)
                        {
                            processWhere.Add(wod.Id);
                        } 
                        if(string.IsNullOrEmpty(wod.CoOwnerId)&& !string.IsNullOrEmpty(wod.CoDeptId))
                        {
                            coprocessWhere.Add(wod.Id);
                        }
                        if (wod.CoOwnerId ==info.OwnerID)
                        {
                            coprocessWhere.Add(wod.Id);
                        }
                        if( (wod.StatusName == nameof(WorkflowStatus.處理中) || wod.StatusName == nameof(WorkflowStatus.協辦中)) && string.IsNullOrEmpty(info.Ishot.ToString()))
                        {
                            result.ErrCode = ErrCode.err1;
                            result.ErrMsg = wod.Id + " :此單據已有" + wod.StatusName + " 狀態，無法再分配";
                            return ToJsonContent(result);
                        }
                        if (wod.StatusName == nameof(WorkflowStatus.已結案) )
                        {
                           
                            result.ErrCode = ErrCode.err1;
                            result.ErrMsg = wod.Id + " :此單據已有"+ wod.StatusName+" 狀態，無法再分配";
                            return ToJsonContent(result);
                        }

                    }
                    
                    setField = $@"StatusId='{assistwhereItem.Id}',CurrentEditorId='{info.OwnerID}',
                        CoOwnerId='{info.OwnerID}',CoOwnerName='{user.RealName}',
                        StatusName='{assistwhereItem.ItemName}',IsNotified=0";

                    break;
            }
            
            where = "id in ('" + info.Ids.Join(",").Replace(",", "','") + "')";

            bool blresut = await iService.SetEnabledMarkByWhereAsync(setField, where, parameter, CurrentUser.UserId);

            if (blresut)
            {
                await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }


            return ToJsonContent(result);
        }
        
        /// <summary>
        /// 取得單一實體
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetById")]
        [AurocoreAuthorize("List")]
        public override async Task<CommonResult<WorkOrderDetailOutputDto>> GetById(string id)
        {
            CommonResult<WorkOrderDetailOutputDto> result = new CommonResult<WorkOrderDetailOutputDto>();
            if (CurrentUser == null)
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "使用者不存在";
                return result;
            }
            WorkOrderDetailOutputDto info = await iService.GetOutDtoAsync(id);
            if (info != null)
            {
                info.IsBlockDisplay = false;
                if (info.CurrentEditorId == CurrentUser.UserId)
                {
                    info.IsBlockDisplay = true;
                }
                // 判斷單據處理區塊是否顯示
                /*
                if (!string.IsNullOrEmpty(info.CurrentEditorId))
                {
                    if (info.CurrentEditorId == info.CoOwnerId && info.CreatorUserId == CurrentUser.UserId)
                    {
                        info.IsBlockDisplay = false;
                    }

                    if (info.CreatorUserId != CurrentUser.UserId && info.CoOwnerId != CurrentUser.UserId)
                    {
                        info.IsBlockDisplay = false;
                    }
                    if (info.StatusName == nameof(WorkflowStatus.已結案))
                    {
                        info.IsBlockDisplay = false;
                    }
                }*/

                WorkOrder workOrder = workOrderService.Get(info.WorkOrderId);

                if (workOrder != null)
                {
                    result.Success = true;
                    result.ErrCode = ErrCode.successCode;
                    result.ResData = info;
                    result.RelationData = workOrder;

                }


            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return result;
        }
        /*
        /// <summary>
        /// 取得單一單據實體
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetById")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetId(string id)
        {
            CommonResult result = new CommonResult();
            if (CurrentUser == null)
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "使用者不存在";
                return ToJsonContent(result);
            }
            WorkOrderDetailOutputDto info = await iService.GetOutDtoAsync(id);
            if (info != null)
            {
                info.IsBlockDisplay = true;
                // 判斷單據處理區塊是否顯示
                if (!string.IsNullOrEmpty(info.CurrentEditorId))
                {
                    if (info.CurrentEditorId == info.CoOwnerId && info.CreatorUserId == CurrentUser.UserId)
                    {
                        info.IsBlockDisplay = false;
                    }

                    if (info.CreatorUserId != CurrentUser.UserId && info.CoOwnerId != CurrentUser.UserId)
                    {
                        info.IsBlockDisplay = false;
                    }
                    if (info.StatusName == nameof(WorkflowStatus.已結案))
                    {
                        info.IsBlockDisplay = false;
                    }
                }

                WorkOrder workOrder = workOrderService.Get(info.WorkOrderId);

                if (workOrder != null)
                {
                    result.ErrCode = ErrCode.successCode;
                    result.ResData = info;
                    result.RelationData = workOrder;

                }


            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }
        */



    }
}