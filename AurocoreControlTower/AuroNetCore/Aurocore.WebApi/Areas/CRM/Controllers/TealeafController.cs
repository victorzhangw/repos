using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.WebAnalytics.Tealeaf.Dtos;
using Aurocore.WebAnalytics.Tealeaf.Models;
using Aurocore.WebAnalytics.Tealeaf.IServices;

namespace Aurocore.TealeafApi.Areas.Tealeaf.Controllers
{
    /// <summary>
    /// Tealeaf Events接口
    /// </summary>
    [ApiController]
    [Route("api/Tealeaf/[controller]")]
    public class TealeafController : AreaApiController<TealeafModel, TealeafOutputDto,TealeafInputDto,ITealeafService,string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public TealeafController(ITealeafService _iService) : base(_iService)
        {
            iService = _iService;
            AuthorizeKey.ListKey = "Tealeaf/List";
            AuthorizeKey.InsertKey = "Tealeaf/Add";
            AuthorizeKey.UpdateKey = "Tealeaf/Edit";
            AuthorizeKey.UpdateEnableKey = "Tealeaf/Enable";
            AuthorizeKey.DeleteKey = "Tealeaf/Delete";
            AuthorizeKey.DeleteSoftKey = "Tealeaf/DeleteSoft";
            AuthorizeKey.ViewKey = "Tealeaf/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(TealeafModel info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(TealeafModel info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(TealeafModel info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
    }
}