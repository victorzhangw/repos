using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.IServices;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Email.Core;
using Aurocore.WorkOrders.Enums;
using Aurocore.WorkOrders.Services;
using Microsoft.AspNetCore.Authorization;
using Aurocore.Security.Models;
using Aurocore.Security.IServices;
using System.Text.Json;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.SMS.Mitake;
using Aurocore.Commons.Json;
using Microsoft.AspNetCore.SignalR;
using Aurocore.WebApi.Hubs;

namespace Aurocore.JobApi.Areas.WorkOrders.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/WorkOrder/[controller]")]
    public class WorkOrderLogController : AreaApiController<WorkOrderLog, WorkOrderLogOutputDto,WorkOrderLogInputDto,IWorkOrderLogService,string>
    {
        private readonly IWorkOrderService workOrderService;
        private readonly IUserService userService;
        private readonly IWorkOrderDetailService workOrderDetailService;
        private readonly IItemsDetailService itemDetailService;
        private readonly IHubContext<BroadcastHub> hubContext;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_workOrderDetailiService"></param>
        /// <param name="_workorderservice"></param>
        /// <param name="_iUserService"></param>
        /// <param name="_itemsDetailService"></param>
        /// <param name="_hubContext"></param>
        public WorkOrderLogController(IWorkOrderLogService _iService, IWorkOrderDetailService _workOrderDetailiService,
                    IWorkOrderService _workorderservice, IUserService _iUserService,
                    IItemsDetailService _itemsDetailService, IHubContext<BroadcastHub> _hubContext) : base(_iService)
        {
            iService = _iService;
            this.workOrderService = _workorderservice;
            this.workOrderDetailService = _workOrderDetailiService;
            this.userService= _iUserService;
            this.itemDetailService = _itemsDetailService;
            hubContext = _hubContext;
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(WorkOrderLog info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(WorkOrderLog info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }
        /// <summary>
        /// 在更新資料前對WorkOrderDetail的狀態操作
        /// </summary>
        /// <param name="dinfo"></param>
        /// <param name="workflowstatus">狀態</param>
        /// <returns></returns>
        private async Task<WorkOrderDetail> ChangeWorkOrderDetailStatus(WorkOrderDetail dinfo, string workflowstatus)
        {
            string where = string.Format("ItemCode='{0}'", workflowstatus);
            var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
            dinfo.StatusId = itemDetail.Id;
            dinfo.StatusName = itemDetail.ItemCode;


            return dinfo;
        }

        /// <summary>
        /// 在更新資料前對WorkOrderDetail的修改操作
        /// </summary>
        /// <param name="dinfo"></param>
        ///  <param name="id"></param>
        /// <returns></returns>
        private async Task<bool> UpdateWorkOrderDetail(WorkOrderDetail dinfo, string id)
        {
            dinfo.LastModifyUserId = CurrentUser.UserId;
            dinfo.LastModifyTime = DateTime.Now;
            bool bl = await workOrderDetailService.UpdateAsync(dinfo, id).ConfigureAwait(false);
            if (bl)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(WorkOrderLog info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 儲存後寄發簡訊
        /// </summary>
        /// <param name="info">ReplyMessage</param>
        private CommonResult SendSms(ReplyMessage info)
        {
            MitakeSMS mitakeSMS = new MitakeSMS();
            CommonResult result = new CommonResult();

            bool sended = mitakeSMS.Send(info.ContactId, info.Header, out string returnMsg);
            if (sended)
            {
                result.ErrCode = ErrCode.err0;
                result.ErrMsg = returnMsg;
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = returnMsg;
            }
            return result;
        }
        /// <summary>
        /// 寄發內部通知郵件
        /// </summary>
        /// <param name="notifyHeader"></param>
        /// <param name="notifyBody"></param>
        /// <param name="notifyAttachments"></param>
        /// <param name="user"></param>
        protected async void SendNotification(User user, string notifyHeader, string notifyBody, string notifyAttachments = "")
        {

            //string content = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));
            string content = notifyBody;
            string header = notifyHeader;

            List<string> attachemnts = new();
            List<string> recipent = new();
            recipent.Add(user.Email);

            attachemnts = notifyAttachments.Split(",").ToList();
            List<MailFile> lstmailFile = new();

            foreach (var attachment in attachemnts)
            {
                string mime = MimeKit.MimeTypes.GetMimeType(attachment);
                string ext = System.IO.Path.GetExtension(attachment).ToLower();
                lstmailFile.Add(new MailFile { MailFilePath = attachment, MailFileType = mime, MailFileSubType = ext });
            }
            var mailBodyEntity = new MailBodyEntity()
            {
                Sender = "新安東京海上產物保險",
                BoxType = "EzGo",
                Body = content + "</br>\n\r\n\r請勿直接回複本郵件！",
                Recipients= recipent,
                Subject = header,
                MailFiles = lstmailFile,

            };
            EWSMailHelper.SendEmail(mailBodyEntity);
            await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
            await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");



        }
        /// <summary>
        /// 寄發外部通知
        /// </summary>
        /// <param name="notifyHeader">主旨</param>
        /// <param name="notifyBody">內文</param>
        /// <param name="notifyAttachments">附件</param>
        /// <param name="contactemail">聯絡郵件</param>
        protected async void SendNotification(string contactemail, string notifyHeader, string notifyBody, string notifyAttachments = "")
        {

            //string content = Encoding.UTF8.GetString(Convert.FromBase64String(info.Body));
            string content = notifyBody;
            string header = notifyHeader;

            List<string> attachemnts = new();
            List<string> recipent = new();
            recipent.Add(contactemail);

            attachemnts = notifyAttachments.Split(",").ToList();
            List<MailFile> lstmailFile = new();

            foreach (var attachment in attachemnts)
            {
                string mime = MimeKit.MimeTypes.GetMimeType(attachment);
                string ext = System.IO.Path.GetExtension(attachment).ToLower();
                lstmailFile.Add(new MailFile { MailFilePath = attachment, MailFileType = mime, MailFileSubType = ext });
            }
            var mailBodyEntity = new MailBodyEntity()
            {
                Sender = "新安東京海上產物保險",
                BoxType = "EzGo",
                Body = content + "</br>\n\r\n\r請勿直接回複本郵件！",
                Recipients = recipent,
                Subject = header,
                MailFiles = lstmailFile,

            };
            EWSMailHelper.SendEmail(mailBodyEntity);
            await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
            await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
        }
        /// <summary>
        /// 非同步更新資料(不傳送通知)
        /// </summary>
        /// <param name="jinfo">WorkOrderLogInputDto</param>
        /// <param name="id">歷程記錄Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(WorkOrderLogInputDto jinfo, string id)
        {
            CommonResult result = new CommonResult();

            WorkOrderLog info = iService.Get(id);
            info.EnabledMark = jinfo.EnabledMark;
            info.ReplyMessage = jinfo.ReplyMessage;
            info.ReplyStatus = jinfo.ReplyStatus;
            WorkOrderDetail orderDtailinfo = workOrderDetailService.Get(jinfo.WorkOrderDetailId);
            if (string.IsNullOrEmpty(jinfo.ReplyStatus.ToString()) && orderDtailinfo.StatusName == nameof(WorkflowStatus.協辦中))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = ErrCode.err43001;

            }
            OnBeforeUpdate(info);

            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 新增回覆資料
        /// </summary>
        /// <param name="jinfo">WorkOrderLogInputDto</param>
        /// <returns></returns>
        [HttpPost("Insert")]
        [AurocoreAuthorize("Add")]
        public override async Task<IActionResult> InsertAsync(WorkOrderLogInputDto jinfo)
        {
            CommonResult result = new CommonResult();
            WorkOrderDetail orderDtailinfo = workOrderDetailService.Get(jinfo.WorkOrderDetailId);
            WorkOrder workOrder = workOrderService.Get(jinfo.WorkOrderId);
            if( workOrder == null)
            {
                throw new Exception("案件號碼不存在");
            }
            if (orderDtailinfo == null)
            {
                throw new Exception("案件相關單據號碼不存在");
            }
            if (string.IsNullOrEmpty(jinfo.ReplyStatus.ToString())&& orderDtailinfo.StatusName == nameof(WorkflowStatus.協辦中))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg =  ErrCode.err43001;

            }

            User user = userService.Get(orderDtailinfo.CreatorUserId);
            WorkOrderLog info = jinfo.MapTo<WorkOrderLog>();

            //info.CurrentFormLog = JsonSerializer.Serialize(orderDtailinfo);
            OnBeforeInsert(info);

            long ln = await iService.InsertAsync(info).ConfigureAwait(true);
            
            result.Success = ln > 0;
            result.Success = !String.IsNullOrEmpty(info.WorkOrderId);
            
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = info;
                //await hubContext.Clients.All.SendAsync("SendMessage", "WorkOrderBriefbyType");
                //await hubContext.Clients.All.SendAsync("SendMessage", "WorkOrderTreeBriefData");

            }
            else
            {
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }
    
        


        /// <summary>
        /// 傳送回覆訊息與改變單據狀態
        /// </summary>
        /// <param name="id">歷程記錄Id</param>
        /// <returns></returns>
        [HttpPost("SendNotifytoNext")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> SendReplyNotify(string id)
        {
            CommonResult result = new CommonResult();
            try
            {
                
                WorkOrderLog info = iService.Get(id);
                WorkOrderDetail orderDtailinfo = workOrderDetailService.Get(info.WorkOrderDetailId);
                WorkOrder workOrder = workOrderService.Get(info.WorkOrderId);
                User user = new();
                string mailHeaderTmp = string.Empty;
                string mailBodyTmp = string.Empty;
                if (orderDtailinfo.CurrentEditorId == orderDtailinfo.CoOwnerId)
                {
                    if ((bool)info.ReplyStatus)
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                        mailHeaderTmp = $"[核可] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }
                    else
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                        orderDtailinfo.CurrentEditorId = orderDtailinfo.CreatorUserId;
                        
                        mailHeaderTmp = $"[拒絕] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }

                }else if(orderDtailinfo.CurrentEditorId == orderDtailinfo.CreatorUserId)
                {
                    orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.協辦中)).ConfigureAwait(false);
                    orderDtailinfo.CurrentEditorId = orderDtailinfo.CoOwnerId;
                    mailHeaderTmp = $"[回覆] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                    mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";

                }
                user = await userService.GetWhereAsync("Id='" + orderDtailinfo.CurrentEditorId + "'");
                string where = string.Format("ItemCode='{0}'", nameof(ResponseType.InternalEMail));
                var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
                ReplyMessage replyMessage = new()
                {
                    MessageType = LogMessageType.內部聯繫.ToString(),
                    ContactCategory = itemDetail.Id,
                    ContactId = user.Email,
                    ContactName = user.RealName,
                    Header = info.ReplyMessage
                };
                string _message = JsonHelper.ToJson(replyMessage);
                info.ReplyMessage = _message;
                info.ToUserId = user.Id;
                info.LogTypeName = LogType.回覆訊息.ToString();
                try
                {
                    bool orderdtailbl = await UpdateWorkOrderDetail(orderDtailinfo, orderDtailinfo.Id).ConfigureAwait(false);
                    bool logbl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
                    if (orderdtailbl && logbl)
                    {
                        SendNotification(user, mailHeaderTmp, mailBodyTmp);
                        result.ErrMsg = ErrCode.successCode1;
                        result.ErrCode = ErrCode.err0;
                    }
                    else
                    {
                        result.ErrMsg = ErrCode.err1;
                        result.ErrCode = ErrCode.err43002;
                    }
                }
                catch(Exception ex)
                {
                    result.ErrMsg = ErrCode.err1;
                    result.ErrCode = ex.ToString();

                }
               
                



            }
            catch(Exception ex)
            {
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = ex.ToString();
            }
           
            return ToJsonContent(result);
        }
        /// <summary>
        /// 傳送回覆訊息與改變單據狀態
        /// </summary>
        /// <param name="jinfo">案件 Log</param>
        /// <returns></returns>
        [HttpPost("SendLogNotifytoNext")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> SendLogReplyNotify(WorkOrderLogInputDto jinfo)
        {
            CommonResult result = new CommonResult();
            WorkOrderDetail orderDtailinfo = workOrderDetailService.Get(jinfo.WorkOrderDetailId);
            WorkOrder workOrder = workOrderService.Get(jinfo.WorkOrderId);
            string mailHeaderTmp = string.Empty;
            string mailBodyTmp = string.Empty;
            if (workOrder == null)
            {
                throw new Exception("案件號碼不存在");
            }
            if (orderDtailinfo == null)
            {
                throw new Exception("案件相關單據號碼不存在");
            }
            if (orderDtailinfo.StatusName == nameof(WorkflowStatus.已結案))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "單據已結案";
                return ToJsonContent(result);
            }
            if (workOrder.StatusName == nameof(WorkflowStatus.已結案))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "案件已結案";
                return ToJsonContent(result);
            }
            if (orderDtailinfo.CurrentEditorId != CurrentUser.UserId)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "單據目前處理中，不可回覆訊息";
                return ToJsonContent(result);
            }
            if (string.IsNullOrEmpty(jinfo.ReplyStatus.ToString()) )
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "未提供回覆狀態";
                return ToJsonContent(result);
            }
            
            User user = userService.Get(orderDtailinfo.CreatorUserId);
            WorkOrderLog info = jinfo.MapTo<WorkOrderLog>();
            OnBeforeInsert(info);
            long ln = await iService.InsertAsync(info).ConfigureAwait(false);

            if (ln > 0)
            {
                if (orderDtailinfo.CurrentEditorId == orderDtailinfo.CoOwnerId)
                {
                    if ((bool)info.ReplyStatus)
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                        mailHeaderTmp = $"[結案] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }
                    else
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.處理中)).ConfigureAwait(false);
                        orderDtailinfo.CurrentEditorId = orderDtailinfo.CreatorUserId;
                        mailHeaderTmp = $"[回覆] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }

                }
                else if (orderDtailinfo.CurrentEditorId == orderDtailinfo.CreatorUserId )
                {
                    if ((bool)info.ReplyStatus && string.IsNullOrEmpty(orderDtailinfo.CoOwnerId))
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                        mailHeaderTmp = $"[核可] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }
                    else if((bool)info.ReplyStatus && !string.IsNullOrEmpty(orderDtailinfo.CoOwnerId))
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.已結案)).ConfigureAwait(false);
                        mailHeaderTmp = $"[核可] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }
                    else
                    {
                        orderDtailinfo = await ChangeWorkOrderDetailStatus(orderDtailinfo, nameof(WorkflowStatus.協辦中)).ConfigureAwait(false);
                        orderDtailinfo.CurrentEditorId = orderDtailinfo.CoOwnerId;
                        mailHeaderTmp = $"[回覆] 單據編號：{info.WorkOrderDetailId}-{DateTime.Now.ToString("yyyy/MM/dd:HH:mm")}";
                        mailBodyTmp = $"<h3>{info.ReplyMessage}</h3>";
                    }
                       

                }
                string where = string.Format("ItemCode='{0}'", nameof(ResponseType.InternalEMail));
                var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
                ReplyMessage replyMessage = new()
                {
                    MessageType = LogMessageType.內部聯繫.ToString(),
                    ContactCategory = itemDetail.Id,
                    ContactId = user.Id,
                    ContactWay = user.Email,
                    ContactName = user.RealName,
                    Header = info.ReplyMessage
                };
                string _message = JsonHelper.ToJson(replyMessage);
                info.ReplyMessage = _message;
                info.ToUserId = user.Id;
                info.LogTypeName = LogType.回覆訊息.ToString();
                try
                {
                    bool orderdtailbl = await UpdateWorkOrderDetail(orderDtailinfo, orderDtailinfo.Id).ConfigureAwait(false);
                    bool logbl = await iService.UpdateAsync(info, info.Id).ConfigureAwait(false);
                    if (orderdtailbl && logbl)
                    {
                        SendNotification(user, mailHeaderTmp, mailBodyTmp);
                        
                        result.ErrMsg = ErrCode.successCode1;
                        result.ErrCode = ErrCode.err0;
                    }
                    else
                    {
                        result.ErrMsg = ErrCode.err1;
                        result.ErrCode = ErrCode.err43002;
                    }
                }
                catch (Exception ex)
                {
                    result.ErrMsg = ErrCode.err1;
                    result.ErrCode = ex.ToString();

                }
            }


            return ToJsonContent(result);
        }
        /// <summary>
        /// 傳送簡訊至客戶
        /// </summary>
        /// <param name="workordertailId">工作單據編號</param>
        /// <param name="replyMessage"></param>
        /// <returns></returns>
        [HttpPost("SendNotifytoCustomerbySms")]
        [AurocoreAuthorize("Add")]
        public async Task<IActionResult> SendNotifybySms(string workordertailId, ReplyMessage replyMessage)
        {
            CommonResult result = new CommonResult();
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.ExternalSMS));
            var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
            replyMessage.ContactCategory = itemDetail.Id;
            replyMessage.MessageType = LogMessageType.外部聯繫.ToString();
            var orderDetail = workOrderDetailService.Get(workordertailId);
            
            WorkOrderLog info = new();
            info.WorkOrderDetailId = workordertailId;
            info.WorkOrderId = orderDetail.WorkOrderId;
            info.ReplyMessage= JsonHelper.ToJson(replyMessage);
            info.LogTypeName = LogType.客戶聯繫.ToString();
            OnBeforeInsert(info);
            var rst =SendSms(replyMessage);

            if (rst.ErrCode == ErrCode.err0)
            {
                long ln = await iService.InsertAsync(info).ConfigureAwait(true);
                if (ln > 0)
                {
                    result.ErrCode = ErrCode.err0;
                    result.ErrMsg = ErrCode.successCode;
                }
                else
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = ErrCode.err43001;

                }
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = rst.ErrMsg;
            }
            
            
            return ToJsonContent(result);
        }
        /// <summary>
        /// 撥打電話至客戶
        /// </summary>
        /// <param name="workordertailId">工作單據編號</param>
        /// <param name="replyMessage"></param>
        /// <returns></returns>
        [HttpPost("AfterCalloutCustomer")]
        [AurocoreAuthorize("Add")]

        public async Task<IActionResult> SendNotifybyPhone(string workordertailId, ReplyMessage replyMessage)
        {
            CommonResult result = new CommonResult();
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.話務記錄));
            var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
            replyMessage.ContactCategory = itemDetail.Id;
            replyMessage.MessageType = LogMessageType.外部聯繫.ToString();
            var orderDetail = workOrderDetailService.Get(workordertailId);
            WorkOrderLog info = new();
            info.WorkOrderDetailId = workordertailId;
            info.WorkOrderId = orderDetail.WorkOrderId;
            info.ReplyMessage = JsonHelper.ToJson(replyMessage);
            info.LogTypeName = LogType.客戶聯繫.ToString();
            OnBeforeInsert(info);
            long ln = await iService.InsertAsync(info).ConfigureAwait(true);
            if (ln > 0)
            {
                result.ErrCode = ErrCode.err0;
                result.ErrMsg = ErrCode.successCode;
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = ErrCode.err43001;

            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 傳送郵件至客戶
        /// </summary>
        /// <param name="workordertailId">工作單據編號</param>
        /// <param name="replyMessage"></param>
        /// <returns></returns>
        [HttpPost("SendNotifytoCustomerbyEmail")]
        [AurocoreAuthorize("Add")]

        public async Task<IActionResult> SendNotifybyEmail(string workordertailId, ReplyMessage replyMessage)
        {
            CommonResult result = new CommonResult();
            string where = string.Format("ItemCode='{0}'", nameof(ResponseType.ExternalEMail));
            var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
            replyMessage.ContactCategory = itemDetail.Id;
            replyMessage.MessageType = LogMessageType.外部聯繫.ToString();
            var orderDetail = workOrderDetailService.Get(workordertailId);
            WorkOrderLog info = new();
            info.WorkOrderDetailId = workordertailId;
            info.WorkOrderId = orderDetail.WorkOrderId;
            info.ReplyMessage = JsonHelper.ToJson(replyMessage);
            info.LogTypeName = LogType.客戶聯繫.ToString();
            OnBeforeInsert(info);
            SendNotification(replyMessage.ContactId,replyMessage.Header, replyMessage.Body);
            long ln = await iService.InsertAsync(info).ConfigureAwait(true);
            if (ln > 0)
            {
                result.ErrCode = ErrCode.err0;
                result.ErrMsg = ErrCode.successCode;
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = ErrCode.err43001;

            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 依據單據分類取得全部聯絡記錄
        /// </summary>
        /// <param name="workordertailId">工作單據編號</param>
        /// <param name="messageTypeId">外部聯繫 = 0,內部聯繫 = 1</param>
        /// <returns></returns>
        [HttpGet("GetLogListbyMessageType")]
        [AurocoreAuthorize("List")]

        public async Task<IActionResult> GetLogList(string workordertailId, string messageTypeId)
        {
            CommonResult result = new CommonResult();
            try
            {
                string messageType = string.Empty;
                if (messageTypeId == "0")
                {
                    messageType = nameof(LogMessageType.外部聯繫);
                }
                else
                {
                    messageType = nameof(LogMessageType.內部聯繫);
                }
                var wd  = await workOrderDetailService.GetAsync(workordertailId);
                if(wd == null)
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = ErrCode.err60001;
                    return ToJsonContent(result);

                }
                string where = string.Format("ItemCode='{0}'", nameof(ResponseType.ExternalEMail));
                var itemDetail = await itemDetailService.GetWhereAsync(where).ConfigureAwait(false);
                string logwhere = string.Format("WorkOrderDetailId={0} AND ISJSON(ReplyMessage) > 0 AND JSON_VALUE(ReplyMessage, '$.MessageType') = '{1}'", workordertailId, messageType);
                
                var logList = await iService.GetListWhereAsync(logwhere).ConfigureAwait(false);
                List<ReplyMessage> replyMessages = new();
                foreach (var log in logList)
                {
                    var _msgObj = JsonHelper.ToObject<ReplyMessage>(log.ReplyMessage);
                    replyMessages.Add(new ReplyMessage
                    {
                        ContactId = _msgObj.ContactId,
                        MessageType = _msgObj.MessageType,
                        ContactCategory =_msgObj.ContactCategory,
                        ContactName = _msgObj.ContactName,
                        Header = _msgObj.Header,
                        Body = _msgObj.Body,
                        Notes = _msgObj.Notes,
                        Attachment = _msgObj.Attachment
                    });
                }
                result.ErrCode = ErrCode.err0;
                result.ErrMsg = ErrCode.successCode;
                result.ResData = replyMessages;
            }
            catch
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = ErrCode.err40110;
            }
            
            
            return ToJsonContent(result);
        }

    }
}