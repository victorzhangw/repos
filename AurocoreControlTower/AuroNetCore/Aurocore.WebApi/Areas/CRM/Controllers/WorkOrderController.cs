using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.IServices;
using Aurocore.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Dtos;
using System.Reflection;
using Aurocore.AspNetCore.ViewModel;
using System.Linq;
using Aurocore.Commons.Extensions;
using Microsoft.AspNetCore.Hosting;
using Aurocore.Security.Models;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Json;
using Aurocore.WorkOrders.Enums;
using Aurocore.Security.IServices;
using Newtonsoft.Json;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.SignalR;
using Aurocore.WebApi.Hubs;
using Newtonsoft.Json.Linq;
using Aurocore.Customers.IServices;
using Aurocore.Customers.Models;
using Aurocore.Commons.Extend;
using Aurocore.CMS.Models;

namespace Aurocore.WebApi.Areas.CRM.Controllers
{
    /// <summary>
    /// 案件接口
    /// </summary>
    [ApiController]
    [Route("api/WorkOrder/[controller]")]
    public class WorkOrderController : AreaApiController<WorkOrder, WorkOrderOutputDto,WorkOrderInputDto,IWorkOrderService,string>
    {
        private const string T = "yyyy/MM/dd";
        private readonly IWorkOrderDetailService workOrderDetailService;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IUserService userService;
        private readonly IItemsDetailService itemsDetailService;
        private readonly IOutBoundNameListService  outBoundNameListService;
        private readonly IOutBoundService outBoundService;
        private readonly IHubContext<BroadcastHub> hubContext;
        private readonly IOrganizeService organizeService;
        private readonly IPolicyService policyService;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService">案件</param>
        /// <param name="_WorkOrderDetailService">單據</param>
        /// <param name="hostingEnvironment">主機環境</param>
        /// <param name="_itemsDetailService">資料字典</param>
        /// <param name="_outBoundNameListService">外撥名單明細</param>
        /// <param name="_outBoundService">外撥名單匯總</param>
        /// <param name="_hubContext">Signalr Hub</param>
        /// <param name="_organizeService"></param>
        /// <param name="_userService"></param>
        /// <param name="_policyService"></param>
        public WorkOrderController(IWorkOrderService _iService,IWorkOrderDetailService _WorkOrderDetailService, 
            IItemsDetailService _itemsDetailService, IWebHostEnvironment hostingEnvironment,IPolicyService _policyService,    
            IOutBoundNameListService _outBoundNameListService, IOutBoundService _outBoundService,  
            IHubContext<BroadcastHub> _hubContext, IOrganizeService _organizeService,IUserService _userService) : base(_iService)
        {
            iService = _iService;
            workOrderDetailService = _WorkOrderDetailService;
            itemsDetailService = _itemsDetailService;
            AuthorizeKey.ListKey = "WorkOrder/List";
            AuthorizeKey.InsertKey = "WorkOrder/Add";
            AuthorizeKey.UpdateKey = "WorkOrder/Edit";
            AuthorizeKey.UpdateEnableKey = "WorkOrder/Enable";
            AuthorizeKey.DeleteKey = "WorkOrder/Delete";
            AuthorizeKey.DeleteSoftKey = "WorkOrder/DeleteSoft";
            AuthorizeKey.ViewKey = "WorkOrder/View";
            _hostingEnvironment = hostingEnvironment;
            outBoundService = _outBoundService;
            outBoundNameListService = _outBoundNameListService;
            hubContext = _hubContext;
            organizeService = _organizeService;
            userService = _userService;
            policyService = _policyService;
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(WorkOrder info)
        {
            info.Id = GuidUtils.CreateShortNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeptId = CurrentUser.DeptId;
            info.CaseOpenTime = DateTime.Now;
            info.DeleteMark = false;
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(WorkOrder info)
        {
            
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(WorkOrder info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
       
        /// <summary>
        /// 獲取所有電子郵件
        /// </summary>
        /// <param name="ItemName"></param>
        /// <returns></returns>
        [HttpPost("RetrieveEmail")]
        [AllowAnonymous]
        public async Task<IActionResult> RetrieveEmail(string ItemName)
        {
            CommonResult result = new CommonResult();
            //AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            //SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            string localpath = _hostingEnvironment.WebRootPath;
            var rtnList = await iService.RetrieveEmail(ItemName, _hostingEnvironment);
            if (rtnList.Count != 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ResData = rtnList;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 匯入 MEMO 單
        /// </summary>
        /// <param name="memo"></param>
        /// <returns></returns>
        [HttpPost("ImportMemo")]
        [AllowAnonymous]
        public async Task<IActionResult> ImportMemo(Memo memo)
        {
            CommonResult result = new CommonResult();
            var rtnList = await iService.ImportMemo(memo);
            if (rtnList.Count!=0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ResData = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss")+" : "+"已匯入 MEMO 資料筆數:"+ rtnList.Count;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 匯入車險訂單轉案件
        /// </summary>
        /// <param name="importEstimate">輸入要保書 Id 模型</param>
        /// <remarks>EstimateIds ：要保書 Id，以逗號分開</remarks>
        /// <remarks>EstimateType : 險種類別 </remarks>
        /// <example>傷害險 InjuryInsType </example>
        /// <example>車險 VehicleInsType</example>
        /// <example>住火險 RfInsType</example>
        /// 

        /// <returns></returns>
        [HttpPost("ImportEstimate")]
        
        public async Task<IActionResult> ImportEstimate(ImportEstimateInputDto importEstimate)
        {
            CommonResult result = new CommonResult();
           
            if (string.IsNullOrEmpty(importEstimate.EstimateIds))
            {
                throw new Exception("未傳入要保書編號");
            }
            if (string.IsNullOrEmpty(importEstimate.EstimateType))
            {
                importEstimate.EstimateType ="";
            }

            string[] Ids = importEstimate.EstimateIds.Split(",");
            if (Ids.Length==0)
            {
                throw new Exception("未傳入要保書編號");
            }
            var rst = await  iService.ImportEstimate(Ids, importEstimate.EstimateType, CurrentUser.UserId).ConfigureAwait(false);
            if (rst.Count != 0)
            {
                result.ResData = rst;
                result.Success = true;
                result.ErrMsg = string.Format("已成功匯入{0}筆要保書",rst.Count);
                result.ErrCode = ErrCode.successCode;

                
            }
            else
            {
                Log4NetHelper.Error("匯入要保書失敗");
                result.ResData = "要保書匯入案件";
                result.ErrCode =ErrCode.err43001;
                result.ErrMsg = "匯入失敗";
                
                
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 取得工作單
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("GetWorkOrders")]
        [AurocoreAuthorize("List")]
        
        public virtual async Task<CommonResult<PageResult<WorkOrderOutputDto>>> GetWorkOrders(SeachWorkOrder search)
        {
            CommonResult<PageResult<WorkOrderOutputDto>> result = new CommonResult<PageResult<WorkOrderOutputDto>>();
             
            string formWhere = string.Format("Id='{0}' ", search.Filter.FormTypeId); 
            var formItems = await itemsDetailService.GetWhereAsync(formWhere).ConfigureAwait(false);
            
            User user = userService.Get(CurrentUser.UserId);
            if (!string.IsNullOrEmpty(search.Filter.CaseOpenTime))
            {
                search.StartDateTime = search.Filter.CaseOpenTime;
                if (!string.IsNullOrEmpty(search.Filter.CaseCloseTime))
                {
                    search.EndDateTime = search.Filter.CaseCloseTime;
                }
                else
                {
                    search.EndDateTime = search.Filter.CaseOpenTime;
                }
            }
           
           
            search.CurrentUserId = CurrentUser.UserId;
            search.DepartmentId = user.DepartmentId;
            
            string sortType = string.Empty;
            string staticType = string.Empty;
            string wfstatus = string.Empty;
            int pageIndex = 1;
            int pageSize = 20;
            if (string.IsNullOrEmpty(search.EndDateTime) && !string.IsNullOrEmpty(search.StartDateTime))
            {
                search.EndDateTime = search.StartDateTime;
            }
            if (search.Filter.StatusName==nameof(WorkflowStatus.等待分配))
            {
                staticType = "全部";
            }
            else
            {
                staticType = "個人";
            }
            if (string.IsNullOrEmpty(search.Sort))
            {
                sortType = "Id";
            }
            else
            {
                sortType = search.Sort;
            }
            if (!string.IsNullOrEmpty(search.CurrenetPageIndex.ToString())
                &&(!string.IsNullOrEmpty(search.PageSize.ToString())))
            {
                pageIndex = search.CurrenetPageIndex;
                pageSize = search.PageSize;
            }
            else
            {
                sortType = search.Sort;
            }
            if ( formItems!=null  && formItems.ItemName == nameof(WorkOrderType.待辦單據))
            {
                
                if (search.Filter.StatusName == nameof(WorkflowStatus.等待分配))
                {
                    wfstatus = nameof(WorkflowStatus.等待分配);
                }
                else
                {
                    wfstatus = nameof(WorkflowStatus.處理中);
                
                }

                SearchWorkOrderDetail detailsearch = new SearchWorkOrderDetail() {
                    StartDateTime = string.IsNullOrEmpty(search.Filter.CaseOpenTime) ? "": search.StartDateTime,
                    EndDateTime = string.IsNullOrEmpty(search.Filter.CaseCloseTime ) ? "" : search.EndDateTime,
                    Sort = sortType,
                    CurrenetPageIndex = pageIndex,
                    PageSize = pageSize,
                    EnCode =  string.IsNullOrEmpty(search.Filter.OwnerId) ? ((int)SearchType.allcase).ToString() : ((int)SearchType.personalcase).ToString(),

                    Filter =new()
                    {
                       CurrentEditorId= CurrentUser.UserId,
                       StatusName = wfstatus,
                       
                    }
                    
                };

                

                result.ResData = await workOrderDetailService.FindWithPagerSearchAsync(detailsearch,search);
                result.RelationData = staticType;

            }
            else 
            {
                if (search.Filter.StatusName == nameof(WorkflowStatus.處理中) && string.IsNullOrEmpty(search.Filter.OwnerId))
                {
                    search.Filter.OwnerId = CurrentUser.UserId;
                    search.EnCode = ((int)SearchType.allcase).ToString();
                }

                    
                
                result.ResData = await iService.FindWithPagerSearchAsync(search);
                result.RelationData = staticType;
            }
            
            result.ErrCode = ErrCode.successCode;
            return result;
        }
        /// <summary>
        /// 非同步新增資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <returns></returns>
        [HttpPost("Insert")]
        [AurocoreAuthorize("Add")]
        public override async Task<IActionResult> InsertAsync(WorkOrderInputDto tinfo)
        {
            CommonResult result = new CommonResult();
            User user = userService.Get(tinfo.OwnerId);
            if(user != null)
            {
                tinfo.OwnerName = user.RealName; //加入姓名
                
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
                return ToJsonContent(result);
            }
            WorkOrder info = tinfo.MapTo<WorkOrder>();
            OnBeforeInsert(info);
            var _csa = CheckSetAccount(info);
            if (_csa.IsSuccess == false)
            {
                info.SetAccountStatus = false;
                if (string.IsNullOrEmpty(info.SetAccountStatusDesc))
                {
                    info.SetAccountStatusDesc = _csa.ErroCode;
                }
            }
            long ln = await iService.InsertAsync(info).ConfigureAwait(true);
            result.Success = ln > 0;
            if (result.Success)
            {
                await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = info.Id;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="jinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
       // [NoPermissionRequired]
        public override async Task<IActionResult> UpdateAsync(WorkOrderInputDto jinfo, string id)
        {
            CommonResult result = new CommonResult();

            WorkOrder info = iService.Get(id);

            Type typeWorkOrder = typeof(WorkOrder);
            if (jinfo.StatusName == nameof(WorkflowStatus.已結案))
            {
                string wodWhere = $" WorkOrderId='{id}' AND (StatusName='{nameof(WorkflowStatus.處理中)}'  OR StatusName='{nameof(WorkflowStatus.協辦中)}' OR StatusName='{nameof(WorkflowStatus.等待分配)}')";
                var wodinfo = workOrderDetailService.GetCountByWhere(wodWhere);
                if (wodinfo > 0)
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = "尚有單據處理中,不可結案";
                    return ToJsonContent(result);
                }
                if(info.StatusName== nameof(WorkflowStatus.等待分配))
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = "案件等待分配,無法直接結案";
                    return ToJsonContent(result);
                }
               // jinfo.CaseCloseTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                
            }
            if(jinfo.StatusName==nameof(WorkflowStatus.處理中) && !string.IsNullOrEmpty(jinfo.OwnerId) )
            {
                var _user = userService.Get(jinfo.OwnerId);
                jinfo.OwnerName = _user.RealName;
            }
            
            if (info.StatusName == nameof(WorkflowStatus.已結案)&& info.SetAccountStatus == true)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "案件已結案,且已歸戶,不可再變動狀態";
                return ToJsonContent(result);
            }
            if (!string.IsNullOrEmpty(info.Supplement))
            {
                var _supplement = JsonHelper.ToObject<Memo>(info.Supplement);
                if(_supplement == null)
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = "補充資料格式錯誤";
                    return ToJsonContent(result); 
                }
            }
            if (info.ParentFormTypeName == nameof(WorkOrderType.Memo))
            {
                string wodWhere1 = $" WorkOrderId='{id}' AND ResponseTypeName='{ResponseType.ReplyMemotoSender}'";
                var wodinfo1= workOrderDetailService.GetCountByWhere(wodWhere1);
                if (wodinfo1 == 0 && info.StatusName==nameof(WorkflowStatus.已結案))
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = "尚未回報 Memo,無法直接結案";
                    return ToJsonContent(result);
                }
            }
            if (info != null)
            {
                
                foreach (PropertyInfo prop in jinfo.GetType().GetProperties())
                {
                    var propVal = prop.GetValue(jinfo, null);
                    string propStr = prop.GetValue(jinfo, null) as string;
                    // Console.WriteLine($"{prop.Name}: {prop.GetValue(jinfo, null)}");
                    if (propVal != null)
                    {

                        PropertyInfo piInstance = typeWorkOrder.GetProperty(prop.Name);
                        if (piInstance.Name == "StatusId" || piInstance.Name == "StatusName"|| piInstance.Name == "ApplicantId" || piInstance.Name == "ApplicantName")
                        {
                            if (!String.IsNullOrEmpty(propStr))
                            {
                                piInstance.SetValue(info, propVal);
                            }
                        }
                        else
                        {
                            piInstance.SetValue(info, propVal);
                        }

                        

                    }
                }
                /*
                info.OwnerId = jinfo.OwnerId;
                info.OwnerOrg = jinfo.OwnerOrg;
                info.CoOwnerId = jinfo.CoOwnerId;
                info.CoOwnerOrg = jinfo.CoOwnerOrg;
                info.StatusId = jinfo.StatusId;
                info.StatusName = jinfo.StatusName;
                info.CaseCategoryId = jinfo.CaseCategoryId;
                info.CaseCategoryName   = jinfo.CaseCategoryName;
                info.ApplicantId = jinfo.ApplicantId;
                info.ApplicantName = jinfo.ApplicantName;
                info.ApplicantCTIPhone = jinfo.ApplicantCTIPhone;
                info.ApplicantLocalPhone = jinfo.ApplicantLocalPhone;
                info.ApplicantMobilePhone = jinfo.ApplicantMobilePhone;
                info.ApplicantAddress = jinfo.ApplicantAddress;
                info.ApplicantEmail = jinfo.ApplicantEmail;
                info.Header = jinfo.Header;
                info.Body = jinfo.Body;
                info.IsHot = jinfo.IsHot;
                info.CaseCloseTime = jinfo.CaseCloseTime;*/
                var _csa =  CheckSetAccount(info);
                if (_csa.IsSuccess == false)
                {
                    info.SetAccountStatus = false;
                    if (string.IsNullOrEmpty(info.SetAccountStatusDesc))
                    {
                        info.SetAccountStatusDesc = _csa.ErroCode;
                    }
                }
                else
                {
                    info.SetAccountStatus = true;
                    if (string.IsNullOrEmpty(info.SetAccountStatusDesc)||info.SetAccountStatusDesc==ErrCode.err70001||
                        info.SetAccountStatusDesc==ErrCode.err70002||info.SetAccountStatusDesc==ErrCode.err70003)
                    {
                        info.SetAccountStatusDesc = DateTime.Now.ToString("yyyy/MM/dd") + "歸戶";
                    }
                }
                OnBeforeUpdate(info);
                if (info.StatusName == nameof(WorkflowStatus.已結案))
                {
                    info.CaseCloseTime = DateTime.Now;
                }
                
                result.Success = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            }
            

            
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
               
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 非同步歸戶
        /// </summary>
        /// <param name="jinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("SetAccountV0")]
        [AurocoreAuthorize("Edit")]
        // [NoPermissionRequired]
        public  async Task<IActionResult> SetAccountAsyncV0(WorkOrder jinfo, string id)
        {
            CommonResult result = new CommonResult();

            WorkOrder info = iService.Get(id);
            /*
            CommonResult result = new CommonResult();

            WorkOrder info = iService.Get(id);
            if(info !=null && !string.IsNullOrEmpty(jinfo.Contact.Name)&& !string.IsNullOrEmpty(jinfo.Contact.ContactPhone))
            {
                string accountinfo = JsonHelper.ToJson(jinfo);
                info.SetAccount = accountinfo;
                int accountStatus = (int)SetAccountStatus.已歸戶;
                info.SetAccountStatus = accountStatus.ToString();
                info.SetAccountStatusDesc= "歸戶日期："+DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                OnBeforeUpdate(info);
                result.Success = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            }*/

            Type typeWorkOrder = typeof(WorkOrder);
            if (jinfo.SetAccountStatus==false || string.IsNullOrEmpty(jinfo.SetAccountStatusDesc))
            {
                
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "狀態漏填,請再輸入後歸戶";
                return ToJsonContent(result);
            }

            if (info != null)
            {
                // Console.WriteLine($"{prop.Name}: {prop.GetValue(jinfo, null)}");
               
            foreach (PropertyInfo prop in jinfo.GetType().GetProperties())
            {
                var propVal = prop.GetValue(jinfo, null);
                string propStr = prop.GetValue(jinfo, null) as string;

                if (propVal != null)
                {

                    PropertyInfo piInstance = typeWorkOrder.GetProperty(prop.Name);
                    if (piInstance.Name == "StatusId" || piInstance.Name == "StatusName" || piInstance.Name == "ApplicantId" || piInstance.Name == "ApplicantName")
                    {
                        if (!String.IsNullOrEmpty(propStr))
                        {
                            piInstance.SetValue(info, propVal);
                        }
                    }
                    else
                    {
                        piInstance.SetValue(info, propVal);
                    }



                }
            }
            
               
                
                OnBeforeUpdate(info);
                result.Success = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            }



            if (result.Success)
            {
                result.ErrCode = ErrCode.setAccountsuccessCode1;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 非同步歸戶
        /// </summary>
        /// <param name="jinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("SetAccountV1")]
        [AurocoreAuthorize("Edit")]
        // [NoPermissionRequired]
        public IActionResult SetAccountAsyncV1(WorkOrderSetAccount jinfo, string id)
        {
            CommonResult result = new CommonResult();
            /*
            WorkOrderAccount workOrderAccount = new WorkOrderAccount();
            WorkOrder info = iService.Get(id);
            string accountinfo = string.Empty;

          
            if (info != null&& workOrderAccount==null)
            {
                if(info.FormTypeName==nameof(WorkOrderType.EzGo) || info.FormTypeName == nameof(WorkOrderType.EFax) || info.FormTypeName == nameof(WorkOrderType.InBound))
                {
                    workOrderAccount.PolicyNo = info.ApplicantPolicyNO;
                    workOrderAccount.Policyholder = new()
                    {
                        Id=info.ApplicantId,
                        Name = info.ApplicantName,
                        ContactPhone = info.ApplicantMobilePhone,
                        Email =info.ApplicantEmail
                    };
                    if (string.IsNullOrEmpty(info.ApplicantMobilePhone))
                    {
                        info.ApplicantMobilePhone = jinfo.ApplicantMobilePhone;
                    }
                    
                }
                else
                {
                    if (string.IsNullOrEmpty(info.ApplicantMobilePhone))
                    {
                        info.ApplicantMobilePhone = jinfo.ApplicantMobilePhone;
                    }
                    if (string.IsNullOrEmpty(jinfo.Relation))
                    {
                        workOrderAccount.PolicyNo = info.ApplicantPolicyNO;

                        workOrderAccount.Policyholder = new()
                        {
                            Id = info.ApplicantId,
                            Name = info.ApplicantName,
                            ContactPhone = info.ApplicantMobilePhone,
                            Email = info.ApplicantEmail
                        };
                    }
                    else
                    {
                        string relationWhere = string.Format("Id='{0}' ", jinfo.Relation); ;
                        var relateItem = await itemsDetailService.GetWhereAsync(relationWhere).ConfigureAwait(false);
                        if(relateItem != null)
                        {
                            // 確認歸戶關係
                            switch (relateItem.ItemName)
                            {
                                case nameof(SetAccountRealation.聯絡人):
                                    workOrderAccount.PolicyNo = info.ApplicantPolicyNO;

                                    workOrderAccount.Contact = new()
                                    {
                                        Id = info.ApplicantId,
                                        Name = info.ApplicantName,
                                        ContactPhone = info.ApplicantMobilePhone,
                                        Email = info.ApplicantEmail,
                                        Relation=jinfo.Relation,
                                        RelatedPerson = jinfo.RelatedPerson
                                    };
                                    break;
                                case nameof(SetAccountRealation.被保人):
                                    workOrderAccount.PolicyNo = info.ApplicantPolicyNO;

                                    workOrderAccount.Insured = new()
                                    {
                                        Id = info.ApplicantId,
                                        Name = info.ApplicantName,
                                        ContactPhone = info.ApplicantMobilePhone,
                                        Email = info.ApplicantEmail
                                    };
                                    break;
                                case nameof(SetAccountRealation.要保人):
                                    workOrderAccount.PolicyNo = info.ApplicantPolicyNO;

                                    workOrderAccount.Policyholder = new()
                                    {
                                        Id = info.ApplicantId,
                                        Name = info.ApplicantName,
                                        ContactPhone = info.ApplicantMobilePhone,
                                        Email = info.ApplicantEmail
                                    };
                                    break;
                                    
                            }
                            //判斷是否有舊歸戶資料
                            
                            if (!string.IsNullOrEmpty(info.SetAccount))
                            {
                                WorkOrderAccount currentSetAccount = JsonHelper.ToObject<WorkOrderAccount>(info.SetAccount);
                                if (workOrderAccount.Policyholder != null)
                                {
                                    currentSetAccount.Policyholder = new();
                                    currentSetAccount.Policyholder = workOrderAccount.Policyholder.ShallowCopy();
                                }
                                if (workOrderAccount.Insured != null)
                                {
                                    currentSetAccount.Insured = new();
                                    currentSetAccount.Insured = workOrderAccount.Insured.ShallowCopy();
                                }
                                if (workOrderAccount.Contact != null)
                                {
                                    currentSetAccount.Contact = new();
                                    currentSetAccount.Contact = workOrderAccount.Contact.ShallowCopy();
                                }

                                accountinfo = JsonHelper.ToJson(currentSetAccount);
                            }

                        }
                        
                    }

                    accountinfo = JsonHelper.ToJson(workOrderAccount);
                    info.SetAccount = accountinfo;
                    int accountStatus = (int)SetAccountStatus.已歸戶;
                    info.SetAccountStatus = accountStatus.ToString();
                    info.SetAccountStatusDesc = "歸戶日期：" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                   
                }
                OnBeforeUpdate(info);
                result.Success = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            }
           


            if (result.Success)
            {
                result.ErrCode = ErrCode.setAccountsuccessCode1;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }*/
            return ToJsonContent(result);
        }
        /// <summary>
        /// 非同步歸戶
        /// </summary>
        /// <param name="jinfo">WorkOrder</param>
        /// <returns></returns>
        [HttpPost("SetAccount")]
        [AurocoreAuthorize("Edit")]
        // [NoPermissionRequired]
        public async Task<IActionResult> SetAccountAsync(WorkOrder jinfo)
        {
            CommonResult result = new CommonResult();
            if (string.IsNullOrEmpty(jinfo.Id))
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = ErrCode.err40007;
                result.ResData = "未提供案件編號";
                return ToJsonContent(result);

            }
            WorkOrder info = iService.Get(jinfo.Id);
            if(info == null)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = ErrCode.err60001;
                result.ResData = "無此案件編號";
                return ToJsonContent(result);
            }
            if (info.StatusName == nameof(WorkflowStatus.已結案) && info.SetAccountStatus == true)
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = "案件已結案,且已歸戶,不可再變動狀態";
                return ToJsonContent(result);
            }
            if (!string.IsNullOrEmpty(info.Supplement))
            {
                var _supplement = JsonHelper.ToObject<Memo>(info.Supplement);
                if (_supplement == null)
                {
                    throw new Exception("補充資料格式錯誤");
                }
            }
            Type typeWorkOrder = typeof(WorkOrder);
            if (info != null)
            {

                foreach (PropertyInfo prop in jinfo.GetType().GetProperties())
                {
                    var propVal = prop.GetValue(jinfo, null);
                    string propStr = prop.GetValue(jinfo, null) as string;
                    // Console.WriteLine($"{prop.Name}: {prop.GetValue(jinfo, null)}");
                    if (propVal != null)
                    {

                        PropertyInfo piInstance = typeWorkOrder.GetProperty(prop.Name);
                        if (piInstance.Name == "StatusId" || piInstance.Name == "StatusName" || piInstance.Name == "ApplicantId" || piInstance.Name == "ApplicantName")
                        {
                            if (!String.IsNullOrEmpty(propStr))
                            {
                                piInstance.SetValue(info, propVal);
                            }
                        }
                        else
                        {
                            piInstance.SetValue(info, propVal);
                        }



                    }
                }
                
                var _csa = CheckSetAccount(info);
                if (_csa.IsSuccess == false)
                {
                    info.SetAccountStatus = false;
                    if (string.IsNullOrEmpty(info.SetAccountStatusDesc))
                    {
                        info.SetAccountStatusDesc = _csa.ErroCode;
                    }
                }
                else
                {
                    info.SetAccountStatus = true;
                    if (string.IsNullOrEmpty(info.SetAccountStatusDesc))
                    {
                        info.SetAccountStatusDesc = DateTime.Now.ToString("yyyy/MM/dd") + "歸戶";
                    }
                }
                OnBeforeUpdate(info);
                result.Success = await iService.UpdateAsync(info, jinfo.Id).ConfigureAwait(false);
            }
            
            
            
            if (result.Success)
            {
                result.ErrCode = ErrCode.setAccountsuccessCode1;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 非同步批量更新工作單據狀態
        /// </summary>
        /// <param name="info"></param>
        [HttpPost("SetOwnerStatusBatchAsync")]
        [AurocoreAuthorize("Enable")]
        public virtual async Task<IActionResult> SetEnabledMarktBatchAsync(UpdateStatusViewModel info)
        {
            CommonResult result = new CommonResult();
            string itemWhere = $"ItemName='{nameof(WorkflowStatus.等待分配)}' ";
            var pendingItem = await itemsDetailService.GetWhereAsync(itemWhere).ConfigureAwait(false);
            if (string.IsNullOrEmpty(info.OwnerID)&& string.IsNullOrEmpty(info.Ishot.ToString()))
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "未傳入案件擁有者編號";
                return ToJsonContent(result);
            }

            var user = userService.Get(info.OwnerID);
            int isHot = Convert.ToInt32(info.Ishot);
            //資料分配碼
            if(info.All ==null && info.Ids.Length == 0 && string.IsNullOrEmpty(info.FormTypeId))
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "未傳入案件分配碼";
                return ToJsonContent(result);
            }
            
            if (info.All == false && info.Ids.Length == 0 && string.IsNullOrEmpty(info.FormTypeId))
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "未傳入案件分配碼";
                
                return ToJsonContent(result);
            }
            if (info.All == true && info.Ids.Length == 0 && string.IsNullOrEmpty(info.FormTypeId))
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "未傳入案件代號";

                return ToJsonContent(result);
            }
            if (info.All == true && info.Ids.Length==0)
            {
                string formWhere = $"Id='{info.FormTypeId}' ";
                var formType = await itemsDetailService.GetWhereAsync(formWhere).ConfigureAwait(false);
                if (formType == null)
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "單據類型編號錯誤";
                    return ToJsonContent(result);
                }
                string caseWhere =$" StatusId='{pendingItem.Id}' AND FormTypeID='{formType.Id}' ";
                var _wos = await iService.GetListWhereAsync(caseWhere);
                
                if (_wos!=null)
                {
                    List<WorkOrder> workOrders = _wos.ToList();
                    if (workOrders.Count == 0)
                    {
                        result.ErrMsg = ErrCode.err43002;
                        result.ErrCode = "沒有案件可分配";
                        return ToJsonContent(result);

                    }
                    else
                    {
                        IEnumerable<object> query = from t in workOrders select t.Id;
                        object[] ids = query.ToArray();
                        info.Ids = ids;
                        if (info.Ids.Length == 0)
                        {
                            result.ErrMsg = ErrCode.err43002;
                            result.ErrCode = "案件編號有誤，請重新嘗試";

                           return ToJsonContent(result);
                        }
                    }
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "沒有案件可分配";
                    return ToJsonContent(result);
                }
            }

            string where = string.Empty;
  
            object parameter =  null;
            where = "id in ('" + info.Ids.Join(",").Replace(",", "','") + "')";
            string setField = string.Empty;
            if (info.Ishot == true)
            {

                 setField = $"Ishot='{isHot}'";
                
            }
            else
            {
                
                 setField = $"StatusId='{info.StatusId}',OwnerId='{info.OwnerID}',OwnerName='{user.RealName}',StatusName='{info.StatusName}',CaseOpenTime=GETDATE(),IsNotified=0";
            }

            dynamic[] jobsId = info.Ids;
            if (!string.IsNullOrEmpty(where))
            {
                bool blresut = await iService.SetEnabledMarkByWhereAsync(setField,where, parameter,CurrentUser.UserId);
                if (blresut)
                {
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = ErrCode.err0;
                }
                else
                {
                    result.ErrMsg = ErrCode.err43002;
                    result.ErrCode = "43002";
                }
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 取得單一案件樹狀概觀資料
        /// </summary>
        /// <param name="Id">案件編號</param>
        /// <returns></returns>
        [HttpPost("WorkOrderTreeBriefData")]
        [AurocoreAuthorize("List")]
        public IActionResult WorkOrderTree(string Id)
        {
            CommonResult result = new CommonResult();
            List<WorkOrderTreeOutputDto> treeOutputDtos = iService.WorkOrderTree(Id);
            if (treeOutputDtos.Count > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = treeOutputDtos;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
           
            return ToJsonContent(result);
        }
        /// <summary>
        /// 取得單一案件概觀資料
        /// </summary>
        /// <param name="Id">案件編號</param>
        /// <returns></returns>
        [HttpPost("WorkOrderBriefbyType")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> WorkOrderTypeTree(string Id)
        {
            CommonResult result = new CommonResult();
            WorkOrderRealateList treeOutputDtos = await iService.WorkOrderTypeTree(Id);
            if (treeOutputDtos!= null)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = treeOutputDtos;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }

            return ToJsonContent(result);
        }
        /// <summary>
        /// 取得案件列表表頭
        /// </summary>
        /// <returns></returns>
        [HttpPost("WorkOrderHeader")]
        [AurocoreAuthorize("List")]
        public IActionResult WorkOrderHeader()
        {
            CommonResult result = new CommonResult();
            WorkOrderListTableSchema rst = iService.GetworkOrderListTableHeader();
            if (rst != null)
            {


                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
                result.ResData = rst;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "err60001";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 匯入 Excel 外撥名單
        /// </summary>
        /// <param name="_outBound"></param>
        /// <param name="sheetIndex">頁簽編號（由 0 開始，預設為 0）</param>
        /// <param name="startRowNumber">起始行數（預設 0 為表頭）</param>
        /// <returns></returns>
        [HttpPost("ImportExcelOutBoundNameList")]
        [AurocoreAuthorize("Add")]
        public async Task<IActionResult> ImportOutBoundNameList(OutBound _outBound, int sheetIndex = 0, int startRowNumber = 0)
        {
            CommonResult result = new CommonResult();
            string localpath = _hostingEnvironment.WebRootPath;
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            /*
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            var _tempfilepath = sysSetting.Filepath;*/
            if (!String.IsNullOrEmpty(_outBound.ProjectId))
            {

                var formItems =  itemsDetailService.Get(_outBound.ProjectId);
                if (formItems != null)
                {
                    _outBound.WorkOrderTypeId = formItems.Id;
                    _outBound.WorkOrderTypeName = formItems.ItemName;
                    int _hours = formItems.ItemProperty1.ToInt();
                    _outBound.CaseCloseTime = DateTime.Now.AddHours(_hours);

                }
                else
                {
                    result.ErrCode = ErrCode.err43001;
                    result.ErrMsg = "請提供案件類別Id ";
                    return ToJsonContent(result);
                }
                
            }
            if (!string.IsNullOrEmpty(_outBound.ProjectName)&&(String.IsNullOrEmpty(_outBound.ProjectId)))
            {
                string formWhere = $"ItemName='{_outBound.ProjectName}'";
                var formItems = await itemsDetailService.GetWhereAsync(formWhere).ConfigureAwait(false);
                if(formItems != null)
                {
                    _outBound.WorkOrderTypeId = formItems.Id;
                    _outBound.WorkOrderTypeName = formItems.ItemName;
                    int _hours = formItems.ItemProperty1.ToInt();
                    _outBound.CaseCloseTime = DateTime.Now.AddHours(_hours);
                }
                else
                {
                    result.ErrMsg = ErrCode.err43001;
                    result.ErrCode = "請提供案件類別Id";
                    return ToJsonContent(result);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(_outBound.WorkOrderTypeId))
                {
                    result.ErrMsg = ErrCode.err43001;
                    result.ErrCode = "請提供案件類別Id";
                    return ToJsonContent(result);
                }
            }
            if (string.IsNullOrEmpty(_outBound.FileName))
            {
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "請提供上傳名單路徑";
                return ToJsonContent(result);
            }
            string[] _filename = _outBound.FileName.Split("/");
            string[] paths = { localpath, _filename[0], _filename[1], _filename[2] };
            string fullPath = Path.Combine(paths);
            var nameListDT = NPOIHelper.ImportxlsxExcel(fullPath, sheetIndex, startRowNumber);
            List<OutBoundDbNameList> outBoundDbNameLists = new List<OutBoundDbNameList>();
            if (nameListDT.Rows.Count>0)
            {
                _outBound.Id = GuidUtils.CreateShortNo();
                _outBound.CreatorTime = DateTime.Now;
                _outBound.CreatorUserId = CurrentUser.UserId;
                _outBound.DeleteMark = false;
                _outBound.EnabledMark = false;
                _outBound.TotalRecord = nameListDT.Rows.Count;
                _outBound.TransportMark = false;
                var obsAdd = await outBoundService.InsertAsync(_outBound).ConfigureAwait(false);
                if (obsAdd > 0)
                {
                    foreach (DataRow dr in nameListDT.Rows)
                    {
                        OutBoundUploadNameList outBoundName = new();
                        OutBoundDbNameList dbNameList = new();
                        outBoundName.PolicyNo = dr.Field<string>("要保序號/保險單號");
                        outBoundName.PolicyholderType = dr.Field<string>("保戶類別");
                        outBoundName.ProposerName = dr.Field<string>("要保人姓名");
                        outBoundName.ProposerId = dr.Field<string>("要保人ID");
                        outBoundName.ProposerPhone = dr.Field<string>("要保人手機號碼");
                        outBoundName.InsuredName = dr.Field<string>("被保人姓名");
                        outBoundName.InsuredId = dr.Field<string>("被保人ID");
                        outBoundName.Itag = dr.Field<string>("車號/牌照號碼");
                        outBoundName.SubjectMatterInsuredAddr = dr.Field<string>("標的物地址");
                        outBoundName.PolicyStartDate = dr.Field<string>("保單起始日");
                        outBoundName.PolicyEndDate = dr.Field<string>("保單到期日");
                        outBoundName.InsuranceTypeId = dr.Field<string>("險種代號");
                        outBoundName.InsuranceTypeName = dr.Field<string>("險種名稱");
                        outBoundName.InsuredAmount = dr.Field<string>("總保額");
                        outBoundName.RefundAmount = dr.Field<string>("退費金額");
                        outBoundName.RenewCheck = dr.Field<string>("續保比對");
                        outBoundName.InqueryStatus = dr.Field<string>("查詢狀況");
                        dbNameList.Id = GuidUtils.CreateShortNo();
                        dbNameList.OutBoundId = _outBound.Id;
                        dbNameList.ApplicantId = outBoundName.ProposerId;
                        dbNameList.ApplicantPolicyNo = outBoundName.PolicyNo;
                        dbNameList.ApplicantName = outBoundName.ProposerName;
                        dbNameList.ApplicantMobilePhone = outBoundName.ProposerPhone;
                        dbNameList.OwnerId = outBoundName.AgentinCharge;
                        dbNameList.Supplement = Regex.Replace(JsonHelper.ToJson(outBoundName), @"\s", "");
                        dbNameList.CreatorUserId= CurrentUser.UserId;
                        dbNameList.CreatorTime=DateTime.Now;
                        outBoundDbNameLists.Add(dbNameList);

                    }
                    int _rstCount = await outBoundNameListService.AddNameLists(outBoundDbNameLists);
                    _outBound.ImportedRecord = _rstCount;
                    _outBound.TotalRecord = nameListDT.Rows.Count;
                    _outBound.EnabledMark = true;
                    if (_rstCount > 0)
                    {
                        var obsUpdate = await outBoundService.UpdateAsync(_outBound, _outBound.Id);

                        if (obsUpdate)
                        {
                            result.ErrCode = ErrCode.successCode;
                            result.ErrMsg = ErrCode.err0;
                            result.ResData = $"新增{_rstCount} 筆";
                        }
                        else
                        {
                            result.ErrMsg = ErrCode.err43001;
                            result.ErrCode = "43001";
                        }
                    }
                    
                }

            }

            return ToJsonContent(result);
        }
        /// <summary>
        /// 上傳列表
        /// </summary>
        /// <param name="search">搜尋參數</param>
        /// <returns></returns>
        [HttpPost("ListUploadedOutBounds")]
        [AurocoreAuthorize("List")]
        public async Task<CommonResult<PageResult<OutBoundOutputDto>>> ListUploadedOutBounds(SearchInputDto<OutBound> search)
        {
            CommonResult<PageResult<OutBoundOutputDto>> result = new CommonResult<PageResult<OutBoundOutputDto>>();
            result.ResData = await outBoundService.FindWithPagerAsync(search);
            result.ErrCode = ErrCode.successCode;
            return result;
        }
        /// <summary>
        /// 修改外撥名單專案類別
        /// </summary>
        /// <param name="_outBound">電訪名單資料</param>
        /// <returns></returns>
        [HttpPost("UpdateUploadedOBList")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> UpdateUploadedOutBound(OutBound _outBound)
        {
            CommonResult result = new CommonResult();
            var outBound = outBoundService.Get(_outBound.Id);
            if (outBound != null)
            {
                if (!string.IsNullOrEmpty(_outBound.WorkOrderTypeId))
                {
                    outBound.WorkOrderTypeId = _outBound.WorkOrderTypeId;
                    outBound.WorkOrderTypeName = _outBound.WorkOrderTypeName;
                    outBound.CaseCloseTime = _outBound.CaseCloseTime;

                }
                if (!string.IsNullOrEmpty(_outBound.ProjectName))
                {
                    outBound.ProjectName = _outBound.ProjectName;
                }
                outBound.LastModifyUserId = CurrentUser.UserId;
                outBound.LastModifyTime = DateTime.Now;
                var rst = await outBoundService.UpdateAsync(outBound,outBound.Id);
                if (rst)
                {
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = $"電訪名單:{outBound.Id}" + "變更成功";
                }
                else
                {
                    result.ErrCode = ErrCode.err43002;
                    result.ErrMsg = $"電訪名單:{outBound.Id}" + "變更失敗";
                }
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = $"Id:{_outBound.Id}" + ErrCode.err40007;
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 刪除列表資料
        /// </summary>
        /// <param name="Id">電訪名單編號</param>
        /// <returns></returns>
        [HttpPost("DelUploadedOBList")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> DeleteUploadedOutBound(string Id)
        {
            
            CommonResult result = new CommonResult();
            OutBound outBound = outBoundService.Get(Id);

            if (outBound != null)
            {
                if (outBound.TransportMark == false)
                {
                    outBoundService.Delete(Id);
                    string where = $"OutBoundId='{Id}'";
                    var ob = await outBoundNameListService.DeleteBatchWhereAsync(where);
                    if (ob)
                    {
                        result.ErrCode = ErrCode.successCode;
                        result.ErrMsg = $"電訪名單:{Id}" + "物理刪除成功";
                    }
                }
                else
                {
                    var bs = await outBoundService.DeleteSoftAsync(false, Id, CurrentUser.UserId);
                    string where = $"OutBoundId='{Id}'";
                    if (bs)
                    {
                        var ob = await outBoundNameListService.DeleteSoftBatchAsync(false, where, CurrentUser.UserId);
                        if (ob)
                        {
                            result.ErrCode = ErrCode.successCode;
                            result.ErrMsg = $"電訪名單:{Id}" + "物理刪除成功";
                        }
                    }


                }
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = $"Id:{Id}" + ErrCode.err60001;

            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 轉為正式案件資料
        /// </summary>
        /// <param name="Id">電訪名單編號</param>
        /// <returns></returns>
        [HttpPost("OutBoundstoWorkOrders")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> OutBoundstoWorkOrders(string Id)
        {
            CommonResult result = new CommonResult();
            OutBound outBound = outBoundService.Get(Id);
            
            if (outBound != null)
            {
                if (outBound.TransportMark == false)
                {
                    var _imported = await iService.OutBoundListtoWorkOrder(outBound.Id, CurrentUser.UserId,outBound.WorkOrderTypeId,outBound.WorkOrderTypeName,(DateTime)outBound.CaseCloseTime);
                    if (_imported)
                    {
                        await hubContext.Clients.All.SendAsync("SendMessage", "GetUserNotification");
                        await hubContext.Clients.All.SendAsync("SendMessage", "GetWorkOrders");
                        result.ErrCode = ErrCode.successCode;
                        result.ErrMsg = $"電訪名單:{Id} 已匯入案件系統";
                    }
                }
                else
                {
                    result.ErrCode = ErrCode.err1;
                    result.ErrMsg = $"電訪名單:{Id} 已匯入案件系統,無法再匯入" ;
                }
            }
            else
            {
                result.ErrCode = ErrCode.err1;
                result.ErrMsg = $"電訪名單:{Id}" + ErrCode.err60001;
            }

            return ToJsonContent(result);
        }
        /// <summary>
        /// 外撥成效
        /// </summary>
        /// <returns></returns>
        [HttpPost("CaculateOutBounds")]
        [AurocoreAuthorize("List")]
        public  IActionResult CaculateOutBounds()
        {
            CommonResult result = new CommonResult();
            OutBoundStatistics outBoundStatistics = new OutBoundStatistics();
            Random random = new Random();
            outBoundStatistics.Indicator1= random.Next(2000, 3000).ToString();
            outBoundStatistics.Indicator2= random.Next(1000, 1500).ToString();
            outBoundStatistics.Indicator3= random.Next(1, 99).ToString();
            outBoundStatistics.Indicator4= random.Next(500,999).ToString();
            outBoundStatistics.Indicator5= random.Next(2000, 3000).ToString();
            outBoundStatistics.Indicator6= random.Next(1, 99).ToString();
            outBoundStatistics.Indicator7= random.Next(1, 99).ToString();
            outBoundStatistics.Indicator8= random.Next(500, 999).ToString();
            result.ErrCode = ErrCode.successCode;
            result.ErrMsg = ErrCode.err0;
            result.ResData = outBoundStatistics;
            var jsonPath = Path.Combine(_hostingEnvironment.ContentRootPath, "jsonconfig", "outboundcontact.json"); 
            //var jsonPath =  "/jsonconfig/outboundcontact.json";
            var jsonString = FileHelper.ReadFile(jsonPath);
            var OutBoundContactColumns = JsonConvert.DeserializeObject<List<OutBoundContactColumn>>(jsonString);
            result.RelationData = OutBoundContactColumns;
            return ToJsonContent(result);
        }
        /// <summary>
        /// 檢查歸戶狀態
        /// </summary>
        /// <param name="workOrder"></param>
        /// <returns></returns>
        private static AccountStatus CheckSetAccount(WorkOrder workOrder)
        {
            AccountStatus accountStatus = new();
            accountStatus.IsSuccess = false;
            if(string.IsNullOrEmpty(workOrder.ApplicantPolicyNO))
            {
                accountStatus.ErroCode = ErrCode.err70003;
                

            }
            if (string.IsNullOrEmpty(workOrder.SetAccountRelation))
            {
                accountStatus.ErroCode = ErrCode.err70001;
               

            }
            if (string.IsNullOrEmpty(workOrder.ApplicantName))
            {
                accountStatus.ErroCode = ErrCode.err70003;
                

            }
            if (string.IsNullOrEmpty(workOrder.ApplicantId))
            {
                accountStatus.ErroCode = ErrCode.err70003;
                

            }
            accountStatus.IsSuccess = true;
            accountStatus.ErroCode = "";
            return accountStatus;
        }
        private class AccountStatus
        {
            public bool? IsSuccess { get; set; }
            public string ErroCode { get; set; }
        }

        /// <summary>
        /// 非同步批量讀取工作單據狀態
        /// </summary>
        /// <param name="id"></param>
        
        [HttpGet("GetById")]
        [AurocoreAuthorize("List")]
        public override  async Task<CommonResult<WorkOrderOutputDto>> GetById(string id)
        {
            CommonResult<WorkOrderOutputDto> result = new CommonResult<WorkOrderOutputDto>();
            WorkOrder _wo = await iService.GetAsync(id);
            var woType = await itemsDetailService.GetWhereAsync("Id='"+_wo.FormTypeId+"'");
           
            if (_wo != null)
            {
                SearchInputDto<Policy> psearch = new();
                psearch.Filter = new();
                psearch.Filter.CpolicyNo = _wo.ApplicantPolicyNO;

                var _policy = await policyService.FindCorrespondPerson(psearch);
                if (_policy !=null)
                {
                    WorkOrderRelationData workOrderRelationData = new WorkOrderRelationData { 
                     Name=_policy.Insuredname,
                     Email =_policy.Insuredemail,
                     Phone =_policy.Insuredcontactmobile
                    };
                    result.RelationData = workOrderRelationData;
                    
                }
                WorkOrderOutputDto workOrderOutputDto = new();
                DateTime dt = (DateTime)_wo.CaseOpenTime;
                DateTime cdt = (DateTime)_wo.CreatorTime;
                workOrderOutputDto = _wo.MapTo<WorkOrderOutputDto>();
                workOrderOutputDto.Timeless = dt.AddHours(woType.ItemProperty1.ToInt());
                workOrderOutputDto.CaseCloseTime = dt.AddHours(woType.ItemProperty1.ToInt());
                workOrderOutputDto.CreatorTime = ((DateTime)_wo.CreatorTime).ToString("yyyy-MM-dd HH:mm");
                if(workOrderOutputDto.StatusName==nameof(WorkflowStatus.已結案) || workOrderOutputDto.OwnerId != CurrentUser.UserId)
                {
                    workOrderOutputDto.IsBlockDisplay = false;
                }
                if (!string.IsNullOrEmpty(workOrderOutputDto.ApplicantEmail))
                {
                    if(workOrderOutputDto.ParentFormTypeName != nameof(WorkOrderType.EzGo))
                    {
                        workOrderOutputDto.ApplicantEmail = _policy.Insuredemail;
                    }
                    
                   
                }
                result.ResData = workOrderOutputDto;
                result.Success = true;
                result.ErrCode = ErrCode.successCode;
            }
            else
            {
                result.ErrMsg = ErrCode.err60001;
                result.ErrCode = "60001";
            }
            return result;
        }
        /// <summary>
        /// 案件編號或 要
        /// </summary>
        /// <param name="workOrder"></param>
        /// <returns></returns>
        [HttpPost("DeleteFile")]
        [AurocoreAuthorize("Edit")]
        public async Task<IActionResult> DeleteFile(WorkOrder  workOrder)
        {
           
            CommonResult result = new CommonResult();
            bool _bol;
            if (string.IsNullOrEmpty(workOrder.Id) )
            {
                result.ErrMsg = "未填入案件編號";
                result.ErrCode = ErrCode.failCode;
                return ToJsonContent(result);
            }
            if (string.IsNullOrEmpty(workOrder.Attachment) )
            {
                result.ErrMsg = "未填入附件編號";
                result.ErrCode = ErrCode.failCode;
                return ToJsonContent(result);
            }
            var _wo = iService.Get(workOrder.Id);
            if (_wo == null)
            {
                result.ErrMsg = "無此案件編號";
                result.ErrCode = ErrCode.failCode;
                return ToJsonContent(result);
            }
            else
            {
                string _attAry = _wo.Attachment;
                string _newVal = string.Empty;
                foreach(string item in _attAry.Split(","))
                {
                    var _files = item.Split("/");
                    int _filesLength = _files.Length;
                    var _fileId = _files[_filesLength - 1].Split("[")[0];
                    if(_fileId != workOrder.Attachment)
                    {
                        _newVal = string.Join(",", item);
                    }
                    


                }
                string _where = $"Id='{workOrder.Id}'";
                _bol = await iService.UpdateTableFieldAsync("Attachment", _newVal, _where);
                if (_bol)
                {
                    
                    result.Success = true;
                    result.ErrCode = ErrCode.successCode;
                    result.ErrMsg = "刪除成功";
                    await hubContext.Clients.All.SendAsync("SendMessage", "GetEstimatewithRealtions");
                }
                else
                {
                    result.Success = false;
                    result.ErrCode = ErrCode.err43002;
                    result.ErrMsg = "刪除失敗";
                }
                
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 測試廣播
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost("TestBroadcast")]
        [AllowAnonymous]
        public async Task<string> TestBroadcast(string name)
        {
            string rtnStr = string.Empty;
            await hubContext.Clients.All.SendAsync("SendMessage", "TestBroadcast:"+DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            return rtnStr = "OK";
        }

    }
}