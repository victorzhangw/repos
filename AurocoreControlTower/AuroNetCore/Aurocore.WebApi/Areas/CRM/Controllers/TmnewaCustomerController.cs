using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.Customers.Dtos;
using Aurocore.Customers.Models;
using Aurocore.Customers.IServices;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Dtos;
using Aurocore.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Aurocore.WorkOrders.Services;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Dtos;
using Aurocore.WorkOrders.Models;
namespace Aurocore.CustomerApi.Areas.Customers.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class TmnewaCustomerController : AreaApiController<Customer, CustomerOutputDto,CustomerInputDto,ICustomerService,string>
    {
        private readonly IPolicyService  policyService;
        private readonly ICrossAreaService crossAreaService;
        private readonly IWorkOrderService workOrderService;
        private readonly ICustomer_ActivityService customer_ActivityService;
        private readonly ICustomer_ContactService customer_ContactService;
        private readonly ICustomer_SourceService customer_SourceService;
        private readonly ICustomer_DetailService customer_DetailService;
        private readonly ICustomer_SystemService customer_SystemService;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_policyService"></param>
        /// <param name="_crossAreaService"></param>
        /// <param name="_workOrderService"></param>
        /// <param name="_customer_ActivityService"></param>
        /// <param name="_customer_ContactService"></param>
        /// <param name="_customer_DetailService"></param>
        /// <param name="_customer_SourceService"></param>
        /// <param name="_customer_SystemService"></param>
        public TmnewaCustomerController(ICustomerService _iService,IPolicyService _policyService,
            ICrossAreaService _crossAreaService, IWorkOrderService _workOrderService, 
            ICustomer_ActivityService _customer_ActivityService, 
            ICustomer_ContactService _customer_ContactService, 
            ICustomer_SourceService _customer_SourceService, 
            ICustomer_DetailService _customer_DetailService, 
            ICustomer_SystemService _customer_SystemService) : base(_iService)
        {
            iService = _iService;
            policyService = _policyService;
            crossAreaService = _crossAreaService;
            workOrderService = _workOrderService;
            customer_ActivityService = _customer_ActivityService;
            customer_ContactService = _customer_ContactService;
            customer_SourceService = _customer_SourceService;
            customer_DetailService = _customer_DetailService;
            customer_SystemService = _customer_SystemService;
            AuthorizeKey.ListKey = "Customer/List";
            AuthorizeKey.InsertKey = "Customer/Add";
            AuthorizeKey.UpdateKey = "Customer/Edit";
            AuthorizeKey.UpdateEnableKey = "Customer/Enable";
            AuthorizeKey.DeleteKey = "Customer/Delete";
            AuthorizeKey.DeleteSoftKey = "Customer/DeleteSoft";
            AuthorizeKey.ViewKey = "Customer/View";
            
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Customer info)
        {
            info.Id = info.CustomerID;
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
           
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Customer info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Customer info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        #region 客戶主檔處理
        /// <summary>
        /// 新增客戶主檔
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("InsertCustomer")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public  async Task<IActionResult> InsertCustomer(CustomerInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer customer = new Customer(); 
            customer = info.MapTo<Customer>();
            var _result = iService.Get(info.CustomerID);
            if (_result !=null)
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = $"資料庫已有此客戶 Id:{info.CustomerID}";
                return ToJsonContent(result);
            }
            OnBeforeInsert(customer);
            long ln = await iService.InsertAsync(customer);
            

            if (ln>0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg= ErrCode.err0;
              
            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode =ErrCode.err43001;
            }


            return ToJsonContent(result);
            
        }
        /// <summary>
        /// 修改客戶
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("UpdateCustomer")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateCustomer(CustomerInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer customer = new Customer();
            customer = info.MapTo<Customer>();
            var _result = iService.Get(info.CustomerID);
            if (_result == null)
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"資料庫沒有此客戶 Id:{info.CustomerID}";
                return ToJsonContent(result);
            }
            OnBeforeUpdate(customer);
            bool bl = await iService.UpdateAsync(customer,info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 修改客戶
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("DeleteCustomer")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> DeleteCustomer(CustomerInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer customer = new Customer();
            customer = info.MapTo<Customer>();
            var _result = iService.Get(info.CustomerID);
            if (_result == null)
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"資料庫沒有此客戶 Id:{info.CustomerID}";
                return ToJsonContent(result);
            }
            OnBeforeUpdate(customer);
            OnBeforeSoftDelete(customer);
            bool bl = await iService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        #endregion
        #region 客戶參與活動
        /// <summary>
        /// 新增客戶活動
        /// </summary>
        /// <param name="info">客戶活動</param>
        /// <returns></returns>
        [HttpPost("InsertCustomerActivity")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> InsertCustomerActivity(Customer_ActivityInputDto info)
        {
            
            CommonResult result = new CommonResult();

            Customer_Activity customer = new Customer_Activity();
            customer = info.MapTo<Customer_Activity>();
            if (string.IsNullOrEmpty(info.Id) || string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = $"未提供活動識別碼";
                return ToJsonContent(result);
            }

            
            customer.CreatorTime = DateTime.Now;
            customer.CreatorUserId = CurrentUser.UserId;
            customer.DeleteMark = false;
            long ln = await customer_ActivityService.InsertAsync(customer);


            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43001;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 修改客戶活動
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("UpdateCustomerActivity")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateCustomerActivity(Customer_ActivityInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Activity customer = new Customer_Activity();
            customer = info.MapTo<Customer_Activity>();
            if (string.IsNullOrEmpty(info.Id) || string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"未提供活動 / 客戶識別碼";
                return ToJsonContent(result);
            }

            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            bool bl = await customer_ActivityService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 刪除客戶活動
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("DeleteCustomerActivity")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> DeleteCustomerActivity(CustomerInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Activity customer = new Customer_Activity();
            customer = info.MapTo<Customer_Activity>();
           
            if (string.IsNullOrEmpty(info.Id) || string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"未提供活動 / 客戶識別碼";
                return ToJsonContent(result);
            }
            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            customer.DeleteMark = true;
            customer.DeleteTime = DateTime.Now;
            customer.DeleteUserId = CurrentUser.UserId;
            bool bl = await customer_ActivityService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        #endregion
        #region 客戶聯絡人
        /// <summary>
        /// 新增客戶聯絡人
        /// </summary>
        /// <param name="info">客戶聯絡人</param>
        /// <returns></returns>
        [HttpPost("InsertCustomerContact")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> InsertCustomerContact(Customer_ContactInputDto info)
        {

            CommonResult result = new CommonResult();

            Customer_Contact customer = new Customer_Contact();
            customer = info.MapTo<Customer_Contact>();
            if (string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = $"未提供客戶識別碼";
                return ToJsonContent(result);
            }


            customer.CreatorTime = DateTime.Now;
            customer.CreatorUserId = CurrentUser.UserId;
            customer.DeleteMark = false;
            long ln = await customer_ContactService.InsertAsync(customer);


            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43001;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 修改客戶聯絡人
        /// </summary>
        /// <param name="info">客戶聯絡人</param>
        /// <returns></returns>
        [HttpPost("UpdateCustomerContact")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateCustomerContact(Customer_ContactInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Contact customer = new Customer_Contact();
            customer = info.MapTo<Customer_Contact>();
            if ( string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"客戶識別碼";
                return ToJsonContent(result);
            }

            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            bool bl = await customer_ContactService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 刪除客戶聯絡人
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("DeleteCustomerContact")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> DeleteCustomerContact(Customer_ContactInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Contact customer = new Customer_Contact();
            customer = info.MapTo<Customer_Contact>();

            if (string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"未提供客戶識別碼";
                return ToJsonContent(result);
            }
            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            customer.DeleteMark = true;
            customer.DeleteTime = DateTime.Now;
            customer.DeleteUserId = CurrentUser.UserId;
            bool bl = await customer_ContactService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        #endregion
        #region 客戶子表
        /// <summary>
        /// 新增客戶子表
        /// </summary>
        /// <param name="info">客戶子表</param>
        /// <returns></returns>
        [HttpPost("InsertCustomerDeatail")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> InsertCustomerDeatail(Customer_DetailInputDto info)
        {

            CommonResult result = new CommonResult();

            Customer_Detail customer = new Customer_Detail();
            customer = info.MapTo<Customer_Detail>();
            if (string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43001;
                result.ErrMsg = $"未提供客戶識別碼";
                return ToJsonContent(result);
            }


            customer.CreatorTime = DateTime.Now;
            customer.CreatorUserId = CurrentUser.UserId;
            customer.DeleteMark = false;
            long ln = await customer_DetailService.InsertAsync(customer);


            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43001;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 修改客戶子表
        /// </summary>
        /// <param name="info">客戶子表</param>
        /// <returns></returns>
        [HttpPost("UpdateCustomerDeatail")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateCustomerDeatail(Customer_DetailInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Detail customer = new Customer_Detail();
            customer = info.MapTo<Customer_Detail>();
            if (string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"客戶識別碼";
                return ToJsonContent(result);
            }

            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            bool bl = await customer_DetailService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 刪除客戶子表
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("DeleteCustomerDeatail")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> DeleteCustomerDeatail(Customer_DetailInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Detail customer = new Customer_Detail();
            customer = info.MapTo<Customer_Detail>();

            if (string.IsNullOrEmpty(info.CustomerID))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"未提供客戶識別碼";
                return ToJsonContent(result);
            }
            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            customer.DeleteMark = true;
            customer.DeleteTime = DateTime.Now;
            customer.DeleteUserId = CurrentUser.UserId;
            bool bl = await customer_DetailService.UpdateAsync(customer, info.CustomerID).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        #endregion
        #region 資料來源
        /// <summary>
        /// 新增資料來源
        /// </summary>
        /// <param name="info">資料來源</param>
        /// <returns></returns>
        [HttpPost("InsertSouce")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> InsertSouce(Customer_SourceInputDto info)
        {

            CommonResult result = new CommonResult();

            Customer_Source customer = new Customer_Source();
            customer = info.MapTo<Customer_Source>();
            if (string.IsNullOrEmpty(info.SourceID.ToString()))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"缺少來源識別碼";
                return ToJsonContent(result);
            }

            customer.Id = info.SourceID.ToString();
            customer.CreatorTime = DateTime.Now;
            customer.CreatorUserId = CurrentUser.UserId;
            customer.DeleteMark = false;
            long ln = await customer_SourceService.InsertAsync(customer);


            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43001;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 修改資料來源
        /// </summary>
        /// <param name="info">資料來源</param>
        /// <returns></returns>
        [HttpPost("UpdateSource")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateSource(Customer_SourceInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Source customer = new Customer_Source();
            customer = info.MapTo<Customer_Source>();
            if (string.IsNullOrEmpty(info.SourceID.ToString()))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"缺少來源識別碼";
                return ToJsonContent(result);
            }

            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            bool bl = await customer_SourceService.UpdateAsync(customer, info.Id).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 刪除資料來源
        /// </summary>
        /// <param name="info">客戶實體</param>
        /// <returns></returns>
        [HttpPost("DeleteSource")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> DeleteSource(Customer_SourceInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_Source customer = new Customer_Source();
            customer = info.MapTo<Customer_Source>();

            if (string.IsNullOrEmpty(info.SourceID.ToString()))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"未提供來源識別碼";
                return ToJsonContent(result);
            }
            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            customer.DeleteMark = true;
            customer.DeleteTime = DateTime.Now;
            customer.DeleteUserId = CurrentUser.UserId;
            bool bl = await customer_SourceService.UpdateAsync(customer, info.Id).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        #endregion
        #region 系統平台
        /// <summary>
        /// 新增系統平台
        /// </summary>
        /// <param name="info">系統平台</param>
        /// <returns></returns>
        [HttpPost("InsertSystem")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> InsertSystem(Customer_SystemInputDto info)
        {

            CommonResult result = new CommonResult();

            Customer_System customer = new Customer_System();
            customer = info.MapTo<Customer_System>();
            if (string.IsNullOrEmpty(info.SystemID.ToString()))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"缺少系統識別碼";
                return ToJsonContent(result);
            }

            customer.Id = info.SystemID.ToString();
            customer.CreatorTime = DateTime.Now;
            customer.CreatorUserId = CurrentUser.UserId;
            customer.DeleteMark = false;
            long ln = await customer_SystemService.InsertAsync(customer);


            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43001;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 修改系統平台
        /// </summary>
        /// <param name="info">系統平台</param>
        /// <returns></returns>
        [HttpPost("UpdateSystem")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateSystem(Customer_SystemInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_System customer = new Customer_System();
            customer = info.MapTo<Customer_System>();
            if (string.IsNullOrEmpty(info.SystemID.ToString()))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"缺少系統識別碼";
                return ToJsonContent(result);
            }

            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            bool bl = await customer_SystemService.UpdateAsync(customer, info.Id).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        /// <summary>
        /// 刪除系統平台
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost("DeleteSystem")]
        [NoPermissionRequired]
        [AllowAnonymous]
        //[AurocoreAuthorize("List")]
        public async Task<IActionResult> DeleteSystem(Customer_SystemInputDto info)
        {
            CommonResult result = new CommonResult();
            Customer_System customer = new Customer_System();
            customer = info.MapTo<Customer_System>();

            if (string.IsNullOrEmpty(info.SystemID.ToString()))
            {
                result.ErrCode = ErrCode.err43002;
                result.ErrMsg = $"缺少系統識別碼";
                return ToJsonContent(result);
            }
            customer.LastModifyUserId = CurrentUser.UserId;
            customer.LastModifyTime = DateTime.Now;
            customer.DeleteMark = true;
            customer.DeleteTime = DateTime.Now;
            customer.DeleteUserId = CurrentUser.UserId;
            bool bl = await customer_SystemService.UpdateAsync(customer, info.Id).ConfigureAwait(false); ;


            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;

            }
            else
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = ErrCode.err43002;
            }


            return ToJsonContent(result);

        }
        #endregion
    }
}