using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.CMS.IServices;
using Aurocore.AspNetCore.Mvc;

namespace Aurocore.WebApi.Areas.CMS.Controllers
{
    /// <summary>
    /// 文章，通知公告介面
    /// </summary>
    [ApiController]
    [Route("api/CMS/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ArticlenewsController : AreaApiController<Articlenews, ArticlenewsOutputDto,ArticlenewsInputDto,IArticlenewsService,string>
    {
        private IArticlecategoryService articlecategoryService;
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_articlecategoryService"></param>
        public ArticlenewsController(IArticlenewsService _iService,IArticlecategoryService _articlecategoryService) : base(_iService)
        {
            iService = _iService;
            articlecategoryService = _articlecategoryService;
            AuthorizeKey.ListKey = "Articlenews/List";
            AuthorizeKey.InsertKey = "Articlenews/Add";
            AuthorizeKey.UpdateKey = "Articlenews/Edit";
            AuthorizeKey.UpdateEnableKey = "Articlenews/Enable";
            AuthorizeKey.DeleteKey = "Articlenews/Delete";
            AuthorizeKey.DeleteSoftKey = "Articlenews/DeleteSoft";
            AuthorizeKey.ViewKey = "Articlenews/View";
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Articlenews info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CategoryName = articlecategoryService.Get(info.CategoryId).Title;
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.CompanyId = CurrentUser.OrganizeId;
            info.DeptId = CurrentUser.DeptId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Articlenews info)
        {
            info.CategoryName = articlecategoryService.Get(info.CategoryId).Title;
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Articlenews info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 非同步更新資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <param name="id">主鍵Id</param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurocoreAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(ArticlenewsInputDto tinfo, string id)
        {
            CommonResult result = new CommonResult();

            Articlenews info = iService.Get(id);
            info.CategoryId = tinfo.CategoryId;
            info.Title = tinfo.Title;
            info.EnabledMark = tinfo.EnabledMark;
            info.SortCode = tinfo.SortCode;
            info.Description = tinfo.Description;
            info.SubTitle = tinfo.SubTitle;
            info.LinkUrl = tinfo.LinkUrl;
            info.IsHot = tinfo.IsHot;
            info.IsNew = tinfo.IsNew;
            info.IsRed = tinfo.IsRed;
            info.IsTop = tinfo.IsTop;

            OnBeforeUpdate(info);
            bool bl = await iService.UpdateAsync(info, id).ConfigureAwait(false);
            if (bl)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43002;
                result.ErrCode = "43002";
            }
            return ToJsonContent(result);
        }
    }
}