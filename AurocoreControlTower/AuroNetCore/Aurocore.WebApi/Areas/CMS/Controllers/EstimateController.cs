using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.CMS.IServices;
using Aurocore.WebApi.Hubs;
using Microsoft.AspNetCore.SignalR;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Dtos;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Dynamic;
using System.Text.Json.Nodes;
using Newtonsoft.Json.Converters;
using Aurocore.Customers.Models;
using NPOI.OpenXmlFormats.Spreadsheet;
using Aurocore.Security.IServices;
using Aurocore.Security.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Aurocore.Commons.Extensions;

namespace Aurocore.WebApi.Areas.CMS.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/CMS/[controller]")]
    public class EstimateController : AreaApiController<Vehicle_Estimate, EstimateOutputDto, EstimateInputDto,
        IEstimateService, string>
    {
        private readonly IEstimate_Ins_TypeService estimate_Ins_TypeService;
        private readonly IEstimate_HccService hccService;
        private readonly IHubContext<BroadcastHub> hubContext;
        private readonly IUserDefinedService userDefinedService;
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        /// <param name="_estimate_Ins_TypeService"></param>
        /// <param name="_HccService"></param>
        /// <param name="_hubContext"></param>
        public EstimateController(IEstimateService _iService,
            IEstimate_Ins_TypeService _estimate_Ins_TypeService,
            IEstimate_HccService _HccService, IHubContext<BroadcastHub> _hubContext,IUserDefinedService _userDefinedService) : base(_iService)
        {
            iService = _iService;
            estimate_Ins_TypeService = _estimate_Ins_TypeService;
            hccService = _HccService;
            hubContext = _hubContext;
            userDefinedService = _userDefinedService;
            AuthorizeKey.ListKey = "Estimate/List";
        }
        /// <summary>
        /// 依據 FilterStatus 加入條件
        /// </summary>
        /// <param name="search"></param>
        /// <param name="Value"></param>
        /// <param name="Key"></param>
        public static void AddFilterType(SearchInputDto<Vehicle_Estimate> search, string Value, string Key)
        {
            //強制不搜出已轉案件的要保書 
            search.Filter.IsCheck = "0";
            switch (Key)
            {
                case FilterStatus.TypeStatus:
                    foreach (var skey in FilterStatus.Status.Keys)
                    {
                        if (skey == Value)
                        {

                            search.Filter.Status = FilterStatus.Status[skey];
                            break;
                        }
                    }

                    break;
                case FilterStatus.TypeDealStatus:
                    foreach (var skey in FilterStatus.DealStatus.Keys)
                    {
                        if (skey == Value)
                        {

                            search.Filter.DealStatus = FilterStatus.DealStatus[skey];
                            break;
                        }
                    }

                    break;
                case FilterStatus.TypePayStatus:
                    foreach (var skey in FilterStatus.PayStatus.Keys)
                    {
                        if (skey == Value)
                        {

                            search.Filter.PayStatus = FilterStatus.PayStatus[skey];
                            break;
                        }
                    }

                    break;
            }
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Vehicle_Estimate info)
        {
            info.Id = GuidUtils.CreateNo();

        }

        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Vehicle_Estimate info)
        {

        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Vehicle_Estimate info)
        {

        }
        /// <summary>
        /// 獲取要保書列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpPost("GetEstimateListbyKeywords")]
        [AurocoreAuthorize("List")]
        public virtual async Task<CommonResult<PageResult<EstimateOutputDto>>> GetEstimateList(SearchInputDto<Vehicle_Estimate> search)
        {
            // 
            CommonResult<PageResult<EstimateOutputDto>> result = new CommonResult<PageResult<EstimateOutputDto>>();
            result.ResData = iService.GetVehicleEstimates(search);
            return result;
        }
        /// <summary>
        /// 透過篩選器取得要保書列表
        /// </summary>
        /// <param name="type">險種</param>
        /// <param name="filters">篩選器 Json </param>
        /// <returns></returns>
        [HttpPost("GetEstimateListbyFilter")]
        [AurocoreAuthorize("List")]
        public async  Task<PageResult<EstimateOutputDto>> GetEstimateListbyFilter([FromQuery]string type, [FromBody] dynamic filters)
        {
            
            CommonResult result = new CommonResult();
            
             var converter = new ExpandoObjectConverter();
            var ItemList = JsonConvert.DeserializeObject<ExpandoObject>(filters.ToString(), converter) as dynamic;
            
            // OR parse the json string
            //JsonNode data = JsonNode.Parse(query);
            SearchInputDto<Vehicle_Estimate> search =new SearchInputDto<Vehicle_Estimate>();
            search.Filter = new();
            foreach (var item in ItemList)
            {
                /* IDictionary<string, object> propertyValues = item;

                 foreach (var property in propertyValues.Keys)
                 {
                     Console.WriteLine(String.Format("{0} : {1}", property, propertyValues[property]));

                 }*/

                string Value =(item.Value.ToString()) as string;
                string Key =(item.Key.ToString()) as string;
                if(Key=="Id" && !string.IsNullOrEmpty(Value))
                {
                    var _uds = await userDefinedService.GetAsync(Value);

                    if (_uds != null)
                    {
                        var filterAry = _uds.CustomSetting.Split(",");
                        for (int i = 0; i < filterAry.Length; i++)
                        {
                            var _filter = filterAry[i].Split("|");
                            if (!string.IsNullOrEmpty(_filter[1]) && !string.IsNullOrEmpty(_filter[0]))
                            {
                                // AddFilterType 強制不搜出已轉案件的要保書 
                                AddFilterType(search, _filter[1], _filter[0]);
                            }
                        }

                    }
                    else
                    {
                        PageResult<EstimateOutputDto> pageResult = new PageResult<EstimateOutputDto>

                        {
                            CurrentPage = 1,
                            Items = new List<EstimateOutputDto>(),
                            ItemsPerPage = 1,

                            TotalItems = 0

                        };
                        pageResult.Success = true;
                        pageResult.ErrCode = ErrCode.successCode;
                        pageResult.RelationData = "訂單號碼,保單號碼,保單起始日,保單結束日,要保人Id,要保人姓名,被保人Id,被保人姓名,客戶備註,管理者備註,車牌號碼";
                        return pageResult;
                    }
                    

                }
                else
                {
                    if (!string.IsNullOrEmpty(Value) && Value.ToString() == "2206140725510")
                    {

                        search.Filter.IsCheck = "1";
                    }
                    AddFilterType(search, Value, Key);
                }

            }
            
            
            result.Success = true;
            result.ErrCode = ErrCode.successCode;
            result.ResData = iService.GetVehicleEstimates(search);

              return iService.GetVehicleEstimates(search);
        }

        

        /// <summary>
        /// 透過篩選器標籤取得要保書列表
        /// </summary>
        /// <param name="Id">篩選器 Id</param>

        /// <returns></returns>
        [HttpPost("GetEstimateListbyLabel")]
        [AurocoreAuthorize("List")]
        public async Task<PageResult<EstimateOutputDto>> GetEstimateListbyLabel(string Id)
        {

            CommonResult result = new CommonResult();
           
            // OR parse the json string
            //JsonNode data = JsonNode.Parse(query);
            SearchInputDto<Vehicle_Estimate> search = new SearchInputDto<Vehicle_Estimate>();
            result.Success = true;
            result.ErrCode = ErrCode.successCode;
           // result.ResData = iService.GetVehicleEstimates(search);
            
            //result.ResData = data;
            
            
  
            return iService.GetVehicleEstimates(search);
        }
        /// <summary>
        /// 透過客戶 Pid 取得 要保書
        /// </summary>
        /// <param name="PId"></param>
        /// <returns></returns>
        [HttpPost("GetEstimateListbyCustomer")]
        [AurocoreAuthorize("List")]
        public async Task<PageResult<EstimateOutputDto>> GetEstimateListbyCustomer(string PId)
        {

            CommonResult result = new CommonResult();

            // OR parse the json string
            //JsonNode data = JsonNode.Parse(query);
            SearchInputDto<Vehicle_Estimate> search = new SearchInputDto<Vehicle_Estimate>();
            result.Success = true;
            result.ErrCode = ErrCode.successCode;
            // result.ResData = iService.GetVehicleEstimates(search);

            //result.ResData = data;



            return iService.GetVehicleEstimates(search);
        }
        /// <summary>
        /// 獲取車險要保書實體
        /// </summary>
        /// <param name="Id">要保書Id (Estimate Id)</param>
        /// <returns></returns>
        [HttpPost("GetVehicleEstimate")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetVehicleEstimate(string Id)
        {
            CommonResult result = new CommonResult();

            
            result.ResData = iService.GetVehicleEstimate(Id);
            result.ErrCode = ErrCode.successCode;
            return ToJsonContent(result);
            
            

        }
        ///<summary>
        /// 獲取要保書實體與其關聯資料
        /// </summary>
        /// <param name="EstimateId">要保書Id (Estimate Id)</param>
        /// <returns></returns>
        [HttpPost("GetEstimatewithRealtions")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetEstimatewithRealtions(string EstimateId)
        {
            // 取得險種->依據險種 取得 model
            CommonResult result = new CommonResult();

            
            result.ResData = iService.GetVehicleEstimatewithRealtions(EstimateId);
            result.ErrCode = ErrCode.successCode;
           
            return ToJsonContent(result);



        }
        ///<summary>
        /// 獲取要保書實體與其關聯資料
        /// </summary>
        /// <param name="search">搜索條件 </param>
        /// <remarks>Id: search.Filter.Pid</remarks>
        /// <remarks>Phone: search.Filter.PhoneNo</remarks>
        /// <remarks>PlateNo: search.Filter.PlateNo</remarks>
        /// <remarks>EstimateId: search.Filter.EstimateId</remarks>
        /// <returns></returns>
        [HttpPost("GetEstimateswithCustomer")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> GetEstimateswithCustomer(SearchInputDto<CrossAreaSearch> search)
        {
            // 取得險種->依據險種 取得 model
            CommonResult result = new CommonResult();
            if (string.IsNullOrEmpty(search.Filter.Pid) 
                && string.IsNullOrEmpty(search.Filter.PhoneNo) 
                && string.IsNullOrEmpty(search.Filter.PlateNo)
                && string.IsNullOrEmpty(search.Filter.EstimateId))
            {
                result.ErrCode = ErrCode.successCode;
                result.ResData = "未傳入條件";
                return ToJsonContent(result);
            }
            {
                result.ResData = iService.GetEstimatesbyPid(search.Filter.Pid);
            }
            if (!string.IsNullOrEmpty(search.Filter.Pid))
            {
                result.ResData = iService.GetEstimatesbyPid(search.Filter.Pid);
            }
            if (!string.IsNullOrEmpty(search.Filter.PhoneNo))
            {
                result.ResData = iService.GetEstimatesbyPhoneNo(search.Filter.PhoneNo);
            }
            if (!string.IsNullOrEmpty(search.Filter.PlateNo))
            {
                result.ResData = iService.GetEstimatesbyPlateNo(search.Filter.PlateNo);
            }
            if (!string.IsNullOrEmpty(search.Filter.EstimateId))
            {
                result.ResData = iService.GetEstimatesbyEstimateId(search.Filter.EstimateId);
            }
           
            result.ErrCode = ErrCode.successCode;

            return ToJsonContent(result);



        }
        /// <summary>
        /// 取消訂單
        /// </summary>
        /// <param name="estimateInput">要保書Id </param>
        /// <returns></returns>
        [HttpPost("CancelEstimate")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> CancelEstimate(EstimateInputDto estimateInput)
        {
            CommonResult result = new CommonResult();
            if (string.IsNullOrEmpty(estimateInput.EstimateId))
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "未提供要保書 Id";
                return ToJsonContent(result);
            }

            result.Success = true;
            result.ErrCode = ErrCode.err0;
            return ToJsonContent(result);



        }
        /// <summary>
        /// 訂單重新報價
        /// </summary>
        /// <param name="estimateInput">要保書Id </param>
        /// <returns></returns>
        [HttpPost("UpdateEstimateQuotation")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> UpdateEstimateQuotation(EstimateInputDto estimateInput)
        {
            CommonResult result = new CommonResult();
            if (string.IsNullOrEmpty(estimateInput.EstimateId))
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "未提供要保書 Id";
                return ToJsonContent(result);
            }

            result.Success = true;
            result.ErrCode = ErrCode.err0;
            return ToJsonContent(result);



        }
        /// <summary>
        /// 新增管理者備註
        /// </summary>
        /// <param name="estimateInput">要保書  </param>
        /// <returns></returns>
        [HttpPost("AddManagerNote")]
        [AurocoreAuthorize("List")]
        public async Task<IActionResult> AddManagerNote(EstimateInputDto estimateInput)
        {
            CommonResult result = new CommonResult();
            if (string.IsNullOrEmpty(estimateInput.EstimateId))
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "未提供要保書 Id";
                return ToJsonContent(result);
            }
            if (string.IsNullOrEmpty(estimateInput.ManagerNote))
            {
                result.ErrMsg = ErrCode.err1;
                result.ErrCode = "未提供管理者備註";
                return ToJsonContent(result);
            }


            result.Success = true;
            result.ErrCode = ErrCode.err0;
            return ToJsonContent(result);



        }
    }
}