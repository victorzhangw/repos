﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aurocore.WebApi.Hubs
{
    /// <summary>
    /// SignalR 中樞
    /// </summary>
    [EnableCors("AuroSignalrCorsPolicy")]
    public class BroadcastHub : Hub
    {
        /// <summary>
        /// 廣播
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task sendAllMessage(string message)
        {

            await Clients.All.SendAsync(message);
        }

    }
}

