using Autofac;
using AutoMapper;
using log4net;
using log4net.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.Impl;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using Aurocore.AspNetCore.Common;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Attributes;
using Aurocore.Commons.Cache;
using Aurocore.Commons.DbContextCore;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Linq;
using Aurocore.Commons.Log;
using Aurocore.Commons.Module;
using Aurocore.Commons.Options;
using Aurocore.Quartz.Jobs;
using Aurocore.WebApi.Hubs;

namespace Aurocore.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        public static ILoggerRepository loggerRepository { get; set; }
        string targetPath = string.Empty;
        IMvcBuilder mvcBuilder;
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            //初始化log4net
            loggerRepository = LogManager.CreateRepository("NETCoreRepository");
            Log4NetHelper.SetConfig(loggerRepository, "log4net.config");
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();
            services.AddSingleton(Configuration);

            //如果部署在linux系統上，需要加上下面的設定：
            services.Configure<KestrelServerOptions>(options => options.AllowSynchronousIO = true);
            //如果部署在IIS上，需要加上下面的設定：
            services.Configure<IISServerOptions>(options => options.AllowSynchronousIO = true);


            #region Swagger Api文件
            services.AddSwaggerGen(options =>
            {
                string contactName = Configuration.GetSection("SwaggerDoc:ContactName").Value;
                string contactNameEmail = Configuration.GetSection("SwaggerDoc:ContactEmail").Value;
                string contactUrl = Configuration.GetSection("SwaggerDoc:ContactUrl").Value;
                options.SwaggerDoc("v1", new OpenApiInfo
                {

                    Version = Configuration.GetSection("SwaggerDoc:Version").Value,
                    Title = Configuration.GetSection("SwaggerDoc:Title").Value,
                    Description = Configuration.GetSection("SwaggerDoc:Description").Value,
                    Contact = new OpenApiContact { Name = contactName, Email = contactNameEmail, Url = new Uri(contactUrl) },
                    License = new OpenApiLicense { Name = contactName, Url = new Uri(contactUrl) }
                });
                Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.xml").ToList().ForEach(file =>
                {
                    options.IncludeXmlComments(file, true);
                });
                options.DocumentFilter<HiddenApiFilter>(); // 在介面類、方法標記屬性 [HiddenApi]，可以阻止【Swagger文件】產生
                options.OperationFilter<AddResponseHeadersFilter>();
                options.OperationFilter<SecurityRequirementsOperationFilter>();
                options.OperationFilter<SwaggerFileUploadFilter>();
                //給api新增token令牌證書
                options.AddSecurityDefinition("CoreApi", new OpenApiSecurityScheme
                {
                    Description = "JWT授權(資料將在請求頭中進行傳輸) 直接在下框中輸入Bearer {token}（注意兩者之間是一個空格）\"",
                    Name = "Authorization",//jwt預設的參數名稱
                    In = ParameterLocation.Header,//jwt預設存放Authorization資訊的位置(請求頭中)
                    Type = SecuritySchemeType.ApiKey
                });
            });


            // Api設定版本資訊
            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(1, 0);
            });
            #endregion

            #region 全域性設定跨域
            //允許所有跨域，測試用
            //services.AddCors(options => options.AddPolicy("AurocoreCors",
            //   policy => policy.WithOrigins("*").AllowAnyHeader().AllowAnyMethod()));
            // SignalR 跨域 要加入 Credentials，所以要另外加入 policy



            services.AddCors(options =>
            {
                options.AddPolicy("AuroSignalrCorsPolicy",
                    builder => builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .SetIsOriginAllowed(hostName => true));
                options.AddPolicy("aurocoreCors",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod());
            });
            // 跨域設定 正式環境
            //services.AddCors(options => options.AddPolicy("AurocoreCors",
            //    policy => policy.WithOrigins(Configuration.GetSection("AppSetting:AllowOrigins").Value.Split(',', StringSplitOptions.RemoveEmptyEntries)).AllowAnyHeader().AllowAnyMethod()));
            #endregion


            #region MiniProfiler
            services.AddMiniProfiler(options =>
            {
                options.RouteBasePath = "/profiler";
            }).AddEntityFramework();
            #endregion

            #region 控制器
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.WriteIndented = true;
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                //設定時間格式
                options.JsonSerializerOptions.Converters.Add(new DateTimeJsonConverter());
                options.JsonSerializerOptions.Converters.Add(new DateTimeNullableConverter());
                //設定bool獲取格式
                options.JsonSerializerOptions.Converters.Add(new BooleanJsonConverter());
                //設定數字
                options.JsonSerializerOptions.Converters.Add(new IntJsonConverter());
                options.JsonSerializerOptions.PropertyNamingPolicy = new UpperFirstCaseNamingPolicy();
                options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
            });

            mvcBuilder = services.AddMvc(option =>
            {
                option.Filters.Add<AurocoreAuthorizationFilter>();
                option.Filters.Add(new ExceptionHandlingAttribute());
                option.Filters.Add<ActionFilter>();
            }).SetCompatibilityVersion(CompatibilityVersion.Latest).AddRazorRuntimeCompilation();

            services.AddMvcCore()
                .AddAuthorization().AddApiExplorer();
            services.AddSignalR();//使用 SignalR
            #endregion


            return InitIoC(services);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (app != null)
            {
                app.UseStaticHttpContextAccessor();
                IServiceProvider provider = app.ApplicationServices;
                AutoMapperService.UsePack(provider);
                //載入外掛應用
                LoadMoudleApps(env);

                app.UseMiniProfiler();
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }
                else
                {
                    app.UseExceptionHandler("/Home/Error");
                    app.UseHsts();
                }
                app.UseStaticFiles();
                app.UseRouting();
                // app.UseCors("AuroSignalrCorsPolicy");
                //跨域
                //呼叫 UseCors 必須放在 UseRouting之後 ，但在 UseAuthorization之前 
                app.UseMiddleware<CorsMiddleware>();
                app.UseCors("aurocoreCors");
                app.UseAuthentication();
                app.UseAuthorization();



                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapHub<BroadcastHub>("/Hubs"); //SignalR的路由與配對的Hub
                    endpoints.MapControllerRoute("default", "api/{controller=Home}/{action=Index}/{id?}");
                });
                app.UseStatusCodePages();
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {

                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "Aurocore System API V1");
                    options.RoutePrefix = string.Empty;//這裡主要是不需要再輸入swagger這個預設字首
                });

                AurocoreInitialization.Initial();

            }
        }



        /// <summary>
        /// IoC初始化
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private IServiceProvider InitIoC(IServiceCollection services)
        {
            #region 快取

            services.AddMemoryCache();// 啟用MemoryCache
            CacheProvider cacheProvider = new CacheProvider
            {
                IsUseRedis = Configuration.GetSection("CacheProvider:UseRedis").Value.ToBool(false),
                ConnectionString = Configuration.GetSection("CacheProvider:Redis_ConnectionString").Value,
                InstanceName = Configuration.GetSection("CacheProvider:Redis_InstanceName").Value
            };
            //判斷是否使用Redis，如果不使用 Redis就預設使用 MemoryCache
            if (cacheProvider.IsUseRedis)
            {
                //Use Redis
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = cacheProvider.ConnectionString;
                    options.InstanceName = cacheProvider.InstanceName;
                });
                services.AddSingleton(typeof(ICacheService), new RedisCacheService(new RedisCacheOptions
                {
                    Configuration = cacheProvider.ConnectionString,
                    InstanceName = cacheProvider.InstanceName
                }, 0));
                services.Configure<DistributedCacheEntryOptions>(option => option.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5));//設定Redis快取有效時間為5分鐘。
            }
            else
            {
                //Use MemoryCache
                services.AddSingleton<IMemoryCache>(factory =>
                {
                    var cache = new MemoryCache(new MemoryCacheOptions());
                    return cache;
                });
                services.AddSingleton<ICacheService, MemoryCacheService>();
                services.Configure<MemoryCacheEntryOptions>(
                    options => options.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)); //設定MemoryCache快取有效時間為5分鐘
            }
            #endregion


            #region 身份認證授權

            var jwtConfig = Configuration.GetSection("Jwt");
            var jwtOption = new JwtOption
            {
                Issuer = jwtConfig["Issuer"],
                Expiration = Convert.ToInt16(jwtConfig["Expiration"]),
                Secret = jwtConfig["Secret"],
                Audience = jwtConfig["Audience"],
                refreshJwtTime = Convert.ToInt16(jwtConfig["refreshJwtTime"])
            };
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme; ;

            }).AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(jwtOption.Secret)),//秘鑰
                    ValidateIssuer = true,
                    ValidIssuer = jwtOption.Issuer,
                    ValidateAudience = true,
                    ValidAudience = jwtOption.Audience,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(5)
                };
            });
            #endregion
            services.AddTransient<IDbContextCore, SqlServerDbContext>(); //注入EF

            IoCContainer.Register(cacheProvider);//註冊快取設定
            IoCContainer.Register(Configuration);//註冊設定
            IoCContainer.Register(jwtOption);//註冊設定
            var codeGenerateOption = new CodeGenerateOption
            {
                ModelsNamespace = "",
                IRepositoriesNamespace = "",
                RepositoriesNamespace = "",
                IServicsNamespace = "",
                ServicesNamespace = ""
            };
            IoCContainer.Register(codeGenerateOption);//註冊程式碼產生器相關設定資訊
            IoCContainer.Register("Aurocore.Commons");
            IoCContainer.Register("Aurocore.AspNetCore");
            IoCContainer.Register("Aurocore.Security.Core");
            IoCContainer.RegisterNew("Aurocore.Security.Core", "Aurocore.Security");
            // IoCContainer.Register("Aurocore.Messages.Core");
            //  IoCContainer.RegisterNew("Aurocore.Messages.Core", "Aurocore.Messages");
            IoCContainer.Register("Aurocore.CMS.Core");
            IoCContainer.RegisterNew("Aurocore.CMS.Core", "Aurocore.CMS");
            IoCContainer.Register("AurocoreBulletinBoardCore");
            IoCContainer.RegisterNew("Aurocore.BulletinBoard.Core", "Aurocore.BulletinBoard");
            IoCContainer.Register("Aurocore.WorkOrders.Core");
            IoCContainer.RegisterNew("Aurocore.WorkOrders.Core", "Aurocore.WorkOrders");
            IoCContainer.Register("Aurocore.WebAnalytics");
            IoCContainer.RegisterNew("Aurocore.WebAnalytics.Core", "Aurocore.WebAnalytics.GoogleAnalytics");
            IoCContainer.Register("Aurocore.Customers.Core");
            IoCContainer.RegisterNew("Aurocore.Customers.Core", "Aurocore.Customers");

            #region automapper
            List<Assembly> myAssembly = new List<Assembly>();
            myAssembly.Add(Assembly.Load("Aurocore.Security.Core"));
            myAssembly.Add(Assembly.Load("Aurocore.WebAnalytics.Core"));
            myAssembly.Add(Assembly.Load("Aurocore.BulletinBoard.Core"));
            myAssembly.Add(Assembly.Load("Aurocore.WorkOrders.Core"));
            myAssembly.Add(Assembly.Load("Aurocore.Customers.Core"));
            myAssembly.Add(Assembly.Load("Aurocore.CMS.Core"));

            services.AddAutoMapper(myAssembly);
            services.AddScoped<IMapper, Mapper>();

            #endregion

            #region 定時任務
            services.AddTransient<HttpResultfulJob>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            //設定定時啟動的任務
            services.AddHostedService<QuartzService>();
            #endregion

            return IoCContainer.Build(services);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            #region AutoFac IOC容器
            try
            {
                #region SingleInstance
                //無介面注入單例

                //有介面注入單例
                #endregion

                #region Aop
                var interceptorServiceTypes = new List<Type>();
                builder.RegisterType<UnitOfWorkIInterceptor>();
                interceptorServiceTypes.Add(typeof(UnitOfWorkIInterceptor));
                #endregion

                #region Repository
                #endregion

                #region Service
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n" + ex.InnerException);
            }
            #endregion
        }
        /// <summary>
        /// 載入模組應用
        /// </summary>
        /// <param name="env"></param>
        private void LoadMoudleApps(IWebHostEnvironment env)
        {
            // 定位到外掛應用目錄 Apps
            var apps = env.ContentRootFileProvider.GetFileInfo("Apps");
            if (!Directory.Exists(apps.PhysicalPath))
            {
                return;
            }

            // 把 Apps 下的動態庫拷貝一份來執行，
            // 使 Apps 中的動態庫不會在執行時被佔用（以便重新編譯）
            var shadows = targetPath = PrepareShadowCopies();
            // 從 Shadow Copy 目錄載入 Assembly 並註冊到 Mvc 中
            LoadFromShadowCopies(shadows);

            string PrepareShadowCopies()
            {
                // 準備 Shadow Copy 的目標目錄
                var target = Path.Combine(env.ContentRootPath, "app_data", "apps-cache");
                Directory.CreateDirectory(target);

                // 找到外掛目錄下 bin 目錄中的 .dll，拷貝
                Directory.EnumerateDirectories(apps.PhysicalPath)
                    .Select(path => Path.Combine(path, "bin"))
                    .Where(Directory.Exists)
                    .SelectMany(bin => Directory.EnumerateFiles(bin, "*.dll"))
                    .ForEach(dll => File.Copy(dll, Path.Combine(target, Path.GetFileName(dll)), true));

                return target;
            }

            void LoadFromShadowCopies(string targetPath)
            {
                foreach (string dll in Directory.GetFiles(targetPath, "*.dll"))
                {
                    try
                    {
                        //解決外掛還引用其他主程式沒有引用的第三方dll問題System.IO.FileNotFoundException
                        AssemblyLoadContext.Default.LoadFromAssemblyPath(dll);
                    }
                    catch (Exception ex)
                    {
                        //非.net程式集型別的dll關聯load時會報錯，這裡忽略就可以
                        Log4NetHelper.Error(ex.Message);
                    }
                }
                // 從 Shadow Copy 目錄載入 Assembly 並註冊到 Mvc 中
                var groups = Directory.EnumerateFiles(targetPath, "Aurocore.*App.Api.dll").Select(AssemblyLoadContext.Default.LoadFromAssemblyPath);

                // 直接載入到為 ApplicationPart
                groups.ForEach(mvcBuilder.AddApplicationPart);

            }
        }
    }
}