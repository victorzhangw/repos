using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Models;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

namespace Aurocore.WebApi.Controllers
{
    /// <summary>
    /// 功能模組介面
    /// </summary>
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class FunctionController: AreaApiController<Menu, MenuOutputDto, MenuInputDto, IMenuService, string>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_iService"></param>
        public FunctionController(IMenuService _iService) : base(_iService)
        {
            iService = _iService;
        }

        /// <summary>
        /// 根據父級功能編碼查詢所有子集功能，主要用於頁面操作按鈕許可權
        /// </summary>
        /// <param name="enCode">選單功能編碼</param>
        /// <returns></returns>
        [HttpGet("GetListByParentEnCode")]
        [AurocoreAuthorize("")]
        public IActionResult GetListByParentEnCode(string enCode)
        {
            CommonResult result = new CommonResult();
            try
            {
                if (CurrentUser != null)
                {
                    AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                    List<MenuOutputDto> functions = new List<MenuOutputDto>();
                    functions = AurocoreCacheHelper.Get("User_Function_" + CurrentUser.UserId).ToJson().ToObject<List<MenuOutputDto>>();
                    MenuOutputDto functionOutputDto = functions.Find(s => s.EnCode == enCode);
                    List<MenuOutputDto> nowFunList = new List<MenuOutputDto>();
                    if (functionOutputDto != null)
                    {
                        nowFunList = functions.FindAll(s => s.ParentId == functionOutputDto.Id && s.IsShow && s.MenuType.Equals("F")).OrderBy(s=>s.SortCode).ToList();
                    }
                    result.ErrCode = ErrCode.successCode;
                    result.ResData = nowFunList;
                }
                else
                {
                    result.ErrCode = "40008";
                    result.ErrMsg = ErrCode.err40008;
                }
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("根據父級功能編碼查詢所有子集功能，主要用於頁面操作按鈕許可權,程式碼產生異常", ex);
                result.ErrCode = ErrCode.failCode;
                result.ErrMsg = "獲取模組功能異常";
            }
            return ToJsonContent(result);
        }
    }
}