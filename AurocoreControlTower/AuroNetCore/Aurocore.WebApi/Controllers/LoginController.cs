using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Common;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.AspNetCore.Mvc.Filter;
using Aurocore.Commons.Cache;
using Aurocore.Commons.IoC;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Commons.Net;
using Aurocore.Commons.Options;
using Aurocore.Security.Application;
using Aurocore.Security.Dtos;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;
using Aurocore.Commons.Log;

namespace Aurocore.WebApi.Controllers
{
    /// <summary>
    /// 使用者登錄介面控制器
    /// </summary>
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class LoginController : ApiController
    {
        private IUserService _userService;
        private IUserLogOnService _userLogOnService;
        private ISystemTypeService _systemTypeService;
        private IAPPService _appService;
        private IRoleService _roleService;
        private IRoleDataService _roleDataService;
        private ILogService _logService;
        private IFilterIPService _filterIPService;
        private IMenuService _menuService;
        private IUserDefinedService _userDefined;

        /// <summary>
        /// 建構函式注入服務
        /// </summary>
        /// <param name="iService"></param>
        /// <param name="userLogOnService"></param>
        /// <param name="systemTypeService"></param>
        /// <param name="logService"></param>
        /// <param name="appService"></param>
        /// <param name="roleService"></param>
        /// <param name="filterIPService"></param>
        /// <param name="roleDataService"></param>
        /// <param name="menuService"></param>
        /// <param name="userDefined"></param>
        public LoginController(IUserService iService, IUserLogOnService userLogOnService, ISystemTypeService systemTypeService,ILogService logService, IAPPService appService, IRoleService roleService, IFilterIPService filterIPService, IRoleDataService roleDataService, IMenuService menuService , IUserDefinedService userDefined)
        {
            _userService = iService;
            _userLogOnService = userLogOnService;
            _systemTypeService = systemTypeService;
            _logService = logService;
            _appService = appService;
            _roleService = roleService;
            _filterIPService = filterIPService;
            _roleDataService = roleDataService;
            _menuService = menuService;
            _userDefined = userDefined;

        }
        /// <summary>
        /// 使用者登錄，必須要有驗證碼
        /// </summary>
        /// <param name="username">使用者名稱</param>
        /// <param name="password">密碼</param>
        /// <param name="vcode">驗證碼</param>
        /// <param name="vkey">驗證碼key</param>
        /// <param name="appId">AppId</param>
        /// <param name="systemCode">系統編碼</param>
        /// <returns>返回使用者User物件</returns>
        [HttpGet("GetCheckUser")]
        [NoPermissionRequired]
        [AllowAnonymous]
        public async Task<IActionResult> GetCheckUser(string username, string password, string vkey, string appId,string systemCode, string vcode = "disabled")
        {

            CommonResult result = new CommonResult();
            RemoteIpParser remoteIpParser = new RemoteIpParser();
            string strIp = remoteIpParser.GetClientIp(HttpContext).MapToIPv4().ToString();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            if (vcode !="disabled")
            {
                var vCode = AurocoreCacheHelper.Get("ValidateCode" + vkey);
                string code = vCode != null ? vCode.ToString() : "11";
                if (vcode.ToUpper() != code)
                {
                    result.ErrMsg = "驗證碼錯誤";
                    return ToJsonContent(result);
                }
            }
           
            Log logEntity = new Log();
            bool blIp=_filterIPService.ValidateIP(strIp);
            if (blIp)
            {
                result.ErrMsg = strIp+"該IP已被管理員禁止登錄！";
            }
            else
            {

                if (string.IsNullOrEmpty(username))
                {
                    result.ErrMsg = "使用者名稱不能為空！";
                }
                else if (string.IsNullOrEmpty(password))
                {
                    result.ErrMsg = "密碼不能為空！";
                }
                if (string.IsNullOrEmpty(systemCode))
                {

                    result.ErrMsg = ErrCode.err40009;
                }
                else
                {
                    string strHost = Request.Host.ToString();
                    APP app = _appService.GetAPP(appId);
                    if (app == null)
                    {
                        result.ErrCode = "40001";
                        result.ErrMsg = ErrCode.err40001;
                    }
                    else
                    {
                        if (!app.RequestUrl.Contains(strHost, StringComparison.Ordinal) && !strHost.Contains("localhost", StringComparison.Ordinal) && !strHost.Contains("devzoneapi2", StringComparison.Ordinal))
                        {
                            result.ErrCode = "40002";
                            result.ErrMsg = ErrCode.err40002 + "，你目前請求主機：" + strHost;
                           
                        }
                        else
                        {
                            SystemType systemType = _systemTypeService.GetByCode(systemCode);
                            if (systemType == null)
                            {
                                result.ErrMsg = ErrCode.err40009;
                            }
                            else
                            {
                                Tuple<User, string> userLogin = await this._userService.Validate(username, password);
                                if (userLogin != null)
                                {
                                    
                                    if (userLogin.Item1 != null)
                                    {
                                        result.Success = true;

                                        User user = userLogin.Item1;

                                        JwtOption jwtModel = IoCContainer.Resolve<JwtOption>();
                                        TokenProvider tokenProvider = new TokenProvider(jwtModel);
                                        TokenResult tokenResult = tokenProvider.LoginToken(user, appId);
                                        AurocoreCurrentUser currentSession = new AurocoreCurrentUser
                                        {
                                            UserId = user.Id,
                                            Account = user.Account,
                                            Name = user.RealName,
                                            NickName = user.NickName,
                                            AccessToken = tokenResult.AccessToken,
                                            AppKey = appId,
                                            CreateTime = DateTime.Now,
                                            HeadIcon = user.HeadIcon,
                                            Gender = user.Gender,
                                            ReferralUserId = user.ReferralUserId,
                                            MemberGradeId = user.MemberGradeId,
                                            Role = _roleService.GetRoleEnCode(user.RoleId),
                                            MobilePhone = user.MobilePhone,
                                            OrganizeId = user.OrganizeId,
                                            DeptId = user.DepartmentId,
                                            CurrentLoginIP = strIp,
                                            TenantId = ""
                                        };
                                        currentSession.ActiveSystem = systemType.FullName;
                                        currentSession.ActiveSystemUrl = systemType.Url;
                                        List<MenuOutputDto> listFunction = new List<MenuOutputDto>();
                                        MenuApp menuApp = new MenuApp();
                                        if (Permission.IsAdmin(currentSession))
                                        {
                                            currentSession.SubSystemList = _systemTypeService.GetAllByIsNotDeleteAndEnabledMark().MapTo<SystemTypeOutputDto>();
                                            //取得使用者可使用的授權功能資訊，並存儲在快取中
                                            listFunction = menuApp.GetFunctionsBySystem(systemType.Id);
                                        }
                                        else
                                        {
                                            currentSession.SubSystemList = _systemTypeService.GetSubSystemList(user.RoleId);
                                            //取得使用者可使用的授權功能資訊，並存儲在快取中
                                            listFunction = menuApp.GetFunctionsByUser(user.Id, systemType.Id);
                                        }

                                        currentSession.MenusRouter = menuApp.GetVueRouter(user.RoleId, systemCode);
                                        TimeSpan expiresSliding = DateTime.Now.AddMinutes(120) - DateTime.Now;
                                        AurocoreCacheHelper.Add("User_Function_" + user.Id, listFunction,expiresSliding, true);
                                        currentSession.Modules = listFunction;
                                        AurocoreCacheHelper.Add("login_user_" + user.Id, currentSession, expiresSliding, true);
                                        //該使用者的資料許可權
                                        List<String> roleDateList = _roleDataService.GetListDeptByRole(user.RoleId);
                                        
                                        AurocoreCacheHelper.Add("User_RoleData_" + user.Id, roleDateList, expiresSliding, true);

                                        CurrentUser = currentSession;
                                        result.ResData = currentSession;
                                        result.RelationData = roleDateList;
                                        result.ErrCode = ErrCode.successCode;
                                        result.Success = true;
                                        
                                        logEntity.Account = CurrentUser.Account;
                                        logEntity.NickName = CurrentUser.NickName;
                                        logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                                        logEntity.IPAddress = CurrentUser.CurrentLoginIP;
                                        logEntity.IPAddressName = CurrentUser.IPAddressName;
                                        logEntity.Result = true;
                                        logEntity.ModuleName = "登錄";
                                        logEntity.Description = "登錄成功";
                                        logEntity.Type = "Login";

                                        _logService.Insert(logEntity);
                                    }
                                    else
                                    {
                                        result.ErrCode = ErrCode.failCode;
                                        result.ErrMsg = userLogin.Item2;
                                        logEntity.Account = username;
                                        logEntity.Date = logEntity.CreatorTime = DateTime.Now;
                                        logEntity.IPAddress = strIp;
                                        logEntity.Result = false;
                                        logEntity.ModuleName = "登錄";
                                        logEntity.Type = "Login";
                                        logEntity.Description = "登錄失敗，" + userLogin.Item2;
                                        _logService.Insert(logEntity);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            AurocoreCacheHelper.Remove("LoginValidateCode");
            return ToJsonContent(result,true);
        }


        /// <summary>
        /// 退出登錄
        /// </summary>
        /// <returns></returns>
        [HttpGet("Logout")]
        [AurocoreAuthorize("")]
        public IActionResult Logout()
        {
            CommonResult result = new CommonResult();
            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            AurocoreCacheHelper.Remove("login_user_" + CurrentUser.UserId);
            AurocoreCacheHelper.Remove("User_Function_" + CurrentUser.UserId);
            //AurocoreCacheHelper.Remove("User_Menu_" + CurrentUser.UserId);
            UserLogOn userLogOn = _userLogOnService.GetWhere("UserId='"+ CurrentUser.UserId + "'");
            userLogOn.UserOnLine = false;
            _userLogOnService.Update(userLogOn,userLogOn.Id);
            CurrentUser = null;
            result.Success = true;
            result.ErrCode = ErrCode.successCode;
            result.ErrMsg = "成功退出";
            return ToJsonContent(result);
        }

        /// <summary>
        /// 子系統切換登錄
        /// </summary>
        /// <param name="openmf">憑據</param>
        /// <param name="appId">應用Id</param>
        /// <param name="systemCode">子系統程式碼</param>
        /// <returns>返回使用者User物件</returns>
        [HttpGet("SysConnect")]
        [AllowAnonymous]
        [NoPermissionRequired]
        public IActionResult SysConnect(string openmf, string appId, string systemCode)
        {
            CommonResult result = new CommonResult();
            RemoteIpParser remoteIpParser = new RemoteIpParser();
            string strIp = remoteIpParser.GetClientIp(HttpContext).MapToIPv4().ToString();
            if (string.IsNullOrEmpty(openmf))
            {
                result.ErrMsg = "切換參數錯誤！";
            }

            bool blIp = _filterIPService.ValidateIP(strIp);
            if (blIp)
            {
                result.ErrMsg = strIp + "該IP已被管理員禁止登錄！";
            }
            else
            {
                
                if (string.IsNullOrEmpty(systemCode))
                {
                    result.ErrMsg = ErrCode.err40009;
                }
                else
                {
                    string strHost = Request.Host.ToString();
                    APP app = _appService.GetAPP(appId);
                    if (app == null)
                    {
                        result.ErrCode = "40001";
                        result.ErrMsg = ErrCode.err40001;
                    }
                    else
                    {
                        if (!app.RequestUrl.Contains(strHost, StringComparison.Ordinal) && !strHost.Contains("localhost", StringComparison.Ordinal))
                        {
                            result.ErrCode = "40002";
                            result.ErrMsg = ErrCode.err40002 + "，你目前請求主機：" + strHost;
                        }
                        else
                        {
                            SystemType systemType = _systemTypeService.GetByCode(systemCode);
                            if (systemType == null)
                            {
                                result.ErrMsg = ErrCode.err40009;
                            }
                            else
                            {
                                AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                                object cacheOpenmf = AurocoreCacheHelper.Get("openmf" + openmf);
                                AurocoreCacheHelper.Remove("openmf" + openmf);
                                if (cacheOpenmf == null)
                                {
                                    result.ErrCode = "40007";
                                    result.ErrMsg = ErrCode.err40007;
                                }
                                else
                                {
                                    User user = _userService.Get(cacheOpenmf.ToString());
                                    if (user != null)
                                    {
                                        result.Success = true;
                                        JwtOption jwtModel = IoCContainer.Resolve<JwtOption>();
                                        TokenProvider tokenProvider = new TokenProvider(jwtModel);
                                        TokenResult tokenResult = tokenProvider.LoginToken(user, appId);
                                        AurocoreCurrentUser currentSession = new AurocoreCurrentUser
                                        {
                                            UserId = user.Id,
                                            Account = user.Account,
                                            Name = user.RealName,
                                            NickName = user.NickName,
                                            AccessToken = tokenResult.AccessToken,
                                            AppKey = appId,
                                            CreateTime = DateTime.Now,
                                            HeadIcon = user.HeadIcon,
                                            Gender = user.Gender,
                                            ReferralUserId = user.ReferralUserId,
                                            MemberGradeId = user.MemberGradeId,
                                            Role = _roleService.GetRoleEnCode(user.RoleId),
                                            MobilePhone = user.MobilePhone,
                                            OrganizeId = user.OrganizeId,
                                            DeptId = user.DepartmentId,
                                            CurrentLoginIP = strIp,
                                        
                                            TenantId = ""
                                        };
                                        currentSession.ActiveSystem = systemType.FullName;
                                        currentSession.ActiveSystemUrl = systemType.Url;
                                        List<MenuOutputDto> listFunction = new List<MenuOutputDto>();
                                        MenuApp menuApp = new MenuApp();
                                        if (Permission.IsAdmin(currentSession))
                                        {
                                            currentSession.SubSystemList = _systemTypeService.GetAllByIsNotDeleteAndEnabledMark().MapTo<SystemTypeOutputDto>();
                                            //取得使用者可使用的授權功能資訊，並存儲在快取中
                                            listFunction = menuApp.GetFunctionsBySystem(systemType.Id);
                                        }
                                        else
                                        {
                                            currentSession.SubSystemList = _systemTypeService.GetSubSystemList(user.RoleId);
                                            //取得使用者可使用的授權功能資訊，並存儲在快取中
                                            listFunction = menuApp.GetFunctionsByUser(user.Id, systemType.Id);
                                        }

                                        currentSession.MenusRouter = menuApp.GetVueRouter(user.RoleId, systemCode);
                                        TimeSpan expiresSliding = DateTime.Now.AddMinutes(120) - DateTime.Now;
                                        AurocoreCacheHelper.Add("User_Function_" + user.Id, listFunction, expiresSliding, true);
                                        currentSession.Modules = listFunction;
                                        AurocoreCacheHelper.Add("login_user_" + user.Id, currentSession, expiresSliding, true);
                                        //該使用者的資料許可權
                                        List<String> roleDateList = _roleDataService.GetListDeptByRole(user.RoleId);
                                        AurocoreCacheHelper.Add("User_RoleData_" + user.Id, roleDateList, expiresSliding, true);

                                        CurrentUser = currentSession;
                                        result.ResData = currentSession;
                                        result.ErrCode = ErrCode.successCode;
                                        result.Success = true;
                                    }
                                    else
                                    {
                                        result.ErrCode = ErrCode.failCode;

                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ToJsonContent(result);
        }
    }
}