using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using Aurocore.AspNetCore.Controllers;
using Aurocore.AspNetCore.Models;
using Aurocore.Commons.Cache;
using Aurocore.Commons.Extend;
using Aurocore.Commons.Extensions;
using Aurocore.Commons.Helpers;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Models;
using Aurocore.Security.Application;
using Aurocore.Security.Dtos;
using Aurocore.Security.Models;
using Aurocore.WebApi.Hubs;
using Microsoft.AspNetCore.SignalR;
using Aurocore.WorkOrders.IServices;
using Aurocore.WorkOrders.Models;

namespace Aurocore.WebApi.Controllers
{
    /// <summary>
    /// 檔案上傳
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ApiController
    {

        private string _filePath;
        private string _dbfileId;
        private string _dbFilePath;   //資料庫中的檔案路徑
        private string _dbThumbnail;   //資料庫中的縮圖路徑
        private string _belongApp;//所屬應用
        private string _belongAppId;//所屬應用ID 
        private string _fileName;//檔名稱
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IHubContext<BroadcastHub> _hubContext;
        private readonly IWorkOrderService _workOrderService;
        /// <summary>
        /// 檔案上傳
        /// </summary>
        /// <param name="hostingEnvironment"></param>
        /// <param name="_hubContext"></param>
        /// <param name="_workOrderService"></param>
        public FilesController(IWebHostEnvironment hostingEnvironment, IHubContext<BroadcastHub> hubContext, IWorkOrderService iworkOrderService)
        {
            _hostingEnvironment = hostingEnvironment;
            _hubContext = hubContext;   
            _workOrderService = iworkOrderService;  

        }

        /// <summary>
        ///  單檔案上傳介面
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns>伺服器儲存的檔案資訊</returns>
        [HttpPost("Upload")]
        public IActionResult Upload([FromForm] IFormCollection formCollection)
        {
            CommonResult result = new CommonResult();

            FormFileCollection filelist = (FormFileCollection)formCollection.Files;
            string belongApp = formCollection["belongApp"].ToString();
            string belongAppId = formCollection["belongAppId"].ToString();
            _fileName = filelist[0].FileName;
            try
            {
                result.ResData = Add(filelist[0], belongApp, belongAppId);
                result.ErrCode = ErrCode.successCode;
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.ErrCode = "500";
                result.ErrMsg = ex.Message;
                Log4NetHelper.Error("", ex);
                //throw ex;
            }
            return ToJsonContent(result);
        }
        /// <summary>
        ///  批量上傳檔案介面
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns>伺服器儲存的檔案資訊</returns>
        [HttpPost("Uploads")]
        public IActionResult  Uploads([FromForm] IFormCollection formCollection)
        {
            CommonResult result = new CommonResult();
            FormFileCollection filelist = (FormFileCollection)formCollection.Files;
            List<UploadFileResultOuputDto> outputs = new List<UploadFileResultOuputDto>();
            string belongApp = formCollection["belongApp"].ToString();
            string belongAppId = formCollection["belongAppId"].ToString();
            try
            {
                outputs = Adds(filelist, belongApp, belongAppId);
                result.ResData = outputs;

            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("", ex);
                result.ErrCode = "500";
                result.ErrMsg = ex.Message;
            }
            WorkOrder workOrder =new WorkOrder();
            if (!string.IsNullOrEmpty(belongAppId))
            {
                var _where = $"SourceId='{belongAppId}'";
                 workOrder =  _workOrderService.GetWhere(_where);
                if (workOrder != null)
                {
                    belongApp = "WorkOrder";
                }
            }
            switch (belongApp)
            {
                case "WorkOrder":
                    var _wfilepath = string.Empty;
                    foreach(var output in outputs)
                    {
                        _wfilepath =string.Join(",", output.FilePath);
                        
                    }
                    if (string.IsNullOrEmpty(workOrder.Attachment))
                    {
                        workOrder.Attachment = _wfilepath;
                    }
                    else
                    {
                        workOrder.Attachment = workOrder.Attachment + "," + _wfilepath;
                    }
                    
                    
                    var _bol = _workOrderService.Update(workOrder,workOrder.Id);
                    if (_bol ==false)
                    {
                        Log4NetHelper.Error("更新附加檔案資料失敗：WordOrder："+workOrder.Id);
                        result.ErrCode = "500";
                        result.ErrMsg = "更新附加檔案資料失敗-案件編號：" + workOrder.Id;
                    }
                    break;
            }
            return ToJsonContent(result);
        }
        /// <summary>
        ///  批量上傳檔案介面
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns>伺服器儲存的檔案資訊</returns>
        [HttpPost("EstimateUploads")]
        public IActionResult EstimateUploads([FromForm] IFormCollection formCollection)
        {
            CommonResult result = new CommonResult();
            FormFileCollection filelist = (FormFileCollection)formCollection.Files;
            string estimateId = formCollection["EstimateId"];
            string belongApp = formCollection["belongApp"].ToString();
            string belongAppId = formCollection["belongAppId"].ToString();
            try
            {
                result.ResData = Adds(filelist, belongApp, belongAppId);
            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("", ex);
                result.ErrCode = "500";
                result.ErrMsg = ex.Message;
            }

            return ToJsonContent(result);
        }
        /// <summary>
        /// 刪除檔案
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("DeleteFile")]
        public IActionResult DeleteFile(string id)
        {
            CommonResult result = new CommonResult();
            try
            {
                UploadFile uploadFile = new UploadFileApp().Get(id);

                AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
                SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
                string localpath = _hostingEnvironment.WebRootPath;
                if (uploadFile != null)
                {
                    string filepath = (localpath + "/" + uploadFile.FilePath).ToFilePath();
                    if (System.IO.File.Exists(filepath))
                        System.IO.File.Delete(filepath);
                    string filepathThu = (localpath + "/" + uploadFile.Thumbnail).ToFilePath();
                    if (System.IO.File.Exists(filepathThu))
                        System.IO.File.Delete(filepathThu);

                    result.ErrCode = ErrCode.successCode;
                    result.Success = true;
                }
                else
                {
                    result.ErrCode = ErrCode.failCode;
                    result.Success = false;
                }

            }
            catch (Exception ex)
            {
                Log4NetHelper.Error("", ex);
                result.ErrCode = "500";
                result.ErrMsg = ex.Message;
            }
            return ToJsonContent(result);
        }

        /// <summary>
        /// 批量上傳檔案
        /// </summary>
        /// <param name="files">檔案</param>
        /// <param name="belongApp">所屬應用，如文章article</param>
        /// <param name="belongAppId">所屬應用ID，如文章id</param>
        /// <returns></returns>
        private List<UploadFileResultOuputDto> Adds(IFormFileCollection files, string belongApp, string belongAppId)
        {
            List<UploadFileResultOuputDto> result = new List<UploadFileResultOuputDto>();
            foreach (var file in files)
            {
                if (file != null)
                {
                    result.Add(Add(file, belongApp, belongAppId,file.FileName));
                }
            }
            return result;
        }
        /// <summary>
        /// 單個上傳檔案
        /// </summary>
        /// <param name="file"></param>
        /// <param name="belongApp"></param>
        /// <param name="belongAppId"></param>
        /// <returns></returns>
        private UploadFileResultOuputDto Add(IFormFile file, string belongApp, string belongAppId)
        {
            _belongApp = belongApp;
            _belongAppId = belongAppId;
            if (file != null && file.Length > 0 && file.Length < 10485760)
            {
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    var fileName = string.Empty;
                        fileName = _fileName;
                    
                    var data = binaryReader.ReadBytes((int)file.Length);
                    UploadFile(fileName, data);
                    
                    UploadFile filedb = new UploadFile
                    {
                        Id = _dbfileId,
                        FilePath = _dbFilePath,
                        Thumbnail = _dbThumbnail,
                        FileName = fileName,
                        FileSize = file.Length.ToInt(),
                        FileType = Path.GetExtension(fileName),
                        Extension = Path.GetExtension(fileName),
                        BelongApp = _belongApp,
                        BelongAppId = _belongAppId
                    };
                    new UploadFileApp().Insert(filedb);
                    UploadFileResultOuputDto uploadFileResultOuputDto = filedb.MapTo<UploadFileResultOuputDto>();
                    uploadFileResultOuputDto.PhysicsFilePath = (_hostingEnvironment.WebRootPath + "/"+ _dbThumbnail).ToFilePath(); ;
                    return uploadFileResultOuputDto;
                }
            }
            else
            {
                Log4NetHelper.Info("檔案過大");
                throw new Exception("檔案過大");
            }
        }
        private UploadFileResultOuputDto Add(IFormFile file, string belongApp, string belongAppId,string _fileName)
        {
            _belongApp = belongApp;
            _belongAppId = belongAppId;
            if (file != null && file.Length > 0 && file.Length < 10485760)
            {
                using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                {
                    var fileName = string.Empty;
                    fileName = _fileName;

                    var data = binaryReader.ReadBytes((int)file.Length);
                    UploadFile(fileName, data);
                    ;
                    UploadFile filedb = new UploadFile
                    {
                        Id = _dbfileId,
                        FilePath = _dbFilePath,
                        Thumbnail = _dbThumbnail,
                        FileName = fileName,
                        FileSize = file.Length.ToInt(),
                        FileType = Path.GetExtension(fileName),
                        Extension = Path.GetExtension(fileName),
                        BelongApp = _belongApp,
                        BelongAppId = _belongAppId
                    };
                    new UploadFileApp().Insert(filedb);
                    UploadFileResultOuputDto uploadFileResultOuputDto = filedb.MapTo<UploadFileResultOuputDto>();
                    uploadFileResultOuputDto.PhysicsFilePath = (_hostingEnvironment.WebRootPath + "/" + _dbThumbnail).ToFilePath(); ;
                    return uploadFileResultOuputDto;
                }
            }
            else
            {
                Log4NetHelper.Info("檔案過大");
                throw new Exception("檔案過大");
            }
        }
        /// <summary>
        /// 實現檔案上傳到伺服器儲存，並產生縮圖
        /// </summary>
        /// <param name="fileName">檔名稱</param>
        /// <param name="fileBuffers">檔案位元組流</param>
        private void UploadFile(string fileName, byte[] fileBuffers)
        {

             
            //判斷檔案是否為空
            if (string.IsNullOrEmpty(fileName))
            {
                Log4NetHelper.Info("檔名不能為空");
                throw new Exception("檔名不能為空");
            }

            //判斷檔案是否為空
            if (fileBuffers.Length < 1)
            {
                Log4NetHelper.Info("檔案不能為空");
                throw new Exception("檔案不能為空");
            }

            AurocoreCacheHelper AurocoreCacheHelper = new AurocoreCacheHelper();
            SysSetting sysSetting = AurocoreCacheHelper.Get("SysSetting").ToJson().ToObject<SysSetting>();
            string folder = DateTime.Now.ToString("yyyyMMdd");
            _filePath = _hostingEnvironment.WebRootPath;
            var _tempfilepath = sysSetting.Filepath;

            if (!string.IsNullOrEmpty(_belongApp))
            {
                _tempfilepath += "/"+_belongApp;
            }
            if (!string.IsNullOrEmpty(_belongAppId))
            {
                _tempfilepath += "/" + _belongAppId;
            }
            if (sysSetting.Filesave == "1")
            {
                _tempfilepath = _tempfilepath + "/" + folder + "/";
            }
            if (sysSetting.Filesave == "2")
            {
                DateTime date = DateTime.Now;
                _tempfilepath = _tempfilepath + "/" + date.Year + "/" + date.Month + "/" + date.Day + "/";
            }

            var uploadPath = _filePath +"/"+ _tempfilepath;
            if (sysSetting.Fileserver == "localhost")
            {
                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }
            }
            string ext = Path.GetExtension(fileName).ToLower();
            string newName = GuidUtils.CreateNo();
            _dbfileId = newName;
            string newfileName= newName +"["+Path.GetFileNameWithoutExtension(fileName)+"]" + ext;

            using (var fs = new FileStream(uploadPath + newfileName, FileMode.Create))
            {
                fs.Write(fileBuffers, 0, fileBuffers.Length);
                fs.Close();
                //產生縮圖
                if (ext.Contains(".jpg") || ext.Contains(".jpeg") || ext.Contains(".png") || ext.Contains(".bmp") || ext.Contains(".gif"))
                {
                    string thumbnailName = newName + "_" + sysSetting.Thumbnailwidth + "x" + sysSetting.Thumbnailheight + ext;
                    ImgHelper.MakeThumbnail(uploadPath + newfileName, uploadPath + thumbnailName, sysSetting.Thumbnailwidth.ToInt(), sysSetting.Thumbnailheight.ToInt());
                    _dbThumbnail = _tempfilepath +  thumbnailName;
                }
                _dbFilePath = _tempfilepath + newfileName;
            }
        }
    }
}
