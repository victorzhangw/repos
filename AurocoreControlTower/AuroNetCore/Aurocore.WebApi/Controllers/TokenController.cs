using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Aurocore.AspNetCore.Models;
using Aurocore.AspNetCore.Mvc;
using Aurocore.Commons.Json;
using Aurocore.Commons.Log;
using Aurocore.Commons.Models;
using Aurocore.Commons.Options;
using Aurocore.Security.Application;
using Aurocore.Security.IServices;
using Aurocore.Security.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Aurocore.WebApi.Controllers
{
    /// <summary>
    /// Token令牌介面控制器
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private IAPPService _iAPPService;
        private readonly IUserService userService;
        private readonly JwtOption _jwtModel;

        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="iAPPService"></param>
        /// <param name="_userService"></param>
        /// <param name="jwtModel"></param>
        public TokenController(IAPPService iAPPService, IUserService _userService, JwtOption jwtModel)
        {
            if (iAPPService == null)
                throw new ArgumentNullException(nameof(iAPPService));
            _iAPPService = iAPPService;
            userService = _userService;
            _jwtModel = jwtModel;
        }

        /// <summary>
        /// 根據應用資訊獲得token令牌
        /// </summary>
        /// <param name="grant_type">獲取access_token填寫client_credential</param>
        /// <param name="appid">使用者唯一憑證，應用AppId</param>
        /// <param name="secret">應用金鑰AppSecret</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get(string grant_type, string appid, string secret)
        {
            CommonResult result = new CommonResult();
            if (!grant_type.Equals(GrantType.ClientCredentials))
            {
                result.ErrCode = "40003";
                result.ErrMsg = ErrCode.err40003;
                return ToJsonContent(result);
            }
            else if(string.IsNullOrEmpty(grant_type))
            {
                result.ErrCode = "40003";
                result.ErrMsg = ErrCode.err40003;
                return ToJsonContent(result);
            }
            string strHost = Request.Host.ToString();
            
            APP app = _iAPPService.GetAPP(appid, secret);
           
            if (app == null)
            {
                result.ErrCode = "40001";
                result.ErrMsg = ErrCode.err40001;
            }
            else
            {
                
                if (!app.RequestUrl.Contains(strHost) && !strHost.Contains("localhost"))
                {
                    result.ErrCode = "40002";
                    result.ErrMsg = ErrCode.err40002+"，你目前請求主機："+strHost;
                }
                else
                {
                    TokenProvider tokenProvider = new TokenProvider(_jwtModel);
                    TokenResult tokenResult = tokenProvider.GenerateToken(grant_type, appid, secret);
                    result.ResData = tokenResult;
                    result.ErrCode = "0";
                    return ToJsonContent(result);
                }
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 驗證token的合法性。
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("CheckToken")]
        [AllowAnonymous]
        public IActionResult CheckToken(string token)
        {
            CommonResult result = new CommonResult();
            TokenProvider tokenProvider = new TokenProvider(_jwtModel);
            result = tokenProvider.ValidateToken(token);
            return ToJsonContent(result);
        }


        /// <summary>
        /// 重新整理token。
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("RefreshToken")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken(string token)
        {
            CommonResult result = new CommonResult();
            TokenProvider tokenProvider = new TokenProvider(_jwtModel);
            if (!string.IsNullOrEmpty(token))
            {
                JwtSecurityToken jwtToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
                #if DEBUG
                Log4NetHelper.Debug(jwtToken.ToJson());
                #endif
                if (jwtToken != null)
                {
                    if (jwtToken.Subject == GrantType.ClientCredentials)
                    {
                        TokenResult tresult = new TokenResult();
                        var claimlist = jwtToken?.Payload.Claims as List<Claim>;
                        string strHost = Request.Host.ToString();
                        APP app = _iAPPService.GetAPP(claimlist[0].Value);
                        if (app == null)
                        {
                            result.ErrCode = "40001";
                            result.ErrMsg = ErrCode.err40001;
                        }
                        else
                        {
                            if (!app.RequestUrl.Contains(strHost) && !strHost.Contains("localhost"))
                            {
                                result.ErrCode = "40002";
                                result.ErrMsg = ErrCode.err40002 + "，你目前請求主機：" + strHost;
                            }
                            else
                            {
                                TokenResult tokenResult = tokenProvider.GenerateToken(GrantType.ClientCredentials, app.AppId, app.AppSecret);
                                result.ResData = tokenResult;
                                result.ErrCode = "0";
                                result.Success = true;
                            }
                        }
                    }
                    if (jwtToken.Subject == GrantType.Password)
                    {
                        var claimlist = jwtToken?.Payload.Claims as List<Claim>;
                        User user = await userService.GetByUserName(claimlist[2].Value);
                        TokenResult tokenResult = tokenProvider.LoginToken(user, claimlist[0].Value);
                        result.ResData = tokenResult;
                        result.ErrCode = "0";
                        result.Success = true;
                    }
                }
                else
                {
                    result.ErrMsg = ErrCode.err40004;
                    result.ErrCode = "40004";
                }
            }
            else
            {
                result.ErrMsg = ErrCode.err40004;
                result.ErrCode = "40004";
            }
            return ToJsonContent(result);
        }
        /// <summary>
        /// 把object物件轉換為ContentResult
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ToJsonContent")]
        protected IActionResult ToJsonContent(object obj)
        {
            string result = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return Content(obj.ToJson());
        }

    }
}
