import http from '@/utils/request'
import defaultSettings from '@/settings'

/**
   * 系統公告分页查询
   * @param {查询条件} data
   */
export function getSys_BulletinListWithPager (data) {
  return http.request({
    url: '/BulletinBoard/Bulletin/FindWithPagerAsync',
    method: 'post',
    data: data,
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}/**
   * 获取所有可用的系統公告
   */
export function getAllSys_BulletinList () {
  return http.request({
    url: '/BulletinBoard/Bulletin/GetAllEnable',
    method: 'get',
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}
/**
   * 新增或修改保存系統公告
   * @param data
   */
export function saveSys_Bulletin (data, url) {
  return http.request({
    url: url,
    method: 'post',
    data: data,
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}
/**
   * 获取系統公告详情
   * @param {Id} 系統公告Id
   */
export function getSys_BulletinDetail (id) {
  return http({
    url: '/BulletinBoard/Bulletin/GetById?id=' + id,
    method: 'get',
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}
/**
   * 批量设置启用状态
   * @param {id集合} ids
   */
export function setSys_BulletinEnable (data) {
  return http({
    url: '/BulletinBoard/Bulletin/SetEnabledMarktBatchAsync',
    method: 'post',
    data: data,
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}
/**
   * 批量软删除
   * @param {id集合} ids
   */
export function deleteSoftSys_Bulletin (data) {
  return http({
    url: '/BulletinBoard/Bulletin/DeleteSoftBatchAsync',
    method: 'post',
    data: data,
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}

/**
   * 批量删除
   * @param {id集合} ids
   */
export function deleteSys_Bulletin (data) {
  return http({
    url: '/BulletinBoard/Bulletin/DeleteBatchAsync',
    method: 'delete',
    data: data,
    baseURL: defaultSettings.apiHostUrl // 直接通过覆盖的方式
  })
}
