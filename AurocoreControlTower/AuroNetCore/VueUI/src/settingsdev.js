module.exports = {
  title: '管理系統',
  /**
   *是否固定頭部
   */
  fixedHeader: false,
  /**
   * 是否顯示側邊Logo
   */
  sidebarLogo: true,
  /**
 * 開啟 Tags-View
 */
  tagsView: true,

  /**
   * 應用Id
   */
  appId: 'system',
  /**
   * 應用金鑰
   */
  appSecret: '87135AB0160F706D8B47F06BDABA6FC6',
  /**
   * 子系統
   */
  subSystem: {},
  /**
   * 目前訪問系統程式碼
   */
  activeSystemCode: 'openauth',
  /**
   * 目前訪問系統名稱
   */
  activeSystemName: '',
  /**
   * 動態可訪問路由
   */
  addRouters: {},

  // apiHostUrl: 'http://netcoreapi.ts.aurocore.com/api/', // 基礎介面
  // apiSecurityUrl: 'http://netcoreapi.ts.aurocore.com/api/Security/', // 許可權管理系統介面
  // apiCMSUrl: 'http://netcoreapi.ts.aurocore.com/api/CMS/', // 文章
  // fileUrl: 'http://netcoreapi.ts.aurocore.com/', // 檔案訪問路徑
  // fileUploadUrl: 'http://netcoreapi.ts.aurocore.com/api/Files/Upload'// 檔案上傳路徑

  apiHostUrl: 'https://localhost:44372/api/', // 基礎介面
  apiSecurityUrl: 'https://localhost:44372/api/Security/', // 許可權管理系統介面
  apiCMSUrl: 'https://localhost:44372/api/CMS/', // 文章
  fileUrl: 'https://localhost:44372/', // 檔案訪問路徑
  fileUploadUrl: 'https://localhost:44372/api/Files/Upload'// 檔案上傳路徑
  /*
apiHostUrl: 'http://devzoneapi2.aurocore.com/api/', // 基礎介面
apiSecurityUrl: 'http://devzoneapi2.aurocore.com/api/Security/', // 許可權管理系統介面
apiCMSUrl: 'http://devzoneapi2.aurocore.com/api/CMS/', // 文章
fileUrl: 'http://devzoneapi2.aurocore.com/', // 檔案訪問路徑
fileUploadUrl: 'http://devzoneapi2.aurocore.com/api/Files/Upload'// 檔案上傳路徑
*/
  // apiHostUrl: 'http://193.168.25.137:8082/api/', // 基礎介面
  // apiSecurityUrl: 'http://193.168.25.137:8082/api/Security/', // 許可權管理系統介面
  // fileUrl: 'http://193.168.25.137:8082/', // 檔案訪問路徑
  // fileUploadUrl: 'http://193.168.25.137:8082/api/Files/Upload'// 檔案上傳路徑
}
