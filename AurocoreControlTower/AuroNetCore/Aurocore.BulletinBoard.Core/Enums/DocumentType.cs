﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.BulletinBoard.Enums
{
    public enum DocumentType
    {
        /// <summary>
        /// 批改申請書
        /// </summary>
        EndorsementApplication = 0,
        /// <summary>
        /// 要保書
        /// </summary>        
        InsuranceForm,
        /// <summary>
        /// 信封
        /// </summary>
        Envelope,
        /// <summary>
        /// 車輛保險單補發申請書
        /// </summary>
        CarInsuranceReissueForm,
        /// <summary>
        /// 車輛批改申請書
        /// </summary>
        CarEndorsementForm



    }
}
