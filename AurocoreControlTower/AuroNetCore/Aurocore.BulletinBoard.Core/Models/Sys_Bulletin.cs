using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.BulletinBoard.Models
{
    /// <summary>
    /// 系統公告，資料實體物件
    /// </summary>
    [Table("Sys_Bulletin")]
    [Serializable]
    public class Sys_Bulletin:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        

        /// <summary>
        /// 設定或獲取主標題
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 設定或獲取次標題 
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// 設定或獲取內文 
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// 設定或獲取標籤 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 設定或獲取作者 
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 設定或獲取敘述 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否可用 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取刪除標記
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取建立時間
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取建立人
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取建立人組織
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取部門
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取修改時間
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取修改人
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取刪除時間
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取刪除人 
        /// </summary>
        public string DeleteUserId { get; set; }

        /// <summary>
        /// 設定或獲取排序碼 
        /// </summary>
        public int? SortCode { get; set; }

    }
    
    public class InsuranceForm
    {
        /// <summary>
        /// 身份證/居留證/統一編號
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
        public string InsuranceType { get; set; }
        public string Memo { get; set; }


    }
    public class Endorsement : InsuranceForm
    {
        
        public string Context { get; set; }
    }
    public class InsuranceReissue
    {
        /// <summary>
        /// 申請人姓名
        /// </summary>
        public string ApplicantName { get; set; }
        /// <summary>
        /// 要保人
        /// </summary>
        public string PolicyHolderName { get; set; }
        /// <summary>
        /// 保險單號
        /// </summary>
        public string PolicyNo { get; set; }
       /// <summary>
       /// 補發文件(強制證)
       /// </summary>
        public string ReissueDoc1 { get; set; }
        /// <summary>
        /// 補發文件(任意保險)
        /// </summary>
        public string ReissueDoc2 { get; set; }
        /// <summary>
        /// 補發文件(收據)
        /// </summary>
        public string ReissueDoc3 { get; set; }
        /// <summary>
        /// 補發文件(收據)
        /// </summary>
        public string ReissueDoc4 { get; set; }
        /// <summary>
        ///補發理由 - 郵局寄失
        /// </summary>
        public string Reissuereason1 { get; set; }
        /// <summary>
        ///補發理由 - 遺失補發
        /// </summary>
        public string Reissuereason2 { get; set; }
        /// <summary>
        ///補發理由 - 其他
        /// </summary>
        public string Reissuereason3 { get; set; }
        /// <summary>
        /// 補發理由 - 其他說明
        /// </summary>
        public string Reissuedescription { get; set; }
        /// <summary>
        /// 補發寄送方式-掛號
        /// </summary>
        public string Delivery1 { get; set; }
        /// <summary>
        /// 補發寄送方式-限時
        /// </summary>
        public string Delivery2 { get; set; }
        /// <summary>
        /// 補發寄送方式-平信
        /// </summary>
        public string Delivery3 { get; set; }
        public string Recipient { get; set; }
        public string MailAddress { get; set; }
        /// <summary>
        /// 補發份數
        /// </summary>
        public string NumberofCopies { get; set; }

    }
    public class CarReissue:InsuranceReissue
    {
       
        /// <summary>
        /// 車牌號碼
        /// </summary>
        public string LicensePlateNo { get; set; }
        /// <summary>
        /// 強制卡號
        /// </summary>
        public string CompulsoryCardNo { get; set; }
    }
    public class Envelope
    {
        public string Name { get; set; }
        public string Addresss { get; set; }
    }


}
