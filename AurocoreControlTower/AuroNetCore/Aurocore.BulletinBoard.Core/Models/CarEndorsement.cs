﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.BulletinBoard.Core.Models
{
    
    public class CarInsuranceEndorsement
    {
        /// <summary>
        /// 要保人
        /// </summary>
        public string applicantckb { get; set; }
        /// <summary>
        /// 被保人
        /// </summary>
        public string insuredckb { get; set; }
        /// <summary>
        /// 批改書分類-過戶
        /// </summary>
        public string formtypeckb1 { get; set; }
        /// <summary>
        /// 批改書分類-退保
        /// </summary>
        public string formtypeckb2 { get; set; }
        /// <summary>
        /// 批改書分類-批加減保額
        /// </summary>
        public string formtypeckb3 { get; set; }
        /// <summary>
        /// 批改書分類-批退險種
        /// </summary>
        public string formtypeckb4 { get; set; }
        /// <summary>
        /// 批改書分類-補發
        /// </summary>
        public string formtypeckb5 { get; set; }
        /// <summary>
        /// 紙本補發次數
        /// </summary>
        public string reissueNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string iroute_accept { get; set; }
        /// <summary>
        /// 批改前-被保險人
        /// </summary>
        public string nassured { get; set; }
        /// <summary>
        /// 保單號碼
        /// </summary>
        public string ipolicy { get; set; }
        /// <summary>
        /// 強制卡號
        /// </summary>
        public string icard { get; set; }
        /// <summary>
        /// 是否已收費
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// 票據日
        /// </summary>
        public string billdate { get; set; }
        /// <summary>
        /// 批改前-住所/行動電話
        /// </summary>
        public string aassured { get; set; }
        /// <summary>
        /// 保險期間-開始
        /// </summary>
        public string dplybegin { get; set; }
        /// <summary>
        /// 批改前-要保人
        /// </summary>        
        public string napplicant { get; set; }
        /// <summary>
        /// 批改前-牌照號碼
        /// </summary>
        public string itag { get; set; }
        /// <summary>
        /// 批改前-引擎/車身號碼
        /// </summary>
        public string ibody { get; set; }
        /// <summary>
        /// 批改前-排氣量
        /// </summary>
        public string qcylinder { get; set; }
        /// <summary>
        /// 批改前-車種
        /// </summary>
        public string icartype { get; set; }
        /// <summary>
        /// 批改前-廠牌車型
        /// </summary>
        public string ibrand { get; set; }
        /// <summary>
        /// 批改前-發照年月
        /// </summary>
        public string dissue { get; set; }
        /// <summary>
        /// 批改前-製造年份
        /// </summary>
        public string iproyear { get; set; }
        /// <summary>
        /// 批改前-車損保費
        /// </summary>
        public string pdmgfactor { get; set; }
        /// <summary>
        /// 批改前-責任險
        /// </summary>
        public string pliaacc2 { get; set; }
        /// <summary>
        /// 批改前-強制險
        /// </summary>
        public string pliaacc1 { get; set; }
        /// <summary>
        /// 保險期間-結束
        /// </summary>
        public string dplyend { get; set; }
        /// <summary>
        /// 批單號碼
        /// </summary>
        public string iendorse { get; set; }
        /// <summary>
        /// 批單生效日-年
        /// </summary>        
        public string dedr_yy { get; set; }
        /// <summary>
        /// 批單生效日-月
        /// </summary>    
        public string dedr_mm { get; set; }
        /// <summary>
        /// 批單生效日-日
        /// </summary>    
        public string dedr_dd { get; set; }
        /// <summary>被保險人
        /// 批改後-
        /// </summary>
        public string nassured_new { get; set; }
        /// <summary>
        /// 批改後-住所/行動電話
        /// </summary>
        public string aassured_new { get; set; }
        /// <summary>
        /// 批改後-要保人
        /// </summary>
        public string napplicant_new { get; set; }
        /// <summary>
        /// 批改後-保險期間-開始日
        /// </summary>
        public string dedrbegindd_new { get; set; }
        /// <summary>
        /// 批改後-保險期間-開始月
        /// </summary>
        public string dedrbeginmm_new { get; set; }
        /// <summary>
        /// 批改後-保險期間-開始年
        /// </summary>
        public string dedrbeginyy_new { get; set; }
        /// <summary>
        /// 批改後-保險期間-結束日
        /// </summary>
        public string dedrenddd_new { get; set; }
        /// <summary>
        /// 批改後-保險期間-結束月
        /// </summary>
        public string dedrendmm_new { get; set; }
        /// <summary>
        /// 批改後-保險期間-結束年
        /// </summary>        
        public string dedrendyy_new { get; set; }
        /// <summary>
        /// 批改後-文字
        /// </summary>
        public string notes_new { get; set; }
        /// <summary>
        /// 批改後-車主生日
        /// </summary>
        public string carownerbirth_new { get; set; }
        /// <summary>
        /// 批改後-排氣量
        /// </summary>
        public string qcylinder_new { get; set; }
        /// <summary>
        /// 批改後-發照年月
        /// </summary>
        public string dissue_new { get; set; }
        /// <summary>
        /// 批改後-廠牌車型
        /// </summary>
        public string ibrand_new { get; set; }
        /// <summary>
        /// 批改後-車種
        /// </summary>
        public string icartype_new { get; set; }
        /// <summary>
        /// 批改後-引擎/車身號碼
        /// </summary>
        public string ibody_new { get; set; }
        /// <summary>
        /// 批改後-牌照號碼
        /// </summary>
        public string itag_new { get; set; }
        /// <summary>
        /// 批改後-製造年份
        /// </summary>
        public string iproyear_new { get; set; }
        /// <summary>
        /// 批改後-增減內容
        /// </summary>
        public string content_new { get; set; }
        /// <summary>
        /// 批改後-增減
        /// </summary>        
        public string plus_new { get; set; }
        /// <summary>
        /// 批改原因
        /// </summary>
        public string edr_reason { get; set; }
        /// <summary>
        /// 批改後-責任險保費
        /// </summary>
        public string pliaacc2_new { get; set; }
        /// <summary>
        /// 批改後-強制險保費
        /// </summary>
        public string pliaacc1_new { get; set; }
        /// <summary>
        /// 批改後-車損險保費
        /// </summary>
        public string pdmgfactor_new { get; set; }
        /// <summary>
        /// 批改後車主身份證號
        /// </summary>
        public string carownerId_new { get; set; }
        /// <summary>
        /// 列印日期
        /// </summary>
        public string printdate { get; set; }
        /// <summary>
        /// 保經代/銀行分行名稱
        /// </summary>
        public string nroute { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string iroute { get; set; }
        public string isales { get; set; }
        public string iofficer { get; set; }
        public string ileader { get; set; }
        /// <summary>
        /// 申請人簽章
        /// </summary>
        public string applicant_sign { get; set; }
        /// <summary>
        /// 申請日期
        /// </summary>
        public string dentry { get; set; }
        /// <summary>
        /// 批改前-受益人姓名
        /// </summary>
        public string nbeneficiary { get; set; }
        /// <summary>
        /// 批改後-受益人姓名
        /// </summary>
        public string nbeneficiary_new { get; set; }
        /// <summary>
        /// 批改前-受益人與被保險人關係
        /// </summary>
        public string relationbene { get; set; }
        /// <summary>
        /// 批改後-受益人與被保險人關係
        /// </summary>
        public string relationbene_new { get; set; }
        /// <summary>
        /// 批改前-受益人地址
        /// </summary>
        public string abeneficiary { get; set; }
        /// <summary>
        /// 批改後-受益人地址
        /// </summary>
        public string abeneficiary_new { get; set; }
        /// <summary>
        /// 批改前-受益人電話
        /// </summary>
        public string tbeneficiary { get; set; }
        /// <summary>
        /// 批改後-受益人電話
        /// </summary>
        public string tbeneficiary_new { get; set; }
        /// <summary>
        /// 電子批單郵寄帳號
        /// </summary>
        public string aemail_reply { get; set; }
        public string imgr_icard { get; set; }
        public string MemberInfoID { get; set; }
        /// <summary>
        /// 保單條碼
        /// </summary>
        public string ipolicyBarCode { get; set; }
        public string ipolicyBarCode2 { get; set; }
    }
}
