﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.BulletinBoard.Models
{
    public class UserNotification
    {
        //案件或單據 主鍵
        public string Id { get; set; }
        public string ParentFormTypeId { get; set; }
        public string ParentFormTypeName { get; set; }
        public string FormTypeId { get; set; }
        public string FormTypeName { get; set; }
        public string Head { get; set; }
        public string Body { get; set; }
        public string AssignedTime { get; set; }
       

    }
}
