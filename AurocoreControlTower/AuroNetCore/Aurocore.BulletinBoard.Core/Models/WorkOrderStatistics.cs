﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.BulletinBoard.Models
{
    /// <summary>
    /// 公佈欄案件統計數字
    /// </summary>
    public class WorkOrderStatistics
    {
        
        
        public string ParentFormTypeId { get; set; }
        public string ParentFormTypeName { get; set; }
        public string statisticsStartDate { get; set; }
        public string statisticsEndDate { get; set; }
        public StatisticsIndicator statisticsIndicator { get; set; }
       
    }
    /// <summary>
    /// 統計指標
    /// </summary>
    public class StatisticsIndicator
    {
        public StatisticsIndicator()
        {
            Indc1 = "0";
            Indc2 = "0";

        }
        public string Indc1 { get; set; }
        public string Indc2 { get; set; }
    }
    /// <summary>
    ///  資料庫回傳
    /// </summary>
    public class WorkOrderDbStatistics
    {
        public string ParentFormTypeId { get; set; }
        public string ParentFormTypeName { get; set; }
        public string Indicator { get; set; }

    }
}
