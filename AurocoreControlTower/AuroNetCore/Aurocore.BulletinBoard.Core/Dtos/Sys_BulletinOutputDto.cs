using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.BulletinBoard.Dtos
{
    /// <summary>
    /// 系統公告輸出物件模型
    /// </summary>
    [Serializable]
    public class Sys_BulletinOutputDto
    {
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(100)]
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取主標題 
        /// </summary>
        [MaxLength(400)]
        public string Title { get; set; }

        /// <summary>
        /// 設定或獲取次標題 
        /// </summary>
        [MaxLength(400)]
        public string SubTitle { get; set; }

        /// <summary>
        /// 設定或獲取內文 
        /// </summary>
        [MaxLength(2000)]
        public string Context { get; set; }

        /// <summary>
        /// 設定或獲取標籤 
        /// </summary>
        [MaxLength(510)]
        public string Type { get; set; }

        /// <summary>
        /// 設定或獲取作者 
        /// </summary>
        [MaxLength(510)]
        public string Author { get; set; }

        /// <summary>
        /// 設定或獲取敘述  
        /// </summary>
        [MaxLength(400)]
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否可用 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取刪除標記 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取建立時間 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取建立人 
        /// </summary>
        [MaxLength(100)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取建立人組織 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取部門 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取修改時間 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取修改人 
        /// </summary>
        [MaxLength(100)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取刪除時間 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取刪除人 
        /// </summary>
        [MaxLength(100)]
        public string DeleteUserId { get; set; }

        /// <summary>
        /// 設定或獲取排序碼 
        /// </summary>
        public int? SortCode { get; set; }

    }
}
