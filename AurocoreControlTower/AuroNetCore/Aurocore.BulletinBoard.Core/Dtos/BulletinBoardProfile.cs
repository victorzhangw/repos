using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.BulletinBoard.Models;

namespace Aurocore.BulletinBoard.Dtos
{
    public class BulletinBoardProfile : Profile
    {
        public BulletinBoardProfile()
        {
           CreateMap<Sys_Bulletin, Sys_BulletinOutputDto>();
           CreateMap<Sys_BulletinInputDto, Sys_Bulletin>();

        }
    }
}
