using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.BulletinBoard.Models;

namespace Aurocore.BulletinBoard.Dtos
{
    /// <summary>
    /// 系統公告輸入物件模型
    /// </summary>
    [AutoMap(typeof(Sys_Bulletin))]
    [Serializable]
    public class Sys_BulletinInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 標題 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SortCode { get; set; }

    }
}
