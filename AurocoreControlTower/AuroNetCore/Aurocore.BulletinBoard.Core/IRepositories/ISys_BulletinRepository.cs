using System;
using Aurocore.Commons.IRepositories;
using Aurocore.BulletinBoard.Models;
using Aurocore.Commons.Pages;
using Aurocore.BulletinBoard.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aurocore.BulletinBoard.IRepositories
{
    /// <summary>
    /// 定義系統公告倉儲接口
    /// </summary>
    public interface ISys_BulletinRepository:IRepository<Sys_Bulletin, string>
    {
        /// <summary>
        /// 取得公告
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<Sys_BulletinOutputDto> GetBulletins(string top);
        /// <summary>
        /// 取得個人通知
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<UserNotification>> GetUserNotifications(string sql);

    }
}