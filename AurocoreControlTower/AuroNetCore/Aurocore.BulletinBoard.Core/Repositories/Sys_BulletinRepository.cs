using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.BulletinBoard.IRepositories;
using Aurocore.BulletinBoard.Models;
using Dapper;
using Aurocore.Commons.Pages;
using Aurocore.BulletinBoard.Dtos;
using System.Collections.Generic;
using System.Linq;
using Aurocore.Commons.Mapping;
using Aurocore.WorkOrders.Enums;
using System.Threading.Tasks;

namespace Aurocore.BulletinBoard.Repositories
{
	/// <summary>
	/// 系統公告倉儲接口實現
	/// </summary>
	public class Sys_BulletinRepository : BaseRepository<Sys_Bulletin, string>, ISys_BulletinRepository
	{
		public Sys_BulletinRepository()
		{
			this.tableName = "Sys_BulletinBoard";
			this.primaryKey="Id";

		}

		public Sys_BulletinRepository(IDbContextCore context) : base(context)
		{
		}
		public PageResult<Sys_BulletinOutputDto> GetBulletins(string top)
		{
			List<WorkOrderStatistics> woStatistics = new();
			string FirstDay = DateTime.Now.AddDays(-DateTime.Now.Day + 1).ToString("yyyy/MM/dd");

			string LastDay = DateTime.Now.AddMonths(1).AddDays(-DateTime.Now.AddMonths(1).Day).ToString("yyyy/MM/dd");
			foreach (string itemName in Enum.GetNames(typeof(WorkOrderTypeforBulletin)))
			{
				if(itemName != "個人案件")
				{
					string formTypeSql = $" SELECT Id FROM Sys_ItemsDetail WHERE ItemName='{itemName}'";
					string formId = DapperConn.QueryFirstOrDefault<string>(formTypeSql);
					if (string.IsNullOrEmpty(formId))
					{
						throw new Exception("數據字典單據 ItemName 有誤,需檢查");
						
					}
					
					string toDayOpenCase = string.Empty;
					string toDayCloseCase = string.Empty;
					switch (itemName)
					{
						case "全部案件":
							//當月所有進件 SQL
							toDayOpenCase = @$"SELECT '{formId}' as ParentFormTypeId, '{itemName}' as ParentFormTypeName,COUNT(ParentFormTypeId)as 'Indicator' FROM CRM_WorkOrder 
						WHERE convert(varchar,LastModifyTime, 111) BETWEEN convert(varchar, dateadd(m, datediff(m,0,getdate()),0), 111) AND convert(varchar, dateadd(day ,-1, dateadd(m, datediff(m,0,getdate())+1,0)), 111)";


							//當月所有結案 SQL
							toDayCloseCase = @$" SELECT '{formId}' as ParentFormTypeId, '{itemName}' as ParentFormTypeName,COUNT(ParentFormTypeId)as 'Indicator' FROM CRM_WorkOrder 
						WHERE convert(varchar,LastModifyTime, 111) BETWEEN convert(varchar, dateadd(m, datediff(m,0,getdate()),0), 111) AND convert(varchar, dateadd(day ,-1, dateadd(m, datediff(m,0,getdate())+1,0)), 111)
						AND StatusId='2021021618370591104724' ";

							break;
						case "待辦單據":
							//當月所有進件單據 SQL
							toDayOpenCase = @$"SELECT '{formId}' as ParentFormTypeId, '{itemName}' as ParentFormTypeName,COUNT(Id)as 'Indicator' FROM CRM_WorkOrderDetail 
						WHERE convert(varchar,LastModifyTime, 111) BETWEEN convert(varchar, dateadd(m, datediff(m,0,getdate()),0), 111) AND convert(varchar, dateadd(day ,-1, dateadd(m, datediff(m,0,getdate())+1,0)), 111)";


							//當月所有結案單據 SQL
							toDayCloseCase = @$" SELECT '{formId}' as ParentFormTypeId, '{itemName}' as ParentFormTypeName,COUNT(Id)as 'Indicator' FROM CRM_WorkOrderDetail 
						WHERE convert(varchar,LastModifyTime, 111) BETWEEN convert(varchar, dateadd(m, datediff(m,0,getdate()),0), 111) AND convert(varchar, dateadd(day ,-1, dateadd(m, datediff(m,0,getdate())+1,0)), 111) 
						AND StatusId='2021021618370591104724' ";
							break;
						default:
							//當月所有進件 SQL
							toDayOpenCase = @$"SELECT ParentFormTypeId,ParentFormTypeName,COUNT(ParentFormTypeId)as 'Indicator' FROM CRM_WorkOrder 
						WHERE convert(varchar,LastModifyTime, 111) BETWEEN convert(varchar, dateadd(m, datediff(m,0,getdate()),0), 111) AND convert(varchar, dateadd(day ,-1, dateadd(m, datediff(m,0,getdate())+1,0)), 111)
						AND ParentFormTypeId='{formId}'
						 Group by ParentFormTypeId,ParentFormTypeName";

							//當月所有結案 SQL
							toDayCloseCase = @$" SELECT ParentFormTypeId,ParentFormTypeName,COUNT(ParentFormTypeId)as 'Indicator' FROM CRM_WorkOrder 
						WHERE convert(varchar,LastModifyTime, 111) BETWEEN convert(varchar, dateadd(m, datediff(m,0,getdate()),0), 111) AND convert(varchar, dateadd(day ,-1, dateadd(m, datediff(m,0,getdate())+1,0)), 111)
						AND StatusId='2021021618370591104724' AND  ParentFormTypeId='{formId}'
						 Group by ParentFormTypeId,ParentFormTypeName";
							break;

					};
					
					
					WorkOrderDbStatistics toDayOpenCount = DapperConn.QueryFirstOrDefault<WorkOrderDbStatistics>(toDayOpenCase);
					WorkOrderDbStatistics toDayCloseCount = DapperConn.QueryFirstOrDefault<WorkOrderDbStatistics>(toDayCloseCase);
					StatisticsIndicator indicator = new StatisticsIndicator();
					if (toDayOpenCount != null)
					{
						indicator.Indc1 = toDayOpenCount.Indicator;
					}
					if (toDayCloseCount != null)
					{
						indicator.Indc2 = toDayCloseCount.Indicator;
					}
					if(toDayOpenCount == null && toDayCloseCount == null)
					{
						woStatistics.Add(new WorkOrderStatistics
						{
							ParentFormTypeId = formId,
							ParentFormTypeName = itemName,
							statisticsIndicator = indicator
						});
					}
					else
					{
						woStatistics.Add(new WorkOrderStatistics
						{
							ParentFormTypeId = toDayOpenCount.ParentFormTypeId,
							ParentFormTypeName = toDayOpenCount.ParentFormTypeName,
							statisticsIndicator = indicator
						});
					}
					


				}
			}


			

			string sql = @"SELECT TOP " + top + " * FROM Sys_Bulletin b WHERE EnabledMark=1 ORDER BY CreatorTime";
			List<Sys_Bulletin> result = DapperConn.Query<Sys_Bulletin>(sql).ToList();
			PageResult<Sys_BulletinOutputDto> pageResult = new PageResult<Sys_BulletinOutputDto>
			{
				CurrentPage = 1,
				Items = result.MapTo<Sys_BulletinOutputDto>(),
				ItemsPerPage = 1,
				ResData= woStatistics,
				RelationData= FirstDay+"|"+ LastDay,
				TotalItems = result.Count
			};
			return pageResult;
		}

		public async Task<List<UserNotification>> GetUserNotifications( string sql)
		{
			
			var result = await DapperConn.QueryAsync<UserNotification>(sql).ConfigureAwait(false);
			
			return result.ToList();

		}

	   
	}
}