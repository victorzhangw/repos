using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.BulletinBoard.IRepositories;
using Aurocore.BulletinBoard.IServices;
using Aurocore.BulletinBoard.Dtos;
using Aurocore.BulletinBoard.Models;
using Aurocore.BulletinBoard.Enums;
using iText;
using System.Reflection;
using System.IO;
using iText.Kernel.Pdf;
using iText.Forms;
using iText.Kernel.Font;
using iText.IO.Font;
using AutoMapper;
using iText.Barcodes;
using System.Drawing;
using iText.Kernel.Pdf.Xobject;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Colors;
using iText.Forms.Fields;
using System.Globalization;
using Aurocore.BulletinBoard.Core.Models;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.Enums;
using Aurocore.WorkOrders.IRepositories;
using Aurocore.Security.IRepositories;


namespace Aurocore.BulletinBoard.Services
{
	/// <summary>
	/// 系統公告服務接口實現
	/// </summary>
	public class Sys_BulletinService : BaseService<Sys_Bulletin, Sys_BulletinOutputDto, string>, ISys_BulletinService
	{

		private readonly ISys_BulletinRepository _repository;
		private readonly IWorkOrderRepository _workOrderrepository;
		private readonly IWorkOrderDetailRepository _workOrderDetailrepository;
		private readonly IItemsDetailRepository _itemsDetailRepository;
		public static string fontType = "kaiu.ttf";
		public static Assembly currentAssembly = Assembly.GetExecutingAssembly();
		public static TaiwanCalendar taiwanCalendar = new TaiwanCalendar();
		public Sys_BulletinService(ISys_BulletinRepository repository,
			IWorkOrderRepository workOrderRepository,IWorkOrderDetailRepository _workOrderDetailRepository ,IItemsDetailRepository itemsDetailRepository) : base(repository)
		{
			_repository = repository;
			_workOrderrepository = workOrderRepository;
			_workOrderDetailrepository = _workOrderDetailRepository;
			_itemsDetailRepository = itemsDetailRepository;
		}
		/// <summary>
		/// 獲取 公告
		/// </summary>
		/// <param name="top">資料筆數</param>
		///
		/// <returns></returns>

		public PageResult<Sys_BulletinOutputDto> GetBulletin(string top)
		{
			return _repository.GetBulletins(top);
		}
		/// <summary>
		/// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
		/// </summary>
		/// <param name="search">查詢的條件</param>
		/// <returns>指定物件的集合</returns>
		public override async Task<PageResult<Sys_BulletinOutputDto>> FindWithPagerAsync(SearchInputDto<Sys_Bulletin> search)
		{
			bool order = search.Order == "asc" ? false : true;
			string where = GetDataPrivilege(false);
			if (!string.IsNullOrEmpty(search.Keywords))
			{
				where += " and (Title like '%" + search.Keywords + "%' or Context like '%" + search.Keywords + "%')";
			};
			PagerInfo pagerInfo = new PagerInfo
			{
				CurrenetPageIndex = search.CurrenetPageIndex,
				PageSize = search.PageSize
			};
			List<Sys_Bulletin> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
			PageResult<Sys_BulletinOutputDto> pageResult = new PageResult<Sys_BulletinOutputDto>
			{
				CurrentPage = pagerInfo.CurrenetPageIndex,
				Items = list.MapTo<Sys_BulletinOutputDto>(),
				ItemsPerPage = pagerInfo.PageSize,
				TotalItems = pagerInfo.RecordCount
			};
			return pageResult;
		}

		public void GeneratePDFFiles(string baseName, DocumentType formType, dynamic dynObject)
		{

			var config = new MapperConfiguration(cfg => { });
			var mapper = config.CreateMapper();

			switch (formType)
			{
				case DocumentType.EndorsementApplication:
					var _endorsement = mapper.Map<Endorsement>(dynObject);
					Generate(baseName, DocumentType.EndorsementApplication.ToString(), _endorsement);
					break;
				case DocumentType.InsuranceForm:
					var insuranceForm = mapper.Map<Endorsement>(dynObject);
					//Generate(DocumentType.InsuranceForm.ToString(), dynObject);
					break;
				case DocumentType.Envelope:
					var envelope = mapper.Map<Endorsement>(dynObject);
					//Generate(DocumentType.Envelope.ToString(), dynObject);
					break;
				case DocumentType.CarInsuranceReissueForm:
					var _carinsurancereissue = mapper.Map<CarReissue>(dynObject);
					Generate(baseName, DocumentType.CarInsuranceReissueForm.ToString(), _carinsurancereissue);
					break;
				case DocumentType.CarEndorsementForm:
					var _carendorsement = mapper.Map<CarInsuranceEndorsement>(dynObject);
					Generate(baseName, DocumentType.CarInsuranceReissueForm.ToString(), _carendorsement);
					break;

			}
		}
		/// <summary>
		/// 批改申請
		/// </summary>
		/// <param name="outFilenName"></param>
		/// <param name="formType"></param>
		/// <param name="endorsement"></param>
		private static void Generate(string outFilenName, string formType, Endorsement endorsement)
		{

			string docPath = $"{currentAssembly.GetName().Name}.DocTemplate.{formType}.pdf";
			string fontPath = Path.Combine(Path.GetDirectoryName(currentAssembly.Location), "DocTemplate", fontType);
			//using (var stream = new FileStream(docPath, FileMode.Open))
			using (var stream = currentAssembly.GetManifestResourceStream(docPath))
			{
				var path = AppDomain.CurrentDomain.BaseDirectory;
				//path = path.Substring(0, path.IndexOf("\\bin"));
				var parentPath = path.Substring(0, path.LastIndexOf("\\"));
				var servicesPath = parentPath + "\\" + formType;
				if (!Directory.Exists(servicesPath))
				{
					servicesPath = parentPath + "\\" + formType;
					Directory.CreateDirectory(servicesPath);
				}
				var outPath = servicesPath + "\\" + $"{outFilenName}-{DateTime.Now.Ticks}.pdf";
				if (File.Exists(outPath))
					return;

				//using (var inputStream = new FileStream(docPath, FileMode.Open))
				using (var outputStream = File.Create(outPath))
				{

					PdfFont horizonFont = PdfFontFactory.CreateFont(fontPath, PdfEncodings.IDENTITY_H, true);
					PdfFont verticalFont = PdfFontFactory.CreateFont(fontPath, PdfEncodings.IDENTITY_V, true);
					PdfDocument pdfDoc = new PdfDocument(new PdfReader(stream), new PdfWriter(outputStream));
					PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDoc, true);
					var fields = form.GetFormFields();

					foreach (var member in endorsement.GetType().GetProperties())
					{
						fields[member.Name].SetValue((string)member.GetValue(endorsement, null), horizonFont, 10);
					}
					var f = fields["EnvelopeName"].SetValue(endorsement.Name, verticalFont, 12);

					iText.Kernel.Geom.Rectangle pageSize = pdfDoc.GetFirstPage().GetPageSize();
					Barcode39 barcode = new Barcode39(pdfDoc);
					// barcode.SetCodeType(Barcode39.Equals.);
					barcode.SetCode("APC0123456789852");
					PdfFormXObject barcodeXObject = barcode.CreateFormXObject(ColorConstants.BLACK, ColorConstants.BLACK, pdfDoc);
					PdfCanvas canvas = new PdfCanvas(pdfDoc.GetFirstPage());
					float x = (pageSize.GetWidth() / 2) - (barcodeXObject.GetWidth() / 2);
					float y = pageSize.GetBottom() + 50;
					float w = barcodeXObject.GetWidth();
					float h = barcodeXObject.GetHeight();
					canvas.SaveState();
					canvas.SetFillColor(ColorConstants.LIGHT_GRAY);
					canvas.Rectangle(x, y, w, h);
					canvas.Fill();
					canvas.RestoreState();
					canvas.AddXObjectAt(barcodeXObject, x, y);
					/*
					foreach (var field in fields)
					{
						FieldInfo inputField = typeof(Endorsement).GetField(field.Key);
						fields[field.Key].SetValue(inputField.GetValue(endorsement).ToString());
					}*/
					pdfDoc.Close();

				}
			}

		}
		/// <summary>
		/// 汽車險補發
		/// </summary>
		/// <param name="outFilenName"></param>
		/// <param name="formType"></param>
		/// <param name="carReissue"></param>
		private static void Generate(string outFilenName, string formType, CarReissue carReissue)
		{
			string docPath = $"{currentAssembly.GetName().Name}.DocTemplate.{formType}.pdf";
			string fontPath = Path.Combine(Path.GetDirectoryName(currentAssembly.Location), "DocTemplate", fontType);
			//using (var stream = new FileStream(docPath, FileMode.Open))
			using (var stream = currentAssembly.GetManifestResourceStream(docPath))
			{
				var path = AppDomain.CurrentDomain.BaseDirectory;
				//path = path.Substring(0, path.IndexOf("\\bin"));
				var parentPath = path.Substring(0, path.LastIndexOf("\\"));
				var servicesPath = parentPath + "\\" + formType;
				if (!Directory.Exists(servicesPath))
				{
					servicesPath = parentPath + "\\" + formType;
					Directory.CreateDirectory(servicesPath);
				}
				var outPath = servicesPath + "\\" + $"{outFilenName}-{DateTime.Now.Ticks}.pdf";
				if (File.Exists(outPath))
					return;

				//using (var inputStream = new FileStream(docPath, FileMode.Open))
				using (var outputStream = File.Create(outPath))
				{

					PdfFont horizonFont = PdfFontFactory.CreateFont(fontPath, PdfEncodings.IDENTITY_H, true);
					PdfFont verticalFont = PdfFontFactory.CreateFont(fontPath, PdfEncodings.IDENTITY_V, true);
					PdfDocument pdfDoc = new PdfDocument(new PdfReader(stream), new PdfWriter(outputStream));
					PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDoc, true);

					var fields = form.GetFormFields();

					foreach (var member in carReissue.GetType().GetProperties())
					{

						if (!String.IsNullOrEmpty((string)member.GetValue(carReissue)))
						{

							if (fields[member.Name].GetType() == typeof(PdfButtonFormField))
							{
								fields[member.Name].SetCheckType(PdfFormField.TYPE_CHECK);//PdfFormField.TYPE_CIRCLE,PdfFormField.TYPE_CROSS,PdfFormField.TYPE_DIAMOND,PdfFormField.TYPE_SQUARE,PdfFormField.TYPE_STAR,etc
								fields[member.Name].SetValue("Yes", true);
							}
							else
							{
								fields[member.Name].SetValue((string)member.GetValue(carReissue, null), horizonFont, 10);
							}
						}


					}
					PdfFormField ROCYear = fields["ROCYear"].SetValue(taiwanCalendar.GetYear(DateTime.Now).ToString(), horizonFont, 12);
					PdfFormField ROCMonth = fields["ROCMonth"].SetValue(taiwanCalendar.GetMonth(DateTime.Now).ToString(), horizonFont, 12);
					PdfFormField ROCDay = fields["ROCDay"].SetValue(taiwanCalendar.GetDayOfMonth(DateTime.Now).ToString(), horizonFont, 12);
					PdfFormField vAddr = fields["VEnvelopeAddr"].SetValue(carReissue.MailAddress, horizonFont, 12);
					PdfFormField VName = fields["VEnvelopeName"].SetValue(carReissue.Recipient, horizonFont, 14);
					PdfFormField hAddr = fields["HEnvelopeAddr"].SetValue(carReissue.MailAddress, horizonFont, 12);
					PdfFormField hName = fields["HEnvelopeName"].SetValue(carReissue.Recipient, horizonFont, 14);
					form.FlattenFields();
					pdfDoc.Close();

				}
			}

		}
		private static void Generate(string outFilenName, string formType, CarInsuranceEndorsement carEndorsement)
		{
			string docPath = $"{currentAssembly.GetName().Name}.DocTemplate.{formType}.pdf";
			string fontPath = Path.Combine(Path.GetDirectoryName(currentAssembly.Location), "DocTemplate", fontType);
			//using (var stream = new FileStream(docPath, FileMode.Open))
			using (var stream = currentAssembly.GetManifestResourceStream(docPath))
			{
				var path = AppDomain.CurrentDomain.BaseDirectory;
				//path = path.Substring(0, path.IndexOf("\\bin"));
				var parentPath = path.Substring(0, path.LastIndexOf("\\"));
				var servicesPath = parentPath + "\\" + formType;
				if (!Directory.Exists(servicesPath))
				{
					servicesPath = parentPath + "\\" + formType;
					Directory.CreateDirectory(servicesPath);
				}
				var outPath = servicesPath + "\\" + $"{outFilenName}-{DateTime.Now.Ticks}.pdf";
				if (File.Exists(outPath))
					return;

				//using (var inputStream = new FileStream(docPath, FileMode.Open))
				using (var outputStream = File.Create(outPath))
				{

					PdfFont horizonFont = PdfFontFactory.CreateFont(fontPath, PdfEncodings.IDENTITY_H, true);
					PdfFont verticalFont = PdfFontFactory.CreateFont(fontPath, PdfEncodings.IDENTITY_V, true);
					PdfDocument pdfDoc = new PdfDocument(new PdfReader(stream), new PdfWriter(outputStream));
					PdfAcroForm form = PdfAcroForm.GetAcroForm(pdfDoc, true);

					var fields = form.GetFormFields();

					foreach (var member in carEndorsement.GetType().GetProperties())
					{

						if (!String.IsNullOrEmpty((string)member.GetValue(carEndorsement)))
						{

							if (fields[member.Name].GetType() == typeof(PdfButtonFormField))
							{
								fields[member.Name].SetCheckType(PdfFormField.TYPE_CHECK);//PdfFormField.TYPE_CIRCLE,PdfFormField.TYPE_CROSS,PdfFormField.TYPE_DIAMOND,PdfFormField.TYPE_SQUARE,PdfFormField.TYPE_STAR,etc
								fields[member.Name].SetValue("Yes", true);
							}
							else
							{
								fields[member.Name].SetValue((string)member.GetValue(carEndorsement, null), horizonFont, 10);
							}
						}


					}
					/*
					PdfFormField ROCYear = fields["ROCYear"].SetValue(taiwanCalendar.GetYear(DateTime.Now).ToString(), horizonFont, 12);
					PdfFormField ROCMonth = fields["ROCMonth"].SetValue(taiwanCalendar.GetMonth(DateTime.Now).ToString(), horizonFont, 12);
					PdfFormField ROCDay = fields["ROCDay"].SetValue(taiwanCalendar.GetDayOfMonth(DateTime.Now).ToString(), horizonFont, 12);
					PdfFormField vAddr = fields["VEnvelopeAddr"].SetValue(carReissue.MailAddress, horizonFont, 12);
					PdfFormField VName = fields["VEnvelopeName"].SetValue(carReissue.Recipient, horizonFont, 14);
					PdfFormField hAddr = fields["HEnvelopeAddr"].SetValue(carReissue.MailAddress, horizonFont, 12);
					PdfFormField hName = fields["HEnvelopeName"].SetValue(carReissue.Recipient, horizonFont, 14);
					*/
					form.FlattenFields();
					pdfDoc.Close();

				}
			}

		}
		public async Task<List<UserNotification>> FindNotificationAsync(SeachWorkOrder search)
		{
			string formWhere1 = string.Format("ItemName='{0}' ", nameof(WorkflowStatus.處理中)); ;
			var formItems1 = await _itemsDetailRepository.GetWhereAsync(formWhere1).ConfigureAwait(false);
			string formWhere2 = string.Format("ItemName='{0}' ", nameof(WorkflowStatus.協辦中)); ;
			var formItems2 = await _itemsDetailRepository.GetWhereAsync(formWhere2).ConfigureAwait(false);
			string formWhere3 = string.Format("ItemName='{0}' ", nameof(WorkOrderType.待辦單據)); ;
			var formItems3 = await _itemsDetailRepository.GetWhereAsync(formWhere3).ConfigureAwait(false);
			List<UserNotification> userNotifications = new();
			//  案件 where conditions
			string forCasewhere = " WHERE 1=1 ";
			string forFormwhere = " WHERE 1=1 ";
			forCasewhere += string.Format(" AND CONVERT(varchar(10), CaseOpenTime, 111) = CONVERT(varchar(10), GETDATE(), 111) AND  OwnerId = '"+search.CurrentUserId+"' AND StatusId='"+ formItems1.Id + "' AND IsNotified=0");
			forFormwhere += string.Format(" AND (CONVERT(varchar(10), LastModifyTime, 111) = CONVERT(varchar(10), GETDATE(), 111) OR  CONVERT(varchar(10), CreatorTime, 111) = CONVERT(varchar(10), GETDATE(), 111)) ");
			forFormwhere += string.Format(" AND CurrentEditorId = '"+ search.CurrentUserId + "' AND(StatusId = '"+ formItems1.Id + "' OR StatusId = '" + formItems2.Id + "')  AND IsNotified=0");
			string notifySql = $@"SELECT Id ,  ParentFormTypeId, ParentFormTypeName,FormTypeId,FormTypeName,Header,Body,CONVERT(varchar(10), GETDATE(), 111)   as AssignedTime  from CRM_WorkOrder {forCasewhere}
				union
				SELECT Id,'{formItems3.Id}' as ParentFormTypeId,'{formItems3.ItemName}' as ParentFormTypeName,ResponseTypeId as FormTypeId,ResponseTypeName as FormTypeName ,Header,Body, 
				CONVERT(varchar(10), GETDATE(), 111) as AssignedTime from CRM_WorkOrderDetail  {forFormwhere}";

			//string notifySql = $@"SELECT Id ,'{formItems3.Id}' as ParentFormTypeId,'{formItems3.ItemName}' as ParentFormTypeName,FormTypeId,FormTypeName,Header,Body,CONVERT(varchar(10), GETDATE(), 111)   as AssignedTime  from CRM_WorkOrder {forCasewhere}
			//	union
			//	SELECT Id,'{formItems3.Id}' as ParentFormTypeId,'{formItems3.ItemName}' as ParentFormTypeName,ResponseTypeId as FormTypeId,ResponseTypeName as FormTypeName ,Header,Body, 
			//	CONVERT(varchar(10), GETDATE(), 111) as AssignedTime from CRM_WorkOrderDetail  {forFormwhere}";
			userNotifications = await _repository.GetUserNotifications(notifySql).ConfigureAwait(false);

			//List<WorkOrder> list = await DapperConn.Query<WorkOrderMenu>(menuSql).ToList();
			
			
			return userNotifications;


		}
		public async Task<bool>  NotificationStatus(WorkOrder workOrder)
		{
			
			workOrder.IsNotified = true;
			var bl = await _workOrderrepository.UpdateAsync(workOrder, workOrder.Id);
			return bl;

		}
		public async Task<bool> NotificationStatus(WorkOrderDetail workOrderDetail)
		{
			workOrderDetail.IsNotified = true;
			var bl = await _workOrderDetailrepository.UpdateAsync(workOrderDetail, workOrderDetail.Id);
			return bl;
		}
	}
}
	

