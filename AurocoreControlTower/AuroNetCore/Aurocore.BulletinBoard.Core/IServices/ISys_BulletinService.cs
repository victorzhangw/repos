using System;
using Aurocore.Commons.IServices;
using Aurocore.BulletinBoard.Dtos;
using Aurocore.BulletinBoard.Models;
using Aurocore.Commons.Pages;
using Aurocore.BulletinBoard.Enums;
using System.Threading.Tasks;
using Aurocore.WorkOrders.Models;
using Aurocore.WorkOrders.Dtos;
using System.Collections.Generic;

namespace Aurocore.BulletinBoard.IServices
{
    /// <summary>
    /// 定義系統公告服務接口
    /// </summary>
    public interface ISys_BulletinService:IService<Sys_Bulletin,Sys_BulletinOutputDto, string>
    {
        PageResult<Sys_BulletinOutputDto> GetBulletin(string top);
        void GeneratePDFFiles(string baseName, DocumentType formType, dynamic dynObject);
        Task<List<UserNotification>> FindNotificationAsync(SeachWorkOrder search);
        Task<bool> NotificationStatus(WorkOrder workOrder);
        Task<bool> NotificationStatus(WorkOrderDetail workOrderDetail);
    }
}
