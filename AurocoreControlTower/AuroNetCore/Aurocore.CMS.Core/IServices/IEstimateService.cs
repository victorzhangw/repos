using System;
using Aurocore.Commons.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Dtos;

namespace Aurocore.CMS.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface IEstimateService:IService<Vehicle_Estimate,EstimateOutputDto, string>
       
    {
        /// <summary>
        /// 取得要保書資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Vehicle_Estimate> search);
        /// <summary>
        /// 取得要保書資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        VehicleEstimate GetVehicleEstimate(string EstimateId);
        /// <summary>
        /// 取得要保書資料與關聯資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Vehiche_withRelations GetVehicleEstimatewithRealtions(string EstimateId);
        /// <summary>
        /// 取得要保書資料列表與客戶資料
        /// </summary>
        /// <param name="Pid">身份證號碼</param>
        /// <returns></returns>
        EstimatesWithCustomer GetEstimatesbyPid(string Pid);
        /// <summary>
        /// 取得要保書資料列表與客戶資料
        /// </summary>
        /// <param name="PlateNo">車牌號碼</param>
        /// <returns></returns>
        EstimatesWithCustomer GetEstimatesbyPlateNo(string PlateNo);
        /// <summary>
        /// 取得要保書資料列表與客戶資料
        /// </summary>
        /// <param name="PhoneNo">電話號碼</param>
        /// <returns></returns>
        EstimatesWithCustomer GetEstimatesbyPhoneNo(string PhoneNo);
        /// <summary>
        /// 取得要保書資料列表與客戶資料
        /// </summary>
        /// <param name="EstimateId">要保書Id</param>
        /// <returns></returns>
        EstimatesWithCustomer GetEstimatesbyEstimateId(string EstimateId);
    }
}
