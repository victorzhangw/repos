using System;
using Aurocore.Commons.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.IServices
{
    /// <summary>
    /// 定義服務接口
    /// </summary>
    public interface IEstimate_HccService:IService<Estimate_Hcc,Estimate_HccOutputDto, string>
    {
    }
}
