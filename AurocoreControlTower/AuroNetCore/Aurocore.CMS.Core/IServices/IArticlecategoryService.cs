using System;
using Aurocore.Commons.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using Aurocore.Commons.Models;
using Aurocore.Commons.Core.Dtos;
using System.Data;

namespace Aurocore.CMS.IServices
{
    /// <summary>
    /// 定義文章分類服務介面
    /// </summary>
    public interface IArticlecategoryService:IService<Articlecategory,ArticlecategoryOutputDto, string>
    {

        /// <summary>
        /// 獲取章分類適用於Vue 樹形列表，關鍵詞為空時獲取所有
        /// <param name="keyword">名稱關鍵詞</param>
        /// </summary>
        /// <returns></returns>
        Task<List<ArticlecategoryOutputDto>> GetAllArticlecategoryTreeTable(string keyword);


        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="ids">主鍵Id集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        CommonResult DeleteBatchWhere(DeletesInputDto ids, IDbTransaction trans = null);
        /// <summary>
        /// 非同步按條件批量刪除
        /// </summary>
        /// <param name="ids">主鍵Id集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        Task<CommonResult> DeleteBatchWhereAsync(DeletesInputDto ids, IDbTransaction trans = null);
    }
}
