using System;
using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.IDbContext;

namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 文章分類倉儲介面的實現
    /// </summary>
    public class ArticlecategoryRepository : BaseRepository<Articlecategory, string>, IArticlecategoryRepository
    {
		public ArticlecategoryRepository()
        {
        }

        public ArticlecategoryRepository(IDbContextCore context) : base(context)
        {
        }
    }
}