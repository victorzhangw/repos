using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Dapper;

namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class TradeDataRepository : BaseRepository<TradeData, string>, ITradeDataRepository
    {
		public TradeDataRepository()
        {
        }

        public TradeDataRepository(IDbContextCore context) : base(context)
        {
        }

        TradeData ITradeDataRepository.GetTradeData(string EstimateId)
        {
            string sql = string.Format("SELECT * FROM BTC_TradeData WHERE EstimateId='{0}' ", EstimateId);
            TradeData estimate = DapperConn.QueryFirst<TradeData>(sql);
            return estimate;
        }

        PageResult<TradeData> ITradeDataRepository.GetTradeDatas(string top)
        {
            throw new NotImplementedException();
        }
    }
}