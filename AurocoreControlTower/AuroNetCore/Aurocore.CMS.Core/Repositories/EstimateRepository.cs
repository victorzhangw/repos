using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Dapper;
using System.Linq;
using System.Collections.Generic;
using Aurocore.CMS.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Dtos;
using Aurocore.AspNetCore.Models;
using System.Reflection;

namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class EstimateRepository : BaseRepository<Vehicle_Estimate, string>, IEstimateRepository
    {
		public EstimateRepository()
        {
        }

        public EstimateRepository(IDbContextCore context) : base(context)
        {
        }

        public Vehicle_Estimate GetVehicleEstimate(string EstimateId)
        {
            string _woid = string.Empty;
            string sql = string.Format("SELECT * FROM BTC_Estimate WHERE EstimateId='{0}' ", EstimateId);
            string wosql = string.Format("SELECT Id,Attachment from CRM_WorkOrder WHERE SourceId='{0}' ", EstimateId);
            Vehicle_Estimate estimate = DapperConn.QueryFirst<Vehicle_Estimate>(sql);
             var _wo = DapperConn.QueryFirstOrDefault<WorkorderModel>(wosql);
            if (_wo == null)
            {
                estimate.WorkOrderId = "";
               
            }
            else
            {
                estimate.WorkOrderId = _wo.Id;
                estimate.Attachment = _wo.Attachment;
            }
            
            return estimate;
        }

        public PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Vehicle_Estimate> search)
        {
            string _where = string.Empty;
            List<string> condtions = new List<string>();
            if (search.Filter != null)
            {
                foreach (PropertyInfo pi in search.Filter.GetType().GetProperties())
                {
                    var _val = (pi.GetValue(search.Filter,null)) as string;
                    if (!string.IsNullOrEmpty(_val))
                    {
                        
                        if (pi.Name == "Id")
                        {
                            break;
                        }
                        if(pi.Name== "IsCheck")
                        {
                            if ((_val)=="0")
                                condtions.Add("A.IsCheckCase=0");
                        }else if (pi.Name == "PayStatus")
                        {
                            string _tmpVal = $"B.{pi.Name}='{_val}'";
                            condtions.Add(_tmpVal);
                        }
                        else
                        {
                            string _tmpVal = $"A.{pi.Name}='{_val}'";
                            condtions.Add(_tmpVal);
                        }
                        
                    }
                }
                if(condtions.Count > 0)
                {
                    _where = "WHERE " + string.Join(" AND ", condtions.ToArray());
                }
                else
                {
                    _where = "WHERE 1=1";
                }
                

            }
            else
            {
                _where=string.Empty;
            }
            string sql = $@"SELECT  A.*,B.PayStatus FROM BTC_Estimate A INNER JOIN 
                BTC_TradeData B ON A.EstimateId = B.EstimateId {_where}";
             





            List<Vehicle_Estimate> result = DapperConn.Query<Vehicle_Estimate>(sql).ToList();
            PageResult<EstimateOutputDto> pageResult = new PageResult<EstimateOutputDto>
            
            {
                CurrentPage = 1,
                Items = result.MapTo<EstimateOutputDto>(),
                ItemsPerPage = 1,
                
                TotalItems = result.Count
                
            };
            pageResult.Success = true;
            pageResult.ErrCode = ErrCode.successCode;
            pageResult.RelationData = "訂單號碼,保單號碼,保單起始日,保單結束日,要保人Id,要保人姓名,被保人Id,被保人姓名,客戶備註,管理者備註,車牌號碼";
            return pageResult;
        }
        public List<Vehicle_Estimate> GetVehicleEstimatesbyPid(string Pid)
        {


            string sql = string.Format("SELECT * FROM BTC_Estimate WHERE ApplicantID='{0}'", Pid);
            List<Vehicle_Estimate> result = DapperConn.Query<Vehicle_Estimate>(sql).ToList();
           
            return result;
        }
        public List<Vehicle_Estimate> GetVehicleEstimatesbyPlateNo(string PlateNo)
        {


            string sql = string.Format("SELECT * FROM BTC_Estimate WHERE TagId='{0}'", PlateNo);
            List<Vehicle_Estimate> result = DapperConn.Query<Vehicle_Estimate>(sql).ToList();

            return result;
        }
        public List<Vehicle_Estimate> GetVehicleEstimatesbyPhoneNo(string PhoneNo)
        {


            string sql = string.Format("SELECT * FROM BTC_Estimate WHERE ApplicantMobilPhone='{0}' or AssuredMobilPhone='{0}'", PhoneNo);
            List<Vehicle_Estimate> result = DapperConn.Query<Vehicle_Estimate>(sql).ToList();

            return result;
        }
        public List<Vehicle_Estimate> GetVehicleEstimatesbyEstimateId(string EstimateId)
        {


            string sql = string.Format("SELECT * FROM BTC_Estimate WHERE Id='{0}' ", EstimateId);
            List<Vehicle_Estimate> result = DapperConn.Query<Vehicle_Estimate>(sql).ToList();

            return result;
        }
        private class WorkorderModel
        {
            public string Id { get; set; }
            public string Attachment { get; set; }
        }

    }
}