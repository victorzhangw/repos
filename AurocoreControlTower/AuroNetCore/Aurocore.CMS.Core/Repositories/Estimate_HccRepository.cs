using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Dapper;
namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Estimate_HccRepository : BaseRepository<Estimate_Hcc, string>, IEstimate_HccRepository
    {
		public Estimate_HccRepository()
        {
        }

        public Estimate_HccRepository(IDbContextCore context) : base(context)
        {
        }

        Estimate_Hcc IEstimate_HccRepository.GetEstimateHcc(string EstimateId)
        {
            string sql = string.Format("SELECT * FROM BTC_Estimate_Hcc WHERE EstimateId='{0}' ", EstimateId);
            Estimate_Hcc estimate = DapperConn.QueryFirst<Estimate_Hcc>(sql);
            return estimate;
        }

        PageResult<Estimate_Hcc> IEstimate_HccRepository.GetEstimateHccs(string top)
        {
            throw new NotImplementedException();
        }
    }
}