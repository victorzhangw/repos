using System;

using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.IDbContext;

namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 文章倉儲介面的實現
    /// </summary>
    public class ArticlenewsRepository : BaseRepository<Articlenews, string>, IArticlenewsRepository
    {
		public ArticlenewsRepository()
        {
        }

        public ArticlenewsRepository(IDbContextCore context) : base(context)
        {
        }
    }
}