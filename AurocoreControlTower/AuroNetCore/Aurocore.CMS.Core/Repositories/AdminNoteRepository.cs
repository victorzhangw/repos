using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Aurocore.CMS.Dtos;
using Newtonsoft.Json.Converters;
using System.Dynamic;

namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class AdminNoteRepository : BaseRepository<Vehicle_Estimate, string>, IAdminNoteRepository
    {
		public AdminNoteRepository()
        {
        }

        public AdminNoteRepository(IDbContextCore context) : base(context)
        {
        }

        public  List<AdminNote> GetAdminNotes(string EstimateId)
        {
            List<AdminNote> notes = new List<AdminNote>();
            string _woId = string.Empty;
            string _wodId = string.Empty;
            string _woSql = string.Format("SELECT Id FROM CRM_WorkOrder WHERE SourceId ='{0}' ", EstimateId);
             _woId=  DapperConn.QueryFirstOrDefault<string>(_woSql);
            if (!string.IsNullOrEmpty(_woId)){
                string _wodSql = string.Format("SELECT Id  FROM CRM_WorkOrderDetail where WorkOrderId='{0}' AND ResponseTypeId='2206163508106'", _woId);
                 _wodId =  DapperConn.QueryFirstOrDefault<string>(_wodSql);
                if (!string.IsNullOrEmpty(_wodId)) {
                    string wologSql = $@"SELECT a.Id,a.CreatorTime,a.ReplyMessage , RealName FROM CRM_WorkOrderLog AS a INNER JOIN
                                     Sys_User as b ON  a.CreatorUserId =b.Id
                                     WHERE 
                                    WorkOrderId='{_woId}' AND WorkOrderDetailId='{_wodId}' AND LogTypeName='管理者備註'";

                    var Wologs = DapperConn.Query<WoLog>(wologSql).ToList();
                    if (Wologs.Count > 0) {

                        foreach (var Wolog in Wologs) {
                            AdminNote adminNote_Log = new AdminNote();
                            var converter = new ExpandoObjectConverter();
                            var _message = JsonConvert.DeserializeObject<ExpandoObject>(Wolog.ReplyMessage, converter) as dynamic;
                          // adminNote_Log = JsonConvert.DeserializeObject<AdminNote>();
                            if (_message != null) { 
                                adminNote_Log.Id = Wolog.Id;
                                adminNote_Log.Body = _message.Body;
                                adminNote_Log.CreatorUser = Wolog.RealName;
                                adminNote_Log.CreatorTime = Wolog.CreatorTime;
                            }
                            notes.Add(adminNote_Log);
                        }
                    }

                }
            }


            notes = notes.OrderByDescending(x => x.CreatorTime).ToList();
            return notes;
        }
        class WoLog
        {
            public string Id { get; set; }
            public string ReplyMessage { get; set; }
            public DateTime CreatorTime { get; set; }
            public string RealName { get; set; }
        }
       

    }
}