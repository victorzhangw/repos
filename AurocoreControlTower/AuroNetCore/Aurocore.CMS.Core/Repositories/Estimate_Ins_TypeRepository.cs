using System;
using Aurocore.Commons.IDbContext;
using Aurocore.Commons.Repositories;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Dapper;
using System.Linq;
using System.Collections.Generic;

namespace Aurocore.CMS.Repositories
{
    /// <summary>
    /// 倉儲接口的實現
    /// </summary>
    public class Estimate_Ins_TypeRepository : BaseRepository<Estimate_Ins_Type, string>, IEstimate_Ins_TypeRepository
    {
		public Estimate_Ins_TypeRepository()
        {
        }

        public Estimate_Ins_TypeRepository(IDbContextCore context) : base(context)
        {
        }

        List<Estimate_Ins_Type> IEstimate_Ins_TypeRepository.GetEstimateType(string EstimateId)
        {
            string sql = string.Format("SELECT * FROM BTC_Estimate_Ins_Type WHERE EstimateId='{0}' ", EstimateId);
            List<Estimate_Ins_Type> estimate = DapperConn.Query<Estimate_Ins_Type>(sql).ToList();
            return estimate;
        }

        PageResult<Estimate_Ins_Type> IEstimate_Ins_TypeRepository.GetEstimateTypes(string top)
        {
            throw new NotImplementedException();
        }
    }
}