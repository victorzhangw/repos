using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_OutputDto
    {
        private string _device;
        private string _policyPostWayName;
        private string _reNewCompelPolicyId;
        private string _reNewChoicePolicyId;
   
        /// <summary>
        /// 處理狀態
        /// </summary>
        public string DealStatus { get; set; }
        
        /// <summary>
        /// 要保書 ID 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 簽單日期 
        /// </summary>
        public string PolicyDate { get; set; }

        /// <summary>
        /// 要保日期
        /// </summary>
        public string ApplicantDate { get; set; }

        /// <summary>
        /// 強制證號 
        /// </summary>
        public string CompelCardId { get; set; }
        /// <summary>
        /// 強制承保起期 
        /// </summary>
        public string CompelDateBegin { get; set; }

        /// <summary>
        /// 強制承保迄期 
        /// </summary>
        public string CompelDateEnd { get; set; }

        /// <summary>
        /// 任意承保起期 
        /// </summary>
        public string ChoiceDateBegin { get; set; }

        /// <summary>
        /// 任意承保迄期 
        /// </summary>
        public string ChoiceDateEnd { get; set; }
        /// <summary>
        /// 保單狀態 
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 轉檔狀態 
        /// </summary>
        public string TransferStatus { get; set; }

        /// <summary>
        /// 總保費 
        /// </summary>
        public string TotalPremium { get; set; }

        /// <summary>
        /// 保單寄送方式 
        /// </summary>
        public string PolicyPostWay 
        {
            get { return _policyPostWayName; }
            set {

                switch (value)
                {
                    case "1":
                        _policyPostWayName = "平信";
                    break;

                    case "2":
                    _policyPostWayName = "限時";
                    break;

                    case "3":
                        _policyPostWayName = "掛號";
                        break;
                    case "4":
                        _policyPostWayName = "限時掛號";
                        break;
                }
            
            }
        }
        /// <summary>
        /// 保單實際委外廠商寄送時間 
        /// </summary>
        public string PolicyOutTrans { get; set; }
        /// <summary>
        /// 電子保單寄送 Email 
        /// </summary>
        public string EPolicyEmail { get; set; }
        /// <summary>
        /// 電子保單寄送時間
        /// </summary>
        public string EPolicyEmailoutTrans { get; set; }

        /// <summary>
        /// 投保組合
        /// </summary>
        public string PackageId { get; set; }
        /// <summary>
        /// 投保組合是否一致
        /// </summary>
        public bool IsPackageId { get; set; }
        /// <summary>
        /// 核保備註 
        /// </summary>
        public string CheckNote { get; set; }

        /// <summary>
        /// 核保類型 
        /// </summary>
        public string CheckType { get; set; }

        /// <summary>
        /// 是否為續保單 
        /// </summary>
        public bool IsRenewCompelPolicy { get; set; } = false;

        /// <summary>
        /// 強制續保單號 
        /// </summary>
        public string RenewCompelPolicyId {

            get { return _reNewCompelPolicyId; }
            set { _reNewCompelPolicyId = value ; }
        }
        /// <summary>
        /// 任意續保單號 
        /// </summary>
        public string RenewChoicePolicyId {
            get { return _reNewChoicePolicyId; }
            set { _reNewChoicePolicyId = value; }

        }
        /// <summary>
        /// 客戶備註 
        /// </summary>
        public string CustomerNote { get; set; }


        /// <summary>
        /// OCR 辨識異常原因 
        /// </summary>
        public string MvdisJson { get; set; }
        /// <summary>
        /// 職團通路代號
        /// </summary>
        public string ChRouter { get; set; }
        /// <summary>
        /// 職團通路經辦代號
        /// </summary>
        public string ChOfficerId { get; set; }
        /// <summary>
        /// 職團通路管理人
        /// </summary>
        public string ChLeader { get; set; }
        /// <summary>
        /// 職團通路業績單位
        /// </summary>
        public string ChQuotaId { get; set; }
        /// <summary>
        /// 活動代碼
        /// </summary>
        public string ActivityCode { get; set; }
        /// <summary>
        /// 來源裝置
        /// </summary>
        public string Device
        {
            get { return _device; }
            set
            {

                switch (value)
                {
                    case "1":
                        _device = "桌機";
                        break;

                    case "2":
                        _device = "手機";
                        break;

                    case "3":
                        _device = "平版";
                        break;
                  
                }

            }
        }
          


    }
}
