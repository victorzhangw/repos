
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 被保險人資料
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Applicant_OutputDto
    {

        public Realtion_Vehicle_Applicant_OutputDto()
        {
            this.applicantMaster = new ApplicantMaster();
            this.applicantMaster.ColumnHeader = new string[] { "姓名", "身份證字號", "出生日期", "行動電話","Email"};
            this.ApplicantDetails = new List<Dictionary<string, string>>();
        }
        /// <summary>
        /// 表格欄位
        /// </summary>
        
        public ApplicantMaster applicantMaster { get; set; }
        /// <summary>
        /// 欄位值
        /// </summary>
       
        /// <summary>
        /// 被保險人詳細資料
        /// </summary>
        public List<Dictionary<string,string>> ApplicantDetails { get; set; }
       






    }
    public class ApplicantMaster
    {
        public string[] ColumnHeader { get; set; }
        public string ApplicantName { get; set; }
        public string ApplicantID { get; set; }
        public string ApplicantBirthDay { get; set; }
        public string ApplicantMobilPhone { get; set; }
        public string ApplicantEmail { get; set; }
    }
}
