
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 被保險人資料
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Registration_OutputDto
    {

        public Realtion_Vehicle_Registration_OutputDto()
        {
            this.registrationMaster = new RegistrationMaster();
            this.registrationMaster.ColumnHeader = new string[] { "車輛種類", "車牌號碼", "出廠年月", "排氣量", "發照日期" };
            this.RegistrationDetails = new List<Dictionary<string, string>>();
        }
        
        /// <summary>
        /// 欄位值
        /// </summary>
        public RegistrationMaster registrationMaster { get; set; }
        /// <summary>
        /// 被保險人詳細資料
        /// </summary>
        public List<Dictionary<string, string>> RegistrationDetails { get; set; }







    }
    public class RegistrationMaster
    {
        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }
        public string CarType { get; set; }
        //拍照號碼
        public string TagId { get; set; }
        //出廠日期
        public string ProductDate { get; set; }
        // 排氣量
        public string Cylinder { get; set; }
        //發照日期
        public string IssueDate { get; set; }
        
    }
}