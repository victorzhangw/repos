
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 被保險人資料
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Assured_OutputDto
    {

        public Realtion_Vehicle_Assured_OutputDto()
        {
            this.assuredMaster = new AssuredMaster();
            this.assuredMaster.ColumnHeader = new string[] { "姓名", "身份證字號", "出生日期", "行動電話","Email"};
            this.AssuredDetails = new List<Dictionary<string, string>>();
        }
        
        
        public AssuredMaster assuredMaster { get; set; }

        /// <summary>
        /// 被保險人詳細資料
        /// </summary>
        public List<Dictionary<string,string>> AssuredDetails { get; set; }
       






    }
    public class AssuredMaster
    {
        public string[] ColumnHeader { get; set; }
        public string AssuredName { get; set; }
        public string AssuredId { get; set; }
        public string AssuredBirthDay { get; set; }
        public string AssuredMobilPhone { get; set; }
        public string AssuredEmail { get; set; }
    }
}
