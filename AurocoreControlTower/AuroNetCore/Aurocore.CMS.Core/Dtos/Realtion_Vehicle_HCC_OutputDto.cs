
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備係數註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_HCC_OutputDto
    {

        public Realtion_Vehicle_HCC_OutputDto()
        {
            this.hccMaster = new HccMaster();
            this.hccMaster.ColumnHeader = new string[] { "主通路代號", "費率生效日", "等級係數", "評分" };
            this.HCCDetails = new List<Dictionary<string, string>>();
        }
        /// <summary>
        /// 表格欄位
        /// </summary>
       



        public HccMaster hccMaster  { get; set; }
        public List<Dictionary<string, string>> HCCDetails { get; set; }
    }
    public class HccMaster
    {
        public string[] ColumnHeader { get; set; }
        public string RouteMain { get; set; }
        public string RateDate { get; set; }
        public string Level { get; set; }
        public string Score { get; set; }
    }
    
}
