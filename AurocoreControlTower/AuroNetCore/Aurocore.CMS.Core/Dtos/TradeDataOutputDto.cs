using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class TradeDataOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsCategory { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string MerchOID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string LogDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Amount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PayWay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PayStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PayDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PayUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string TradeSuccess { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string OrderId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PaidUpAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PaidUpSerial { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Properties { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AllowHolders { get; set; }


    }
}
