using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Detail_OutputDto
    {

        public Realtion_Vehicle_Detail_OutputDto()
        {
            this.DealStatus = new Dictionary<string, string>();
            this.EstimateId = new Dictionary<string, string>();
            this.PolicyDate = new Dictionary<string, string>();
            this.ApplicantDate = new Dictionary<string, string>();
            this.CompelCardId = new Dictionary<string, string>();
            this.CompelDateBeginEnd = new Dictionary<string, string>();
            
            this.ChoiceDateBeginEnd = new Dictionary<string, string>();
            
            this.Status = new Dictionary<string, string>();
            this.TransferStatus = new Dictionary<string, string>();
            this.TotalPremium = new Dictionary<string, string>();
            this.PolicyPostWay = new Dictionary<string, string>();
            this.PolicyOutTrans = new Dictionary<string, string>();
            this.EPolicyEmail = new Dictionary<string, string>();
            this.EPolicyEmailoutTrans = new Dictionary<string, string>();
            this.PackageId = new Dictionary<string, string>();
            this.IsPackageId = new Dictionary<string, bool>();
            this.CheckType = new Dictionary<string, string>();
            this.IsRenewCompelPolicy = new Dictionary<string, bool>();
            this.RenewCompelPolicyId = new Dictionary<string, string>();
            this.RenewChoicePolicyId = new Dictionary<string, string>();
            this.CustomerNote = new Dictionary<string, string>();
            this.MvdisJson = new Dictionary<string, string>();
            this.ChRouter = new Dictionary<string, string>();
            this.ChOfficerId = new Dictionary<string, string>();
            this.ChLeader = new Dictionary<string, string>();
            this.ChQuotaId = new Dictionary<string, string>();
            this.ActivityCode = new Dictionary<string, string>();
            this.Device = new Dictionary<string, string>();
            ColNumber = new int[] { 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };
            
        }
        public int[] ColNumber { get; set; }
        
        public Dictionary<string, string> DealStatus { get; set; }
        public Dictionary<string, string> EstimateId { get; set; }
        public Dictionary<string, string> PolicyDate { get; set; }
        public Dictionary<string, string> ApplicantDate { get; set; }
        public Dictionary<string, string> CompelCardId { get; set; }
        public Dictionary<string, string> CompelDateBeginEnd { get; set; }
        public Dictionary<string, string> ChoiceDateBeginEnd { get; set; }
        public Dictionary<string, string> Status { get; set; }
        public Dictionary<string, string> TransferStatus { get; set; }
        public Dictionary<string, string> TotalPremium { get; set; }
        public Dictionary<string, string> PolicyPostWay { get; set; }
        public Dictionary<string, string> PolicyOutTrans { get; set; }
        public Dictionary<string, string> EPolicyEmail { get; set; }
        public Dictionary<string, string> EPolicyEmailoutTrans { get; set; }
        public Dictionary<string, string> PackageId { get; set; }
        public Dictionary<string, bool> IsPackageId { get; set; }
        public Dictionary<string, string> CheckType { get; set; }
        public Dictionary<string, bool> IsRenewCompelPolicy { get; set; }
        public Dictionary<string, string> RenewCompelPolicyId { get; set; }
        public Dictionary<string, string> RenewChoicePolicyId { get; set; }
        public Dictionary<string, string> CustomerNote { get; set; }
        public Dictionary<string, string> MvdisJson { get; set; }
        public Dictionary<string, string> ChRouter { get; set; }
        public Dictionary<string, string> ChOfficerId { get; set; }
        public Dictionary<string, string> ChLeader { get; set; }
        public Dictionary<string, string> ChQuotaId { get; set; }
        public Dictionary<string, string> ActivityCode { get; set; }
        public Dictionary<string, string> Device { get; set; }
        




    }
    
}
