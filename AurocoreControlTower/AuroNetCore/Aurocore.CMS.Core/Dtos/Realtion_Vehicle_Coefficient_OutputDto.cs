
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備係數註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Coefficient_OutputDto
    {
        public Realtion_Vehicle_Coefficient_OutputDto()
        {
            this.coefficients = new List<Dictionary<string, string>>();
        }

        /// <summary>
        /// 係數字典
        /// </summary>
        public List<Dictionary<string,string>> coefficients { get; set; }
       
       





    }
}
