using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Dtos
{
    public class CMSProfile : Profile
    {
        public CMSProfile()
        {
           CreateMap<Articlecategory, ArticlecategoryOutputDto>();
           CreateMap<ArticlecategoryInputDto, Articlecategory>();
           CreateMap<Articlenews, ArticlenewsOutputDto>();
           CreateMap<ArticlenewsInputDto, Articlenews>();
		   CreateMap<Ca_Ea_Pst, Ca_Ea_PstOutputDto>();
           CreateMap<Ca_Ea_PstInputDto, Ca_Ea_Pst>();
           CreateMap<Vehicle_Estimate, EstimateOutputDto>();
           CreateMap<Vehicle_Estimate, EstimateListOutputDto>();
           CreateMap<EstimateInputDto, Vehicle_Estimate>();
           CreateMap<Estimate_Hcc, Estimate_HccOutputDto>();
           CreateMap<Estimate_HccInputDto, Estimate_Hcc>();
           CreateMap<Estimate_Ins_Type, Estimate_Ins_TypeOutputDto>();
           CreateMap<Estimate_Ins_TypeInputDto, Estimate_Ins_Type>();
           CreateMap<TradeData, TradeDataOutputDto>();
           CreateMap<TradeDataInputDto, TradeData>();
           CreateMap<Estimate_Hcc, Realtion_Vehicle_HCC_OutputDto>();
           CreateMap<Vehicle_Estimate, Realtion_Vehicle_OutputDto>();
            CreateMap<Vehicle_Estimate,Realtion_Vehicle_Applicant_OutputDto>();
            CreateMap<Vehicle_Estimate,Realtion_Vehicle_Assured_OutputDto>();
            
        }
    }
}
