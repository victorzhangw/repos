
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Uploads_OutputDto
    {

        public Realtion_Vehicle_Uploads_OutputDto()
        {
            this.ColumnHeader = new string[] {"No", "檔名", "上傳時間", "功能"};
            this.vehile_Uploads = new List<Vehile_Upload>();
           
        }
        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }

        /// <summary>
        /// 管理者備註（來源： Outbound 單）
        /// </summary>
        public List<Vehile_Upload>  vehile_Uploads{ get; set; }
       
        





    }
    public class Vehile_Upload
    {
        public string SerialNo { get; set; }
        public string FileName { get; set; }
        public string UploadDateTime { get; set; }
        public string FileUrl { get; set; }
        public string Id { get; set; }

    }
}
