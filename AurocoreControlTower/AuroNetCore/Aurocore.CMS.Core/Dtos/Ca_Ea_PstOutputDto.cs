using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Ca_Ea_PstOutputDto
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsurantId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsurantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsurantBirthday { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsurantAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsScope { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsurantCareerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsPremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CommissionRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CommissionMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AgentRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AgentMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DiscountRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BeneficiaryName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BeneficiaryRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RemedyDailyPay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RemedyInsPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RemedyPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BeneficiaryAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BeneficiaryTel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BeneficiaryZip { get; set; }


    }
}
