using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Ca_Ea_Pst))]
    [Serializable]
    public class Ca_Ea_PstInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantBirthday { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsScope { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsurantCareerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsPremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgentRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgentMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DiscountRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyDailyPay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyInsPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryTel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BeneficiaryZip { get; set; }


    }
}
