
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_ReIssue_OutputDto
    {

        public Realtion_Vehicle_ReIssue_OutputDto()
        {
            this.ColumnHeader = new string[] { "寄送 Email", "寄送時間", "附件" };
        }
        

        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }
        /// <summary>
        /// 核保備註（來源： 核保單）
        /// </summary>
        public List<ReIssueRecord>  reIssueRecords{ get; set; }


    }

    public class ReIssueRecord
    {
        /// <summary>
        /// 寄送 Email
        /// </summary>
        public string Email { get; set; }
        public string SendTime { get; set; }
       // public string Attachment { get; set; }
        
    }

}
