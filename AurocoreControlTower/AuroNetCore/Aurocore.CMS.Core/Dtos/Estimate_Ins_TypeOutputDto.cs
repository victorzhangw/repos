using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class Estimate_Ins_TypeOutputDto
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InjuredCover { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DeathCover { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DamageCover { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Deduct { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsPremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string IndirectPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string PurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Serial { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CommissionRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string CommissionMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AgentRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string AgentMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DiscountRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Prem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ProductCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string QDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ExpenseMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ExpenseDiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Provision { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string OtherInsComp { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string ExtraChargeRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string BusinessRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DrunkPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string DrunkPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string InsScope { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string RemedyDailyPay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(600)]
        public string Pas { get; set; }


    }
}
