using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Vehicle_Estimate))]
    [Serializable]
    public class EstimateInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string FK { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OtherCompanyCompelCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelDatePeriod { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsIns24IncludeCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelPolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewCompelPolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewCompelCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelCheck { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChoiceDatePeriod { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChoiceDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChoiceDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChoicePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewChoicePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewChoiceCardId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ProvisionD { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredIdExt { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredBirthDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredSex { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredMarriage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredTel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredTelNight { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredMobilPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredFax { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredDeputy { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredAssets { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredCntryRiskLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Assured_cntry { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredRiskLevelOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AssuredLicense { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string UserNames { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantBirthDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantSex { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantMobilPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantTelDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantTelNight { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantFax { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantDeputy { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantAssets { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantRelation { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantCntryRiskLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Applicant_cntry { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantRiskLevelOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantOccup { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TagId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BrandId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BrandName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BrandGroupId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarStyle { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IssueDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ProductYear { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ProductMonth { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarTypeName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Cylinder { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EngineId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BodyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarPrice { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TransportAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TransportUnitType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string VehicleModelName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsNew { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PowerType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OfficerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RouteId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RateType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RouteComm { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessFromType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SubCompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LeaderId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string QuotaId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionTargetId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Resource { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BrokerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BranchId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsContact { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ContactDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ContactUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ContactReason { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DealStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicantPostStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ExamineDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EntryDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string UpdateDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ReceiveFaxDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryResultCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumCompelPolicyInfo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryResultCompelPolicyInfo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SexAgeFactorCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LastCompelLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OtherCompanyCompelPolicyDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryLastCompelDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastCompelPolicyDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DrunkDrivingTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastCompelCarType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompelFirstDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastCompelCompany { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastChoicePolicyNum { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastChoicePolicyDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastChoicePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChoiceFirstDateBegin { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsExistPolicyDetail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarDamageProductYearRateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarDamageProductYearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryResultDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SexAgeFactorDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarDamageAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumOnceUnknownDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarOnceUnknownDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarOnceUnknownDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarOnceUnknownDamageAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumNoneUnknownDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarNoneUnknownDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarNoneUnknownDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CarNoneUnknownDamageAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string UnknownDamageFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumLastDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryDamageDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryDamagePolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryDamageInsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewTradevanQueryNumDamage { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewCarDamageAccidentLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RenewCarDamageAccidentFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DamageAccidentCloseTimesFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DamageDutyAndCountFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Damage_Acc_1 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Damage_Acc_2 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Damage_Acc_3 { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BurglaryProductYearRateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BurglaryProductYearFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumTheft { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryTheftDateEnd { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanLastTheftPolicyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SexAgeFactorLiability { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaFactor { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryResultLia { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradevanQueryNumLia { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaAccidentTimesInThreeYears { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaAccidentTimesWithinYear { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaAccidentTimesFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LiaDutyAndCountFiveYearsAgo { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PremiumChoice { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PremiumCompel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ReadyMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string StableMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CompensationMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ResearchMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InvestMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessRuleMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AdjustPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CapitalMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MaintainMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsDiscount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Nequipment { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CalculateTimeUnit { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MisEntryDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplyDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyOutTrans { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PaymentAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PaymentZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyAreaCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessCompany { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessRoute { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessSales { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsPolicyPostOutsourcing { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyPostWay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyPostZip { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyPostAddress { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyPostRecipient { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EPolicyEmail { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TermType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CancelEstimateUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CancelEstimateDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LastEstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsCheck { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CheckDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CheckUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CheckReason { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CustomerCheckReason { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CheckNote { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CheckType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SentMailDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SentMailReceiver { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SentMailReceiverName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SentMailReceiverPhone { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChOfficerId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChQuotaId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChLeader { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ChRouter { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Corp { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CorpName { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MvdisStatusCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MvdisJson { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MvdisImgPath { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsCategory { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ProjectID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CustomerID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PromotionID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ActivityCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CustomerNote { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ManagerNote { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ProjectAttribute { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string SourceIP { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PackageId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PromoPackageId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Device { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PersonalNoteDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsuranceNoteDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ServiceNoteDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PolicyEntry { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ApplicationReminder { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TransferDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CashierId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PublicWelfare { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ActivityReminder { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Lottery { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IsOtherRoute { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Depreciate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ACCUSerialNumber { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string VirtualHCCLevel { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string VirtualHCCScore { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string KYCDate { get; set; }
        /// <summary>
        /// 設定或獲取 是否轉為核保案件
        /// </summary>
        public bool IsCheckCase { get; set; }


    }
}
