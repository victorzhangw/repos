
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 核保備註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Check_OutputDto
    {
        public Realtion_Vehicle_Check_OutputDto()
        {
            this.ColumnHeader = new string[] { "單據編號", "建立時間", "照會原因","備註內容","審核結果", "車牌號碼" };
            this.CheckHint = new List<string>();
        }
        /// <summary>
        /// 核保提示
        /// </summary>
        public List<string> CheckHint { get; set; }
       
        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }
        /// <summary>
        /// 核保備註（來源： 核保單）
        /// </summary>
        public List<CheckNote> checkNotes{ get; set; }

    }
    public class CheckNote
    {
        /// <summary>
        /// WorkOrderLog Id
        /// </summary>
        public string Id { get; set; }
        public DateTime? CreatorTime { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public bool Status { get; set; }
        public string CreatorUser { get; set; }

    }
}
