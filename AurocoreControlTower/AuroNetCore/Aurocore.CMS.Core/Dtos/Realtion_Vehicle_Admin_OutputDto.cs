
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Admin_OutputDto
    {

        public Realtion_Vehicle_Admin_OutputDto()
        {
           
            this.ColumnHeader = new string[] { "單據編號", "建立時間", "備註內容", "建立人員"};
        }
        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }

        /// <summary>
        /// 管理者備註（來源： Outbound 單）
        /// </summary>
        public List<AdminNote> AdminNotes { get; set; }
        






    }
    public class AdminNote
    {
        /// <summary>
        /// WorkOrderLog Id
        /// </summary>
        public string Id { get; set; }
        public DateTime? CreatorTime { get; set; }
        public string Body { get; set; }
        public string CreatorUser { get; set; }

    }
}
