
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_Desc_OutputDto
    {

        public Realtion_Vehicle_Desc_OutputDto()
        {
            this.ColumnHeader = new string[] { "保險種類", "保額", "自付額", "保費"};
            this.Vehicle_Descs = new List<Vehicle_Desc>();
        }
        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }

        /// <summary>
        /// 管理者備註（來源： Outbound 單）
        /// </summary>
        public List<Vehicle_Desc>  Vehicle_Descs{ get; set; }
       
        




    }
    public class Vehicle_Desc
    {
        public string PolicyCate { get; set; }
        public string[] PolicyCover { get; set; }
        public string Deduct { get; set; }
        public string Premium { get; set; }
    }

}
