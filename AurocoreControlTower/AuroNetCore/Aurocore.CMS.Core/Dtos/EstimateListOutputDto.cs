using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class EstimateListOutputDto
    {
        /// <summary>
        ///  訂單號碼
        /// </summary>
        [MaxLength(600)]
        public string EstimateId { get; set; }


        /// <summary>
        ///  保單號碼
        /// </summary>
        [MaxLength(600)]
        public string PolicyId { get; set; }

        /// <summary>
        ///  保單起始日-結束日
        /// </summary>
        [MaxLength(600)]
        public string PolicyDateBeginEnd { get; set; }

      

        /// <summary>
        /// 要保人Id 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantID { get; set; }

        /// <summary>
        /// 要保人姓名 
        /// </summary>
        [MaxLength(600)]
        public string ApplicantName { get; set; }

        /// <summary>
        ///  被保人 Id
        /// </summary>
        [MaxLength(600)]
        public string AssuredId { get; set; }



        /// <summary>
        /// 被保人姓名
        /// </summary>
        [MaxLength(600)]
        public string AssuredName { get; set; }

        /// <summary>
        /// 客戶備註
        /// </summary>
       public string CustomerNote { get; set; }
       /// <summary>
       /// 車牌號碼
       /// </summary>
       [MaxLength(50)]
        public string TagId { get; set; }
        /// <summary>
        /// 交易狀態
        /// </summary>
        [MaxLength(50)]
        public string Status { get; set; }
        





    }
}
