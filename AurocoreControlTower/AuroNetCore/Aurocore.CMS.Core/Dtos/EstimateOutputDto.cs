using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class EstimateOutputDto
    {
       
        /// <summary>
        ///  訂單號碼
        /// </summary>
        [MaxLength(100)]
        public string EstimateId { get; set; } = String.Empty;


        /// <summary>
        ///  保單號碼
        /// </summary>
        [MaxLength(100)]
        public string PolicyId { get; set; } = String.Empty;

        /// <summary>
        ///  保單起始日
        /// </summary>
        [MaxLength(100)]
        public string PolicyDateBegin { get; set; } = String.Empty;

        // <summary>
        ///  保單結束日
        /// </summary>
        [MaxLength(100)]
        public string PolicyDateEnd { get; set; } = String.Empty;
        /// <summary>
        /// 要保人Id 
        /// </summary>
        [MaxLength(100)]
        public string ApplicantID { get; set; } = String.Empty;

        /// <summary>
        /// 要保人姓名 
        /// </summary>
        [MaxLength(100)]
        public string ApplicantName { get; set; } = String.Empty;

        /// <summary>
        ///  被保人 Id
        /// </summary>
        [MaxLength(100)]
        public string AssuredId { get; set; } = String.Empty;



        /// <summary>
        /// 被保人姓名
        /// </summary>
        [MaxLength(100)]
        public string AssuredName { get; set; }= String.Empty;

        /// <summary>
        /// 客戶備註 
        /// </summary>
        [MaxLength(100)]
        public string CustomerNote { get; set; } = String.Empty;

        /// <summary>
        /// 管理者備註
        /// </summary>
        [MaxLength(100)]
        public string ManagerNote { get; set; } = String.Empty;

        /// <summary>
        /// 車牌號碼
        /// </summary>
        [MaxLength(50)]
        public string TagId { get; set; } = String.Empty;







    }
}
