using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurocore.Commons.Models;
using Aurocore.Commons.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(TradeData))]
    [Serializable]
    public class TradeDataInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsCategory { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string MerchOID { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string LogDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayWay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayStatus { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayDate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PayUser { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string TradeSuccess { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PaidUpAmount { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PaidUpSerial { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Properties { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AllowHolders { get; set; }


    }
}
