
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurocore.CMS.Dtos
{
    /// <summary>
    /// 管理者備係數註
    /// </summary>
    [Serializable]
    public class Realtion_Vehicle_TradeData_OutputDto
    {

        public Realtion_Vehicle_TradeData_OutputDto()
        {
            this.tradeDataMaster = new TradeDataMaster();
            this.tradeDataMaster.ColumnHeader = new string[] { "本地訂單編號", "交易時間", "金額", "付款方式", "付款狀態" ,"交易成功","繳費時間"};
            this.TradeDetails = new List<Dictionary<string, string>>();
        }
        



        public TradeDataMaster tradeDataMaster  { get; set; }
        public List<Dictionary<string, string>> TradeDetails { get; set; }
    }
    public class TradeDataMaster
    {
        /// <summary>
        /// 表格欄位
        /// </summary>
        public string[] ColumnHeader { get; set; }
        public string MerchOID { get; set; }
        public string LogDate { get; set; }
        public string Amount { get; set; }
        public string PayWay { get; set; }
        public string PayStatus { get; set; }
        public string TradeSuccess { get; set; }
        public string PayDate { get; set; }
    }
    
}
