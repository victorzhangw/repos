﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.CMS.Core.Enums
{
    public enum EstimateBlockType
    {
        // 表單
        Form =1,
        // 表格
        Table =2,
        // 含子表格
        TablewithChild=3


    }
}
