﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.CMS.Models
{
    public static class CmsStaticCode
    {
        /// <summary>
        /// 篩選器各險種 Id 
        /// </summary>
        public static string vehicleFilterPid = "2206147064760";
        /// <summary>
        /// 篩選器各險種處理狀態 Id
        /// </summary>
        public static string vehicleFilterProcess = "2206148226821";
        
         
        /// <summary>
        /// 狀態顏色碼
        /// </summary>
        public static string bgCode1 = "#ACEBA9";
        public static string textCode1 = "#000000";
        public static string bgCode2 = "#F8E091";
        public static string textCode2 = "#000000";
        public static string bgCode3 = "#FAC7C5";
        public static string textCode3 = "#000000";
        public static string bgCode4 = "#BBBBBB";
        public static string textCode4 = "#000000";
    }
}
