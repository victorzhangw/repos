using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using Aurocore.CMS.Dtos;

namespace Aurocore.CMS.Models
{
    /// <summary>
    /// 要保書資料實體物件與關聯資料
    /// </summary>

    [Serializable]
   
    public class EstimatesWithCustomer : EstimatewithRelations
    {
       
       
        
        public Base_Estimate_Customer Estimate_Customer { get; set; }
        public CustomerEstimates Estimates { get; set; }



    }
    public class CustomerEstimates
    {
        public CustomerEstimates()
        {
            this.StatusHeader = new string[] { "待處理", "尚未付款", "處理中", "退款", "已完成", "不處理/取消" };
            this.ColumnHeader = new string[] {
                "要保序號", "保單號碼", "保單起迄日", "要保人Id", "要保人姓名", "被保人Id", "被保人姓名", "客戶備註" , "被保人姓名" ,"交易狀態" };

        }
        public List<EstimateListOutputDto> Estimates { get; set; }
        public string[] StatusHeader { get; set; }
        public string[] ColumnHeader { get; set; }
    }
}
