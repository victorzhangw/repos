﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.CMS.Models
{
    [Serializable]
    public class AdminNote_Log
    {
        public string Id { get; set; }

        public DateTime CreatorTime { get; set; }
        public string Body { get; set; }
        
        public string CreatorUser { get; set; }
    }
}
