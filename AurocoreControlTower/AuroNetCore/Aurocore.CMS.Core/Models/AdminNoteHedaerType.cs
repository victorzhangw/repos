﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.CMS.Core.Models
{
    /// <summary>
    /// 管理者備註  各式後續標籤
    /// </summary>
    public static class  AdminNoteHedaerType
    {

        /// <summary>
        /// 核保審批
        /// </summary>
        public const string Header1 = "送核保";
        /// <summary>
        /// 外撥客戶
        /// </summary>
        public const string Header2 = "外撥客戶";
       

        /// <summary>
        /// 客戶撥入
        /// </summary>
        public const string Header3 = "客戶撥入";
        /// <summary>
        /// 結案
        /// </summary>
        public const string Header4 = "結案";

    }
}
