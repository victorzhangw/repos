using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using Aurocore.CMS.Dtos;

namespace Aurocore.CMS.Models
{
    /// <summary>
    /// 要保書資料實體物件與關聯資料
    /// </summary>

    [Serializable]
    public class EstimatewithRelations
    {
        public EstimatewithRelations()
        {
            this.InsuranceTypeID = "2021022314490279171796";
            this.ColShowFormats = new List<ColShowFormat>();
        }

        public string[] Anchors { get; set; }
        public string EstimateType{ get; set; }
        public string EstimateTypeName{ get; set; }
        public string EstimateDealStatus { get; set; }
        public string InsuranceTypeID{ get; set; }
        public string WorkOrderId{ get; set; }
        public List<ColShowFormat> ColShowFormats{ get; set; }
    

    }
    public class Vehiche_withRelations : EstimatewithRelations
    {
        public  Vehiche_withRelations()
        {
            this.Anchors = new string[] 
            {"客戶資料","訂單詳細資料", "核保備註", "管理者備註","電子保單寄發","被保人資料","要保人資料","車籍資料","險種詳細資料","上傳檔案","係數相關資料","HCC係數相關資料","交易資料"};
        }
        public Base_Estimate_Customer Estimate_Customer { get; set; }
        public Realtion_Vehicle_Detail_OutputDto Vehicle { get; set; }
        public Realtion_Vehicle_Check_OutputDto Check { get; set; }
        public Realtion_Vehicle_Admin_OutputDto Admin { get; set; }
        public Realtion_Vehicle_ReIssue_OutputDto ReIssue { get; set; }
        public Realtion_Vehicle_Assured_OutputDto Assured { get; set; }
        public Realtion_Vehicle_Applicant_OutputDto Applicant { get; set; }
        public Realtion_Vehicle_Registration_OutputDto Registration { get; set; }
        public Realtion_Vehicle_Desc_OutputDto Desc { get; set; }
        public Realtion_Vehicle_Uploads_OutputDto Uploads { get; set; }
        public Realtion_Vehicle_Coefficient_OutputDto Coefficient { get; set; }
        public Realtion_Vehicle_HCC_OutputDto HCC { get; set; }
        public Realtion_Vehicle_TradeData_OutputDto TradeData { get; set; }



    }
    public class ColShowFormat
    {
        public string BlockName { get; set; }
        public string ColName { get; set; }
        public string BgCode { get; set; }
        public string TextCode { get; set; }
    }
    public class ColEditType
    {
        public string BlockName { get; set; }
        public string ColName { get; set; }
        public string IsEditable { get; set; }
        
    }
}
