using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;

namespace Aurocore.CMS.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("BTC_Estimate_Ins_Type")]
    [Serializable]
    public class Estimate_Ins_Type:BaseEntity<string>
    {
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string EstimateId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsType { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InjuredCover { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DeathCover { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DamageCover { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Deduct { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsPremium { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string IndirectPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string PurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Serial { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string CommissionMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgentRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string AgentMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DiscountRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RateCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Prem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string QDay { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ExpenseMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ExpenseDiscountMoney { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string Provision { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string OtherInsComp { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string ExtraChargeRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string BusinessRate { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DrunkPrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string DrunkPurePrem { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string InsScope { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public string RemedyDailyPay { get; set; }

        /// <summary>
        ///  傷害險名冊檔
        /// Dictionary<string, ca_pa_est>
        /// </summary>
        public string Pas { get; set; }


    }
}
