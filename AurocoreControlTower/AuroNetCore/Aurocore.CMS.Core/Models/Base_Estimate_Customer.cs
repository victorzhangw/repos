using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;
using Aurocore.CMS.Core.Enums;

namespace Aurocore.CMS.Models
{
    /// <summary>
    /// 要保書資料實體物件與關聯資料
    /// </summary>
  
    [Serializable]
    public class Base_Estimate_Customer
    {
        public Base_Estimate_Customer()
        {
            this.Pid = new Dictionary<string, string>();
            this.Name = new Dictionary<string, string>();
            this.MobilePhone = new Dictionary<string, string>();
            this.Email = new Dictionary<string, string>();
            this.Address = new Dictionary<string, string>();
            this.Birthday = new Dictionary<string, string>();
            this.Tel = new Dictionary<string, string>();
            this.OTPRecord = new Dictionary<string, string>();
            this.Memo = new Dictionary<string, string>();
            this.ContactPersonName = new Dictionary<string, string>();
            this.ContactPersonPhone = new Dictionary<string, string>();
            this.ContactPersonRelation = new Dictionary<string, string>();
            this.CDPTags = new Dictionary<string, string>();
            this.CRMTags = new Dictionary<string, string>();
            ColNumber =new int[] { 2, 2, 2, 2,4,1,1,2,4,1,1,1,4,4};
        }
        public int[] ColNumber { get; set; }
        public Dictionary<string,string>  Pid { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> MobilePhone { get; set; }
        public Dictionary<string, string> Email { get; set; }
        public Dictionary<string, string> Address { get; set; }
        public Dictionary<string, string> Birthday { get; set; }
        public Dictionary<string, string> Tel { get; set; }
        public Dictionary<string, string> OTPRecord { get; set; }
        public Dictionary<string, string> Memo { get; set; }
        public Dictionary<string, string> ContactPersonName { get; set; }
        public Dictionary<string, string> ContactPersonPhone { get; set; }
        public Dictionary<string, string> ContactPersonRelation { get; set; }
        public Dictionary<string, string> CDPTags { get; set; }
        public Dictionary<string, string> CRMTags { get; set; }
        


    }
}
