﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurocore.CMS.Core.Models
{
    /// <summary>
    /// 核保備註 各式後續標籤
    /// </summary>
    public static class CheckNoteHedaerType
    {
        /// <summary>
        /// 免勘承保
        /// </summary>
        public const string Header1 = "免勘承保";
        /// <summary>
        /// 審核通過，客服後續追蹤
        /// </summary>
        public const string Header2 = "審核通過，客服後續追蹤";
        /// <summary>
        /// 資料有誤/不足
        /// </summary>
        public const string Header3 = "資料有誤/不足";
        /// <summary>
        /// 影響保額保費
        /// </summary>
        public const string Header4 = "影響保額保費";
        /// <summary>
        /// 限保業務
        /// </summary>
        public const string Header5 = "限保業務";
        /// <summary>
        /// 勘車承保
        /// </summary>
        public const string Header6 = "勘車承保";
        /// <summary>
        /// 重複投保
        /// </summary>
        public const string Header7 = "重複投保";

        
    }
        

        
    
}
