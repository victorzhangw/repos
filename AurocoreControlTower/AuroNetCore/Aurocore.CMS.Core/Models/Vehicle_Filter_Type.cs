﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurocore.Commons.Models;
namespace Aurocore.CMS.Models
{
    public class Vehicle_Filter_Type 
    {
        //策略聯盟
        public string project { get; set; }
        public string inscats { get; set; }
        //保單狀態
        public string insstatus { get; set; }
        public string insdeal { get; set; }
        //付款類別
        public string paymenttype { get; set; }
        //付款狀態
        public string paymentstatus { get; set; }
        public string insuploads { get; set; }
        public string insunaudit { get; set; }
        public string comdatec { get; set; }
        public string anydatec { get; set; }
        public string empid { get; set; }
        public string issend { get; set; }
        public string ismail { get; set; }
        public string takecats { get; set; }
        public string tradevanvehicle { get; set; }
        public string isrecomm { get; set; }
        public string srcdevice { get; set; }
        public string packageid { get; set; }
        public string busgrpsrc { get; set; }
        public string busgrpname { get; set; }
        public string iscoupon { get; set; }
        public string sortcol { get; set; }
        public string policyentry { get; set; }
        public string insappyoe { get; set; }
        public string insocrstatus { get; set; }
        public string isae { get; set; }
        public string insconstatus { get; set; }
        public string insnetpay { get; set; }
        public string insnetpaytype { get; set; }
        public string estimatestat { get; set; }
        public string assuredid { get; set; }
        public string assuredname { get; set; }
        public string plateno { get; set; }
        public string engineno { get; set; }
        public string applicantid { get; set; }
        public string applicantdate { get; set; }
        public string insdatebegin { get; set; }
        public string insyear { get; set; }
        public string estimateid { get; set; }
        public string activitycode { get; set; }
        public string inssigndate { get; set; }
    }
}
