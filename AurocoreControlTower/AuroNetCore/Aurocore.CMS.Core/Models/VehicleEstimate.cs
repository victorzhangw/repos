﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

using System.ComponentModel.DataAnnotations.Schema;
using Aurocore.Commons.Models;


namespace Aurocore.CMS.Models
{
    public class VehicleEstimate
    {
        public VehicleEstimate()
        {
            Estimate_Ins_Types = new List<Estimate_Ins_Type>();
            
        }
        public Vehicle_Estimate Estimate { get; set; }
        public List<Estimate_Ins_Type> Estimate_Ins_Types { get; set; }
        public Estimate_Hcc Estimate_Hcc { get; set; }
        public TradeData TradeData { get; set; }
    }
}
