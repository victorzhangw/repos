using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義文章分類倉儲介面
    /// </summary>
    public interface IArticlecategoryRepository:IRepository<Articlecategory, string>
    {
    }
}