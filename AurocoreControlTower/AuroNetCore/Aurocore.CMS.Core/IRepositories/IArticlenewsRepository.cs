using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義文章倉儲介面
    /// </summary>
    public interface IArticlenewsRepository:IRepository<Articlenews, string>
    {
    }
}