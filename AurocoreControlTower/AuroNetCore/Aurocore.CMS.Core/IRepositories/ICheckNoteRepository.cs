using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.CMS.Dtos;

namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義交易資料倉儲接口
    /// </summary>
    public interface ICheckNoteRepository : IRepository<Vehicle_Estimate, string>
    {

        /// <summary>
        /// 取得交易資料資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>

        List<CheckNote> GetCheckNotes(string EstimateId);
       
    }
}