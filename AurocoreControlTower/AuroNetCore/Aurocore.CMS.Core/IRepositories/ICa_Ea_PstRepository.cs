using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義傷害險名冊倉儲接口
    /// </summary>
    public interface ICa_Ea_PstRepository:IRepository<Ca_Ea_Pst, string>
    {

        /// <summary>
        /// 取得傷害險名冊資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<Ca_Ea_Pst> GetEstimates(string top);
        /// <summary>
        /// 取得傷害險名冊
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Ca_Ea_Pst GetEstimate(string EstimateId);
    }
}