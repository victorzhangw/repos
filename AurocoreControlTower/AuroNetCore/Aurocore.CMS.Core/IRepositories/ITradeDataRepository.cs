using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義交易資料倉儲接口
    /// </summary>
    public interface ITradeDataRepository:IRepository<TradeData, string>
    {
        /// <summary>
        /// 取得交易資料 資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<TradeData> GetTradeDatas(string top);
        /// <summary>
        /// 取得交易資料資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        
        TradeData GetTradeData(string EstimateId);
       
    }
}