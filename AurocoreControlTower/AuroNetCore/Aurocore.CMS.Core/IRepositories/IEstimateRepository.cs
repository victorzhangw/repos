using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
using Aurocore.CMS.Dtos;
using Aurocore.Commons.Dtos;
using System.Collections.Generic;

namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義車險主檔倉儲接口
    /// </summary>
    public interface IEstimateRepository:IRepository<Vehicle_Estimate, string>
    {
        /// <summary>
        /// 取得要保書資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Vehicle_Estimate> search);
        /// <summary>
        /// 取得要保書資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Vehicle_Estimate GetVehicleEstimate(string EstimateId);
        List<Vehicle_Estimate> GetVehicleEstimatesbyPid(string Pid);
        List<Vehicle_Estimate> GetVehicleEstimatesbyPlateNo(string PlateNo);
        List<Vehicle_Estimate> GetVehicleEstimatesbyPhoneNo(string PhoneNo);
        List<Vehicle_Estimate> GetVehicleEstimatesbyEstimateId(string EstimateID);
    }
}