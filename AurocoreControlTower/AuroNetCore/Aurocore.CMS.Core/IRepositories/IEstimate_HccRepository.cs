using System;
using Aurocore.Commons.IRepositories;
using Aurocore.CMS.Models;
using Aurocore.Commons.Pages;
namespace Aurocore.CMS.IRepositories
{
    /// <summary>
    /// 定義倉儲接口
    /// </summary>
    public interface IEstimate_HccRepository:IRepository<Estimate_Hcc, string>
    {
        /// <summary>
        /// 取得要保書 HCC 資料列表
        /// </summary>
        /// <param name="top">資料筆數</param>
        /// <returns></returns>
        PageResult<Estimate_Hcc> GetEstimateHccs(string top);
        /// <summary>
        /// 取得要保書 HCC資料
        /// </summary>
        /// <param name="EstimateId">要保序號</param>
        /// <returns></returns>
        Estimate_Hcc GetEstimateHcc(string EstimateId);
    }
}