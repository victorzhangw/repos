using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Estimate_HccService: BaseService<Estimate_Hcc,Estimate_HccOutputDto, string>, IEstimate_HccService
    {
		private readonly IEstimate_HccRepository _repository;
        public Estimate_HccService(IEstimate_HccRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}