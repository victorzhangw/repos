using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;
using Dapper;
using Aurocore.Commons.Dapper;
using System.Data.Common;
using Aurocore.Commons.Helpers;
using System.Reflection ;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Aurocore.Security.IRepositories;


namespace Aurocore.CMS.Services
{
    public static class Extensions
    {
        public static void Awesome<T>(this T myObject) where T : class
        {
            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (var info in properties)
            {
                

                // if a string and null, set to String.Empty
                if (info.PropertyType == typeof(string) &&
                   info.GetValue(myObject, null) == null)
                {
                    info.SetValue(myObject, String.Empty, null);
                }
               
              
            }
        }
    }
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class EstimateService: BaseService<Vehicle_Estimate,EstimateOutputDto, string>, IEstimateService
    {
		private readonly IEstimateRepository _repository;
        private readonly IEstimate_HccRepository _hccRepository;
        private readonly IEstimate_Ins_TypeRepository _ins_typeRepository;
        private readonly ITradeDataRepository _tradeDataRepository;
        private readonly IAdminNoteRepository _adminNoteRepository;
        private readonly ICheckNoteRepository _checkNoteRepository;
        private readonly IItemsDetailRepository _itemsDetailRepository;
        public string _dealStatus=string.Empty; /* 頁簽表頭需要的處理狀態 */
        private ColShowFormat colShowFormat = new(); /* 頁簽表頭需要的處理狀態 */ 
        public EstimateService(IEstimateRepository repository,
            IItemsDetailRepository itemsDetailRepository,
            IEstimate_HccRepository estimate_HccRepository, 
            IEstimate_Ins_TypeRepository estimate_Ins_TypeRepository,
            ITradeDataRepository tradeDataRepository, 
            IAdminNoteRepository adminNoteRepository, 
            ICheckNoteRepository checkNoteRepository) : base(repository)
        {
            _repository = repository;
            _hccRepository = estimate_HccRepository;
            _ins_typeRepository = estimate_Ins_TypeRepository;
            _tradeDataRepository = tradeDataRepository;
            _adminNoteRepository = adminNoteRepository;
            _checkNoteRepository = checkNoteRepository;
            _itemsDetailRepository = itemsDetailRepository;

        }

        public VehicleEstimate GetVehicleEstimate(string EstimateId)
        {
            VehicleEstimate vehicleEstimate = new VehicleEstimate();
            vehicleEstimate.Estimate = _repository.GetVehicleEstimate(EstimateId);
            vehicleEstimate.Estimate_Ins_Types = _ins_typeRepository.GetEstimateType(EstimateId);
            vehicleEstimate.TradeData= _tradeDataRepository.GetTradeData(EstimateId);
            vehicleEstimate.Estimate_Hcc= _hccRepository.GetEstimateHcc(EstimateId);

            return vehicleEstimate;
        }

        public PageResult<EstimateOutputDto> GetVehicleEstimates(SearchInputDto<Vehicle_Estimate> search)
        {
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            return _repository.GetVehicleEstimates(search);
        }
        public Vehiche_withRelations GetVehicleEstimatewithRealtions(string EstimateId)
        {
            Vehiche_withRelations vehiche_WithRelations = new Vehiche_withRelations();
            VehicleEstimate vehicleEstimate = new VehicleEstimate();
            vehicleEstimate.Estimate = _repository.GetVehicleEstimate(EstimateId);
            vehicleEstimate.Estimate_Ins_Types = _ins_typeRepository.GetEstimateType(EstimateId);
            vehicleEstimate.TradeData = _tradeDataRepository.GetTradeData(EstimateId);
            vehicleEstimate.Estimate_Hcc = _hccRepository.GetEstimateHcc(EstimateId);
            vehiche_WithRelations.WorkOrderId = vehicleEstimate.Estimate.WorkOrderId;
            #region 客戶資料
            vehiche_WithRelations.Estimate_Customer = Customer(vehicleEstimate);
            
            #endregion
            #region 訂單詳細資料
            vehiche_WithRelations.Vehicle = Vehicle(vehicleEstimate);
            #endregion
            #region 核保備註
            vehiche_WithRelations.Check = CheckNote(vehicleEstimate);
            #endregion
            #region 管理者備註
            vehiche_WithRelations.Admin = AdminNote(vehicleEstimate);
            #endregion
            #region 電子保單補發記錄
            vehiche_WithRelations.ReIssue = ReIssue(vehicleEstimate);
            #endregion
            #region 被保人資料
            vehiche_WithRelations.Assured = Assured(vehicleEstimate);
            #endregion
            #region 要保人資料
            vehiche_WithRelations.Applicant = Applicant(vehicleEstimate);
            #endregion
            #region 車籍資料
            vehiche_WithRelations.Registration = Registration(vehicleEstimate);
            #endregion
            #region 險種詳細資料
            vehiche_WithRelations.Desc = Desc(vehicleEstimate);
            #endregion
            #region 上傳檔案
            vehiche_WithRelations.Uploads = Uploads(vehicleEstimate);
            #endregion
            #region 係數相關
            vehiche_WithRelations.Coefficient = Coefficient(vehicleEstimate);
            #endregion
            #region HCC係數
            vehiche_WithRelations.HCC = HCC(vehicleEstimate);
            #endregion
            #region 交易
            vehiche_WithRelations.TradeData = TradeData(vehicleEstimate);
            #endregion
            vehiche_WithRelations.EstimateType = "Vehicle";
            vehiche_WithRelations.EstimateTypeName = "汽車險";
            vehiche_WithRelations.EstimateDealStatus = _dealStatus;
            vehiche_WithRelations.ColShowFormats.Add(colShowFormat);
            
            return vehiche_WithRelations;
        }
        #region 資料整形
        private Base_Estimate_Customer Customer(VehicleEstimate vehicleEstimate)
        {

            Base_Estimate_Customer result = new Base_Estimate_Customer();
            string sql = $@"SELECT Pid, Name ,MobilePhone ,Email,Address,Birthday,Tel,Memo,ContactPersonName,
                        ContactPersonPhone,ContactPersonRelation FROM BTC_Customer as A
                        Left JOIN
                         BTC_Customer_Contact as B    
                        ON A.CustomerID=  B.CustomerID    
                        WHERE Pid =@id";
            //取得客戶資料
           using (DbConnection conn = OpenSharedConnectionHelper.OpenConnection())
            {
                ECustomer eCustomer = conn.QueryFirst<ECustomer>(sql, new { id = vehicleEstimate.Estimate.ApplicantID });
                
                result.Pid.Add("ID/居留證號", eCustomer.Pid);
                result.Name.Add("客戶姓名", eCustomer.Name);
                result.MobilePhone.Add("手機號碼", eCustomer.MobilePhone);
                result.Email.Add("EMAIL", eCustomer.Email);
                result.Address.Add("地址", eCustomer.Address);
                result.Birthday.Add("出生日期", eCustomer.Birthday);
                result.Tel.Add("電話", eCustomer.Tel);
                result.Memo.Add("客戶特殊註記", eCustomer.Memo);
                result.ContactPersonName.Add("聯絡人姓名", eCustomer.ContactPersonName);
                result.ContactPersonPhone.Add("聯絡人電話", eCustomer.ContactPersonPhone);
                result.ContactPersonRelation.Add("聯絡人關係", eCustomer.ContactPersonRelation);
               

            }
            
            result.OTPRecord.Add("OTP發送記錄", "2022-07-11 04:53:11");
            result.CDPTags.Add("CDP標籤", "樂天;電商會員;MOMO;玉山分期;HCC50");
            result.CRMTags.Add("CRM標籤", "AAAA;BBBB;CCCC");
            var JsonStr = JsonConvert.SerializeObject(result, Formatting.None,
               new JsonSerializerSettings { ContractResolver = new JsonStringNullToEmpty() });
            result = JsonConvert.DeserializeObject<Base_Estimate_Customer>(JsonStr);
            return result;
        }
        private Base_Estimate_Customer Customer(Vehicle_Estimate vehicleEstimate)
        {

            Base_Estimate_Customer result = new Base_Estimate_Customer();
            string sql = $@"SELECT Pid, Name ,MobilePhone ,Email,Address,Birthday,Tel,Memo,ContactPersonName,
                        ContactPersonPhone,ContactPersonRelation FROM BTC_Customer as A
                        Left JOIN
                         BTC_Customer_Contact as B    
                        ON A.CustomerID=  B.CustomerID    
                        WHERE Pid =@id";
            //取得客戶資料
           using (DbConnection conn = OpenSharedConnectionHelper.OpenConnection())
            {
                ECustomer eCustomer = conn.QueryFirst<ECustomer>(sql, new { id = vehicleEstimate.ApplicantID });
                
                result.Pid.Add("ID/居留證號", eCustomer.Pid);
                result.Name.Add("客戶姓名", eCustomer.Name);
                result.MobilePhone.Add("手機號碼", eCustomer.MobilePhone);
                result.Email.Add("EMAIL", eCustomer.Email);
                result.Address.Add("地址", eCustomer.Address);
                result.Birthday.Add("出生日期", eCustomer.Birthday);
                result.Tel.Add("電話", eCustomer.Tel);
                result.Memo.Add("客戶特殊註記", eCustomer.Memo);
                result.ContactPersonName.Add("聯絡人姓名", eCustomer.ContactPersonName);
                result.ContactPersonPhone.Add("聯絡人電話", eCustomer.ContactPersonPhone);
                result.ContactPersonRelation.Add("聯絡人關係", eCustomer.ContactPersonRelation);
               

            }
            
            result.OTPRecord.Add("OTP發送記錄", "2022-07-11 04:53:11");
            result.CDPTags.Add("CDP標籤", "樂天;電商會員;MOMO;玉山分期;HCC50");
            result.CRMTags.Add("CRM標籤", "AAAA;BBBB;CCCC");
            var JsonStr = JsonConvert.SerializeObject(result, Formatting.None,
               new JsonSerializerSettings { ContractResolver = new JsonStringNullToEmpty() });
            result = JsonConvert.DeserializeObject<Base_Estimate_Customer>(JsonStr);
            return result;
        }
        private Realtion_Vehicle_Detail_OutputDto Vehicle(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_OutputDto _vehicle = new Realtion_Vehicle_OutputDto();
            _vehicle = vehicleEstimate.Estimate.MapTo<Realtion_Vehicle_OutputDto>();
            Realtion_Vehicle_Detail_OutputDto result = new Realtion_Vehicle_Detail_OutputDto();
            string _where = string.Format("ParentId='{0}' AND  ItemProperty2='{1}'", 
                CmsStaticCode.vehicleFilterProcess, _vehicle.DealStatus);
            var _item = _itemsDetailRepository.GetWhere(_where);
            //判斷處理狀態
            switch (_item.ItemProperty2Desc)
            {
                case "1":
                    colShowFormat.BgCode = CmsStaticCode.bgCode1;
                    colShowFormat.TextCode = CmsStaticCode.textCode1;
                    colShowFormat.BlockName = "Vehicle";
                    colShowFormat.ColName = "處理狀態";
                    

                    break;
                case "2":
                    colShowFormat.BgCode = CmsStaticCode.bgCode2;
                    colShowFormat.TextCode = CmsStaticCode.textCode2;
                    colShowFormat.BlockName = "Vehicle";
                    colShowFormat.ColName = "處理狀態";
                    break;
                case "3":
                    colShowFormat.BgCode = CmsStaticCode.bgCode3;
                    colShowFormat.TextCode = CmsStaticCode.textCode3;
                    colShowFormat.BlockName = "Vehicle";
                    colShowFormat.ColName = "處理狀態";
                    break;
                case "4":
                    colShowFormat.BgCode = CmsStaticCode.bgCode4;
                    colShowFormat.TextCode = CmsStaticCode.textCode4;
                    colShowFormat.BlockName = "Vehicle";
                    colShowFormat.ColName = "處理狀態";
                    break;

            }

            //switch 
            _dealStatus = _item.ItemName;
            result.DealStatus.Add("處理狀態", _item.ItemName);
            result.EstimateId.Add("要保序號", _vehicle.EstimateId);
            result.PolicyDate.Add("簽單日期", _vehicle.PolicyDate);
            result.ApplicantDate.Add("要保日期", _vehicle.ApplicantDate);
            result.CompelCardId.Add("強制證號", _vehicle.CompelCardId);
            result.CompelDateBeginEnd.Add("強制承保起迄期", _vehicle.CompelDateBegin + "~" + _vehicle.CompelDateEnd);
            result.ChoiceDateBeginEnd.Add("任意承保起迄期", _vehicle.ChoiceDateBegin + "~" + _vehicle.ChoiceDateEnd);

            result.Status.Add("保單狀態", _vehicle.Status);
            result.TransferStatus.Add("轉檔狀態", _vehicle.TransferStatus);
            result.TotalPremium.Add("總保費", _vehicle.TotalPremium);
            result.PolicyPostWay.Add("保單寄送方式", _vehicle.PolicyPostWay);
            result.PolicyOutTrans.Add("保單實際委外廠商寄送時間", _vehicle.PolicyOutTrans);
            result.EPolicyEmail.Add("電子保單寄送 Email", _vehicle.EPolicyEmail);
            result.EPolicyEmailoutTrans.Add("電子保單寄送時間", _vehicle.EPolicyEmailoutTrans);
            result.PackageId.Add("投保組合", _vehicle.PackageId);
            result.IsPackageId.Add("投保組合是否一致", _vehicle.IsPackageId);

            result.CheckType.Add("核保類型", _vehicle.CheckType);
            result.IsRenewCompelPolicy.Add("是否為續保單", _vehicle.IsRenewCompelPolicy);
            result.RenewCompelPolicyId.Add("強制續保單號", _vehicle.RenewCompelPolicyId);
            result.RenewChoicePolicyId.Add("任意續保單號", _vehicle.RenewChoicePolicyId);
            result.CustomerNote.Add("客戶備註", _vehicle.CustomerNote);
            result.MvdisJson.Add("OCR 辨識異常原因", _vehicle.MvdisJson);
            result.ChRouter.Add("職團通路代號", _vehicle.ChRouter);
            result.ChOfficerId.Add("職團通路經辦代號", _vehicle.ChOfficerId);
            result.ChLeader.Add("職團通路管理人", _vehicle.ChLeader);
            result.ChQuotaId.Add("職團通路業績單位", _vehicle.ChQuotaId);
            result.ActivityCode.Add("活動代碼", _vehicle.ActivityCode);
            result.Device.Add("來源裝置", _vehicle.Device);
            var JsonStr = JsonConvert.SerializeObject(result, Formatting.None,
                new JsonSerializerSettings { ContractResolver = new JsonStringNullToEmpty() });
            result = JsonConvert.DeserializeObject<Realtion_Vehicle_Detail_OutputDto>(JsonStr);
            return result;
        }
        private Realtion_Vehicle_Check_OutputDto CheckNote(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Check_OutputDto result = new Realtion_Vehicle_Check_OutputDto();

            List<CheckNote> checkNotes = new List<CheckNote>();
            checkNotes = _checkNoteRepository.GetCheckNotes(vehicleEstimate.Estimate.EstimateId);
            if (checkNotes.Count == 0 || checkNotes == null)
            {
                checkNotes.Add(new CheckNote { Id = " ",Header="", CreatorUser = " ", Body = " " });
            }
            /*
            checkNotes.Add(new CheckNote { Id = "2206271088611", CreatorUser = "User001", Header = "資料有誤/不足", Body = "確認姓名地址", Status = false });
            checkNotes.Add(new CheckNote { Id = "2206280009561", CreatorUser = "User001", Header = "信用卡授權失敗", Body = "請客戶重新付款", Status = false });
            */
            result.CheckHint.Add("2021-10-01至2022-07-01 總投保金額： 10,000 元 ");
            result.CheckHint.Add("被保人近一個月累計保期：  ");
            result.checkNotes= checkNotes;
            return result;
        }
        private Realtion_Vehicle_Admin_OutputDto AdminNote(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Admin_OutputDto result = new Realtion_Vehicle_Admin_OutputDto();
            List<AdminNote> adminNotes = new List<AdminNote>();
            adminNotes = _adminNoteRepository.GetAdminNotes(vehicleEstimate.Estimate.EstimateId);
            if (adminNotes.Count == 0 || adminNotes ==null)
            {
                adminNotes.Add(new AdminNote { Id = " ", CreatorUser = " ", Body = " " });
            }
            /*
            adminNotes.Add(new AdminNote { Id = "2206281088611", CreatorUser = "User001", Body = "Call配偶鄧先生，補足個人資料" });
            adminNotes.Add(new AdminNote { Id = "2206290009561", CreatorUser = "User001",  Body = "Call配偶鄧先生，請客戶重新付款" });
            */
            result.AdminNotes = adminNotes;
            return result;
        }
        private Realtion_Vehicle_ReIssue_OutputDto ReIssue(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_ReIssue_OutputDto result = new Realtion_Vehicle_ReIssue_OutputDto();

            List<ReIssueRecord> reIssueRecords= new List<ReIssueRecord>();
            //reIssueRecords.Add(new ReIssueRecord { SendTime="2022-07-10 16:05:08", Email="tbaking@gmail.com", Attachment ="upload/20220620/2206207138881[redeem_consent].pdf" });
            reIssueRecords.Add(new ReIssueRecord { SendTime="2022-07-10 16:05:08", Email="tbaking@gmail.com" });
            
            
            result.reIssueRecords = reIssueRecords;
            return result;
        }
        private Realtion_Vehicle_Assured_OutputDto Assured(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Assured_OutputDto result = new Realtion_Vehicle_Assured_OutputDto();

            Dictionary<string, string> dict = new Dictionary<string, string>();

         
            dict.Add("分號", vehicleEstimate.Estimate.AssuredIdExt);
           
            dict.Add("性質", vehicleEstimate.Estimate.AssuredType);
            
            dict.Add("性別", vehicleEstimate.Estimate.AssuredSex);
            dict.Add("婚姻", vehicleEstimate.Estimate.AssuredMarriage);
            dict.Add("住址", vehicleEstimate.Estimate.AssuredAddress);
            dict.Add("郵遞區號", vehicleEstimate.Estimate.AssuredZip);
            dict.Add("電話(日)", vehicleEstimate.Estimate.AssuredTel);
            dict.Add("電話(夜)", vehicleEstimate.Estimate.AssuredTelNight);
            
            dict.Add("傳真電話", vehicleEstimate.Estimate.AssuredFax);
            dict.Add("法人代表人", vehicleEstimate.Estimate.AssuredDeputy);
            dict.Add("法人被保人資產是否大於5000萬", vehicleEstimate.Estimate.AssuredAssets);
            dict.Add("國籍", vehicleEstimate.Estimate.AssuredCntryRiskLevel);
            dict.Add("國別名稱", vehicleEstimate.Estimate.Assured_cntry);
            dict.Add("職業別", vehicleEstimate.Estimate.AssuredRiskLevelOccup);
            dict.Add("高風險職業代號", vehicleEstimate.Estimate.AssuredOccup);
            result.assuredMaster.AssuredEmail = vehicleEstimate.Estimate.AssuredEmail;
            result.assuredMaster.AssuredName = vehicleEstimate.Estimate.AssuredName;
            result.assuredMaster.AssuredBirthDay = vehicleEstimate.Estimate.AssuredBirthDay;
            result.assuredMaster.AssuredId = vehicleEstimate.Estimate.AssuredId;
            result.assuredMaster.AssuredMobilPhone = vehicleEstimate.Estimate.AssuredMobilPhone;
            DictionaryValuetoEmpty(dict);
            result.AssuredDetails.Add(dict);
            return result;
        }
        private Realtion_Vehicle_Applicant_OutputDto Applicant(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Applicant_OutputDto result = new Realtion_Vehicle_Applicant_OutputDto();
            
            Dictionary<string, string> dict = new Dictionary<string, string>();
            
            dict.Add("性質", vehicleEstimate.Estimate.ApplicantType);
            dict.Add("性別", vehicleEstimate.Estimate.ApplicantSex);
            dict.Add("住址", vehicleEstimate.Estimate.ApplicantAddress);
            dict.Add("郵遞區號", vehicleEstimate.Estimate.ApplicantZip);
            dict.Add("電話(日)", vehicleEstimate.Estimate.ApplicantTelDay);
            dict.Add("電話(夜)", vehicleEstimate.Estimate.ApplicantTelNight);
            dict.Add("傳真電話", vehicleEstimate.Estimate.ApplicantFax);
            dict.Add("法人代表人", vehicleEstimate.Estimate.ApplicantDeputy);
            dict.Add("與被保險人關係", vehicleEstimate.Estimate.ApplicantRelation);
            dict.Add("法人要保人資產是否大於5000萬", vehicleEstimate.Estimate.ApplicantAssets);
            dict.Add("國籍", vehicleEstimate.Estimate.ApplicantCntryRiskLevel);
            dict.Add("國別名稱", vehicleEstimate.Estimate.Applicant_cntry);
            dict.Add("職業別", vehicleEstimate.Estimate.ApplicantRiskLevelOccup);
            dict.Add("高風險職業代號", vehicleEstimate.Estimate.ApplicantOccup);

            result.applicantMaster.ApplicantEmail = vehicleEstimate.Estimate.ApplicantEmail;
            result.applicantMaster.ApplicantName = vehicleEstimate.Estimate.ApplicantName;
            result.applicantMaster.ApplicantBirthDay = vehicleEstimate.Estimate.ApplicantBirthDay;
            result.applicantMaster.ApplicantID = vehicleEstimate.Estimate.ApplicantID;
            result.applicantMaster.ApplicantMobilPhone = vehicleEstimate.Estimate.ApplicantMobilPhone;

            DictionaryValuetoEmpty(dict);

            result.ApplicantDetails.Add(dict);

            
            return result;
        }
        private Realtion_Vehicle_Registration_OutputDto Registration(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Registration_OutputDto result = new Realtion_Vehicle_Registration_OutputDto();

            Dictionary<string, string> dict = new Dictionary<string, string>();

            
            dict.Add("廠牌車型代號", vehicleEstimate.Estimate.BrandId);
            dict.Add("廠牌列印名稱", vehicleEstimate.Estimate.BrandName);
            dict.Add("群組化廠牌代號", vehicleEstimate.Estimate.BrandGroupId);
            dict.Add("車系代號", vehicleEstimate.Estimate.CarStyle);
            dict.Add("車種列印名稱", vehicleEstimate.Estimate.CarTypeName);
            dict.Add("引擎號碼", vehicleEstimate.Estimate.EngineId);
            dict.Add("車身號碼", vehicleEstimate.Estimate.BodyId);
            dict.Add("重置價值(元)", vehicleEstimate.Estimate.CarPrice);
            dict.Add("乘載數量", vehicleEstimate.Estimate.TransportAmount);
            dict.Add("乘載單位", vehicleEstimate.Estimate.TransportUnitType);
            dict.Add("車型名稱", vehicleEstimate.Estimate.VehicleModelName);
            dict.Add("新舊車註記", vehicleEstimate.Estimate.IsNew);
            dict.Add("動力類型", vehicleEstimate.Estimate.PowerType);
            result.registrationMaster.CarType = vehicleEstimate.Estimate.CarType;
            result.registrationMaster.TagId = vehicleEstimate.Estimate.TagId;
            result.registrationMaster.Cylinder = vehicleEstimate.Estimate.Cylinder;
            result.registrationMaster.ProductDate = vehicleEstimate.Estimate.ProductYear+"/"+ vehicleEstimate.Estimate.ProductMonth;
            result.registrationMaster.IssueDate = vehicleEstimate.Estimate.IssueDate;
            var JsonStr = JsonConvert.SerializeObject(dict, Formatting.None,
             new JsonSerializerSettings { ContractResolver = new JsonStringNullToEmpty() });
            dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonStr);
            result.RegistrationDetails.Add(dict);





            return result;
        }
        private Realtion_Vehicle_Desc_OutputDto Desc(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Desc_OutputDto result = new Realtion_Vehicle_Desc_OutputDto();
            string[] _policycover = new string[] { "每一人傷害醫療最高 20 萬元整", "每一人失能最高 200 萬元整", "每一人死亡最高 200 萬元整" };
            //string[] _policycover = new string[] { "每一人傷害醫療最高 20 萬元整 <br> 每一人失能最高 200 萬元整 <br> 每一人死亡最高 200 萬元整" };
            string[] _policycover2 = new string[] { "26.6 萬" };
            string[] _policycover3 = new string[] { "26.6 萬" };
            string[] _policycover4= new string[] { "每一個人傷害 300 萬元整", "每一意外事故之傷害 3600 萬元整", "每一人意外事故之財損 60 萬元整" };
            string[] _policycover5= new string[] { "每人失能  1500 萬元整" };
            string[] _policycover6= new string[] { "每一個人傷害 200 萬元整", "每一意外事故之傷害 800 萬元整" };
            string[] _policycover7= new string[] { "保險金額 1000萬元整" };
            string[] _policycover8= new string[] { "拖吊里程 ：100公里", "每一事故補償限額 2 萬元整" };
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "21 強制責任險", PolicyCover = _policycover, Deduct="無", Premium = "$ 1,193" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "09 車體險-丙式(P)", PolicyCover = _policycover2, Deduct = "無", Premium = "$ 2,990" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "11 竊盜險損失(P)", PolicyCover = _policycover3, Deduct = "10%", Premium = "$ 229" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "31 32 第三人責任險", PolicyCover = _policycover4, Deduct = "無", Premium = "$ 3,672" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "109 失能責任增額附加條款", PolicyCover = _policycover5, Deduct = "無", Premium = "$ 82" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "55 乘客體傷第三人責任險", PolicyCover = _policycover6, Deduct = "無", Premium = "$ 782" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "301 超額責任險-國道倍增型", PolicyCover = _policycover7, Deduct = "365", Premium = "$ 953" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "238 道路救援費用附加條款", PolicyCover = _policycover8, Deduct = "無", Premium = "$ 295" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "保費總額(任意險保費已為優惠 18% 的金額)", Premium = "$ 10,196" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "強制折減", Premium = "$ 330" });
            result.Vehicle_Descs.Add(new Vehicle_Desc { PolicyCate = "合計總額", Premium = "$ 9,866" });





            return result;
        }
        private Realtion_Vehicle_Uploads_OutputDto Uploads(VehicleEstimate vehicleEstimate)
        {
            Realtion_Vehicle_Uploads_OutputDto result = new Realtion_Vehicle_Uploads_OutputDto();
            string _attachment = vehicleEstimate.Estimate.Attachment;
            if (!string.IsNullOrEmpty(_attachment))
            {
                var _attachmentAry = _attachment.Split(",");
                
                foreach (var attachment in _attachmentAry)
                {
                    string _filename = string.Empty;
                    string _Id = string.Empty;
                    var _attachAry = attachment.Split("/");
                    if (_attachAry.Length > 1)
                    {
                        int _length = _attachAry.Length;
                        _filename= _attachAry[_length-1];
                        _Id = _filename.Split("[")[0];
                    }
                    result.vehile_Uploads.Add(new Vehile_Upload { SerialNo= _Id, FileUrl=attachment,FileName= _filename });
                    
                }
            }
            else
            {
                result.vehile_Uploads.Add(new Vehile_Upload { FileUrl = String.Empty, FileName = String.Empty });
            }
            
            return result;
        }
        private Realtion_Vehicle_Coefficient_OutputDto Coefficient(VehicleEstimate vehicleEstimate)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            Realtion_Vehicle_Coefficient_OutputDto result = new Realtion_Vehicle_Coefficient_OutputDto();
            dict.Add("關貿查詢時間", vehicleEstimate.Estimate.TradevanQueryDate);
            dict.Add("關貿強制險查詢序號", vehicleEstimate.Estimate.TradevanQueryNumCompel);
            dict.Add("關貿強制險查詢傳回代碼", vehicleEstimate.Estimate.TradevanQueryResultCompel);
            dict.Add("關貿強制保單資料查詢序號", vehicleEstimate.Estimate.TradevanQueryNumCompelPolicyInfo);
            dict.Add("關貿強制保單資料查詢結果", vehicleEstimate.Estimate.TradevanQueryResultCompelPolicyInfo);
            dict.Add("強制性別年齡係數", vehicleEstimate.Estimate.SexAgeFactorCompel);
            dict.Add("強制車責賠係數", vehicleEstimate.Estimate.CompelFactor);
            dict.Add("強制本期級數", vehicleEstimate.Estimate.CompelLevel);
            dict.Add("強制前期級數", vehicleEstimate.Estimate.LastCompelLevel);
            dict.Add("關貿強制保單到期日", vehicleEstimate.Estimate.OtherCompanyCompelPolicyDateEnd);
            dict.Add("關貿查詢前期強制保單單開始日期", vehicleEstimate.Estimate.TradevanQueryLastCompelDateBegin);
            dict.Add("關貿查詢前期強制保單終止日期", vehicleEstimate.Estimate.TradevanLastCompelPolicyDateEnd);
            dict.Add("酒後駕車次數", vehicleEstimate.Estimate.DrunkDrivingTime);
            dict.Add("關貿前期車種", vehicleEstimate.Estimate.TradevanLastCompelCarType);
            dict.Add("強制險最早投保日", vehicleEstimate.Estimate.CompelFirstDateBegin);
            dict.Add("關貿前期強制承保公司", vehicleEstimate.Estimate.TradevanLastCompelCompany);
            dict.Add("關貿前期任意保單查詢序號", vehicleEstimate.Estimate.TradevanLastChoicePolicyNum);
            dict.Add("關貿前期任意保單終止日期", vehicleEstimate.Estimate.TradevanLastChoicePolicyDateEnd);
            dict.Add("關貿前期任意保單號碼", vehicleEstimate.Estimate.TradevanLastChoicePolicyId);
            dict.Add("任意險最早投保日", vehicleEstimate.Estimate.ChoiceFirstDateBegin);
            dict.Add("是否有承保明細", vehicleEstimate.Estimate.IsExistPolicyDetail);
            dict.Add("車體賠款係數", vehicleEstimate.Estimate.CarDamageAccidentFactor);
            dict.Add("車體險製造年份費率代號", vehicleEstimate.Estimate.CarDamageProductYearRateCode);
            dict.Add("車體險製造年份費率係數", vehicleEstimate.Estimate.CarDamageProductYearFactor);
            dict.Add("關貿車體險查詢傳回代碼", vehicleEstimate.Estimate.TradevanQueryResultDamage);
            dict.Add("車體賠款級數", vehicleEstimate.Estimate.CarDamageAccidentLevel);
            dict.Add("車體險年齡性別係數", vehicleEstimate.Estimate.SexAgeFactorDamage);
            dict.Add("車體三年賠款總次數", vehicleEstimate.Estimate.CarDamageAccidentTimesInThreeYears);
            dict.Add("關貿車體險查詢序號", vehicleEstimate.Estimate.TradevanQueryNumDamage);
            dict.Add("關貿105,85不明車損不計車體序號", vehicleEstimate.Estimate.TradevanQueryNumOnceUnknownDamage);
            dict.Add("關貿105,85不明車損不計車體賠款級數", vehicleEstimate.Estimate.CarOnceUnknownDamageAccidentLevel);
            dict.Add("關貿105,85不明車損不計車體係數", vehicleEstimate.Estimate.CarOnceUnknownDamageAccidentFactor);
            dict.Add("關貿105,85不明車損不計車體三年內肇事次數", vehicleEstimate.Estimate.CarOnceUnknownDamageAccidentTimesInThreeYears);
            dict.Add("關貿05,09不明車損不計車體序號", vehicleEstimate.Estimate.TradevanQueryNumNoneUnknownDamage);
            dict.Add("關貿05,09不明車損不計車體賠款級數", vehicleEstimate.Estimate.CarNoneUnknownDamageAccidentLevel);
            dict.Add("關貿05,09不明車損不計車體係數", vehicleEstimate.Estimate.CarNoneUnknownDamageAccidentFactor);
            dict.Add("關貿105,09不明車損不計車體三年內肇事次數", vehicleEstimate.Estimate.CarNoneUnknownDamageAccidentTimesInThreeYears);
            dict.Add("選擇使用哪一組車體系數", vehicleEstimate.Estimate.UnknownDamageFactor);
            dict.Add("前期車體險查詢序號", vehicleEstimate.Estimate.TradevanQueryNumLastDamage);
            dict.Add("關貿前期車體險到期日", vehicleEstimate.Estimate.TradevanQueryDamageDateEnd);
            dict.Add("前期車體險保單號", vehicleEstimate.Estimate.TradevanQueryDamagePolicyId);
            dict.Add("前期車體險代號", vehicleEstimate.Estimate.TradevanQueryDamageInsType);
            dict.Add("關貿續保原始組合車體險查詢序號", vehicleEstimate.Estimate.RenewTradevanQueryNumDamage);
            dict.Add("續保原始組合車體賠款級數", vehicleEstimate.Estimate.RenewCarDamageAccidentLevel);
            dict.Add("車體前五年賠款金額(已決)為0之賠案次數", vehicleEstimate.Estimate.DamageAccidentCloseTimesFiveYearsAgo);
            dict.Add("車體前五年有肇責計次次數", vehicleEstimate.Estimate.DamageDutyAndCountFiveYearsAgo);
            dict.Add("車體賠款次數(前一年)", vehicleEstimate.Estimate.Damage_Acc_1);
            dict.Add("車體賠款次數(前二年)", vehicleEstimate.Estimate.Damage_Acc_2);
            dict.Add("車體賠款次數(前三年)", vehicleEstimate.Estimate.Damage_Acc_3);
            dict.Add("竊盜險製造年份費率代號", vehicleEstimate.Estimate.BurglaryProductYearRateCode);
            dict.Add("竊盜險製造年份費率係數", vehicleEstimate.Estimate.BurglaryProductYearFactor);
            dict.Add("關貿前期竊盜險查詢序號", vehicleEstimate.Estimate.TradevanQueryNumTheft);
            dict.Add("關貿前期竊盜險到期日", vehicleEstimate.Estimate.TradevanQueryTheftDateEnd);
            dict.Add("關貿前期竊盜險保單號", vehicleEstimate.Estimate.TradevanLastTheftPolicyId);
            dict.Add("任意性別年齡係數", vehicleEstimate.Estimate.SexAgeFactorLiability);
            dict.Add("車責本期級數", vehicleEstimate.Estimate.LiaLevel);
            dict.Add("任意車責賠款係數", vehicleEstimate.Estimate.LiaFactor);
            dict.Add("關貿第三人責任險查詢傳回代碼", vehicleEstimate.Estimate.TradevanQueryResultLia);
            dict.Add("關貿第三人責任險查詢序號", vehicleEstimate.Estimate.TradevanQueryNumLia);
            dict.Add("車責三年賠款總次數", vehicleEstimate.Estimate.LiaAccidentTimesInThreeYears);
            dict.Add("車責理賠次數一年內", vehicleEstimate.Estimate.LiaAccidentTimesWithinYear);
            dict.Add("車責前五年之賠案次數", vehicleEstimate.Estimate.LiaAccidentTimesFiveYearsAgo);
            DictionaryValuetoEmpty(dict);
            result.coefficients.Add(dict);
            
            return result;
        }
        private Realtion_Vehicle_HCC_OutputDto HCC(VehicleEstimate vehicleEstimate)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            Realtion_Vehicle_HCC_OutputDto result = new Realtion_Vehicle_HCC_OutputDto();

            dict.Add("性別及婚姻", vehicleEstimate.Estimate_Hcc.SexAndMarriage);
            dict.Add("性別及婚姻係數", vehicleEstimate.Estimate_Hcc.SexAndMarriageFactor);
            dict.Add("被保險人年齡", vehicleEstimate.Estimate_Hcc.Age);
            dict.Add("年齡係數", vehicleEstimate.Estimate_Hcc.AgeFactor);
            dict.Add("車體前五年賠款金額(已決)為0之賠案次數", vehicleEstimate.Estimate_Hcc.DamageAccSum5Year);
            dict.Add("車體前五年有肇責有計次之賠案次數係數", vehicleEstimate.Estimate_Hcc.DamageAccSum5YearFactor);
            dict.Add("車責前五年之賠案件數", vehicleEstimate.Estimate_Hcc.LiaAccSum5Year);
            dict.Add("車責前五年有肇責有計次之賠案件數", vehicleEstimate.Estimate_Hcc.LiaAccSum5YearFactor);
            dict.Add("前五年有肇責有計次之賠案件數(車體+車責)", vehicleEstimate.Estimate_Hcc.DutyAccSum5Year);
            dict.Add("前五年有肇責有計次之賠案件數係數(車體+車責)", vehicleEstimate.Estimate_Hcc.DutyAccSum5YearFactor);
            dict.Add("年期", vehicleEstimate.Estimate_Hcc.Year);
            dict.Add("年期係數", vehicleEstimate.Estimate_Hcc.YearFactor);
            dict.Add("車種", vehicleEstimate.Estimate_Hcc.CarType);
            dict.Add("車種係數", vehicleEstimate.Estimate_Hcc.CarTypeFactor);
            dict.Add("車齡", vehicleEstimate.Estimate_Hcc.CarAge);
            dict.Add("車齡係數", vehicleEstimate.Estimate_Hcc.CarAgeFactor);
            dict.Add("是否投保車體險(N、Y)", vehicleEstimate.Estimate_Hcc.HasDamage);
            dict.Add("投保車體險係數", vehicleEstimate.Estimate_Hcc.DamageFactor);
            dict.Add("是否投保車責險(N、Y)", vehicleEstimate.Estimate_Hcc.HasLia);
            dict.Add("投保車責險係數", vehicleEstimate.Estimate_Hcc.LiaFactor);
            dict.Add("是否投保竊盜險(N、Y)", vehicleEstimate.Estimate_Hcc.HasTheft);
            dict.Add("投保竊盜險係數", vehicleEstimate.Estimate_Hcc.TheftFactor);
            dict.Add("附加險數量", vehicleEstimate.Estimate_Hcc.ExtInsSum);
            dict.Add("附加險數量係數", vehicleEstimate.Estimate_Hcc.ExtInsSumFactor);
            dict.Add("郵遞區號", vehicleEstimate.Estimate_Hcc.ZipCode);
            dict.Add("郵遞區號係數", vehicleEstimate.Estimate_Hcc.ZipCodeFactor);
            dict.Add("車種及保期", vehicleEstimate.Estimate_Hcc.CarTypeYear);
            dict.Add("車種及保期係數", vehicleEstimate.Estimate_Hcc.CarTypeYearFactor);
        
            result.hccMaster.RouteMain = vehicleEstimate.Estimate_Hcc.RouteMain;
            result.hccMaster.RateDate = vehicleEstimate.Estimate_Hcc.RateDate;
            result.hccMaster.Level = vehicleEstimate.Estimate_Hcc.Level;
            result.hccMaster.Score = vehicleEstimate.Estimate_Hcc.Score;
            DictionaryValuetoEmpty(dict);
            result.HCCDetails.Add(dict);
            return result;
        }
        private Realtion_Vehicle_TradeData_OutputDto TradeData(VehicleEstimate vehicleEstimate)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            Realtion_Vehicle_TradeData_OutputDto result = new Realtion_Vehicle_TradeData_OutputDto();
            dict.Add("險別", vehicleEstimate.TradeData.InsCategory);
            dict.Add("試算號碼", vehicleEstimate.TradeData.EstimateId);
            dict.Add("付款確認人員代號", vehicleEstimate.TradeData.PayUser);
            dict.Add("是否交易成功", vehicleEstimate.TradeData.TradeSuccess);
            dict.Add("訂單編號", vehicleEstimate.TradeData.OrderId);
            dict.Add("泰新實收金額", vehicleEstimate.TradeData.PaidUpAmount);
            dict.Add("泰新暫收編號", vehicleEstimate.TradeData.PaidUpSerial);
            dict.Add("其他資料", vehicleEstimate.TradeData.Properties);
            dict.Add("允許的持卡人清單", vehicleEstimate.TradeData.AllowHolders);
            result.tradeDataMaster.PayDate = vehicleEstimate.TradeData.PayDate;
            result.tradeDataMaster.LogDate = vehicleEstimate.TradeData.LogDate;
            result.tradeDataMaster.Amount = vehicleEstimate.TradeData.Amount;
            result.tradeDataMaster.PayStatus = vehicleEstimate.TradeData.PayStatus;
            result.tradeDataMaster.PayWay = vehicleEstimate.TradeData.PayWay;
            result.tradeDataMaster.MerchOID = vehicleEstimate.TradeData.MerchOID;
            result.tradeDataMaster.TradeSuccess = vehicleEstimate.TradeData.TradeSuccess;
            result.tradeDataMaster.PayDate = "2022-07-03";
            result.tradeDataMaster.LogDate = "2022-07-03";
            result.tradeDataMaster.Amount = "789";
            result.tradeDataMaster.PayStatus = "已付";
            result.tradeDataMaster.PayWay = "銀行帳戶扣款";
            result.tradeDataMaster.MerchOID = "0CD1111150000519";
            result.tradeDataMaster.TradeSuccess = "成功";
            DictionaryValuetoEmpty(dict);
            result.TradeDetails.Add(dict);
            return result;
        }
        #endregion
        /*
        private void SetNullPropertiesToEmptyString(object root)
        {
            var queue = new Queue<object>();
            queue.Enqueue(root);
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                foreach (var property in current.GetType().GetProperties())
                {
                    var propertyType = property.PropertyType;
                    var value = property.GetValue(current, null);
                    if (propertyType == typeof(string) && value == null)
                    {
                        property.SetValue(current, string.Empty);
                    }
                    else if (propertyType.IsClass && value != null && value != current && !queue.Contains(value))
                    {
                        queue.Enqueue(value);
                    }
                }
            }
        }*/
        public EstimatesWithCustomer GetEstimatesbyPid(string Pid)
        {
            EstimatesWithCustomer estimatesWithCustomer = new EstimatesWithCustomer();
            List<Vehicle_Estimate> estimates = _repository.GetVehicleEstimatesbyPid(Pid);
            estimatesWithCustomer.Estimate_Customer = new();
            CustomerEstimates customerEstimates = new CustomerEstimates();
            customerEstimates.Estimates = new List<EstimateListOutputDto>();
            estimatesWithCustomer.Estimates = new CustomerEstimates();
            if (estimates.Count > 0)
            {
                estimatesWithCustomer.Estimate_Customer = Customer(estimates.FirstOrDefault());
            }
            customerEstimates.Estimates = estimates.MapTo<EstimateListOutputDto>();
            /* mock 交易狀態*/
            String[] _status = new string[] { "待處理", "尚未付款","處理中","退款","已完成", "不處理/取消" };
            foreach (var item in customerEstimates.Estimates)
            {
                Random random = new Random();
                int randomNumber = random.Next(0, _status.Length);
                int randomsDay = random.Next(30, 90);
                int randomeDay = random.Next(90, 455);
                item.Status=_status[randomNumber];
                item.PolicyDateBeginEnd = DateTime.Now.AddDays(randomsDay).ToString("yyyy-MM-dd")+" ~ "+ DateTime.Now.AddDays(randomeDay).ToString("yyyy-MM-dd");
               
            }
            estimatesWithCustomer.Estimates = customerEstimates;
            return estimatesWithCustomer;


        }
        public EstimatesWithCustomer GetEstimatesbyPlateNo(string PlateNo)
        {
            EstimatesWithCustomer estimatesWithCustomer = new EstimatesWithCustomer();
            List<Vehicle_Estimate> estimates = _repository.GetVehicleEstimatesbyPlateNo(PlateNo);
            estimatesWithCustomer.Estimate_Customer = new();
            CustomerEstimates customerEstimates = new CustomerEstimates();
            customerEstimates.Estimates = new List<EstimateListOutputDto>();
            estimatesWithCustomer.Estimates = new CustomerEstimates();
            if (estimates.Count > 0)
            {
                estimatesWithCustomer.Estimate_Customer = Customer(estimates.FirstOrDefault());
            }
            customerEstimates.Estimates = estimates.MapTo<EstimateListOutputDto>();
            /* mock 交易狀態*/
            String[] _status = new string[] { "待處理", "尚未付款", "處理中", "退款", "已完成", "不處理/取消" };
            foreach (var item in customerEstimates.Estimates)
            {
                Random random = new Random();
                int randomNumber = random.Next(0, _status.Length);
                int randomsDay = random.Next(30, 90);
                int randomeDay = random.Next(90, 455);
                item.Status = _status[randomNumber];
                item.PolicyDateBeginEnd = DateTime.Now.AddDays(randomsDay).ToString("yyyy-MM-dd") + " ~ " + DateTime.Now.AddDays(randomeDay).ToString("yyyy-MM-dd");

            }
            estimatesWithCustomer.Estimates = customerEstimates;
            return estimatesWithCustomer;


        }
        public EstimatesWithCustomer GetEstimatesbyPhoneNo(string PhoneNo)
        {
            EstimatesWithCustomer estimatesWithCustomer = new EstimatesWithCustomer();
            List<Vehicle_Estimate> estimates = _repository.GetVehicleEstimatesbyPhoneNo(PhoneNo);
            estimatesWithCustomer.Estimate_Customer = new();
            CustomerEstimates customerEstimates = new CustomerEstimates();
            customerEstimates.Estimates = new List<EstimateListOutputDto>();
            estimatesWithCustomer.Estimates = new CustomerEstimates();
            if (estimates.Count > 0)
            {
                estimatesWithCustomer.Estimate_Customer = Customer(estimates.FirstOrDefault());
            }
            customerEstimates.Estimates = estimates.MapTo<EstimateListOutputDto>();
            /* mock 交易狀態*/
            String[] _status = new string[] { "待處理", "尚未付款", "處理中", "退款", "已完成", "不處理/取消" };
            foreach (var item in customerEstimates.Estimates)
            {
                Random random = new Random();
                int randomNumber = random.Next(0, _status.Length);
                int randomsDay = random.Next(30, 90);
                int randomeDay = random.Next(90, 455);
                item.Status = _status[randomNumber];
                item.PolicyDateBeginEnd = DateTime.Now.AddDays(randomsDay).ToString("yyyy-MM-dd") + " ~ " + DateTime.Now.AddDays(randomeDay).ToString("yyyy-MM-dd");

            }
            estimatesWithCustomer.Estimates = customerEstimates;
            return estimatesWithCustomer;


        }
        public EstimatesWithCustomer GetEstimatesbyEstimateId(string EstimateId)
        {
            EstimatesWithCustomer estimatesWithCustomer = new EstimatesWithCustomer();
            List<Vehicle_Estimate> estimates = _repository.GetVehicleEstimatesbyEstimateId(EstimateId);
            estimatesWithCustomer.Estimate_Customer = new();
            CustomerEstimates customerEstimates = new CustomerEstimates();
            customerEstimates.Estimates = new List<EstimateListOutputDto>();
            estimatesWithCustomer.Estimates = new CustomerEstimates();
            if (estimates.Count > 0)
            {
                estimatesWithCustomer.Estimate_Customer = Customer(estimates.FirstOrDefault());
            }
            customerEstimates.Estimates = estimates.MapTo<EstimateListOutputDto>();
            /* mock 交易狀態*/
            String[] _status = new string[] { "待處理", "尚未付款", "處理中", "退款", "已完成", "不處理/取消" };
            foreach (var item in customerEstimates.Estimates)
            {
                Random random = new Random();
                int randomNumber = random.Next(0, _status.Length);
                int randomsDay = random.Next(30, 90);
                int randomeDay = random.Next(90, 455);
                item.Status = _status[randomNumber];
                item.PolicyDateBeginEnd = DateTime.Now.AddDays(randomsDay).ToString("yyyy-MM-dd") + " ~ " + DateTime.Now.AddDays(randomeDay).ToString("yyyy-MM-dd");

            }
            estimatesWithCustomer.Estimates = customerEstimates;
            return estimatesWithCustomer;


        }

        class ECustomer
        {
            public string Pid { get; set; }
            public string Name { get; set; }
            public string MobilePhone { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string Birthday { get; set; }
            public string Tel { get; set; }
            public string Memo { get; set; }
            public string ContactPersonName { get; set; }
            public string ContactPersonPhone { get; set; }
            public string ContactPersonRelation { get; set; }
            public string CDPTags { get; set; }
            public string CRMTags { get; set; }



        }
        private void DictionaryValuetoEmpty(Dictionary<string, string> keyValues)
        {
            foreach(var key in keyValues.Keys.ToList())
            {
                if (string.IsNullOrEmpty(keyValues[key]) || keyValues[key] == "null")
                {
                    keyValues[key] = string.Empty;
                }
            }
        }

        
        /// <summary>
        /// 實作Newtonsoft.Json的DefaultContractResolver
        /// </summary>
        public class JsonStringNullToEmpty : DefaultContractResolver
        {
            public JsonStringNullToEmpty() { }

            protected override IList<JsonProperty> CreateProperties(System.Type oType, Newtonsoft.Json.MemberSerialization oMS)
            {
                return oType.GetProperties().Select(oP =>
                {
                    var oJP = base.CreateProperty(oP, oMS);
                    oJP.ValueProvider = new JsonStringNullToEmptyValueProvider(oP);
                    return oJP;
                }).ToList();
            }
        }

        /// <summary>
        /// 實作Newtonsoft.Json的IValueProvider
        /// </summary>
        internal class JsonStringNullToEmptyValueProvider : Newtonsoft.Json.Serialization.IValueProvider
        {
            System.Reflection.PropertyInfo _oMemberInfo;

            //建構子（將成員資訊帶入成為內部變數）
            public JsonStringNullToEmptyValueProvider(System.Reflection.PropertyInfo oMI) { _oMemberInfo = oMI; }

            //實作IValueProvider介面的寫入動作
            public void SetValue(object oTarget, object oValue) { _oMemberInfo.SetValue(oTarget, oValue); }

            //實作IValueProvider介面的寫入動作
            public object GetValue(object oTarget)
            {
                //設定回傳變數
                object oResult = _oMemberInfo.GetValue(oTarget);
                //若成員為字串型態，就處理他
                if (_oMemberInfo.PropertyType == typeof(System.String) && oResult == null) oResult = string.Empty;
                //若成員為表格型態，就進入巡訪
                if (_oMemberInfo.PropertyType == typeof(System.Data.DataTable))
                {
                    System.Data.DataTable oDT = (System.Data.DataTable)oResult;
                    foreach (System.Data.DataRow oDR in oDT.Rows)
                    {
                        foreach (System.Data.DataColumn oDC in oDT.Columns)
                        {
                            if (oDC.DataType == typeof(System.String))
                            {
                                oDR[oDC.ColumnName] = oDR[oDC.ColumnName] as string ?? string.Empty;
                            }
                        }
                    }
                    oResult = oDT;
                }
                if (_oMemberInfo.PropertyType == typeof(Dictionary<string,string>))
                {
                    Dictionary<string, string> oDY = (Dictionary<string, string>)oResult;
                    foreach (string key in oDY.Keys.ToList())
                    {
                        

                        if (string.IsNullOrEmpty(oDY[key])|| oDY[key]=="null")
                        {
                            oDY[key]=string.Empty;
                        }
                    }
                    oResult = oDY;
                }
                //回傳結果
                return oResult;
            }
        }
    }
}