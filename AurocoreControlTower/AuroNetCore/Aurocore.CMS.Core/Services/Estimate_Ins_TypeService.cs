using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class Estimate_Ins_TypeService: BaseService<Estimate_Ins_Type,Estimate_Ins_TypeOutputDto, string>, IEstimate_Ins_TypeService
    {
		private readonly IEstimate_Ins_TypeRepository _repository;
        public Estimate_Ins_TypeService(IEstimate_Ins_TypeRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}