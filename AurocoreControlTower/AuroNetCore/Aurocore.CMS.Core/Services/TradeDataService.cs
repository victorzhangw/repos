using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Services
{
    /// <summary>
    /// 服務接口實現
    /// </summary>
    public class TradeDataService: BaseService<TradeData,TradeDataOutputDto, string>, ITradeDataService
    {
		private readonly ITradeDataRepository _repository;
        public TradeDataService(ITradeDataRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}