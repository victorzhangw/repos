using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurocore.Commons.Dtos;
using Aurocore.Commons.Mapping;
using Aurocore.Commons.Pages;
using Aurocore.Commons.Services;
using Aurocore.CMS.IRepositories;
using Aurocore.CMS.IServices;
using Aurocore.CMS.Dtos;
using Aurocore.CMS.Models;

namespace Aurocore.CMS.Services
{
    /// <summary>
    /// 文章服務介面實現
    /// </summary>
    public class ArticlenewsService : BaseService<Articlenews, ArticlenewsOutputDto, string>, IArticlenewsService
    {
        private readonly IArticlenewsRepository _repository;
        public ArticlenewsService(IArticlenewsRepository repository) : base(repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="search">查詢的條件</param>
        /// <returns>指定物件的集合</returns>
        public override async Task<PageResult<ArticlenewsOutputDto>> FindWithPagerAsync(SearchInputDto<Articlenews> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege();
            if (search.Keywords is { Length: > 0 })
            {
                where += string.Format(" and  Title like '%{0}%'", search.Keywords);
            };
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<Articlenews> list = await _repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
            PageResult<ArticlenewsOutputDto> pageResult = new PageResult<ArticlenewsOutputDto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<ArticlenewsOutputDto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }
    }
}