﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.IO;
using System.Text;
using System.IO.Compression;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Extensions.Compression.Core.Models;

namespace Tealeaf_DataColletcor.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DataCollectorController : ControllerBase
    {
        private readonly IHttpClientFactory _clientFactory;
        public DataCollectorController(IHttpClientFactory clientFactory) 
        {
            _clientFactory = clientFactory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost,HttpGet]
        public async Task<HttpResponseMessage> Clients()
        {


            var client = _clientFactory.CreateClient("TLT_Transport");
            
            HttpResponseMessage rst = new HttpResponseMessage();
            var result = string.Empty;
            var list_hader= Request.Headers.ToList();
            HttpRequestMessage replyMessage = new HttpRequestMessage(HttpMethod.Post, "collector/collectorPost");
            
            try {
                for (int i = 0; i < list_hader.Count; i++)
                {
                    /*
                    if (list_hader[i].Key.ToString() == "Referer")
                    {
                        string _host = list_hader[i].Value.ToString();
                        client.DefaultRequestHeaders.Host = _host;
                    }*/
                    replyMessage.Headers.TryAddWithoutValidation(list_hader[i].Key.ToString(), list_hader[i].Value.ToString());
                }
                
                GZipStream decompressionStream = new GZipStream(Request.Body, CompressionMode.Decompress);

                using (var reader = new StreamReader(decompressionStream, Encoding.UTF8))
                {
                    result = await reader.ReadToEndAsync();
                    replyMessage.Content = new StringContent(result);
                    await client.SendAsync(replyMessage).ContinueWith(
                        responseTask =>
                        {
                            rst = responseTask.GetAwaiter().GetResult();
                        }
                        );

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            
            /*
            using (GZipStream decompressionStream = new GZipStream(Request.Body, CompressionMode.Decompress))
            {
                decompressionStream.CopyToAsync()
            }*/

            //var response = client.PostAsync("", Request.C);
           
            
            return rst;

        }

       
    }
}