using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Aurobase.AspNetCore.ViewModel
{
    /// <summary>
    /// 資料庫連線返回結果實體
    /// </summary>
    [Serializable]
    public class DBConnResult
    {
        /// <summary>
        /// 未加密字串
        /// </summary>
        [DataMember]
        public string ConnStr { get; set; }
        /// <summary>
        /// 資料庫名稱
        /// </summary>
        [DataMember]
        public string EncryptConnStr { get; set; }
    }
}
