using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Helpers;
using Aurobase.Security.Models;

namespace Aurobase.AspNetCore.Common
{
    /// <summary>
    /// 系統初始化內容
    /// </summary>
    public abstract class AurobaseInitialization
    {
        /// <summary>
        /// 初始化
        /// </summary>
        public virtual  void Initial()
        {
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            SysSetting sysSetting = XmlConverter.Deserialize<SysSetting>("xmlconfig/sys.config");
            if (sysSetting != null)
            {
                AurobaseCacheHelper.Add("SysSetting", sysSetting);
            }
       }
    }
}
