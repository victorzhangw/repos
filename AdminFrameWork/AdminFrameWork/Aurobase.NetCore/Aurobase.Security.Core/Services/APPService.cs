using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Pages;
using Aurobase.Commons.Services;
using Aurobase.Security.Dtos;
using Aurobase.Security.IRepositories;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.Security.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class APPService: BaseService<APP,AppOutputDto,string>, IAPPService
    {
        private readonly IAPPRepository _appRepository;
        private readonly ILogService _logService;
        public APPService(IAPPRepository repository, ILogService logService) : base(repository)
        {
            _appRepository = repository;
            _logService = logService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity">ʵ</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public override long Insert(APP entity, IDbTransaction trans = null)
        {
            long result = repository.Insert(entity, trans); 
            this.UpdateCacheAllowApp();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity">ʵ</param>
        /// <param name="id">ID</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public override async Task<bool> UpdateAsync(APP entity, string id, IDbTransaction trans = null)
        {
            bool result=await repository.UpdateAsync(entity, id, trans);
            this.UpdateCacheAllowApp();
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity">ʵ</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public override async Task<long> InsertAsync(APP entity, IDbTransaction trans = null)
        {
            long result = await repository.InsertAsync(entity, trans);
            this.UpdateCacheAllowApp();
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appid">ӦID</param>
        /// <param name="secret">ӦԿAppSecret</param>
        /// <returns></returns>
        public APP GetAPP(string appid, string secret)
        {
            return _appRepository.GetAPP(appid, secret);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appid">ӦID</param>
        /// <returns></returns>
        public APP GetAPP(string appid)
        {
            return _appRepository.GetAPP(appid);
        }
        public IList<AppOutputDto> SelectApp()
        {
            return _appRepository.SelectApp();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search<param>
        /// <returns>ָ</returns>
        public override async Task<PageResult<AppOutputDto>> FindWithPagerAsync(SearchInputDto<APP> search)
        {
            bool order = search.Order == "asc" ? false : true;
            string where = GetDataPrivilege(false);
            if (!string.IsNullOrEmpty(search.Keywords))
            {
                where += string.Format(" and (AppId like '%{0}%' or RequestUrl like '%{0}%')", search.Keywords);
            };
            PagerInfo pagerInfo = new PagerInfo
            {
                CurrenetPageIndex = search.CurrenetPageIndex,
                PageSize = search.PageSize
            };
            List<APP> list = await repository.FindWithPagerAsync(where, pagerInfo, search.Sort, order);
            PageResult<AppOutputDto> pageResult = new PageResult<AppOutputDto>
            {
                CurrentPage = pagerInfo.CurrenetPageIndex,
                Items = list.MapTo<AppOutputDto>(),
                ItemsPerPage = pagerInfo.PageSize,
                TotalItems = pagerInfo.RecordCount
            };
            return pageResult;
        }
        public void UpdateCacheAllowApp()
        {
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            IEnumerable<APP> appList = repository.GetAllByIsNotDeleteAndEnabledMark();
            AurobaseCacheHelper.Add("AllowAppId", appList);
        }
    }
}