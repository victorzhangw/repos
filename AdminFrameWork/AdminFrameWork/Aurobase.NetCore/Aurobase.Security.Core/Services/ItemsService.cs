using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Services;
using Aurobase.Security.Dtos;
using Aurobase.Security.IRepositories;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.Security.Services
{
    public class ItemsService: BaseService<Items, ItemsOutputDto, string>, IItemsService
    {
        private readonly IItemsRepository _repository;
        private readonly ILogService _logService;
        public ItemsService(IItemsRepository repository, ILogService logService) : base(repository)
        {
            _repository = repository;
            _logService = logService;
        }


        /// <summary>
        /// ȡܲ˵Vue б
        /// </summary>
        /// <returns></returns>
        public async Task<List<ItemsOutputDto>> GetAllItemsTreeTable()
        {
            List<ItemsOutputDto> reslist = new List<ItemsOutputDto>();
            IEnumerable<Items> elist =await _repository.GetListWhereAsync("1=1");
            List<Items> list = elist.OrderBy(t => t.SortCode).ToList();
            List<Items> oneMenuList = list.FindAll(t => t.ParentId == "");
            foreach (Items item in oneMenuList)
            {
                ItemsOutputDto menuTreeTableOutputDto = new ItemsOutputDto();
                menuTreeTableOutputDto = item.MapTo<ItemsOutputDto>();
                menuTreeTableOutputDto.Children = GetSubMenus(list, item.Id).ToList<ItemsOutputDto>();
                reslist.Add(menuTreeTableOutputDto);
            }
            return reslist;
        }

        /// <summary>
        /// ݱѯֵ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        public async Task<Items> GetByEnCodAsynce(string enCode)
        {
            return await _repository.GetByEnCodAsynce(enCode);
        }
        /// <summary>
        /// ʱжϷǷڣųԼ
        /// </summary>
        /// <param name="enCode"></param
        /// <param name="id">Id</param>
        /// <returns></returns>
        public async Task<Items> GetByEnCodAsynce(string enCode, string id)
        {
            return await _repository.GetByEnCodAsynce(enCode,id);
        }

        /// <summary>
        /// ȡӲ˵ݹ
        /// </summary>
        /// <param name="data"></param>
        /// <param name="parentId">Id</param>
        /// <returns></returns>
        private List<ItemsOutputDto> GetSubMenus(List<Items> data, string parentId)
        {
            List<ItemsOutputDto> list = new List<ItemsOutputDto>();
            ItemsOutputDto menuTreeTableOutputDto = new ItemsOutputDto();
            var ChilList = data.FindAll(t => t.ParentId == parentId);
            foreach (Items entity in ChilList)
            {
                menuTreeTableOutputDto = entity.MapTo<ItemsOutputDto>();
                menuTreeTableOutputDto.Children = GetSubMenus(data, entity.Id).OrderBy(t => t.SortCode).MapTo<ItemsOutputDto>();
                list.Add(menuTreeTableOutputDto);
            }
            return list;
        }
    }
}