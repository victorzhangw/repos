using System;
using System.Collections.Generic;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRoleService:IService<Role, RoleOutputDto, string>
    {
        /// <summary>
        /// ݽɫȡɫ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        Role GetRole(string enCode);


        /// <summary>
        /// ûɫIDȡɫ
        /// </summary>
        /// <param name="ids">ɫIDַá,ָ</param>
        /// <returns></returns>
        string GetRoleEnCode(string ids);


        /// <summary>
        /// ûɫIDȡɫ
        /// </summary>
        /// <param name="ids">ɫIDַá,ָ</param>
        /// <returns></returns>
       string GetRoleNameStr(string ids);
    }
}
