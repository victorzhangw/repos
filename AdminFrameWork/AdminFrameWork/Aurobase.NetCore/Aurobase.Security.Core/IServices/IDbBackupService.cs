using System;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    public interface IDbBackupService:IService<DbBackup, DbBackupOutputDto, string>
    {
    }
}
