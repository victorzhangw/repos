using System;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 定義序號編碼規則表服務介面
    /// </summary>
    public interface ISequenceRuleService:IService<SequenceRule,SequenceRuleOutputDto, string>
    {
    }
}
