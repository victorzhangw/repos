using System;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    public interface IUserLogOnService:IService<UserLogOn, UserLogOnOutputDto, string>
    {

        /// <summary>
        /// ݻԱIDȡû¼Ϣʵ
        /// </summary>
        /// <param name="userId">ûId</param>
        /// <returns></returns>
        UserLogOn GetByUserId(string userId);

        /// <summary>
        /// ݻԱIDȡû¼Ϣʵ
        /// </summary>
        /// <param name="info">Ϣ</param>
        /// <param name="userId">ûId</param>
        /// <returns></returns>
        Task<bool> SaveUserTheme(UserThemeInputDto info, string userId);
    }
}
