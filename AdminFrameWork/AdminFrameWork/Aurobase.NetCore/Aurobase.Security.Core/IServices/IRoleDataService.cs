using System;
using System.Collections.Generic;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    public interface IRoleDataService:IService<RoleData, RoleDataOutputDto, string>
    {

        /// <summary>
        /// ݽɫȨʲ
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        List<string> GetListDeptByRole(string roleIds);
    }
}
