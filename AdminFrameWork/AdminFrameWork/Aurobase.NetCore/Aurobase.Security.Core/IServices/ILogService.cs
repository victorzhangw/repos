using System;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Commons.Pages;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// ־¼
    /// </summary>
    public interface ILogService:IService<Log, LogOutputDto, string>
    {
        /// <summary>
        /// ϢдûĲ־¼
        /// Ҫдݿ־
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="operationType"></param>
        /// <param name="note">ϸ</param>
        /// <returns></returns>
         bool OnOperationLog(string tableName, string operationType, string note);

        /// <summary>
        /// ϢдûĲ־¼
        /// Ҫдģ־
        /// </summary>
        /// <param name="module">ģ</param>
        /// <param name="operationType"></param>
        /// <param name="note">ϸ</param>
        /// <param name="currentUser">û</param>
        /// <returns></returns>
        bool OnOperationLog(string module, string operationType,  string note, AurobaseCurrentUser currentUser);
        /// <summary>
        /// ѯݿ,ض󼯺(ڷҳʾ)
        /// </summary>
        /// <param name="search">ѯ</param>
        /// <returns>ָļ</returns>
        Task<PageResult<LogOutputDto>> FindWithPagerSearchAsync(SearchLogModel search);
    }
}
