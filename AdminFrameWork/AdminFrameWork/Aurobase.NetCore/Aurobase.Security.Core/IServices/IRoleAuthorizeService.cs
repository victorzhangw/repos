using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRoleAuthorizeService:IService<RoleAuthorize, RoleAuthorizeOutputDto, string>
    {
        /// <summary>
        /// ݽɫĿͲѯȨ
        /// </summary>
        /// <param name="roleIds"></param>
        /// <param name="itemType"></param>
        /// <returns></returns>
        IEnumerable<RoleAuthorize> GetListRoleAuthorizeByRoleId(string roleIds, string itemType);


        /// <summary>
        /// ȡܲ˵Vue Tree
        /// </summary>
        /// <returns></returns>
        Task<List<ModuleFunctionOutputDto>> GetAllFunctionTree();

        /// <summary>
        /// ɫȨ
        /// </summary>
        /// <param name="roleId">ɫId</param>
        /// <param name="roleAuthorizesList">ɫģ</param>
        /// <param name="roleDataList">ɫɷ</param>
        /// <param name="trans"></param>
        /// <returns>ִгɹ<c>true</c>Ϊ<c>false</c></returns>
        Task<bool> SaveRoleAuthorize(string roleId,List<RoleAuthorize> roleAuthorizesList, List<RoleData> roleDataList,
           IDbTransaction trans = null);
    }
}
