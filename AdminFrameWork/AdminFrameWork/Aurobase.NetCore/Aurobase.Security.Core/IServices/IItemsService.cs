using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    public interface IItemsService:IService<Items, ItemsOutputDto, string>
    {

        /// <summary>
        /// ȡܲ˵Vue б
        /// </summary>
        /// <returns></returns>
        Task<List<ItemsOutputDto>> GetAllItemsTreeTable();

        /// <summary>
        /// ݱѯֵ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
        Task<Items> GetByEnCodAsynce(string enCode);


        /// <summary>
        /// ʱжϷǷڣųԼ
        /// </summary>
        /// <param name="enCode"></param
        /// <param name="id">Id</param>
        /// <returns></returns>
        Task<Items> GetByEnCodAsynce(string enCode, string id);
    }
}
