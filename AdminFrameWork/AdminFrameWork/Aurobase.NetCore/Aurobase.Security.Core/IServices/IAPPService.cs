using System;
using System.Collections.Generic;
using Aurobase.Commons.IServices;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAPPService:IService<APP, AppOutputDto, string>
    {
        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ӦID</param>
        /// <param name="secret">ӦԿAppSecret</param>
        /// <returns></returns>
        APP GetAPP(string appid, string secret);

        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ӦID</param>
        /// <returns></returns>
        APP GetAPP(string appid);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppOutputDto> SelectApp();
        /// <summary>
        /// ¿õӦõ
        /// </summary>
        void  UpdateCacheAllowApp();
    }
}
