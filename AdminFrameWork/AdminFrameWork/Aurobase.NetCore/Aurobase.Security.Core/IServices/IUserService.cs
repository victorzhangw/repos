using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Aurobase.Commons.Enums;
using Aurobase.Commons.IServices;
using Aurobase.Commons.Pages;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// ûӿ
    /// </summary>
    public interface IUserService:IService<User, UserOutputDto, string>
    {
        /// <summary>
        /// û½֤
        /// </summary>
        /// <param name="userName">û</param>
        /// <param name="password">루һmd5ܺ</param>
        /// <returns>֤ɹûʵ壬֤ʧܷnull|ʾϢ</returns>
        Task<Tuple<User, string>> Validate(string userName, string password);

        /// <summary>
        /// û½֤
        /// </summary>
        /// <param name="userName">û</param>
        /// <param name="password">루һmd5ܺ</param>
        /// <param name="userType">û</param>
        /// <returns>֤ɹûʵ壬֤ʧܷnull|ʾϢ</returns>
        Task<Tuple<User, string>> Validate(string userName, string password, UserType userType);

        /// <summary>
        /// û˺ŲѯûϢ
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<User> GetByUserName(string userName);

        /// <summary>
        /// ûֻѯûϢ
        /// </summary>
        /// <param name="mobilePhone">ֻ</param>
        /// <returns></returns>
        Task<User> GetUserByMobilePhone(string mobilePhone);
        /// <summary>
        /// EmailAccountֻŲѯûϢ
        /// </summary>
        /// <param name="account">¼˺</param>
        /// <returns></returns>
        Task<User> GetUserByLogin(string account);
        /// <summary>
        /// עû
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="trans"></param>
        bool Insert(User entity, UserLogOn userLogOnEntity, IDbTransaction trans = null);
        /// <summary>
        /// עû
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="trans"></param>
        Task<bool> InsertAsync(User entity, UserLogOn userLogOnEntity, IDbTransaction trans = null);
        /// <summary>
        /// עû,ƽ̨
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="userOpenIds"></param>
        /// <param name="trans"></param>
        bool Insert(User entity, UserLogOn userLogOnEntity, UserOpenIds userOpenIds,IDbTransaction trans = null);
        /// <summary>
        /// ݵOpenIdѯûϢ
        /// </summary>
        /// <param name="openIdType"></param>
        /// <param name="openId">OpenIdֵ</param>
        /// <returns></returns>
        User GetUserByOpenId(string openIdType, string openId);

        /// <summary>
        /// ΢UnionIdѯûϢ
        /// </summary>
        /// <param name="unionId">UnionIdֵ</param>
        /// <returns></returns>
        User GetUserByUnionId(string unionId);
        /// <summary>
        /// userIdѯûϢ
        /// </summary>
        /// <param name="openIdType"></param>
        /// <param name="userId">userId</param>
        /// <returns></returns>
        UserOpenIds GetUserOpenIdByuserId(string openIdType, string userId);
        /// <summary>
        /// ûϢ,ƽ̨
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="userLogOnEntity"></param>
        /// <param name="trans"></param>
        bool UpdateUserByOpenId(User entity, UserLogOn userLogOnEntity, UserOpenIds userOpenIds, IDbTransaction trans = null);

        


        /// <summary>
        /// ΢עͨԱû
        /// </summary>
        /// <param name="userInPut"></param>
        /// <returns></returns>
       bool CreateUserByWxOpenId(UserInputDto userInPut);
        /// <summary>
        /// û
        /// </summary>
        /// <param name="userInPut"></param>
        /// <returns></returns>
        bool UpdateUserByOpenId(UserInputDto userInPut);

        /// <summary>
        /// ѯݿ,ض󼯺(ڷҳʾ)
        /// </summary>
        /// <param name="search">ѯ</param>
        /// <returns>ָļ</returns>
        Task<PageResult<UserOutputDto>> FindWithPagerSearchAsync(SearchUserModel search);
    }
}
