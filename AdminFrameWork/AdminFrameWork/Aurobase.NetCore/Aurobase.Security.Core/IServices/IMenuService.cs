using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurobase.Commons.Core.Dtos;
using Aurobase.Commons.IServices;
using Aurobase.Commons.Models;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMenuService:IService<Menu, MenuOutputDto, string>
    {

        /// <summary>
        /// ûȡܲ˵
        /// </summary>
        /// <param name="userId">ûID</param>
        /// <returns></returns>
        List<Menu> GetMenuByUser(string userId);

        /// <summary>
        /// ȡܲ˵Vue б
        /// </summary>
        /// <param name="systemTypeId">ϵͳId</param>
        /// <returns></returns>
        Task<List<MenuTreeTableOutputDto>> GetAllMenuTreeTable(string systemTypeId);


        /// <summary>
        /// ݽɫIDַŷֿ)ϵͳIDȡӦĲб
        /// </summary>
        /// <param name="roleIds">ɫID</param>
        /// <param name="typeID">ϵͳID</param>
        /// <param name="isMenu">Ƿǲ˵</param>
        /// <returns></returns>
        List<Menu> GetFunctions(string roleIds, string typeID,bool isMenu=false);

        /// <summary>
        /// ϵͳIDȡӦĲб
        /// </summary>
        /// <param name="typeID">ϵͳID</param>
        /// <returns></returns>
        List<Menu> GetFunctions(string typeID);

        /// <summary>
        /// ݸܱѯӼܣҪҳťȨ
        /// </summary>
        /// <param name="enCode">˵ܱ</param>
        /// <returns></returns>
        Task<IEnumerable<MenuOutputDto>> GetListByParentEnCode(string enCode);

        /// <summary>
        /// ɾ
        /// </summary>
        /// <param name="ids">Id</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        CommonResult DeleteBatchWhere(DeletesInputDto ids, IDbTransaction trans = null);
        /// <summary>
        /// 첽ɾ
        /// </summary>
        /// <param name="ids">Id</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        Task<CommonResult> DeleteBatchWhereAsync(DeletesInputDto ids, IDbTransaction trans = null);
    }
}
