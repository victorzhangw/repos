
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aurobase.Security.Models
{
    /// <summary>
    /// 第三方登錄與使用者繫結表，資料實體物件
    /// </summary>
    [Table("Sys_UserOpenIds")]
    public class UserOpenIds:BaseEntity<string>
    {
        #region Property Members
        /// <summary>
        /// 使用者編號
        /// </summary>
        public virtual string UserId { get; set; }

        /// <summary>
        /// 第三方型別
        /// </summary>
        public virtual string OpenIdType { get; set; }

        /// <summary>
        /// OpenId
        /// </summary>
        public virtual string OpenId { get; set; }

        #endregion

    }
}