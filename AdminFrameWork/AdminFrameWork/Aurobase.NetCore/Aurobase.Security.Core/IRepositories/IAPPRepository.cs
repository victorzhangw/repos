using System;
using System.Collections.Generic;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Dtos;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAPPRepository:IRepository<APP,string>
    {
        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ӦID</param>
        /// <param name="secret">ӦԿAppSecret</param>
        /// <returns></returns>
        APP GetAPP(string appid, string secret);

        /// <summary>
        /// ȡapp
        /// </summary>
        /// <param name="appid">ӦID</param>
        /// <returns></returns>
        APP GetAPP(string appid);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppOutputDto> SelectApp();
    }
}