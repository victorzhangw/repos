using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    /// <summary>
    /// 定義序號編碼規則表倉儲介面
    /// </summary>
    public interface ISequenceRuleRepository:IRepository<SequenceRule, string>
    {
    }
}