using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface IFilterIPRepository:IRepository<FilterIP, string>
    {
        /// <summary>
        /// ֤IPַǷ񱻾ܾ
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        bool ValidateIP(string ip);
    }
}