using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface IRoleAuthorizeRepository:IRepository<RoleAuthorize,string>
    {
        /// <summary>
        /// ɫȨ
        /// </summary>
        /// <param name="roleId">ɫId</param>
        /// <param name="roleAuthorizesList">ɫģ</param>
        /// <param name="roleDataList">ɫɷ</param>
        /// <param name="trans"></param>
        /// <returns>ִгɹ<c>true</c>Ϊ<c>false</c></returns>
        Task<bool> SaveRoleAuthorize(string roleId,List<RoleAuthorize> roleAuthorizesList, List<RoleData> roleDataList,
           IDbTransaction trans = null);
    }
}