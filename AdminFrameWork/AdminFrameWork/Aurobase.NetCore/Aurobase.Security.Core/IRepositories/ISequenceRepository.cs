using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    /// <summary>
    /// 定義單據編碼倉儲介面
    /// </summary>
    public interface ISequenceRepository:IRepository<Sequence, string>
    {
    }
}