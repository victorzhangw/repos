using System;
using System.Threading.Tasks;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface IItemsRepository:IRepository<Items, string>
    {
        /// <summary>
        /// ݱѯֵ
        /// </summary>
        /// <param name="enCode"></param>
        /// <returns></returns>
       Task<Items> GetByEnCodAsynce(string enCode);
        /// <summary>
        /// ʱжϷǷڣųԼ
        /// </summary>
        /// <param name="enCode"></param
        /// <param name="id">Id</param>
        /// <returns></returns>
        Task<Items> GetByEnCodAsynce(string enCode, string id);
    }
}