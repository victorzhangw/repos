using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface IUserLogOnRepository:IRepository<UserLogOn, string>
    {
        /// <summary>
        /// ݻԱIDȡû¼Ϣʵ
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserLogOn GetByUserId(string userId);
    }
}