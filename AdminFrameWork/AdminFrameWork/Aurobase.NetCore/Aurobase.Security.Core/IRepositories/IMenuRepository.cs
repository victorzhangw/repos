using System;
using System.Collections.Generic;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    public interface IMenuRepository:IRepository<Menu, string>
    {

        /// <summary>
        /// ݽɫIDַŷֿ)ϵͳIDȡӦĲб
        /// </summary>
        /// <param name="roleIds">ɫID</param>
        /// <param name="typeID">ϵͳID</param>
        /// <param name="isMenu">Ƿǲ˵</param>
        /// <returns></returns>
        IEnumerable<Menu> GetFunctions(string roleIds, string typeID, bool isMenu = false);

        /// <summary>
        /// ϵͳIDȡӦĲб
        /// </summary>
        /// <param name="typeID">ϵͳID</param>
        /// <returns></returns>
        IEnumerable<Menu> GetFunctions(string typeID);
    }
}