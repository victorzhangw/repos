using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.IRepositories
{
    /// <summary>
    /// ִ֯ӿ
    /// õOrganizeҵ
    /// </summary>
    public interface IOrganizeRepository:IRepository<Organize, string>
    {
        /// <summary>
        /// ȡڵ֯
        /// </summary>
        /// <param name="id">֯Id</param>
        /// <returns></returns>
        Organize GetRootOrganize(string id);
    }
}