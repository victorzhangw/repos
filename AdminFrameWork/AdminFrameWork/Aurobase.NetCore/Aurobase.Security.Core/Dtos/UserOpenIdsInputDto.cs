using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(UserOpenIds))]
    [Serializable]
    public class UserOpenIdsInputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string OpenIdType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string OpenId { get; set; }


    }
}
