using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class AppOutputDto: IOutputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string AppId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string AppSecret { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(256)]
        public string EncodingAESKey { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(256)]
        public string RequestUrl { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(256)]
        public string Token { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsOpenAEKey { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string CompanyId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string DeptId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual User UserInfo { get; set; }


    }
}
