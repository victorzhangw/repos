using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(UserLogOn))]
    [Serializable]
    public class UserLogOnInputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string UserPassword { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string UserSecretkey { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? AllowStartTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? AllowEndTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LockStartDate { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LockEndDate { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? FirstVisitTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? PreviousVisitTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastVisitTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? ChangePasswordDate { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? MultiUserLogin { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? LogOnCount { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? UserOnLine { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string AnswerQuestion { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? CheckIPAddress { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Theme { get; set; }


    }
}
