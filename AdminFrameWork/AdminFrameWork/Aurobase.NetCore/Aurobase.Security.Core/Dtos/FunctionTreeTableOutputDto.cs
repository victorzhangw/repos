using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// vue樹形表
    /// </summary>
    [Serializable]
    public class FunctionTreeTableOutputDto
    {

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string SystemTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SystemTypeName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string ParentId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string EnCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string FullName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Icon { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string UrlAddress { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Target { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsMenu { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsExpand { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsPublic { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

        /// <summary>
        /// 子菜單
        /// </summary>
        public List<FunctionTreeTableOutputDto> Children { get; set; }

        /// <summary>
        /// 系統標記
        /// </summary>
        public bool SystemTag { get; set; }
    }
}
