using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class UserOpenIdsOutputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(256)]
        public string OpenIdType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(128)]
        public string OpenId { get; set; }


    }
}
