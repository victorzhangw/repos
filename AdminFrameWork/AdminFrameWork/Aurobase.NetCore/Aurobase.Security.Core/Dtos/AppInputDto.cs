using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(APP))]
    [Serializable]
    public class APPInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EncodingAESKey { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool IsOpenAEKey { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


    }
}
