using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class RoleAuthorizeOutputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? ItemType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string ItemId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? ObjectType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string ObjectId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        public List<RoleAuthorizeOutputDto> Children { get; set; }
    }
}
