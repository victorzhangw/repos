using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Area))]
    [Serializable]
    public class AreaInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SimpleSpelling { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FullIdPath { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsLast { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


    }
}
