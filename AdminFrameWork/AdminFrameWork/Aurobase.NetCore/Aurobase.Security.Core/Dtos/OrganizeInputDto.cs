using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Organize))]
    [Serializable]
    public class OrganizeInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string CategoryId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ManagerId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string TelePhone { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool EnabledMark { get; set; }
        /// <summary>
        /// Get / Set
        /// </summary>
        public bool DeleteMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


    }
}
