using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class UserLogOnOutputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string UserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string UserPassword { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string UserSecretkey { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? AllowStartTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? AllowEndTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LockStartDate { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LockEndDate { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? FirstVisitTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? PreviousVisitTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastVisitTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? ChangePasswordDate { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? MultiUserLogin { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? LogOnCount { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? UserOnLine { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Question { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string AnswerQuestion { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? CheckIPAddress { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Language { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Theme { get; set; }


    }
}
