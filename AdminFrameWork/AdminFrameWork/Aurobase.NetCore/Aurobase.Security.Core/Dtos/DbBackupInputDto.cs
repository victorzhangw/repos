using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(DbBackup))]
    [Serializable]
    public class DbBackupInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string BackupType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string DbName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FileSize { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? BackupTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


    }
}
