using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Log))]
    [Serializable]
    public class LogInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string IPAddressName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ModuleId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? Result { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

    }
}
