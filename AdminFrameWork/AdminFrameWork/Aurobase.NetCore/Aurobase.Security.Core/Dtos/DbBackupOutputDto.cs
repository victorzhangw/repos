using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class DbBackupOutputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string BackupType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string DbName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string FileName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string FileSize { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string FilePath { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? BackupTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(500)]
        public string DeleteUserId { get; set; }


    }
}
