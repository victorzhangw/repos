using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Menu))]
    [Serializable]
    public class MenuInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SystemTypeId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? Layers { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// 設定或獲取路由
        /// </summary>
        public virtual string UrlAddress { get; set; }

        /// <summary>
        /// 設定或獲取目標打開方式
        /// </summary>
        public virtual string Target { get; set; }

        /// <summary>
        /// 設定或獲取菜單型別（C目錄 M菜單 F按鈕）
        /// </summary>
        public virtual string MenuType { get; set; }
        /// <summary>
        /// 設定或獲取元件路徑
        /// </summary>
        public virtual string Component { get; set; }
        /// <summary>
        /// 設定目前選中菜單，用於新增、編輯、檢視操作為單獨的路由時指定選中菜單路由
        /// 同時設定為隱藏時才有效
        /// </summary>
        public virtual string ActiveMenu { get; set; }
        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsExpand { get; set; }
        /// <summary>
        /// Get / Set是否顯示
        /// </summary>
        public bool? IsShow { get; set; }
        /// <summary>
        /// 設定或獲取是否快取
        /// </summary>
        public bool? IsCache { get; set; }
        /// <summary>
        /// Get / Set是否外鏈
        /// </summary>
        public bool? IsFrame { get; set; }
        /// <summary>
        /// Get / Set批量新增
        /// </summary>
        public bool IsBatch { get; set; }
        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsPublic { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// 建立日期
        /// </summary>
        public virtual DateTime? CreatorTime { get; set; }



        /// <summary>
        /// Get / Set
        /// </summary>
        public string SystemTypeName { get; set; }
    }
}
