using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(UploadFile))]
    [Serializable]
    public class UploadFileInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FileType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? FileSize { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Thumbnail { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string BelongApp { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string BelongAppId { get; set; }


    }
}
