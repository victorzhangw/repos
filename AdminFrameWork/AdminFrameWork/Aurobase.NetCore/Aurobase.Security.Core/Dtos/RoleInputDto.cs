using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(Role))]
    [Serializable]
    public class RoleInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? Category { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowEdit { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? AllowDelete { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


    }
}
