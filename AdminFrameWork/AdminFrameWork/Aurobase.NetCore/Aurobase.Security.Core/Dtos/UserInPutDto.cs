using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Security.Models;

namespace Aurobase.Security.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(User))]
    [Serializable]
    public class UserInputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string HeadIcon { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? Gender { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string ManagerId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SecurityLevel { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Signature { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string OrganizeId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string DepartmentId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string DutyId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsAdministrator { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool? IsMember { get; set; }
        /// <summary>
        /// 語言
        /// </summary>
        public virtual string language { get; set; }
        /// <summary>
        /// OpenId
        /// </summary>
        public virtual string OpenId { get; set; }
        /// <summary>
        /// 第三方登錄系統型別
        /// </summary>
        public virtual string OpenIdType { get; set; }
        /// <summary>
        /// 會員等級
        /// </summary>
        public string MemberGradeId { get; set; }

        /// <summary>
        /// 上級推廣員
        /// </summary>
        public string ReferralUserId { get; set; }

        /// <summary>
        /// 使用者在微信開放平臺的唯一識別符號
        /// </summary>
        public string UnionId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public int? SortCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool EnabledMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }


    }
}
