using Dapper;
using System.Collections.Generic;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    public class MenuRepository : BaseRepository<Menu, string>, IMenuRepository
    {
        public MenuRepository()
        {
        }

        public MenuRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }


        /// <summary>
        /// ݽɫIDַŷֿ)ϵͳIDȡӦĲб
        /// </summary>
        /// <param name="roleIds">ɫID</param>
        /// <param name="typeID">ϵͳID</param>
        /// <param name="isMenu">Ƿǲ˵</param>
        /// <returns></returns>
        public IEnumerable<Menu> GetFunctions(string roleIds, string typeID,bool isMenu = false)
        {
            string sql = $"SELECT DISTINCT b.* FROM sys_menu as b INNER JOIN Sys_RoleAuthorize as a On b.Id = a.ItemId  WHERE ObjectId IN (" + roleIds + ")";
            if (roleIds == "")
            {
                sql = $"SELECT DISTINCT b.* FROM sys_menu as b where 1=1 ";
            }
            if (isMenu)
            {
                sql = sql + "and menutype in('M','C')";
            }
            if (!string.IsNullOrEmpty(typeID))
            {
                sql = sql + string.Format(" AND SystemTypeId='{0}' ", typeID);
            }
            return DapperConnRead.Query<Menu>(sql);
        }


        /// <summary>
        /// ϵͳIDȡӦĲб
        /// </summary>
        /// <param name="typeID">ϵͳID</param>
        /// <returns></returns>
        public IEnumerable<Menu> GetFunctions(string typeID)
        {
            string sql = $"SELECT DISTINCT b.* FROM sys_menu as b ";
            if (!string.IsNullOrEmpty(typeID))
            {
                sql = sql + string.Format(" Where SystemTypeId='{0}' ", typeID);
            }
            return DapperConnRead.Query<Menu>(sql);
        }
    }
}