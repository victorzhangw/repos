using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Security.Repositories
{
    public class RoleDataRepository : BaseRepository<RoleData, string>, IRoleDataRepository
    {
		public RoleDataRepository()
        {
        }

        public RoleDataRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// ݽɫȨʲ
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        public List<string> GetListDeptByRole(string roleIds)
        {
            string roleIDsStr = string.Format("'{0}'", roleIds.Replace(",", "','"));
            string where = " RoleId in(" + roleIDsStr + ") and DType='dept'";
            string sql = $"select AuthorizeData from { tableName} ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sql += " where " + where;
            }
            using (IDbConnection connection = DapperConn)
            {
                bool isClosed = connection.State == ConnectionState.Closed;
                if (isClosed) connection.Open();
                IEnumerable<String> resultList = connection.Query<String>(sql);
                return resultList.ToList();
            }
        }

    }
}