/*******************************************************************************
 * Copyright © 2017-2020 Aurobase.Framework 版權所有
 * Author: Aurobase
 * Description: Aurobase快速開發平臺
 * Website：http://www.Aurobase.com
*********************************************************************************/
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Encrypt
{
    /// <summary>
    /// MD5各種長度加密字元、驗證MD5等操作輔助類
    /// </summary>
    public class MD5Util
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        public MD5Util()
        {
        }

        /// <summary>
        /// 獲得32位的MD5加密
        /// </summary>
        public static string GetMD5_32(string input)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 獲得16位的MD5加密
        /// </summary>
        public static string GetMD5_16(string input)
        {
            return GetMD5_32(input).Substring(8, 16);
        }

        /// <summary>
        /// 獲得8位的MD5加密
        /// </summary>
        public static string GetMD5_8(string input)
        {
            return GetMD5_32(input).Substring(8, 8);
        }

        /// <summary>
        /// 獲得4位的MD5加密
        /// </summary>
        public static string GetMD5_4(string input)
        {
            return GetMD5_32(input).Substring(8, 4);
        }

        /// <summary>
        /// 新增MD5的字首，便於檢查有無篡改
        /// </summary>
        public static string AddMD5Profix(string input)
        {
            return GetMD5_4(input) + input;
        }

        /// <summary>
        /// 移除MD5的字首
        /// </summary>
        public static string RemoveMD5Profix(string input)
        {
            return input.Substring(4);
        }

        /// <summary>
        /// 驗證MD5字首處理的字串有無被篡改
        /// </summary>
        public static bool ValidateValue(string input)
        {
            bool res = false;
            if (input.Length >= 4)
            {
                string tmp = input.Substring(4);
                if (input.Substring(0, 4) == GetMD5_4(tmp))
                {
                    res = true;
                }
            }
            return res;
        }

        #region MD5簽名驗證

        /// <summary>
        /// 對給定檔案路徑的檔案加上標籤
        /// </summary>
        /// <param name="path">要加密的檔案的路徑</param>
        /// <returns>標籤的值</returns>
        public static bool AddMD5(string path)
        {
            bool IsNeed = true;

            if (CheckMD5(path)) //已進行MD5處理
                IsNeed = false;

            try
            {
                FileStream fsread = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                byte[] md5File = new byte[fsread.Length];
                fsread.Read(md5File, 0, (int)fsread.Length);                               // 將檔案流讀取到Buffer中
                fsread.Close();

                if (IsNeed)
                {
                    string result = MD5Buffer(md5File, 0, md5File.Length);             // 對Buffer中的位元組內容算MD5
                    byte[] md5 = System.Text.Encoding.ASCII.GetBytes(result);       // 將字串轉換成位元組陣列以便寫人到檔案中
                    FileStream fsWrite = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                    fsWrite.Write(md5File, 0, md5File.Length);                               // 將檔案，MD5值 重新寫入到檔案中。
                    fsWrite.Write(md5, 0, md5.Length);
                    fsWrite.Close();
                }
                else
                {
                    FileStream fsWrite = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                    fsWrite.Write(md5File, 0, md5File.Length);
                    fsWrite.Close();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 對給定路徑的檔案進行驗證，如果一致返回True，否則返回False
        /// </summary>
        /// <param name="path"></param>
        /// <returns>是否加了標籤或是否標籤值與內容值一致</returns>
        public static bool CheckMD5(string path)
        {
            try
            {
                FileStream get_file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                byte[] md5File = new byte[get_file.Length];                                      // 讀入檔案
                get_file.Read(md5File, 0, (int)get_file.Length);
                get_file.Close();

                string result = MD5Buffer(md5File, 0, md5File.Length - 32);             // 對檔案除最後32位以外的位元組計算MD5，這個32是因為標籤位為32位。
                string md5 = System.Text.Encoding.ASCII.GetString(md5File, md5File.Length - 32, 32);   //讀取檔案最後32位，其中儲存的就是MD5值
                return result == md5;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 計算檔案的MD5值
        /// </summary>
        /// <param name="MD5File">MD5簽名檔案字元陣列</param>
        /// <param name="index">計算起始位置</param>
        /// <param name="count">計算終止位置</param>
        /// <returns>計算結果</returns>
        private static string MD5Buffer(byte[] MD5File, int index, int count)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider get_md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hash_byte = get_md5.ComputeHash(MD5File, index, count);
            string result = System.BitConverter.ToString(hash_byte);

            result = result.Replace("-", "");
            return result;
        }
        #endregion

    }
}
