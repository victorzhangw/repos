using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurobase.Commons.Enums;

namespace Aurobase.Commons.Core.DataManager
{
    public class AurobaseDbOptions
    {
        public AurobaseDbOptions()
        {
        }

        /// <summary>
        /// 預設資料庫型別
        /// </summary>
        public DatabaseType DefaultDatabaseType { get; set; } = DatabaseType.SqlServer;

        /// <summary>
        /// 資料庫連線設定
        /// </summary>
        public IDictionary<string, DbConnectionOptions> DbConnections { get; set; }
    }
}
