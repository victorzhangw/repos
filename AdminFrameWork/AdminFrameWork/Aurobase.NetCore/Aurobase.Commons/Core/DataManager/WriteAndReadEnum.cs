using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.Commons.DataManager
{
    /// <summary>
    /// 資料庫讀、寫操作列舉
    /// </summary>
    public enum WriteAndReadEnum
    {
        /// <summary>
        /// 寫操作
        /// </summary>
        Write,
        /// <summary>
        /// 讀操作
        /// </summary>
        Read,
        /// <summary>
        /// 預設，不區分讀寫
        /// </summary>
        Default
    }
}
