using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Core.DataManager
{
    /// <summary>
    /// 資料庫連線設定特性
    /// </summary>
    public class AppDBContextAttribute : Attribute
    {
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        public string DbConfigName { get; set; }
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="dbConfigName"></param>
        public AppDBContextAttribute(string dbConfigName)
        {
            DbConfigName = dbConfigName;
        }
    }
}
