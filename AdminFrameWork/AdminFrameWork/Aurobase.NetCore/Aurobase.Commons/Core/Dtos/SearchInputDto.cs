using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Aurobase.Commons.Pages;

namespace Aurobase.Commons.Dtos
{
    /// <summary>
    /// 查詢條件公共實體類
    /// </summary>
    [Serializable]
    [DataContract]
    public class SearchInputDto<T> : PagerInfo
    {
        /// <summary>
        /// 關鍵詞
        /// </summary>
        public string Keywords
        {
            get; set;
        }
        /// <summary>
        /// 編碼/程式碼
        /// </summary>
        public string EnCode
        {
            get; set;
        }
        /// <summary>
        /// 排序方式 預設asc 
        /// </summary>
        public string Order
        {
            get; set;
        }
        /// <summary>
        /// 排序欄位 預設Id
        /// </summary>
        public string Sort
        {
            get; set;
        }

        /// <summary>
        /// 查詢條件
        /// </summary>
        public T Filter { get; set; }
    }
}
