using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aurobase.Commons.Core.Dapper;
using Aurobase.Commons.Core.DataManager;
using Aurobase.Commons.DataManager;
using Aurobase.Commons.DbContextCore;
using Aurobase.Commons.DependencyInjection;
using Aurobase.Commons.Enums;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.IRepositories;
using Aurobase.Commons.Json;
using Aurobase.Commons.Log;
using Aurobase.Commons.Models;
using Aurobase.Commons.Pages;
using static Dapper.SqlMapper;

namespace Aurobase.Commons.Repositories
{
    /// <summary>
    /// 泛型倉儲，實現泛型倉儲介面
    /// </summary>
    /// <typeparam name="T">實體型別</typeparam>
    /// <typeparam name="TKey">實體主鍵型別</typeparam>
    public abstract class BaseRepository<T, TKey> : IRepository<T, TKey> ,ITransientDependency
        where T : Entity
    {
        #region 建構函式及基本設定
        /// <summary>
        ///  EF DBContext
        /// </summary>
        private IDbContextCore _dbContext;
        private IDbContextFactory _dbContextFactory;
        /// <summary>
        /// 
        /// </summary>
        protected DbSet<T> DbSet => DbContext.GetDbSet<T>();
        /// <summary>
        /// 獲取訪問資料庫設定
        /// </summary>
        protected DbConnectionOptions dbConnectionOptions = DBServerProvider.GeDbConnectionOptions<T>();
        /// <summary>
        /// 需要初始化的物件表名
        /// </summary>
        protected string tableName= typeof(T).GetCustomAttribute<TableAttribute>(false)?.Name;
        /// <summary>
        /// 資料庫參數化訪問的佔位符
        /// </summary>
        protected string parameterPrefix = "@";
        /// <summary>
        /// 防止和保留字、關鍵字同名的欄位格式，如[value]
        /// </summary>
        protected string safeFieldFormat = "[{0}]";
        /// <summary>
        /// 資料庫的主鍵欄位名,若主鍵不是Id請過載BaseRepository設定
        /// </summary>
        protected string primaryKey="Id";
        /// <summary>
        /// 排序欄位
        /// </summary>
        protected string sortField;
        /// <summary>
        /// 是否為降序
        /// </summary>
        protected bool isDescending = true;
        /// <summary>
        /// 選擇的欄位，預設為所有(*) 
        /// </summary>
        protected string selectedFields = " * ";
        /// <summary>
        /// 是否開啟多租戶
        /// </summary>
        protected bool isMultiTenant = false;


        /// <summary>
        /// 排序欄位
        /// </summary>
        public string SortField
        {
            get
            {
                return sortField;
            }
            set
            {
                sortField = value;
            }
        }

        /// <summary>
        /// 資料庫訪問物件的外來鍵約束
        /// </summary>
        public string PrimaryKey
        {
            get
            {
                return primaryKey;
            }
        }


        /// <summary>
        /// 構造方法
        /// </summary>
        public BaseRepository()
        {
        }

        /// <summary>
        /// 構造方法，注入上下文
        /// </summary>
        /// <param name="dbContext">上下文</param>
        public BaseRepository(IDbContextCore dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));
            _dbContext = dbContext;
            _dbContext.EnsureCreated();
        }

        /// <summary>
        /// 構造方法，注入上下文
        /// </summary>
        /// <param name="dbContextFactory">上下文</param>
        public BaseRepository(IDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        #endregion

        #region Dapper 操作


        /// <summary>
        /// 用Dapper原生方法運算元據，支援讀寫操作
        /// </summary>
        public IDbConnection DapperConn
        {
            get { return new DapperDbContext().GetConnection<T>(); }
        }

        /// <summary>
        /// 用Dapper原生方法，僅用於只讀資料庫
        /// </summary>
        public IDbConnection DapperConnRead
        {
            get { return new DapperDbContext().GetConnection<T>(false); }
        }

        #region 查詢獲得物件和列表
        /// <summary>
        /// 根據id獲取一個物件
        /// </summary>
        /// <param name="primaryKey">主鍵</param>
        /// <returns></returns>
        public virtual T Get(TKey primaryKey)
        {
            return DapperConnRead.Get<T>(primaryKey);
        }
        /// <summary>
        /// 非同步根據id獲取一個物件
        /// </summary>
        /// <param name="primaryKey">主鍵</param>
        /// <returns></returns>
        public virtual async  Task<T> GetAsync(TKey primaryKey)
        {
            return await DapperConnRead.GetAsync<T>(primaryKey);
        }
        /// <summary>
        /// 根據條件獲取一個物件
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual T GetWhere(string where)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"select * from { tableName} ";
            sql += " where " + where;
            return DapperConnRead.QueryFirstOrDefault<T>(sql);
        }

        /// <summary>
        /// 根據條件非同步獲取一個物件
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public virtual async Task<T> GetWhereAsync(string where)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string  sql = $"select * from { tableName} ";
            sql += " where "+where;

            return await DapperConnRead.QueryFirstOrDefaultAsync<T>(sql);
        }

        /// <summary>
        /// 獲取所有資料，謹慎使用
        /// </summary>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll(IDbTransaction trans=null)
        {
            return GetListWhere();
        }
        /// <summary>
        /// 獲取所有資料，謹慎使用
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllAsync( IDbTransaction trans=null)
        {
            return await GetListWhereAsync();
        }


        /// <summary>
        /// 根據查詢條件獲取資料集合
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetListWhere(string where = null, IDbTransaction trans=null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sql = $"select {selectedFields} from { tableName} ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sql += " where " + where;
            }
            return DapperConnRead.Query<T>(sql, trans);
        }

        /// <summary>
        /// 根據查詢條件獲取資料集合
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetListWhereAsync(string where = null, IDbTransaction trans=null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sql = $"select {selectedFields}  from { tableName} ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sql += " where " + where;
            }
            return await DapperConnRead.QueryAsync<T>(sql, trans);
        }

        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetListTopWhere(int top, string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            
            string sql =  $"select top {top} {selectedFields} from " + tableName; ;
            if (dbConnectionOptions.DatabaseType==DatabaseType.SqlServer)
            {
                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
            }
            else if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select {selectedFields} from " + tableName;

                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
                sql += $"  LIMIT 0,{top}; ";
            }
            return DapperConnRead.Query<T>(sql, trans);
        }


        /// <summary>
        /// 根據查詢條件查詢前多少條資料
        /// </summary>
        /// <param name="top">多少條資料</param>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetListTopWhereAsync(int top, string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sql = $"select top {top} {selectedFields} from " + tableName;
            if (dbConnectionOptions.DatabaseType == DatabaseType.SqlServer)
            {
                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
            }
            else if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select {selectedFields} from " + tableName;

                if (!string.IsNullOrWhiteSpace(where))
                {
                    sql += " where " + where;
                }
                sql += $"  LIMIT 0,{top}; ";
            }
            return await DapperConnRead.QueryAsync<T>(sql, trans);
        }
        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsDeleteMark(string where = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " DeleteMark=1 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return GetListWhere(sqlWhere,trans);
        }

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotDeleteMark(string where = null, IDbTransaction trans=null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " DeleteMark=0 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return GetListWhere(sqlWhere, trans);
        }

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsEnabledMark(string where = null, IDbTransaction trans=null)
        {
            
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " EnabledMark=1 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return GetListWhere(sqlWhere, trans);
        }

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotEnabledMark(string where = null, IDbTransaction trans = null)
        {
            
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " EnabledMark=0 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return GetListWhere(sqlWhere, trans);
        }
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAllByIsNotDeleteAndEnabledMark(string where = null, IDbTransaction trans = null)
        {
            
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " DeleteMark=0 and EnabledMark=1 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return GetListWhere(sqlWhere, trans);
        }

        /// <summary>
        /// 查詢軟刪除的資料，如果查詢條件為空，即查詢所有軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsDeleteMarkAsync(string where = null, IDbTransaction trans = null)
        {
            
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " DeleteMark=1";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return await GetListWhereAsync(sqlWhere, trans);
        }

        /// <summary>
        /// 查詢未軟刪除的資料，如果查詢條件為空，即查詢所有未軟刪除的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotDeleteMarkAsync(string where = null, IDbTransaction trans = null)
        {
            
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }

            string sqlWhere = " DeleteMark=0 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return await GetListWhereAsync(sqlWhere, trans);
        }

        /// <summary>
        /// 查詢有效的資料，如果查詢條件為空，即查詢所有有效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsEnabledMarkAsync(string where = null, IDbTransaction trans = null)
        {
            
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }

            string sqlWhere = " EnabledMark=1 ";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return await GetListWhereAsync(sqlWhere, trans);
        }

        /// <summary>
        /// 查詢無效的資料，如果查詢條件為空，即查詢所有無效的資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotEnabledMarkAsync(string where = null, IDbTransaction trans = null)
        {

            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " EnabledMark=0";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return await GetListWhereAsync(sqlWhere, trans);
        }
        /// <summary>
        /// 查詢未軟刪除且有效的資料，如果查詢條件為空，即查詢所有資料
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllByIsNotDeleteAndEnabledMarkAsync(string where = null, IDbTransaction trans=null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            string sqlWhere = " DeleteMark=0 and EnabledMark=1";
            if (!string.IsNullOrWhiteSpace(where))
            {
                sqlWhere += " and " + where;
            }
            return await GetListWhereAsync(sqlWhere, trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, IDbTransaction trans = null)
        {
           return FindWithPager(condition, info, this.SortField, this.isDescending, trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null)
        {
            return FindWithPager(condition, info, fieldToSort, this.isDescending, trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, IDbTransaction trans = null)
        {
            return await FindWithPagerAsync(condition, info, fieldToSort, this.isDescending, trans);
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, IDbTransaction trans = null)
        {
            return await FindWithPagerAsync(condition, info, this.SortField, trans);
        }
        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual List<T> FindWithPager(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            List<T> list = new List<T>();
            
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            PagerHelper pagerHelper = new PagerHelper(this.tableName, this.selectedFields, fieldToSort, info.PageSize, info.CurrenetPageIndex, desc, condition);

            string pageSql = pagerHelper.GetPagingSql(true, dbConnectionOptions.DatabaseType);
            pageSql += ";" + pagerHelper.GetPagingSql(false, dbConnectionOptions.DatabaseType);

            var reader = DapperConnRead.QueryMultiple(pageSql);
            info.RecordCount = reader.ReadFirst<int>();
            list = reader.Read<T>().AsList();
            return list;
        }

        /// <summary>
        /// 根據條件查詢資料庫,並返回物件集合(用於分頁資料顯示)
        /// </summary>
        /// <param name="condition">查詢的條件</param>
        /// <param name="info">分頁實體</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans">事務物件</param>
        /// <returns>指定物件的集合</returns>
        public virtual async Task<List<T>> FindWithPagerAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {

            List<T> list = new List<T>();
            
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }

            PagerHelper pagerHelper = new PagerHelper(this.tableName, this.selectedFields, fieldToSort, info.PageSize, info.CurrenetPageIndex, desc, condition);

            string pageSql = pagerHelper.GetPagingSql(true, dbConnectionOptions.DatabaseType);
            pageSql += ";" + pagerHelper.GetPagingSql(false, dbConnectionOptions.DatabaseType);

            var reader = await DapperConnRead.QueryMultipleAsync(pageSql);
            info.RecordCount = reader.ReadFirst<int>();
            list = reader.Read<T>().AsList();
            return list;
        }

        /// <summary>
        /// 分頁查詢，自行封裝sql語句(僅支援sql server)
        /// 非常複雜的查詢，可在具體業務模組重寫該方法
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual List<T> FindWithPagerSql(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            List<T> list = new List<T>();
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始記錄
            int endNum = info.CurrenetPageIndex * info.PageSize;//結束記錄
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select {0} FROM {1} where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by {0}) AS rows ,{1} FROM {2} where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);
            var reader = DapperConnRead.QueryMultiple(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            list = reader.Read<T>().AsList();
            return list;
        }

        /// <summary>
        /// 分頁查詢，自行封裝sql語句(僅支援sql server)
        /// 非常複雜的查詢，可在具體業務模組重寫該方法
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位</param>
        /// <param name="desc">排序方式 true為desc，false為asc</param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> FindWithPagerSqlAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始記錄
            int endNum = info.CurrenetPageIndex * info.PageSize;//結束記錄
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select {0} FROM {1} where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by {0}) AS rows ,{1} FROM {2} where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);
            var reader = await DapperConnRead.QueryMultipleAsync(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            List<T> list  = reader.Read<T>().AsList();
            return list;
        }
        /// <summary>
        /// 分頁查詢包含使用者資訊(僅支援sql server)
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表字段
        /// 使用者資訊主要有使用者賬號：Account、昵稱：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        public virtual List<object> FindWithPagerRelationUser(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始記錄
            int endNum = info.CurrenetPageIndex * info.PageSize;//結束記錄
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select t1.{0} FROM {1} t1 inner join Sys_User t2 on t1.CreatorUserId = t2.Id where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM (SELECT ROW_NUMBER() OVER (order by  {0}) AS rows ,t1.{1},t2.Account as Account,t2.NickName as NickName,t2.RealName as RealName,t2.HeadIcon as HeadIcon ,t2.MobilePhone as MobilePhone  FROM {2} t1 inner join Sys_User t2 on t1.CreatorUserId = t2.Id " +
                "where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);

            var reader = DapperConnRead.QueryMultiple(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            List<object> list = reader.Read<object>().AsList();
            return list;
        }

        /// <summary>
        /// 分頁查詢包含使用者資訊(僅支援sql server)
        /// 查詢主表別名為t1,使用者表別名為t2，在查詢欄位需要注意使用t1.xxx格式，xx表示主表字段
        /// 使用者資訊主要有使用者賬號：Account、昵稱：NickName、真實姓名：RealName、頭像：HeadIcon、手機號：MobilePhone
        /// 輸出物件請在Dtos中進行自行封裝，不能是使用實體Model類
        /// </summary>
        /// <param name="condition">查詢條件欄位需要加表別名</param>
        /// <param name="info">分頁資訊</param>
        /// <param name="fieldToSort">排序欄位，也需要加表別名</param>
        /// <param name="desc">排序方式</param>
        /// <param name="trans">事務</param>
        /// <returns></returns>
        public virtual async Task<List<object>> FindWithPagerRelationUserAsync(string condition, PagerInfo info, string fieldToSort, bool desc, IDbTransaction trans = null)
        {
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            StringBuilder sb = new StringBuilder();
            int startRows = (info.CurrenetPageIndex - 1) * info.PageSize + 1;//起始記錄
            int endNum = info.CurrenetPageIndex * info.PageSize;//結束記錄
            string strOrder = string.Format(" {0} {1}", fieldToSort, desc ? "DESC" : "ASC");
            sb.AppendFormat("SELECT count(*) as RecordCount FROM (select t1.{0} FROM {1} t1 inner join Sys_User t2 on t1.CreatorUserId = t2.Id where {2})  AS main_temp;", primaryKey, tableName, condition);
            sb.AppendFormat("SELECT * FROM (SELECT ROW_NUMBER() OVER (order by  {0}) AS rows ,t1.{1},t2.Account as Account,t2.NickName as NickName,t2.RealName as RealName,t2.HeadIcon as HeadIcon ,t2.MobilePhone as MobilePhone  FROM {2} t1 inner join Sys_User t2 on t1.CreatorUserId = t2.Id " +
                "where {3}) AS main_temp where rows BETWEEN {4} and {5}", strOrder, selectedFields, tableName, condition, startRows, endNum);

            var reader = await DapperConnRead.QueryMultipleAsync(sb.ToString());
            info.RecordCount = reader.ReadFirst<int>();
            List<object> list = reader.Read<object>().AsList();
            return list;
        }
        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="fieldName">統計欄位名稱</param>
        /// <returns></returns>
        public virtual int GetCountByWhere(string condition,string fieldName="*")
        {
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            string sql = $"select count({fieldName}) from {tableName}  where ";
            if (!string.IsNullOrWhiteSpace(condition))
            {
                sql = sql + condition;
            }
            return DapperConnRead.Query<int>(sql).FirstOrDefault();
        }

        /// <summary>
        /// 根據條件統計資料
        /// </summary>
        /// <param name="condition">查詢條件</param>
        /// <param name="fieldName">統計欄位名稱</param>
        /// <returns></returns>
        public virtual async Task<int> GetCountByWhereAsync(string condition, string fieldName = "*")
        {
            if (HasInjectionData(condition))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", condition));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(condition))
            {
                condition = "1=1";
            }
            string sql = $"select count({fieldName}) from {tableName}  where ";
            if (!string.IsNullOrWhiteSpace(condition))
            {
                sql = sql + condition;
            }
            return await DapperConnRead.QueryFirstAsync<int>(sql);
        }

        /// <summary>
        /// 根據條件查詢獲取某個欄位的最大值
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位的最大值</returns>
        public virtual async Task<dynamic> GetMaxValueByFieldAsync(string strField, string where, IDbTransaction trans = null)
        {
            string sql = $"select isnull(MAX({strField}),0) as maxVaule from {tableName} ";
            if (dbConnectionOptions.DatabaseType==DatabaseType.MySql)
            {
                sql = $"select if(isnull(MAX({strField})),0,MAX({strField})) as maxVaule from {tableName} ";
            }
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }

            return await DapperConnRead.QueryFirstAsync<dynamic>(sql);
        }
        /// <summary>
        /// 根據條件統計某個欄位之和,sum(欄位)
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="where">條件</param>
        /// <param name="trans">事務</param>
        /// <returns>返回欄位求和后的值</returns>
        public virtual async Task<dynamic> GetSumValueByFieldAsync(string strField, string where, IDbTransaction trans = null)
        {
            string sql = $"select isnull(sum({strField}),0) as sumVaule from {tableName} ";
            if (dbConnectionOptions.DatabaseType == DatabaseType.MySql)
            {
                sql = $"select if(isnull(sum({strField})),0,sum({strField})) as sumVaule from {tableName} ";
            }
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            return await DapperConnRead.QueryFirstAsync<dynamic>(sql);
        }
        #endregion

        #region 新增、修改和刪除

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual long Insert(T entity, IDbTransaction trans=null)
        {
            if (entity.KeyIsNull())
            {
                entity.GenerateDefaultKeyVal();
            }
            return DapperConn.Insert<T>(entity);
        }


        /// <summary>
        /// 非同步新增
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<long> InsertAsync(T entity, IDbTransaction trans=null)
        {
            if (entity.KeyIsNull())
            {
                entity.GenerateDefaultKeyVal();
            }
            return await DapperConn.InsertAsync<T>(entity);
        }
        
        /// <summary>
        /// 批量插入資料
        /// </summary>
        /// <param name="entities"></param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual void Insert(List<T> entities)
        {
            DbContext.BulkInsert<T>(entities);
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="primaryKey">主鍵</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool Update(T entity, TKey primaryKey, IDbTransaction trans=null)
        {
           return DbContext.Edit<T>(entity)>0;
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool Update(T entity, IDbTransaction trans = null)
        {
            return DbContext.Edit(entity)>0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="primaryKey"></param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> UpdateAsync(T entity, TKey primaryKey, IDbTransaction trans=null)
        {
            return await DapperConn.UpdateAsync<T>(entity);
        }

        /// <summary>
        /// 同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <returns></returns>
        public virtual bool Delete(T entity)
        {
            DbContext.GetDbSet<T>().Remove(entity);
            return DbContext.SaveChanges() > 0;
        }

        /// <summary>
        /// 非同步物理刪除實體。
        /// </summary>
        /// <param name="entity">實體</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteAsync(T entity, IDbTransaction trans = null)
        {
            DbContext.GetDbSet<T>().Remove(entity);
            return await DbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 物理刪除資訊
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool Delete(TKey primaryKey, IDbTransaction trans=null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + "=@PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });

            param.Add(tupel);
            Tuple<bool, string> result= ExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 非同步物理刪除資訊
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> DeleteAsync(TKey primaryKey, IDbTransaction trans=null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + "=@PrimaryKey" ;
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });
            param.Add(tupel);
            Tuple<bool, string> result =await ExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 按主鍵批量刪除
        /// </summary>
        /// <param name="ids">主鍵Id List集合</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public  virtual bool DeleteBatch(IList<dynamic> ids, IDbTransaction trans = null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where PrimaryKey in (@PrimaryKey)";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = ids });

            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool DeleteBatchWhere(string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + where;
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);

            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 按條件批量刪除
        /// </summary>
        /// <param name="where">條件</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual  async Task<bool> DeleteBatchWhereAsync(string where, IDbTransaction trans = null)
        {
            if(HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + where;
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 根據指定物件的ID和使用者ID,從資料庫中刪除指定物件(用於記錄人員的操作日誌）
        /// </summary>
        /// <param name="primaryKey">指定物件的ID</param>
        /// <param name="userId">使用者ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool DeleteByUser(TKey primaryKey, string userId, IDbTransaction trans=null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + " = @PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });
            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }
        /// <summary>
        /// 非同步根據指定物件的ID和使用者ID,從資料庫中刪除指定物件(用於記錄人員的操作日誌）
        /// </summary>
        /// <param name="primaryKey">指定物件的ID</param>
        /// <param name="userId">使用者ID</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> DeleteByUserAsync(TKey primaryKey, string userId, IDbTransaction trans=null)
        {
            var param = new List<Tuple<string, object>>();
            string sql = $"delete from {tableName} where " + PrimaryKey + " = @PrimaryKey";
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 邏輯刪除資訊，bl為true時將DeleteMark設定為1刪除，bl為flase時將DeleteMark設定為10-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool DeleteSoft(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "DeleteMark=0 ";
            }
            else
            {
                sql += "DeleteMark=1 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + PrimaryKey + "=@PrimaryKey" ;
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey, @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }

        /// <summary>
        /// 非同步邏輯刪除資訊，bl為true時將DeleteMark設定為1刪除，bl為flase時將DeleteMark設定為0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> DeleteSoftAsync(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "DeleteMark=0 ";
            }
            else
            {
                sql += "DeleteMark=1 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + PrimaryKey + "=@PrimaryKey";
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey, @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 非同步批量軟刪除資訊，將DeleteMark設定為1-刪除，0-恢復刪除
        /// </summary>
        /// <param name="bl">true為不刪除，false刪除</param> c
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteSoftBatchAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "DeleteMark=0 ";
            }
            else
            {
                sql += "DeleteMark=1 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",DeleteUserId='" + userId + "'";
            }
            DateTime deleteTime = DateTime.Now;
            sql += ",DeleteTime=@DeleteTime where " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @DeleteTime = deleteTime });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 設定資料有效性，將EnabledMark設定為1-有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool SetEnabledMark(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "EnabledMark=1 ";
            }
            else
            {
                sql += "EnabledMark=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@lastModifyTime where " + PrimaryKey + "=@PrimaryKey";

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = primaryKey, @lastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }

        /// <summary>
        /// 非同步設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="primaryKey">主鍵ID</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> SetEnabledMarkAsync(bool bl, TKey primaryKey, string userId = null, IDbTransaction trans = null)
        {
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "EnabledMark=1 ";
            }
            else
            {
                sql += "EnabledMark=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime where " + PrimaryKey + "=@PrimaryKey";

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { @PrimaryKey = PrimaryKey, @LastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }

        /// <summary>
        /// 非同步按條件設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="where">條件</param>
        /// <param name="userId">操作使用者</param>
        /// <param name="trans">事務物件</param>
        /// <returns></returns>
        public virtual async Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "EnabledMark=1 ";
            }
            else
            {
                sql += "EnabledMark=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime where " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { LastModifyTime = lastModifyTime });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 非同步按條件設定資料有效性，將EnabledMark設定為1:有效，0-為無效
        /// </summary>
        /// <param name="bl">true為有效，false無效</param>
        /// <param name="where">條件</param>
        /// <param name="paramparameters"></param>
        /// <param name="userId"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public virtual async Task<bool> SetEnabledMarkByWhereAsync(bool bl, string where, object paramparameters = null, string userId = null, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set ";
            if (bl)
            {
                sql += "EnabledMark=1 ";
            }
            else
            {
                sql += "EnabledMark=0 ";
            }
            if (!string.IsNullOrEmpty(userId))
            {
                sql += ",LastModifyUserId='" + userId + "'";
            }
            DateTime lastModifyTime = DateTime.Now;
            sql += ",LastModifyTime=@LastModifyTime  " + where;

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, new { LastModifyTime = lastModifyTime, paramparameters });
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 更新某一欄位值,欄位值字元型別
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值字元型別</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool UpdateTableField(string strField, string fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "='" + fieldValue + "'";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }

            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }

        /// <summary>
        /// 更新某一欄位值,欄位值字元型別
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值字元型別</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> UpdateTableFieldAsync(string strField, string fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "='" + fieldValue + "'";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }
        /// <summary>
        /// 更新某一欄位值，欄位值為數字
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual bool UpdateTableField(string strField, int fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "=" + fieldValue + "";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = ExecuteTransaction(param);
            return result.Item1;
        }


        /// <summary>
        /// 更新某一欄位值，欄位值為數字
        /// </summary>
        /// <param name="strField">欄位</param>
        /// <param name="fieldValue">欄位值數字</param>
        /// <param name="where">條件,為空更新所有內容</param>
        /// <param name="trans">事務物件</param>
        /// <returns>執行成功返回<c>true</c>，否則為<c>false</c>。</returns>
        public virtual async Task<bool> UpdateTableFieldAsync(string strField, int fieldValue, string where, IDbTransaction trans = null)
        {
            if (HasInjectionData(where))
            {
                Log4NetHelper.Info(string.Format("檢測出SQL隱碼攻擊的惡意資料, {0}", where));
                throw new Exception("檢測出SQL隱碼攻擊的惡意資料");
            }
            if (string.IsNullOrEmpty(where))
            {
                where = "1=1";
            }
            string sql = $"update {tableName} set " + strField + "=" + fieldValue + "";
            if (!string.IsNullOrEmpty(where))
            {
                sql += " where " + where;
            }
            var param = new List<Tuple<string, object>>();
            Tuple<string, object> tupel = new Tuple<string, object>(sql, null);
            param.Add(tupel);
            Tuple<bool, string> result = await ExecuteTransactionAsync(param);
            return result.Item1;
        }
        
        /// <summary>
        /// 多表多資料操作批量插入、更新、刪除--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        public async Task<Tuple<bool, string>> ExecuteTransactionAsync(List<Tuple<string, object>> trans, int? commandTimeout = null)
        {
            if (!trans.Any()) return new Tuple<bool, string>(false, "執行事務SQL語句不能為空！");
            using (IDbConnection connection = DapperConn)
            {
                bool isClosed = connection.State == ConnectionState.Closed;
                if (isClosed) connection.Open();
                using (var transaction =  connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var tran in trans)
                        {
                            await connection.ExecuteAsync(tran.Item1, tran.Item2, transaction, commandTimeout);
                        }
                        //提交事務
                        transaction.Commit();
                        return new Tuple<bool, string>(true, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        //回滾事務
                        Log4NetHelper.Error("", ex);
                        transaction.Rollback();
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                    }
                }
            }
        }


        /// <summary>
        /// 多表多資料操作批量插入、更新、刪除--事務
        /// </summary>
        /// <param name="trans">事務</param>
        /// <param name="commandTimeout">超時</param>
        /// <returns></returns>
        public Tuple<bool, string> ExecuteTransaction(List<Tuple<string, object>> trans, int? commandTimeout = null)
        {
            if (!trans.Any()) return new Tuple<bool, string>(false, "執行事務SQL語句不能為空！");
            using (IDbConnection connection = DapperConn)
            {
                bool isClosed = connection.State == ConnectionState.Closed;
                if (isClosed) connection.Open();
                //開啟事務
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var tran in trans)
                        {
                            connection.Execute(tran.Item1, tran.Item2, transaction, commandTimeout);
                        }
                        //提交事務
                        transaction.Commit();
                        return new Tuple<bool, string>(true, string.Empty);
                    }
                    catch (Exception ex)
                    {
                        //回滾事務
                        Log4NetHelper.Error("", ex);
                        transaction.Rollback();
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                        return new Tuple<bool, string>(false, ex.ToString());
                    }
                    finally
                    {
                        connection.Close();
                        connection.Dispose();
                        DapperConn.Close();
                        DapperConn.Dispose();
                    }
                }
            }
        }

        #endregion

        #endregion

        #region EF操作

        /// <summary>
        /// EF 上下文介面，可讀可寫
        /// </summary>
        public virtual IDbContextCore DbContext
        {
            get { return _dbContext; }
        }

        /// <summary>
        /// EF 上下文介面，僅可讀
        /// </summary>
        public virtual IDbContextCore DbContextRead
        {
            get { return _dbContextFactory.CreateContext<T>(WriteAndReadEnum.Read); }
        }

        #region 新增
        /// <summary>
        /// 新增實體
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Add(T entity)
        {
            return DbContext.Add<T>(entity);
        }
        /// <summary>
        /// 新增實體
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual async Task<int> AddAsync(T entity)
        {
            return await DbContext.AddAsync(entity);
        }
        /// <summary>
        /// 批量新增實體，數量量較多是推薦使用BulkInsert()
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public virtual int AddRange(ICollection<T> entities)
        {
            return DbContext.AddRange(entities);
        }
        /// <summary>
        /// 批量新增實體，數量量較多是推薦使用BulkInsert()
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public virtual async Task<int> AddRangeAsync(ICollection<T> entities)
        {
            return await DbContext.AddRangeAsync(entities);
        }
        /// <summary>
        /// 批量新增SqlBulk方式，效率最高
        /// </summary>
        /// <param name="entities">資料實體集合</param>
        /// <param name="destinationTableName">資料庫表名稱，預設為實體名稱</param>
        public virtual void BulkInsert(IList<T> entities, string destinationTableName = null)
        {
            DbContext.BulkInsert<T>(entities, destinationTableName);
        }
        /// <summary>
        /// 執行新增的sql語句
        /// </summary>
        /// <param name="sql">新增Sql語句</param>
        /// <returns></returns>
        public int AddBySql(string sql)
        {
            return DbContext.ExecuteSqlWithNonQuery(sql);
        }

        #endregion

        #region Update

        /// <summary>
        /// 更新資料實體
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Edit(T entity)
        {
            return DbContext.Edit<T>(entity);
        }
        /// <summary>
        /// 批量更新資料實體
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public virtual int EditRange(ICollection<T> entities)
        {
            return DbContext.EditRange(entities);
        }
        /// <summary>
        /// 更新指定欄位的值
        /// </summary>
        /// <param name="model">資料實體</param>
        /// <param name="updateColumns">指定欄位</param>
        /// <returns></returns>
        public virtual int Update(T model, params string[] updateColumns)
        {
            DbContext.Update(model, updateColumns);
            return DbContext.SaveChanges();
        }
        /// <summary>
        /// 執行更新資料的Sql語句
        /// </summary>
        /// <param name="sql">更新資料的Sql語句</param>
        /// <returns></returns>
        public int UpdateBySql(string sql)
        {
            return DbContext.ExecuteSqlWithNonQuery(sql);
        }
        #endregion

        #region Delete

        /// <summary>
        /// 根據主鍵刪除資料
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual int Delete(TKey key)
        {
            return DbContext.Delete<T, TKey>(key);
        }
        /// <summary>
        /// 執行刪除資料Sql語句
        /// </summary>
        /// <param name="sql">刪除的Sql語句</param>
        /// <returns></returns>
        public int DeleteBySql(string sql)
        {
            return DbContext.ExecuteSqlWithNonQuery(sql);
        }

        #endregion

        #region Query
        /// <summary>
        /// 根據條件統計數量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual int Count(Expression<Func<T, bool>> @where = null)
        {
            return DbContext.Count(where);
        }

        /// <summary>
        /// 根據條件統計數量Count()
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<int> CountAsync(Expression<Func<T, bool>> @where = null)
        {
            return await DbContext.CountAsync(where);
        }
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>

        public virtual bool Exist(Expression<Func<T, bool>> @where = null)
        {
            return DbContext.Exist(where);
        }
        /// <summary>
        /// 是否存在,存在返回true，不存在返回false
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<bool> ExistAsync(Expression<Func<T, bool>> @where = null)
        {
            return await DbContext.ExistAsync(where);
        }

        /// <summary>
        /// 根據主鍵獲取實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual T GetSingle(TKey key)
        {
            return DbContext.Find<T, TKey>(key);
        }


        /// <summary>
        /// 根據主鍵獲取實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual async Task<T> GetSingleAsync(TKey key)
        {
            return await DbContext.FindAsync<T, TKey>(key);
        }

        /// <summary>
        /// 獲取單個實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual T GetSingleOrDefault(Expression<Func<T, bool>> @where = null)
        {
            return DbContext.GetSingleOrDefault(@where);
        }

        /// <summary>
        /// 獲取單個實體。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<T> GetSingleOrDefaultAsync(Expression<Func<T, bool>> @where = null)
        {
            return await DbContext.GetSingleOrDefaultAsync(where);
        }

        /// <summary>
        /// 獲取實體列表。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IList<T> Get(Expression<Func<T, bool>> @where = null)
        {
            return DbContext.GetByCompileQuery(where);
        }
        /// <summary>
        /// 獲取實體列表。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual async Task<List<T>> GetAsync(Expression<Func<T, bool>> @where = null)
        {
            return await DbContext.GetByCompileQueryAsync(where);
        }

        /// <summary>
        ///  分頁獲取實體列表。建議：如需使用Include和ThenInclude請過載此方法。
        /// </summary>
        /// <param name="where">查詢條件</param>
        /// <param name="pagerInfo">分頁資訊</param>
        /// <param name="asc">排序方式</param>
        /// <param name="orderby">排序欄位</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetByPagination(Expression<Func<T, bool>> @where, PagerInfo pagerInfo,  bool asc = false, params Expression<Func<T, object>>[] @orderby)
        {
            var filter = DbContext.Get(where);
            if (orderby != null)
            {
                foreach (var func in orderby)
                {
                    filter = asc ? filter.OrderBy(func).AsQueryable() : filter.OrderByDescending(func).AsQueryable();
                }
            }
            pagerInfo.RecordCount = filter.Count();
            return filter.Skip(pagerInfo.PageSize * (pagerInfo.CurrenetPageIndex - 1)).Take(pagerInfo.PageSize);
        }
        /// <summary>
        /// sql語句查詢資料集
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<T> GetBySql(string sql)
        {
            return DbContext.SqlQuery<T, T>(sql);
        }
        /// <summary>
        /// sql語句查詢資料集，返回輸出Dto實體
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<TView> GetViews<TView>(string sql)
        {
            var list = DbContext.SqlQuery<T, TView>(sql);
            return list;
        }
        /// <summary>
        /// 查詢檢視
        /// </summary>
        /// <typeparam name="TView">返回結果物件</typeparam>
        /// <param name="viewName">檢視名稱</param>
        /// <param name="where">查詢條件</param>
        /// <returns></returns>
        public List<TView> GetViews<TView>(string viewName, Func<TView, bool> @where)
        {
            var list = DbContext.SqlQuery<T, TView>($"select * from {viewName}");
            if (where != null)
            {
                return list.Where(where).ToList();
            }

            return list;
        }

        #endregion
        #endregion

        #region 輔助類方法


        /// <summary>
        /// 驗證是否存在注入程式碼(條件語句）
        /// </summary>
        /// <param name="inputData"></param>
        public virtual bool HasInjectionData(string inputData)
        {
            if (string.IsNullOrEmpty(inputData))
                return false;

            //裡面定義惡意字符集合
            //驗證inputData是否包含惡意集合
            if (Regex.IsMatch(inputData.ToLower(), GetRegexString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 獲取正規表示式
        /// </summary>
        /// <returns></returns>
        private static string  GetRegexString()
        {
            //構造SQL的注入關鍵字元
            string[] strBadChar =
            {
                "select\\s",
                "from\\s",
                "insert\\s",
                "delete\\s",
                "update\\s",
                "drop\\s",
                "truncate\\s",
                "exec\\s",
                "count\\(",
                "declare\\s",
                "asc\\(",
                "mid\\(",
                //"char\\(",
                "net user",
                "xp_cmdshell",
                "/add\\s",
                "exec master.dbo.xp_cmdshell",
                "net localgroup administrators"
            };

            //構造正規表示式
            string str_Regex = ".*(";
            for (int i = 0; i < strBadChar.Length - 1; i++)
            {
                str_Regex += strBadChar[i] + "|";
            }
            str_Regex += strBadChar[^1] + ").*";

            return str_Regex;
        }


        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // 要檢測冗餘呼叫
        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 釋放託管狀態(託管物件)。
                }

                // TODO: 釋放未託管的資源(未託管的物件)並在以下內容中替代終結器。
                // TODO: 將大型欄位設定為 null。

                disposedValue = true;
            }
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
            if (DapperConn != null)
            {
                DapperConn?.Dispose();
            }
        }

        // TODO: 僅當以上 Dispose(bool disposing) 擁有用於釋放未託管資源的程式碼時才替代終結器。
        // ~BaseRepository() {
        //   // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
        //   Dispose(false);
        // }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            // 請勿更改此程式碼。將清理程式碼放入以上 Dispose(bool disposing) 中。
            Dispose(true);

            DbContext?.Dispose();
            DapperConn?.Dispose();
            // TODO: 如果在以上內容中替代了終結器，則取消註釋以下行。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
