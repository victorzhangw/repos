using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Aurobase.Commons.Extend;

namespace Aurobase.Commons.Helpers
{
    /// <summary>
    /// 首字母大寫
    /// </summary>
    public class UpperFirstCaseNamingPolicy:JsonNamingPolicy
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public override string ConvertName(string name) =>
            name.UpperFirst();
    }
}
