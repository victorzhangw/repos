using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.Helpers
{
    /// <summary>
    /// 應用版本號、軟體廠商等資訊
    /// </summary>
   public class AppVersionHelper
    {
        /// <summary>
        /// 版本號
        /// </summary>
        public const string Version = "3.0";
        /// <summary>
        /// 軟體廠商
        /// </summary>
        public const string Manufacturer = "";
        /// <summary>
        /// 網站地址
        /// </summary>
        public const string WebSite = "http://www.Aurobase.com";
        /// <summary>
        /// 更新地址
        /// </summary>
        public const string UpdateUrl = "http://netcore.Aurobase.com/update";
    }
}
