using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Aurobase.Commons.Helpers
{
    /// <summary>
    /// byte轉換類
    /// </summary>
    public class ByteConvertHelper
    {
        /// <summary>
        /// 將物件轉換為byte陣列
        /// </summary>
        /// <param name="obj">被轉換物件</param>
        /// <returns>轉換后byte陣列</returns>
        public static byte[] Object2Bytes(object obj)
        {
            string json = JsonConvert.SerializeObject(obj);
            byte[] serializedResult = System.Text.Encoding.UTF8.GetBytes(json);
            return serializedResult;
        }

        /// <summary>
        /// 將byte陣列轉換成物件
        /// </summary>
        /// <param name="buff">被轉換byte陣列</param>
        /// <returns>轉換完成後的物件</returns>
        public static object Bytes2Object(byte[] buff)
        {
            string json = System.Text.Encoding.UTF8.GetString(buff);
            return JsonConvert.DeserializeObject<object>(json);
        }

        /// <summary>
        /// 將byte陣列轉換成物件
        /// </summary>
        /// <param name="buff">被轉換byte陣列</param>
        /// <returns>轉換完成後的物件</returns>
        public static T Bytes2Object<T>(byte[] buff)
        {
            string json = System.Text.Encoding.UTF8.GetString(buff);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
