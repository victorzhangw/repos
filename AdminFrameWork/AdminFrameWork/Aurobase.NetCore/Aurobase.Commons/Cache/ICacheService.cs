using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.Commons.Cache
{
    /// <summary>
    /// 快取服務介面
    /// </summary>
    public interface ICacheService
    {
        #region  驗證快取項是否存在
        /// <summary>
        /// 驗證快取項是否存在
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        bool Exists(string key);


        #endregion

        #region  新增快取
        /// <summary>
        /// 新增快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">快取Value</param>
        /// <returns></returns>
        bool Add(string key, object value);


        /// <summary>
        /// 新增快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">快取Value</param>
        /// <param name="expiresSliding">滑動過期時長（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <param name="expiressAbsoulte">絕對過期時長</param>
        /// <returns></returns>
        bool Add(string key, object value, TimeSpan expiresSliding, TimeSpan expiressAbsoulte);


        /// <summary>
        /// 新增快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">快取Value</param>
        /// <param name="expiresIn">快取時長</param>
        /// <param name="isSliding">是否滑動過期（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <returns></returns>
        bool Add(string key, object value, TimeSpan expiresIn, bool isSliding = false);

        #endregion

        #region  刪除快取
        /// <summary>
        /// 刪除快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        bool Remove(string key);

        /// <summary>
        /// 批量刪除快取
        /// </summary>
        /// <param name="keys">快取Key集合</param>
        /// <returns></returns>
        void RemoveAll(IEnumerable<string> keys);


        /// <summary>
        /// 使用萬用字元找出所有的key然後逐個刪除
        /// </summary>
        /// <param name="pattern">萬用字元</param>
        void RemoveByPattern(string pattern);

        /// <summary>
        /// 刪除所有快取
        /// </summary>
        void RemoveCacheAll();
        #endregion

        #region  獲取快取
        /// <summary>
        /// 獲取快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        T Get<T>(string key) where T : class;


        /// <summary>
        /// 獲取快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <returns></returns>
        object Get(string key);


        /// <summary>
        /// 獲取快取集合
        /// </summary>
        /// <param name="keys">快取Key集合</param>
        /// <returns></returns>
        IDictionary<string, object> GetAll(IEnumerable<string> keys);

        #endregion

        #region  修改快取
        /// <summary>
        /// 修改快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">新的快取Value</param>
        /// <returns></returns>
        bool Replace(string key, object value);


        /// <summary>
        /// 修改快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">新的快取Value</param>
        /// <param name="expiresSliding">滑動過期時長（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <param name="expiressAbsoulte">絕對過期時長</param>
        /// <returns></returns>
        bool Replace(string key, object value, TimeSpan expiresSliding, TimeSpan expiressAbsoulte);


        /// <summary>
        /// 修改快取
        /// </summary>
        /// <param name="key">快取Key</param>
        /// <param name="value">新的快取Value</param>
        /// <param name="expiresIn">快取時長</param>
        /// <param name="isSliding">是否滑動過期（如果在過期時間內有操作，則以目前時間點延長過期時間）</param>
        /// <returns></returns>
        bool Replace(string key, object value, TimeSpan expiresIn, bool isSliding = false);

        #endregion

    }
}
