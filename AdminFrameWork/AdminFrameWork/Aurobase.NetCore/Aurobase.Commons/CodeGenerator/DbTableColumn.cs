using System;
using System.Collections.Generic;
using System.Text;

namespace Aurobase.Commons.CodeGenerator
{

    [Serializable]
    public class DbTableColumn
    {
        /// <summary>
        /// 表名稱
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 欄位名
        /// </summary>
        public string ColName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// 是否自增
        /// </summary>
        public bool IsIdentity { get; set; }
        /// <summary>
        /// 是否主鍵
        /// </summary>
        public bool IsPrimaryKey { get; set; }
        /// <summary>
        /// 欄位資料型別
        /// </summary>
        public string ColumnType { get; set; }
        /// <summary>
        /// 欄位資料長度
        /// </summary>
        public long? ColumnLength { get; set; }
        /// <summary>
        /// 是否允許為空
        /// </summary>
        public bool IsNullable { get; set; }
        /// <summary>
        /// 預設值
        /// </summary>
        public string DefaultValue { get; set; }
        /// <summary>
        /// 欄位說明
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// C#資料型別
        /// </summary>
        public string CSharpType { get; set; }
        /// <summary>
        /// 資料精度
        /// </summary>
        public int? DataPrecision { get; set; }
        /// <summary>
        /// 資料刻度
        /// </summary>
        public int? DataScale { get; set; }
    }
}
