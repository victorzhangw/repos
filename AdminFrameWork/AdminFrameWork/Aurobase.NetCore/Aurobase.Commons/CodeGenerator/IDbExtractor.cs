using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurobase.Commons.Pages;

namespace Aurobase.Commons.CodeGenerator
{
    /// <summary>
    /// 資料庫操作介面
    /// </summary>
    public interface IDbExtractor : IDisposable
    {
        /// <summary>
        /// 獲取資料庫資訊
        /// </summary>
        /// <returns></returns>
        List<DataBaseInfo> GetAllDataBases();
        /// <summary>
        /// 獲取資料庫表的資訊
        /// </summary>
        /// <param name="tablelist">資料庫表名稱</param>
        /// <returns></returns>
        List<DbTableInfo> GetWhereTables(string tablelist=null);

        /// <summary>
        /// 根據條件獲取資料庫的所有表的資訊
        /// </summary>
        /// <param name="strwhere"></param>
        /// <param name="fieldNameToSort"></param>
        /// <param name="isDescending"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        List<DbTableInfo> GetTablesWithPage(string strwhere, string fieldNameToSort, bool isDescending, PagerInfo info);

        /// <summary>
        /// 獲取表的所有欄位名及欄位型別
        /// </summary>
        /// <param name="tableName">資料表的名稱</param>
        /// <returns></returns>
        List<DbFieldInfo> GetAllColumns(string tableName);
    }
}
