using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.Commons.Options
{
    /// <summary>
    /// 快取中可用的應用
    /// </summary>
    [Serializable]
    public class AllowCacheApp
    {
        /// <summary>
        /// Get / Set ID
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set應用Id
        /// </summary>
        [MaxLength(50)]
        public string AppId { get; set; }

        /// <summary>
        /// Get / Set應用金鑰
        /// </summary>
        [MaxLength(50)]
        public string AppSecret { get; set; }

        /// <summary>
        /// Get / Set訊息加密金鑰
        /// </summary>
        [MaxLength(256)]
        public string EncodingAESKey { get; set; }

        /// <summary>
        /// Get / Set請求url
        /// </summary>
        [MaxLength(256)]
        public string RequestUrl { get; set; }

        /// <summary>
        /// Get / Set token
        /// </summary>
        [MaxLength(256)]
        public string Token { get; set; }

        /// <summary>
        /// Get / Set 是否開啟訊息加解密
        /// </summary>
        public bool? IsOpenAEKey { get; set; }

    }
}
