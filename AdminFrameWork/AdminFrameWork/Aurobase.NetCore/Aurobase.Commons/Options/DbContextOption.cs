using Aurobase.Commons.Enums;

namespace Aurobase.Commons.Options
{
    /// <summary>
    /// 資料庫上下文設定
    /// </summary>
    public class DbContextOption
    {
        /// <summary>
        /// 資料庫連線字串
        /// </summary>
        public string dbConfigName { get; set; }
        /// <summary>
        /// 實體程式集名稱
        /// </summary>
        public string ModelAssemblyName { get; set; }
        /// <summary>
        /// 資料庫型別
        /// </summary>
        public DatabaseType DbType { get; set; } = DatabaseType.SqlServer;
        /// <summary>
        /// 是否輸出Sql日誌
        /// </summary>
        public bool IsOutputSql;
    }

}
