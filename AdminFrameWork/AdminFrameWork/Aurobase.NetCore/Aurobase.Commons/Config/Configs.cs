using Microsoft.Extensions.Configuration;
using Aurobase.Commons.Core.App;
using Aurobase.Commons.IoC;

namespace Aurobase.Commons
{
    /// <summary>
    /// 設定檔案讀取操作
    /// </summary>
    public class Configs
    {
        public static  IConfiguration configuration;
        static Configs()
        {
            configuration =App.GetService<IConfiguration>();
        
        }
        /// <summary>
        /// 根據Key獲取數設定內容
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static IConfigurationSection GetSection(string key)
        {
            return configuration.GetSection(key);
        }
        /// <summary>
        /// 根據section和key獲取設定內容
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigurationValue(string section, string key)
        {
            return GetSection(section)?[key];
        }

        /// <summary>
        /// 根據Key獲取資料庫連線字串
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            return configuration.GetConnectionString(key);
        }
    }
}
