using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Log;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Commons.Pages;
using {DtosNamespace};
using {ModelsNamespace};
using {IServicsNamespace};

namespace {ApiControllerNamespace}.Areas.{BaseNameSpaceEx}.Controllers
{
    /// <summary>
    /// {TableNameDesc}介面
    /// </summary>
    [ApiController]
    [Route("api/{BaseNameSpaceEx}/[controller]")]
    public class {ModelTypeName}Controller : AreaApiController<{ModelTypeName}, {ModelTypeName}OutputDto,{ModelTypeName}InputDto,I{ModelTypeName}Service,{KeyTypeName}>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public {ModelTypeName}Controller(I{ModelTypeName}Service _iService) : base(_iService)
        {
            iService = _iService;
        }
        /// <summary>
        /// 新增前處理數據
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert({ModelTypeName} info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            if (info.SortCode == null)
            {
                info.SortCode = 99;
            }
        }
        
        /// <summary>
        /// 在更新數據前對數據的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate({ModelTypeName} info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除數據前對數據的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete({ModelTypeName} info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
    }
}