using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace {DtosNamespace}
{
    /// <summary>
    /// {TableNameDesc}輸出物件模型
    /// </summary>
    [Serializable]
    public class {ModelTypeName}OutputDto
    {
{ModelContent}
    }
}
