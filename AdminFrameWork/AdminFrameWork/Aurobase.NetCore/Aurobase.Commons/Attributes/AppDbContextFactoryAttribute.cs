using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.Commons.Attributes
{
    /// <summary>
    /// 資料庫上下文設定
    /// </summary>
    public class AppDbContextFactoryAttribute : Attribute
    {
        /// <summary>
        /// 資料庫設定名稱
        /// </summary>
        public string DbConfigName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbConfigName">資料庫設定名稱</param>
        public AppDbContextFactoryAttribute(string dbConfigName)
        {
            DbConfigName = dbConfigName;
        }
    }

}
