using AutoMapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.Properties;

namespace Aurobase.Commons.Mapping
{
    /// <summary>
    /// 物件對映擴充套件操作
    /// </summary>
    public static class MapperExtensions
    {
        private static IMapper _mapper;

        /// <summary>
        /// 設定物件對映執行者
        /// </summary>
        /// <param name="mapper">對映執行者</param>
        public static void SetMapper(IMapper mapper)
        {
            mapper.CheckNotNull("mapper");
            _mapper = mapper;
        }

        /// <summary>
        /// 將物件對映為指定型別
        /// </summary>
        /// <typeparam name="TTarget">要對映的目標型別</typeparam>
        /// <param name="source">源物件</param>
        /// <returns>目標型別的物件</returns>
        public static TTarget MapTo<TTarget>(this object source)
        {
            CheckMapper();
            return _mapper.Map<TTarget>(source);
        }

        /// <summary>
        /// 使用源型別的物件更新目標型別的物件
        /// </summary>
        /// <typeparam name="TSource">源型別</typeparam>
        /// <typeparam name="TTarget">目標型別</typeparam>
        /// <param name="source">源物件</param>
        /// <param name="target">待更新的目標物件</param>
        /// <returns>更新后的目標型別物件</returns>
        public static TTarget MapTo<TSource, TTarget>(this TSource source, TTarget target)
        {
            CheckMapper();
            return _mapper.Map(source, target);
        }

        /// <summary>
        /// 將資料源對映為指定<typeparamref name="TOutputDto"/>的集合
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TOutputDto"></typeparam>
        /// <param name="source"></param>
        /// <param name="membersToExpand"></param>
        /// <returns></returns>
        public static IQueryable<TOutputDto> ToOutput<TEntity, TOutputDto>(this IQueryable<TEntity> source,
            params Expression<Func<TOutputDto, object>>[] membersToExpand)
        {
            CheckMapper();
            return _mapper.ProjectTo<TOutputDto>(source, membersToExpand);
        }

        /// <summary>
        /// 集合到集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>

        public static List<T> MapTo<T>(this IEnumerable obj)
        {
            CheckMapper();
            return _mapper.Map<List<T>>(obj);
        }

        /// <summary>
        /// 驗證對映執行者是否為空
        /// </summary>
        private static void CheckMapper()
        {
            if (_mapper == null)
            {
                throw new NullReferenceException(Resources.Map_MapperIsNull);
            }
        }
    }
}
