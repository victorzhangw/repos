using System;
using Aurobase.Commons.IRepositories;
using Aurobase.CMS.Models;

namespace Aurobase.CMS.IRepositories
{
    /// <summary>
    /// 定義文章倉儲介面
    /// </summary>
    public interface IArticlenewsRepository:IRepository<Articlenews, string>
    {
    }
}