using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.CMS.Models;

namespace Aurobase.CMS.Dtos
{
    public class CMSProfile : Profile
    {
        public CMSProfile()
        {
           CreateMap<Articlecategory, ArticlecategoryOutputDto>();
           CreateMap<ArticlecategoryInputDto, Articlecategory>();
           CreateMap<Articlenews, ArticlenewsOutputDto>();
           CreateMap<ArticlenewsInputDto, Articlenews>();

        }
    }
}
