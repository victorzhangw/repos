using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Commons.Dtos;
using Aurobase.CMS.Models;

namespace Aurobase.CMS.Dtos
{
    /// <summary>
    /// 文章分類輸入物件模型
    /// </summary>
    [AutoMap(typeof(Articlecategory))]
    [Serializable]
    public class ArticlecategoryInputDto: IInputDto<string>
    {
        /// <summary>
        /// 主鍵
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 標題
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 父級Id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 全路徑
        /// </summary>
        public string ClassPath { get; set; }

        /// <summary>
        /// 層級
        /// </summary>
        public int? ClassLayer { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int SortCode { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 外鏈地址
        /// </summary>
        public string LinkUrl { get; set; }

        /// <summary>
        /// 主圖圖片
        /// </summary>
        public string ImgUrl { get; set; }

        /// <summary>
        /// SEO標題
        /// </summary>
        public string SeoTitle { get; set; }

        /// <summary>
        /// SEO關鍵詞
        /// </summary>
        public string SeoKeywords { get; set; }

        /// <summary>
        /// SEO描述
        /// </summary>
        public string SeoDescription { get; set; }

        /// <summary>
        /// 是否熱門
        /// </summary>
        public bool? IsHot { get; set; }

        /// <summary>
        /// 是否可用
        /// </summary>
        public bool? EnabledMark { get; set; }


    }
}
