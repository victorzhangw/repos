using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Encrypt;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.Json;
using Aurobase.Commons.Options;

namespace Aurobase.Email
{
    /// <summary>
    /// 發送郵件
    /// </summary>
    public static class SendMailHelper
    {

        /// <summary>
        /// 發送郵件
        /// </summary>
        /// <param name="mailBodyEntity">郵件基礎資訊</param>
        public static SendResultEntity SendMail(MailBodyEntity mailBodyEntity)
        {
            var sendResultEntity = new SendResultEntity();
            if (mailBodyEntity == null)
            {
                throw new ArgumentNullException();
            }

            SendServerConfigurationEntity sendServerConfiguration = new SendServerConfigurationEntity();
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            AppSetting sysSetting = AurobaseCacheHelper.Get<AppSetting>("SysSetting");
            if (sysSetting != null)
            {
                sendServerConfiguration.SmtpHost = DEncrypt.Decrypt(sysSetting.Emailsmtp);
                sendServerConfiguration.SenderAccount = sysSetting.Emailusername;
                sendServerConfiguration.SenderPassword = DEncrypt.Decrypt(sysSetting.Emailpassword);
                sendServerConfiguration.SmtpPort = sysSetting.Emailport.ToInt();
                sendServerConfiguration.IsSsl =sysSetting.Emailssl.ToBool();
                sendServerConfiguration.MailEncoding ="utf-8";

                mailBodyEntity.Sender = sysSetting.Emailnickname;
                mailBodyEntity.SenderAddress = sysSetting.Emailusername;

            }
            else
            {
                sendResultEntity.ResultInformation ="郵件服務器未設定";
                sendResultEntity.ResultStatus = false;
                throw new ArgumentNullException();
            }
            sendResultEntity= SendMail(mailBodyEntity, sendServerConfiguration);
            return sendResultEntity;
        }
        /// <summary>
        /// 發送郵件
        /// </summary>
        /// <param name="mailBodyEntity">郵件基礎資訊</param>
        /// <param name="sendServerConfiguration">發件人基礎資訊</param>
        public static SendResultEntity SendMail(MailBodyEntity mailBodyEntity,
            SendServerConfigurationEntity sendServerConfiguration)
        {

            var sendResultEntity = new SendResultEntity();
            if (mailBodyEntity == null)
            {
                throw new ArgumentNullException();
            }

            if (sendServerConfiguration == null)
            {
                sendResultEntity.ResultInformation = "郵件服務器未設定";
                sendResultEntity.ResultStatus = false;
                throw new ArgumentNullException();
            }

            using (var client = new SmtpClient(new ProtocolLogger(MailMessage.CreateMailLog())))
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                Connection(mailBodyEntity, sendServerConfiguration, client, sendResultEntity);

                if (sendResultEntity.ResultStatus == false)
                {
                    return sendResultEntity;
                }

                SmtpClientBaseMessage(client);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                Authenticate(mailBodyEntity, sendServerConfiguration, client, sendResultEntity);

                if (sendResultEntity.ResultStatus == false)
                {
                    return sendResultEntity;
                }

                Send(mailBodyEntity, sendServerConfiguration, client, sendResultEntity);

                if (sendResultEntity.ResultStatus == false)
                {
                    return sendResultEntity;
                }
                client.Disconnect(true);
            }
            return sendResultEntity;
        }


        /// <summary>
        /// 連線服務器
        /// </summary>
        /// <param name="mailBodyEntity">郵件內容</param>
        /// <param name="sendServerConfiguration">發送設定</param>
        /// <param name="client">客戶端物件</param>
        /// <param name="sendResultEntity">發送結果</param>
        public static void Connection(MailBodyEntity mailBodyEntity, SendServerConfigurationEntity sendServerConfiguration,
            SmtpClient client, SendResultEntity sendResultEntity)
        {
            try
            {
                client.Connect(sendServerConfiguration.SmtpHost, sendServerConfiguration.SmtpPort, sendServerConfiguration.IsSsl);
            }
            catch (SmtpCommandException ex)
            {
                sendResultEntity.ResultInformation = $"嘗試連線時出錯:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
            catch (SmtpProtocolException ex)
            {
                sendResultEntity.ResultInformation = $"嘗試連線時的協議錯誤:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
            catch (Exception ex)
            {
                sendResultEntity.ResultInformation = $"服務器連線錯誤:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
        }

        /// <summary>
        /// 賬戶認證
        /// </summary>
        /// <param name="mailBodyEntity">郵件內容</param>
        /// <param name="sendServerConfiguration">發送設定</param>
        /// <param name="client">客戶端物件</param>
        /// <param name="sendResultEntity">發送結果</param>
        public static void Authenticate(MailBodyEntity mailBodyEntity, SendServerConfigurationEntity sendServerConfiguration,
            SmtpClient client, SendResultEntity sendResultEntity)
        {
            try
            {
                client.Authenticate(sendServerConfiguration.SenderAccount, sendServerConfiguration.SenderPassword);
            }
            catch (AuthenticationException ex)
            {
                sendResultEntity.ResultInformation = $"無效的使用者名稱或密碼:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
            catch (SmtpCommandException ex)
            {
                sendResultEntity.ResultInformation = $"嘗試驗證錯誤:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
            catch (SmtpProtocolException ex)
            {
                sendResultEntity.ResultInformation = $"嘗試驗證時的協議錯誤:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
            catch (Exception ex)
            {
                sendResultEntity.ResultInformation = $"賬戶認證錯誤:{0}" + ex.Message;
                sendResultEntity.ResultStatus = false;
            }
        }

        /// <summary>
        /// 發送郵件
        /// </summary>
        /// <param name="mailBodyEntity">郵件內容</param>
        /// <param name="sendServerConfiguration">發送設定</param>
        /// <param name="client">客戶端物件</param>
        /// <param name="sendResultEntity">發送結果</param>
        public static void Send(MailBodyEntity mailBodyEntity, SendServerConfigurationEntity sendServerConfiguration,
            SmtpClient client, SendResultEntity sendResultEntity)
        {
            try
            {
                client.Send(MailMessage.AssemblyMailMessage(mailBodyEntity));
            }
            catch (SmtpCommandException ex)
            {
                switch (ex.ErrorCode)
                {
                    case SmtpErrorCode.RecipientNotAccepted:
                        sendResultEntity.ResultInformation = $"收件人未被接受:{ex.Message}";
                        break;
                    case SmtpErrorCode.SenderNotAccepted:
                        sendResultEntity.ResultInformation = $"發件人未被接受:{ex.Message}";
                        break;
                    case SmtpErrorCode.MessageNotAccepted:
                        sendResultEntity.ResultInformation = $"訊息未被接受:{ex.Message}";
                        break;
                }
                sendResultEntity.ResultStatus = false;
            }
            catch (SmtpProtocolException ex)
            {
                sendResultEntity.ResultInformation = $"發送訊息時的協議錯誤:{ex.Message}";
                sendResultEntity.ResultStatus = false;
            }
            catch (Exception ex)
            {
                sendResultEntity.ResultInformation = $"郵件接收失敗:{ex.Message}";
                sendResultEntity.ResultStatus = false;
            }
        }

        /// <summary>
        /// 獲取SMTP基礎資訊
        /// </summary>
        /// <param name="client">客戶端物件</param>
        /// <returns></returns>
        public static MailServerInformation SmtpClientBaseMessage(SmtpClient client)
        {
            var mailServerInformation = new MailServerInformation
            {
                Authentication = client.Capabilities.HasFlag(SmtpCapabilities.Authentication),
                BinaryMime = client.Capabilities.HasFlag(SmtpCapabilities.BinaryMime),
                Dsn = client.Capabilities.HasFlag(SmtpCapabilities.Dsn),
                EightBitMime = client.Capabilities.HasFlag(SmtpCapabilities.EightBitMime),
                Size = client.MaxSize
            };

            return mailServerInformation;
        }
    }
}
