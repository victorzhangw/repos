using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Pages;
using Aurobase.Commons.Services;
using Aurobase.Tenants.IRepositories;
using Aurobase.Tenants.IServices;
using Aurobase.Tenants.Dtos;
using Aurobase.Tenants.Models;

namespace Aurobase.Tenants.Services
{
    /// <summary>
    /// 用户登录信息服務接口实现
    /// </summary>
    public class TenantLogonService : BaseService<TenantLogon,TenantLogonOutputDto, string>, ITenantLogonService
    {
		private readonly ITenantLogonRepository _repository;
        public TenantLogonService(ITenantLogonRepository repository) : base(repository)
        {
			_repository=repository;
        }
    }
}