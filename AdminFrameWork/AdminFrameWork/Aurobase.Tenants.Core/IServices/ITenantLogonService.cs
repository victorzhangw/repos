using System;
using Aurobase.Commons.IServices;
using Aurobase.Tenants.Dtos;
using Aurobase.Tenants.Models;

namespace Aurobase.Tenants.IServices
{
    /// <summary>
    /// 定义用户登录信息服務接口
    /// </summary>
    public interface ITenantLogonService:IService<TenantLogon,TenantLogonOutputDto, string>
    {

    }
}
