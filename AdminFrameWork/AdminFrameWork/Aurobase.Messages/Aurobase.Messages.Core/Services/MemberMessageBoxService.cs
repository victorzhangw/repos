using System;
using Aurobase.Commons.Services;
using Aurobase.Security.IServices;
using Aurobase.Messages.IRepositories;
using Aurobase.Messages.IServices;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Services
{
    /// <summary>
    /// 服務介面實現
    /// </summary>
    public class MemberMessageBoxService: BaseService<MemberMessageBox,MemberMessageBoxOutputDto, string>, IMemberMessageBoxService
    {
		private readonly IMemberMessageBoxRepository _repository;
        private readonly ILogService _logService;
        public MemberMessageBoxService(IMemberMessageBoxRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }

        public int GetTotalCounts(int isread, string userid)
        {
            return _repository.GetTotalCounts(isread,userid);
        }

        public bool UpdateIsReadStatus(string id, int isread, string userid)
        {
            return _repository.UpdateIsReadStatus(id, isread, userid);
        }
    }
}