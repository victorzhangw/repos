using System;
using Aurobase.Commons.Services;
using Aurobase.Security.IServices;
using Aurobase.Messages.IRepositories;
using Aurobase.Messages.IServices;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Services
{
    /// <summary>
    /// 服務介面實現
    /// </summary>
    public class MessageMailBoxService: BaseService<MessageMailBox,MessageMailBoxOutputDto, string>, IMessageMailBoxService
    {
		private readonly IMessageMailBoxRepository _repository;
        private readonly ILogService _logService;
        public MessageMailBoxService(IMessageMailBoxRepository repository,ILogService logService) : base(repository)
        {
			_repository=repository;
			_logService=logService;
        }
    }
}