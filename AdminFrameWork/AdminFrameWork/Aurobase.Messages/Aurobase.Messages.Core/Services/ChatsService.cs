using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Pages;
using Aurobase.Commons.Services;
using Aurobase.Messages.IRepositories;
using Aurobase.Messages.IServices;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Services
{
	/// <summary>
	/// 服務接口實現
	/// </summary>
	public class ChatsService: BaseService<Chats,ChatsOutputDto, string>, IChatsService
	{
		private readonly IChatsRepository _repository;
		public ChatsService(IChatsRepository repository) : base(repository)
		{
			_repository=repository;
		}
		public bool ReceiveSuperEightBotCustomer(Chats chats)
		{
			return true;
		}

	}
}