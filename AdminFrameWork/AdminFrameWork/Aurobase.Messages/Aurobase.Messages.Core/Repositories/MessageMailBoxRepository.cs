using System;

using Aurobase.Commons.Repositories;
using Aurobase.Messages.IRepositories;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Repositories
{
    /// <summary>
    /// 倉儲介面的實現
    /// </summary>
    public class MessageMailBoxRepository : BaseRepository<MessageMailBox, string>, IMessageMailBoxRepository
    {
		public MessageMailBoxRepository()
        {
            this.tableName = "Sys_MessageMailBox";
            this.primaryKey = "Id";
        }
    }
}