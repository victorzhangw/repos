using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Models;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(MessageTemplates))]
    [Serializable]
    public class MessageTemplatesInputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string MessageType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendEmail { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendSMS { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendInnerMessage { get; set; }

      

        /// <summary>
        /// Get / Set
        /// </summary>
        public string TagDescription { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EmailSubject { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EmailBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string InnerMessageSubject { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string InnerMessageBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SMSBody { get; set; }

        

        /// <summary>
        /// 設定或獲取模板編號
        /// </summary>
        public string AppletTemplateNo { get; set; }

        /// <summary>
        /// 設定或獲取模板名稱
        /// </summary>
        public string AppletTemplateName { get; set; }

        

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SMSTemplateCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SMSTemplateContent { get; set; }





    }
}
