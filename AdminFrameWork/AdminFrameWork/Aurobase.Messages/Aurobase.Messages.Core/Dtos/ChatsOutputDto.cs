using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 输出物件模型
    /// </summary>
    [Serializable]
    public class ChatsOutputDto
    {
        /// <summary>
        /// 設定或獲取主鍵
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取來源代表/主 ID
        /// </summary>
        [MaxLength(50)]
        public string MasterId { get; set; }

        /// <summary>
        /// 設定或獲取來源代表/主 姓名
        /// </summary>
        [MaxLength(30)]
        public string MasterName { get; set; }

        /// <summary>
        /// 設定或獲取頭像
        /// </summary>
        [MaxLength(30)]
        public string HeadIcon { get; set; }

        /// <summary>
        /// 設定或獲取次頻道名稱1
        /// </summary>
        [MaxLength(50)]
        public string SubChannelName1 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道名稱2
        /// </summary>
        [MaxLength(50)]
        public string SubChannelName2 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道名稱3
        /// </summary>
        [MaxLength(50)]
        public string SubChannelName3 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道名稱4
        /// </summary>
        [MaxLength(50)]
        public string SubChannelName4 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道Id 1
        /// </summary>
        [MaxLength(50)]
        public string SubChannelId1 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道Id 2
        /// </summary>
        [MaxLength(50)]
        public string SubChannelId2 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道Id 3
        /// </summary>
        [MaxLength(50)]
        public string SubChannelId3 { get; set; }

        /// <summary>
        /// 設定或獲取次頻道Id 4
        /// </summary>
        [MaxLength(50)]
        public string SubChannelId4 { get; set; }

        /// <summary>
        /// 設定或獲取顯示名稱/暱稱
        /// </summary>
        [MaxLength(30)]
        public string DisplayName { get; set; }

        /// <summary>
        /// 設定或獲取原始名稱
        /// </summary>
        [MaxLength(30)]
        public string OriginName { get; set; }

        /// <summary>
        /// 設定或獲取電郵
        /// </summary>
        [MaxLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// 設定或獲取電話
        /// </summary>
        [MaxLength(50)]
        public string Phone { get; set; }

        /// <summary>
        /// 設定或獲取攜帶電話
        /// </summary>
        [MaxLength(50)]
        public string Mobile { get; set; }

        /// <summary>
        /// 設定或獲取敘述
        /// </summary>
        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取標籤
        /// </summary>
        [MaxLength(100)]
        public string Tags { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(50)]
        public string CreatorUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string CompanyId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(100)]
        public string DeptId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(50)]
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 設定或獲取 
        /// </summary>
        [MaxLength(50)]
        public string DeleteUserId { get; set; }

    }
}
