using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 輸出物件模型
    /// </summary>
    [Serializable]
    public class MessageTemplatesOutputDto
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(50)]
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(100)]
        public string MessageType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendEmail { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendSMS { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendInnerMessage { get; set; }

        

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(-1)]
        public string TagDescription { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(-1)]
        public string EmailSubject { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(1073741823)]
        public string EmailBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(-1)]
        public string InnerMessageSubject { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(1073741823)]
        public string InnerMessageBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(-1)]
        public string SMSBody { get; set; }

        

        /// <summary>
        /// 設定或獲取模板編號
        /// </summary>
        [MaxLength(50)]
        public string AppletTemplateNo { get; set; }

        /// <summary>
        /// 設定或獲取模板名稱
        /// </summary>
        [MaxLength(100)]
        public string AppletTemplateName { get; set; }

        

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(-1)]
        public string SMSTemplateCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        [MaxLength(-1)]
        public string SMSTemplateContent { get; set; }

      


    }
}
