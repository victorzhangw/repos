using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 输入物件模型
    /// </summary>
    [AutoMap(typeof(Chats))]
    [Serializable]
    public class ChatsInputDto : IInputDto<string>
    {
        /// <summary>
        /// 設定或獲取主鍵
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 設定或獲取來源代表/主 ID
        /// </summary>
        public string MasterId { get; set; }
        /// <summary>
        /// 設定或獲取來源代表/主 姓名
        /// </summary>
        public string MasterName { get; set; }
        /// <summary>
        /// 設定或獲取頭像
        /// </summary>
        public string HeadIcon { get; set; }
        /// <summary>
        /// 設定或獲取次頻道名稱1
        /// </summary>
        public string SubChannelName1 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道名稱2
        /// </summary>
        public string SubChannelName2 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道名稱3
        /// </summary>
        public string SubChannelName3 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道名稱4
        /// </summary>
        public string SubChannelName4 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道Id 1
        /// </summary>
        public string SubChannelId1 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道Id 2
        /// </summary>
        public string SubChannelId2 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道Id 3
        /// </summary>
        public string SubChannelId3 { get; set; }
        /// <summary>
        /// 設定或獲取次頻道Id 4
        /// </summary>
        public string SubChannelId4 { get; set; }
        /// <summary>
        /// 設定或獲取顯示名稱/暱稱
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 設定或獲取原始名稱
        /// </summary>
        public string OriginName { get; set; }
        /// <summary>
        /// 設定或獲取電郵
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 設定或獲取電話
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 設定或獲取攜帶電話
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 設定或獲取敘述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 設定或獲取標籤
        /// </summary>
        public string Tags { get; set; }
        /// <summary>
        /// 設定或獲取 
        /// </summary>
        public bool? EnabledMark { get; set; }

    }
}
