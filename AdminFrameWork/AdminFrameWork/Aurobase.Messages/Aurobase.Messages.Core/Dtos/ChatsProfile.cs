using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Dtos
{
    public class ChatsProfile : Profile
    {
        public ChatsProfile()
        {
           CreateMap<Chats, ChatsOutputDto>();
           CreateMap<ChatsInputDto, Chats>();

        }
    }
}
