using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(MessageMailBox))]
    [Serializable]
    public class MessageMailBoxInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否簡訊提醒
        /// </summary>
        public bool? IsMsgRemind { get; set; }

        /// <summary>
        /// 設定或獲取是否發送
        /// </summary>
        public bool? IsSend { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime SendDate { get; set; }

        /// <summary>
        /// 設定或獲取是否是強制訊息
        /// </summary>
        public bool? IsCompel { get; set; }


    }
}
