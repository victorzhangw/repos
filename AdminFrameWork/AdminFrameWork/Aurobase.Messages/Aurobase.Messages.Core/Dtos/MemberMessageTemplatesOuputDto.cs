using System;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 使用者訂閱訊息模板定義
    /// </summary>
    [Serializable]
    public class MemberMessageTemplatesOuputDto : IOutputDto
    {
        #region Property Members

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 訊息型別
        /// </summary>
        public  string MessageType
        {
            get; set;
        }
        /// <summary>
        /// 訊息名稱
        /// </summary>
        public  string Name
        {
            get; set;
        }
        /// <summary>
        /// 是否發送郵件
        /// </summary>
        public  bool SendEmail
        {
            get; set;
        }
        /// <summary>
        /// 是否發送簡訊
        /// </summary>
        public  bool SendSMS
        {

            get; set;
        }
        /// <summary>
        /// 是否發送站內訊息
        /// </summary>
        public  bool SendInnerMessage
        {

            get; set;
        }
        /// <summary>
        /// 是否發送微信公眾號訊息
        /// </summary>
        public  bool SendWeixin
        {

            get; set;
        }
        /// <summary>
        /// 微信公眾號訊息模板Id
        /// </summary>
        public  string WeixinTemplateId
        {

            get; set;
        }
        /// <summary>
        /// 訊息內容
        /// </summary>

        public  string TagDescription
        {

            get; set;
        }
        /// <summary>
        /// Email郵件訊息主題
        /// </summary>
        public  string EmailSubject
        {

            get; set;
        }
        /// <summary>
        /// Email郵件訊息內容
        /// </summary>
        public  string EmailBody
        {

            get; set;
        }
        /// <summary>
        /// 站內訊息主題
        /// </summary>
        public  string InnerMessageSubject
        {

            get; set;
        }
        /// <summary>
        /// 站內訊息內容
        /// </summary>
        public  string InnerMessageBody
        {

            get; set;
        }
        /// <summary>
        /// 簡訊內容
        /// </summary>
        public  string SMSBody
        {

            get; set;
        }

        /// <summary>
        /// 微信模板編號，如果為空則表示不支援微信訊息提醒
        /// </summary>
        public  string WeiXinTemplateNo
        {

            get; set;
        }

        /// <summary>
        /// 微信模板中對應的名稱
        /// </summary>
        public  string WeiXinName
        {

            get; set;
        }

        /// <summary>
        /// 是否用於微信小程式
        /// </summary>
        public  bool UseInWxApplet
        {

            get; set;
        }

        /// <summary>
        /// 小程式模板ID
        /// </summary>
        public  string WxAppletTemplateId
        {

            get; set;
        }

        /// <summary>
        /// 小程式模板編號
        /// </summary>
        public  string AppletTemplateNo
        {

            get; set;
        }

        /// <summary>
        /// 小程式模板名稱
        /// </summary>
        public  string AppletTemplateName
        {

            get; set;
        }


        /// <summary>
        /// 小程式模板訊息模板ID
        /// </summary>
        public virtual string WxAppletSubscribeTemplateId
        {
            get; set;
        }
        /// <summary>
        /// 小程式訂閱訊息模板編號
        /// </summary>
        public virtual string WxAppletSubscribeTemplateNo
        {
            get; set;
        }
        /// <summary>
        /// 小程式訂閱訊息模板名稱
        /// </summary>
        public virtual string WxAppletSubscribeTemplateName
        {
            get; set;
        }
        /// <summary>
        /// 簡訊模板程式碼
        /// </summary>
        public  string SMSTemplateCode
        {

            get; set;
        }
        /// <summary>
        /// 簡訊模板內容
        /// </summary>
        public  string SMSTemplateContent
        {

            get; set;
        }

        /// <summary>
        /// O2O小程式模板ID 
        /// </summary>
        public  string WxO2OAppletTemplateId
        {

            get; set;
        }

        /// <summary>
        /// 是否在O2O小程式中使用
        /// </summary>
        public  bool UseInO2OApplet
        {

            get; set;
        }

        /// <summary>
        /// 訊息訂閱者訂閱訊息ID 
        /// </summary>
        public string MemberSubscribeMsgId
        {

            get; set;
        }
        /// <summary>
        /// 訊息訂閱者訂閱訊息狀態
        /// </summary>
        public string SubscribeStatus
        {

            get; set;
        }
        /// <summary>
        /// 訊息訂閱者訂閱型別：SMS簡訊，WxApplet 微信小程式，InnerMessage站內訊息 ，Email郵件通知
        /// </summary>
        public string SubscribeType
        {

            get; set;
        }
        
        #endregion
    }
}
