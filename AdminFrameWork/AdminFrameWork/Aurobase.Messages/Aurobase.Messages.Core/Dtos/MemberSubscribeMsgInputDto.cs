using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Aurobase.Commons.Dtos;
using Aurobase.Commons.Models;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.Dtos
{
    /// <summary>
    /// 輸入物件模型
    /// </summary>
    [AutoMap(typeof(MemberSubscribeMsg))]
    [Serializable]
    public class MemberSubscribeMsgInputDto: IInputDto<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 設定或獲取訂閱使用者
        /// </summary>
        public string SubscribeUserId { get; set; }

        /// <summary>
        /// 設定或獲取訂閱型別：SMS簡訊，WxApplet 微信小程式，InnerMessage站內訊息 ，Email郵件通知
        /// </summary>
        public string SubscribeType { get; set; }

        /// <summary>
        /// 設定或獲取訊息模板Id主鍵
        /// </summary>
        public string MessageTemplateId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SubscribeTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取訂閱狀態
        /// </summary>
        public string SubscribeStatus { get; set; }


    }
}
