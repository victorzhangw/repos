using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.IRepositories
{
    /// <summary>
    /// 定义仓储接口
    /// </summary>
    public interface IChatsRepository:IRepository<Chats, string>
    {
        bool ReceiveSuperEightBotCustomer(Chats chats);
    }
}