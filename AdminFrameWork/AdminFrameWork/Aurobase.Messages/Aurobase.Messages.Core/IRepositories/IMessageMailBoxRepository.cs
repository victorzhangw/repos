using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.IRepositories
{
    /// <summary>
    /// 定義倉儲介面
    /// </summary>
    public interface IMessageMailBoxRepository:IRepository<MessageMailBox, string>
    {
    }
}