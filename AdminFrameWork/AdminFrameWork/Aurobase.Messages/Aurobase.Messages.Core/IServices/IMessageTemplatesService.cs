using System;
using System.Collections.Generic;
using Aurobase.Commons.IServices;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.IServices
{
    /// <summary>
    /// 定義服務介面
    /// </summary>
    public interface IMessageTemplatesService:IService<MessageTemplates,MessageTemplatesOutputDto, string>
    {

        /// <summary>
        /// 根據使用者查詢微信小程式訂閱訊息模板列表，關聯使用者訂閱表
        /// </summary>
        /// <param name="userId">使用者編號</param>
        /// <returns></returns>
        List<MemberMessageTemplatesOuputDto> ListByUseInWxApplet(string userId);

        /// <summary>
        /// 根據訊息型別查詢訊息模板
        /// </summary>
        /// <param name="messageType">訊息型別</param>
        /// <returns></returns>
        MessageTemplates GetByMessageType(string messageType);
    }
}
