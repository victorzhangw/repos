using System;
using Aurobase.Commons.IServices;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.IServices
{
    /// <summary>
    /// 定義服務介面
    /// </summary>
    public interface IMessageMailBoxService:IService<MessageMailBox,MessageMailBoxOutputDto, string>
    {
    }
}
