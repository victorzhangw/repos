using System;
using Aurobase.Commons.IServices;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;

namespace Aurobase.Messages.IServices
{
    /// <summary>
    /// 定义服務接口
    /// </summary>
    public interface IChatsService:IService<Chats,ChatsOutputDto, string>
    {
        bool ReceiveSuperEightBotCustomer(Chats chats);
    }
}
