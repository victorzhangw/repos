using System;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Messages.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("Sys_MemberSubscribeMsg")]
    [Serializable]
    public class MemberSubscribeMsg:BaseEntity<string>
    {

        /// <summary>
        /// 設定或獲取訂閱使用者
        /// </summary>
        public string SubscribeUserId { get; set; }

        /// <summary>
        /// 設定或獲取訂閱型別：SMS簡訊，WxApplet ，InnerMessage站內訊息 ，Email郵件通知
        /// </summary>
        public string SubscribeType { get; set; }

        /// <summary>
        /// 設定或獲取訊息模板Id主鍵
        /// </summary>
        public string MessageTemplateId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SubscribeTemplateId { get; set; }

        /// <summary>
        /// 設定或獲取訂閱狀態
        /// </summary>
        public string SubscribeStatus { get; set; }


    }
}
