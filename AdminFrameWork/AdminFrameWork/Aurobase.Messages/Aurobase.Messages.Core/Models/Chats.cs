﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Messages.Models
{
    /// <summary>
    /// 資料實體物件
    /// </summary>
    [Table("Sys_Chats")]
    [Serializable]
    public class Chats : BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        

        /// <summary>
        /// 來源代表/主 ID
        /// </summary>
        public string MasterId { get; set; }

        /// <summary>
        /// 來源代表/主 姓名
        /// </summary>
        public string MasterName { get; set; }

        /// <summary>
        /// 頭像
        /// </summary>
        public string HeadIcon { get; set; }

        /// <summary>
        /// 次頻道名稱1
        /// </summary>
        public string SubChannelName1 { get; set; }

        /// <summary>
        /// 次頻道名稱2
        /// </summary>
        public string SubChannelName2 { get; set; }

        /// <summary>
        /// 次頻道名稱3
        /// </summary>
        public string SubChannelName3 { get; set; }

        /// <summary>
        /// 次頻道名稱4
        /// </summary>
        public string SubChannelName4 { get; set; }

        /// <summary>
        /// 次頻道Id 1
        /// </summary>
        public string SubChannelId1 { get; set; }

        /// <summary>
        /// 次頻道Id 2
        /// </summary>
        public string SubChannelId2 { get; set; }

        /// <summary>
        /// 次頻道Id 3
        /// </summary>
        public string SubChannelId3 { get; set; }

        /// <summary>
        /// 次頻道Id 4
        /// </summary>
        public string SubChannelId4 { get; set; }

        /// <summary>
        /// 顯示名稱/暱稱
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 原始名稱
        /// </summary>
        public string OriginName { get; set; }

        /// <summary>
        /// 電郵
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 攜帶電話
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 敘述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 標籤
        /// </summary>
        public string Tags { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public bool? EnabledMark { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        ///  
        /// </summary>
        public string DeleteUserId { get; set; }
    }
}
