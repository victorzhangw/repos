using System;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Messages.Models
{
    /// <summary>
    /// 資料實體物件
    /// </summary>
    [Table("Sys_MessageTemplates")]
    [Serializable]
    public class MessageTemplates:BaseEntity<string>
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        public string MessageType { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendEmail { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendSMS { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public bool SendInnerMessage { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string TagDescription { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EmailSubject { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string EmailBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string InnerMessageSubject { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string InnerMessageBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SMSBody { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SMSTemplateCode { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string SMSTemplateContent { get; set; }

      


    }
}
