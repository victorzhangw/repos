using System;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Messages.Models
{
    /// <summary>
    /// 資料實體物件
    /// </summary>
    [Table("Sys_MemberMessageBox")]
    [Serializable]
    public class MemberMessageBox:BaseEntity<string>
    {
        /// <summary>
        /// 設定或獲取訊息內容Id
        /// </summary>
        public long? ContentId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string MsgContent { get; set; }

        /// <summary>
        /// 設定或獲取發送者
        /// </summary>
        public string Sernder { get; set; }

        /// <summary>
        /// 設定或獲取接受者
        /// </summary>
        public string Accepter { get; set; }

        /// <summary>
        /// 設定或獲取是否已讀
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? ReadDate { get; set; }


    }
}
