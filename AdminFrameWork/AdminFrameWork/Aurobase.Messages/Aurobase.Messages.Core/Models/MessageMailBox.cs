using System;
using System.ComponentModel.DataAnnotations.Schema;
using Aurobase.Commons.Models;

namespace Aurobase.Messages.Models
{
    /// <summary>
    /// ，資料實體物件
    /// </summary>
    [Table("Sys_MessageMailBox")]
    [Serializable]
    public class MessageMailBox:BaseEntity<string>, ICreationAudited, IModificationAudited, IDeleteAudited
    {
        /// <summary>
        /// Get / Set
        /// </summary>
        //public string Id { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 設定或獲取是否簡訊提醒
        /// </summary>
        public bool? IsMsgRemind { get; set; }

        /// <summary>
        /// 設定或獲取是否發送
        /// </summary>
        public bool? IsSend { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime SendDate { get; set; }

        /// <summary>
        /// 設定或獲取是否是強制訊息
        /// </summary>
        public bool? IsCompel { get; set; }

        /// <summary>
        /// 設定或獲取是否發送
        /// </summary>
        public bool? DeleteMark { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? CreatorTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string CompanyId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string LastModifyUserId { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// Get / Set
        /// </summary>
        public string DeleteUserId { get; set; }


    }
}
