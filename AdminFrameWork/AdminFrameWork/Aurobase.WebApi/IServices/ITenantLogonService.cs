using System;
using Aurobase.Commons.IServices;
using Aurobase.Tenants.Dtos;
using Aurobase.Tenants.Models;

namespace Aurobase.Tenants.IServices
{
    /// <summary>
    /// 定義使用者登錄資訊服務介面
    /// </summary>
    public interface ITenantLogonService:IService<TenantLogon,TenantLogonOutputDto, string>
    {

    }
}
