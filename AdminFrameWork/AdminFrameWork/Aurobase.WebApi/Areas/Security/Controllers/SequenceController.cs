using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.AspNetCore.Mvc;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Security.Application;
using Aurobase.Security.Dtos;
using Aurobase.Security.IServices;
using Aurobase.Security.Models;

namespace Aurobase.SecurityApi.Areas.Security.Controllers
{
    /// <summary>
    /// 單據編碼介面
    /// </summary>
    [ApiController]
    [Route("api/Security/[controller]")]
    public class SequenceController : AreaApiController<Sequence, SequenceOutputDto, SequenceInputDto, ISequenceService,string>
    {
        /// <summary>
        /// 建構函式
        /// </summary>
        /// <param name="_iService"></param>
        public SequenceController(ISequenceService _iService) : base(_iService)
        {
            iService = _iService;
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Sequence info)
        {
            info.Id = GuidUtils.CreateNo();
            info.Id = new SequenceApp().GetSequenceNext("SortingSn");
            info.CreatorTime=info.LastModifyTime = DateTime.Now;
            info.CreatorUserId = info.LastModifyUserId= CurrentUser.UserId;
            info.CompanyId = CurrentUser.OrganizeId;
            info.DeptId = CurrentUser.DeptId;
            info.CurrentNo = 0;
            info.CurrentReset = "";
            info.DeleteMark = false;
        }
        
        /// <summary>
        /// 在更新資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Sequence info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 在軟刪除資料前對資料的修改操作
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Sequence info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }


        /// <summary>
        /// 非同步新增或修改資料
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>

        [HttpPost("Insert")]
        [AurobaseAuthorize("Add")]
        public override async Task<IActionResult> InsertAsync(SequenceInputDto info)
        {
            CommonResult result = new CommonResult();

            if (string.IsNullOrEmpty(info.SequenceName))
            {
                result.ErrMsg = "單據名稱不能為空";
                return ToJsonContent(result);
            }

            if (string.IsNullOrEmpty(info.Id))
            {
                string where = string.Format("SequenceName='{0}'", info.SequenceName);
                Sequence sequenceIsExist = iService.GetWhere(where);
                if (sequenceIsExist != null)
                {
                    result.ErrMsg = "規則名稱不能重複";
                    return ToJsonContent(result);
                }
                Sequence sequence =info.MapTo<Sequence>();
                OnBeforeInsert(sequence);
                long ln = await iService.InsertAsync(sequence).ConfigureAwait(true);
                result.Success = ln > 0;
            }
            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.Success = false;
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }


        /// <summary>
        /// 非同步更新資料，需要在業務模組控制器重寫該方法,否則更新無效
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost("Update")]
        [AurobaseAuthorize("Edit")]
        public override async Task<IActionResult> UpdateAsync(SequenceInputDto info)
        {
            CommonResult result = new CommonResult();
            if (string.IsNullOrEmpty(info.SequenceName))
            {
                result.ErrMsg = "單據名稱不能為空";
                return ToJsonContent(result);
            }

            string where = string.Format("SequenceName='{0}' and id!='{1}'", info.SequenceName, info.Id);
            Sequence goodsIsExist = iService.GetWhere(where);
            if (goodsIsExist != null)
            {
                result.ErrMsg = "規則名稱不能重複";
                return ToJsonContent(result);
            }
            Sequence sequence = iService.Get(info.Id);
            sequence.SequenceName = info.SequenceName;
            sequence.SequenceDelimiter = info.SequenceDelimiter;
            sequence.SequenceReset = info.SequenceReset;
            sequence.Step = info.Step;
            sequence.CurrentNo = info.CurrentNo;
            sequence.CurrentCode = info.CurrentCode;
            sequence.CurrentReset = info.CurrentReset;
            sequence.EnabledMark = info.EnabledMark;
            sequence.Description = info.Description;
            OnBeforeUpdate(sequence);
            result.Success = await iService.UpdateAsync(sequence, info.Id).ConfigureAwait(true);

            if (result.Success)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.Success = false;
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }
    }
}