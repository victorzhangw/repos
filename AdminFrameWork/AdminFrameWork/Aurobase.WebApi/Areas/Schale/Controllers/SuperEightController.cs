﻿using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Log;
using Aurobase.Commons.Mapping;
using Aurobase.Commons.Models;
using Aurobase.Commons.Pages;
using Aurobase.Messages.Dtos;
using Aurobase.Messages.Models;
using Aurobase.Messages.IServices;
using Aurobase.SapOdata.Models;
using Aurobase.SapOdata;

namespace Aurobase.WebApi.Areas.Schale.Controllers
{
    /// <summary>
    /// 接口
    /// </summary>
    [ApiController]
    [Route("api/Schale/[controller]")]
    public class SuperEightController : AreaApiController<Chats, ChatsOutputDto, ChatsInputDto, IChatsService, string>
    {
        /// <summary>
        /// 構造函數
        /// </summary>
        /// <param name="_iService"></param>
        public SuperEightController(IChatsService _iService) : base(_iService)
        {
            iService = _iService;
        }
        /// <summary>
        /// 新增前處理資料
        /// </summary>
        /// <param name="info"></param>
        protected override void OnBeforeInsert(Chats info)
        {
            info.Id = GuidUtils.CreateNo();
            info.CreatorTime = DateTime.Now;
            info.CreatorUserId = CurrentUser.UserId;
            info.DeleteMark = false;
            
        }

        /// <summary>
        /// 更新前修改資料
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeUpdate(Chats info)
        {
            info.LastModifyUserId = CurrentUser.UserId;
            info.LastModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 軟刪除
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected override void OnBeforeSoftDelete(Chats info)
        {
            info.DeleteMark = true;
            info.DeleteTime = DateTime.Now;
            info.DeleteUserId = CurrentUser.UserId;
        }
        /// <summary>
        /// 獲取 S8  Bot 資料
        /// </summary>
        /// <param name="tinfo"></param>
        /// <returns></returns>
        [HttpPost("RetrieveSEightBot")]
        [AllowAnonymous]
        public async Task<IActionResult> RetrieveSEightBot(ChatsInputDto tinfo)
        {
            CommonResult result = new CommonResult();
            //Console.WriteLine(tinfo);
            Login login = new() { 
                Account="01",
                Password= "Bb1234567@@"
            };

            SendOdataEntities.ConnectwithBasicAuth(login);
            Chats info = tinfo.MapTo<Chats>();
            OnBeforeInsert(info);
            long ln = 1;
            //long ln = await iService.InsertAsync(info).ConfigureAwait(false);
            if (ln > 0)
            {

            }
            if (ln > 0)
            {
                result.ErrCode = ErrCode.successCode;
                result.ErrMsg = ErrCode.err0;
            }
            else
            {
                result.ErrMsg = ErrCode.err43001;
                result.ErrCode = "43001";
            }
            return ToJsonContent(result);
        }
    }
}
