using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Aurobase.AspNetCore.Controllers;
using Aurobase.AspNetCore.Models;
using Aurobase.AspNetCore.Mvc.Filter;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Models;
using Aurobase.Commons.Net;
using Aurobase.Commons.VerificationCode;
using Aurobase.Security.Dtos;

namespace Aurobase.WebApi.Controllers
{
    /// <summary>
    /// 驗證碼介面
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class CaptchaController : ApiController
    {
        /// <summary>
        /// 獲取驗證碼
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [NoPermissionRequired]
        public async Task<CommonResult<AuthGetVerifyCodeOutputDto>> CaptchaAsync()
        {
            Captcha captcha = new Captcha();
            var code =await  captcha.GenerateRandomCaptchaAsync().ConfigureAwait(false);
            var result =await  captcha.GenerateCaptchaImageAsync(code);
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            TimeSpan expiresSliding = DateTime.Now.AddMinutes(5) - DateTime.Now;
            
            AurobaseCacheHelper.Add("ValidateCode"+ result.Timestamp.ToString("yyyyMMddHHmmssffff"), code, expiresSliding,false);
            AuthGetVerifyCodeOutputDto authGetVerifyCodeOutputDto = new AuthGetVerifyCodeOutputDto();
            authGetVerifyCodeOutputDto.Img =Convert.ToBase64String(result.CaptchaMemoryStream.ToArray());
            authGetVerifyCodeOutputDto.Key = result.Timestamp.ToString("yyyyMMddHHmmssffff");
            CommonResult<AuthGetVerifyCodeOutputDto> commonResult = new CommonResult<AuthGetVerifyCodeOutputDto>();
            commonResult.ErrCode= ErrCode.successCode;
            commonResult.ResData = authGetVerifyCodeOutputDto;
            return commonResult;
        }
    }
}
