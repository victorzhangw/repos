using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Quartz.Models;
using Aurobase.Security.Models;

namespace Aurobase.Quartz.IRepositories
{
    /// <summary>
    /// 定義定時任務倉儲介面
    /// </summary>
    public interface ITaskManagerRepository:IRepository<TaskManager, string>
    {
    }
}