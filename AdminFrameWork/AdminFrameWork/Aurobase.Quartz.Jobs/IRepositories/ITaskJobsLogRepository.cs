using System;
using Aurobase.Commons.IRepositories;
using Aurobase.Quartz.Models;
using Aurobase.Security.Models;

namespace Aurobase.Quartz.IRepositories
{
    /// <summary>
    /// 定義定時任務執行日誌倉儲介面
    /// </summary>
    public interface ITaskJobsLogRepository:IRepository<TaskJobsLog, string>
    {
    }
}