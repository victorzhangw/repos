using System;
using Aurobase.Commons.IDbContext;
using Aurobase.Commons.Repositories;
using Aurobase.Quartz.IRepositories;
using Aurobase.Quartz.Models;
using Aurobase.Security.IRepositories;
using Aurobase.Security.Models;

namespace Aurobase.Quartz.Repositories
{
    /// <summary>
    /// 定時任務執行日誌倉儲介面的實現
    /// </summary>
    public class TaskJobsLogRepository : BaseRepository<TaskJobsLog, string>, ITaskJobsLogRepository
    {
		public TaskJobsLogRepository()
        {
        }

        public TaskJobsLogRepository(IDbContextCore dbContext) : base(dbContext)
        {
        }
    }
}