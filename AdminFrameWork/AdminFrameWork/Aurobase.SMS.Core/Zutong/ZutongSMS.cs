using Newtonsoft.Json;
using StackExchange.Profiling.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Encrypt;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.Helpers;
using Aurobase.Commons.Log;
using Aurobase.Commons.Options;

namespace Aurobase.SMS.Zutong
{
    /// <summary>
    /// 助通科技融合云通訊
    /// </summary>
    public class ZutongSMS
    {

        public ZutongSMS()
        {
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            AppSetting sysSetting = AurobaseCacheHelper.Get<AppSetting>("SysSetting");
            if (sysSetting != null)
            {
                this.Appkey = DEncrypt.Decrypt(sysSetting.Smsusername);
                this.Appsecret = DEncrypt.Decrypt(sysSetting.Smspassword);
                this.Domain = sysSetting.Smsapiurl;
                this.SignName = sysSetting.SmsSignName;
            }
        }

        /// <summary>
        /// 簡訊發送
        /// </summary>
        /// <param name="cellPhone">必填:待發送手機號。支援以逗號分隔的形式進行批量呼叫，批量上限為1000個手機號碼,批量呼叫相對於單條呼叫及時性稍有延遲,驗證碼型別的簡訊推薦使用單條呼叫的方式，發送國際/港澳臺訊息時，接收號碼格式為00+國際區號+號碼，如「0085200000000」</param>
        /// <param name="templateCode">模板code</param>
        /// <param name="OutId">可選:outId為提供給業務方擴充套件欄位,最終在簡訊回執訊息中將此值帶回給呼叫者</param>
        /// <param name="message">可選:模板中的變數替換JSON串,如模板內容為"親愛的${name},您的驗證碼為${code}"時,此處的值為 "{\"name\":\"Tom\"， \"code\":\"123\"}"</param>
        /// <param name="returnMsg"></param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public bool Send(string cellPhone, string templateCode, string message, out string returnMsg, string OutId = "", string speed = "0")
        {
            if ((string.IsNullOrEmpty(cellPhone) || (cellPhone.Trim().Length == 0)))
            {
                returnMsg = "手機號碼和訊息內容不能為空";
                return false;
            }
            if (string.IsNullOrEmpty(message))
            {
                message = "";
            }
            try
            {
                SendSmsTp sendSmsTp = new SendSmsTp();
                TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                sendSmsTp.username = this.Appkey;
                sendSmsTp.password = Encode(this.Appsecret);
                sendSmsTp.tKey = (long)ts.TotalMilliseconds / 1000;
                sendSmsTp.signature = this.SignName;
                sendSmsTp.tpId = templateCode.ToLong();
                List<record> records = new List<record>();
                record item=   new record
                {
                    mobile = cellPhone,
                    tpContent = message
                };
                records.Add(item);
                sendSmsTp.records = records;
                var json = sendSmsTp.ToJson();
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var response =  HttpRequestHelper.HttpPost(this.Domain, data);
                SMSResult sMSResult = JsonConvert.DeserializeObject<SMSResult>(response);
                returnMsg = sMSResult.Msg;
                if (sMSResult.Code ==200)
                {
                    return true;
                }
                else
                {
                    Log4NetHelper.Error("發送簡訊錯誤，" + response.ToJson().ToString());
                    return false;
                }


            }
            catch (Exception e)
            {
                returnMsg = "未知錯誤(Exception )" + e.Message;
                return false;
            }
        }

        private static string Encode(string txt)
        {
            using (MD5 mi = MD5.Create())
            {
                byte[] buffer = Encoding.Default.GetBytes(txt);
                byte[] newBuffer = mi.ComputeHash(buffer);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < newBuffer.Length; i++)
                {
                    sb.Append(newBuffer[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }

        /// <summary>
        /// Appkey 應用Id
        /// </summary>
        public string Appkey { get; set; }
        /// <summary>
        /// Appsecret應用金鑰
        /// </summary>
        public string Appsecret { get; set; }

        /// <summary>
        /// 簽名
        /// </summary>
        public string SignName { get; set; }

        /// <summary>
        /// 簡訊API產品域名 Url地址
        /// </summary>
        public string Domain { get; set; }
    }
}
