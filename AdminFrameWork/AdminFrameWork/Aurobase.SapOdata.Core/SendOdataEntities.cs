﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurobase.Commons.Cache;
using Aurobase.Commons.Encrypt;
using Aurobase.Commons.Extensions;
using Aurobase.Commons.Json;
using Aurobase.Commons.Options;
using Aurobase.SapOdata.Models;
using RestSharp;
using RestSharp.Authenticators;

namespace Aurobase.SapOdata
{
    
    public static class SendOdataEntities
    {
        public static  void ConnectwithBasicAuth(Login login) {
            AurobaseCacheHelper AurobaseCacheHelper = new AurobaseCacheHelper();
            AppSetting sysSetting = AurobaseCacheHelper.Get<AppSetting>("SysSetting");
            if (sysSetting != null)
            {
                string apiUrl = sysSetting.SapC4CodataUrl;
                var client = new RestClient(apiUrl);
                client.Authenticator = new SimpleAuthenticator("username", "001", "password", "bar");
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("x-csrf-token", "fetch");
                IRestResponse response = client.Execute(request);
                var content = response.Content;
            }
            
           
        }
    }
}
