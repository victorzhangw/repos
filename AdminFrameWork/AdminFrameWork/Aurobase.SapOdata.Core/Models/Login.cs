﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurobase.SapOdata.Models
{
    public class Login
    {
        public string ServiceUrl { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
    }
}
