#pragma checksum "C:\Users\adrian\Documents\repos\WebAnalytics\Pages\Shared\Components\_EChartBar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1ff6c6e579398fb1f8b28194bcfa59be4c3afae9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(WebAnalytics.Pages.Shared.Components.Pages_Shared_Components__EChartBar), @"mvc.1.0.view", @"/Pages/Shared/Components/_EChartBar.cshtml")]
namespace WebAnalytics.Pages.Shared.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\adrian\Documents\repos\WebAnalytics\Pages\_ViewImports.cshtml"
using WebAnalytics;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1ff6c6e579398fb1f8b28194bcfa59be4c3afae9", @"/Pages/Shared/Components/_EChartBar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fbb9dcc39236bf02dcaaaf6b63e16157e62a01e2", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Shared_Components__EChartBar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<script type=\"text/x-template\" id=\"xbargraph\">\r\n    <div :id=\"chartid\" ref=\"panelBox\" style=\"display:block;width:100%;height:100%\"></div>\r\n</script>\r\n<script src=\"../Scripts/component/echart/vue-bargraph.js\"></script>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
