﻿
require.config({
    baseUrl: 'Scripts/utils',
   
    paths: {
        'settings': 'settings',
        'user':'user',
        'ejs':'ej2.vue',
        'axios':'axios.min',
        'echarts':'https://cdn.jsdelivr.net/npm/echarts@5.1.1/dist/echarts.min',
        'mitt': 'https://cdn.jsdelivr.net/npm/mitt@2.1.0/dist/mitt.umd',
        'chartBar':'../app/chart.bar',
        'chartLine':'../app/chart.line',
        'chartPie':'../app/chart.pie',
        'gaService':'../api/gaservice',

    }

});
