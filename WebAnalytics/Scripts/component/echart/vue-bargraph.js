﻿ /**  BAR */

    var ebarecomp = Vue.component("ec-bar-graph", {
        template: '#xbargraph',
        data: function () {
            return {
                chart: null,
                options: null,
                localData:null,
                width: 0,
  
            }
        },
        props: ["chartid", "mypanelsize"],
        watch: {
            mypanelsize: {
                handler(val) {
                    this.chartResize()
                },
                deep: true,
            }
        },
        created() {
            this.getLocalData()
            this.formatOption()
        },
        destroyed() {
            window.removeEventListener("resize", this.myEventHandler);
        },
        methods: {
            drawGraph() {

                this.chart = echarts.init(document.getElementById(this.chartid))
                this.options && this.chart.setOption(this.options)
            },
            chartResize() {
                this.chart.resize()
            },
            getLocalData() {
                
            },
            formatOption() {
                this.options = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                        }
                    },
                    textStyle: {
                        fontFamily: '思源黑體,源泉圓體, 微軟正黑體, 蘋方黑體, 華康麗黑體, Helvetica, Arial, sans-serif, serif',
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                            axisTick: {
                                alignWithLabel: true
                            }
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: '直接访问',
                            type: 'bar',
                            barWidth: '60%',
                            data: [10, 52, 200, 334, 390, 330, 220]
                        }
                    ]
                }
            }
            


        },
        mounted() {

            this.drawGraph()
            
        },

    })