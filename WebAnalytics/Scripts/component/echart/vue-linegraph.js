﻿/** Line*/
    var elinecomp = Vue.component("ec-line-graph", {
        // name: "lineGraph",
        //template: '<div :id="chartid" ref="panelBox" :style="styleObject"></div>',
        template: '#xlinegraph',
        data: function () {
            return {
                chart: null,
                options: null,
                localData:null,
                width: 0,
  
            }
        },
        props: ["chartid", "mypanelsize"],
        watch: {
            mypanelsize: {
                handler(val) {
                    this.chartResize()
                },
                deep: true,
            }
        },
        created() {
            this.getLocalData()
            this.formatOption()
        },
        destroyed() {
            window.removeEventListener("resize", this.myEventHandler);
        },
        methods: {
            drawGraph() {

                this.chart = echarts.init(document.getElementById(this.chartid))
                this.options && this.chart.setOption(this.options)
            },
            chartResize() {
                this.chart.resize()
            },
            getLocalData() {
                
            },
            formatOption() {
                this.options = {
                    xAxis: {
                        type: 'category',
                        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                    },
                    yAxis: {
                        type: 'value'
                    },
                    textStyle: {
                        fontFamily: '思源黑體, 微軟正黑體, 蘋方黑體, 華康麗黑體, Helvetica, Arial, sans-serif, serif',
                    },
                    grid: {
                        left: '4%',
                        right: '3%',
                        bottom: '3%',
                        height:'80%',//圖表高度
                        containLabel: true
                    },
                    series: [{
                        data: [820, 932, 901, 934, 1290, 1330, 1320],
                        type: 'line',
                        smooth: true
                    }]
                }
            }
            


        },
        mounted() {

            this.drawGraph()
            
        },

    })