﻿
/**  Pie graph **/
var epiecomp = Vue.component("ec-pie-graph", {
    // name: "lineGraph",
    //template: '<div :id="chartid" ref="panelBox" :style="styleObject"></div>',
    template: '#xpiegraph',
    data: function () {
        return {
            chart: null,
            options: null,
            localData: null,
            width: 0,

        }
    },
    props: ["chartid", "mypanelsize"],
    watch: {
        mypanelsize: {
            handler(val) {
                this.chartResize()
            },
            deep: true,
        }
    },
    created() {
        this.getLocalData()
        this.formatOption()
    },
    destroyed() {
        window.removeEventListener("resize", this.myEventHandler);
    },
    methods: {
        drawGraph() {

            this.chart = echarts.init(document.getElementById(this.chartid))
            this.options && this.chart.setOption(this.options)
        },
        chartResize() {
            this.chart.resize()
        },
        getLocalData() {

        },
        formatOption() {
            this.options = {

                title: {

                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'left',
                    data: ['直接存取', '郵件行銷', '聯盟廣告', '視訊廣告', '搜尋引擎']
                },
                textStyle: {
                    fontFamily: '思源黑體,源泉圓體, 微軟正黑體, 蘋方黑體, 華康麗黑體, Helvetica, Arial, sans-serif, serif',
                },
                series: [
                    {
                        name: '存取來源',
                        type: 'pie',
                        radius: '55%',
                        center: ['60%', '60%'],
                        data: [
                            { value: 335, name: '直接存取' },
                            { value: 310, name: '郵件行銷' },
                            { value: 234, name: '聯盟廣告' },
                            { value: 135, name: '視訊廣告' },
                            { value: 1548, name: '搜尋引擎' }
                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ]
            }
        }



    },
    mounted() {

        this.drawGraph()
        //console.log('ref', this.$refs.panelBox.clientWidth)
    },

})