﻿define(['../utils/request', '../utils/settings'], function (http, sets) {

    var defaultData = function () {
        const url = 'GoogleAnalytics/GAReports'
        //console.log('sets', sets.definition().apiHostUrl);
        let _s = new Date();
        let _e = new Date();
        _s.setDate(_s.getDate() - sets.definition().ga.startDays);
        _e.setDate(_e.getDate() - sets.definition().ga.endDays);
        
        let startDate = _s.toLocaleDateString('zh-TW', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        }).split('/').join('-')


        let enddate = _e.toLocaleDateString('zh-TW', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        }).split('/').join('-')
        data = {

            "EventStartDate": startDate,
            "EventEndDate": enddate,
            "ParameterDefinition": [
                {
                    "Dimension": "deviceCategory,pageTitle,channelGrouping",
                    "Metric": "users,bounceRate,organicSearches,pageviews,pageviewsPerSession,uniquePageviews,timeOnPage,exitRate,pageLoadTime"
                },
                {
                    "Dimension": "sourceMedium",
                    "Metric": "users,bounceRate,organicSearches,pageviews,pageviewsPerSession,uniquePageviews,timeOnPage,exitRate,pageLoadTime"
                },
                {
                    "Dimension": "sourceMedium,userType",
                    "Metric": "users,bounceRate,organicSearches,pageviews,pageviewsPerSession,uniquePageviews,timeOnPage,exitRate,pageLoadTime"
                },
                {
                    "Dimension": "sourceMedium,userBucket",
                    "Metric": "users,bounceRate,organicSearches,pageviews,pageviewsPerSession,uniquePageviews,timeOnPage,exitRate,pageLoadTime"
                }
            ]
        }
       
        var rst = http.request({
            url: url,
            method: 'post',
            data: data,
            baseURL: sets.definition().apiHostUrl // 直接通過覆蓋的方式
        })
       
        return rst
    }

    return {
        defaultData: defaultData
    }
    

})








