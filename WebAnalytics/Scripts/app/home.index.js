﻿
require(['../config'], function () {
    
    require(['home.index2']);
    require(['chartBar']);
    require(['chartLine']);
    require(['chartPie']);
   
    
})
define("home.index2", ['ejs', 'echarts', 'mitt', 'gaService', 'user','settings'], function ( ejs, echarts, mitt, gaService,user,sets) {
    //var mitt = require(mitt)
   // var ej = require(ejs)
    
    Vue.prototype.$echarts = echarts 
    Vue.prototype.$basesettings = this.settings 
    const emitter = mitt()
    //const Apiservice =gaService()
    
    header.dp(emitter,sets)
    header.layout(emitter, gaService,user)
    
    
    //bar.chart()
   // line.chart()
   // pie.chart()
    //chartbar.ebarecomp()
})


var header = (function () {

    let _count = 0;
    
    let datePicker = function (emitter,sets) {
        Vue.use(ejs.calendars.DateRangePickerPlugin)
        var dp = new Vue({
            el: '#maincalendar',
            data: function () {
                return {
                    waterMark: '選擇日期區間',
                    daterange: null,
                    startVal: new Date("12/10/2021"),
                    endVal: new Date("12/20/2021") ,
                    // enabled:false
                }

            },
            mounted() {
                let _d = this.setDefaultDate()
                console.log('_d',_d)
               // this.startVal = _d.startVal
               // this.endVal = _d.endVal
                this.getDateData()

            },
            methods: {
                getDateData() {
                    console.log('function', this.daterange)
                    emitter.emit('getDateRangeData', this.daterange);
                },
                setDefaultDate() {
                    let _s = new Date()
                    let _e = new Date()
                    
                    _s.setDate(_s.getDate() - sets.definition().ga.startDays)
                    _e.setDate(_e.getDate() - sets.definition().ga.endDays)
                    console.log('_s',_s)
                    return {
                        startVal: _s.toLocaleString(),
                        endVal: _e.toLocaleString()
                    }
                }
            }



        })
        dp.$watch('daterange', function (nVal, oVal) {
            // console.log('dp', nVal)
            this.getDateData()
        })
    };

    let layout = function (emitter, gaService,user) {
        let vmBody = new Vue(
            {
                el: '#app',
                data: function () {
                    return {
                        daterange: null,
                        headerCount: 0,
                        spacing: [10, 10],
                        cellAspect: 2,

                        layoutParas: {
                            row: 0,
                            col: 0,
                            sizeX: 0,
                            sizeY: 0,
                            panelid: null,
                            headerText: null,
                            panelSize: {

                                width: 0,
                                height: 0,
                                offsetWidth: 0,
                                offsetHeight: 0
                            }
                        },
                        layoutDefine: []
                    }

                },
                methods: {
                    getData() {

                        console.log("child", this.daterange)
                    },
                    getAnalyticsData() {

                    },
                    getDefaultData() {

                        console.log('gaService', gaService)

                        //gaService.defaultData()
                    },
                    myEventHandler(e) {
                        //console.log(this.$refs.dashboard.$el.children)
                        /*
                        for (var item of this.$refs.dashboard.$el.children) {
                            this.panelSize = {}
                            this.panelSize.width = item.clientWidth
                            this.panelSize.height = item.clientHeight
                            this.panelSize.offsetWidth = item.offsetWidth
                            this.panelSize.offsetHeight = item.clientHeight
                            this.panelDefine.push(this.panelSize)
                        }*/

                        for (let i = 0; i < this.$refs.dashboard.$el.children.length; i++) {
                            this.layoutDefine[i].panelSize.width = this.$refs.dashboard.$el.children[i].clientWidth
                            this.layoutDefine[i].panelSize.height = this.$refs.dashboard.$el.children[i].clientHeight
                            this.layoutDefine[i].panelSize.offsetWidth = this.$refs.dashboard.$el.children[i].offsetWidth
                            this.layoutDefine[i].panelSize.offsetHeight = this.$refs.dashboard.$el.children[i].clientHeight
                        }


                    }

                },
                created() {

                    window.addEventListener("resize", this.myEventHandler);
                    var panelData = [
                        { compName: 'ec-pie-graph', row: 0, col: 0, sizeX: 1, sizeY: 1, panelid: 'panel1', headerText: '過去7日使用者', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 1, col: 0, sizeX: 0, sizeY: 0, panelid: 'panel2', headerText: 'panel2', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: 'ec-bar-graph', row: 0, col: 1, sizeX: 1, sizeY: 2, panelid: 'panel3', headerText: 'panel3', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: 'ec-line-graph', row: 0, col: 2, sizeX: 1, sizeY: 1, panelid: 'panel4', headerText: 'panel4', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 1, col: 2, sizeX: 1, sizeY: 1, panelid: 'panel5', headerText: 'panel5', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 1, col: 3, sizeX: 1, sizeY: 2, panelid: 'panel6', headerText: 'panel6', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 2, col: 0, sizeX: 1, sizeY: 2, panelid: 'panel7', headerText: 'panel7', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 2, col: 1, sizeX: 1, sizeY: 1, panelid: 'panel8', headerText: 'panel8', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 3, col: 1, sizeX: 1, sizeY: 1, panelid: 'panel9', headerText: 'panel9', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 3, col: 2, sizeX: 1, sizeY: 2, panelid: 'panel10', headerText: 'panel10', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } },
                        { compName: '', row: 3, col: 3, sizeX: 1, sizeY: 2, panelid: 'panel11', headerText: 'panel11', panelSize: { width: 0, height: 0, offsetWidth: 0, offsetHeight: 0 } }
                    ];
                    this.layoutDefine = panelData
                },
                destroyed() {
                    window.removeEventListener("resize", this.myEventHandler);
                },
                mounted() {
                    emitter.on('getDateRangeData', (msg) => {
                        this.daterange = msg;
                        console.log("msg", msg);
                    });
                    this.myEventHandler()
                    this.getDefaultData()
                }


            })
    }

    return {
        dp: datePicker,
        layout: layout
    }

})();


