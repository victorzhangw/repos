const PieSeries = ejs.charts.PieSeries;
const AccumulationDataLabel = ejs.charts.AccumulationDataLabel
const AccumulationTooltip = ejs.charts.AccumulationTooltip
var pietemplate = Vue.component("contentTemp2", {
  template: `
<div style='display:block;height:100%; width:100%;'>
<ejs-accumulationchart class="chart-content" ref="accumulationInstance" style='display:block;height:100%; :width:100%;' :legendSettings="legendSettings" :tooltip="tooltip">
  <e-accumulation-series-collection>
    <e-accumulation-series :dataSource='seriesData' xName='x' yName='y' innerRadius="40%" :dataLabel="dataLabel"> </e-accumulation-series>
  </e-accumulation-series-collection>
</ejs-accumulationchart>
</div>`,
  data() {
    return {
      seriesData: {},
      legendSettings: { visible: false },
      dataLabel: { visible: true, position: 'Inside', name: 'text' },
      tooltip: {
        enable: true, header: '<b>${point.x}</b>', format: 'Composition: <b>${point.y}</b>'
      },
      height: '100%',
      width: '100%'
    };
  },
  provide: {
    accumulationchart: [PieSeries, AccumulationDataLabel, AccumulationTooltip]
  },
  created() {
    emitter.on('getData', (msg) => {
      this.seriesData = msg;
    });
  },
  mounted() {
    //this.testdata = 1

    //this.$refs.accumulationInstance.height = "100%";
    //this.$refs.accumulationInstance.width = "100%";
  }
});
