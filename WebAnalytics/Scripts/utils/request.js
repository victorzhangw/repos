﻿
define(['axios.min', 'settings'], function (axios, sets) {

    const service = axios.create({
        baseURL: sets.definition().apiHostUrl,
        timeout: 50000
    })
    service.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'

    // request interceptor
    
    service.interceptors.request.use(
        function (config) {
            config.headers['Content-Type'] = 'application/json;charset=UTF-8'
            const token = getToken()
            if (token) {
                config.headers['Authorization'] = 'Bearer ' + token
            }
        
            
            document.body.classList.add('loading-indicator');
            
            return config;
        },
        function (error) {
            
            return Promise.reject(error);
        }
    );
    service.interceptors.response.use(
        function (response) {

            document.body.classList.remove('loading-indicator');
           
            return response;

        },
        function (error) {
            document.body.classList.remove('loading-indicator');
            if (error.response) {
                switch (error.response.status) {
                    case 404:
                        console.log("你要找的頁面不存在")
                        // go to 404 page
                        break
                    case 500:
                        console.log("程式發生問題")
                        // go to 500 page
                        break
                    default:
                        console.log(error.message)
                }
            }
            if (!window.navigator.onLine) {
                alert("網路出了點問題，請重新連線後重整網頁");
                return;
            }
            return Promise.reject(error);
        }
    );
    return service
})





function getToken() {
    return '1'
}