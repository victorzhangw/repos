﻿define([], function () {
    var settings = function () {
        let definition = {
            "ga": {
                "startDays": 7,
                "endDays": 1
            },
            "apiHostUrl": "https://localhost:44372/api/"
        }
        return definition

    }
    return {
        definition: settings
    }

})

