﻿define([''], function () {
  
    const mutation = {

        Set_GADefault: (data) => {
            localStorage.setItem('_gadefault', JSON.stringify(data))
        },
        Get_GADefault: () => {
            return JSON.parse(localStorage.getItem('_gadefault'))
        },
        //item : LocalStorage Item ,key: 驗證碼(分割識別 "|")
        Validate_GADefault: (item, key) => {
            let bool = new Boolean(false);
            if (key&&item) {
                switch (item) {
                    case '_gadefault':
                        let _res = key.split('|')
                        let gaObj = this.Get_GADefault()
                        //let gaObj = JSON.parse(localStorage.getItem('_gadefault'))
                        if (gaObj.StartDate == _res[0] && gaObj.EndDate == _res[1]) {
                            bool =true
                        }
                        break
 
                    default:
                        console.log('default')
                }
            }
            return {
                Success: bool,
                Data: gaObj
            }
        }

    }
    return mutation
})