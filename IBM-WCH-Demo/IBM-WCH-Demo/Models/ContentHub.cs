﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IBM_WCH_Demo.Models
{
	public class ContentHub
	{
		public class RootAssets
		{
			public int numFound { get; set; }
			public AssetDocument[] documents { get; set; }
		}
		
		public class AssetDocument
		{
			public string id { get; set; }
			public string name { get; set; }
			public string classification { get; set; }
			public DateTime lastModified { get; set; }
			public string lastModifierId { get; set; }
			public DateTime created { get; set; }
			public string creatorId { get; set; }
			public string[] tags { get; set; }
			public string[] systemTags { get; set; }
			public string mediaType { get; set; }
			public string path { get; set; }
			public int fileSize { get; set; }
			public string location { get; set; }
			public string locationPaths { get; set; }
			public string assetType { get; set; }
			public string resource { get; set; }
			public string url { get; set; }
			public int height { get; set; }
			public int width { get; set; }
			public string document { get; set; }
			public string media { get; set; }
			public string thumbnail { get; set; }
			public bool isManaged { get; set; }
			public string status { get; set; }
			public bool restricted { get; set; }
			public string libraryId { get; set; }
			public DateTime systemModified { get; set; }
			public float dominantColor_0_d { get; set; }
			public float dominantColor_1_d { get; set; }
			public float dominantColor_2_d { get; set; }
			public string dominantColor { get; set; }
		}

		public class CustomAssets {
			public string[] tags { get; set; }
			
			public string mediaType { get; set; }
			public string path { get; set; }
			public int fileSize { get; set; }
			public string location { get; set; }
			public string locationPaths { get; set; }
			public string assetType { get; set; }
			public string resource { get; set; }
			public string url { get; set; }
			
		}

		public class AssetTags {

			public int Index { get; set; } = 0;
			public string Tag{get;set;}
		}
		public class SelectedTags
		{
			public List<string> Message { get; set; }
		}
		
	}
}
