﻿using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Newtonsoft.Json;
using IBM_WCH_Demo.Models;
using System.Collections.Generic;


namespace IBM_WCH_Demo.Controllers
{
	[Route("api/[controller]/[action]/")]
	[ApiController]
	public class AssetsController : ControllerBase
	{
		private readonly IConfiguration _config;
		public AssetsController(IConfiguration config)
		{
			_config = config;
		}

		
		[HttpGet]
		
		public string GetImage([FromQuery(Name = "stags")] string stags)
		{
			ContentHub.SelectedTags _tags = JsonConvert.DeserializeObject<ContentHub.SelectedTags>(stags);
			StringBuilder tagsb = new StringBuilder("fq=tags:(");
			for (int i = 0; i < _tags.Message.Count; i++) {
				
				if (i == _tags.Message.Count - 1)
				{
					tagsb.Append(_tags.Message[i] );
					tagsb.Append(")");
				}
				else {
					tagsb.Append(_tags.Message[i]);
					tagsb.Append(" OR ");
				}
			}
			Console.WriteLine(tagsb);
			var baseUrl = _config.GetSection("WCHBaseUrl");
			var apiuri = _config.GetSection("WCHApiUri");
			string restUrl = $"{baseUrl.Value}{apiuri.Value}/delivery/v1/search?q = *: *&fl = *&fq = classification:asset & fq = assetType:image&"+tagsb;
			var client = new RestClient(restUrl);
			var request = new RestRequest(Method.GET);

			IRestResponse response = client.Execute(request);
			ContentHub.RootAssets obj = JsonConvert.DeserializeObject<ContentHub.RootAssets>(response.Content);
			var rst = from doc in obj.documents
					  select new ContentHub.CustomAssets()
					  {
						  tags = doc.tags,
						  mediaType = doc.mediaType,
						  path = doc.path,
						  fileSize = doc.fileSize,
						  location = doc.location,
						  locationPaths = doc.locationPaths,
						  assetType = doc.assetType,
						  resource = doc.resource,
						  url = baseUrl.Value + doc.url

					  };
			return JsonConvert.SerializeObject(rst); ;
		}
		public string GetImageTags(string parameters)
		{
			var baseUrl = _config.GetSection("WCHBaseUrl");
			var apiuri = _config.GetSection("WCHApiUri");
			string restUrl = $"{baseUrl.Value}{apiuri.Value}/delivery/v1/search?q = *: *&fl=tags&fq = classification:asset & fq = assetType:image&rows=1000&fq=libraryId:a09dc8a9-6bdc-41b5-a4d4-3923dc94998a";
			var client = new RestClient(restUrl);
			var request = new RestRequest(Method.GET);
			List<ContentHub.AssetTags> _assettags = new List<ContentHub.AssetTags>();
			IRestResponse response = client.Execute(request);
			ContentHub.RootAssets obj = JsonConvert.DeserializeObject<ContentHub.RootAssets>(response.Content);
			HashSet<string> tagList = new HashSet<string>();
			
			for (int i = 0; i < obj.documents.Length; i++) {
				if (!IsNullOrEmpty(obj.documents[i].tags)) {
					foreach (string tag in obj.documents[i].tags)
					{
						try
						{
							tagList.Add(tag);
						}
						catch (Exception ex)
						{
							Console.WriteLine(ex);
						}

					}

				}
				
			}
			var _tagList = tagList.OrderBy(tags => tags);
			int index = 0;
			foreach (var l in _tagList)
			{
				index++;
				var tagVal = new ContentHub.AssetTags() { Tag = l,Index=index };
				_assettags.Add(tagVal);
			}
			

			return JsonConvert.SerializeObject(_assettags); 
		}
		/// <summary>Indicates whether the specified array is null or has a length of zero.</summary>
		/// <param name="array">The array to test.</param>
		/// <returns>true if the array parameter is null or has a length of zero; otherwise, false.</returns>
		public static bool IsNullOrEmpty(Array array)
		{
			return (array == null || array.Length == 0);
		}
	}	
}