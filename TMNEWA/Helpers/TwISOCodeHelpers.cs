﻿using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Threading.Tasks;

namespace EnsembleCRM.Helpers
{
    public class TwISOCodeHelpers
    {
        private readonly IConfiguration _configuration;
        public TwISOCodeHelpers( IConfiguration config)
        {
            
            _configuration = config;
        }
        public List<RegionIso> IsoCodeConverter(string value)
        {
            List<RegionIso> regionIso = new List<RegionIso>();
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            string checkSql = @"SELECT isocode,shortname,fullname,areaname FROM `isocode` 
            as a inner join `area` as b on a.area_id=b.id where isocode=@isocode";
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {


                    regionIso = connection.Query<RegionIso>(checkSql,new {isocode=value}).ToList();
                    
                }

            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                
            }
            return regionIso;
        }
    }
    public class RegionIso
    {
        public string IsoCode { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string AreaName { get; set; }
    }

}
