﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System.Net.Mail;
using EnsembleCRM.Models;
using NLog.Time;

namespace EnsembleCRM.Helpers
{
    public  class HangFireStart
    {
        private  static IConfiguration _configuration;
        private static ILogger<HangFireStart> _logger { get; set; }
       
        public HangFireStart(IConfiguration config,ILogger<HangFireStart> logger)
        {

            _configuration = config;
            _logger = logger;
           
        }
        public  void Register()
        {
            
            // WebApiRole.SendController oController = new WebApiRole.SendController();
            // fire and got:站台啟動後只會執行一次
            //BackgroundJob.Enqueue(() => Console.WriteLine("Fire and forgot"));

            // delay: 設定時間間隔，每隔時間間隔執行一次
            //BackgroundJob.Schedule(() => Console.WriteLine("Delayed"), TimeSpan.FromDays(1));

            // recurring: 設定cron敘述，重複執行多次,指定 timezoneinfo （預設為 UTC）
            RecurringJob.AddOrUpdate(() => ImportTealeafEventsfromEmail(), Cron.Daily(1,20), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(() => DownloadDatafromHBT(), Cron.Daily(1,30), TimeZoneInfo.Local);
            RecurringJob.AddOrUpdate(() => ImportDatafromHBT(), Cron.Daily(1,40), TimeZoneInfo.Local);

            // continue: 在某個job執行完後接續執行
            //var id = BackgroundJob.Enqueue(() => Console.WriteLine("Hello, "));
            //BackgroundJob.ContinueWith(id, () => Console.WriteLine("world!"));

            //BackgroundJob.Enqueue(() => TestFail());
        }
        public static void   ImportTealeafEventsfromEmail()
        {

            try
            {
                string restUrl = _configuration.GetSection("RestUrl").Value;
                string apiMethod= "TealeafEvent/ImportEvents";
                var client = new RestClient(restUrl+ apiMethod);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                //Console.WriteLine(response.Content);
            }
            catch (Exception ex)
            {
                _logger.LogError($"<---HangFire : ImportTealeafEventsfromEmail : {0}---> " + ex.Message);
            }
            //var logger = NLogBuilder.ConfigureNLog("defaultNlog.config").GetCurrentClassLogger();
            
            
        }
        public static void DownloadDatafromHBT()
        {

            try
            {
                string restUrl = _configuration.GetSection("RestUrl").Value;
                string apiMethod = "HbtService/DownloadData";
                var client = new RestClient(restUrl + apiMethod);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                
    
            }
            catch (Exception ex)
            {
                _logger.LogError($"<---HangFire : Download HBT Data : {0}---> " + ex.Message);
                
            }
            //var logger = NLogBuilder.ConfigureNLog("defaultNlog.config").GetCurrentClassLogger();
            

        }
        public static void ImportDatafromHBT()
        {

            try
            {
                string restUrl = _configuration.GetSection("RestUrl").Value;
                string apiMethod = "HbtService/SaveData";
                var client = new RestClient(restUrl + apiMethod);
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", "", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                //Console.WriteLine(response.Content);
            }
            catch (Exception ex)
            {
                _logger.LogError($"<---HangFire : Save HBT Data : {0}---> " + ex.Message);
            }
            //var logger = NLogBuilder.ConfigureNLog("defaultNlog.config").GetCurrentClassLogger();


        }

    }
}
