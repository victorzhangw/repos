﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace EnsembleCRM.Helpers
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
    public class CommonHelpers
    {
        private readonly IConfiguration _configuration;
        private ILogger<CommonHelpers> _logger { get; set; }
        public CommonHelpers(IConfiguration config, ILogger<CommonHelpers> logger)
        {
           
            _configuration = config;
           
            _logger = logger;

        }

        
        public void SendAutomatedEmail(MailContextModel mailContextModel)
        {
            var smtpServer = _configuration.GetSection("EmailConfiguration:SmtpServer").Value;
            var smtpPort = Int32.Parse(_configuration.GetSection("EmailConfiguration:SmtpPort").Value);
            var userName = _configuration.GetSection("EmailConfiguration:SmtpUsername").Value;
            var userPassword = _configuration.GetSection("EmailConfiguration:SmtpPassword").Value;

            MailMessage MyMail = new MailMessage();
            MyMail.From = new System.Net.Mail.MailAddress(mailContextModel.FromAddress);
            MyMail.To.Add(mailContextModel.ToAddress); //設定收件者Email
                                        // MyMail.Bcc.Add("密件副本的收件者Mail"); //加入密件副本的Mail          
            MyMail.Subject = mailContextModel.MailHead;
            MyMail.Body = mailContextModel.MailBody; //設定信件內容
            MyMail.IsBodyHtml = true; //是否使用html格式
            SmtpClient MySMTP = new SmtpClient(smtpServer, smtpPort);
            MySMTP.EnableSsl = true;
            MySMTP.Credentials = new System.Net.NetworkCredential(userName, userPassword);
            try
            {
                MySMTP.Send(MyMail);
                MyMail.Dispose(); //釋放資源
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ex.ToString();
            }
        }
        public static bool IsBig5Encoding(byte[] bytes)
        {
            Encoding big5 = Encoding.GetEncoding(950);
            //將byte[]轉為string再轉回byte[]看位元數是否有變
            return bytes.Length ==
                big5.GetByteCount(big5.GetString(bytes));
        }
        //偵測檔案否為BIG5編碼
        public static bool IsBig5Encoding(string file)
        {
            if (!File.Exists(file)) return false;
            return IsBig5Encoding(File.ReadAllBytes(file));
        }
        public static string FormatStringTwoDecimal(string Str)
        {
            string formatted = String.Empty;
            if (Str.Contains("."))
            {
                string[] tmpStr = Str.Split(".");
                string xvalue1 = tmpStr[0];
                string xvalue2 = tmpStr[1];


                if (xvalue2.Length > 2)
                {
                    formatted = xvalue1 + "." + xvalue2.Substring(0, 2);
                }
                else
                {
                    formatted = xvalue1 + "." + xvalue2;
                }

            }
            else
            {
                formatted = Str;
            }
            return formatted;
        }
        public static class EnumHelper
        {
            public static List<T> ToList<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>().ToList<T>();
            }

            public static IEnumerable<T> ToEnumerable<T>()
            {
                return Enum.GetValues(typeof(T)).Cast<T>();
            }

        }
    }
    public class MailContextModel
    {
        public string ToAddress { get; set; }
        public string FromAddress { get; set; }
        public string MailHead { get; set; }
        public string MailBody { get; set; }
    }
}
