﻿using EnsembleCRM.Models;
using ChoETL;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EnsembleCRM.Helpers
{
    public class CsvHelpers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        private readonly IConfiguration _configuration;
        private ILogger<CsvHelpers> _logger { get; set; }
        private CommonHelpers CommonHelpers { get; set; }
        private DataInsightHelpers _dataInsight;
        public CsvHelpers(IConfiguration config ,ILogger<CsvHelpers> logger,CommonHelpers commonHelpers,DataInsightHelpers dataInsightHelpers)
        {
            CommonHelpers = commonHelpers;
            _configuration = config;
            _logger = logger;
            _dataInsight = dataInsightHelpers; 
        }

        MailContextModel mailContextModel = new MailContextModel()
        {
            ToAddress = "victor.cheng@aurocore.com",
            FromAddress = "caco.tealeaf@gmail.com"
        };
        public int ParseCsvFiles()
        {
            
            int recordCount=0;
            var outDirPath = _configuration.GetSection("EmailConfiguration:outDirPath").Value;
            var addDays = Int32.Parse(_configuration.GetSection("EmailConfiguration:EventDay").Value);
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            int reportCount = Int32.Parse(_configuration.GetSection("Tealeaf:EventReportCount").Value);
            var eventSection = _configuration.GetSection("Tealeaf:Events").Get<List<List<string>>>();
            string insertCommand = "insert into tealeaf (eventstring,eventname,eventdate) values(@eventstring,@eventname,@eventdate)";
            string selectCommand = "select count(*) from  tealeaf where eventdate = @eventdate";
            DateTime eventDate = DateTime.Now.Date.AddDays(addDays);
            outDirPath = outDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
            try
            {
                for (int i = 0; i < eventSection.Count; i++)
                {
                    var fileName = Directory
                    .EnumerateFiles(outDirPath, "*", SearchOption.AllDirectories)
                    .Where(file => file.EndsWith(".csv") && file.Contains(eventSection[i][0]))
                    .Select(Path.GetFileName).FirstOrDefault(); // <-- note you can shorten the lambda
                    if (fileName != null)
                    {

                        string filePath =Path.GetFullPath(Path.Combine(outDirPath, fileName));
                        using var sr = new StreamReader(filePath);
                        string[] fileType = fileName.Split("~");
                        for (int j = 0; j < int.Parse(eventSection[i][1]); j++)
                        {
                            sr.ReadLine();
                        }
                        //  string remainingText = sr.ReadToEnd();
                        StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());
                        StringBuilder sb = new StringBuilder();
                        
                        using (var csv = new ChoCSVReader(remainingText))
                        {
                            using (var parser = new ChoJSONWriter(sb))
                            {


                                parser.Write(csv);

                                var eventstring = StringFormatter(sb.Append("]").ToString());
                                if (eventstring.Length < 2)
                                {
                                    eventstring = @"[{ Column1:0,Column2:0,Column3:0,Column4:0,Column5:0,Column6:0,Column7:0}]";
                                }
                                var tealeafEvent = new Models.TealeafEvent.TealeafEvents()
                                {
                                    eventdate = eventDate,
                                    eventstring = eventstring,
                                    eventname = fileType[0]
                                };
                                using (var connection = new SqlConnection(cnStr.Value))
                                {
                                    recordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });

                                    if (recordCount <reportCount)
                                    {
                                        
                                        var insertResult = connection.Execute(insertCommand, tealeafEvent);
                                    }


                                }


                            }

                        }

                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"<--- Tealeaf Mail 匯入資料庫 --->{0} "+ ex.Message);
                mailContextModel.MailHead = "Tealeaf Mail 匯入資料庫失敗";
                mailContextModel.MailBody = ex.Message;
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            finally
            {
                _logger.LogInformation($"<--- Tealeaf Mail 匯入資料庫 --->{0} " + "成功");
            }

            // Get only the CSV files.
            try
            {
                string filePath = Path.GetFullPath(Path.Combine(outDirPath));
                foreach (string file in Directory.GetFiles(filePath))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                mailContextModel.MailHead = "Tealeaf Mail 匯入資料庫失敗";
                mailContextModel.MailBody = ex.Message;
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            finally
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    recordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });

                   

                }
            }
            return (recordCount);
        }
        public int ParseTMEvents()
        {
            int recordCount = 0;
            string insertCommand = @"INSERT INTO tmevents.ubx_ibmvideoplayed
        (version, provider, source, channel, x1id, event_code, event_timestamp, 
        event_namespace, event_version, identifier_deviceid, identifier_googleanalyticscookie, 
        event_attribute_interactionid, event_attribute_browsername, 
        event_attribute_browserversion, event_attribute_vendor,
        event_attribute_model, event_attribute_manufacturer, 
        event_attribute_modelname, event_attribute_modelyear, 
        event_attribute_ip, event_attribute_locationcontinent, event_attribute_locationcountry,
        event_attribute_locationcity, event_attribute_latitude, event_attribute_longitude, 
        event_attribute_os, event_attribute_versionos, event_attribute_subchannel, 
        event_attribute_devicetype, event_attribute_tldatacenter, event_attribute_tlorgcode, 
        event_attribute_appkey, event_attribute_tlprocid, event_attribute_eventname, 
        event_attribute_elementid, identifier_dacookieid)
        VALUES(
        @version, @provider, @source, @channel, @x1id, @event_code, @event_timestamp, @
        event_namespace, @event_version, @identifier_deviceid, @identifier_googleanalyticscookie, @
        event_attribute_interactionid, @event_attribute_browsername, @
        event_attribute_browserversion, @event_attribute_vendor,
        event_attribute_model, @event_attribute_manufacturer, @
        event_attribute_modelname, @event_attribute_modelyear, @
        event_attribute_ip, @event_attribute_locationcontinent, @event_attribute_locationcountry,
        event_attribute_locationcity, @event_attribute_latitude, @event_attribute_longitude, @
        event_attribute_os, @event_attribute_versionos, @event_attribute_subchannel, @
        event_attribute_devicetype, @event_attribute_tldatacenter, @event_attribute_tlorgcode, @
        event_attribute_appkey, @event_attribute_tlprocid, @event_attribute_eventname, @
        event_attribute_elementid, @identifier_dacookieid);
            ";
            var ibmvideoplayed = new List<TealeafEvent.ubx_ibmvideoplayed>();

            return recordCount;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "<Pending>")]
        public int ParseHBTFiles()
        {
            int recordCount = 0;
            var sendmailzone = _configuration.GetSection("Logging:host").Value;
            var outDirPath = _configuration.GetSection("FtpConfiguration:ftp1:filepath").Value;
            outDirPath = outDirPath.Replace("-", Path.DirectorySeparatorChar.ToString());
            var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
            var eventSection = _configuration.GetSection("FtpConfiguration:ftp1:reports").Get<List<List<string>>>();
            DateTime dt = DateTime.Now;
            string create_date = dt.ToLocalTime().ToString("yyyyMMddHHmmss");
            string modify_date = dt.ToLocalTime().ToString("yyyyMMddHHmmss");
            string startdate = dt.ToLocalTime().ToString("yyyy-MM-dd");
            string eventstartdate = dt.ToLocalTime().AddDays(-1).ToString("yyyy-MM-dd");
            string enddate = dt.ToLocalTime().ToString("yyyy-MM-dd");
            string eventenddate = dt.ToLocalTime().AddDays(-1).ToString("yyyy-MM-dd");
            var addDays = Int32.Parse(_configuration.GetSection("EmailConfiguration:EventDay").Value);
            var selectCommand = "select count(*) from orders where date_format(orderdate,'%Y%m%d') =date_format(@eventDate,'%Y%m%d')";
            DateTime eventDate = DateTime.Now.Date.AddDays(addDays);
            try
            {
                using (var connection = new SqlConnection(cnStr.Value))
                {

                    recordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });
                }
                if (recordCount == 0)
                {
                    var files = Directory
                        .EnumerateFiles(outDirPath, "*", SearchOption.AllDirectories)
                        .Where(file => file.EndsWith(".csv"))
                        .Select(Path.GetFileName);
                    if(files.Count()== eventSection.Count)
                    {
                        for (int i = 0; i < eventSection.Count; i++)
                        {


                            string filekeyWord = eventSection[i][3];
                            var fileName = Directory
                            .EnumerateFiles(outDirPath, "*", SearchOption.AllDirectories)

                            // .Where(file => file.EndsWith(".csv") && file.Contains(eventSection[i][0]))
                            .Where(file => file.Contains(eventSection[i][0]))
                            .Select(Path.GetFileName).FirstOrDefault(); // <-- note you can shorten the lambda

                            if (fileName != null)
                            {

                                bool isbig5 = false;
                                string filePath = Path.GetFullPath(Path.Combine(outDirPath, fileName));
                                //using var sr = new StreamReader(filePath);
                                string[] fileType = fileName.Split("~");
                                List<HbtData.City> enumCityList = CommonHelpers.EnumHelper.ToList<HbtData.City>();
                                switch (filekeyWord)
                                {
                                    case "members":


                                        string chkMembershipSql = @"SELECT
                                    MAX(CASE WHEN s.memberrank = '一般' THEN s.membership end )AS 'regular',
                                    MAX(CASE WHEN s.memberrank = 'VIP' THEN s.membership end )AS 'vip',
                                    MAX(CASE WHEN s.memberrank = 'SVIP' THEN s.membership end )AS 'svip'
                                    FROM(
                                    SELECT memberrank,count(memberrank) AS membership
                                    FROM member 
                                    WHERE memberrank='一般' OR memberrank='VIP' OR memberrank='SVIP'
                                    GROUP BY memberrank) s";
                                        string eventName = "Member-NumberofMember";
                                        string source = "membership";
                                        List<HbtData.Members> listMember = new List<HbtData.Members>();
                                        isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                        if (isbig5)
                                        {
                                            File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                        }
                                        using (var sr = new StreamReader(filePath, Encoding.GetEncoding(950)))
                                        {
                                            for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                            {
                                                sr.ReadLine();
                                            }
                                            StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                            StringBuilder sb = new StringBuilder();


                                            using (var csv = new ChoCSVReader(remainingText))
                                            {

                                                foreach (dynamic col in csv)
                                                {

                                                    var member = new HbtData.Members()
                                                    {
                                                        channel = col[0],
                                                        memberid = col[1].Replace("'", ""),
                                                        uuid = col[2],
                                                        membername = col[3],
                                                        registerdate = col[4],
                                                        memberemail = col[5],
                                                        gender = col[6],
                                                        mobile = col[7],
                                                        country = col[8],
                                                        city = col[9],
                                                        area = col[10],
                                                        memberrank = col[11],
                                                        bonus = col[12],
                                                        create_date = create_date,
                                                        modify_date = modify_date


                                                    };
                                                    listMember.Add(member);
                                                }
                                                //Console.WriteLine(listMember);
                                            }
                                        }

                                        using (var connection = new SqlConnection(cnStr.Value))
                                        {
                                            connection.Open();
                                            using (var trans = connection.BeginTransaction())
                                            {

                                                var insertResult = connection.Execute(eventSection[i][1], listMember, trans);
                                                var membershipRank = connection.QuerySingleOrDefault<HbtData.NumberofMember>(chkMembershipSql, transaction: trans);
                                                _dataInsight.SaveEventtoDB(JsonConvert.SerializeObject(membershipRank), eventName, source, eventstartdate, eventenddate);
                                                trans.Commit();
                                            }

                                        }

                                        break;
                                    case "online_order_raw":
                                        List<HbtData.Orders> listOrder = new List<HbtData.Orders>();
                                        List<HbtData.Orders> detailListOrder = new List<HbtData.Orders>();
                                        isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                        if (isbig5)
                                        {
                                            File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                        }
                                        using (var sr = new StreamReader(filePath, Encoding.GetEncoding(950)))
                                        {
                                            for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                            {
                                                sr.ReadLine();
                                            }
                                            StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                            StringBuilder sb = new StringBuilder();


                                            using (var csv = new ChoCSVReader(remainingText))
                                            {

                                                foreach (dynamic col in csv)
                                                {
                                                    
                                                    string cityName = string.Empty;
                                                    string regionName = string.Empty;
                                                    if (!string.IsNullOrEmpty(col[14]) & col[0] == "m")
                                                    {
                                                        var cityList = from r in enumCityList
                                                                       where col[14].Contains(r.ToString())
                                                                       select r.ToString();


                                                        if (!cityList.Any())
                                                        {
                                                            cityName = "其他";
                                                            string addressString = col[14];
                                                            var splitRegion = addressString.Split(" ");
                                                            regionName = splitRegion[0];
                                                        }
                                                        else
                                                        {
                                                            regionName = "臺灣";
                                                            cityName = cityList.FirstOrDefault().ToString();
                                                        }

                                                    }


                                                    var order = new HbtData.Orders()
                                                    {
                                                        ordertype = col[0],
                                                        channel = col[1],
                                                        orderno = col[2],
                                                        orderdate = col[3],
                                                        orderstatus = col[4],
                                                        memberid = col[5].Replace("'", ""),
                                                        membername = col[6],
                                                        productid = col[7],
                                                        productcategory = col[8],
                                                        unitprice = col[9],
                                                        saleprice = col[10],
                                                        quantity = col[11],
                                                        amount = col[12],
                                                        totalamount = col[13],
                                                        shippingaddress = col[14],
                                                        ordercampaign = col[15],
                                                        create_date = create_date,
                                                        modify_date = modify_date,
                                                        region = regionName,
                                                        city = cityName


                                                    };
                                                    if (col[0] == "d" & col[4] == "成立")
                                                    {
                                                        var detailOrder = new HbtData.Orders()
                                                        {
                                                            ordertype = col[0],
                                                            channel = col[1],
                                                            orderno = col[2],
                                                            orderdate = col[3],
                                                            orderstatus = col[4],
                                                            memberid = col[5].Replace("'", ""),
                                                            membername = col[6],
                                                            productid = col[7],
                                                            productcategory = col[8],
                                                            unitprice = col[9],
                                                            saleprice = col[10],
                                                            quantity = col[11],
                                                            amount = col[12],
                                                            totalamount = col[13],
                                                            shippingaddress = col[14],
                                                            ordercampaign = col[15],
                                                            create_date = create_date,
                                                            modify_date = modify_date,
                                                            region = regionName,
                                                            city = cityName


                                                        };
                                                        detailListOrder.Add(detailOrder);
                                                    }

                                                    listOrder.Add(order);

                                                }

                                            }
                                        }

                                        using (var connection = new SqlConnection(cnStr.Value))
                                        {
                                            connection.Open();
                                            using (var trans = connection.BeginTransaction())
                                            {
                                                var insertResult1 = connection.Execute(eventSection[i][1], listOrder, trans);
                                                var insertResult2 = connection.Execute(eventSection[i][4], detailListOrder, trans);
                                                trans.Commit();
                                            }

                                        }

                                        break;
                                    case "offline_order_raw":
                                        List<HbtData.Orders> offlistOrder = new List<HbtData.Orders>();
                                        List<HbtData.Orders> offdetailListOrder = new List<HbtData.Orders>();
                                        isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                        if (isbig5)
                                        {
                                            File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                        }
                                        using (var sr = new StreamReader(filePath, Encoding.GetEncoding(950)))
                                        {
                                            for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                            {
                                                sr.ReadLine();
                                            }
                                            StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                            StringBuilder sb = new StringBuilder();


                                            using (var csv = new ChoCSVReader(remainingText))
                                            {


                                                foreach (dynamic col in csv)
                                                {
                                                    var regionName = string.Empty;
                                                    var cityName = string.Empty;
                      
                                                    if (!string.IsNullOrWhiteSpace(col[4]))
                                                    {
                                                        string[] shopInfo = col[4].Split('|');
                                                        if (shopInfo[2].Length > 1)
                                                        {
                                                            var cityList = from r in enumCityList
                                                                           where shopInfo[2].Contains(r.ToString())
                                                                           select r.ToString();
                                                            if (!cityList.Any())
                                                            {
                                                                cityName = "其他";
                                                                regionName = "其他";
                                                            }
                                                            else
                                                            {
                                                                regionName = "臺灣";
                                                                cityName = cityList.FirstOrDefault().ToString();
                                                            }

                                                        }
                                                    }
                                                    var order = new HbtData.Orders()
                                                    {
                                                        ordertype = col[0],
                                                        channel = col[1],
                                                        orderno = col[2],
                                                        orderdate = col[3],
                                                        shop = col[4],
                                                        orderstatus = col[5],
                                                        memberid = col[6].Replace("'", ""),
                                                        membername = col[7],
                                                        productid = col[8],
                                                        productcategory = col[9],
                                                        unitprice = col[10],
                                                        saleprice = col[11],
                                                        quantity = col[12],
                                                        amount = col[13],
                                                        totalamount = col[14],
                                                        shippingaddress = col[15],
                                                        ordercampaign = col[16],
                                                        create_date = create_date,
                                                        modify_date = modify_date,
                                                        region = regionName,
                                                        city = cityName


                                                    };
                                                    if (col[0] == "d" & col[5] == "成立")
                                                    {
                                                        var detailOrder = new HbtData.Orders()
                                                        {
                                                            ordertype = col[0],
                                                            channel = col[1],
                                                            orderno = col[2],
                                                            orderdate = col[3],
                                                            shop = col[4],
                                                            orderstatus = col[5],
                                                            memberid = col[6].Replace("'", ""),
                                                            membername = col[7],
                                                            productid = col[8],
                                                            productcategory = col[9],
                                                            unitprice = col[10],
                                                            saleprice = col[11],
                                                            quantity = col[12],
                                                            amount = col[13],
                                                            totalamount = col[14],
                                                            shippingaddress = col[15],
                                                            ordercampaign = col[16],
                                                            create_date = create_date,
                                                            modify_date = modify_date,
                                                            region = regionName,
                                                            city = cityName


                                                        };
                                                        offdetailListOrder.Add(detailOrder);
                                                    }

                                                    offlistOrder.Add(order);

                                                }

                                            }
                                        }

                                        using (var connection = new SqlConnection(cnStr.Value))
                                        {
                                            connection.Open();
                                            using (var trans = connection.BeginTransaction())
                                            {
                                                var insertResult1 = connection.Execute(eventSection[i][1], offlistOrder, trans);
                                                var insertResult2 = connection.Execute(eventSection[i][4], offdetailListOrder, trans);
                                                trans.Commit();
                                            }

                                        }

                                        break;

                                    case "products":

                                        List<HbtData.Products> listProduct = new List<HbtData.Products>();
                                        isbig5 = CommonHelpers.IsBig5Encoding(filePath);
                                        if (isbig5)
                                        {
                                            File.WriteAllText(string.Format(filePath), File.ReadAllText(filePath, Encoding.GetEncoding("Big5")), Encoding.UTF8);
                                        }

                                        using (var sr = new StreamReader(filePath))
                                        {
                                            for (int j = 0; j < int.Parse(eventSection[i][2]); j++)
                                            {
                                                sr.ReadLine();
                                            }
                                            StringBuilder remainingText = new StringBuilder(sr.ReadToEnd());

                                            // StringBuilder sb = new StringBuilder();


                                            using (var csv = new ChoCSVReader(remainingText))
                                            {

                                                foreach (dynamic col in csv)
                                                {

                                                    var product = new HbtData.Products()
                                                    {
                                                        product_id = col[0],
                                                        product_group_id = col[1],
                                                        title = col[2],
                                                        description = col[3],
                                                        analyze_title = col[4],
                                                        analyze_description = col[5],
                                                        image_link = col[6],
                                                        availability = col[8],
                                                        price = col[9],
                                                        currency = col[10],
                                                        brand = col[11],
                                                        product_type = col[12],
                                                        condition = col[14],
                                                        size = col[15],
                                                        size_system = col[16],
                                                        color = col[17],
                                                        pattern = col[18],

                                                        gender = col[21],
                                                        sale_price = col[22],
                                                        custom_label_0 = col[30],
                                                        custom_label_1 = col[31],
                                                        custom_label_2 = col[32],
                                                        custom_label_3 = col[33],
                                                        custom_label_4 = col[34],
                                                        create_date = create_date,
                                                        modify_date = modify_date

                                                    };
                                                    listProduct.Add(product);
                                                }

                                            }
                                        }

                                        using (var connection = new SqlConnection(cnStr.Value))
                                        {
                                            connection.Open();
                                            using (var trans = connection.BeginTransaction())
                                            {
                                                var insertResult = connection.Execute(eventSection[i][1], listProduct, trans);
                                                trans.Commit();

                                            }

                                        }
                                        break;

                                }


                            }
                        }
                    }
                    else
                    {
                      //  throw new Exception("今日檔案數量有誤，需檢查海博FTP");
                        mailContextModel.MailHead = ($"海博資料匯入{sendmailzone}資料庫失敗");
                        mailContextModel.MailBody = "今日檔案數量有誤，需檢查海博FTP";
                        CommonHelpers.SendAutomatedEmail(mailContextModel);
                        return 0;
                    }
                    
                } 
                
            }
            catch (Exception ex)
            {
                _logger.LogError($"<--- 海博資料匯入{sendmailzone}資料庫  ");
                mailContextModel.MailHead = ($"海博資料匯入{sendmailzone}資料庫失敗" );
                mailContextModel.MailBody = ex.Message;
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            finally
            {
                _logger.LogInformation($"<--- Mail 匯入資料庫 --->{0} " + "成功");
            }

            // Get only the CSV files.
            try
            {
                string filePath = Path.GetFullPath(Path.Combine(outDirPath));
                foreach (string file in Directory.GetFiles(filePath))
                {
                    File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                
            }
            finally
            {
                int resultrecordCount = 0;
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    
                    resultrecordCount = connection.ExecuteScalar<int>(selectCommand, new { eventdate = eventDate });
                }
                if (recordCount> 0)
                {
                    mailContextModel.MailHead = $"海博資料未匯入{sendmailzone}資料庫";
                    mailContextModel.MailBody = $"{DateTime.Now:yyyy/MM/dd}-{sendmailzone}資料庫已有訂單: {recordCount} 筆";
                    
                }
                else
                {
                    mailContextModel.MailHead = $"海博資料匯入{sendmailzone}資料庫成功";
                    mailContextModel.MailBody = $"{DateTime.Now:yyyy/MM/dd}-{sendmailzone}海博訂單資料匯入 {resultrecordCount} 筆";
                    recordCount = resultrecordCount;
                }
                
                CommonHelpers.SendAutomatedEmail(mailContextModel);
            }
            return recordCount;
        }
        
        private static string DateFormatter(string datecolumn)
        {
            CultureInfo MyCultureInfo = new CultureInfo("zh-TW");
            DateTime MyDateTime = DateTime.Parse(datecolumn, MyCultureInfo);
            var formatteredDate = MyDateTime.ToString("yyyy-MM-dd");
            return formatteredDate;
        }
        private static string StringFormatter(string input)
        {
            //Regex pattern = new Regex("<\\/div>|<div>", RegexOptions.Compiled);
            string eraseString = Regex.Replace(input, @"</div>|<div>", String.Empty);
            return eraseString;
        }



    }
    }
