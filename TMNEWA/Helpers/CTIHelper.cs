﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using EnsembleCRM.Models;
using Dapper;
using EnsembleCRM.Interfaces;


namespace EnsembleCRM.Helpers
{
   
    public class CTIHelper: IInboundCall
    {
        private static IConfiguration _configuration;
        private static ILogger<CTIHelper> _logger { get; set; }
        public CTIHelper(IConfiguration config, ILogger<CTIHelper> logger)
        {

            _configuration = config;

            _logger = logger;

        }

        public List<CTI.InfoACDCalls> ReadCalls(int days)
        {

            var cnStr = _configuration.GetSection("ConnectionStrings:InfoCTIConnection");
            string currDate = DateTime.Now.AddDays(days*-1).ToString("yyyy-MM-dd");
            string selectCommand = "SELECT CID,C_FROM,TIME_STAMP FROM[dbo].[CALLS] WHERE TIME_STAMP>=@currDate";
            List<CTI.InfoACDCalls> aCDCalls = new List<CTI.InfoACDCalls>();
            using (var connection = new SqlConnection(cnStr.Value))
            {
                aCDCalls = connection.Query<CTI.InfoACDCalls>(selectCommand, new { currDate = currDate }).ToList();
            }
            return aCDCalls;


        }
        public List<CTI.InfoACDCalls> DateDateRangeCalls(DateTime sdate, DateTime edate)
        {
            var cnStr = _configuration.GetSection("ConnectionStrings:InfoCTIConnection");
            string currDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string selectCommand = "SELECT CID,C_FROM,TIME_STAMP FROM[dbo].[CALLS] WHERE TIME_STAMP>=@sdate AND TIME_STAMP<=@edate";
            List<CTI.InfoACDCalls> aCDCalls = new List<CTI.InfoACDCalls>();
            using (var connection = new SqlConnection(cnStr.Value))
            {
                aCDCalls = connection.Query<CTI.InfoACDCalls>(selectCommand, new { currDate = currDate }).ToList();
            }
            return aCDCalls;
        }

    }
}
