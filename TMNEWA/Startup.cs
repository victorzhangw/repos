using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnsembleCRM.Helpers;
using EnsembleCRM.Interfaces;
namespace EnsembleCRM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            services.AddResponseCompression();
            services.AddSingleton<TwISOCodeHelpers>();
            services.AddSingleton<JwtHelpers>();
            services.AddSingleton<MailHelpers>();
            services.AddSingleton<HangFireStart>();
            services.AddSingleton<CsvHelpers>();
            services.AddSingleton<TealeafEventHelpers>();
            services.AddSingleton<FtpHelpers>();
            services.AddSingleton<CommonHelpers>();
            services.AddSingleton<DataInsightHelpers>();
            services.AddTransient<IInboundCall,CTIHelper>();
            services.AddTransient<MailInterface, AuroMailHelpers>();
            services.AddTransient<JobInterface.InboundEvtInterface, InboundEvtHelpers.MailEvent>();
            services.AddTransient<JobInterface.InboundEvtInterface, InboundEvtHelpers.FaxEvent>();
            services.AddCors(options =>
            {
                // CorsPolicy �O�ۭq�� Policy �W��
                options.AddPolicy("CorsPolicy", policy =>
                {
                    policy.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();


                });
            });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseResponseCompression();
            app.UseStaticFiles();
            app.UseCors("CorsPolicy");
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
