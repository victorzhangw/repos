﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using EnsembleCRM.Helpers;
using EnsembleCRM.Models;
using EnsembleCRM.Interfaces;

namespace EnsembleCRM.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CTIController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private ILogger<CTIController> _logger { get; set; }
        private IInboundCall _ctiHelper;
        private MailInterface _auroMailHelper;
        public CTIController(IConfiguration config,  ILogger<CTIController> logger, IInboundCall cTIHelper,MailInterface mailInterface)
        {
            _logger = logger;
            _configuration = config;
            _ctiHelper = cTIHelper;
            _auroMailHelper = mailInterface;
        }
        /// <summary>
        /// 依據需要天數取用
        /// </summary>
        /// <param name="days"></param>
        /// <param name="ReceiveMail"></param>
        /// <param name="Subject"></param>
        /// <param name="FromAddress"></param>
        /// <param name="bcc"></param>
        /// <returns></returns>
        [HttpPost]
        
        public IActionResult ReadbyDays([FromForm] int days, [FromForm] string ReceiveMail, [FromForm] string Subject,
            [FromForm]  string FromAddress, [FromForm] string bcc )
        {
            var callList = _ctiHelper.ReadCalls(days);
            Subject += "--"+DateTime.Now.ToString("yyyy-MM-dd");
            string mailBody = string.Empty;
            if (callList.Count > 0)
            {
                string trRows = string.Empty;

                for (int i = 0; i < callList.Count; i++)
                {

                    string _td1 = callList[i].CID.Substring(0, 8);
                    string _td2 = callList[i].CID.Substring(8, 4);
                    string _td3 = callList[i].C_From;
                    string trTmp = $"<tr><td>{_td1}</td><td>{_td2}</td><td>{_td3}</td></tr>";
                    trRows += trTmp;
                }
                mailBody = $@"<body> 
                        <table cellpadding=""5"" cellspacing=""2"" width=""640"" align=""center"" border=""1"">     
                            <tr><td> 日期 </td ><td> 時間 </td ><td> 電話 </td> </tr>
                            {trRows}  <tr><td colspan=""2"">本日進線</td><td>{callList.Count}筆</td></tr> 
                        </ table >
                        </ body > ";
            }
            else
            {
                mailBody = $@"<body> 
                        <table cellpadding=""5"" cellspacing=""2"" width=""640"" align=""center"" border=""1"">     
                            <tr><td> 日期 </td><td> 時間 </td><td> 電話 </td> </tr>
                           <tr><td colspan=""2"">本日進線</td><td>0筆</td></tr> 
                        </ table >
                        </ body > ";
            }
            _auroMailHelper.SendEmail(ReceiveMail, Subject, mailBody, FromAddress);
            return Ok(callList);
        }
    }
}
