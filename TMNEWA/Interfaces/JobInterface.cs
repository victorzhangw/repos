﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EnsembleCRM.Models;
namespace EnsembleCRM.Interfaces
{
    interface JobInterface
    {
        public interface InboundEvtInterface
        {
            List<InboundEvent> ReadAll();
            int Create(InboundEvent inboundEvent);
            int Update(InboundEvent inbound);
            int Delete(int id);
            InboundEvent  Read(int id);
            int ImportData();

        }
    }
}
