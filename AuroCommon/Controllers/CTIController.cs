﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Dapper;
using AuroCommon.Models;
using AuroCommon.Assests;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.SignalR.Protocol;

namespace AuroCommon.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CTIController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private CommonAssests _commonAssests;
        public CTIController(IConfiguration configuration,CommonAssests commonAssests)
        {
            _configuration = configuration;
            _commonAssests = commonAssests;
        }
        [HttpGet]
        public IActionResult DailyCalls()
        {
            try
            {
                string returnStr = string.Empty;
                var cnStr = _configuration.GetSection("ConnectionStrings:DefaultConnection");
                string currDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                string selectCommand = "SELECT CID,C_FROM,TIME_STAMP FROM[dbo].[CALLS] WHERE TIME_STAMP>=@currDate";
                List<InfoACDCalls> aCDCalls = new List<InfoACDCalls>();
                Universal.MailContextModel mailContext = new Universal.MailContextModel();
                mailContext.FromAddress = "caco.tealeaf@gmail.com";
                mailContext.ToAddress = "carol.chen@aurocore.com";
                mailContext.MailHead = "每日進線資料通知";
                using (var connection = new SqlConnection(cnStr.Value))
                {
                    aCDCalls = connection.Query<InfoACDCalls>(selectCommand, new { currDate = currDate }).ToList();
                    if (aCDCalls.Count > 0)
                    {
                        string trRows = string.Empty;

                        for (int i = 0; i < aCDCalls.Count; i++)
                        {

                            string _td1 = aCDCalls[i].CID.Substring(0, 8);
                            string _td2 = aCDCalls[i].CID.Substring(8, 4);
                            string _td3 = aCDCalls[i].C_From;
                            string trTmp = $"<tr><td>{_td1}</td><td>{_td2}</td><td>{_td3}</td></tr>";
                            trRows += trTmp;
                        }
                        mailContext.MailBody = $@"<body> 
                        <table cellpadding=""5"" cellspacing=""2"" width=""640"" align=""center"" border=""1"">     
                            <tr><td> 日期 </td ><td> 時間 </td ><td> 電話 </td> </tr>
                            {trRows}  <tr><td colspan=""2"">本日進線</td><td>{aCDCalls.Count}筆</td></tr> 
                        </ table >
                        </ body > ";
                    }
                    else
                    {
                        mailContext.MailBody = $@"<body> 
                        <table cellpadding=""5"" cellspacing=""2"" width=""640"" align=""center"" border=""1"">     
                            <tr><td> 日期 </td><td> 時間 </td><td> 電話 </td> </tr>
                           <tr><td colspan=""2"">本日進線</td><td>0筆</td></tr> 
                        </ table >
                        </ body > ";
                    }

                    _commonAssests.SendAutomatedEmail(mailContext);

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return Ok(ex.ToString());
            }
            
                return Ok();
        } 

    }
}
