﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuroCommon.Models
{
    public class Universal
    {
        public class MailContextModel
        {
            public string ToAddress { get; set; }
            public string FromAddress { get; set; }
            public string MailHead { get; set; }
            public string MailBody { get; set; }
        }
    }
}
